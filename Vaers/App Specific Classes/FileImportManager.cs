﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vaers
{

    public class FileImportManager
    {

        #region Static Methods





        #endregion

        #region Interfaces


        interface DataFile
        {
            // Defines the expected column headers depending on data file type
            List<string> ExpectedColHeaders { get; }

            // Users required to implement an import and ToSqlInserts
            bool LoadFromFile(string FilePath);
            bool LoadFromFile(Utilities.CSVParser FileContent);
            List<string> ToSqlInsertCmds(List<uint> IDsToIgnore);
        }


        #endregion

        #region Sub Classes


        public class TableMain : DataFile
        {
            // Defines a data row for this type of file
            public class Record
            {
                // Vars to hold info in this type of data file
                public uint ID { get; set; } = 0;
                public string DateFiled { get; set; } = string.Empty;
                public double AgeOfPatient { get; set; } = 0;
                public string Sex { get; set; } = string.Empty;
                public string IsDead { get; set; } = string.Empty;
                public string IsRecovered { get; set; } = string.Empty;
                public uint NumOfDays { get; set; } = 0;
                public string OtherMeds { get; set; } = string.Empty;
                public string OtherIllness { get; set; } = string.Empty;
                public string History { get; set; } = string.Empty;
                public string Allergies { get; set; } = string.Empty;
                public string Description { get; set; } = string.Empty;

                // Read only vars for returning clean strings that might have sql escapes
                public string CleanOtherMeds
                {
                    get
                    {
                        return CleanSql(OtherMeds);
                    }
                }
                public string CleanOtherIllness
                {
                    get
                    {
                        return CleanSql(OtherIllness);
                    }
                }
                public string CleanHistory
                {
                    get
                    {
                        return CleanSql(History);
                    }
                }
                public string CleanAllergies
                {
                    get
                    {
                        return CleanSql(Allergies);
                    }
                }
                public string CleanDescription
                {
                    get
                    {
                        return CleanSql(Description);
                    }
                }
            }

            // Var to hold all loaded records
            public List<Record> Records { internal set; get; } = new List<Record>();

            // The expected cols for this type of data file
            public List<string> ExpectedColHeaders
            {
                get
                {
                    // Create list to return
                    var ListToReturn = new List<string>();

                    // Add all items
                    ListToReturn.Add("VAERS_ID");
                    ListToReturn.Add("RECVDATE");
                    ListToReturn.Add("STATE");
                    ListToReturn.Add("AGE_YRS");
                    ListToReturn.Add("CAGE_YR");
                    ListToReturn.Add("CAGE_MO");
                    ListToReturn.Add("SEX");
                    ListToReturn.Add("RPT_DATE");
                    ListToReturn.Add("SYMPTOM_TEXT");
                    ListToReturn.Add("DIED");
                    ListToReturn.Add("DATEDIED");
                    ListToReturn.Add("L_THREAT");
                    ListToReturn.Add("ER_VISIT");
                    ListToReturn.Add("HOSPITAL");
                    ListToReturn.Add("HOSPDAYS");
                    ListToReturn.Add("X_STAY");
                    ListToReturn.Add("DISABLE");
                    ListToReturn.Add("RECOVD");
                    ListToReturn.Add("VAX_DATE");
                    ListToReturn.Add("ONSET_DATE");
                    ListToReturn.Add("NUMDAYS");
                    ListToReturn.Add("LAB_DATA");
                    ListToReturn.Add("V_ADMINBY");
                    ListToReturn.Add("V_FUNDBY");
                    ListToReturn.Add("OTHER_MEDS");
                    ListToReturn.Add("CUR_ILL");
                    ListToReturn.Add("HISTORY");
                    ListToReturn.Add("PRIOR_VAX");
                    ListToReturn.Add("SPLTTYPE");
                    ListToReturn.Add("FORM_VERS");
                    ListToReturn.Add("TODAYS_DATE");
                    ListToReturn.Add("BIRTH_DEFECT");
                    ListToReturn.Add("OFC_VISIT");
                    ListToReturn.Add("ER_ED_VISIT");
                    ListToReturn.Add("ALLERGIES");

                    // Return the list
                    return ListToReturn;
                }
            }

            // Implement load methods
            public bool LoadFromFile(string FilePath)
            {
                try
                {
                    // Log debug info
                    ThisApp.Methods.Logging.WriteMsg("Attempting to load main table data using file path [ " + FilePath + " ]...", ThisApp.Methods.Logging.MsgTypeOptions.Info);

                    // Report error if file is bad
                    if (string.IsNullOrEmpty(FilePath) == true) throw new Exception("Input Vaers CSV file is missing!");
                    if (System.IO.File.Exists(FilePath) == false) throw new Exception("The Vaers file [ " + FilePath + " ] was not found!");

                    // If we look good, attempt to load
                    ThisApp.Methods.Logging.WriteMsg("Creating CSV parsing helper...", ThisApp.Methods.Logging.MsgTypeOptions.Info);
                    var CSV = new Utilities.CSVParser();

                    // Attempt to load
                    if (CSV.Load(FilePath) == false)
                    {
                        throw new Exception("Unable to load CSV file [ " + FilePath + " ] using parser!");
                    }

                    // Once loaded, use underlying method
                    return LoadFromFile(CSV);
                }
                catch (Exception ex)
                {
                    // Return error if we can't figure it out
                    ThisApp.Methods.Logging.WriteMsgWithException("Exception while attempting to load data!", ex);
                    return false;
                }
            }
            public bool LoadFromFile(Utilities.CSVParser FileContent)
            {
                try
                {
                    // Ensure records are initialized
                    Records = new List<Record>();

                    // Log debug info
                    ThisApp.Methods.Logging.WriteMsg("Attempting to load main table data using input CSV parser object...", ThisApp.Methods.Logging.MsgTypeOptions.Info);

                    // Report error if missing info
                    if (FileContent == null)
                    {
                        throw new Exception("No file content found in CSV Parser object!");
                    }

                    // Return false if this isn't the correct column structure (don't throw error for better speed)
                    if (DoHeadersMatch(FileContent.CSVContent, ExpectedColHeaders) == false)
                    {
                        // Log and return false
                        ThisApp.Methods.Logging.WriteMsg("This file object is not the correct type, skipping.", ThisApp.Methods.Logging.MsgTypeOptions.Warning);
                        return false;
                    }

                    // If we're good, loop through and create objects (ignore header row)
                    for (int i = 1; i <= FileContent.CSVContent.GetUpperBound(0); i++)
                    {
                        // Get all string info
                        string NextId = FileContent.CSVContent[i, 0];
                        string NextDateVaxxed = FileContent.CSVContent[i, 1];
                        string NextAge = FileContent.CSVContent[i, 3];
                        string NextSex = FileContent.CSVContent[i, 6];
                        string NextIsDead = FileContent.CSVContent[i, 9];
                        string NextIsRecovered = FileContent.CSVContent[i, 17];
                        string NextNumOfDays = FileContent.CSVContent[i, 20];
                        string NextOtherMeds = FileContent.CSVContent[i, 24];
                        string NextOtherIllness = FileContent.CSVContent[i, 25];
                        string NextHistory = FileContent.CSVContent[i, 26];
                        string NextAllergies = FileContent.CSVContent[i, 34];
                        string NextDescription = FileContent.CSVContent[i, 8];

                        // Skip if ID isn't legit
                        if (StaticMethods.Strings.TypeCheck.IsUInt32(NextId) == false)
                        {
                            ThisApp.Methods.Logging.WriteMsg("ID on line index [ " + i.ToString() + " ] isn't numeric, skipping...", ThisApp.Methods.Logging.MsgTypeOptions.Warning);
                            continue;
                        }

                        // Skip if date isn't legit
                        if (StaticMethods.Strings.TypeCheck.IsDate(NextDateVaxxed) == false)
                        {
                            ThisApp.Methods.Logging.WriteMsg("Vax date on line index [ " + i.ToString() + " ] isn't valid, skipping...", ThisApp.Methods.Logging.MsgTypeOptions.Warning);
                            continue;
                        }

                        // If age is missing, use 0
                        if (StaticMethods.Strings.TypeCheck.IsDouble(NextAge) == false)
                        {
                            NextAge = "0";
                        }

                        // Ensure IsDead isn't null and is uppercase
                        if (NextIsDead == null) NextIsDead = string.Empty;
                        NextIsDead = NextIsDead.ToUpper();

                        // Clean up is dead ID
                        if (NextIsDead != "Y" && NextIsDead != "N") NextIsDead = "?";

                        // Ensure IsRecovered isn't null and is uppercase
                        if (NextIsRecovered == null) NextIsRecovered = string.Empty;
                        NextIsRecovered = NextIsDead.ToUpper();

                        // Clean up is dead ID
                        if (NextIsRecovered != "Y" && NextIsRecovered != "N") NextIsRecovered = "?";

                        // If num of days is missing, use 0
                        if (StaticMethods.Strings.TypeCheck.IsUInt32(NextNumOfDays) == false)
                        {
                            NextNumOfDays = "0";
                        }

                        // Create list to hold common terms used to express none / no entry
                        var NoneEntries = new[] { "NONE", "N/A", "UNKNOWN", "NO", "UNK", "NO OTHER MEDICATIONS" }.ToList();

                        // Clean up entries that might have aliases for none
                        if (NoneEntries.Contains(NextOtherMeds.ToUpper()) == true) NextOtherMeds = string.Empty;
                        if (NoneEntries.Contains(NextOtherIllness.ToUpper()) == true) NextOtherIllness = string.Empty;
                        if (NoneEntries.Contains(NextHistory.ToUpper()) == true) NextHistory = string.Empty;

                        // At this point we're good, new up class to hold info
                        var NextRecord = new Record
                        {
                            ID = Convert.ToUInt32(NextId),
                            DateFiled = StaticMethods.Strings.Convert.DateToSortableString(Convert.ToDateTime(NextDateVaxxed), false),
                            AgeOfPatient = Convert.ToDouble(NextAge),
                            Sex = NextSex,
                            IsDead = NextIsDead,
                            IsRecovered = NextIsRecovered,
                            NumOfDays = Convert.ToUInt32(NextNumOfDays),
                            OtherMeds = NextOtherMeds,
                            OtherIllness = NextOtherIllness,
                            History = NextHistory,
                            Allergies = NextAllergies,
                            Description = NextDescription
                        };

                        // Add record to collection
                        Records.Add(NextRecord);
                    }

                    // Return true if processed
                    ThisApp.Methods.Logging.WriteMsg("Records imported successfully.", ThisApp.Methods.Logging.MsgTypeOptions.Info);
                    return true;
                }
                catch (Exception ex)
                {
                    // Return error if we can't figure it out
                    ThisApp.Methods.Logging.WriteMsgWithException("Exception while attempting to load data!", ex);
                    return false;
                }
            }

            // Implement method to convert to sql insert cmds
            public List<string> ToSqlInsertCmds(List<uint> IDsToIgnore)
            {
                // Ensure list isn't null ever
                if (IDsToIgnore == null) IDsToIgnore = new List<uint>();

                // Create a copy of records with IDs to ignore omitted
                var CopyOfRecords = (from AllInfo in Records where IDsToIgnore.Contains(AllInfo.ID) == false select AllInfo).ToList();

                // Create list to return
                var ListOfSql = new List<string>(CopyOfRecords.Count + 1);

                // Loop through and create SQL for IDs in range
                foreach (var RECORD in CopyOfRecords)
                {
                    // If it's in range, add sql insert cmd
                    ListOfSql.Add("INSERT INTO " + DatabaseManager.Database.Table.Main + " (ID, DateVaxxed, AgeOfPatient, Sex, IsDead, IsRecovered, NumOfDays, OtherMeds, OtherIllness, History, Allergies, Description) VALUES (" + RECORD.ID + ", '" + RECORD.DateFiled + "', " + RECORD.AgeOfPatient + ", '" + RECORD.Sex + "', '" + RECORD.IsDead + "', '" + RECORD.IsRecovered + "', " + RECORD.NumOfDays + ", '" + RECORD.CleanOtherMeds + "', '" + RECORD.CleanOtherIllness + "', '" + RECORD.CleanHistory + "', '" + RECORD.CleanAllergies + "', '" + RECORD.CleanDescription + "')");
                }

                // Return list after built
                return ListOfSql;
            }

            // Override ToString() for ease of reading
            public override string ToString()
            {
                return StaticMethods.Strings.XML.Serialize(this);
            }
        }

        public class TableSymptom : DataFile
        {
            // Defines a data row for this type of file
            public class Record
            {
                // Vars to hold info in this type of data file
                public uint ID { get; set; } = 0;
                public string Symptom { get; set; } = string.Empty;

                // Read only vars for returning clean strings that might have sql escapes
                public string CleanSymptom
                {
                    get
                    {
                        return CleanSql(Symptom);
                    }
                }
            }

            // Var to hold all loaded records
            public List<Record> Records { internal set; get; } = new List<Record>();

            // The expected cols for this type of data file
            public List<string> ExpectedColHeaders
            {
                get
                {
                    // Create list to return
                    var ListToReturn = new List<string>();

                    // Add all items
                    ListToReturn.Add("VAERS_ID");
                    ListToReturn.Add("SYMPTOM1");
                    ListToReturn.Add("SYMPTOMVERSION1");
                    ListToReturn.Add("SYMPTOM2");
                    ListToReturn.Add("SYMPTOMVERSION2");
                    ListToReturn.Add("SYMPTOM3");
                    ListToReturn.Add("SYMPTOMVERSION3");
                    ListToReturn.Add("SYMPTOM4");
                    ListToReturn.Add("SYMPTOMVERSION4");
                    ListToReturn.Add("SYMPTOM5");
                    ListToReturn.Add("SYMPTOMVERSION5");

                    // Return the list
                    return ListToReturn;
                }
            }

            // Implement load methods
            public bool LoadFromFile(string FilePath)
            {
                try
                {
                    // Log debug info
                    ThisApp.Methods.Logging.WriteMsg("Attempting to load symptom table data using file path [ " + FilePath + " ]...", ThisApp.Methods.Logging.MsgTypeOptions.Info);

                    // Report error if file is bad
                    if (string.IsNullOrEmpty(FilePath) == true) throw new Exception("Input Vaers CSV file is missing!");
                    if (System.IO.File.Exists(FilePath) == false) throw new Exception("The Vaers file [ " + FilePath + " ] was not found!");

                    // If we look good, attempt to load
                    ThisApp.Methods.Logging.WriteMsg("Creating CSV parsing helper...", ThisApp.Methods.Logging.MsgTypeOptions.Info);
                    var CSV = new Utilities.CSVParser();

                    // Attempt to load
                    if (CSV.Load(FilePath) == false)
                    {
                        throw new Exception("Unable to load CSV file [ " + FilePath + " ] using parser!");
                    }

                    // Once loaded, use underlying method
                    return LoadFromFile(CSV);
                }
                catch (Exception ex)
                {
                    // Return error if we can't figure it out
                    ThisApp.Methods.Logging.WriteMsgWithException("Exception while attempting to load data!", ex);
                    return false;
                }
            }
            public bool LoadFromFile(Utilities.CSVParser FileContent)
            {
                try
                {
                    // Ensure records are initialized
                    Records = new List<Record>();

                    // Log debug info
                    ThisApp.Methods.Logging.WriteMsg("Attempting to load symptom table data using input CSV parser object...", ThisApp.Methods.Logging.MsgTypeOptions.Info);

                    // Report error if missing info
                    if (FileContent == null)
                    {
                        throw new Exception("No file content found in CSV Parser object!");
                    }

                    // Return false if this isn't the correct column structure (don't throw error for better speed)
                    if (DoHeadersMatch(FileContent.CSVContent, ExpectedColHeaders) == false)
                    {
                        // Log and return false
                        ThisApp.Methods.Logging.WriteMsg("This file object is not the correct type, skipping.", ThisApp.Methods.Logging.MsgTypeOptions.Warning);
                        return false;
                    }

                    // If we're good, loop through and create objects (ignore header row)
                    for (int i = 1; i <= FileContent.CSVContent.GetUpperBound(0); i++)
                    {
                        // Get all string info
                        string NextId = FileContent.CSVContent[i, 0];
                        string NextSymptom1 = FileContent.CSVContent[i, 1];
                        string NextSymptom2 = FileContent.CSVContent[i, 3];
                        string NextSymptom3 = FileContent.CSVContent[i, 5];
                        string NextSymptom4 = FileContent.CSVContent[i, 7];
                        string NextSymptom5 = FileContent.CSVContent[i, 9];

                        // Skip if ID isn't legit
                        if (StaticMethods.Strings.TypeCheck.IsUInt32(NextId) == false)
                        {
                            ThisApp.Methods.Logging.WriteMsg("ID on line index [ " + i.ToString() + " ] isn't numeric, skipping...", ThisApp.Methods.Logging.MsgTypeOptions.Warning);
                            continue;
                        }

                        // Create list to hold all non-empty symptoms
                        var AllNonEmptySymptoms = new List<string>();

                        // Add all non-empty symptoms to list for looping
                        foreach (var SYMPTOM in new[] { NextSymptom1, NextSymptom2, NextSymptom3, NextSymptom4, NextSymptom5 })
                        {
                            // Add if there
                            if (string.IsNullOrEmpty(SYMPTOM) == false)
                            {
                                AllNonEmptySymptoms.Add(SYMPTOM);
                            }
                        }

                        // Add record for all non-empties
                        foreach (var NONEMPTY in AllNonEmptySymptoms)
                        {
                            Records.Add(new Record { ID = Convert.ToUInt32(NextId), Symptom = NONEMPTY });
                        }
                    }

                    // Return true if processed
                    ThisApp.Methods.Logging.WriteMsg("Records imported successfully.", ThisApp.Methods.Logging.MsgTypeOptions.Info);
                    return true;
                }
                catch (Exception ex)
                {
                    // Return error if we can't figure it out
                    ThisApp.Methods.Logging.WriteMsgWithException("Exception while attempting to load data!", ex);
                    return false;
                }
            }

            // Implement method to convert to sql insert cmds
            public List<string> ToSqlInsertCmds(List<uint> IDsToIgnore)
            {
                // Ensure list isn't null ever
                if (IDsToIgnore == null) IDsToIgnore = new List<uint>();

                // Create a copy of records with IDs to ignore omitted
                var CopyOfRecords = (from AllInfo in Records where IDsToIgnore.Contains(AllInfo.ID) == false select AllInfo).ToList();

                // Create list to return
                var ListOfSql = new List<string>(CopyOfRecords.Count + 1);

                // Loop through and create SQL for IDs in range
                foreach (var RECORD in CopyOfRecords)
                {
                    // If it's in range, add sql insert cmd
                    ListOfSql.Add("INSERT INTO " + DatabaseManager.Database.Table.Symptoms + " (ID, Symptom) VALUES (" + RECORD.ID + ", '" + RECORD.CleanSymptom + "')");
                }

                // Return list after built
                return ListOfSql;
            }

            // Override ToString() for ease of reading
            public override string ToString()
            {
                return StaticMethods.Strings.XML.Serialize(this);
            }
        }

        public class TableVaxInfo : DataFile
        {
            // Defines a data row for this type of file
            public class Record
            {
                // Vars to hold info in this type of data file
                public uint ID { get; set; } = 0;
                public string Type { get; set; } = string.Empty;
                public string VaxMfg { get; set; } = string.Empty;
                public string Lot { get; set; } = string.Empty;
                public string VaxSite { get; set; } = string.Empty;
                public string VaxName { get; set; } = string.Empty;

                // Read only vars for returning clean strings that might have sql escapes
                public string CleanType
                {
                    get
                    {
                        return CleanSql(Type);
                    }
                }
                public string CleanVaxMfg
                {
                    get
                    {
                        return CleanSql(VaxMfg);
                    }
                }
                public string CleanLot
                {
                    get
                    {
                        return CleanSql(Lot);
                    }
                }
                public string CleanVaxSite
                {
                    get
                    {
                        return CleanSql(VaxSite);
                    }
                }
                public string CleanVaxName
                {
                    get
                    {
                        return CleanSql(VaxName);
                    }
                }
            }

            // Var to hold all loaded records
            public List<Record> Records { internal set; get; } = new List<Record>();

            // The expected cols for this type of data file
            public List<string> ExpectedColHeaders
            {
                get
                {
                    // Create list to return
                    var ListToReturn = new List<string>();

                    // Add all items
                    ListToReturn.Add("VAERS_ID");
                    ListToReturn.Add("VAX_TYPE");
                    ListToReturn.Add("VAX_MANU");
                    ListToReturn.Add("VAX_LOT");
                    ListToReturn.Add("VAX_DOSE_SERIES");
                    ListToReturn.Add("VAX_ROUTE");
                    ListToReturn.Add("VAX_SITE");
                    ListToReturn.Add("VAX_NAME");

                    // Return the list
                    return ListToReturn;
                }
            }

            // Implement load methods
            public bool LoadFromFile(string FilePath)
            {
                try
                {
                    // Log debug info
                    ThisApp.Methods.Logging.WriteMsg("Attempting to load vax info table data using file path [ " + FilePath + " ]...", ThisApp.Methods.Logging.MsgTypeOptions.Info);

                    // Report error if file is bad
                    if (string.IsNullOrEmpty(FilePath) == true) throw new Exception("Input Vaers CSV file is missing!");
                    if (System.IO.File.Exists(FilePath) == false) throw new Exception("The Vaers file [ " + FilePath + " ] was not found!");

                    // If we look good, attempt to load
                    ThisApp.Methods.Logging.WriteMsg("Creating CSV parsing helper...", ThisApp.Methods.Logging.MsgTypeOptions.Info);
                    var CSV = new Utilities.CSVParser();

                    // Attempt to load
                    if (CSV.Load(FilePath) == false)
                    {
                        throw new Exception("Unable to load CSV file [ " + FilePath + " ] using parser!");
                    }

                    // Once loaded, use underlying method
                    return LoadFromFile(CSV);
                }
                catch (Exception ex)
                {
                    // Return error if we can't figure it out
                    ThisApp.Methods.Logging.WriteMsgWithException("Exception while attempting to load data!", ex);
                    return false;
                }
            }
            public bool LoadFromFile(Utilities.CSVParser FileContent)
            {
                try
                {
                    // Ensure records are initialized
                    Records = new List<Record>();

                    // Log debug info
                    ThisApp.Methods.Logging.WriteMsg("Attempting to load vax info table data using input CSV parser object...", ThisApp.Methods.Logging.MsgTypeOptions.Info);

                    // Report error if missing info
                    if (FileContent == null)
                    {
                        throw new Exception("No file content found in CSV Parser object!");
                    }

                    // Return false if this isn't the correct column structure (don't throw error for better speed)
                    if (DoHeadersMatch(FileContent.CSVContent, ExpectedColHeaders) == false)
                    {
                        // Log and return false
                        ThisApp.Methods.Logging.WriteMsg("This file object is not the correct type, skipping.", ThisApp.Methods.Logging.MsgTypeOptions.Warning);
                        return false;
                    }

                    // If we're good, loop through and create objects (ignore header row)
                    for (int i = 1; i <= FileContent.CSVContent.GetUpperBound(0); i++)
                    {
                        // Get all string info
                        string NextId = FileContent.CSVContent[i, 0];
                        string NextType = FileContent.CSVContent[i, 1];
                        string NextManufacturer = FileContent.CSVContent[i, 2];
                        string NextLot = FileContent.CSVContent[i, 3];
                        string NextVaxSite = FileContent.CSVContent[i, 6];
                        string NextVaxName = FileContent.CSVContent[i, 7];

                        // Skip if ID isn't legit
                        if (StaticMethods.Strings.TypeCheck.IsUInt32(NextId) == false)
                        {
                            ThisApp.Methods.Logging.WriteMsg("ID on line index [ " + i.ToString() + " ] isn't numeric, skipping...", ThisApp.Methods.Logging.MsgTypeOptions.Warning);
                            continue;
                        }

                        // Clean up vax name for formatting
                        if (NextVaxName == null) NextVaxName = string.Empty;
                        NextVaxName = NextVaxName.ToUpper();

                        // Lots of vax names have brand in them - SomeVax (BrandX) - strip that out as there's also a manufacturer field, seems unneeded
                        if (NextVaxName == null) NextVaxName = string.Empty;
                        NextVaxName = NextVaxName.Split(new[] { "(" }, StringSplitOptions.None)[0];
                        NextVaxName = NextVaxName.Trim().ToUpper();
                        
                        // Once good, save record
                        var NextRecord = new Record
                        {
                            ID = Convert.ToUInt32(NextId),
                            Type = NextType,
                            VaxMfg = NextManufacturer,
                            Lot = NextLot,
                            VaxSite = NextVaxSite,
                            VaxName = NextVaxName
                        };

                        // Save the record
                        Records.Add(NextRecord);
                    }

                    // Return true if processed
                    ThisApp.Methods.Logging.WriteMsg("Records imported successfully.", ThisApp.Methods.Logging.MsgTypeOptions.Info);
                    return true;
                }
                catch (Exception ex)
                {
                    // Return error if we can't figure it out
                    ThisApp.Methods.Logging.WriteMsgWithException("Exception while attempting to load data!", ex);
                    return false;
                }
            }

            // Implement method to convert to sql insert cmds
            public List<string> ToSqlInsertCmds(List<uint> IDsToIgnore)
            {
                // Ensure list isn't null ever
                if (IDsToIgnore == null) IDsToIgnore = new List<uint>();

                // Create a copy of records with IDs to ignore omitted
                var CopyOfRecords = (from AllInfo in Records where IDsToIgnore.Contains(AllInfo.ID) == false select AllInfo).ToList();

                // Create list to return
                var ListOfSql = new List<string>(CopyOfRecords.Count + 1);

                // Loop through and create SQL for IDs in range
                foreach (var RECORD in CopyOfRecords)
                {
                    // If it's in range, add sql insert cmd
                    ListOfSql.Add("INSERT INTO " + DatabaseManager.Database.Table.VaxInfo + " (ID, Type, VaxMfg, Lot, VaxSite, VaxName) VALUES (" + RECORD.ID + ", '" + RECORD.CleanType + "', '" + RECORD.CleanVaxMfg + "', '" + RECORD.CleanLot + "', '" + RECORD.CleanVaxSite + "' ,'" + RECORD.CleanVaxName + "')");
                }

                // Return list after built
                return ListOfSql;
            }

            // Override ToString() for ease of reading
            public override string ToString()
            {
                return StaticMethods.Strings.XML.Serialize(this);
            }
        }


        #endregion

        #region Declarations


        // Holds callback to return status msgs
        private Action<string> InputCallbackReportStatus = null;


        #endregion

        #region Load & Unload Methods


        public FileImportManager(Action<string> MethodToReportStatusMsgsTo)
        {
            // Save inputs
            InputCallbackReportStatus = MethodToReportStatusMsgsTo;
        }


        #endregion

        #region Exposed Methods


        public System.Threading.Tasks.Task<bool> LoadFolderAsync(string FolderPath)
        {
            return System.Threading.Tasks.Task.Run(() => LoadFolder(FolderPath));
        }
        public bool LoadFolder(string FolderPath)
        {
            try
            {
                // Report main status to caller
                ReportStatusMsg("Checking folder [ " + FolderPath + " ] for records new VAERS records...");

                // Report debug
                ThisApp.Methods.Logging.WriteMsg("Begin folder import of [ " + FolderPath + " ] for raw VAERS data...", ThisApp.Methods.Logging.MsgTypeOptions.Info);

                // Report error if path is missing
                if (string.IsNullOrEmpty(FolderPath) == true) throw new Exception("Input folder path cannot be null!");
                if (System.IO.Directory.Exists(FolderPath) == false) throw new Exception("Folder [ " + FolderPath + " ] does not exist!");

                // Get all files in folder
                ThisApp.Methods.Logging.WriteMsg("Getting all CSV files in folder...", ThisApp.Methods.Logging.MsgTypeOptions.Info);
                var AllFiles = System.IO.Directory.GetFiles(FolderPath, "*.CSV").ToList();

                // Report error if there are no files
                if (AllFiles == null || AllFiles.Count == 0) throw new Exception("No files were found in the search folder!");

                // If we have files, report status
                ThisApp.Methods.Logging.WriteMsg("Found the following files in sweep folder...", ThisApp.Methods.Logging.MsgTypeOptions.Info);
                ThisApp.Methods.Logging.WriteMsg(string.Join("\r\n", AllFiles), ThisApp.Methods.Logging.MsgTypeOptions.AsIs);
                ReportStatusMsg("Found [ " + AllFiles.Count + " ] files in [ " + FolderPath + " ], processing...");

                // Create list to hold all processed files
                var AllProcessedFiles = new List<DataFile>();

                // Loop through and process all files
                foreach (var FILEPATH in AllFiles)
                {
                    // Save the file name and number for prompting
                    string FileName = System.IO.Path.GetFileName(FILEPATH);
                    int FileNumber = AllFiles.IndexOf(FILEPATH) + 1;

                    // Create string to hold status msg
                    string StatusMsg = "Processing file [ " + FileName + " ] | [ " + FileNumber + " of " + AllFiles.Count + " ]...";

                    // Report status
                    ThisApp.Methods.Logging.WriteMsg(StatusMsg, ThisApp.Methods.Logging.MsgTypeOptions.Info);
                    ReportStatusMsg(StatusMsg);

                    // Create var to attempt load (we don't know file type)
                    DataFile LoadedFileData = new TableMain();

                    // Attempt load and add to list if it works
                    if (LoadedFileData.LoadFromFile(FILEPATH) == true)
                    {
                        ThisApp.Methods.Logging.WriteMsg("File was parsed as MainTable.", ThisApp.Methods.Logging.MsgTypeOptions.Info);
                        ReportStatusMsg("File was parsed as main table data.");
                        AllProcessedFiles.Add(LoadedFileData);
                        continue;
                    }

                    // If it didn't work, change type
                    LoadedFileData = new TableSymptom();

                    // Try loading again
                    if (LoadedFileData.LoadFromFile(FILEPATH) == true)
                    {
                        ThisApp.Methods.Logging.WriteMsg("File was parsed as SymptomTable.", ThisApp.Methods.Logging.MsgTypeOptions.Info);
                        ReportStatusMsg("File was parsed as symptom table data.");
                        AllProcessedFiles.Add(LoadedFileData);
                        continue;
                    }

                    // If that doesn't work, try last type
                    LoadedFileData = new TableVaxInfo();

                    // Attempt load again
                    if (LoadedFileData.LoadFromFile(FILEPATH) == true)
                    {
                        ThisApp.Methods.Logging.WriteMsg("File was parsed as VaxInfoTable.", ThisApp.Methods.Logging.MsgTypeOptions.Info);
                        ReportStatusMsg("File was parsed as vax info data.");
                        AllProcessedFiles.Add(LoadedFileData);
                        continue;
                    }

                    // If none worked, just log and keep going
                    ThisApp.Methods.Logging.WriteMsg("The file [ " + FileName + " ] could not be parsed into any known data type!", ThisApp.Methods.Logging.MsgTypeOptions.Warning);
                    ReportStatusMsg("File [ " + FileName + " ] could not be parsed, skipping...");
                }

                // Report error if we couldn't parse any files
                if (AllProcessedFiles.Count == 0) throw new Exception("No files found in the search directory could be parsed into valid VAERS table data!");

                // Report status
                ThisApp.Methods.Logging.WriteMsg("Requesting IDs already processed...", ThisApp.Methods.Logging.MsgTypeOptions.Info);
                ReportStatusMsg("Checking for IDs already processed...");

                // Get the already processed IDs
                var IdsAlreadyHandled = DatabaseManager.GetExistingEntryIds();
                if (IdsAlreadyHandled == null)
                {
                    // Report error if we can't check
                    throw new Exception("Unable to get existing entry IDs, cannot continue!");
                }

                // If we have data, report status
                ThisApp.Methods.Logging.WriteMsg("Creating SQL commands to perform inserts...", ThisApp.Methods.Logging.MsgTypeOptions.Info);
                ReportStatusMsg("Building SQL cmds...");

                // Create list to hold all sql cmds
                var SqlCmds = new List<string>();

                // Loop through and get all sql cmds
                foreach (var DATAFILE in AllProcessedFiles)
                {
                    ReportStatusMsg("Building SQL insert cmds for file [ " + (AllProcessedFiles.IndexOf(DATAFILE) + 1).ToString() + " of " + AllProcessedFiles.Count + " ]...");
                    SqlCmds.AddRange(DATAFILE.ToSqlInsertCmds(IdsAlreadyHandled));
                }

                // Report how many inserts we're doing
                ThisApp.Methods.Logging.WriteMsg("Created [ " + SqlCmds.Count + " ] SQL commands to perform inserts...", ThisApp.Methods.Logging.MsgTypeOptions.Info);
                ReportStatusMsg("Built [ " + SqlCmds.Count + " ] SQL commands to add new data...");
                
                // Report status
                ThisApp.Methods.Logging.WriteMsg("Attempting SQL insert xaction...", ThisApp.Methods.Logging.MsgTypeOptions.Info);
                ReportStatusMsg("Attempting SQL insert xaction...");

                // Report error if SQL update fails
                if (StaticMethods.Database.SQLite.ExecuteSQLXactionCmds(DatabaseManager.Database.FilePath, string.Empty, SqlCmds) == false)
                {
                    throw new Exception("Unable to perform SQL xaction insert!");
                }

                // Return true if we're good
                ThisApp.Methods.Logging.WriteMsg("All SQL inserts processed successfully.", ThisApp.Methods.Logging.MsgTypeOptions.Info);
                ReportStatusMsg("All SQL commands executed successfully.");
                return true;
            }
            catch (Exception ex)
            {
                // Just log errors
                ThisApp.Methods.Logging.WriteMsgWithException("Error occurred while attempting to load folder for file import!", ex);
                ReportStatusMsg("Error while attempting to load folder!");
                ReportStatusMsg(ex.Message.ToUpper());
                return false;
            }
        }


        #endregion

        #region Helper Methods


        // Helper method to compare top row of array against expected headers
        private static bool DoHeadersMatch(string[,] FullArray, List<string> HeadersToCheckAgainst)
        {
            try
            {
                // Create list to hold the full header row of the array
                var HeaderRowOfArray = new List<string>();

                // Loop through and add
                for (int i = 0; i <= FullArray.GetUpperBound(1); i++)
                {
                    // Only add data that isn't empty
                    if (string.IsNullOrEmpty(FullArray[0, i]) == false)
                    {
                        HeaderRowOfArray.Add(FullArray[0, i]);
                    }
                }

                // Compare and return
                return StaticMethods.Enumerables.AreEqual<string>(HeaderRowOfArray, HeadersToCheckAgainst, false);
            }
            catch (Exception)
            {
                // Return false if error occurs
                return false;
            }
        }

        // Helper to escape SQL before inserts
        private static string CleanSql(string StringToClean)
        {
            return StaticMethods.Database.CleanSqlString(StringToClean);
        }

        // Helper to report status to callback method
        private void ReportStatusMsg(string Msg)
        {
            try
            {
                // Bail if there's no callback
                if (InputCallbackReportStatus == null) return;

                // If we have a callback, hit it
                InputCallbackReportStatus(Msg);
            }
            catch (Exception ex)
            {
                // If error occurs, just log and bail
                ThisApp.Methods.Logging.WriteMsgWithException("Error occurred while hitting callback to report status in " + nameof(FileImportManager) + "!", ex);
            }
        }


        #endregion

    }

}
