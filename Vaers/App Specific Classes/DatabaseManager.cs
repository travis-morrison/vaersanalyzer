﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vaers
{

    public class DatabaseManager
    {

        #region Static Methods


        public static System.Threading.Tasks.Task<BasicStats> RequestBasicStatsAsync()
        {
            return System.Threading.Tasks.Task.Run(() => RequestBasicStats());
        }
        public static BasicStats RequestBasicStats()
        {
            // Log debug
            ThisApp.Methods.Logging.WriteMsg("Attempting to get basic database stats...", ThisApp.Methods.Logging.MsgTypeOptions.Info);

            // Create SQL to get min and max
            string GetMinId = "SELECT MIN(ID) FROM " + Database.Table.Main;
            string GetMaxId = "SELECT MAX(ID) FROM " + Database.Table.Main;

            // Create SQL to get min and max dates
            string GetMinDate = "SELECT MIN(DateVaxxed) FROM " + Database.Table.Main;
            string GetMaxDate = "SELECT MAX(DateVaxxed) FROM " + Database.Table.Main;

            // Create SQL cmd to get full count
            string GetRecordCount = "SELECT COUNT(ID) FROM " + Database.Table.Main;

            // Add all cmds to list to do them at once
            var AllCmds = new[] { GetMinId, GetMaxId, GetMinDate, GetMaxDate, GetRecordCount }.ToList();

            // Create var to hold returned result
            var ReturnedValues = new List<object>();

            // Return null if we can't get values
            if (StaticMethods.Database.SQLite.ExecuteMultipleSQLScalarCmds(Database.FilePath, string.Empty, AllCmds, ref ReturnedValues) == false)
            {
                ThisApp.Methods.Logging.WriteMsg("Unable to execute scalar cmds to get basic database stats!", ThisApp.Methods.Logging.MsgTypeOptions.Error);
                return null;
            }

            try
            {
                // If we got any nulls back, there's no data
                if (ReturnedValues.Contains(System.DBNull.Value) == true) return new BasicStats();

                // If we get data back, attempt to save info
                var StatsToReturn = new BasicStats
                {
                    MinId = Convert.ToUInt32(ReturnedValues[0]),
                    MaxId = Convert.ToUInt32(ReturnedValues[1]),
                    MinVaxDate = Convert.ToString(ReturnedValues[2]),
                    MaxVaxDate = Convert.ToString(ReturnedValues[3]),
                    RecordCount = Convert.ToUInt32(ReturnedValues[4])
                };

                // Log info and return
                ThisApp.Methods.Logging.WriteMsg(StatsToReturn.ToString(), ThisApp.Methods.Logging.MsgTypeOptions.AsIs);
                return StatsToReturn;
            }
            catch (Exception ex)
            {
                // Log and return null if any values are bad
                ThisApp.Methods.Logging.WriteMsgWithException("Unable to convert scalar results back to basic stats!", ex);
                return null;
            }

        }

        public static System.Threading.Tasks.Task<List<uint>> GetExistingEntryIdsAsync()
        {
            return System.Threading.Tasks.Task.Run(() => GetExistingEntryIds());
        }
        public static List<uint> GetExistingEntryIds()
        {
            try
            {
                // Create SQL to get all unique entry IDs
                string SqlGetEntryIds = "SELECT DISTINCT ID FROM " + Database.Table.Main;

                // Create list hold info
                List<object> ReturnedUniqueIds = null;

                // Attempt query
                if (StaticMethods.Database.SQLite.GetSearchResultsAsList(Database.FilePath, string.Empty, SqlGetEntryIds, ref ReturnedUniqueIds, false) == false)
                {
                    throw new Exception("Unable to query unique IDs!");
                }

                // Return empty if there are none
                if (ReturnedUniqueIds == null) return new List<uint>();

                // If we get data back, convert and return it
                return (from AllInfo in ReturnedUniqueIds select Convert.ToUInt32(AllInfo)).ToList();
            }
            catch (Exception ex)
            {
                // Return nothing if error occurs
                ThisApp.Methods.Logging.WriteMsgWithException("Unable to query existing entry IDs!", ex);
                return null;
            }
        }

        public static System.Threading.Tasks.Task<bool> CompactDatabaseAsync()
        {
            return System.Threading.Tasks.Task.Run(() => CompactDatabase());
        }
        public static bool CompactDatabase()
        {
            return StaticMethods.Database.SQLite.CompactDatabase(Database.FilePath, string.Empty);
        }

        public static System.Threading.Tasks.Task<bool> ResetDatabaseAsync()
        {
            return System.Threading.Tasks.Task.Run(() => ResetDatabase());
        }
        public static bool ResetDatabase()
        {
            try
            {
                // Log debug
                ThisApp.Methods.Logging.WriteMsg("Attempting to write empty database...", ThisApp.Methods.Logging.MsgTypeOptions.Info);

                // Attempt to write new empty database
                var EmptyDbBytes = Vaers.Properties.Resources.EmptyDB;
                System.IO.File.WriteAllBytes(Database.FilePath, EmptyDbBytes);

                // Return true if it works
                ThisApp.Methods.Logging.WriteMsg("Empty database restored.", ThisApp.Methods.Logging.MsgTypeOptions.Info);
                return true;
            }
            catch (Exception ex)
            {
                // Return false if error occurs
                ThisApp.Methods.Logging.WriteMsgWithException("Error resetting database!", ex);
                return false;
            }
        }


        #endregion

        #region Sub Classes


        // Sub class to hold basic database statistics / info
        public class BasicStats
        {
            // Vars to hold info
            public uint MinId = 0;
            public uint MaxId = 0;
            public string MinVaxDate = string.Empty;
            public string MaxVaxDate = string.Empty;
            public uint RecordCount = 0;

            // Override ToString() for ease of reading
            public override string ToString()
            {
                return StaticMethods.Strings.XML.Serialize(this);
            }
        }

        // Sub class to hold database info
        public class Database
        {
            // All table names
            public class Table
            {
                public const string Main = "tblMain";
                public const string Symptoms = "tblSymptoms";
                public const string VaxInfo = "tblVaxInfo";
            }

            // Read only to return the expected file path of the database
            public static string FilePath
            {
                get
                {
                    return System.IO.Path.Combine(ThisApp.Folders.AppRoot, "Vaers.db");
                }
            }
        }

        // Sub class to define an individual joined vaers record
        public class VaersRecord
        {
            // Vars to hold info from main table
            public uint ID { get; set; } = 0;
            public string DateReceived { get; set; } = string.Empty;
            public double AgeOfPatient { get; set; } = 0;
            public string Sex { get; set; } = string.Empty;
            public string IsDead { get; set; } = string.Empty;
            public string IsRecovered { get; set; } = string.Empty;
            public uint NumOfDays { get; set; } = 0;
            public string OtherMeds { get; set; } = string.Empty;
            public string OtherIllness { get; set; } = string.Empty;
            public string History { get; set; } = string.Empty;
            public string Allergies { get; set; } = string.Empty;
            public string Description { get; set; } = string.Empty;

            // Vars to hold joined info from vax table
            public string Type { get; set; } = string.Empty;
            public string VaxMfg { get; set; } = string.Empty;
            public string Lot { get; set; } = string.Empty;
            public string VaxSite { get; set; } = string.Empty;
            public string VaxName { get; set; } = string.Empty;

            // Joint symptoms for displaying in cell in grid
            public string SymptomsMerged
            {
                get
                {
                    return string.Join(" | ", Symptoms);
                }
            }

            // Vars to hold joined info from symptoms table (NOT visible in grids)
            public List<string> Symptoms = new List<string>();

            // Override ToString() for ease of reading
            public override string ToString()
            {
                return StaticMethods.Strings.XML.Serialize(this);
            }
        }

        // Sub class to hold top level vaccine report
        public class VaccineEventReport
        {
            // Sub struc to hold counts for a specified vaccine
            public class VaxEventCount
            {
                // Vars to hold info
                public string TimePeriod = string.Empty;
                public List<Utilities.SerializableKeyValuePair<string, uint>> VaxNameToEventCount = new List<Utilities.SerializableKeyValuePair<string, uint>>();

                // Read only to return sorted vax names and counts
                public List<KeyValuePair<string, uint>> SortedVaxNameToEventCount
                {
                    get
                    {
                        // Get all data sorted
                        var Sorted = (from AllInfo in VaxNameToEventCount orderby AllInfo.Value descending select AllInfo).ToList();

                        // Create the list to return
                        var ListToReturn = new List<KeyValuePair<string, uint>>();
                        foreach (var PAIR in Sorted)
                        {
                            ListToReturn.Add(new KeyValuePair<string, uint>(PAIR.Key, PAIR.Value));
                        }

                        // Return the list
                        return ListToReturn;
                    }
                }

                // Method to dump vax names and events to markdown
                public string VaxNameToEventCountAsMarkdown()
                {
                    // Create var to hold data
                    string[,] ArrData = new string[VaxNameToEventCount.Count + 1, 2];

                    // Add header row
                    ArrData[0, 0] = "Vaccine";
                    ArrData[0, 1] = "Count";

                    // Pointer for current pair
                    int CurrentIndex = 1;

                    // Fill array
                    foreach (var PAIR in SortedVaxNameToEventCount)
                    {
                        ArrData[CurrentIndex, 0] = PAIR.Key;
                        ArrData[CurrentIndex, 1] = ThisApp.Methods.Common.AddCommasToNumber(PAIR.Value, 0);
                        CurrentIndex += 1;
                    }

                    // Create and return markdown
                    return StaticMethods.Strings.Generate.MarkdownTextTable(ArrData);
                }
            }

            // The date range
            public string StartDate = string.Empty;
            public string StopDate = string.Empty;

            // The total events on file
            public uint EventCount = 0;

            // Unique vaccine names
            public List<string> Vaccines = new List<string>();

            // Cumulative, monthly, & yearly summaries
            public VaxEventCount VaxEventsCumulative = new VaxEventCount();
            public List<VaxEventCount> VaxEventsPerYear = new List<VaxEventCount>();
            public List<VaxEventCount> VaxEventsPerMonth = new List<VaxEventCount>();

            // Cumulative list to hold vax name to avg monthly event count
            [System.Xml.Serialization.XmlIgnore]
            public List<KeyValuePair<string, double>> VaxNameToAvgMonthlyEventCount = new List<KeyValuePair<string, double>>();

            // Cumulative list of symptoms by vaccine
            [System.Xml.Serialization.XmlIgnore]
            public Dictionary<string, Dictionary<string, uint>> VaxNameToSymptomCountMap = new Dictionary<string, Dictionary<string, uint>>();
            public string VaxNameToSymptomCountMapAsMarkdown
            {
                get
                {
                    // Create var to hold how many rows we need
                    int RowsNeeded = 0;

                    // Loop through and count
                    foreach (var PAIR in VaxNameToSymptomCountMap)
                    {
                        foreach (var INNERPAIR in PAIR.Value)
                        {
                            RowsNeeded += 1;
                        }
                    }

                    // Add an extra row for the headers
                    RowsNeeded += 1;

                    // Create the array
                    string[,] ArrData = new string[RowsNeeded, 4];

                    // Add headers
                    ArrData[0, 0] = "Vaccine";
                    ArrData[0, 1] = "Symptom";
                    ArrData[0, 2] = "Count";
                    ArrData[0, 3] = "Vax Total";

                    // Save the current index / pointer
                    int NextLocation = 1;

                    // Loop through and fill
                    foreach (var PAIR in VaxNameToSymptomCountMap)
                    {
                        // Save the inner dictionary so we can count
                        var InnerListOfSymptomsAndCounts = PAIR.Value.Values.ToList();
                        int VaxTotal = (from AllInfo in InnerListOfSymptomsAndCounts select Convert.ToInt32(AllInfo)).ToList().Sum();

                        foreach (var INNERPAIR in PAIR.Value)
                        {
                            ArrData[NextLocation, 0] = PAIR.Key;
                            ArrData[NextLocation, 1] = INNERPAIR.Key;
                            ArrData[NextLocation, 2] = ThisApp.Methods.Common.AddCommasToNumber(INNERPAIR.Value, 0);
                            ArrData[NextLocation, 3] = VaxTotal.ToString();
                            NextLocation += 1;
                        }
                    }

                    // Join and return when done
                    return StaticMethods.Strings.Generate.MarkdownTextTable(ArrData);
                }
            }

            // Var to hold input callback method
            private Action<string> InputCallbackReportStatus = null;

            // Internal constructor for xml serialization
            private VaccineEventReport()
            {
                // Empty for XML serialize
            }

            // Build report based on input records
            public VaccineEventReport(List<VaersRecord> Records, Action<string> MethodToReportStatusTo)
            {
                // Save inputs
                InputCallbackReportStatus = MethodToReportStatusTo;

                // Report status
                ThisApp.Methods.Logging.WriteMsg("Creating vaccine report...", ThisApp.Methods.Logging.MsgTypeOptions.Info);
                ReportStatusMsg("Creating full vaccine report...");

                // Report error if missing info
                if (Records == null) throw new Exception("Must specify records to build report!");

                // Report status
                ThisApp.Methods.Logging.WriteMsg("Getting basic info...", ThisApp.Methods.Logging.MsgTypeOptions.Info);
                ReportStatusMsg("Computing basic stats...");

                // If we have records, get the min and max date
                StartDate = (from AllInfo in Records select AllInfo.DateReceived).Min();
                StopDate = (from AllInfo in Records select AllInfo.DateReceived).Max();

                // Save the total count
                EventCount = Convert.ToUInt32(Records.Count);

                // Report status
                ThisApp.Methods.Logging.WriteMsg("Computing cumulative breakdown...", ThisApp.Methods.Logging.MsgTypeOptions.Info);
                ReportStatusMsg("Building cumulative breakdown...");

                // Get all the unique vaccine names and sort
                Vaccines.AddRange((from AllInfo in Records select AllInfo.VaxName).Distinct());
                Vaccines.Sort();

                // Set main info for cumulative counts
                VaxEventsCumulative.TimePeriod = StartDate + " to " + StopDate;

                // Create dictionary to hold the average per month counts for each vax
                var VaxToAvgPerMonth = new Dictionary<string, double>();

                // Loop through and get counts
                foreach (var VAX in Vaccines)
                {
                    // Get the count for the current vax name
                    int EventCount = (from AllInfo in Records where AllInfo.VaxName == VAX select AllInfo).Count();

                    // Get the earliest date we have on file so we know when the vax was rolled out
                    string EarliestDate = (from AllInfo in Records where AllInfo.VaxName == VAX select AllInfo.DateReceived).Min();

                    // Add modified name so we can see when vax kicked in
                    VaxEventsCumulative.VaxNameToEventCount.Add(new Utilities.SerializableKeyValuePair<string, uint>(VAX + " (" + EarliestDate + ")", Convert.ToUInt32(EventCount)));

                    // Rail the earliest data so we can compute total months vax was available in range
                    DateTime RailedEarliestDate = StaticMethods.Dates.Convert.SortableStringToDate(EarliestDate).Value;
                    RailedEarliestDate = StaticMethods.Dates.Transform.RailToBeginningOfMonth(RailedEarliestDate);

                    // Compute the total months this vax was available within the selected time frame
                    double TotalMonthsAvailable = (StaticMethods.Dates.Convert.SortableStringToDate(StopDate).Value - RailedEarliestDate).TotalDays / 30.0;
                    TotalMonthsAvailable = Math.Ceiling(TotalMonthsAvailable);

                    // Compute the average event count per month and add to dictionary
                    VaxToAvgPerMonth.Add(VAX, Math.Round(EventCount / TotalMonthsAvailable, 0, MidpointRounding.AwayFromZero));
                }

                // Once we've computed the dictionary of avg events per month, save back to local var
                VaxNameToAvgMonthlyEventCount = (from AllInfo in VaxToAvgPerMonth orderby AllInfo.Value descending select AllInfo).ToList();

                // Report status
                ThisApp.Methods.Logging.WriteMsg("Getting yearly breakdowns...", ThisApp.Methods.Logging.MsgTypeOptions.Info);
                ReportStatusMsg("Building yearly breakdowns...");

                // Save the real start and end dates for ease of parsing
                var DateStart = StaticMethods.Strings.Convert.SortableStringToDate(StartDate).Value;
                var DateStop = StaticMethods.Strings.Convert.SortableStringToDate(StopDate).Value;

                // Create var to hold the next date to make abbreviation for year / year-mm
                DateTime NextDate = DateStart;

                // Create list to hold all YYYY-MM blocks in range
                var YYYYBlocksInRange = new List<string>();

                // Create the blocks
                while (NextDate <= DateStop)
                {
                    // Save the next block (just use YYYY)
                    string NextBlock = NextDate.Year.ToString();

                    // Add the next block and inc to next month
                    YYYYBlocksInRange.Add(NextBlock);
                    NextDate = NextDate.AddYears(1);
                }

                // Once we have all blocks, go through and get counts for each
                foreach (var YYYY in YYYYBlocksInRange)
                {
                    // Create the next object to hold counts
                    var NextSummary = new VaxEventCount { TimePeriod = YYYY };

                    // Get all vax names in this range
                    var AllRecordsInRange = (from AllInfo in Records where AllInfo.DateReceived.StartsWith(YYYY) select AllInfo).ToList();

                    // Save the vax names in range
                    var VaxNamesInRange = (from AllInfo in AllRecordsInRange select AllInfo.VaxName).Distinct().ToList();
                    VaxNamesInRange.Sort();

                    // Loop through and add counts
                    foreach (var VAX in VaxNamesInRange)
                    {
                        // Get the count for this vax name in the range
                        int CountInRange = (from AllInfo in AllRecordsInRange where AllInfo.VaxName == VAX select AllInfo).Count();
                        NextSummary.VaxNameToEventCount.Add(new Utilities.SerializableKeyValuePair<string, uint>(VAX, Convert.ToUInt32(CountInRange)));
                    }

                    // Finally, add record to list
                    VaxEventsPerYear.Add(NextSummary);
                }

                // Report status
                ThisApp.Methods.Logging.WriteMsg("Getting monthly breakdowns...", ThisApp.Methods.Logging.MsgTypeOptions.Info);
                ReportStatusMsg("Building monthly breakdowns...");

                // Re-init var for year & month blocks
                NextDate = DateStart;

                // Create list to hold all YYYY-MM blocks in range
                var YYYYMMBlocksInRange = new List<string>();

                // Create the blocks
                while (NextDate <= DateStop)
                {
                    // Save the next block (just use YYYY-MM)
                    string NextBlock = StaticMethods.Strings.Convert.DateToSortableString(NextDate, false);
                    NextBlock = NextBlock.Substring(0, 7);

                    // Add the next block and inc to next month
                    YYYYMMBlocksInRange.Add(NextBlock);
                    NextDate = NextDate.AddMonths(1);
                }

                // Once we have all blocks, go through and get counts for each
                foreach (var YYYYMM in YYYYMMBlocksInRange)
                {
                    // Create the next object to hold counts
                    var NextSummary = new VaxEventCount { TimePeriod = YYYYMM };

                    // Get all vax names in this range
                    var AllRecordsInRange = (from AllInfo in Records where AllInfo.DateReceived.StartsWith(YYYYMM) select AllInfo).ToList();

                    // Save the vax names in range
                    var VaxNamesInRange = (from AllInfo in AllRecordsInRange select AllInfo.VaxName).Distinct().ToList();
                    VaxNamesInRange.Sort();

                    // Loop through and add counts
                    foreach (var VAX in VaxNamesInRange)
                    {
                        // Get the count for this vax name in the range
                        int CountInRange = (from AllInfo in AllRecordsInRange where AllInfo.VaxName == VAX select AllInfo).Count();
                        NextSummary.VaxNameToEventCount.Add(new Utilities.SerializableKeyValuePair<string, uint>(VAX, Convert.ToUInt32(CountInRange)));
                    }

                    // Finally, add record to list
                    VaxEventsPerMonth.Add(NextSummary);
                }

                // Report status
                ThisApp.Methods.Logging.WriteMsg("Counting symptoms for each vaccine...", ThisApp.Methods.Logging.MsgTypeOptions.Info);
                ReportStatusMsg("Tabulating symptoms by vaccine...");

                // Loop through and create symptom count map for each vaccine
                foreach (var VAX in Vaccines)
                {
                    // Save the status msg to show
                    string StatusMsg = "Processing vaccine [ " + VAX + " ], [ " + (Vaccines.IndexOf(VAX) + 1).ToString() + " of " + Vaccines.Count + " ]...";

                    // Report status
                    ThisApp.Methods.Logging.WriteMsg(StatusMsg, ThisApp.Methods.Logging.MsgTypeOptions.Info);
                    ReportStatusMsg(StatusMsg);

                    // Get all records for this vax
                    var AllRecordsForVax = (from AllInfo in Records where AllInfo.VaxName == VAX select AllInfo).ToList();

                    // Create list of list to hold all lists of symptoms for vax
                    var ListOfListOfSymptoms = (from AllInfo in AllRecordsForVax select AllInfo.Symptoms).ToList();

                    // Create master list to hold all unique symptoms for vax
                    var UniqueSymptomsForVax = new List<string>();

                    // Loop through and add all
                    foreach (var LIST in ListOfListOfSymptoms)
                    {
                        foreach (var SYMPTOM in LIST)
                        {
                            // Add unique items
                            if (UniqueSymptomsForVax.Contains(SYMPTOM) == false)
                            {
                                UniqueSymptomsForVax.Add(SYMPTOM);
                            }
                        }
                    }

                    // Create dictionary to hold counts for all symptoms FOR JUST THIS VAX
                    var SymptomNameAndCountForVax = new Dictionary<string, uint>();

                    // Create place holder for each unique symptom
                    foreach (var SYMPTOM in UniqueSymptomsForVax)
                    {
                        SymptomNameAndCountForVax.Add(SYMPTOM, 0);
                    }

                    // Create vars to hold current record number and total while looping
                    int TotalRecords = AllRecordsForVax.Count;
                    int CurrentRecord = 1;

                    // Loop through and do counts for all symptoms
                    foreach (var RECORD in AllRecordsForVax)
                    {
                        // Define block size fore reporting based on how many datapoints we have (otherwise UI slows stuff down)
                        int BlockInterval = 10;

                        // Make block size bigger depending on how many points we have
                        if (TotalRecords > 1000) BlockInterval = 100;
                        if (TotalRecords > 5000) BlockInterval = 250;
                        if (TotalRecords > 10000) BlockInterval = 1000;
                        if (TotalRecords > 50000) BlockInterval = 2500;
                        if (TotalRecords > 100000) BlockInterval = 5000;

                        // Compute the current block end
                        int BlockEnd = CurrentRecord + BlockInterval - 1;
                        if (CurrentRecord == 1) BlockEnd -= 1;
                        if (BlockEnd > TotalRecords) BlockEnd = TotalRecords;

                        // Only report a block at a time to speed things up
                        if (CurrentRecord == 1 || CurrentRecord % BlockInterval == 0)
                        {
                            ReportStatusMsg("> Summing symptoms for vaccine [ " + VAX + " ], processing records [ " + CurrentRecord + "-" + BlockEnd + " of " + TotalRecords + " ]...");
                        }

                        // Loop through and count each symptom
                        foreach (var SYMPTOM in RECORD.Symptoms)
                        {
                            SymptomNameAndCountForVax[SYMPTOM] += 1;
                        }

                        // Inc counter for next loop
                        CurrentRecord += 1;
                    }

                    // Sort the data for pareto
                    var SymptomNameAndCountForVaxAsList = (from AllInfo in SymptomNameAndCountForVax orderby AllInfo.Value descending select AllInfo).ToList();

                    // Once dictionary is built, add new item to master list
                    VaxNameToSymptomCountMap.Add(VAX, new Dictionary<string, uint>());

                    // Loop through and xfer data to public var
                    foreach (var PAIR in SymptomNameAndCountForVaxAsList)
                    {
                        VaxNameToSymptomCountMap[VAX].Add(PAIR.Key, PAIR.Value);
                    }

                    // Force cleanup each loop as we're building a lot of data
                    GC.Collect();
                }
            }

            // Override ToString() for ease of reading
            public override string ToString()
            {
                // Create list to hold msgs to aggregate
                var LinesToMerge = new List<string>();

                // Add header info
                LinesToMerge.Add(string.Empty);
                LinesToMerge.Add("----- VACCINE REPORT -----");
                LinesToMerge.Add("START DATE:  " + StartDate);
                LinesToMerge.Add("  END DATE:  " + StopDate);
                LinesToMerge.Add("  # EVENTS:  " + EventCount);
                LinesToMerge.Add("");

                // Add vaccines counted
                LinesToMerge.Add("VACCINES:");
                foreach (var VAX in Vaccines)
                {
                    LinesToMerge.Add("> " + VAX);
                }

                // Add divider
                LinesToMerge.Add("");

                // Add cumulative summary
                LinesToMerge.Add("CUMULATIVE SUMMARY");
                LinesToMerge.Add("PERIOD:  " + VaxEventsCumulative.TimePeriod);
                LinesToMerge.Add(VaxEventsCumulative.VaxNameToEventCountAsMarkdown());
                LinesToMerge.Add("");

                // Add yearly data
                LinesToMerge.Add("YEARLY SUMMARY");

                // Loop through and add all years
                foreach (var YEAR in VaxEventsPerYear)
                {
                    LinesToMerge.Add("YEAR:  " + YEAR.TimePeriod);
                    LinesToMerge.Add(YEAR.VaxNameToEventCountAsMarkdown());
                    LinesToMerge.Add(string.Empty);
                }

                // Add monthly data
                LinesToMerge.Add("");
                LinesToMerge.Add("MONTHLY SUMMARY");

                // Loop through and add all years
                foreach (var MONTH in VaxEventsPerMonth)
                {
                    LinesToMerge.Add("MONTH:  " + MONTH.TimePeriod);
                    LinesToMerge.Add(MONTH.VaxNameToEventCountAsMarkdown());
                    LinesToMerge.Add(string.Empty);
                }

                // Add header for symptom count
                LinesToMerge.Add("SYMPTOM SUMMARY");
                LinesToMerge.Add(VaxNameToSymptomCountMapAsMarkdown);
                LinesToMerge.Add(string.Empty);

                // Join and return all info
                return string.Join(Environment.NewLine, LinesToMerge);
            }

            // Helper to report status
            private void ReportStatusMsg(string Msg)
            {
                try
                {
                    // Bail if there's no callback
                    if (InputCallbackReportStatus == null) return;

                    // If we have a callback, hit it
                    InputCallbackReportStatus(Msg);
                }
                catch (Exception ex)
                {
                    // If error occurs, just log and bail
                    ThisApp.Methods.Logging.WriteMsgWithException("Error occurred while hitting callback to report status in " + nameof(VaccineEventReport) + "!", ex);
                }
            }
        }


        #endregion

        #region Declarations


        // Var to hold all loaded vaers records
        public List<VaersRecord> Records { internal set; get; } = new List<VaersRecord>();

        // Holds callback to return status msgs
        private Action<string> InputCallbackReportStatus = null;

        // Internal var only used to pre-load / quick-load all symptom data so it doesn't have to be re-fetched all the time
        private List<string> _PreLoadedSymptoms = null;

        // Block size for writing CSVs (how many rows)
        private const int CsvBlockSize = 2500;


        #endregion

        #region Load & Unload Methods


        public DatabaseManager(Action<string> MethodToReportStatusMsgsTo)
        {
            // Save inputs
            InputCallbackReportStatus = MethodToReportStatusMsgsTo;
        }


        #endregion

        #region Exposed Methods


        public System.Threading.Tasks.Task<bool> LoadAsync(uint MinYear, uint MaxYear)
        {
            return System.Threading.Tasks.Task.Run(() => Load(MinYear, MaxYear));
        }
        public bool Load(uint MinYear, uint MaxYear)
        {
            try
            {
                // Reset existing data
                Records = new List<VaersRecord>();

                // Report error if dates are invalid
                if (MinYear < 1000 || MaxYear > 9999) throw new Exception("Min/Max Year is not valid!");

                // Create vars to hold the search dates
                string StartDate = MinYear + "-01-01";
                string StopDate = MaxYear + "-12-31";

                // Report status
                string StatusMsg = "Attempting to load data between [ " + StartDate + " ] and [ " + StopDate + " ]...";
                ThisApp.Methods.Logging.WriteMsg(StatusMsg, ThisApp.Methods.Logging.MsgTypeOptions.Info);
                ReportStatusMsg(StatusMsg);

                // Create string to get all main entries from primary table joined with vax info
                string SqlGetMainData = "SELECT {1}.ID, {1}.DateVaxxed, {1}.AgeOfPatient, {1}.Sex, {1}.IsDead, {1}.IsRecovered, {1}.NumOfDays, {1}.OtherMeds, {1}.OtherIllness, {1}.History, {1}.Allergies, {1}.Description, {2}.Type, {2}.VaxMfg, {2}.Lot, {2}.VaxSite, {2}.VaxName FROM {1} LEFT JOIN {2} ON {1}.ID = {2}.ID WHERE {1}.DateVaxxed >= '" + StartDate + "' AND {1}.DateVaxxed <= '" + StopDate + "' AND {2}.VaxName <> 'VACCINE NOT SPECIFIED'";
                SqlGetMainData = SqlGetMainData.Replace("{1}", Database.Table.Main);
                SqlGetMainData = SqlGetMainData.Replace("{2}", Database.Table.VaxInfo);

                // Create var to hold results
                object[,] ReturnedMainResults = null;

                // Request the data
                if (StaticMethods.Database.SQLite.GetSearchResultsAsArray(Database.FilePath, string.Empty, SqlGetMainData, ref ReturnedMainResults) == false)
                {
                    throw new Exception("Unable to query / join main vaers data with vax info!");
                }

                // Report error if there is no data
                if (ReturnedMainResults == null)
                {
                    throw new Exception("No data retrieved! Ensure vaers data has been loaded in database!");
                }

                // If we got data, loop through and start building list
                for (int i = 0; i <= ReturnedMainResults.GetUpperBound(0); i++)
                {
                    // Create new object to hold info
                    var NextRecord = new VaersRecord
                    {
                        ID = Convert.ToUInt32(ReturnedMainResults[i, 0]),
                        DateReceived = Convert.ToString(ReturnedMainResults[i, 1]),
                        AgeOfPatient = Convert.ToUInt32(ReturnedMainResults[i, 2]),
                        Sex = Convert.ToString(ReturnedMainResults[i, 3]),
                        IsDead = Convert.ToString(ReturnedMainResults[i, 4]),
                        IsRecovered = Convert.ToString(ReturnedMainResults[i, 5]),
                        NumOfDays = Convert.ToUInt32(ReturnedMainResults[i, 6]),
                        OtherMeds = Convert.ToString(ReturnedMainResults[i, 7]),
                        OtherIllness = Convert.ToString(ReturnedMainResults[i, 8]),
                        History = Convert.ToString(ReturnedMainResults[i, 9]),
                        Allergies = Convert.ToString(ReturnedMainResults[i, 10]),
                        Description = Convert.ToString(ReturnedMainResults[i, 11]),
                        Type = Convert.ToString(ReturnedMainResults[i, 12]),
                        VaxMfg = Convert.ToString(ReturnedMainResults[i, 13]),
                        Lot = Convert.ToString(ReturnedMainResults[i, 14]),
                        VaxSite = Convert.ToString(ReturnedMainResults[i, 15]),
                        VaxName = Convert.ToString(ReturnedMainResults[i, 16])
                    };

                    // Add to list
                    Records.Add(NextRecord);
                }

                // Update status
                StatusMsg = "Attempting to query symptom data...";
                ThisApp.Methods.Logging.WriteMsg(StatusMsg, ThisApp.Methods.Logging.MsgTypeOptions.Info);
                ReportStatusMsg(StatusMsg);

                // Get all unique IDs that we need to grab
                var UniqueIds = (from AllInfo in Records select AllInfo.ID).ToList();
                UniqueIds = UniqueIds.Distinct().ToList();

                // Create sql to get all symptoms for records we have
                string SqlGetSymptoms = "SELECT ID, Symptom FROM " + Database.Table.Symptoms + " WHERE ID IN (" + string.Join(",", UniqueIds) + ") ORDER BY ID, Symptom";

                // Create var to hold symptom results
                object[,] ReturnedSymptomResults = null;

                // Attempt to get data
                if (StaticMethods.Database.SQLite.GetSearchResultsAsArray(Database.FilePath, string.Empty, SqlGetSymptoms, ref ReturnedSymptomResults) == false)
                {
                    throw new Exception("Unable to query symptom data to join back to main vaers data!");
                }

                // Just bail if there's no data to join
                if (ReturnedSymptomResults == null)
                {
                    ThisApp.Methods.Logging.WriteMsg("No data found for symptoms, returning.", ThisApp.Methods.Logging.MsgTypeOptions.Info);
                    ReportStatusMsg("No symptom data found, all other data loaded successfully.");
                    return true;
                }

                // Update status
                StatusMsg = "Attempting to join symptom data back to records...";
                ThisApp.Methods.Logging.WriteMsg(StatusMsg, ThisApp.Methods.Logging.MsgTypeOptions.Info);
                ReportStatusMsg(StatusMsg);

                // If we have data, create dictionary to make it a little easier to parse
                var IDToSymptomList = new Dictionary<uint, List<string>>();

                // If we have data, loop through and match up symptoms
                for (int i = 0; i <= ReturnedSymptomResults.GetUpperBound(0); i++)
                {
                    // Get the next ID and symptom
                    uint Id = Convert.ToUInt32(ReturnedSymptomResults[i, 0]);
                    string Symptom = Convert.ToString(ReturnedSymptomResults[i, 1]);

                    // Add main item to dictionary if not created for this ID yet
                    if (IDToSymptomList.ContainsKey(Id) == false)
                    {
                        IDToSymptomList.Add(Id, new List<string>());
                    }

                    // Add the next symptom
                    IDToSymptomList[Id].Add(Symptom);
                }

                // Once the dictionary is built, loop through all users and finish adding their symptoms
                foreach (var RECORD in Records)
                {
                    // Skip if we have no symptoms for this ID
                    if (IDToSymptomList.ContainsKey(RECORD.ID) == false) continue;

                    // If we have it, copy over symptoms
                    RECORD.Symptoms.AddRange(IDToSymptomList[RECORD.ID]);
                }

                // Return true when done
                ThisApp.Methods.Logging.WriteMsg("All data loaded successfully.", ThisApp.Methods.Logging.MsgTypeOptions.Info);
                ReportStatusMsg("All VAERS info has been successfully loaded for the specified date range.");
                return true;
            }
            catch (Exception ex)
            {
                // Log errors and return false
                ThisApp.Methods.Logging.WriteMsgWithException("Error occurred while attempting to load VAERS data!", ex);
                return false;
            }
            finally
            {
                // Report total records
                ThisApp.Methods.Logging.WriteMsg("Total records in memory [ " + Records.Count + " ].", ThisApp.Methods.Logging.MsgTypeOptions.Info);
                ReportStatusMsg("Records in memory = " + Records.Count + ".");
            }
        }

        public System.Threading.Tasks.Task<VaccineEventReport> ToVaccineEventReportAsync()
        {
            return System.Threading.Tasks.Task.Run(() => ToVaccineEventReport());
        }
        public VaccineEventReport ToVaccineEventReport()
        {
            return new VaccineEventReport(Records, InputCallbackReportStatus);
        }

        public System.Threading.Tasks.Task<List<string>> LookupVaxNamesAsync()
        {
            return System.Threading.Tasks.Task.Run(() => LookupVaxNames());
        }
        public List<string> LookupVaxNames()
        {
            // Get unique vax names, sort, and return
            var VaxNames = (from AllInfo in Records select AllInfo.VaxName).Distinct().ToList();
            VaxNames.Sort();
            return VaxNames;
        }

        public System.Threading.Tasks.Task<List<string>> LookupSymptomNamesAsync()
        {
            return System.Threading.Tasks.Task.Run(() => LookupSymptomNames());
        }
        public List<string> LookupSymptomNames()
        {
            // Use helper with full record list and return result
            return LookupSymptomNamesHelper(Records, true);
        }

        public System.Threading.Tasks.Task<List<VaersRecord>> LookupRecordsForVaxAsync(string VaccineName)
        {
            return System.Threading.Tasks.Task.Run(() => LookupRecordsForVax(VaccineName));
        }
        public List<VaersRecord> LookupRecordsForVax(string VaccineName)
        {
            // Get records that match
            return (from AllInfo in Records where AllInfo.VaxName == VaccineName orderby AllInfo.DateReceived select AllInfo).ToList();
        }

        public System.Threading.Tasks.Task<List<KeyValuePair<string, uint>>> LookupManufacturerEventCountsAsync(string VaccineToRestrictTo = "")
        {
            return System.Threading.Tasks.Task.Run(() => LookupManufacturerEventCounts(VaccineToRestrictTo: VaccineToRestrictTo));
        }
        public List<KeyValuePair<string, uint>> LookupManufacturerEventCounts(string VaccineToRestrictTo = "")
        {
            // Create list to hold records to use for analysis
            var RecordsToUse = new List<VaersRecord>();

            // Check if we're restricting to a vaccine by name
            if (string.IsNullOrEmpty(VaccineToRestrictTo) == false)
            {
                // If so, get matching records for that vaccine
                RecordsToUse.AddRange(LookupRecordsForVax(VaccineToRestrictTo));
            }
            else
            {
                // If not, use full list
                RecordsToUse.AddRange(Records);
            }

            // Once we have records, get all manufacturers (allow duplicates to count easier)
            var ManufacturerList = (from AllInfo in RecordsToUse select AllInfo.VaxMfg).ToList();

            // Create dictionary to hold counts
            var NamesAndCounts = new Dictionary<string, uint>();

            // Loop through and count
            foreach (var MANUFACTURER in ManufacturerList)
            {
                // Add key to dictionary if not there yet
                if (NamesAndCounts.ContainsKey(MANUFACTURER) == false)
                {
                    NamesAndCounts.Add(MANUFACTURER, 0);
                }

                // Once we know there's a key, inc position
                NamesAndCounts[MANUFACTURER] += 1;
            }

            // After tabulating, sort, convert to list, and return
            return (from AllInfo in NamesAndCounts orderby AllInfo.Value descending select AllInfo).ToList();
        }

        public System.Threading.Tasks.Task<List<string>> LookupManufacturersAsync(string VaccineToRestrictTo = "")
        {
            return System.Threading.Tasks.Task.Run(() => LookupManufacturers(VaccineToRestrictTo: VaccineToRestrictTo));
        }
        public List<string> LookupManufacturers(string VaccineToRestrictTo = "")
        {
            // Create list to hold records to use for analysis
            var RecordsToUse = new List<VaersRecord>();

            // Check if we're restricting to a vaccine by name
            if (string.IsNullOrEmpty(VaccineToRestrictTo) == false)
            {
                // If so, get matching records for that vaccine
                RecordsToUse.AddRange(LookupRecordsForVax(VaccineToRestrictTo));
            }
            else
            {
                // If not, use full list
                RecordsToUse.AddRange(Records);
            }

            // Once we have records, get all distinct manufacturers
            return (from AllInfo in RecordsToUse orderby AllInfo.VaxMfg select AllInfo.VaxMfg).Distinct().ToList();
        }

        public System.Threading.Tasks.Task<List<KeyValuePair<string, uint>>> LookupSymptomsAndCountsAsync(string VaccineToRestrictTo = "", string ManufacturerToRestrictTo = "")
        {
            return System.Threading.Tasks.Task.Run(() => LookupSymptomsAndCounts(VaccineToRestrictTo: VaccineToRestrictTo, ManufacturerToRestrictTo: ManufacturerToRestrictTo));
        }
        public List<KeyValuePair<string, uint>> LookupSymptomsAndCounts(string VaccineToRestrictTo = "", string ManufacturerToRestrictTo = "")
        {
            // Create list to hold records to use for analysis
            var RecordsToUse = new List<VaersRecord>();

            // Check if we're restricting to a vaccine by name
            if (string.IsNullOrEmpty(VaccineToRestrictTo) == false)
            {
                // If so, get matching records for that vaccine
                RecordsToUse.AddRange(LookupRecordsForVax(VaccineToRestrictTo));
            }
            else
            {
                // If not, use full list
                RecordsToUse.AddRange(Records);
            }

            // Filter to manufacturer if requested
            if (string.IsNullOrEmpty(ManufacturerToRestrictTo) == false)
            {
                RecordsToUse = (from AllInfo in RecordsToUse where AllInfo.VaxMfg == ManufacturerToRestrictTo select AllInfo).ToList();
            }

            // Get the distinct symptom names
            var DistinctSymptomNames = LookupSymptomNamesHelper(RecordsToUse, true);

            // Once we have the full list, create a dictionary to hold the full counts
            var SymptomNamesAndCounts = new Dictionary<string, uint>();

            // Go through and add place holder for all symptom
            foreach (var SYMPTOM in DistinctSymptomNames)
            {
                SymptomNamesAndCounts.Add(SYMPTOM, 0);
            }

            // Go through each record and count each symptom
            foreach (var RECORD in RecordsToUse)
            {
                // Loop through all symptoms in the record and inc the counter for that symptom
                foreach (var SYMPTOM in RECORD.Symptoms)
                {
                    SymptomNamesAndCounts[SYMPTOM] += 1;
                }
            }

            // Once we have the counts, convert, sort and return
            return (from AllInfo in SymptomNamesAndCounts orderby AllInfo.Value descending select AllInfo).ToList();
        }

        public System.Threading.Tasks.Task<List<KeyValuePair<string, uint>>> LookupBuzzwordsAsync(UInt16 MinCountToReturn, string VaccineToRestrictTo = "", string ManufacturerToRestrictTo = "")
        {
            return System.Threading.Tasks.Task.Run(() => LookupBuzzwords(MinCountToReturn, VaccineToRestrictTo: VaccineToRestrictTo, ManufacturerToRestrictTo: ManufacturerToRestrictTo));
        }
        public List<KeyValuePair<string, uint>> LookupBuzzwords(UInt16 MinCountToReturn, string VaccineToRestrictTo = "", string ManufacturerToRestrictTo = "")
        {
            // Create list to hold records to use for analysis
            var RecordsToUse = new List<VaersRecord>();

            // Check if we're restricting to a vaccine by name
            if (string.IsNullOrEmpty(VaccineToRestrictTo) == false)
            {
                // If so, get matching records for that vaccine
                RecordsToUse.AddRange(LookupRecordsForVax(VaccineToRestrictTo));
            }
            else
            {
                // If not, use full list
                RecordsToUse.AddRange(Records);
            }

            // Filter to manufacturer if requested
            if (string.IsNullOrEmpty(ManufacturerToRestrictTo) == false)
            {
                RecordsToUse = (from AllInfo in RecordsToUse where AllInfo.VaxMfg == ManufacturerToRestrictTo select AllInfo).ToList();
            }

            // Create dictionary to hold all IDs and descriptions (don't want to double count b/c we show multiple rows, one per vax)
            var IDsToDescriptions = new Dictionary<uint, string>();

            // Loop through and add
            foreach (var RECORD in RecordsToUse)
            {
                // Skip if already added
                if (IDsToDescriptions.ContainsKey(RECORD.ID) == true) continue;

                // If new, add description
                IDsToDescriptions.Add(RECORD.ID, RECORD.Description);
            }

            // Now that we have 1-per entry descriptions, save
            var AllDescriptions = IDsToDescriptions.Values.ToList();

            // Get all the symptom names
            var AllSymptomNames = LookupSymptomNamesHelper(RecordsToUse, false);

            // Join all descriptions and symptoms
            string JointDescriptions = string.Join(" ", AllDescriptions).ToUpper();
            string JointSymptoms = string.Join(" ", AllSymptomNames).ToUpper();

            // Join it all
            string JointAllWords = JointDescriptions + " " + JointSymptoms;

            // Remove everything but letters and numbers
            string CleanJointWords = JointAllWords;
            foreach (var PUNCTUATION in new[] { ".", ",", "?", "!", "\"", "(", ")", ":", ";", "%" })
            {
                CleanJointWords = CleanJointWords.Replace(PUNCTUATION, string.Empty);
            }

            // Split everything to get individual words
            var AllWordsAsList = CleanJointWords.Split(new[] { " ", "/" }, StringSplitOptions.RemoveEmptyEntries).ToList();

            // Finally, create dictionary to hold counts
            var WordsToCounts = new Dictionary<string, uint>();

            // Create list to hold common words we don't care about
            var CommonWordsToIgnore = new[] { "REPORTED", "REPORT", "RECEIVED", "PATIENT", "VACCINE", "WITH", "AFTER", "OTHER", "VERY", "SOME", "THERE", "THAT", "THIS", "FROM", "WERE", "FIRST", "AFTER", "HAVE", "SITE", "LIKE", "DATE", "SECOND", "BEEN", "THEN", "ALSO", "SINCE", "NEXT", "JUST", "HAVE", "HAVING", "COULD", "UNTIL", "WILL", "CANNOT", "BECAME", "KNOW", "BEING", "DID", "DIDN'T", "THROUGH", "EVERY", "THEIR", "DOES", "DOESN'T", "HOWEVER", "IT'S", "ITS", "FROM", "FORM", "THEM", "SURE" }.ToList();

            // Loop through and compute counts
            foreach (var WORD in AllWordsAsList)
            {
                // Remove short words like 'and', 'the', etc. 
                if (WORD.Length <= 3) continue;

                // Remove long words that are likely typos
                if (WORD.Length >= 25) continue;

                // Ignore if it's in the list of common words that are really meaningless filler
                if (CommonWordsToIgnore.Contains(WORD) == true) continue;

                // Ensure word is in dictionary
                if (WordsToCounts.ContainsKey(WORD) == false) WordsToCounts.Add(WORD, 0);

                // Once we know it's in there, inc the count
                WordsToCounts[WORD] += 1;
            }

            // Get sort all and remove any that don't meet the min count
            return (from AllInfo in WordsToCounts where AllInfo.Value >= MinCountToReturn orderby AllInfo.Value descending select AllInfo).ToList();
        }

        public int GetCsvChunkCount()
        {
            // The total number of chunks is going to be determined by block size (add 1 for header row)
            return (int)System.Math.Ceiling((double)Records.Count / (double)CsvBlockSize) + 1;
        }

        public System.Threading.Tasks.Task<string[,]> GetCsvChunkAsync(int ChunkIndex)
        {
            return System.Threading.Tasks.Task.Run(() => GetCsvChunk(ChunkIndex));
        }
        public string[,] GetCsvChunk(int ChunkIndex)
        {
            try
            {
                // Report status
                ThisApp.Methods.Logging.WriteMsg("Requesting CSV chunk [ " + ChunkIndex + " ]...", ThisApp.Methods.Logging.MsgTypeOptions.Info);

                // Get total chunks
                int ChunkCount = GetCsvChunkCount();
                if (ChunkIndex < 0 || ChunkIndex > ChunkCount - 1)
                {
                    // Report error for invalid chunk
                    throw new Exception("Invalid chunk index requested!");
                }

                // There are 17 fixed columns that are always there
                const UInt16 FixedColumns = 17;

                // Create list to hold all symptom names
                var AllSymptoms = new List<string>();

                // Check if we preloaded yet
                if (_PreLoadedSymptoms == null)
                {
                    // If not, load up pre-load data to save time later
                    _PreLoadedSymptoms = LookupSymptomNames();
                }

                // At this point we know we have all symptoms, copy them
                AllSymptoms.AddRange(_PreLoadedSymptoms);

                // Check if they requested chunk 0 (the header)
                if (ChunkIndex == 0)
                {
                    // If so, just return header array
                    ThisApp.Methods.Logging.WriteMsg("Returning CSV headers...", ThisApp.Methods.Logging.MsgTypeOptions.Info);
                    return BuildCsvHeaderArray(FixedColumns, AllSymptoms);
                }

                // Total columns will be fixed + however many symptoms there are
                int TotalColumns = FixedColumns + AllSymptoms.Count;

                // If we're going for an actual block, need to figure out the start and stop index
                int StartIndex = (ChunkIndex - 1) * CsvBlockSize;  // subtract 1 b/c 0 is header row
                int EndIndex = StartIndex + CsvBlockSize - 1;

                // If it's the last block, might be incomplete, can't go past max record
                if (EndIndex > Records.Count - 1) EndIndex = Records.Count - 1;

                // Now that we know what to extract, make the array to hold it
                var ArrayToReturn = new string[EndIndex - StartIndex + 1, TotalColumns];

                // Create var to hold where to insert the next item in the main array to
                int NextInsertIndexInArray = 0;

                // Loop through and grab the requested chunk
                for (int i = StartIndex; i <= EndIndex; i++)
                {
                    // Grab the record handle
                    var RECORD = Records[i];

                    // Fill in fixed info
                    ArrayToReturn[NextInsertIndexInArray, 0] = RECORD.ID.ToString();
                    ArrayToReturn[NextInsertIndexInArray, 1] = RECORD.DateReceived;
                    ArrayToReturn[NextInsertIndexInArray, 2] = RECORD.AgeOfPatient.ToString();
                    ArrayToReturn[NextInsertIndexInArray, 3] = RECORD.Sex;
                    ArrayToReturn[NextInsertIndexInArray, 4] = RECORD.IsDead;
                    ArrayToReturn[NextInsertIndexInArray, 5] = RECORD.IsRecovered;
                    ArrayToReturn[NextInsertIndexInArray, 6] = RECORD.NumOfDays.ToString();
                    ArrayToReturn[NextInsertIndexInArray, 7] = RECORD.OtherMeds;
                    ArrayToReturn[NextInsertIndexInArray, 8] = RECORD.OtherIllness;
                    ArrayToReturn[NextInsertIndexInArray, 9] = RECORD.History;
                    ArrayToReturn[NextInsertIndexInArray, 10] = RECORD.Allergies;
                    ArrayToReturn[NextInsertIndexInArray, 11] = RECORD.Description;
                    ArrayToReturn[NextInsertIndexInArray, 12] = RECORD.Type;
                    ArrayToReturn[NextInsertIndexInArray, 13] = RECORD.VaxMfg;
                    ArrayToReturn[NextInsertIndexInArray, 14] = RECORD.Lot;
                    ArrayToReturn[NextInsertIndexInArray, 15] = RECORD.VaxSite;
                    ArrayToReturn[NextInsertIndexInArray, 16] = RECORD.VaxName;

                    // Create array to check off which symptoms they have for this record
                    var SymptomsYesOrNo = new List<string>();

                    // Check each symptom
                    foreach (var SYMPTOM in AllSymptoms)
                    {
                        if (RECORD.Symptoms.Contains(SYMPTOM) == true)
                        {
                            SymptomsYesOrNo.Add("Y");
                        }
                        else
                        {
                            SymptomsYesOrNo.Add("N");
                        }
                    }

                    // Fill in all Yes/No vals
                    for (int j = 0; j < SymptomsYesOrNo.Count; j++)
                    {
                        ArrayToReturn[NextInsertIndexInArray, FixedColumns + j] = SymptomsYesOrNo[j];
                    }

                    // Inc the pointer for the next loop
                    NextInsertIndexInArray += 1;
                }

                // Report status
                ThisApp.Methods.Logging.WriteMsg("CSV chunk extracted successfully.", ThisApp.Methods.Logging.MsgTypeOptions.Info);

                // Return all the escaped arrays
                return EscapeArrayForCsv(ArrayToReturn);
            }
            catch (Exception ex)
            {
                ThisApp.Methods.Logging.WriteMsgWithException("Unable to retrieve chunk at specified index [ " + ChunkIndex + " ]!", ex);
                return null;
            }
        }


        #endregion

        #region Helper Methods


        // Helper to lookup unique symptom names for a set of records
        private List<string> LookupSymptomNamesHelper(List<VaersRecord> RecordsToSearch, bool DistinctOnly)
        {
            // Create dictionary to hold ID to list of symptom mapping to prevent double counting of entries that had more than one vax
            var IDsToSymptomMapping = new Dictionary<uint, List<string>>();

            // Loop through all records and get unique symptom lists
            foreach (var RECORD in RecordsToSearch)
            {
                // Skip if ID already handled
                if (IDsToSymptomMapping.ContainsKey(RECORD.ID) == true) continue;

                // If it's new, add it w/ list
                IDsToSymptomMapping.Add(RECORD.ID, RECORD.Symptoms);
            }

            // Get list of list of all symptoms for vax
            var ListOfListOfSymptoms = IDsToSymptomMapping.Values.ToList();

            // Merge into single list
            var MergedList = new List<string>(RecordsToSearch.Count);
            foreach (var LIST in ListOfListOfSymptoms)
            {
                foreach (var SYMPTOM in LIST)
                {
                    MergedList.Add(SYMPTOM);
                }
            }

            // Check if making distinct
            if (DistinctOnly == true) MergedList = MergedList.Distinct().ToList();

            // Return the list
            return MergedList;
        }

        // Helper to build CSV headers
        private string[,] BuildCsvHeaderArray(int FixedColCount, List<string> AllSymptoms)
        {
            // Total columns will be fixed + however many symptoms there are
            int TotalColumns = FixedColCount + AllSymptoms.Count;

            // Create a single row array to return the col headers outside the loop
            var ColHeaders = new string[1, TotalColumns];

            // Fill in fixed col headers
            ColHeaders[0, 0] = "ID";
            ColHeaders[0, 1] = "DateReceived";
            ColHeaders[0, 2] = "AgeOfPatient";
            ColHeaders[0, 3] = "Sex";
            ColHeaders[0, 4] = "IsDead";
            ColHeaders[0, 5] = "IsRecovered";
            ColHeaders[0, 6] = "NumOfDays";
            ColHeaders[0, 7] = "OtherMeds";
            ColHeaders[0, 8] = "OtherIllness";
            ColHeaders[0, 9] = "History";
            ColHeaders[0, 10] = "Allergies";
            ColHeaders[0, 11] = "Description";
            ColHeaders[0, 12] = "VaxType";
            ColHeaders[0, 13] = "VaxManufacturer";
            ColHeaders[0, 14] = "VaxLot";
            ColHeaders[0, 15] = "VaxSite";
            ColHeaders[0, 16] = "VaxName";

            // Dynamically add in the columns for each known symptom
            foreach (var SYMPTOM in AllSymptoms)
            {
                // Get the index of the current symptom
                int SymptomIndex = AllSymptoms.IndexOf(SYMPTOM);

                // Insert into array at next location
                ColHeaders[0, FixedColCount + SymptomIndex] = SYMPTOM;
            }

            // Escape the array and return it
            return EscapeArrayForCsv(ColHeaders);
        }

        // Helper to escape 2D array so it can be written to CSV
        private string[,] EscapeArrayForCsv(string[,] ArrayToEscape)
        {
            // Create CSV parser to help and load up array
            var CSVParser = new Utilities.CSVParser();
            CSVParser.CSVContent = ArrayToEscape;

            // Escape it and return
            return CSVParser.CSVContentEscaped;
        }

        // Helper to report status to callback method
        private void ReportStatusMsg(string Msg)
        {
            try
            {
                // Bail if there's no callback
                if (InputCallbackReportStatus == null) return;

                // If we have a callback, hit it
                InputCallbackReportStatus(Msg);
            }
            catch (Exception ex)
            {
                // If error occurs, just log and bail
                ThisApp.Methods.Logging.WriteMsgWithException("Error occurred while hitting callback to report status in " + nameof(DatabaseManager) + "!", ex);
            }
        }


        #endregion

    }

}
