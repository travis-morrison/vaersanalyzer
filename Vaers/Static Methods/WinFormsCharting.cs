﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StaticMethods
{

    public partial struct WinForms
    {

        public struct Charting
        {

            /// <summary>
            /// Enum that allows users to specify which axis to use.
            /// </summary>
            public enum AxisOptions
            {
                X,
                Y
            }

            /// <summary>
            /// Plots passed in data as a line chart on a user specified chart control.
            /// </summary>
            /// <param name="ChartToPopulate">The chart control to plot the data on.</param>
            /// <param name="DataSeries">The lists of key / value pairs to plot on the chart.</param>
            /// <param name="DataSeriesName">OPTIONAL:  The data series to display in the chart legend if visible.</param>
            /// <param name="DataSeriesColor">OPTIONAL:  The color to use when drawing the data series line.</param>
            /// <param name="DataSeriesLineThickness">OPTIONAL:  The thickness to draw the line of the series with (values must be >= 1).</param>
            /// <param name="DataSeriesLineStyle">OPTIONAL:  The line style to use when drawing the series (solid line, dotted line, etc.).</param>
            /// <param name="HideDots">OPTIONAL:  Flag to indicate if the chart's dots should be hidden (e.g. show line segments only).</param>
            /// <param name="BackgroundColor">OPTIONAL:  Used to specify the background color that will be visible on the chart.</param>
            /// <param name="BackgroundGradient">OPTIONAL:  Used to specify any color gradient effects you would like applied to chart background.</param>
            /// <param name="HideLegend">OPTIONAL:  Flag to indicate if the chart's legend should be hidden.</param>
            /// <param name="XAxisMin">OPTIONAL:  Used to fix the axis minimum. The minimum will be auto-scaled if the value is null.</param>
            /// <param name="XAxisMax">OPTIONAL:  Used to fix the axis maximum. The maximum will be auto-scaled if the value is null.</param>
            /// <param name="XAxisInterval">OPTIONAL:  Used to set the tick mark interval for the axis. The interval will be auto-scaled if the value is null.</param>
            /// <param name="XAxisShowGridLines">OPTIONAL:  Flag to indicate whether grid lines should be displayed for this axis.</param>
            /// <param name="XAxisFontSize">OPTIONAL:  Sets the font size for this axis (must be >= 1). If left null, the chart will retain the default font sizes.</param>
            /// <param name="YAxisMin">OPTIONAL:  Used to fix the axis minimum. The minimum will be auto-scaled if the value is null.</param>
            /// <param name="YAxisMax">OPTIONAL:  Used to fix the axis maximum. The maximum will be auto-scaled if the value is null.</param>
            /// <param name="YAxisInterval">OPTIONAL:  Used to set the tick mark interval for the axis. The interval will be auto-scaled if the value is null.</param>
            /// <param name="YAxisShowGridLines">OPTIONAL:  Flag to indicate whether grid lines should be displayed for this axis.</param>
            /// <param name="YAxisFontSize">OPTIONAL:  Sets the font size for this axis (must be >= 1). If left null, the chart will retain the default font sizes.</param>
            public static void PopulateLineChart(System.Windows.Forms.DataVisualization.Charting.Chart ChartToPopulate, List<KeyValuePair<double, double>> DataSeries, string DataSeriesName = "", Nullable<System.Drawing.Color> DataSeriesColor = null, Nullable<int> DataSeriesLineThickness = null, Nullable<System.Windows.Forms.DataVisualization.Charting.ChartDashStyle> DataSeriesLineStyle = default(System.Windows.Forms.DataVisualization.Charting.ChartDashStyle?), bool HideDots = false, Nullable<System.Drawing.Color> BackgroundColor = null, System.Windows.Forms.DataVisualization.Charting.GradientStyle BackgroundGradient = System.Windows.Forms.DataVisualization.Charting.GradientStyle.None, bool HideLegend = false, Nullable<double> XAxisMin = default(Double?), Nullable<double> XAxisMax = default(Double?), Nullable<double> XAxisInterval = default(Double?), bool XAxisShowGridLines = false, Nullable<int> XAxisFontSize = null, Nullable<double> YAxisMin = default(Double?), Nullable<double> YAxisMax = default(Double?), Nullable<double> YAxisInterval = default(Double?), bool YAxisShowGridLines = true, Nullable<int> YAxisFontSize = null)
            {
                // Add data series to list for common method
                var AllDataSeries = new[] { DataSeries }.ToList();

                // Create lists to hold series names and colors if they were passed in
                var AllDataSeriesNames = new List<string>();
                var AllDataSeriesColors = new List<System.Drawing.Color>();

                // Check if name was passed in
                if (string.IsNullOrEmpty(DataSeriesName) == false)
                {
                    // If so, add it to list
                    AllDataSeriesNames.Add(DataSeriesName);
                }
                else
                {
                    // If not, null out list
                    AllDataSeriesNames = null;
                }

                // Check if color was passed in
                if (DataSeriesColor != null)
                {
                    // If so, add it to the list
                    AllDataSeriesColors.Add((System.Drawing.Color)DataSeriesColor);
                }
                else
                {
                    // If not, null out list
                    AllDataSeriesColors = null;
                }

                // Create vars to hold line thicknesses and styles
                var AllLineThicknesses = new List<int>();
                var AllLineStyles = new List<System.Windows.Forms.DataVisualization.Charting.ChartDashStyle>();

                // Check if thickness was specified
                if (DataSeriesLineThickness != null)
                {
                    // If so, add it
                    AllLineThicknesses.Add(Convert.ToInt32(DataSeriesLineThickness));
                }
                else
                {
                    // If not, null out list
                    AllLineThicknesses = null;
                }

                // Check if line style was specified
                if (DataSeriesLineStyle != null)
                {
                    // If so, use it
                    AllLineStyles.Add((System.Windows.Forms.DataVisualization.Charting.ChartDashStyle)DataSeriesLineStyle);
                }
                else
                {
                    // If not, null out list
                    AllLineStyles = null;
                }

                // Use common method
                PopulateLineChart(ChartToPopulate, AllDataSeries.ToArray(), AllDataSeriesNames: AllDataSeriesNames, AllDataSeriesColors: AllDataSeriesColors, AllDataSeriesLineThicknesses: AllLineThicknesses, AllDataSeriesLineStyles: AllLineStyles, HideDots: HideDots, BackgroundColor: BackgroundColor, BackgroundGradient: BackgroundGradient, HideLegend: HideLegend, XAxisMin: XAxisMin, XAxisMax: XAxisMax, XAxisInterval: XAxisInterval, XAxisShowGridLines: XAxisShowGridLines, XAxisFontSize: XAxisFontSize, YAxisMin: YAxisMin, YAxisMax: YAxisMax, YAxisInterval: YAxisInterval, YAxisShowGridLines: YAxisShowGridLines, YAxisFontSize: YAxisFontSize);
            }
            /// <summary>
            /// Plots passed in data as a line chart on a user specified chart control.
            /// </summary>
            /// <param name="ChartToPopulate">The chart control to plot the data on.</param>
            /// <param name="AllDataSeries">An array of lists of key / value pairs (in case there are multiple data sets) to plot on the chart.</param>
            /// <param name="AllDataSeriesNames">OPTIONAL:  List of names for each data series to display in the chart legend if visible.</param>
            /// <param name="AllDataSeriesColors">OPTIONAL:  List of colors to use when drawing each data series line.</param>
            /// <param name="AllDataSeriesLineThicknesses">OPTIONAL:  List of integer values that specify how thick to draw each line in the series (values must be >= 1).</param>
            /// <param name="AllDataSeriesLineStyles">OPTIONAL:  List of enums to specify the type of line style to draw each series with (solid line, dotted line, etc.).</param>
            /// <param name="HideDots">OPTIONAL:  Flag to indicate if the chart's dots should be hidden (e.g. show line segments only).</param>
            /// <param name="BackgroundColor">OPTIONAL:  Used to specify the background color that will be visible on the chart.</param>
            /// <param name="BackgroundGradient">OPTIONAL:  Used to specify any color gradient effects you would like applied to chart background.</param>
            /// <param name="HideLegend">OPTIONAL:  Flag to indicate if the chart's legend should be hidden.</param>
            /// <param name="XAxisMin">OPTIONAL:  Used to fix the axis minimum. The minimum will be auto-scaled if the value is null.</param>
            /// <param name="XAxisMax">OPTIONAL:  Used to fix the axis maximum. The maximum will be auto-scaled if the value is null.</param>
            /// <param name="XAxisInterval">OPTIONAL:  Used to set the tick mark interval for the axis. The interval will be auto-scaled if the value is null.</param>
            /// <param name="XAxisShowGridLines">OPTIONAL:  Flag to indicate whether grid lines should be displayed for this axis.</param>
            /// <param name="XAxisFontSize">OPTIONAL:  Sets the font size for this axis (must be >= 1). If left null, the chart will retain the default font sizes.</param>
            /// <param name="YAxisMin">OPTIONAL:  Used to fix the axis minimum. The minimum will be auto-scaled if the value is null.</param>
            /// <param name="YAxisMax">OPTIONAL:  Used to fix the axis maximum. The maximum will be auto-scaled if the value is null.</param>
            /// <param name="YAxisInterval">OPTIONAL:  Used to set the tick mark interval for the axis. The interval will be auto-scaled if the value is null.</param>
            /// <param name="YAxisShowGridLines">OPTIONAL:  Flag to indicate whether grid lines should be displayed for this axis.</param>
            /// <param name="YAxisFontSize">OPTIONAL:  Sets the font size for this axis (must be >= 1). If left null, the chart will retain the default font sizes.</param>
            public static void PopulateLineChart(System.Windows.Forms.DataVisualization.Charting.Chart ChartToPopulate, List<KeyValuePair<double, double>>[] AllDataSeries, List<string> AllDataSeriesNames = null, List<System.Drawing.Color> AllDataSeriesColors = null, List<int> AllDataSeriesLineThicknesses = null, List<System.Windows.Forms.DataVisualization.Charting.ChartDashStyle> AllDataSeriesLineStyles = null, bool HideDots = false, Nullable<System.Drawing.Color> BackgroundColor = null, System.Windows.Forms.DataVisualization.Charting.GradientStyle BackgroundGradient = System.Windows.Forms.DataVisualization.Charting.GradientStyle.None, bool HideLegend = false, Nullable<double> XAxisMin = default(Double?), Nullable<double> XAxisMax = default(Double?), Nullable<double> XAxisInterval = default(Double?), bool XAxisShowGridLines = false, Nullable<int> XAxisFontSize = null, Nullable<double> YAxisMin = default(Double?), Nullable<double> YAxisMax = default(Double?), Nullable<double> YAxisInterval = default(Double?), bool YAxisShowGridLines = true, Nullable<int> YAxisFontSize = null)
            {
                // Use common method
                PopulateLineChartCommon(ChartToPopulate, AllDataSeries, AllDataSeriesNames, AllDataSeriesColors, AllDataSeriesLineThicknesses, AllDataSeriesLineStyles, HideDots, BackgroundColor, BackgroundGradient, HideLegend, XAxisMin, XAxisMax, XAxisInterval, XAxisShowGridLines, XAxisFontSize, YAxisMin, YAxisMax, YAxisInterval, YAxisShowGridLines, YAxisFontSize, false);
            }
            /// <summary>
            /// Plots passed in data as a line chart on a user specified chart control.
            /// </summary>
            /// <param name="ChartToPopulate">The chart control to plot the data on.</param>
            /// <param name="DataSeries">The lists of key / value pairs to plot on the chart.</param>
            /// <param name="DataSeriesName">OPTIONAL:  The data series to display in the chart legend if visible.</param>
            /// <param name="DataSeriesColor">OPTIONAL:  The color to use when drawing the data series line.</param>
            /// <param name="DataSeriesLineThickness">OPTIONAL:  The thickness to draw the line of the series with (values must be >= 1).</param>
            /// <param name="DataSeriesLineStyle">OPTIONAL:  The line style to use when drawing the series (solid line, dotted line, etc.).</param>
            /// <param name="HideDots">OPTIONAL:  Flag to indicate if the chart's dots should be hidden (e.g. show line segments only).</param>
            /// <param name="BackgroundColor">OPTIONAL:  Used to specify the background color that will be visible on the chart.</param>
            /// <param name="BackgroundGradient">OPTIONAL:  Used to specify any color gradient effects you would like applied to chart background.</param>
            /// <param name="HideLegend">OPTIONAL:  Flag to indicate if the chart's legend should be hidden.</param>
            /// <param name="XAxisShowGridLines">OPTIONAL:  Flag to indicate whether grid lines should be displayed for this axis.</param>
            /// <param name="XAxisFontSize">OPTIONAL:  Sets the font size for this axis (must be >= 1). If left null, the chart will retain the default font sizes.</param>
            /// <param name="YAxisMin">OPTIONAL:  Used to fix the axis minimum. The minimum will be auto-scaled if the value is null.</param>
            /// <param name="YAxisMax">OPTIONAL:  Used to fix the axis maximum. The maximum will be auto-scaled if the value is null.</param>
            /// <param name="YAxisInterval">OPTIONAL:  Used to set the tick mark interval for the axis. The interval will be auto-scaled if the value is null.</param>
            /// <param name="YAxisShowGridLines">OPTIONAL:  Flag to indicate whether grid lines should be displayed for this axis.</param>
            /// <param name="YAxisFontSize">OPTIONAL:  Sets the font size for this axis (must be >= 1). If left null, the chart will retain the default font sizes.</param>
            public static void PopulateLineChart(System.Windows.Forms.DataVisualization.Charting.Chart ChartToPopulate, List<KeyValuePair<string, double>> DataSeries, string DataSeriesName = "", Nullable<System.Drawing.Color> DataSeriesColor = null, Nullable<int> DataSeriesLineThickness = null, Nullable<System.Windows.Forms.DataVisualization.Charting.ChartDashStyle> DataSeriesLineStyle = default(System.Windows.Forms.DataVisualization.Charting.ChartDashStyle?), bool HideDots = false, Nullable<System.Drawing.Color> BackgroundColor = null, System.Windows.Forms.DataVisualization.Charting.GradientStyle BackgroundGradient = System.Windows.Forms.DataVisualization.Charting.GradientStyle.None, bool HideLegend = false, bool XAxisShowGridLines = false, Nullable<int> XAxisFontSize = null, Nullable<double> YAxisMin = default(Double?), Nullable<double> YAxisMax = default(Double?), Nullable<double> YAxisInterval = default(Double?), bool YAxisShowGridLines = true, Nullable<int> YAxisFontSize = null)
            {
                // Add data series to list for common method
                var AllDataSeries = new[] { DataSeries }.ToList();

                // Create lists to hold series names and colors if they were passed in
                var AllDataSeriesNames = new List<string>();
                var AllDataSeriesColors = new List<System.Drawing.Color>();

                // Check if name was passed in
                if (string.IsNullOrEmpty(DataSeriesName) == false)
                {
                    // If so, add it to list
                    AllDataSeriesNames.Add(DataSeriesName);
                }
                else
                {
                    // If not, null out list
                    AllDataSeriesNames = null;
                }

                // Check if color was passed in
                if (DataSeriesColor != null)
                {
                    // If so, add it to the list
                    AllDataSeriesColors.Add((System.Drawing.Color)DataSeriesColor);
                }
                else
                {
                    // If not, null out list
                    AllDataSeriesColors = null;
                }

                // Create vars to hold line thicknesses and styles
                var AllLineThicknesses = new List<int>();
                var AllLineStyles = new List<System.Windows.Forms.DataVisualization.Charting.ChartDashStyle>();

                // Check if thickness was specified
                if (DataSeriesLineThickness != null)
                {
                    // If so, add it
                    AllLineThicknesses.Add(Convert.ToInt32(DataSeriesLineThickness));
                }
                else
                {
                    // If not, null out list
                    AllLineThicknesses = null;
                }

                // Check if line style was specified
                if (DataSeriesLineStyle != null)
                {
                    // If so, use it
                    AllLineStyles.Add((System.Windows.Forms.DataVisualization.Charting.ChartDashStyle)DataSeriesLineStyle);
                }
                else
                {
                    // If not, null out list
                    AllLineStyles = null;
                }

                // Use common method
                PopulateLineChart(ChartToPopulate, AllDataSeries.ToArray(), AllDataSeriesNames: AllDataSeriesNames, AllDataSeriesColors: AllDataSeriesColors, AllDataSeriesLineThicknesses: AllLineThicknesses, AllDataSeriesLineStyles: AllLineStyles, HideDots: HideDots, BackgroundColor: BackgroundColor, BackgroundGradient: BackgroundGradient, HideLegend: HideLegend, XAxisShowGridLines: XAxisShowGridLines, XAxisFontSize: XAxisFontSize, YAxisMin: YAxisMin, YAxisMax: YAxisMax, YAxisInterval: YAxisInterval, YAxisShowGridLines: YAxisShowGridLines, YAxisFontSize: YAxisFontSize);
            }
            /// <summary>
            /// Plots passed in data as a line chart on a user specified chart control.
            /// </summary>
            /// <param name="ChartToPopulate">The chart control to plot the data on.</param>
            /// <param name="AllDataSeries">An array of lists of key / value pairs (in case there are multiple data sets) to plot on the chart.</param>
            /// <param name="AllDataSeriesNames">OPTIONAL:  List of names for each data series to display in the chart legend if visible.</param>
            /// <param name="AllDataSeriesColors">OPTIONAL:  List of colors to use when drawing each data series line.</param>
            /// <param name="AllDataSeriesLineThicknesses">OPTIONAL:  List of integer values that specify how thick to draw each line in the series (values must be >= 1).</param>
            /// <param name="AllDataSeriesLineStyles">OPTIONAL:  List of enums to specify the type of line style to draw each series with (solid line, dotted line, etc.).</param>
            /// <param name="HideDots">OPTIONAL:  Flag to indicate if the chart's dots should be hidden (e.g. show line segments only).</param>
            /// <param name="BackgroundColor">OPTIONAL:  Used to specify the background color that will be visible on the chart.</param>
            /// <param name="BackgroundGradient">OPTIONAL:  Used to specify any color gradient effects you would like applied to chart background.</param>
            /// <param name="HideLegend">OPTIONAL:  Flag to indicate if the chart's legend should be hidden.</param>
            /// <param name="XAxisShowGridLines">OPTIONAL:  Flag to indicate whether grid lines should be displayed for this axis.</param>
            /// <param name="XAxisFontSize">OPTIONAL:  Sets the font size for this axis (must be >= 1). If left null, the chart will retain the default font sizes.</param>
            /// <param name="YAxisMin">OPTIONAL:  Used to fix the axis minimum. The minimum will be auto-scaled if the value is null.</param>
            /// <param name="YAxisMax">OPTIONAL:  Used to fix the axis maximum. The maximum will be auto-scaled if the value is null.</param>
            /// <param name="YAxisInterval">OPTIONAL:  Used to set the tick mark interval for the axis. The interval will be auto-scaled if the value is null.</param>
            /// <param name="YAxisShowGridLines">OPTIONAL:  Flag to indicate whether grid lines should be displayed for this axis.</param>
            /// <param name="YAxisFontSize">OPTIONAL:  Sets the font size for this axis (must be >= 1). If left null, the chart will retain the default font sizes.</param>
            public static void PopulateLineChart(System.Windows.Forms.DataVisualization.Charting.Chart ChartToPopulate, List<KeyValuePair<string, double>>[] AllDataSeries, List<string> AllDataSeriesNames = null, List<System.Drawing.Color> AllDataSeriesColors = null, List<int> AllDataSeriesLineThicknesses = null, List<System.Windows.Forms.DataVisualization.Charting.ChartDashStyle> AllDataSeriesLineStyles = null, bool HideDots = false, Nullable<System.Drawing.Color> BackgroundColor = null, System.Windows.Forms.DataVisualization.Charting.GradientStyle BackgroundGradient = System.Windows.Forms.DataVisualization.Charting.GradientStyle.None, bool HideLegend = false, bool XAxisShowGridLines = false, Nullable<int> XAxisFontSize = null, Nullable<double> YAxisMin = default(Double?), Nullable<double> YAxisMax = default(Double?), Nullable<double> YAxisInterval = default(Double?), bool YAxisShowGridLines = true, Nullable<int> YAxisFontSize = null)
            {
                // Use common method
                PopulateLineChartCommon(ChartToPopulate, AllDataSeries, AllDataSeriesNames, AllDataSeriesColors, AllDataSeriesLineThicknesses, AllDataSeriesLineStyles, HideDots, BackgroundColor, BackgroundGradient, HideLegend, null, null, null, XAxisShowGridLines, XAxisFontSize, YAxisMin, YAxisMax, YAxisInterval, YAxisShowGridLines, YAxisFontSize, false);
            }
            /// <summary>
            /// Plots passed in data as a line chart on a user specified chart control.
            /// </summary>
            /// <param name="ChartToPopulate">The chart control to plot the data on.</param>
            /// <param name="DataSeries">The lists of key / value pairs to plot on the chart.</param>
            /// <param name="DataSeriesName">OPTIONAL:  The data series to display in the chart legend if visible.</param>
            /// <param name="DataSeriesColor">OPTIONAL:  The color to use when drawing the data series line.</param>
            /// <param name="DataSeriesLineThickness">OPTIONAL:  The thickness to draw the line of the series with (values must be >= 1).</param>
            /// <param name="DataSeriesLineStyle">OPTIONAL:  The line style to use when drawing the series (solid line, dotted line, etc.).</param>
            /// <param name="HideDots">OPTIONAL:  Flag to indicate if the chart's dots should be hidden (e.g. show line segments only).</param>
            /// <param name="BackgroundColor">OPTIONAL:  Used to specify the background color that will be visible on the chart.</param>
            /// <param name="BackgroundGradient">OPTIONAL:  Used to specify any color gradient effects you would like applied to chart background.</param>
            /// <param name="HideLegend">OPTIONAL:  Flag to indicate if the chart's legend should be hidden.</param>
            /// <param name="XAxisShowGridLines">OPTIONAL:  Flag to indicate whether grid lines should be displayed for this axis.</param>
            /// <param name="XAxisFontSize">OPTIONAL:  Sets the font size for this axis (must be >= 1). If left null, the chart will retain the default font sizes.</param>
            /// <param name="YAxisMin">OPTIONAL:  Used to fix the axis minimum. The minimum will be auto-scaled if the value is null.</param>
            /// <param name="YAxisMax">OPTIONAL:  Used to fix the axis maximum. The maximum will be auto-scaled if the value is null.</param>
            /// <param name="YAxisInterval">OPTIONAL:  Used to set the tick mark interval for the axis. The interval will be auto-scaled if the value is null.</param>
            /// <param name="YAxisShowGridLines">OPTIONAL:  Flag to indicate whether grid lines should be displayed for this axis.</param>
            /// <param name="YAxisFontSize">OPTIONAL:  Sets the font size for this axis (must be >= 1). If left null, the chart will retain the default font sizes.</param>
            public static void PopulateLineChart(System.Windows.Forms.DataVisualization.Charting.Chart ChartToPopulate, List<KeyValuePair<DateTime, double>> DataSeries, string DataSeriesName = "", Nullable<System.Drawing.Color> DataSeriesColor = null, Nullable<int> DataSeriesLineThickness = null, Nullable<System.Windows.Forms.DataVisualization.Charting.ChartDashStyle> DataSeriesLineStyle = default(System.Windows.Forms.DataVisualization.Charting.ChartDashStyle?), bool HideDots = false, Nullable<System.Drawing.Color> BackgroundColor = null, System.Windows.Forms.DataVisualization.Charting.GradientStyle BackgroundGradient = System.Windows.Forms.DataVisualization.Charting.GradientStyle.None, bool HideLegend = false, bool XAxisShowGridLines = false, Nullable<int> XAxisFontSize = null, Nullable<double> YAxisMin = default(Double?), Nullable<double> YAxisMax = default(Double?), Nullable<double> YAxisInterval = default(Double?), bool YAxisShowGridLines = true, Nullable<int> YAxisFontSize = null)
            {
                // Add data series to list for common method
                var AllDataSeries = new[] { DataSeries }.ToList();

                // Create lists to hold series names and colors if they were passed in
                var AllDataSeriesNames = new List<string>();
                var AllDataSeriesColors = new List<System.Drawing.Color>();

                // Check if name was passed in
                if (string.IsNullOrEmpty(DataSeriesName) == false)
                {
                    // If so, add it to list
                    AllDataSeriesNames.Add(DataSeriesName);
                }
                else
                {
                    // If not, null out list
                    AllDataSeriesNames = null;
                }

                // Check if color was passed in
                if (DataSeriesColor != null)
                {
                    // If so, add it to the list
                    AllDataSeriesColors.Add((System.Drawing.Color)DataSeriesColor);
                }
                else
                {
                    // If not, null out list
                    AllDataSeriesColors = null;
                }

                // Create vars to hold line thicknesses and styles
                var AllLineThicknesses = new List<int>();
                var AllLineStyles = new List<System.Windows.Forms.DataVisualization.Charting.ChartDashStyle>();

                // Check if thickness was specified
                if (DataSeriesLineThickness != null)
                {
                    // If so, add it
                    AllLineThicknesses.Add(Convert.ToInt32(DataSeriesLineThickness));
                }
                else
                {
                    // If not, null out list
                    AllLineThicknesses = null;
                }

                // Check if line style was specified
                if (DataSeriesLineStyle != null)
                {
                    // If so, use it
                    AllLineStyles.Add((System.Windows.Forms.DataVisualization.Charting.ChartDashStyle)DataSeriesLineStyle);
                }
                else
                {
                    // If not, null out list
                    AllLineStyles = null;
                }

                // Use common method
                PopulateLineChart(ChartToPopulate, AllDataSeries.ToArray(), AllDataSeriesNames: AllDataSeriesNames, AllDataSeriesColors: AllDataSeriesColors, AllDataSeriesLineThicknesses: AllLineThicknesses, AllDataSeriesLineStyles: AllLineStyles, HideDots: HideDots, BackgroundColor: BackgroundColor, BackgroundGradient: BackgroundGradient, HideLegend: HideLegend, XAxisShowGridLines: XAxisShowGridLines, XAxisFontSize: XAxisFontSize, YAxisMin: YAxisMin, YAxisMax: YAxisMax, YAxisInterval: YAxisInterval, YAxisShowGridLines: YAxisShowGridLines, YAxisFontSize: YAxisFontSize);
            }
            /// <summary>
            /// Plots passed in data as a line chart on a user specified chart control.
            /// </summary>
            /// <param name="ChartToPopulate">The chart control to plot the data on.</param>
            /// <param name="AllDataSeries">An array of lists of key / value pairs (in case there are multiple data sets) to plot on the chart.</param>
            /// <param name="AllDataSeriesNames">OPTIONAL:  List of names for each data series to display in the chart legend if visible.</param>
            /// <param name="AllDataSeriesColors">OPTIONAL:  List of colors to use when drawing each data series line.</param>
            /// <param name="AllDataSeriesLineThicknesses">OPTIONAL:  List of integer values that specify how thick to draw each line in the series (values must be >= 1).</param>
            /// <param name="AllDataSeriesLineStyles">OPTIONAL:  List of enums to specify the type of line style to draw each series with (solid line, dotted line, etc.).</param>
            /// <param name="HideDots">OPTIONAL:  Flag to indicate if the chart's dots should be hidden (e.g. show line segments only).</param>
            /// <param name="BackgroundColor">OPTIONAL:  Used to specify the background color that will be visible on the chart.</param>
            /// <param name="BackgroundGradient">OPTIONAL:  Used to specify any color gradient effects you would like applied to chart background.</param>
            /// <param name="HideLegend">OPTIONAL:  Flag to indicate if the chart's legend should be hidden.</param>
            /// <param name="XAxisShowGridLines">OPTIONAL:  Flag to indicate whether grid lines should be displayed for this axis.</param>
            /// <param name="XAxisFontSize">OPTIONAL:  Sets the font size for this axis (must be >= 1). If left null, the chart will retain the default font sizes.</param>
            /// <param name="YAxisMin">OPTIONAL:  Used to fix the axis minimum. The minimum will be auto-scaled if the value is null.</param>
            /// <param name="YAxisMax">OPTIONAL:  Used to fix the axis maximum. The maximum will be auto-scaled if the value is null.</param>
            /// <param name="YAxisInterval">OPTIONAL:  Used to set the tick mark interval for the axis. The interval will be auto-scaled if the value is null.</param>
            /// <param name="YAxisShowGridLines">OPTIONAL:  Flag to indicate whether grid lines should be displayed for this axis.</param>
            /// <param name="YAxisFontSize">OPTIONAL:  Sets the font size for this axis (must be >= 1). If left null, the chart will retain the default font sizes.</param>
            public static void PopulateLineChart(System.Windows.Forms.DataVisualization.Charting.Chart ChartToPopulate, List<KeyValuePair<DateTime, double>>[] AllDataSeries, List<string> AllDataSeriesNames = null, List<System.Drawing.Color> AllDataSeriesColors = null, List<int> AllDataSeriesLineThicknesses = null, List<System.Windows.Forms.DataVisualization.Charting.ChartDashStyle> AllDataSeriesLineStyles = null, bool HideDots = false, Nullable<System.Drawing.Color> BackgroundColor = null, System.Windows.Forms.DataVisualization.Charting.GradientStyle BackgroundGradient = System.Windows.Forms.DataVisualization.Charting.GradientStyle.None, bool HideLegend = false, bool XAxisShowGridLines = false, Nullable<int> XAxisFontSize = null, Nullable<double> YAxisMin = default(Double?), Nullable<double> YAxisMax = default(Double?), Nullable<double> YAxisInterval = default(Double?), bool YAxisShowGridLines = true, Nullable<int> YAxisFontSize = null)
            {
                // Use common method
                PopulateLineChartCommon(ChartToPopulate, AllDataSeries, AllDataSeriesNames, AllDataSeriesColors, AllDataSeriesLineThicknesses, AllDataSeriesLineStyles, HideDots, BackgroundColor, BackgroundGradient, HideLegend, null, null, null, XAxisShowGridLines, XAxisFontSize, YAxisMin, YAxisMax, YAxisInterval, YAxisShowGridLines, YAxisFontSize, false);
            }
            /// <summary>
            /// Plots passed in data as a FAST line chart on a user specified chart control (used for VERY large data sets).
            /// </summary>
            /// <param name="ChartToPopulate">The chart control to plot the data on.</param>
            /// <param name="DataSeries">The lists of key / value pairs to plot on the chart.</param>
            /// <param name="DataSeriesName">OPTIONAL:  The data series to display in the chart legend if visible.</param>
            /// <param name="DataSeriesColor">OPTIONAL:  The color to use when drawing the data series line.</param>
            /// <param name="DataSeriesLineThickness">OPTIONAL:  The thickness to draw the line of the series with (values must be >= 1).</param>
            /// <param name="DataSeriesLineStyle">OPTIONAL:  The line style to use when drawing the series (solid line, dotted line, etc.).</param>
            /// <param name="BackgroundColor">OPTIONAL:  Used to specify the background color that will be visible on the chart.</param>
            /// <param name="BackgroundGradient">OPTIONAL:  Used to specify any color gradient effects you would like applied to chart background.</param>
            /// <param name="HideLegend">OPTIONAL:  Flag to indicate if the chart's legend should be hidden.</param>
            /// <param name="XAxisMin">OPTIONAL:  Used to fix the axis minimum. The minimum will be auto-scaled if the value is null.</param>
            /// <param name="XAxisMax">OPTIONAL:  Used to fix the axis maximum. The maximum will be auto-scaled if the value is null.</param>
            /// <param name="XAxisInterval">OPTIONAL:  Used to set the tick mark interval for the axis. The interval will be auto-scaled if the value is null.</param>
            /// <param name="XAxisShowGridLines">OPTIONAL:  Flag to indicate whether grid lines should be displayed for this axis.</param>
            /// <param name="XAxisFontSize">OPTIONAL:  Sets the font size for this axis (must be >= 1). If left null, the chart will retain the default font sizes.</param>
            /// <param name="YAxisMin">OPTIONAL:  Used to fix the axis minimum. The minimum will be auto-scaled if the value is null.</param>
            /// <param name="YAxisMax">OPTIONAL:  Used to fix the axis maximum. The maximum will be auto-scaled if the value is null.</param>
            /// <param name="YAxisInterval">OPTIONAL:  Used to set the tick mark interval for the axis. The interval will be auto-scaled if the value is null.</param>
            /// <param name="YAxisShowGridLines">OPTIONAL:  Flag to indicate whether grid lines should be displayed for this axis.</param>
            /// <param name="YAxisFontSize">OPTIONAL:  Sets the font size for this axis (must be >= 1). If left null, the chart will retain the default font sizes.</param>
            public static void PopulateFastLineChart(System.Windows.Forms.DataVisualization.Charting.Chart ChartToPopulate, List<KeyValuePair<double, double>> DataSeries, string DataSeriesName = "", Nullable<System.Drawing.Color> DataSeriesColor = null, Nullable<int> DataSeriesLineThickness = null, Nullable<System.Windows.Forms.DataVisualization.Charting.ChartDashStyle> DataSeriesLineStyle = default(System.Windows.Forms.DataVisualization.Charting.ChartDashStyle?), Nullable<System.Drawing.Color> BackgroundColor = null, System.Windows.Forms.DataVisualization.Charting.GradientStyle BackgroundGradient = System.Windows.Forms.DataVisualization.Charting.GradientStyle.None, bool HideLegend = false, Nullable<double> XAxisMin = default(Double?), Nullable<double> XAxisMax = default(Double?), Nullable<double> XAxisInterval = default(Double?), bool XAxisShowGridLines = false, Nullable<int> XAxisFontSize = null, Nullable<double> YAxisMin = default(Double?), Nullable<double> YAxisMax = default(Double?), Nullable<double> YAxisInterval = default(Double?), bool YAxisShowGridLines = true, Nullable<int> YAxisFontSize = null)
            {
                // Add data series to list for common method
                var AllDataSeries = new[] { DataSeries }.ToList();

                // Create lists to hold series names and colors if they were passed in
                var AllDataSeriesNames = new List<string>();
                var AllDataSeriesColors = new List<System.Drawing.Color>();

                // Check if name was passed in
                if (string.IsNullOrEmpty(DataSeriesName) == false)
                {
                    // If so, add it to list
                    AllDataSeriesNames.Add(DataSeriesName);
                }
                else
                {
                    // If not, null out list
                    AllDataSeriesNames = null;
                }

                // Check if color was passed in
                if (DataSeriesColor != null)
                {
                    // If so, add it to the list
                    AllDataSeriesColors.Add((System.Drawing.Color)DataSeriesColor);
                }
                else
                {
                    // If not, null out list
                    AllDataSeriesColors = null;
                }

                // Create vars to hold line thicknesses and styles
                var AllLineThicknesses = new List<int>();
                var AllLineStyles = new List<System.Windows.Forms.DataVisualization.Charting.ChartDashStyle>();

                // Check if thickness was specified
                if (DataSeriesLineThickness != null)
                {
                    // If so, add it
                    AllLineThicknesses.Add(Convert.ToInt32(DataSeriesLineThickness));
                }
                else
                {
                    // If not, null out list
                    AllLineThicknesses = null;
                }

                // Check if line style was specified
                if (DataSeriesLineStyle != null)
                {
                    // If so, use it
                    AllLineStyles.Add((System.Windows.Forms.DataVisualization.Charting.ChartDashStyle)DataSeriesLineStyle);
                }
                else
                {
                    // If not, null out list
                    AllLineStyles = null;
                }

                // Use common method
                PopulateLineChartCommon(ChartToPopulate, AllDataSeries.ToArray(), AllDataSeriesNames, AllDataSeriesColors, AllLineThicknesses, AllLineStyles, true, BackgroundColor, BackgroundGradient, HideLegend, XAxisMin, XAxisMax, XAxisInterval, XAxisShowGridLines, XAxisFontSize, YAxisMin, YAxisMax, YAxisInterval, YAxisShowGridLines, YAxisFontSize, true);
            }
            /// <summary>
            /// Plots passed in data as a FAST line chart on a user specified chart control (used for VERY large data sets).
            /// </summary>
            /// <param name="ChartToPopulate">The chart control to plot the data on.</param>
            /// <param name="DataSeries">The lists of key / value pairs to plot on the chart.</param>
            /// <param name="DataSeriesName">OPTIONAL:  The data series to display in the chart legend if visible.</param>
            /// <param name="DataSeriesColor">OPTIONAL:  The color to use when drawing the data series line.</param>
            /// <param name="DataSeriesLineThickness">OPTIONAL:  The thickness to draw the line of the series with (values must be >= 1).</param>
            /// <param name="DataSeriesLineStyle">OPTIONAL:  The line style to use when drawing the series (solid line, dotted line, etc.).</param>
            /// <param name="BackgroundColor">OPTIONAL:  Used to specify the background color that will be visible on the chart.</param>
            /// <param name="BackgroundGradient">OPTIONAL:  Used to specify any color gradient effects you would like applied to chart background.</param>
            /// <param name="HideLegend">OPTIONAL:  Flag to indicate if the chart's legend should be hidden.</param>
            /// <param name="XAxisShowGridLines">OPTIONAL:  Flag to indicate whether grid lines should be displayed for this axis.</param>
            /// <param name="XAxisFontSize">OPTIONAL:  Sets the font size for this axis (must be >= 1). If left null, the chart will retain the default font sizes.</param>
            /// <param name="YAxisMin">OPTIONAL:  Used to fix the axis minimum. The minimum will be auto-scaled if the value is null.</param>
            /// <param name="YAxisMax">OPTIONAL:  Used to fix the axis maximum. The maximum will be auto-scaled if the value is null.</param>
            /// <param name="YAxisInterval">OPTIONAL:  Used to set the tick mark interval for the axis. The interval will be auto-scaled if the value is null.</param>
            /// <param name="YAxisShowGridLines">OPTIONAL:  Flag to indicate whether grid lines should be displayed for this axis.</param>
            /// <param name="YAxisFontSize">OPTIONAL:  Sets the font size for this axis (must be >= 1). If left null, the chart will retain the default font sizes.</param>
            public static void PopulateFastLineChart(System.Windows.Forms.DataVisualization.Charting.Chart ChartToPopulate, List<KeyValuePair<string, double>> DataSeries, string DataSeriesName = "", Nullable<System.Drawing.Color> DataSeriesColor = null, Nullable<int> DataSeriesLineThickness = null, Nullable<System.Windows.Forms.DataVisualization.Charting.ChartDashStyle> DataSeriesLineStyle = default(System.Windows.Forms.DataVisualization.Charting.ChartDashStyle?), Nullable<System.Drawing.Color> BackgroundColor = null, System.Windows.Forms.DataVisualization.Charting.GradientStyle BackgroundGradient = System.Windows.Forms.DataVisualization.Charting.GradientStyle.None, bool HideLegend = false, bool XAxisShowGridLines = false, Nullable<int> XAxisFontSize = null, Nullable<double> YAxisMin = default(Double?), Nullable<double> YAxisMax = default(Double?), Nullable<double> YAxisInterval = default(Double?), bool YAxisShowGridLines = true, Nullable<int> YAxisFontSize = null)
            {
                // Add data series to list for common method
                var AllDataSeries = new[] { DataSeries }.ToList();

                // Create lists to hold series names and colors if they were passed in
                var AllDataSeriesNames = new List<string>();
                var AllDataSeriesColors = new List<System.Drawing.Color>();

                // Check if name was passed in
                if (string.IsNullOrEmpty(DataSeriesName) == false)
                {
                    // If so, add it to list
                    AllDataSeriesNames.Add(DataSeriesName);
                }
                else
                {
                    // If not, null out list
                    AllDataSeriesNames = null;
                }

                // Check if color was passed in
                if (DataSeriesColor != null)
                {
                    // If so, add it to the list
                    AllDataSeriesColors.Add((System.Drawing.Color)DataSeriesColor);
                }
                else
                {
                    // If not, null out list
                    AllDataSeriesColors = null;
                }

                // Create vars to hold line thicknesses and styles
                var AllLineThicknesses = new List<int>();
                var AllLineStyles = new List<System.Windows.Forms.DataVisualization.Charting.ChartDashStyle>();

                // Check if thickness was specified
                if (DataSeriesLineThickness != null)
                {
                    // If so, add it
                    AllLineThicknesses.Add(Convert.ToInt32(DataSeriesLineThickness));
                }
                else
                {
                    // If not, null out list
                    AllLineThicknesses = null;
                }

                // Check if line style was specified
                if (DataSeriesLineStyle != null)
                {
                    // If so, use it
                    AllLineStyles.Add((System.Windows.Forms.DataVisualization.Charting.ChartDashStyle)DataSeriesLineStyle);
                }
                else
                {
                    // If not, null out list
                    AllLineStyles = null;
                }

                // Use common method
                PopulateLineChartCommon(ChartToPopulate, AllDataSeries.ToArray(), AllDataSeriesNames, AllDataSeriesColors, AllLineThicknesses, AllLineStyles, true, BackgroundColor, BackgroundGradient, HideLegend, null, null, null, XAxisShowGridLines, XAxisFontSize, YAxisMin, YAxisMax, YAxisInterval, YAxisShowGridLines, YAxisFontSize, true);
            }

            /// <summary>
            /// Used to format the chart control so it is ready to receive line chart data.
            /// </summary>
            /// <param name="ChartToFormat">The chart control to plot the data on.</param>
            /// <param name="DataSeriesName">OPTIONAL:  List of names for each data series to display in the chart legend if visible.</param>
            /// <param name="DataSeriesColor">OPTIONAL:  List of colors to use when drawing each data series line.</param>
            /// <param name="DataSeriesLineThickness">OPTIONAL:  List of integer values that specify how thick to draw each line in the series (values must be >= 1).</param>
            /// <param name="DataSeriesLineStyle">OPTIONAL:  List of enums to specify the type of line style to draw each series with (solid line, dotted line, etc.).</param>
            /// <param name="HideDots">OPTIONAL:  Flag to indicate if the chart's dots should be hidden (e.g. show line segments only).</param>
            /// <param name="BackgroundColor">OPTIONAL:  Used to specify the background color that will be visible on the chart.</param>
            /// <param name="BackgroundGradient">OPTIONAL:  Used to specify any color gradient effects you would like applied to chart background.</param>
            /// <param name="HideLegend">OPTIONAL:  Flag to indicate if the chart's legend should be hidden.</param>
            /// <param name="XAxisShowGridLines">OPTIONAL:  Flag to indicate whether grid lines should be displayed for this axis.</param>
            /// <param name="XAxisFontSize">OPTIONAL:  Sets the font size for this axis (must be >= 1). If left null, the chart will retain the default font sizes.</param>
            /// <param name="YAxisMin">OPTIONAL:  Used to fix the axis minimum. The minimum will be auto-scaled if the value is null.</param>
            /// <param name="YAxisMax">OPTIONAL:  Used to fix the axis maximum. The maximum will be auto-scaled if the value is null.</param>
            /// <param name="YAxisInterval">OPTIONAL:  Used to set the tick mark interval for the axis. The interval will be auto-scaled if the value is null.</param>
            /// <param name="YAxisShowGridLines">OPTIONAL:  Flag to indicate whether grid lines should be displayed for this axis.</param>
            /// <param name="YAxisFontSize">OPTIONAL:  Sets the font size for this axis (must be >= 1). If left null, the chart will retain the default font sizes.</param>
            public static void FormatLineChart(System.Windows.Forms.DataVisualization.Charting.Chart ChartToFormat, string DataSeriesName = "", Nullable<System.Drawing.Color> DataSeriesColor = null, Nullable<int> DataSeriesLineThickness = null, Nullable<System.Windows.Forms.DataVisualization.Charting.ChartDashStyle> DataSeriesLineStyle = default(System.Windows.Forms.DataVisualization.Charting.ChartDashStyle?), bool HideDots = false, Nullable<System.Drawing.Color> BackgroundColor = null, System.Windows.Forms.DataVisualization.Charting.GradientStyle BackgroundGradient = System.Windows.Forms.DataVisualization.Charting.GradientStyle.None, bool HideLegend = false, bool XAxisShowGridLines = false, Nullable<int> XAxisFontSize = null, Nullable<double> YAxisMin = default(Double?), Nullable<double> YAxisMax = default(Double?), Nullable<double> YAxisInterval = default(Double?), bool YAxisShowGridLines = true, Nullable<int> YAxisFontSize = null)
            {
                // Check if on main
                if (ChartToFormat.InvokeRequired == true)
                {
                    // If not on main, invoke main
                    ChartToFormat.Invoke((Action)(() => FormatLineChart(ChartToFormat, DataSeriesName: DataSeriesName, DataSeriesColor: DataSeriesColor, DataSeriesLineThickness: DataSeriesLineThickness, DataSeriesLineStyle: DataSeriesLineStyle, HideDots: HideDots, BackgroundColor: BackgroundColor, BackgroundGradient: BackgroundGradient, HideLegend: HideLegend, XAxisShowGridLines: XAxisShowGridLines, XAxisFontSize: XAxisFontSize, YAxisMin: YAxisMin, YAxisMax: YAxisMax, YAxisInterval: YAxisInterval, YAxisShowGridLines: YAxisShowGridLines, YAxisFontSize: YAxisFontSize)));
                }
                else
                {
                    // Clear existing info
                    ClearChart(ChartToFormat);

                    // Create new chart area from user inputs
                    var ChartArea = CreateChartArea(null, null, null, XAxisShowGridLines, XAxisFontSize, YAxisMin, YAxisMax, YAxisInterval, YAxisShowGridLines, YAxisFontSize, BackgroundColor, BackgroundGradient);

                    // Add area to chart
                    ChartToFormat.ChartAreas.Add(ChartArea);

                    // Set background color if there was one
                    if (BackgroundColor != null) ChartToFormat.BackColor = (System.Drawing.Color)BackgroundColor;

                    // Create a new line series to hold all chart formating options
                    var NewSeries = new System.Windows.Forms.DataVisualization.Charting.Series();
                    NewSeries.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
                    NewSeries.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
                    NewSeries.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;

                    // Set series name if one was passed in
                    if (string.IsNullOrEmpty(DataSeriesName) == false)
                    {
                        NewSeries.Name = DataSeriesName;
                        NewSeries.LegendText = DataSeriesName;
                    }

                    // Set series color if one was passed in
                    if (DataSeriesColor != null) NewSeries.Color = (System.Drawing.Color)DataSeriesColor;

                    // Check if a valid line thickness was passed in
                    if (DataSeriesLineThickness != null && DataSeriesLineThickness >= 1)
                    {
                        // If good, set it and scale dots too (if dots are hidden, it won't matter)
                        NewSeries.BorderWidth = Convert.ToInt32(DataSeriesLineThickness);
                        NewSeries.MarkerSize = NewSeries.BorderWidth * 2 + 2;

                        // Ensure marker width is always at least 8 or it looks too small
                        if (NewSeries.MarkerSize < 8) NewSeries.MarkerSize = 8;
                    }

                    // Check if user specified a line style
                    if (DataSeriesLineStyle != null) NewSeries.BorderDashStyle = (System.Windows.Forms.DataVisualization.Charting.ChartDashStyle)DataSeriesLineStyle;

                    // If showing dots, add circles to datapoints
                    if (HideDots == false) NewSeries.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;

                    // Add series to the chart
                    ChartToFormat.Series.Add(NewSeries);

                    // Hide legend if requested
                    FormatLegend(ChartToFormat, !HideLegend);

                    // Draw the chart
                    ChartToFormat.Invalidate();
                }
            }

            /// <summary>
            /// Plots passed in data as a vertical or horizontal bar chart on a user specified chart control.
            /// </summary>
            /// <param name="ChartToPopulate">The chart control to plot the data on.</param>
            /// <param name="BarNamesAndValues">A list of bar names and their respective values to plot.</param>
            /// <param name="PlotBarsVertically">Indicates the type of bar chart to plot. True will plot a vertical / column bar chart. False will plot a horizontal / row bar chart.</param>
            /// <param name="BarColor">OPTIONAL:  Used to specify the color to use when painting the bars.</param>
            /// <param name="BarColorGradient">OPTIONAL:  Used to specify any color gradient effects you would like applied to the bars.</param>
            /// <param name="BackgroundColor">OPTIONAL:  Used to specify the background color that will be visible on the chart.</param>
            /// <param name="BackgroundGradient">OPTIONAL:  Used to specify any color gradient effects you would like applied to chart background.</param>
            /// <param name="ShowNumericGridLines">OPTIONAL:  Flag to indicate whether grid lines should be displayed on the numeric axis.</param>
            /// <param name="NumericAxisMin">OPTIONAL:  Used to fix the numeric axis minimum. The minimum will be auto-scaled if the value is null.</param>
            /// <param name="NumericAxisMax">OPTIONAL:  Used to fix the numeric axis maximum. The maximum will be auto-scaled if the value is null.</param>
            /// <param name="NumericAxisInterval">OPTIONAL:  Used to set the tick mark interval for the numeric axis. The interval will be auto-scaled if the value is null.</param>
            /// <param name="FontSizeBarNames">OPTIONAL:  Sets the font size for the bar names (must be >= 1). If left null, the chart will retain the default font sizes.</param>
            /// <param name="FontSizeNumericAxis">OPTIONAL:  Sets the font size for the numeric axis (must be >= 1). If left null, the chart will retain the default font sizes.</param>
            public static void PopulateBarChart(System.Windows.Forms.DataVisualization.Charting.Chart ChartToPopulate, List<KeyValuePair<string, double>> BarNamesAndValues, bool PlotBarsVertically, Nullable<System.Drawing.Color> BarColor = null, System.Windows.Forms.DataVisualization.Charting.GradientStyle BarColorGradient = System.Windows.Forms.DataVisualization.Charting.GradientStyle.None, Nullable<System.Drawing.Color> BackgroundColor = null, System.Windows.Forms.DataVisualization.Charting.GradientStyle BackgroundGradient = System.Windows.Forms.DataVisualization.Charting.GradientStyle.None, bool ShowNumericGridLines = true, Nullable<double> NumericAxisMin = 0, Nullable<double> NumericAxisMax = default(Double?), Nullable<double> NumericAxisInterval = default(Double?), Nullable<int> FontSizeBarNames = null, Nullable<int> FontSizeNumericAxis = null)
            {
                // Create list to hold bar color to set
                List<System.Drawing.Color> BarColors = null;

                // If color was specified, convert to list for common method
                if (BarColor != null)
                {
                    BarColors = new[] { BarColor.Value }.ToList();
                }

                // Use common method
                PopulateBarChartHelper(ChartToPopulate, new[] { BarNamesAndValues }, null, PlotBarsVertically, BarColors, new[] { BarColorGradient }.ToList(), BackgroundColor, BackgroundGradient, ShowNumericGridLines, NumericAxisMin, NumericAxisMax, NumericAxisInterval, FontSizeBarNames, FontSizeNumericAxis);
            }
            /// <summary>
            /// Plots passed in data as a vertical or horizontal bar chart on a user specified chart control.
            /// </summary>
            /// <param name="ChartToPopulate">The chart control to plot the data on.</param>
            /// <param name="AllBarNamesAndValuesSeries">A an array of lists of bar names and their respective values to plot (will generate side-by-side bars).</param>
            /// <param name="PlotBarsVertically">Indicates the type of bar chart to plot. True will plot a vertical / column bar chart. False will plot a horizontal / row bar chart.</param>
            /// <param name="AllBarSeriesNames">OPTIONAL:  The names of each series to display in the chart's legend.</param>
            /// <param name="BarColors">OPTIONAL:  Used to specify the colors to use when painting each bar series.</param>
            /// <param name="BarColorGradients">OPTIONAL:  Used to specify any color gradient effects you would like applied to each bar series.</param>
            /// <param name="BackgroundColor">OPTIONAL:  Used to specify the background color that will be visible on the chart.</param>
            /// <param name="BackgroundGradient">OPTIONAL:  Used to specify any color gradient effects you would like applied to chart background.</param>
            /// <param name="ShowNumericGridLines">OPTIONAL:  Flag to indicate whether grid lines should be displayed on the numeric axis.</param>
            /// <param name="NumericAxisMin">OPTIONAL:  Used to fix the numeric axis minimum. The minimum will be auto-scaled if the value is null.</param>
            /// <param name="NumericAxisMax">OPTIONAL:  Used to fix the numeric axis maximum. The maximum will be auto-scaled if the value is null.</param>
            /// <param name="NumericAxisInterval">OPTIONAL:  Used to set the tick mark interval for the numeric axis. The interval will be auto-scaled if the value is null.</param>
            /// <param name="FontSizeBarNames">OPTIONAL:  Sets the font size for the bar names (must be >= 1). If left null, the chart will retain the default font sizes.</param>
            /// <param name="FontSizeNumericAxis">OPTIONAL:  Sets the font size for the numeric axis (must be >= 1). If left null, the chart will retain the default font sizes.</param>
            public static void PopulateBarChart(System.Windows.Forms.DataVisualization.Charting.Chart ChartToPopulate, List<KeyValuePair<string, double>>[] AllBarNamesAndValuesSeries, bool PlotBarsVertically, List<string> AllBarSeriesNames = null, List<System.Drawing.Color> BarColors = null, List<System.Windows.Forms.DataVisualization.Charting.GradientStyle> BarColorGradients = null, Nullable<System.Drawing.Color> BackgroundColor = null, System.Windows.Forms.DataVisualization.Charting.GradientStyle BackgroundGradient = System.Windows.Forms.DataVisualization.Charting.GradientStyle.None, bool ShowNumericGridLines = true, Nullable<double> NumericAxisMin = 0, Nullable<double> NumericAxisMax = default(Double?), Nullable<double> NumericAxisInterval = default(Double?), Nullable<int> FontSizeBarNames = null, Nullable<int> FontSizeNumericAxis = null)
            {
                // Use common method
                PopulateBarChartHelper(ChartToPopulate, AllBarNamesAndValuesSeries, AllBarSeriesNames, PlotBarsVertically, BarColors, BarColorGradients, BackgroundColor, BackgroundGradient, ShowNumericGridLines, NumericAxisMin, NumericAxisMax, NumericAxisInterval, FontSizeBarNames, FontSizeNumericAxis);
            }
            /// <summary>
            /// Plots passed in data as a vertical or horizontal bar chart on a user specified chart control.
            /// </summary>
            /// <param name="ChartToPopulate">The chart control to plot the data on.</param>
            /// <param name="DataSeries">A list of XY values to plot on the bar chart.</param>
            /// <param name="PlotBarsVertically">Indicates the type of bar chart to plot. True will plot a vertical / column bar chart. False will plot a horizontal / row bar chart.</param>
            /// <param name="BarColor">OPTIONAL:  Used to specify the color to use when painting the bars.</param>
            /// <param name="BarColorGradient">OPTIONAL:  Used to specify any color gradient effects you would like applied to the bars.</param>
            /// <param name="BackgroundColor">OPTIONAL:  Used to specify the background color that will be visible on the chart.</param>
            /// <param name="BackgroundGradient">OPTIONAL:  Used to specify any color gradient effects you would like applied to chart background.</param>
            /// <param name="IndependentAxisMin">OPTIONAL:  Used to fix the independent (X) axis minimum. The minimum will be auto-scaled if the value is null.</param>
            /// <param name="IndependentAxisMax">OPTIONAL:  Used to fix the independent (X) axis maximum. The maximum will be auto-scaled if the value is null.</param>
            /// <param name="IndependentAxisInterval">OPTIONAL:  Used to set the tick mark interval for the independent (X) axis. The interval will be auto-scaled if the value is null.</param>
            /// <param name="DependentAxisMin">OPTIONAL:  Used to fix the dependent (Y) axis minimum. The minimum will be auto-scaled if the value is null.</param>
            /// <param name="DependentAxisMax">OPTIONAL:  Used to fix the dependent (Y) axis maximum. The maximum will be auto-scaled if the value is null.</param>
            /// <param name="DependentAxisInterval">OPTIONAL:  Used to set the tick mark interval for the dependent (Y) axis. The interval will be auto-scaled if the value is null.</param>
            /// <param name="ShowDependentAxisGridLines">OPTIONAL:  Flag to indicate whether grid lines should be displayed on the dependent (Y) axis.</param>
            /// <param name="IndependentAxisFontSize">OPTIONAL:  Sets the font size for the bar names (must be >= 1). If left null, the chart will retain the default font sizes.</param>
            /// <param name="DependentAxisFontSize">OPTIONAL:  Sets the font size for the numeric axis (must be >= 1). If left null, the chart will retain the default font sizes.</param>
            public static void PopulateBarChart(System.Windows.Forms.DataVisualization.Charting.Chart ChartToPopulate, List<KeyValuePair<double, double>> DataSeries, bool PlotBarsVertically, Nullable<System.Drawing.Color> BarColor = null, System.Windows.Forms.DataVisualization.Charting.GradientStyle BarColorGradient = System.Windows.Forms.DataVisualization.Charting.GradientStyle.None, Nullable<System.Drawing.Color> BackgroundColor = null, System.Windows.Forms.DataVisualization.Charting.GradientStyle BackgroundGradient = System.Windows.Forms.DataVisualization.Charting.GradientStyle.None, Nullable<double> IndependentAxisMin = 0, Nullable<double> IndependentAxisMax = default(Double?), Nullable<double> IndependentAxisInterval = default(Double?), Nullable<double> DependentAxisMin = 0, Nullable<double> DependentAxisMax = default(Double?), Nullable<double> DependentAxisInterval = default(Double?), bool ShowDependentAxisGridLines = true, Nullable<int> IndependentAxisFontSize = null, Nullable<int> DependentAxisFontSize = null)
            {
                // Create list to hold bar color to set
                List<System.Drawing.Color> BarColors = null;

                // If color was specified, convert to list for common method
                if (BarColor != null)
                {
                    BarColors = new[] { BarColor.Value }.ToList();
                }

                // Use common method
                PopulateBarChartHelper(ChartToPopulate, new[] { DataSeries }, null, PlotBarsVertically, BarColors, new[] { BarColorGradient }.ToList(), BackgroundColor, BackgroundGradient, ShowDependentAxisGridLines, DependentAxisMin, DependentAxisMax, DependentAxisInterval, IndependentAxisFontSize, DependentAxisFontSize);
            }

            /// <summary>
            /// Plots passed in data as a vertical stacked bar chart on a user specified chart control.
            /// </summary>
            /// <param name="ChartToPopulate">The chart control to plot the data on.</param>
            /// <param name="BarNamesAndValues">An array of all data series / bars to plot.</param>
            /// <param name="AllBarSeriesNames">OPTIONAL:  A list of series names to display in the chart's legend.</param>
            /// <param name="BarColors">OPTIONAL:  Used to specify the colors to use when painting the stacked bars.</param>
            /// <param name="BarColorGradient">OPTIONAL:  Used to specify any color gradient effects you would like applied to the bars.</param>
            /// <param name="BackgroundColor">OPTIONAL:  Used to specify the background color that will be visible on the chart.</param>
            /// <param name="BackgroundGradient">OPTIONAL:  Used to specify any color gradient effects you would like applied to chart background.</param>
            /// <param name="ShowNumericGridLines">OPTIONAL:  Flag to indicate whether grid lines should be displayed on the numeric axis.</param>
            /// <param name="NumericAxisMin">OPTIONAL:  Used to fix the numeric axis minimum. The minimum will be auto-scaled if the value is null.</param>
            /// <param name="NumericAxisMax">OPTIONAL:  Used to fix the numeric axis maximum. The maximum will be auto-scaled if the value is null.</param>
            /// <param name="NumericAxisInterval">OPTIONAL:  Used to set the tick mark interval for the numeric axis. The interval will be auto-scaled if the value is null.</param>
            /// <param name="FontSizeBarNames">OPTIONAL:  Sets the font size for the bar names (must be >= 1). If left null, the chart will retain the default font sizes.</param>
            /// <param name="FontSizeNumericAxis">OPTIONAL:  Sets the font size for the numeric axis (must be >= 1). If left null, the chart will retain the default font sizes.</param>
            public static void PopulateStackedBarChart(System.Windows.Forms.DataVisualization.Charting.Chart ChartToPopulate, List<KeyValuePair<string, double>>[] BarNamesAndValues, List<string> AllBarSeriesNames = null, List<System.Drawing.Color> BarColors = null, System.Windows.Forms.DataVisualization.Charting.GradientStyle BarColorGradient = System.Windows.Forms.DataVisualization.Charting.GradientStyle.None, Nullable<System.Drawing.Color> BackgroundColor = null, System.Windows.Forms.DataVisualization.Charting.GradientStyle BackgroundGradient = System.Windows.Forms.DataVisualization.Charting.GradientStyle.None, bool ShowNumericGridLines = true, Nullable<double> NumericAxisMin = 0, Nullable<double> NumericAxisMax = default(Double?), Nullable<double> NumericAxisInterval = default(Double?), Nullable<int> FontSizeBarNames = null, Nullable<int> FontSizeNumericAxis = null)
            {
                // Check if on main
                if (ChartToPopulate.InvokeRequired == true)
                {
                    // If not on main, invoke main
                    ChartToPopulate.Invoke((Action)(() => PopulateStackedBarChart(ChartToPopulate, BarNamesAndValues, AllBarSeriesNames: AllBarSeriesNames, BarColors: BarColors, BackgroundColor: BackgroundColor, BackgroundGradient: BackgroundGradient, ShowNumericGridLines: ShowNumericGridLines, NumericAxisMin: NumericAxisMin, NumericAxisMax: NumericAxisMax, NumericAxisInterval: NumericAxisInterval, FontSizeBarNames: FontSizeBarNames, FontSizeNumericAxis: FontSizeNumericAxis)));
                }
                else
                {
                    // Clear existing info
                    ClearChart(ChartToPopulate);

                    // Create new chart area from user inputs (bar charts should always use 1 for name axis so all bars are named)
                    var ChartArea = CreateChartArea(null, null, 1, false, FontSizeBarNames, NumericAxisMin, NumericAxisMax, NumericAxisInterval, ShowNumericGridLines, FontSizeNumericAxis, BackgroundColor, BackgroundGradient);

                    // Add area to chart
                    ChartToPopulate.ChartAreas.Add(ChartArea);

                    // Set background color if there was one
                    if (BackgroundColor != null) ChartToPopulate.BackColor = (System.Drawing.Color)BackgroundColor;

                    // Loop through and create each series
                    foreach (var DATASERIES in BarNamesAndValues)
                    {
                        // Save the index of this series
                        int Index = Array.IndexOf(BarNamesAndValues, DATASERIES);

                        // Create the series (show outlines around bars)
                        var NewSeries = new System.Windows.Forms.DataVisualization.Charting.Series();
                        NewSeries.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
                        NewSeries.BorderColor = System.Drawing.Color.Black;
                        NewSeries.BorderWidth = 2;

                        // Set the chart type
                        NewSeries.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StackedColumn;

                        // Check if user passed in name for this index
                        if (AllBarSeriesNames != null && AllBarSeriesNames.Count - 1 >= Index)
                        {
                            // If so, set name
                            NewSeries.Name = AllBarSeriesNames[Index];
                            NewSeries.LegendText = AllBarSeriesNames[Index];
                        }

                        // Set color if requested
                        if (BarColors != null && BarColors.Count - 1 >= Index)
                        {
                            NewSeries.Color = (System.Drawing.Color)BarColors[Index];
                        }

                        // Set gradient style
                        NewSeries.BackGradientStyle = BarColorGradient;

                        // Save data types
                        NewSeries.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.String;
                        NewSeries.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;

                        // Create vars to hold x & y vals
                        var XVals = new List<string>();
                        var YVals = new List<double>();

                        // Loop through and store values
                        foreach (KeyValuePair<string, double> PAIR in DATASERIES)
                        {
                            XVals.Add(PAIR.Key);
                            YVals.Add(PAIR.Value);
                        }

                        // Bind data to series
                        NewSeries.Points.DataBindXY(XVals, YVals);

                        // Add series to the chart
                        ChartToPopulate.Series.Add(NewSeries);
                    }

                    // Check if there were series names
                    if (AllBarSeriesNames != null && AllBarSeriesNames.Count > 0)
                    {
                        // If so, show legend
                        FormatLegend(ChartToPopulate, true);
                    }
                    else
                    {
                        // If not, hide legend
                        FormatLegend(ChartToPopulate, false);
                    }

                    // Draw the chart
                    ChartToPopulate.Invalidate();
                }
            }

            /// <summary>
            /// Plots passed in data as a run chart on a user specified chart control.
            /// </summary>
            /// <param name="ChartToPopulate">The chart control to plot the data on.</param>
            /// <param name="PieSliceNamesAndCounts">A list of pie slice names and their respective values to plot.</param>
            /// <param name="PieColorGradient">OPTIONAL:  Used to specify any color gradient effects you would like applied to the pie slices.</param>
            /// <param name="BackgroundColor">OPTIONAL:  Used to specify the background color that will be visible on the chart.</param>
            /// <param name="BackgroundGradient">OPTIONAL:  Used to specify any color gradient effects you would like applied to chart background.</param>
            /// <param name="ChartFontSize">OPTIONAL:  Sets the font size for chart legend / pie slice names (must be >= 1). If left null, the chart will retain the default font sizes.</param>
            public static void PopulatePieChart(System.Windows.Forms.DataVisualization.Charting.Chart ChartToPopulate, List<KeyValuePair<string, double>> PieSliceNamesAndCounts, System.Windows.Forms.DataVisualization.Charting.GradientStyle PieColorGradient = System.Windows.Forms.DataVisualization.Charting.GradientStyle.None, Nullable<System.Drawing.Color> BackgroundColor = null, System.Windows.Forms.DataVisualization.Charting.GradientStyle BackgroundGradient = System.Windows.Forms.DataVisualization.Charting.GradientStyle.None, Nullable<int> ChartFontSize = null)
            {
                // Check if on main
                if (ChartToPopulate.InvokeRequired == true)
                {
                    // If not on main, invoke main
                    ChartToPopulate.Invoke((Action)(() => PopulatePieChart(ChartToPopulate, PieSliceNamesAndCounts, PieColorGradient: PieColorGradient, BackgroundColor: BackgroundColor, BackgroundGradient: BackgroundGradient, ChartFontSize: ChartFontSize)));
                }
                else
                {
                    // Clear existing info
                    ClearChart(ChartToPopulate);

                    // Create new chart area from user inputs
                    var ChartArea = CreateChartArea(null, null, null, false, ChartFontSize, null, null, null, false, ChartFontSize, BackgroundColor, BackgroundGradient);

                    // Add area to chart
                    ChartToPopulate.ChartAreas.Add(ChartArea);

                    // Set background color if there was one
                    if (BackgroundColor != null)
                    {
                        ChartToPopulate.BackColor = (System.Drawing.Color)BackgroundColor;
                    }

                    // Create the series (show outlines around bars)
                    var NewSeries = new System.Windows.Forms.DataVisualization.Charting.Series();
                    NewSeries.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
                    NewSeries.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
                    NewSeries.BorderColor = System.Drawing.Color.Black;
                    NewSeries.BorderWidth = 2;

                    // Set gradient style
                    NewSeries.BackGradientStyle = PieColorGradient;

                    // Set font if one was passed in
                    if (ChartFontSize != null)
                    {
                        NewSeries.Font = new System.Drawing.Font("Tahoma", Convert.ToSingle(ChartFontSize), System.Drawing.FontStyle.Bold);
                    }

                    // Save data types
                    NewSeries.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.String;
                    NewSeries.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;

                    // Create vars to hold x & y vals
                    var XVals = new List<string>();
                    var YVals = new List<double>();

                    // Loop through and store values
                    foreach (KeyValuePair<string, double> PAIR in PieSliceNamesAndCounts)
                    {
                        XVals.Add(PAIR.Key);
                        YVals.Add(PAIR.Value);
                    }

                    // Bind data to series
                    NewSeries.Points.DataBindXY(XVals, YVals);

                    // Add series to the chart
                    ChartToPopulate.Series.Add(NewSeries);

                    // Hide legend (slices are auto-named by chart)
                    FormatLegend(ChartToPopulate, false);

                    // Draw the chart
                    ChartToPopulate.Invalidate();
                }
            }

            /// <summary>
            /// Adds a line series to a chart that may or may not have existing data on it.
            /// </summary>
            /// <param name="ChartToPopulate">The chart to add the series to.</param>
            /// <param name="DataPoints">The list of key value pair datapoints to add.</param>
            /// <param name="SeriesName">OPTIONAL:  The name of the series to display in the chart legend if the legend is visible.</param>
            /// <param name="HideDots">OPTIONAL:  Boolean to indicate if the dots on the series should be visible (note that if IsFastLine is true, the dots will never be visible).</param>
            /// <param name="SeriesColor">OPTIONAL:  The color to use when plotting the series.</param>
            /// <param name="SeriesLineThickness">OPTIONAL:  Used to specify how thick to draw the line.</param>
            /// <param name="SeriesLineStyle">OPTIONAL:  The line style to use when drawing the series (solid line, dotted line, etc.).</param>
            /// <param name="IsFastLine">OPTIONAL:  Boolean to indicate if the series should use fastline plotting (i.e. for large data sets). Dots will never be visible if fastline is used.</param>
            public static void AddLineSeriesToChart(System.Windows.Forms.DataVisualization.Charting.Chart ChartToPopulate, List<KeyValuePair<string, double>> DataPoints, string SeriesName = "", bool HideDots = false, Nullable<System.Drawing.Color> SeriesColor = null, Nullable<UInt16> SeriesLineThickness = null, System.Windows.Forms.DataVisualization.Charting.ChartDashStyle SeriesLineStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid, bool IsFastLine = false)
            {
                // Use common method
                AddLineSeriesToChartCommon(ChartToPopulate, DataPoints, SeriesName, HideDots, SeriesColor, SeriesLineThickness, SeriesLineStyle, IsFastLine);
            }
            /// <summary>
            /// Adds a line series to a chart that may or may not have existing data on it.
            /// </summary>
            /// <param name="ChartToPopulate">The chart to add the series to.</param>
            /// <param name="DataPoints">The list of key value pair datapoints to add.</param>
            /// <param name="SeriesName">OPTIONAL:  The name of the series to display in the chart legend if the legend is visible.</param>
            /// <param name="HideDots">OPTIONAL:  Boolean to indicate if the dots on the series should be visible (note that if IsFastLine is true, the dots will never be visible).</param>
            /// <param name="SeriesColor">OPTIONAL:  The color to use when plotting the series.</param>
            /// <param name="SeriesLineThickness">OPTIONAL:  Used to specify how thick to draw the line.</param>
            /// <param name="SeriesLineStyle">OPTIONAL:  The line style to use when drawing the series (solid line, dotted line, etc.).</param>
            /// <param name="IsFastLine">OPTIONAL:  Boolean to indicate if the series should use fastline plotting (i.e. for large data sets). Dots will never be visible if fastline is used.</param>
            public static void AddLineSeriesToChart(System.Windows.Forms.DataVisualization.Charting.Chart ChartToPopulate, List<KeyValuePair<double, double>> DataPoints, string SeriesName = "", bool HideDots = false, Nullable<System.Drawing.Color> SeriesColor = null, Nullable<UInt16> SeriesLineThickness = null, System.Windows.Forms.DataVisualization.Charting.ChartDashStyle SeriesLineStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid, bool IsFastLine = false)
            {
                // Use common method
                AddLineSeriesToChartCommon(ChartToPopulate, DataPoints, SeriesName, HideDots, SeriesColor, SeriesLineThickness, SeriesLineStyle, IsFastLine);
            }

            /// <summary>
            /// Used to remove the spacing between bars on a bar chart.
            /// </summary>
            /// <param name="ChartToFormat">The bar chart to remove the bar spacing from.</param>
            public static void RemoveSpacingFromBarChart(System.Windows.Forms.DataVisualization.Charting.Chart ChartToFormat)
            {
                // Bail if there is no chart
                if (ChartToFormat == null) return;

                // If we have a chart, check if we're on the main thread
                if (ChartToFormat.InvokeRequired == true)
                {
                    // If not, invoke main
                    ChartToFormat.Invoke((Action)(() => RemoveSpacingFromBarChart(ChartToFormat)));
                }
                else
                {
                    // If we're on the main, bail if there's no series
                    if (ChartToFormat.Series == null || ChartToFormat.Series.Count == 0) return;

                    // If we have series, loop through and remove spacing
                    foreach (var SERIES in ChartToFormat.Series)
                    {
                        SERIES["PointWidth"] = "1";
                    }
                }
            }

            /// <summary>
            /// Used to set the visibility of a specified axis's labels.
            /// </summary>
            /// <param name="ChartToFormat">The chart object to format.</param>
            /// <param name="AxisToFormat">The axis to apply the formatting to.</param>
            /// <param name="IsVisible">Flag to indicate if the axis labels are visible or hidden.</param>
            public static void SetAxisLabelVisibility(System.Windows.Forms.DataVisualization.Charting.Chart ChartToFormat, AxisOptions AxisToFormat, bool IsVisible)
            {
                // Check if on main thread
                if (ChartToFormat.InvokeRequired == true)
                {
                    // If not, invoke main
                    ChartToFormat.Invoke((Action)(() => SetAxisLabelVisibility(ChartToFormat, AxisToFormat, IsVisible)));
                }
                else
                {
                    // Bail if chart is missing chart area (nothing to set)
                    if (ChartToFormat.ChartAreas == null || ChartToFormat.ChartAreas.Count == 0) return;

                    // If we have a chart area, create style to hold formatting info
                    var StyleToUse = new System.Windows.Forms.DataVisualization.Charting.LabelStyle();
                    StyleToUse.Enabled = IsVisible;

                    // Loop through all and set visibility
                    foreach (var AREA in ChartToFormat.ChartAreas)
                    {
                        // Key off axis type
                        switch (AxisToFormat)
                        {
                            case AxisOptions.X:
                                // Format the x axis
                                AREA.AxisX.LabelStyle = StyleToUse;
                                break;
                            case AxisOptions.Y:
                                // Format the y axis
                                AREA.AxisY.LabelStyle = StyleToUse;
                                break;
                        }
                    }
                }
            }

            /// <summary>
            /// Used to set the color of the font used on a specified chart axis.
            /// </summary>
            /// <param name="ChartToFormat">The chart object to format.</param>
            /// <param name="AxisToFormat">The axis to apply the formatting to.</param>
            /// <param name="ColorToSet">The color to set for the axis text.</param>
            public static void SetAxisFontColor(System.Windows.Forms.DataVisualization.Charting.Chart ChartToFormat, AxisOptions AxisToFormat, System.Drawing.Color ColorToSet)
            {
                // Check if on main thread
                if (ChartToFormat.InvokeRequired == true)
                {
                    // If not, invoke main
                    ChartToFormat.Invoke((Action)(() => SetAxisFontColor(ChartToFormat, AxisToFormat, ColorToSet)));
                }
                else
                {
                    // Bail if chart is missing chart area (nothing to set)
                    if (ChartToFormat.ChartAreas == null || ChartToFormat.ChartAreas.Count == 0) return;

                    // If we have a chart area, create style to hold formatting info
                    var StyleToUse = new System.Windows.Forms.DataVisualization.Charting.LabelStyle();
                    StyleToUse.ForeColor = ColorToSet;

                    // Loop through all and set color
                    foreach (var AREA in ChartToFormat.ChartAreas)
                    {
                        // Key off axis type
                        switch (AxisToFormat)
                        {
                            case AxisOptions.X:
                                // Format the x axis
                                AREA.AxisX.LabelStyle = StyleToUse;
                                break;
                            case AxisOptions.Y:
                                // Format the y axis
                                AREA.AxisY.LabelStyle = StyleToUse;
                                break;
                        }
                    }
                }
            }

            /// <summary>
            /// Clears any data series currently displayed on a chart control.
            /// </summary>
            /// <param name="ChartToClear">The chart control to clear.</param>
            public static void ClearChart(System.Windows.Forms.DataVisualization.Charting.Chart ChartToClear)
            {
                // Check if on main thread
                if (ChartToClear.InvokeRequired == true)
                {
                    // If not, invoke main
                    ChartToClear.Invoke((Action)(() => ClearChart(ChartToClear)));
                }
                else
                {
                    // If on main, clear chart info
                    ChartToClear.ChartAreas.Clear();
                    ChartToClear.Series.Clear();
                }
            }

            /// <summary>
            /// Common method for populate line charts with KVP lists.
            /// </summary>
            private static void PopulateLineChartCommon(System.Windows.Forms.DataVisualization.Charting.Chart ChartToPopulate, IList[] AllDataSeries, List<string> AllDataSeriesNames, List<System.Drawing.Color> AllDataSeriesColors, List<int> AllDataSeriesLineThicknesses, List<System.Windows.Forms.DataVisualization.Charting.ChartDashStyle> AllDataSeriesLineStyles, bool HideDots, Nullable<System.Drawing.Color> BackgroundColor, System.Windows.Forms.DataVisualization.Charting.GradientStyle BackgroundGradient, bool HideLegend, Nullable<double> XAxisMin, Nullable<double> XAxisMax, Nullable<double> XAxisInterval, bool XAxisShowGridLines, Nullable<int> XAxisFontSize, Nullable<double> YAxisMin, Nullable<double> YAxisMax, Nullable<double> YAxisInterval, bool YAxisShowGridLines, Nullable<int> YAxisFontSize, bool IsFastLine)
            {
                // Check if on main
                if (ChartToPopulate.InvokeRequired == true)
                {
                    // If not on main, invoke main
                    ChartToPopulate.Invoke((Action)(() => PopulateLineChartCommon(ChartToPopulate, AllDataSeries, AllDataSeriesNames, AllDataSeriesColors, AllDataSeriesLineThicknesses, AllDataSeriesLineStyles, HideDots, BackgroundColor, BackgroundGradient, HideLegend, XAxisMin, XAxisMax, XAxisInterval, XAxisShowGridLines, XAxisFontSize, YAxisMin, YAxisMax, YAxisInterval, YAxisShowGridLines, YAxisFontSize, IsFastLine)));
                }
                else
                {
                    // Clear existing info
                    ClearChart(ChartToPopulate);

                    // Create new chart area from user inputs
                    var ChartArea = CreateChartArea(XAxisMin, XAxisMax, XAxisInterval, XAxisShowGridLines, XAxisFontSize, YAxisMin, YAxisMax, YAxisInterval, YAxisShowGridLines, YAxisFontSize, BackgroundColor, BackgroundGradient);

                    // Add area to chart
                    ChartToPopulate.ChartAreas.Add(ChartArea);

                    // Set background color if there was one
                    if (BackgroundColor != null)
                    {
                        ChartToPopulate.BackColor = (System.Drawing.Color)BackgroundColor;
                    }

                    // Loop through and create all data series
                    int CurrentSeriesIndex = 0;
                    foreach (var SERIES in AllDataSeries)
                    {
                        // Create data series
                        var NewSeries = new System.Windows.Forms.DataVisualization.Charting.Series();
                        NewSeries.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
                        NewSeries.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Auto;
                        NewSeries.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;

                        // Switch to fastline if requested
                        if (IsFastLine == true)
                        {
                            NewSeries.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
                        }

                        // Check if user passed in name for this index
                        if (AllDataSeriesNames != null && AllDataSeriesNames.Count - 1 >= CurrentSeriesIndex)
                        {
                            // If so, set name
                            NewSeries.Name = AllDataSeriesNames[CurrentSeriesIndex];
                            NewSeries.LegendText = AllDataSeriesNames[CurrentSeriesIndex];
                        }

                        // Check if user passed in color for this index
                        if (AllDataSeriesColors != null && AllDataSeriesColors.Count - 1 >= CurrentSeriesIndex)
                        {
                            // If so, set the color
                            NewSeries.Color = AllDataSeriesColors[CurrentSeriesIndex];
                        }

                        // Check if user passed in thickness for this index
                        if (AllDataSeriesLineThicknesses != null && AllDataSeriesLineThicknesses.Count - 1 >= CurrentSeriesIndex)
                        {
                            // If so, ensure it is a valid number
                            if (AllDataSeriesLineThicknesses[CurrentSeriesIndex] >= 1)
                            {
                                // If thickness is good, set it and scale dots too (if dots are hidden, it won't matter)
                                NewSeries.BorderWidth = AllDataSeriesLineThicknesses[CurrentSeriesIndex];
                                NewSeries.MarkerSize = NewSeries.BorderWidth * 2 + 2;

                                // Ensure marker width is always at least 8 or it looks too small
                                if (NewSeries.MarkerSize < 8)
                                {
                                    NewSeries.MarkerSize = 8;
                                }
                            }
                        }

                        // Check if user passed in line style for this index
                        if (AllDataSeriesLineStyles != null && AllDataSeriesLineStyles.Count - 1 >= CurrentSeriesIndex)
                        {
                            // If so, set the style
                            NewSeries.BorderDashStyle = AllDataSeriesLineStyles[CurrentSeriesIndex];
                        }

                        // If showing dots, add circles to datapoints
                        if (HideDots == false)
                        {
                            NewSeries.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
                        }

                        // Create vars to hold x & y vals
                        var XVals = new List<object>();
                        var YVals = new List<double>();

                        // Loop through and get values
                        foreach (var DPOINT in SERIES)
                        {
                            if (DPOINT is KeyValuePair<double, double>)
                            {
                                XVals.Add(((KeyValuePair<double, double>)DPOINT).Key);
                                YVals.Add(((KeyValuePair<double, double>)DPOINT).Value);
                            }
                            else if (DPOINT is KeyValuePair<string, double>)
                            {
                                XVals.Add(((KeyValuePair<string, double>)DPOINT).Key);
                                YVals.Add(((KeyValuePair<string, double>)DPOINT).Value);
                            }
                            else if (DPOINT is KeyValuePair<DateTime, double>)
                            {
                                XVals.Add(((KeyValuePair<DateTime, double>)DPOINT).Key);
                                YVals.Add(((KeyValuePair<DateTime, double>)DPOINT).Value);
                            }
                        }

                        // Bind data to series
                        NewSeries.Points.DataBindXY(XVals, YVals);

                        // Add series to the chart
                        ChartToPopulate.Series.Add(NewSeries);

                        // Inc index for next loop
                        CurrentSeriesIndex = CurrentSeriesIndex + 1;
                    }

                    // Check if there were series names
                    if (AllDataSeriesNames != null && AllDataSeriesNames.Count > 0)
                    {
                        // If so, show legend
                        FormatLegend(ChartToPopulate, true);
                    }
                    else
                    {
                        // If not, hide legend
                        FormatLegend(ChartToPopulate, false);
                    }

                    // Draw the chart
                    ChartToPopulate.Invalidate();
                }
            }
            /// <summary>
            /// Common method for populate bar charts with KVP lists.
            /// </summary>
            private static void PopulateBarChartHelper(System.Windows.Forms.DataVisualization.Charting.Chart ChartToPopulate, IList[] AllBarNamesAndValuesSeries, List<string> AllSeriesNames, bool PlotBarsVertically, List<System.Drawing.Color> BarColors, List<System.Windows.Forms.DataVisualization.Charting.GradientStyle> BarColorGradients, Nullable<System.Drawing.Color> BackgroundColor, System.Windows.Forms.DataVisualization.Charting.GradientStyle BackgroundGradient, bool ShowNumericGridLines, Nullable<double> NumericAxisMin, Nullable<double> NumericAxisMax, Nullable<double> NumericAxisInterval, Nullable<int> FontSizeBarNames, Nullable<int> FontSizeNumericAxis)
            {
                // Check if on main
                if (ChartToPopulate.InvokeRequired == true)
                {
                    // If not on main, invoke main
                    ChartToPopulate.Invoke((Action)(() => PopulateBarChartHelper(ChartToPopulate, AllBarNamesAndValuesSeries, AllSeriesNames, PlotBarsVertically, BarColors, BarColorGradients, BackgroundColor, BackgroundGradient, ShowNumericGridLines, NumericAxisMin, NumericAxisMax, NumericAxisInterval, FontSizeBarNames, FontSizeNumericAxis)));
                }
                else
                {
                    // Clear existing info
                    ClearChart(ChartToPopulate);

                    // Create new chart area from user inputs (bar charts should always use 1 for name axis so all bars are named)
                    var ChartArea = CreateChartArea(null, null, 1, false, FontSizeBarNames, NumericAxisMin, NumericAxisMax, NumericAxisInterval, ShowNumericGridLines, FontSizeNumericAxis, BackgroundColor, BackgroundGradient);

                    // Add area to chart
                    ChartToPopulate.ChartAreas.Add(ChartArea);

                    // Set background color if there was one
                    if (BackgroundColor != null)
                    {
                        ChartToPopulate.BackColor = (System.Drawing.Color)BackgroundColor;
                    }

                    // Loop through and create all data series
                    int CurrentSeriesIndex = 0;
                    foreach (var SERIES in AllBarNamesAndValuesSeries)
                    {
                        // Create the series (show outlines around bars)
                        var NewSeries = new System.Windows.Forms.DataVisualization.Charting.Series();
                        NewSeries.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
                        NewSeries.BorderColor = System.Drawing.Color.Black;
                        NewSeries.BorderWidth = 2;

                        // Check if user passed in name for this index
                        if (AllSeriesNames != null && AllSeriesNames.Count - 1 >= CurrentSeriesIndex)
                        {
                            // If so, set name
                            NewSeries.Name = AllSeriesNames[CurrentSeriesIndex];
                            NewSeries.LegendText = AllSeriesNames[CurrentSeriesIndex];
                        }

                        // Check if vertical or horizontal
                        if (PlotBarsVertically == true)
                        {
                            // Use column chart for vertical bars
                            NewSeries.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column;
                        }
                        else
                        {
                            // Use bar chart for horizontal bars
                            NewSeries.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Bar;
                        }

                        // Check if user passed in color for this series
                        if (BarColors != null && BarColors.Count - 1 >= CurrentSeriesIndex)
                        {
                            // If there, save color
                            NewSeries.Color = BarColors[CurrentSeriesIndex];
                        }

                        // Check if user passed in bar color gradient
                        if (BarColorGradients != null && BarColorGradients.Count - 1 >= CurrentSeriesIndex)
                        {
                            // Set gradient style
                            NewSeries.BackGradientStyle = BarColorGradients[CurrentSeriesIndex];
                        }

                        // Save data types
                        NewSeries.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Auto;
                        NewSeries.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;

                        // Create vars to hold x & y vals
                        var XVals = new List<object>();
                        var YVals = new List<double>();

                        // Loop through each series to plot
                        foreach (var PAIR in SERIES)
                        {
                            if (PAIR is KeyValuePair<double, double>)
                            {
                                XVals.Add(((KeyValuePair<double, double>)PAIR).Key);
                                YVals.Add(((KeyValuePair<double, double>)PAIR).Value);
                            }
                            else if (PAIR is KeyValuePair<string, double>)
                            {
                                XVals.Add(((KeyValuePair<string, double>)PAIR).Key);
                                YVals.Add(((KeyValuePair<string, double>)PAIR).Value);
                            }
                        }

                        // Bind data to series
                        NewSeries.Points.DataBindXY(XVals, YVals);

                        // Add series to the chart
                        ChartToPopulate.Series.Add(NewSeries);

                        // Inc index for next loop
                        CurrentSeriesIndex = CurrentSeriesIndex + 1;
                    }

                    // Check if there were series names
                    if (AllSeriesNames != null && AllSeriesNames.Count > 0)
                    {
                        // If so, show legend
                        FormatLegend(ChartToPopulate, true);
                    }
                    else
                    {
                        // If not, hide legend
                        FormatLegend(ChartToPopulate, false);
                    }

                    // Draw the chart
                    ChartToPopulate.Invalidate();
                }
            }
            private static void FormatLegend(System.Windows.Forms.DataVisualization.Charting.Chart ChartToUse, bool IsLegendVisible)
            {
                // Clear any existing legends first
                ChartToUse.Legends.Clear();

                // Exit if not showing a legend
                if (IsLegendVisible == false) return;

                // If showing, create new legend
                var Legend = new System.Windows.Forms.DataVisualization.Charting.Legend();
                Legend.Alignment = System.Drawing.StringAlignment.Center;
                Legend.BorderColor = System.Drawing.Color.Black;
                Legend.BorderWidth = 1;

                // Add legend to chart
                ChartToUse.Legends.Add(Legend);
            }

            /// <summary>
            /// Common method for adding an additional line series to a chart.
            /// </summary>
            private static void AddLineSeriesToChartCommon(System.Windows.Forms.DataVisualization.Charting.Chart ChartToPopulate, IList DataPoints, string SeriesName, bool HideDots, Nullable<System.Drawing.Color> SeriesColor, Nullable<UInt16> LineThickness, System.Windows.Forms.DataVisualization.Charting.ChartDashStyle SeriesLineStyle, bool IsFastLine)
            {
                // Check if on main
                if (ChartToPopulate.InvokeRequired == true)
                {
                    // If not on main, invoke main
                    ChartToPopulate.Invoke((Action)(() => AddLineSeriesToChartCommon(ChartToPopulate, DataPoints, SeriesName, HideDots, SeriesColor, LineThickness, SeriesLineStyle, IsFastLine)));
                }
                else
                {
                    // Create data series
                    var NewSeries = new System.Windows.Forms.DataVisualization.Charting.Series();
                    NewSeries.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
                    NewSeries.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Auto;
                    NewSeries.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;

                    // Switch to fastline if requested
                    if (IsFastLine == true)
                    {
                        NewSeries.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
                    }

                    // Set line style
                    NewSeries.BorderDashStyle = SeriesLineStyle;

                    // Set the series name
                    NewSeries.Name = SeriesName;
                    NewSeries.LegendText = SeriesName;

                    // Set the color
                    NewSeries.Color = (System.Drawing.Color)SeriesColor;

                    // If showing dots, add circles to datapoints
                    if (HideDots == false)
                    {
                        NewSeries.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
                    }

                    // Create vars to hold x & y vals
                    var XVals = new List<object>();
                    var YVals = new List<double>();

                    // Loop through and get values
                    foreach (var DPOINT in DataPoints)
                    {
                        if (DPOINT is KeyValuePair<double, double>)
                        {
                            XVals.Add(((KeyValuePair<double, double>)DPOINT).Key);
                            YVals.Add(((KeyValuePair<double, double>)DPOINT).Value);
                        }
                        else if (DPOINT is KeyValuePair<string, double>)
                        {
                            XVals.Add(((KeyValuePair<string, double>)DPOINT).Key);
                            YVals.Add(((KeyValuePair<string, double>)DPOINT).Value);
                        }
                    }

                    // Set thickness if specified
                    if (LineThickness != null && LineThickness.Value >= 1)
                    {
                        NewSeries.BorderWidth = LineThickness.Value;
                    }

                    // Bind data to series
                    NewSeries.Points.DataBindXY(XVals, YVals);

                    // Add series to the chart
                    ChartToPopulate.Series.Add(NewSeries);

                    // Draw the chart
                    ChartToPopulate.Invalidate();
                }
            }

            /// <summary>
            /// Common method used by charting functions to create the chart area based on user inputs.
            /// </summary>
            private static System.Windows.Forms.DataVisualization.Charting.ChartArea CreateChartArea(Nullable<double> XAxisMin, Nullable<double> XAxisMax, Nullable<double> XAxisInterval, bool XAxisShowGridLines, Nullable<int> XAxisFontSize, Nullable<double> YAxisMin, Nullable<double> YAxisMax, Nullable<double> YAxisInterval, bool YAxisShowGridLines, Nullable<int> YAxisFontSize, Nullable<System.Drawing.Color> BackgroundColor, System.Windows.Forms.DataVisualization.Charting.GradientStyle BackgroundGradient)
            {
                // Create new chart area
                var ChartArea = new System.Windows.Forms.DataVisualization.Charting.ChartArea();

                // Set min, max, and interval if specified
                if (XAxisMin != null) ChartArea.AxisX.Minimum = Convert.ToDouble(XAxisMin);
                if (XAxisMax != null) ChartArea.AxisX.Maximum = Convert.ToDouble(XAxisMax);
                if (YAxisMin != null) ChartArea.AxisY.Minimum = Convert.ToDouble(YAxisMin);
                if (YAxisMax != null) ChartArea.AxisY.Maximum = Convert.ToDouble(YAxisMax);
                if (XAxisInterval != null) ChartArea.AxisX.Interval = Convert.ToDouble(XAxisInterval);
                if (YAxisInterval != null) ChartArea.AxisY.Interval = Convert.ToDouble(YAxisInterval);

                // Set font sizes if entered
                if (XAxisFontSize != null) ChartArea.AxisX.LabelAutoFitMaxFontSize = Convert.ToInt32(XAxisFontSize);
                if (YAxisFontSize != null) ChartArea.AxisY.LabelAutoFitMaxFontSize = Convert.ToInt32(YAxisFontSize);

                // Show or hide grid lines based on user selections
                if (XAxisShowGridLines == true) ChartArea.AxisX.MajorGrid.LineColor = System.Drawing.Color.Black;
                if (XAxisShowGridLines == false) ChartArea.AxisX.MajorGrid.LineColor = System.Drawing.Color.Transparent;
                if (YAxisShowGridLines == true) ChartArea.AxisY.MajorGrid.LineColor = System.Drawing.Color.Black;
                if (YAxisShowGridLines == false) ChartArea.AxisY.MajorGrid.LineColor = System.Drawing.Color.Transparent;

                // Check if color was specified
                if (BackgroundColor != null)
                {
                    // If so, set color and gradient
                    ChartArea.BackColor = (System.Drawing.Color)BackgroundColor;
                    ChartArea.BackGradientStyle = BackgroundGradient;
                }

                // Return the chart area
                return ChartArea;
            }

        }

    }

}
