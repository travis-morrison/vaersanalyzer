﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StaticMethods
{

    public partial struct Strings
    {

        public struct TypeCheck
        {

            /// <summary>
            /// Used to check if a specified object / string is a valid boolean value.
            /// </summary>
            public static bool IsBoolean(object ItemToCheck)
            {
                // Create dummy var to hold result
                bool ConvertedValue = false;

                // Try to parse and save result
                return bool.TryParse(Convert.ToStringWithNullObjectSafety(ItemToCheck), out ConvertedValue);
            }

            /// <summary>
            /// Used to check if a specified object / string is a valid byte value.
            /// </summary>
            public static bool IsByte(object ItemToCheck)
            {
                // Create dummy var to hold result
                byte ConvertedValue = 0;

                // Try to parse and save result
                return byte.TryParse(Convert.ToStringWithNullObjectSafety(ItemToCheck), out ConvertedValue);
            }

            /// <summary>
            /// Used to check if a specified object / string is a valid hexadecimal value.
            /// </summary>
            public static bool IsHexadecimal(object ItemToCheck, bool RequireHexPrefix = false)
            {
                try
                {
                    // First, safely convert the item to a string
                    string StringToCheck = Convert.ToStringWithNullObjectSafety(ItemToCheck);

                    // Return false if input string is bad
                    if (string.IsNullOrEmpty(StringToCheck) == true) return false;

                    // Check if hex prefix is required
                    if (RequireHexPrefix == true)
                    {
                        // If so, return false if we don't have the 0x or &h prefixes commonly used in .NET
                        if (StringToCheck.ToUpper().Contains("0X") == false && StringToCheck.ToUpper().Contains("&H") == false)
                        {
                            return false;
                        }
                    }

                    // Remove any 0x / &h prefixes
                    StringToCheck = StringToCheck.ToUpper().Replace("0X", string.Empty).Replace("&H", string.Empty);

                    // Append "0" if there's an odd number of chars
                    if (StringToCheck.Length % 2 != 0)
                    {
                        StringToCheck = "0" + StringToCheck;
                    }

                    // Attempt to convert hex string candidate to a number
                    var ConvertedNumber = Int64.Parse(StringToCheck, System.Globalization.NumberStyles.AllowHexSpecifier);

                    // If conversion worked, return true, the string is hex
                    return true;
                }
                catch (Exception ex)
                {
                    // Return false if string can't be parsed as a hex number
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }
            /// <summary>
            /// Used to check if a specified object / string is a valid hexadecimal value.
            /// </summary>
            /// <param name="ItemToCheck">The object (generally a string) to check.</param>
            /// <param name="AdditionalAllowableCharacters">Characters in addition to hex values to allow, useful to allow spaces or wild card characters.</param>
            public static bool IsHexadecimal(object ItemToCheck, string AdditionalAllowableCharacters)
            {
                try
                {
                    // First, safely convert the item to a string
                    string StringToCheck = Convert.ToStringWithNullObjectSafety(ItemToCheck);

                    // Return false if input string is bad
                    if (string.IsNullOrEmpty(StringToCheck) == true) return false;

                    // Remove any 0x / &h prefixes
                    StringToCheck = StringToCheck.ToUpper().Replace("0X", string.Empty).Replace("&H", string.Empty);

                    // Create a string of acceptable characters
                    string AcceptableChars = "0123456789ABCDEF";

                    // Append on any user supplied allowable characters
                    AcceptableChars += AdditionalAllowableCharacters.ToUpper();

                    // Loop through each character in the string to check and see if the value is in the acceptable chars string
                    foreach (var LETTER in StringToCheck.ToUpper())
                    {
                        // If any char isn't allowable, return false
                        if (AcceptableChars.Contains(LETTER) == false)
                        {
                            return false;
                        }
                    }

                    // Return true if all chars were in the acceptable list of chars for hex values
                    return true;
                }
                catch (Exception ex)
                {
                    // Return false if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }

            /// <summary>
            /// Used to check if a specified object / string is a valid date value.
            /// </summary>
            public static bool IsDate(object ItemToCheck)
            {
                // Create dummy var to hold result
                DateTime ConvertedValue = DateTime.MinValue;

                // Try to parse save result
                return DateTime.TryParse(Convert.ToStringWithNullObjectSafety(ItemToCheck), out ConvertedValue);
            }

            /// <summary>
            /// Used to check if a specified object / string is a valid 16-bit unsigned integer value.
            /// </summary>
            public static bool IsUInt16(object ItemToCheck)
            {
                // Create dummy var to hold result
                UInt16 ConvertedValue = 0;

                // Attempt conversion and return the result
                return UInt16.TryParse(Convert.ToStringWithNullObjectSafety(ItemToCheck), out ConvertedValue);
            }
            /// <summary>
            /// Used to check if a specified object / string is a valid 32-bit unsigned integer value.
            /// </summary>
            public static bool IsUInt32(object ItemToCheck)
            {
                // Create dummy var to hold result
                UInt32 ConvertedValue = 0;

                // Attempt conversion and return the result
                return UInt32.TryParse(Convert.ToStringWithNullObjectSafety(ItemToCheck), out ConvertedValue);
            }
            /// <summary>
            /// Used to check if a specified object / string is a valid 64-bit unsigned integer value.
            /// </summary>
            public static bool IsUInt64(object ItemToCheck)
            {
                // Create dummy var to hold result
                UInt64 ConvertedValue = 0;

                // Attempt conversion and return the result
                return UInt64.TryParse(Convert.ToStringWithNullObjectSafety(ItemToCheck), out ConvertedValue);
            }

            /// <summary>
            /// Used to check if a specified string is an integer value.
            /// </summary>
            public static bool IsInteger(object ItemToCheck)
            {
                // Use underlying method to check and return
                return IsInt32(ItemToCheck);
            }
            /// <summary>
            /// Used to check if a specified string is a long value.
            /// </summary>
            public static bool IsLong(object ItemToCheck)
            {
                // Use underlying method to check and return
                return IsInt64(ItemToCheck);
            }

            /// <summary>
            /// Used to check if a specified object / string is a valid 16-bit integer value.
            /// </summary>
            public static bool IsInt16(object ItemToCheck)
            {
                // Create dummy var to hold result
                Int16 ConvertedValue = 0;

                // Attempt conversion and return the result
                return Int16.TryParse(Convert.ToStringWithNullObjectSafety(ItemToCheck), out ConvertedValue);
            }
            /// <summary>
            /// Used to check if a specified object / string is a valid 32-bit integer value.
            /// </summary>
            public static bool IsInt32(object ItemToCheck)
            {
                // Create dummy var to hold result
                Int32 ConvertedValue = 0;

                // Attempt conversion and return the result
                return Int32.TryParse(Convert.ToStringWithNullObjectSafety(ItemToCheck), out ConvertedValue);
            }
            /// <summary>
            /// Used to check if a specified object / string is a valid 64-bit integer value.
            /// </summary>
            public static bool IsInt64(object ItemToCheck)
            {
                // Create dummy var to hold result
                Int64 ConvertedValue = 0;

                // Attempt conversion and return the result
                return Int64.TryParse(Convert.ToStringWithNullObjectSafety(ItemToCheck), out ConvertedValue);
            }

            /// <summary>
            /// Used to check if a specified object / string is a valid float (alias for System.Single) value.
            /// </summary>
            public static bool IsFloat(object ItemToCheck)
            {
                // Use underlying method
                return IsSingle(ItemToCheck);
            }
            /// <summary>
            /// Used to check if a specified object / string is a valid single value.
            /// </summary>
            public static bool IsSingle(object ItemToCheck)
            {
                // Create dummy var to hold result
                float ConvertedValue = 0;

                // Attempt conversion and return the result
                return float.TryParse(Convert.ToStringWithNullObjectSafety(ItemToCheck), out ConvertedValue);
            }
            /// <summary>
            /// Used to check if a specified object / string is a valid double value.
            /// </summary>
            public static bool IsDouble(object ItemToCheck)
            {
                // Create dummy var to hold result
                double ConvertedValue = 0;

                // Attempt conversion and return the result
                return double.TryParse(Convert.ToStringWithNullObjectSafety(ItemToCheck), out ConvertedValue);
            }
            /// <summary>
            /// Used to check if a specified object / string is a valid decimal value (Sytem.Decimal).
            /// </summary>
            public static bool IsDecimal(object ItemToCheck)
            {
                // Create dummy var to hold result
                decimal ConvertedValue = 0;

                // Attempt conversion and return the result
                return decimal.TryParse(Convert.ToStringWithNullObjectSafety(ItemToCheck), out ConvertedValue);
            }

            /// <summary>
            /// Used to check if a specified string can be converted to a numeric value (System.Double).
            /// </summary>
            public static bool IsNumeric(object ItemToCheck)
            {
                // Check if number is a double and return the result
                return IsDouble(ItemToCheck);
            }

        }

        public struct Examine
        {

            /// <summary>
            /// Used to check if a specified string contains one or more numeric characters.
            /// </summary>
            public static bool HasNumericCharacters(string StringToCheck)
            {
                // Use helper to check and return result (numeric chars are in ascii range 48 to 57)
                return HasAsciiCharacters(StringToCheck, 48, 57);
            }

            /// <summary>
            /// Used to check if a specified string contains one or more lower case characters.
            /// </summary>
            public static bool HasLowerCaseCharacters(string StringToCheck)
            {
                // Use helper to check and return result (lower case chars are in ascii range 97 to 122)
                return HasAsciiCharacters(StringToCheck, 97, 122);
            }

            /// <summary>
            /// Used to check if a specified string contains one or more upper case characters.
            /// </summary>
            public static bool HasUpperCaseCharacters(string StringToCheck)
            {
                // Use helper to check and return result (upper case chars are in ascii range 65 to 90)
                return HasAsciiCharacters(StringToCheck, 65, 90);
            }

            /// <summary>
            /// Used to check if a specified string contains one or more special characters (i.e. symbols).
            /// </summary>
            public static bool HasSpecialCharacters(string StringToCheck)
            {
                // Symbols / special chars occur in multiple different ascii ranges, create list of KVPs to hold all ranges
                var AllAsciiSpecialCharRanges = new List<KeyValuePair<UInt16, UInt16>>();

                // Add all ranges
                AllAsciiSpecialCharRanges.Add(new KeyValuePair<UInt16, UInt16>(32, 47));
                AllAsciiSpecialCharRanges.Add(new KeyValuePair<UInt16, UInt16>(58, 64));
                AllAsciiSpecialCharRanges.Add(new KeyValuePair<UInt16, UInt16>(91, 96));
                AllAsciiSpecialCharRanges.Add(new KeyValuePair<UInt16, UInt16>(123, 126));

                // Loop through and check each range
                foreach (var RANGE in AllAsciiSpecialCharRanges)
                {
                    // If any range is detected, than return true (there's at least one special char, no need to continue)
                    if (HasAsciiCharacters(StringToCheck, RANGE.Key, RANGE.Value) == true)
                    {
                        return true;
                    }
                }

                // If no char was found in any of the ranges, there are no special chars
                return false;
            }

            /// <summary>
            /// Helper to check if a given string as at least one character in a specified ascii value range.
            /// </summary>
            private static bool HasAsciiCharacters(string StringToCheck, UInt16 InclusiveLowerBound, UInt16 InclusiveUpperBound)
            {
                try
                {
                    // Return false if string is missing (obviously doesn't have any chars)
                    if (string.IsNullOrEmpty(StringToCheck) == true) return false;

                    // Get string as char array for checking
                    var AllCharacters = StringToCheck.ToArray();

                    // Create flag to hold if it has at least one character in range
                    bool HasAtLeastOneCharInRange = false;

                    // Loop through and check chars
                    foreach (var CHARACTER in AllCharacters)
                    {
                        // Save the ascii value of the current char
                        int AsciiValue = System.Convert.ToInt32(CHARACTER);

                        // Check if the current char is within the range
                        if (AsciiValue >= InclusiveLowerBound && AsciiValue <= InclusiveUpperBound)
                        {
                            // If it's in range, set flag and bail
                            HasAtLeastOneCharInRange = true;
                            break;
                        }
                    }

                    // Return the result
                    return HasAtLeastOneCharInRange;
                }
                catch (Exception ex)
                {
                    // Return false if error occurs somehow
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }

        }

        public struct Generate
        {

            /// <summary>
            /// Used to create a unique ID using the current timestamp (YYYY-MM-DD-hh-mm-ss[-mSmSmS]).
            /// </summary>
            /// <param name="IncludeMS">Boolean to set whether the ID should go out to milliseconds for higher resolution.</param>
            /// <param name="UseDashSeparators">Boolean to set if the final ID should contain "-" characters or be one long number.</param>
            /// <returns>Returns the unique ID as a string.</returns>
            public static string UniqueID(bool IncludeMS, bool UseDashSeparators)
            {
                return CreateTimestampStringIDHelper(false, IncludeMS, UseDashSeparators);
            }
            /// <summary>
            /// Used to create a timestamp string using the current time (YYYY-MM-DD-hh-mm-ss[-mSmSmS]).
            /// </summary>
            /// <param name="IncludeMS">Boolean to set whether the ID should go out to milliseconds for higher resolution.</param>
            /// <param name="UseDashSeparators">Boolean to set if the final string should contain "-" characters or be one long number.</param>
            /// <returns>Returns the unique ID as a string.</returns>
            public static string TimestampString(bool IncludeMS, bool UseDashSeparators)
            {
                // Use common method
                return CreateTimestampStringIDHelper(false, IncludeMS, UseDashSeparators);
            }
            /// <summary>
            /// Used to create a timestamp string using the current time (YYYY-MM-DD-hh-mm-ss[-mSmSmS]).
            /// </summary>
            /// <param name="UseUtcTime">Flag to indicate if timestamp should be in UTC time (vs. using the timezone of the host PC).</param>
            /// <param name="IncludeMS">Boolean to set whether the ID should go out to milliseconds for higher resolution.</param>
            /// <param name="UseDashSeparators">Boolean to set if the final string should contain "-" characters or be one long number.</param>
            /// <returns>Returns the unique ID as a string.</returns>
            public static string TimestampString(bool UseUtcTime, bool IncludeMS, bool UseDashSeparators)
            {
                // Use common method
                return CreateTimestampStringIDHelper(UseUtcTime, IncludeMS, UseDashSeparators);
            }
            private static string CreateTimestampStringIDHelper(bool UseUTCTime, bool IncludeMS, bool UseDashSeparators)
            {
                // Create the date to use
                var DateToUse = DateTime.Now;

                // Flip to UTC if requested
                if (UseUTCTime == true) DateToUse = DateTime.UtcNow;

                // Create base ID (YYYY-MM-DD-HH-MM-SS)
                string IDToReturn = DateToUse.Year.ToString() + "-" + Transform.PadNumberLeadingDigits(DateToUse.Month.ToString(), 2) + "-" + Transform.PadNumberLeadingDigits(DateToUse.Day.ToString(), 2) + "-" + Transform.PadNumberLeadingDigits(DateToUse.Hour.ToString(), 2) + "-" + Transform.PadNumberLeadingDigits(DateToUse.Minute.ToString(), 2) + "-" + Transform.PadNumberLeadingDigits(DateToUse.Second.ToString(), 2);

                // Check if including mS
                if (IncludeMS == true)
                {
                    // If so, add mS
                    IDToReturn += "-" + Transform.PadNumberLeadingDigits(DateToUse.Millisecond.ToString(), 3);
                }

                // Check if user requested dashes
                if (UseDashSeparators == false)
                {
                    // If not, remove them from the string
                    IDToReturn = IDToReturn.Replace("-", string.Empty);
                }

                // Return the string ID
                return IDToReturn;
            }

            /// <summary>
            /// Used to compute a time duration string (hh:mm:ss) from a specified number of seconds.
            /// </summary>
            /// <param name="DurationInSeconds">The total number of seconds for the duration.</param>
            /// <param name="IncludeSeconds">Flag to indicate if the seconds should be shown in the returned string (hh:mm:ss vs. hh:mm).</param>
            /// <returns>Returns a string indicating the total duration for the specified number of seconds.</returns>
            public static string TimeDurationString(int DurationInSeconds, bool IncludeSeconds)
            {
                // Get the total hours and minutes
                int DiffHours = System.Convert.ToInt32(System.Math.Floor(TimeSpan.FromSeconds(DurationInSeconds).TotalHours));
                int DiffMinutes = System.Convert.ToInt32(System.Math.Floor(TimeSpan.FromSeconds(DurationInSeconds).TotalMinutes));

                // Subtract off the total hours from total minutes to get only remainder
                DiffMinutes = DiffMinutes - (DiffHours * 60);

                // Subtract off the total hours & minutes from the total seconds to get only remainder
                DurationInSeconds = DurationInSeconds - (DiffHours * 60 * 60) - (DiffMinutes * 60);

                // Create the string to return
                string StringToReturn = Transform.PadNumberLeadingDigits(DiffHours.ToString(), 2) + ":" + Transform.PadNumberLeadingDigits(DiffMinutes.ToString(), 2);

                // Include time if requested
                if (IncludeSeconds == true)
                {
                    StringToReturn += ":" + Transform.PadNumberLeadingDigits(DurationInSeconds.ToString(), 2);
                }

                // Return the string
                return StringToReturn;
            }

            /// <summary>
            /// Used to compute the time difference string (hh:mm:ss) between two DateTime values.
            /// </summary>
            /// <param name="StartDateTime">The starting DateTime to use.</param>
            /// <param name="EndDateTime">The ending DateTime to use.</param>
            /// <param name="IncludeSeconds">Flag to indicate if seconds should be included in the returned result (hh:mm:ss vs. hh:mm).</param>
            /// <returns>Return a string representing the time difference between the two DateTime values.</returns>
            public static string TimeDifferenceString(DateTime StartDateTime, DateTime EndDateTime, bool IncludeSeconds)
            {
                // Get the difference in hours, minutes, seconds
                double DiffHours = System.Math.Floor((EndDateTime - StartDateTime).TotalHours);
                double DiffMinutes = System.Math.Floor((EndDateTime - StartDateTime).TotalMinutes);
                double DiffSeconds = System.Math.Round((EndDateTime - StartDateTime).TotalSeconds, 0, MidpointRounding.AwayFromZero);

                // Subtract off the total hours from total minutes to get only remainder
                DiffMinutes = DiffMinutes - (DiffHours * 60);

                // Subtract off the total hours & minutes from the total seconds to get only remainder
                DiffSeconds = DiffSeconds - (DiffHours * 60 * 60) - (DiffMinutes * 60);

                // Create the string to return
                string StringToReturn = Transform.PadNumberLeadingDigits(DiffHours.ToString(), 2) + ":" + Transform.PadNumberLeadingDigits(DiffMinutes.ToString(), 2);

                // Append seconds if requested
                if (IncludeSeconds == true)
                {
                    StringToReturn += ":" + Transform.PadNumberLeadingDigits(DiffSeconds.ToString(), 2);
                }

                // Return the string
                return StringToReturn;
            }

            /// <summary>
            /// Used to convert an array of string data to a markdown-formatted text table (i.e. an ASCII table).
            /// </summary>
            /// <param name="TableDataWithColumnHeaders">The 2D array of data (where Row0 is the column headers) to convert to a text table.</param>
            /// <returns>Returns a markdown text table if successful. Returns an empty string in all other conditions.</returns>
            public static string MarkdownTextTable(string[,] TableDataWithColumnHeaders)
            {
                try
                {
                    // Return empty string if there isn't enough info to make table
                    if (TableDataWithColumnHeaders == null || TableDataWithColumnHeaders.GetUpperBound(0) == 0)
                    {
                        return string.Empty;
                    }

                    // If we have all the info we need, create list to hold the max character length in each column
                    var MaxCharLengthInEachColumn = new List<int>();

                    // Loop through and compute max length
                    for (int COL = 0; COL <= TableDataWithColumnHeaders.GetUpperBound(1); COL++)
                    {
                        // Add place holder for the current column
                        MaxCharLengthInEachColumn.Add(0);

                        // Loop through all rows and get the longest char length string in there
                        for (int ROW = 0; ROW <= TableDataWithColumnHeaders.GetUpperBound(0); ROW++)
                        {
                            // Save the string item so we can check its length
                            string Entry = TableDataWithColumnHeaders[ROW, COL];

                            // Skip if item is null (i.e. stick with 0 length)
                            if (string.IsNullOrEmpty(Entry) == true)
                            {
                                // Ensure item isn't null before continuing to prevent any null reference errors
                                TableDataWithColumnHeaders[ROW, COL] = string.Empty;
                                continue;
                            }

                            // If we get a non-zero entry, save the length
                            int LengthOfEntry = Entry.Length;

                            // Overwrite the info for the max length of this column IF it's bigger than the last max on file
                            if (LengthOfEntry > MaxCharLengthInEachColumn[COL])
                            {
                                MaxCharLengthInEachColumn[COL] = LengthOfEntry;
                            }
                        }
                    }

                    // Once we have all max lengths, go through and pad each entry in each column to its length
                    for (int COL = 0; COL <= TableDataWithColumnHeaders.GetUpperBound(1); COL++)
                    {
                        for (int ROW = 0; ROW <= TableDataWithColumnHeaders.GetUpperBound(0); ROW++)
                        {
                            // Pad each entry to the max length of the column
                            TableDataWithColumnHeaders[ROW, COL] = TableDataWithColumnHeaders[ROW, COL].PadRight(MaxCharLengthInEachColumn[COL], System.Convert.ToChar(" "));

                            // Add one additional space char before and after entry per the spec (i.e. there's a space between the |'s and the text in the cell)
                            TableDataWithColumnHeaders[ROW, COL] = " " + TableDataWithColumnHeaders[ROW, COL] + " ";
                        }
                    }

                    // Now that everything is padded correctly, create a list to hold strings joined together and separated by the "|" char
                    var AllJoinedInfoLines = new List<string>();

                    // Loop through and join info
                    for (int ROW = 0; ROW <= TableDataWithColumnHeaders.GetUpperBound(0); ROW++)
                    {
                        // Create list to hold all entries for row
                        var EntriesForRow = new List<string>();

                        // Get info from each column
                        for (int COL = 0; COL <= TableDataWithColumnHeaders.GetUpperBound(1); COL++)
                        {
                            // Add all info
                            EntriesForRow.Add(TableDataWithColumnHeaders[ROW, COL]);
                        }

                        // Join up info and save back to lines (add the post to the beginning and end too)
                        AllJoinedInfoLines.Add("|" + string.Join("|", EntriesForRow) + "|");
                    }

                    // Finally, we need the divider row between the headers and the data (|---------|-------------------|--------|, etc.), create list to hold minus (-) signs
                    var MinusSignBlocks = new List<string>();

                    // Loop through each set of columns and get the full padded lengths to make the minus signs
                    for (int COL = 0; COL <= TableDataWithColumnHeaders.GetUpperBound(1); COL++)
                    {
                        // Get the full padded length of the first item (they will have all the same length at this point)
                        int FullPaddedLength = TableDataWithColumnHeaders[0, COL].Length;

                        // Create minus signs padded to that length
                        string PaddedMinusSigns = new string(System.Convert.ToChar("-"), FullPaddedLength);

                        // Add padded item to minus blocks
                        MinusSignBlocks.Add(PaddedMinusSigns);
                    }

                    // Finally, join up the minus blocks and put post on either end to make the divider row
                    string DividerRow = "|" + string.Join("|", MinusSignBlocks) + "|";

                    // Insert the divider row after the header row
                    AllJoinedInfoLines.Insert(1, DividerRow);

                    // Join and return info
                    return string.Join(Environment.NewLine, AllJoinedInfoLines);
                }
                catch (Exception ex)
                {
                    // Return empty string if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

            /// <summary>
            /// Used to convert an array of string data to an HTML table.
            /// </summary>
            /// <param name="TableDataWithColumnHeaders">The 2D array of data (where Row0 is the column headers) to convert to the HTML table.</param>
            /// <returns>Returns an HTML text table if successful. Returns an empty string in all other conditions.</returns>
            public static string HtmlTable(string[,] TableDataWithColumnHeaders)
            {
                // Use underlying method to do work and return result
                return HTML.TableFromArray(TableDataWithColumnHeaders);
            }

            /// <summary>
            /// Used to create a random product key with either letters or letters and numbers in dashed groupings (e.g. 'ABC-B31-C3A', etc.). Note that the dashes are NOT counted when determining key size.
            /// </summary>
            /// <param name="LengthOfKey">The length to build the key to (dashes used for groupings are NOT counted towards the length).</param>
            /// <param name="UseLettersOnly">Flag to indicate if key should contain only letters or letters and numbers.</param>
            /// <param name="GroupingSize">How many characters apart (if any) to place dashes (i.e. 3 would result in 'ABCB31C3A' becoming 'ABC-B31-C3A').</param>
            /// <returns>Returns a random key of specified length if created successfully. Returns and empty string in all other conditions.</returns>
            public static string ProductKey(UInt16 LengthOfKey, bool UseLettersOnly, UInt16 GroupingSize)
            {
                try
                {
                    // Get the key of specified length
                    string WorkingKey = ProductKey(LengthOfKey, UseLettersOnly);

                    // Return empty if nothing comes back
                    if (string.IsNullOrEmpty(WorkingKey) == true) return string.Empty;

                    // If something comes back, return it if there is no group size requested OR key is smaller than group size
                    if (GroupingSize == 0 || GroupingSize >= WorkingKey.Length) return WorkingKey;

                    // If group size is requested, break key up into characters
                    var KeyCharacters = WorkingKey.ToCharArray().ToList();

                    // Create list to hold all groupings
                    var Groupings = new List<string>();

                    // Create string to hold / build groupings
                    string NextGroup = string.Empty;

                    // Loop through chars and build groupings of specified size
                    foreach (var CHARACTER in KeyCharacters)
                    {
                        // Add character to grouping
                        NextGroup += CHARACTER.ToString();

                        // Check if group size has been hit yet
                        if (NextGroup.Length >= GroupingSize)
                        {
                            // If so, add to list and clear var for next grouping
                            Groupings.Add(NextGroup);
                            NextGroup = string.Empty;
                        }
                    }

                    // Add any remaining text to the groupings in case they didn't provide groupings that divide equally
                    if (string.IsNullOrEmpty(NextGroup) == false)
                    {
                        Groupings.Add(NextGroup);
                    }

                    // Join the key back together and return it
                    return string.Join("-", Groupings);
                }
                catch (Exception ex)
                {
                    // Return empty string if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }
            /// <summary>
            /// Used to create a random product key with either letters or letters and number.
            /// </summary>
            /// <param name="LengthOfKey">The length to build the key to.</param>
            /// <param name="UseLettersOnly">Flag to indicate if key should contain only letters or letters and numbers.</param>
            /// <returns>Returns a random key of specified length if created successfully. Returns and empty string in all other conditions.</returns>
            public static string ProductKey(UInt16 LengthOfKey, bool UseLettersOnly)
            {
                try
                {
                    // Return an empty string if length is bad
                    if (LengthOfKey == 0) return string.Empty;

                    // If length is legit, create master string to hold key
                    string MasterString = string.Empty;

                    // Loop through and keep bolting on GUIDs until it is long enough
                    while (MasterString.Length < LengthOfKey)
                    {
                        // Bolt on next GUID without any dashes
                        MasterString += Guid.NewGuid().ToString().Replace("-", string.Empty);

                        // Check if using letters only
                        if (UseLettersOnly == true)
                        {
                            // If so, loop through and remove all numbers
                            for (int i = 0; i <= 9; i++)
                            {
                                MasterString = MasterString.Replace(i.ToString(), string.Empty);
                            }
                        }
                    }

                    // Once key is created, trim off any extra
                    if (MasterString.Length > LengthOfKey)
                    {
                        MasterString = MasterString.Substring(0, LengthOfKey);
                    }

                    // Return the key in upper case
                    return MasterString.ToUpper();
                }
                catch (Exception ex)
                {
                    // Return an empty string if key can't be created
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

            /// <summary>
            /// Used to return a list of all letters (either "A-Z" or "a-z").
            /// </summary>
            /// <param name="ReturnUpperCase">Flag to indicate if upper or lower case letters should be returned.</param>
            /// <returns>Returns a list of letters with the specified casing if the method is successful. Returns nothing in all other conditions.</returns>
            public static List<string> AllLetters(bool ReturnUpperCase)
            {
                try
                {
                    // Create list to return
                    var Letters = new List<string>();

                    // Loop through all ASCII chars and get A-Z
                    for (int i = 65; i <= 90; i++)
                    {
                        // Check if using upper or lower case
                        switch (ReturnUpperCase)
                        {
                            case true:
                                // Add letter as upper case
                                Letters.Add(System.Convert.ToChar(i).ToString().ToUpper());
                                break;
                            case false:
                                // Add letter as lower case
                                Letters.Add(System.Convert.ToChar(i).ToString().ToLower());
                                break;
                        }
                    }

                    // Return the list when done
                    return Letters;
                }
                catch (Exception ex)
                {
                    // Return nothing if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }

            /// <summary>
            /// Used to get the two-letter abbreviations for all states in America.
            /// </summary>
            /// <returns>Returns a list of every two-letter abbreviation for all of the states in America.</returns>
            public static List<string> AllAmericanStateAbbreviations()
            {
                return new[] { "AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "FL", "GA", "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY" }.ToList();
            }

            /// <summary>
            /// Used to get the three-letter abbreviations for all global country codes.
            /// </summary>
            /// <returns>Returns a list of all 3 digit country codes.</returns>
            public static List<string> AllCountryCodes()
            {
                // Get full dictionary and return the keys / codes
                return AllCountryCodesAndNames().Keys.ToList();
            }
            /// <summary>
            /// Used to get the list of all three-letter country codes and their full name.
            /// </summary>
            /// <returns>Returns a dictionary containing all 3 digit country codes and their corresponding country name.</returns>
            public static Dictionary<string, string> AllCountryCodesAndNames()
            {
                // Create dictionary to hold info
                var AllCodesAndNames = new Dictionary<string, string>();

                // Add all names
                AllCodesAndNames.Add("AFG", "Afghanistan");
                AllCodesAndNames.Add("ALB", "Albania");
                AllCodesAndNames.Add("DZA", "Algeria");
                AllCodesAndNames.Add("ASM", "American Samoa");
                AllCodesAndNames.Add("AND", "Andorra");
                AllCodesAndNames.Add("AGO", "Angola");
                AllCodesAndNames.Add("AIA", "Anguilla");
                AllCodesAndNames.Add("ATA", "Antarctica");
                AllCodesAndNames.Add("ATG", "Antigua and Barbuda");
                AllCodesAndNames.Add("ARG", "Argentina");
                AllCodesAndNames.Add("ARM", "Armenia");
                AllCodesAndNames.Add("ABW", "Aruba");
                AllCodesAndNames.Add("AUS", "Australia");
                AllCodesAndNames.Add("AUT", "Austria");
                AllCodesAndNames.Add("AZE", "Azerbaijan");
                AllCodesAndNames.Add("BHS", "Bahamas");
                AllCodesAndNames.Add("BHR", "Bahrain");
                AllCodesAndNames.Add("BGD", "Bangladesh");
                AllCodesAndNames.Add("BRB", "Barbados");
                AllCodesAndNames.Add("BLR", "Belarus");
                AllCodesAndNames.Add("BEL", "Belgium");
                AllCodesAndNames.Add("BLZ", "Belize");
                AllCodesAndNames.Add("BEN", "Benin");
                AllCodesAndNames.Add("BMU", "Bermuda");
                AllCodesAndNames.Add("BTN", "Bhutan");
                AllCodesAndNames.Add("BOL", "Bolivia");
                AllCodesAndNames.Add("BES", "Bonaire");
                AllCodesAndNames.Add("BIH", "Bosnia and Herzegovina");
                AllCodesAndNames.Add("BWA", "Botswana");
                AllCodesAndNames.Add("BVT", "Bouvet Island");
                AllCodesAndNames.Add("BRA", "Brazil");
                AllCodesAndNames.Add("IOT", "British Indian Ocean Territory");
                AllCodesAndNames.Add("BRN", "Brunei Darussalam");
                AllCodesAndNames.Add("BGR", "Bulgaria");
                AllCodesAndNames.Add("BFA", "Burkina Faso");
                AllCodesAndNames.Add("BDI", "Burundi");
                AllCodesAndNames.Add("KHM", "Cambodia");
                AllCodesAndNames.Add("CMR", "Cameroon");
                AllCodesAndNames.Add("CAN", "Canada");
                AllCodesAndNames.Add("CPV", "Cape Verde");
                AllCodesAndNames.Add("CYM", "Cayman Islands");
                AllCodesAndNames.Add("CAF", "Central African Republic");
                AllCodesAndNames.Add("TCD", "Chad");
                AllCodesAndNames.Add("CHL", "Chile");
                AllCodesAndNames.Add("CHN", "China");
                AllCodesAndNames.Add("CXR", "Christmas Island");
                AllCodesAndNames.Add("CCK", "Cocos (Keeling) Islands");
                AllCodesAndNames.Add("COL", "Colombia");
                AllCodesAndNames.Add("COM", "Comoros");
                AllCodesAndNames.Add("COG", "Congo");
                AllCodesAndNames.Add("COD", "Democratic Republic of the Congo");
                AllCodesAndNames.Add("COK", "Cook Islands");
                AllCodesAndNames.Add("CRI", "Costa Rica");
                AllCodesAndNames.Add("HRV", "Croatia");
                AllCodesAndNames.Add("CUB", "Cuba");
                AllCodesAndNames.Add("CUW", "Curacao");
                AllCodesAndNames.Add("CYP", "Cyprus");
                AllCodesAndNames.Add("CZE", "Czech Republic");
                AllCodesAndNames.Add("CIV", "Cote d'Ivoire");
                AllCodesAndNames.Add("DNK", "Denmark");
                AllCodesAndNames.Add("DJI", "Djibouti");
                AllCodesAndNames.Add("DMA", "Dominica");
                AllCodesAndNames.Add("DOM", "Dominican Republic");
                AllCodesAndNames.Add("ECU", "Ecuador");
                AllCodesAndNames.Add("EGY", "Egypt");
                AllCodesAndNames.Add("SLV", "El Salvador");
                AllCodesAndNames.Add("GNQ", "Equatorial Guinea");
                AllCodesAndNames.Add("ERI", "Eritrea");
                AllCodesAndNames.Add("EST", "Estonia");
                AllCodesAndNames.Add("ETH", "Ethiopia");
                AllCodesAndNames.Add("FLK", "Falkland Islands (Malvinas)");
                AllCodesAndNames.Add("FRO", "Faroe Islands");
                AllCodesAndNames.Add("FJI", "Fiji");
                AllCodesAndNames.Add("FIN", "Finland");
                AllCodesAndNames.Add("FRA", "France");
                AllCodesAndNames.Add("GUF", "French Guiana");
                AllCodesAndNames.Add("PYF", "French Polynesia");
                AllCodesAndNames.Add("ATF", "French Southern Territories");
                AllCodesAndNames.Add("GAB", "Gabon");
                AllCodesAndNames.Add("GMB", "Gambia");
                AllCodesAndNames.Add("GEO", "Georgia");
                AllCodesAndNames.Add("DEU", "Germany");
                AllCodesAndNames.Add("GHA", "Ghana");
                AllCodesAndNames.Add("GIB", "Gibraltar");
                AllCodesAndNames.Add("GRC", "Greece");
                AllCodesAndNames.Add("GRL", "Greenland");
                AllCodesAndNames.Add("GRD", "Grenada");
                AllCodesAndNames.Add("GLP", "Guadeloupe");
                AllCodesAndNames.Add("GUM", "Guam");
                AllCodesAndNames.Add("GTM", "Guatemala");
                AllCodesAndNames.Add("GGY", "Guernsey");
                AllCodesAndNames.Add("GIN", "Guinea");
                AllCodesAndNames.Add("GNB", "Guinea-Bissau");
                AllCodesAndNames.Add("GUY", "Guyana");
                AllCodesAndNames.Add("HTI", "Haiti");
                AllCodesAndNames.Add("HMD", "Heard Island and McDonald Islands");
                AllCodesAndNames.Add("VAT", "Holy See (Vatican City State)");
                AllCodesAndNames.Add("HND", "Honduras");
                AllCodesAndNames.Add("HKG", "Hong Kong");
                AllCodesAndNames.Add("HUN", "Hungary");
                AllCodesAndNames.Add("ISL", "Iceland");
                AllCodesAndNames.Add("IND", "India");
                AllCodesAndNames.Add("IDN", "Indonesia");
                AllCodesAndNames.Add("IRN", "Iran, Islamic Republic of");
                AllCodesAndNames.Add("IRQ", "Iraq");
                AllCodesAndNames.Add("IRL", "Ireland");
                AllCodesAndNames.Add("IMN", "Isle of Man");
                AllCodesAndNames.Add("ISR", "Israel");
                AllCodesAndNames.Add("ITA", "Italy");
                AllCodesAndNames.Add("JAM", "Jamaica");
                AllCodesAndNames.Add("JPN", "Japan");
                AllCodesAndNames.Add("JEY", "Jersey");
                AllCodesAndNames.Add("JOR", "Jordan");
                AllCodesAndNames.Add("KAZ", "Kazakhstan");
                AllCodesAndNames.Add("KEN", "Kenya");
                AllCodesAndNames.Add("KIR", "Kiribati");
                AllCodesAndNames.Add("PRK", "Korea, Democratic People's Republic of");
                AllCodesAndNames.Add("KOR", "Korea, Republic of");
                AllCodesAndNames.Add("KWT", "Kuwait");
                AllCodesAndNames.Add("KGZ", "Kyrgyzstan");
                AllCodesAndNames.Add("LAO", "Lao People's Democratic Republic");
                AllCodesAndNames.Add("LVA", "Latvia");
                AllCodesAndNames.Add("LBN", "Lebanon");
                AllCodesAndNames.Add("LSO", "Lesotho");
                AllCodesAndNames.Add("LBR", "Liberia");
                AllCodesAndNames.Add("LBY", "Libya");
                AllCodesAndNames.Add("LIE", "Liechtenstein");
                AllCodesAndNames.Add("LTU", "Lithuania");
                AllCodesAndNames.Add("LUX", "Luxembourg");
                AllCodesAndNames.Add("MAC", "Macao");
                AllCodesAndNames.Add("MKD", "Macedonia, the Former Yugoslav Republic of");
                AllCodesAndNames.Add("MDG", "Madagascar");
                AllCodesAndNames.Add("MWI", "Malawi");
                AllCodesAndNames.Add("MYS", "Malaysia");
                AllCodesAndNames.Add("MDV", "Maldives");
                AllCodesAndNames.Add("MLI", "Mali");
                AllCodesAndNames.Add("MLT", "Malta");
                AllCodesAndNames.Add("MHL", "Marshall Islands");
                AllCodesAndNames.Add("MTQ", "Martinique");
                AllCodesAndNames.Add("MRT", "Mauritania");
                AllCodesAndNames.Add("MUS", "Mauritius");
                AllCodesAndNames.Add("MYT", "Mayotte");
                AllCodesAndNames.Add("MEX", "Mexico");
                AllCodesAndNames.Add("FSM", "Micronesia, Federated States of");
                AllCodesAndNames.Add("MDA", "Moldova, Republic of");
                AllCodesAndNames.Add("MCO", "Monaco");
                AllCodesAndNames.Add("MNG", "Mongolia");
                AllCodesAndNames.Add("MNE", "Montenegro");
                AllCodesAndNames.Add("MSR", "Montserrat");
                AllCodesAndNames.Add("MAR", "Morocco");
                AllCodesAndNames.Add("MOZ", "Mozambique");
                AllCodesAndNames.Add("MMR", "Myanmar");
                AllCodesAndNames.Add("NAM", "Namibia");
                AllCodesAndNames.Add("NRU", "Nauru");
                AllCodesAndNames.Add("NPL", "Nepal");
                AllCodesAndNames.Add("NLD", "Netherlands");
                AllCodesAndNames.Add("NCL", "New Caledonia");
                AllCodesAndNames.Add("NZL", "New Zealand");
                AllCodesAndNames.Add("NIC", "Nicaragua");
                AllCodesAndNames.Add("NER", "Niger");
                AllCodesAndNames.Add("NGA", "Nigeria");
                AllCodesAndNames.Add("NIU", "Niue");
                AllCodesAndNames.Add("NFK", "Norfolk Island");
                AllCodesAndNames.Add("MNP", "Northern Mariana Islands");
                AllCodesAndNames.Add("NOR", "Norway");
                AllCodesAndNames.Add("OMN", "Oman");
                AllCodesAndNames.Add("PAK", "Pakistan");
                AllCodesAndNames.Add("PLW", "Palau");
                AllCodesAndNames.Add("PSE", "Palestine, State of");
                AllCodesAndNames.Add("PAN", "Panama");
                AllCodesAndNames.Add("PNG", "Papua New Guinea");
                AllCodesAndNames.Add("PRY", "Paraguay");
                AllCodesAndNames.Add("PER", "Peru");
                AllCodesAndNames.Add("PHL", "Philippines");
                AllCodesAndNames.Add("PCN", "Pitcairn");
                AllCodesAndNames.Add("POL", "Poland");
                AllCodesAndNames.Add("PRT", "Portugal");
                AllCodesAndNames.Add("PRI", "Puerto Rico");
                AllCodesAndNames.Add("QAT", "Qatar");
                AllCodesAndNames.Add("ROU", "Romania");
                AllCodesAndNames.Add("RUS", "Russian Federation");
                AllCodesAndNames.Add("RWA", "Rwanda");
                AllCodesAndNames.Add("REU", "Reunion");
                AllCodesAndNames.Add("BLM", "Saint Barthelemy");
                AllCodesAndNames.Add("SHN", "Saint Helena");
                AllCodesAndNames.Add("KNA", "Saint Kitts and Nevis");
                AllCodesAndNames.Add("LCA", "Saint Lucia");
                AllCodesAndNames.Add("MAF", "Saint Martin (French part)");
                AllCodesAndNames.Add("SPM", "Saint Pierre and Miquelon");
                AllCodesAndNames.Add("VCT", "Saint Vincent and the Grenadines");
                AllCodesAndNames.Add("WSM", "Samoa");
                AllCodesAndNames.Add("SMR", "San Marino");
                AllCodesAndNames.Add("STP", "Sao Tome and Principe");
                AllCodesAndNames.Add("SAU", "Saudi Arabia");
                AllCodesAndNames.Add("SEN", "Senegal");
                AllCodesAndNames.Add("SRB", "Serbia");
                AllCodesAndNames.Add("SYC", "Seychelles");
                AllCodesAndNames.Add("SLE", "Sierra Leone");
                AllCodesAndNames.Add("SGP", "Singapore");
                AllCodesAndNames.Add("SXM", "Sint Maarten (Dutch part)");
                AllCodesAndNames.Add("SVK", "Slovakia");
                AllCodesAndNames.Add("SVN", "Slovenia");
                AllCodesAndNames.Add("SLB", "Solomon Islands");
                AllCodesAndNames.Add("SOM", "Somalia");
                AllCodesAndNames.Add("ZAF", "South Africa");
                AllCodesAndNames.Add("SGS", "South Georgia and the South Sandwich Islands");
                AllCodesAndNames.Add("SSD", "South Sudan");
                AllCodesAndNames.Add("ESP", "Spain");
                AllCodesAndNames.Add("LKA", "Sri Lanka");
                AllCodesAndNames.Add("SDN", "Sudan");
                AllCodesAndNames.Add("SUR", "Suriname");
                AllCodesAndNames.Add("SJM", "Svalbard and Jan Mayen");
                AllCodesAndNames.Add("SWZ", "Swaziland");
                AllCodesAndNames.Add("SWE", "Sweden");
                AllCodesAndNames.Add("CHE", "Switzerland");
                AllCodesAndNames.Add("SYR", "Syrian Arab Republic");
                AllCodesAndNames.Add("TWN", "Taiwan");
                AllCodesAndNames.Add("TJK", "Tajikistan");
                AllCodesAndNames.Add("TZA", "United Republic of Tanzania");
                AllCodesAndNames.Add("THA", "Thailand");
                AllCodesAndNames.Add("TLS", "Timor-Leste");
                AllCodesAndNames.Add("TGO", "Togo");
                AllCodesAndNames.Add("TKL", "Tokelau");
                AllCodesAndNames.Add("TON", "Tonga");
                AllCodesAndNames.Add("TTO", "Trinidad and Tobago");
                AllCodesAndNames.Add("TUN", "Tunisia");
                AllCodesAndNames.Add("TUR", "Turkey");
                AllCodesAndNames.Add("TKM", "Turkmenistan");
                AllCodesAndNames.Add("TCA", "Turks and Caicos Islands");
                AllCodesAndNames.Add("TUV", "Tuvalu");
                AllCodesAndNames.Add("UGA", "Uganda");
                AllCodesAndNames.Add("UKR", "Ukraine");
                AllCodesAndNames.Add("ARE", "United Arab Emirates");
                AllCodesAndNames.Add("GBR", "United Kingdom");
                AllCodesAndNames.Add("USA", "United States");
                AllCodesAndNames.Add("UMI", "United States Minor Outlying Islands");
                AllCodesAndNames.Add("URY", "Uruguay");
                AllCodesAndNames.Add("UZB", "Uzbekistan");
                AllCodesAndNames.Add("VUT", "Vanuatu");
                AllCodesAndNames.Add("VEN", "Venezuela");
                AllCodesAndNames.Add("VNM", "Viet Nam");
                AllCodesAndNames.Add("VGB", "British Virgin Islands");
                AllCodesAndNames.Add("VIR", "US Virgin Islands");
                AllCodesAndNames.Add("WLF", "Wallis and Futuna");
                AllCodesAndNames.Add("ESH", "Western Sahara");
                AllCodesAndNames.Add("YEM", "Yemen");
                AllCodesAndNames.Add("ZMB", "Zambia");
                AllCodesAndNames.Add("ZWE", "Zimbabwe");

                // Return the dictionary
                return AllCodesAndNames;
            }

            public struct SpecialCharacter
            {

                /// <summary>
                /// Used to return a character string resembling a check mark (character 0x221A / long division symbol). Will return an "X" if the character can't be created.
                /// </summary>
                public static string CheckMark()
                {
                    try
                    {
                        // Try to get special char
                        return System.Convert.ToChar(0x221A).ToString();
                    }
                    catch (Exception ex)
                    {
                        // Return "X" if it can't be created
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return "X";
                    }
                }

                /// <summary>
                /// Returns a quotation mark (") character string.
                /// </summary>
                public static string QuotationMark()
                {
                    // Return quote using escapes
                    return "\"";
                }

                /// <summary>
                /// Returns a carriage return character string.
                /// </summary>
                public static string CarriageReturn()
                {
                    // Return carriage return
                    return "\r";
                }

                /// <summary>
                /// Returns a line feed character string.
                /// </summary>
                public static string LineFeed()
                {
                    // Return line feed
                    return "\n";
                }

                /// <summary>
                /// Returns a two character string of carriage return and line feed.
                /// </summary>
                public static string CarriageReturnLineFeed()
                {
                    // Return carriage return and line feed
                    return CarriageReturn() + LineFeed();
                }

                /// <summary>
                /// Returns a tab character string.
                /// </summary>
                public static string Tab()
                {
                    // Return tab
                    return "\t";
                }

            }

        }

        public struct Clean
        {

            /// <summary>
            /// Used to make all line endings (CrLf, Cr, Lf) consistent in a given string.
            /// </summary>
            /// <param name="StringToClean">The source string normalize all line endings in.</param>
            /// <param name="NormalizedLineEndingToUse">OPTIONAL:  The character / string to convert all line endings to.</param>
            /// <returns>Returns the source string with all line endings changed to the specified character / string.</returns>
            public static string NormalizeLineEndings(string StringToClean, string NormalizedLineEndingToUse = "\r\n")
            {
                try
                {
                    // Return an empty string if nothing is passed in
                    if (StringToClean == null) return string.Empty;

                    // If there is a string, split it by all possible line endings
                    var SplitByLineEndings = StringToClean.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);

                    // Join back together with standardized line endings and return
                    return string.Join(NormalizedLineEndingToUse, SplitByLineEndings);
                }
                catch (Exception ex)
                {
                    // Return an empty string if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

            /// <summary>
            /// Used to remove all multiple / sequential space chars from a given string.
            /// </summary>
            /// <param name="StringToClean">The string to remove multi-spacing from.</param>
            /// <returns>Returns a string where all multiple space chars are replaced with a single space.</returns>
            public static string RemoveMultiSpacing(string StringToClean)
            {
                try
                {
                    // Return an empty string if nothing is passed in
                    if (string.IsNullOrEmpty(StringToClean) == true) return string.Empty;

                    // If something is passed in, loop until all double spaces are removed
                    while (StringToClean.Contains("  ") == true)
                    {
                        StringToClean = StringToClean.Replace("  ", " ");
                    }

                    // Return the string once all multi spaces are removed
                    return StringToClean;
                }
                catch (Exception ex)
                {
                    // Return an empty string if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

            /// <summary>
            /// Used to remove any "weird" characters from a string. The function will essentially return all alpha and numeric characters as well as standard punctuation and special characters.
            /// </summary>
            /// <param name="StringToClean">The string to clean.</param>
            /// <param name="AllowNewLinesCharacters">OPTIONAL:  Flag to indicate if new line characters should be kept.</param>
            /// <returns>Returns the original string without any "weird" characters.</returns>
            public static string RemoveNonStandardCharacters(string StringToClean, bool AllowNewLinesCharacters = false)
            {
                try
                {
                    // Make sure the string to clean has chars
                    if (string.IsNullOrEmpty(StringToClean) == true) return string.Empty;

                    // Create a list to hold all valid ASCII chars
                    var AllValidAsciiChars = new List<string>();
                    for (int i = 32; i <= 126; i++)
                    {
                        // Ascii alphanumeric and standard symbols are in range 32 to 126
                        AllValidAsciiChars.Add(System.Text.Encoding.ASCII.GetString(new[] { System.Convert.ToByte(i) }));
                    }

                    // Allow new line chars if user said to leave them in
                    if (AllowNewLinesCharacters == true)
                    {
                        AllValidAsciiChars.Add("\r");
                        AllValidAsciiChars.Add("\n");
                    }

                    // Get all characters of the source string
                    var AllCharactersInSourceString = StringToClean.ToCharArray().ToList();

                    // Get all characters from source string that are in the valid ascii char set and return
                    var AllValidCharsToMergeAndReturn = (from AllInfo in AllCharactersInSourceString where AllValidAsciiChars.Contains(AllInfo.ToString()) select AllInfo).ToList();

                    // Join the string back together without illegal chars and return
                    return string.Join(string.Empty, AllValidCharsToMergeAndReturn);
                }
                catch (Exception ex)
                {
                    // Return empty string to be safe if something fails (don't want to return a string that possibly has non-standard chars in it)
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

            /// <summary>
            /// Used to remove all letter characters (a-z and A-Z) from a given string.
            /// </summary>
            /// <param name="StringToClean">The string to clean.</param>
            /// <returns>Returns the StringToClean with all letter characters (upper and lower case) removed.</returns>
            public static string RemoveLetterCharacters(string StringToClean)
            {
                try
                {
                    // Save string to return as upper case version of input string so only upper case letters need to be looped through / removed
                    string StringToReturn = StringToClean.ToUpper();

                    // Loop through all ASCII chars and remove A-Z
                    for (int i = 65; i <= 90; i++)
                    {
                        StringToReturn = StringToReturn.Replace(System.Convert.ToChar(i).ToString(), string.Empty);
                    }

                    // Return the string when all letters are removed
                    return StringToReturn;
                }
                catch (Exception ex)
                {
                    // Remove an empty string if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

            /// <summary>
            /// Used to remove all characters from an input string that aren't either a letter or a number.
            /// </summary>
            /// <param name="StringToClean">The string to clean.</param>
            /// <returns>Returns the StringToClean with all non-letters and non-numbers removed.</returns>
            public static string RemoveEverythingExceptLettersAndNumbers(string StringToClean)
            {
                try
                {
                    // Return empty string if the input is missing
                    if (string.IsNullOrEmpty(StringToClean) == true)
                    {
                        return string.Empty;
                    }

                    // Create list to hold all good chars
                    var AllGoodChars = new List<System.Char>();

                    // Add all upper case
                    for (int i = 65; i <= 90; i++)
                    {
                        AllGoodChars.Add(System.Convert.ToChar(i));
                    }

                    // Add all lower case
                    for (int i = 97; i <= 122; i++)
                    {
                        AllGoodChars.Add(System.Convert.ToChar(i));
                    }

                    // Add all number chars
                    for (int i = 48; i <= 57; i++)
                    {
                        AllGoodChars.Add(System.Convert.ToChar(i));
                    }

                    // Get all chars in the input string
                    var AllChars = StringToClean.ToCharArray();

                    // Get all alpha numeric chars
                    var AlphaNumericChars = (from AllInfo in AllChars where AllGoodChars.Contains(AllInfo) select AllInfo).ToList();

                    // Join and return the info
                    return string.Join(string.Empty, AlphaNumericChars);
                }
                catch (Exception ex)
                {
                    // Return an empty string if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

            /// <summary>
            /// Used to remove all characters from an input string that aren't numbers.
            /// </summary>
            /// <param name="StringToClean">The string to clean.</param>
            /// <returns>Returns the StringToClean with all non-numbers removed.</returns>
            public static string RemoveEverythingExceptNumbers(string StringToClean)
            {
                try
                {
                    // Split the string up to chars
                    var StringAsCharArray = StringToClean.ToCharArray();

                    // Create list to join and return
                    var ListToReturn = new List<string>();

                    // Loop through chars and save only numerics
                    foreach (var ITEM in StringAsCharArray)
                    {
                        if (StaticMethods.Strings.TypeCheck.IsNumeric(ITEM.ToString()) == true)
                        {
                            // Save numbers only
                            ListToReturn.Add(ITEM.ToString());
                        }
                    }

                    // Return the list
                    return string.Join(string.Empty, ListToReturn);
                }
                catch (Exception ex)
                {
                    // Return an empty string if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

            /// <summary>
            /// Used to perform a ToString() on a specified double and ensure that the result is NOT in scientific (3E-5) notation.
            /// </summary>
            /// <param name="DoubleToClean">The double to convert to string.</param>
            /// <returns>Returns the number as a string NOT in scientific notation if successful. Returns an empty string in all other conditions.</returns>
            public static string RemoveScientificNotation(double DoubleToClean)
            {
                try
                {
                    // Convert the number to a decimal and ToString() it to remove scientific notation
                    return new System.Decimal(DoubleToClean).ToString();
                }
                catch (Exception ex)
                {
                    // Return empty string if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

            /// <summary>
            /// Used to format a string such that the first letter of all words in the string will be uppercase (e.g. "some string" to "Some String") - sometimes called TitleCasing.
            /// </summary>
            /// <param name="OriginalString">The source string to format.</param>
            /// <returns>Returns the source string with the first letter of all words in uppercase if the method executes correctly. Returns an empty string in all other conditions.</returns>
            public static string UpperCaseFirstLetterOfAllWords(string OriginalString)
            {
                try
                {
                    // Return an empty string if there is no source string
                    if (string.IsNullOrEmpty(OriginalString) == true) return string.Empty;

                    // If there is an original string, create new text helper to format the string
                    var TextHelper = new System.Globalization.CultureInfo("en-US", false);

                    // Format and return the string
                    return TextHelper.TextInfo.ToTitleCase(OriginalString);
                }
                catch (Exception ex)
                {
                    // Return empty string if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

        }

        public struct Extract
        {

            /// <summary>
            /// Used to get all the first (if any) quoted text within an input string (i.e. There names were ''Bob'' and ''Bill'' -> this would return 'Bob').
            /// </summary>
            /// <param name="StringToParse">The string to parse / find quoted text within.</param>
            /// <param name="RemoveEnclosingQuotesBeforeReturning">Flag to indicate if the matching text should have the '' marks removed before returning.</param>
            /// <returns>Return the first quoted text found if successful. Returns an empty string if there is no quoted text or an error occurs.</returns>
            public static string GetQuotedString(string StringToParse, bool RemoveEnclosingQuotesBeforeReturning)
            {
                // Use duplicate / underlying method from RegEx
                return RegEx.GetQuotedString(StringToParse, RemoveEnclosingQuotesBeforeReturning);
            }
            /// <summary>
            /// Used to get all quoted text within an input string (i.e. There names were ''Bob'' and ''Bill'' -> this would return 'Bob' and 'Bill').
            /// </summary>
            /// <param name="StringToParse">The string to parse / find quoted text within.</param>
            /// <param name="RemoveEnclosingQuotesBeforeReturning">Flag to indicate if the matching text should have the '' marks removed before returning.</param>
            /// <returns>Return a list of all matching / quoted text found if successful. Return NOTHING if an error occurs.</returns>
            public static List<string> GetQuotedStrings(string StringToParse, bool RemoveEnclosingQuotesBeforeReturning)
            {
                // Use duplicate / underlying method from RegEx
                return RegEx.GetQuotedStrings(StringToParse, RemoveEnclosingQuotesBeforeReturning);
            }

            public struct Substring
            {

                /// <summary>
                /// Used to get and return the left 'n' characters of a string.
                /// </summary>
                /// <param name="OriginalString">The original string to return a substring of.</param>
                /// <param name="MaxCharactersToGet">The maximum number of characters from the first character in the original source string to return in the substring.</param>
                /// <returns>Returns a substring of no greater than requested length starting from the first character in the source string (note that the string may be shorter than the requested length if the source string is shorter than the requested length). Returns an empty string if an error occurs while parsing the string or the source string is empty.</returns>
                public static string Left(string OriginalString, int MaxCharactersToGet)
                {
                    try
                    {
                        // Return an empty string if nothing is passed in
                        if (string.IsNullOrEmpty(OriginalString) == true) return string.Empty;

                        // Return an empty string if the number of chars is invalid
                        if (MaxCharactersToGet <= 0) return string.Empty;

                        // If inputs are good, return the source string if it's already short enough
                        if (OriginalString.Length <= MaxCharactersToGet) return OriginalString;

                        // If the string is longer then the requested length, trim it down
                        return OriginalString.Substring(0, MaxCharactersToGet);
                    }
                    catch (Exception ex)
                    {
                        // Return an empty string if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return string.Empty;
                    }
                }

                /// <summary>
                /// Used to get and return the right 'n' characters of a string.
                /// </summary>
                /// <param name="OriginalString">The original string to return a substring of.</param>
                /// <param name="MaxCharactersToGet">The maximum number of characters from the last character in the original source string to return in the substring.</param>
                /// <returns>Returns a substring of no greater than requested length starting from the last character in the source string (note that the string may be shorter than the requested length if the source string is shorter than the requested length). Returns an empty string if an error occurs while parsing the string or the source string is empty.</returns>
                public static string Right(string OriginalString, int MaxCharactersToGet)
                {
                    try
                    {
                        // Return an empty string if nothing is passed in
                        if (string.IsNullOrEmpty(OriginalString) == true) return string.Empty;

                        // Return an empty string if the number of chars is invalid
                        if (MaxCharactersToGet <= 0) return string.Empty;

                        // If inputs are good, return the source string if it's already short enough
                        if (OriginalString.Length <= MaxCharactersToGet) return OriginalString;

                        // If the string is longer then the requested length, trim it down
                        return OriginalString.Substring(OriginalString.Length - MaxCharactersToGet, MaxCharactersToGet);
                    }
                    catch (Exception ex)
                    {
                        // Return an empty string if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return string.Empty;
                    }
                }

                /// <summary>
                /// Used to get and return a substring of the original string starting from the requested start index of the original string.
                /// </summary>
                /// <param name="OriginalString">The original string to return a substring of.</param>
                /// <param name="CharacterStartIndex">The zero based start index to get the substring from in the original string.</param>
                /// <returns>Returns a substring of the original string starting from the requested start index of the original string. Returns an empty string if an error occurs while parsing the string or the source string is empty.</returns>
                public static string Mid(string OriginalString, int CharacterStartIndex)
                {
                    try
                    {
                        // Return an empty string if nothing is passed in
                        if (string.IsNullOrEmpty(OriginalString) == true) return string.Empty;

                        // Return an empty string if index is invalid
                        if (CharacterStartIndex < 0 || CharacterStartIndex > OriginalString.Length - 1) return string.Empty;

                        // If string is good, return substring
                        return OriginalString.Substring(CharacterStartIndex);
                    }
                    catch (Exception ex)
                    {
                        // Return an empty string if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return string.Empty;
                    }
                }

                /// <summary>
                /// Used to get and return a substring of the original string starting from the requested start index of the original string and capped at a requested length.
                /// </summary>
                /// <param name="OriginalString">The original string to return a substring of.</param>
                /// <param name="CharacterStartIndex">The zero based start index to get the substring from in the original string.</param>
                /// <param name="LengthOfStringToReturn">The maximum length to cap the returned string to.</param>
                /// <returns>Returns a substring of the original string starting from the requested start index of the original string and capped at the requested length (note that the length may be shorter if the source string is shorter then the requested length). Returns an empty string if an error occurs while parsing the string or the source string is empty.</returns>
                public static string Mid(string OriginalString, int CharacterStartIndex, int LengthOfStringToReturn)
                {
                    try
                    {
                        // First, get the substring from the start index
                        string StringFromStartIndex = Mid(OriginalString, CharacterStartIndex);

                        // Return an empty string if nothing comes back
                        if (string.IsNullOrEmpty(StringFromStartIndex) == true) return string.Empty;

                        // If there's a string returned, return the left chunk up to the length
                        return Left(StringFromStartIndex, LengthOfStringToReturn);
                    }
                    catch (Exception ex)
                    {
                        // Return an empty string if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return string.Empty;
                    }
                }

            }

        }

        public struct Convert
        {

            /// <summary>
            /// Enum to hold available text encoding options.
            /// </summary>
            public enum TextEncodingOptions
            {
                ASCII,
                UTF8,
                Unicode
            }

            /// <summary>
            /// Enum to hold options for string formats.
            /// </summary>
            public enum FormatOptions
            {
                Currency0Decimal = 0,
                Currency1Decimal = 1,
                Currency2Decimal = 2,
                Number0Decimal = 3,
                Number1Decimal = 4,
                Number2Decimal = 5,
                Number3Decimal = 6,
                Number4Decimal = 7
            }

            /// <summary>
            /// Used to return the string format identifier needed to use String.Format.
            /// </summary>
            /// <param name="FormatType">Parameter to request the type of formatting string to return.</param>
            /// <returns>Returns the specified string formatting command.</returns>
            public static string GetFormatIdentifier(FormatOptions FormatType)
            {
                // Figure out which format they want
                switch (FormatType)
                {
                    case FormatOptions.Currency0Decimal:
                        return "C0";
                    case FormatOptions.Currency1Decimal:
                        return "C1";
                    case FormatOptions.Currency2Decimal:
                        return "C2";
                    case FormatOptions.Number0Decimal:
                        return "#";
                    case FormatOptions.Number1Decimal:
                        return "#.0";
                    case FormatOptions.Number2Decimal:
                        return "#.00";
                    case FormatOptions.Number3Decimal:
                        return "#.000";
                    case FormatOptions.Number4Decimal:
                        return "#.0000";
                    default:
                        return string.Empty;
                }
            }

            /// <summary>
            /// Used to convert dates into sortable strings in YYYY-MM-DD[-HH-MM-SS][-mmm] format.
            /// </summary>
            /// <param name="DateToConvert">The date to convert to a sortable string.</param>
            /// <param name="IncludeTime">Flag to indicate if the HHMMSS time should be included.</param>
            /// <param name="IncludeMilliseconds">OPTIONAL:  Flag to indicate if the 3 digit milliseconds should be included (will be ignored if time is not included).</param>
            /// <returns>Returns a sortable string representation of the date.</returns>
            public static string DateToSortableString(DateTime DateToConvert, bool IncludeTime, bool IncludeMilliseconds = false)
            {
                // Create the YYYY-MM-DD string to return
                string StringToReturn = DateToConvert.Year.ToString() + "-" + Transform.PadNumberLeadingDigits(DateToConvert.Month.ToString(), 2) + "-" + Transform.PadNumberLeadingDigits(DateToConvert.Day.ToString(), 2);

                // Check if appending time
                if (IncludeTime == true)
                {
                    // If so, add time components as well
                    StringToReturn += "-" + Transform.PadNumberLeadingDigits(DateToConvert.Hour.ToString(), 2) + "-" + Transform.PadNumberLeadingDigits(DateToConvert.Minute.ToString(), 2) + "-" + Transform.PadNumberLeadingDigits(DateToConvert.Second.ToString(), 2);

                    // Check if appending milliseconds (only applicable if time is included)
                    if (IncludeMilliseconds == true)
                    {
                        // If so, append padded milliseconds
                        StringToReturn += "-" + Transform.PadNumberLeadingDigits(DateToConvert.Millisecond.ToString(), 3);
                    }
                }

                // Return the string
                return StringToReturn;
            }

            /// <summary>
            /// Used to convert a sortable date string back to a date.
            /// </summary>
            /// <param name="StringToConvert">The date string to convert back to a date.</param>
            /// <returns>Returns the date if the conversion works. Returns NOTHING if the string cannot be converted back.</returns>
            public static Nullable<DateTime> SortableStringToDate(string StringToConvert)
            {
                try
                {
                    // Return nothing if input is bad
                    if (string.IsNullOrEmpty(StringToConvert) == true) return null;

                    // If input is possibly good, create vars to hold the Y, M, D, h, m, s, mS values from the string
                    int Year = 0;
                    int Month = 0;
                    int Day = 0;
                    int Hour = 0;
                    int Minute = 0;
                    int Second = 0;
                    int Millisecond = 0;

                    // Save the possible / known formats that we have a good input
                    const string YYYYMMDDHHMMSSmmm = "YYYYMMDDHHMMSSmmm";
                    const string YYYYMMDDHHMMSS = "YYYYMMDDHHMMSS";
                    const string YYYYMMDD = "YYYYMMDD";

                    // Remove any delimiting chars that may be in string
                    StringToConvert = StringToConvert.Replace("-", string.Empty).Replace(":", string.Empty).Replace(".", string.Empty);

                    // Save the current length of the string
                    int RemainingLengthOfString = StringToConvert.Length;

                    // Return nothing if it's not a match
                    if (new[] { YYYYMMDDHHMMSSmmm.Length, YYYYMMDDHHMMSS.Length, YYYYMMDD.Length }.ToList().Contains(RemainingLengthOfString) == false)
                    {
                        return null;
                    }

                    // If it's one of the known lengths, get YYYY MM DD (those should always be there)
                    Year = System.Convert.ToInt32(StringToConvert.Substring(0, 4));
                    Month = System.Convert.ToInt32(StringToConvert.Substring(4, 2));
                    Day = System.Convert.ToInt32(StringToConvert.Substring(6, 2));

                    // Check if we've got enough to bolt on H:M:S
                    if (StringToConvert.Length >= YYYYMMDDHHMMSS.Length)
                    {
                        // If so, get those too
                        Hour = System.Convert.ToInt32(StringToConvert.Substring(8, 2));
                        Minute = System.Convert.ToInt32(StringToConvert.Substring(10, 2));
                        Second = System.Convert.ToInt32(StringToConvert.Substring(12, 2));
                    }

                    // Check if there's enough for mS
                    if (StringToConvert.Length >= YYYYMMDDHHMMSSmmm.Length)
                    {
                        // If so, get mS
                        Millisecond = System.Convert.ToInt32(StringToConvert.Substring(14, 3));
                    }

                    // Once we've got everything, return date
                    return new System.DateTime(Year, Month, Day, Hour, Minute, Second, Millisecond);
                }
                catch (Exception ex)
                {
                    // Use nothing if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }

            /// <summary>
            /// Used to attempt to convert a string representation of an enum to the enum value.
            /// </summary>
            /// <param name="StringToConvert">The string to convert.</param>
            /// <param name="EnumType">The enum type to convert to.</param>
            /// <returns>Returns the converted enum if successful. Returns NOTHING in all other conditions.</returns>
            public static object ToEnum(string StringToConvert, System.Type EnumType)
            {
                try
                {
                    // Return nothing if input is bad
                    if (string.IsNullOrEmpty(StringToConvert) == true) return null;

                    // If input looks good, attempt conversion
                    return Enum.Parse(EnumType, StringToConvert);
                }
                catch (Exception ex)
                {
                    // Return nothing if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }

            /// <summary>
            /// Used to convert a number to a string representation of the number with the specified amount of decimal points.
            /// </summary>
            /// <param name="NumberToConvert">The number to convert to a string.</param>
            /// <param name="NumberOfDigitsAfterDecimal">The number of decimal points to display in the returned number string.</param>
            /// <returns>Returns the string representation of the number with the requested amount of decimal points.</returns>
            public static string ToNumberString(double NumberToConvert, int NumberOfDigitsAfterDecimal)
            {
                try
                {
                    // Return empty string if decimal points aren't valid
                    if (NumberOfDigitsAfterDecimal < 0) return string.Empty;

                    // If the decimal points are valid, round the number first
                    NumberToConvert = System.Math.Round(NumberToConvert, NumberOfDigitsAfterDecimal, MidpointRounding.AwayFromZero);

                    // Create format string that will be required by ToString()
                    string FormatStyle = "#";

                    // Check if this is a whole number or a decimal
                    if (NumberOfDigitsAfterDecimal > 0)
                    {
                        // If it's a decimal, append dot and number of decimal points to syntax
                        FormatStyle += "." + new string(System.Convert.ToChar("0"), NumberOfDigitsAfterDecimal);
                    }

                    // Get the number as a string
                    string NumberAsString = NumberToConvert.ToString(FormatStyle, System.Globalization.CultureInfo.CurrentCulture);

                    // If it's between +/ -1, it will be missing leading zero, need to fix that(will either be.xyz or -.xyz)
                    if (NumberAsString.StartsWith(".") == true) NumberAsString = NumberAsString.Replace(".", "0.");
                    if (NumberAsString.StartsWith("-.") == true) NumberAsString = NumberAsString.Replace("-.", "-0.");

                    // Return number when formatted
                    return NumberAsString;
                }
                catch (Exception ex)
                {
                    // Return empty string if conversion fails somehow
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

            /// <summary>
            /// Used to convert a number to a string representation of the number formatted as currency (e.g. "41.3" to "$41.30").
            /// </summary>
            /// <param name="NumberToConvert">The number to convert to a string.</param>
            /// <param name="NumberOfDigitsAfterDecimal">The number of decimal points to display in the returned currency string.</param>
            /// <param name="UseDigitGroupingsForLargeNumbers">Flag to indicate if groupings should be used for large numbers (e.g. "6251253.2" w/ False="$6251253.20", w/ True="$6,251,253.20").</param>
            /// <returns>Returns the currency string representation of the number with the requested amount of decimal points and optional digit grouping.</returns>
            public static string ToCurrencyString(double NumberToConvert, int NumberOfDigitsAfterDecimal, bool UseDigitGroupingsForLargeNumbers)
            {
                try
                {
                    // Return empty string if decimal points aren't valid
                    if (NumberOfDigitsAfterDecimal < 0) return string.Empty;

                    // If the decimal points are valid, round the number first
                    NumberToConvert = System.Math.Round(NumberToConvert, NumberOfDigitsAfterDecimal, MidpointRounding.AwayFromZero);

                    // Create format string that will be required by ToString()
                    string FormatStyle = "C" + NumberOfDigitsAfterDecimal.ToString();

                    // Get the converted number
                    string ConvertedString = NumberToConvert.ToString(FormatStyle, System.Globalization.CultureInfo.CurrentCulture);

                    // Remove commas if not grouping
                    if (UseDigitGroupingsForLargeNumbers == false)
                    {
                        ConvertedString = ConvertedString.Replace(",", string.Empty);
                    }

                    // Return the string
                    return ConvertedString;
                }
                catch (Exception ex)
                {
                    // Return empty string if conversion fails somehow
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

            /// <summary>
            /// Used to split a string into a list of strings by a configurable string delimiter.
            /// </summary>
            /// <param name="StringToSplit">The full string to split into a list.</param>
            /// <param name="DelimiterToSplitBy">The delimiter to split the string by.</param>
            /// <returns>Returns a list of all sub strings once they are split by the specified delimiter.</returns>
            public static List<string> StringToListOfStrings(string StringToSplit, string DelimiterToSplitBy)
            {
                // Create list to return
                var ListToReturn = new List<string>();

                // If nothing is passed in, return empty list
                if (StringToSplit == null) return ListToReturn;

                // If there is info, split it
                ListToReturn = StringToSplit.Split(new[] { StringToSplit }, StringSplitOptions.None).ToList();

                // Return the list
                return ListToReturn;
            }

            /// <summary>
            /// Used to convert an ASCII string into a list of bytes.
            /// </summary>
            /// <param name="StringToConvert">The string to convert to bytes.</param>
            /// <returns>Returns a list of bytes. Returns an empty list if the conversion fails.</returns>
            public static List<byte> ASCIIStringToBytes(string StringToConvert)
            {
                try
                {
                    // Create byte array
                    var ByteArray = System.Text.Encoding.ASCII.GetBytes(StringToConvert);

                    // Return empty list if there's no bytes
                    if (ByteArray == null) return new List<byte>();

                    // If there is data, convert it to list and return it
                    return ByteArray.ToList();
                }
                catch (Exception ex)
                {
                    // Return empty list if conversion fails
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return new List<byte>();
                }
            }

            /// <summary>
            /// Used to convert a Unicode string into a list of bytes.
            /// </summary>
            /// <param name="StringToConvert">The string to convert to bytes.</param>
            /// <returns>Returns a list of bytes. Returns an empty list if the conversion fails.</returns>
            public static List<byte> UnicodeStringToBytes(string StringToConvert)
            {
                try
                {
                    // Create byte array
                    var ByteArray = System.Text.Encoding.Unicode.GetBytes(StringToConvert);

                    // Return empty list if there's no bytes
                    if (ByteArray == null) return new List<byte>();

                    // If there is data, convert it to list and return it
                    return ByteArray.ToList();
                }
                catch (Exception ex)
                {
                    // Return empty list if conversion fails
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return new List<byte>();
                }
            }

            /// <summary>
            /// Used to convert a byte to an ASCII string character.
            /// </summary>
            /// <param name="ByteToConvert">The byte to convert to string.</param>
            /// <returns>Returns a string representation of the byte if the conversion works. Returns an empty string if the conversion fails.</returns>
            public static string ByteToASCIIString(byte ByteToConvert)
            {
                // Add byte to list and return result of common method
                return BytesToASCIIString(new[] { ByteToConvert }.ToList());
            }

            /// <summary>
            /// Used to convert a list of bytes to an ASCII string.
            /// </summary>
            /// <param name="BytesToConvert">The list of bytes to convert to string.</param>
            /// <returns>Returns a string representation of the bytes if the conversion works. Returns an empty string if the conversion fails.</returns>
            public static string BytesToASCIIString(IEnumerable<byte> BytesToConvert)
            {
                try
                {
                    // Try to convert and return the result
                    return System.Text.Encoding.ASCII.GetString(BytesToConvert.ToArray());
                }
                catch (Exception ex)
                {
                    // Return an empty string if conversion fails
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

            /// <summary>
            /// Used to convert a byte to a Unicode string character.
            /// </summary>
            /// <param name="ByteToConvert">The byte to convert to string.</param>
            /// <returns>Returns a string representation of the byte if the conversion works. Returns an empty string if the conversion fails.</returns>
            public static string ByteToUnicodeString(byte ByteToConvert)
            {
                // Add byte to list and return result of common method
                return BytesToUnicodeString(new[] { ByteToConvert }.ToList());
            }

            /// <summary>
            /// Used to convert a list of bytes to a Unicode string.
            /// </summary>
            /// <param name="BytesToConvert">The list of bytes to convert to string.</param>
            /// <returns>Returns a string representation of the bytes if the conversion works. Returns an empty string if the conversion fails.</returns>
            public static string BytesToUnicodeString(IEnumerable<byte> BytesToConvert)
            {
                try
                {
                    // Try to convert and return the result
                    return System.Text.Encoding.Unicode.GetString(BytesToConvert.ToArray());
                }
                catch (Exception ex)
                {
                    // Return an empty string if conversion fails
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

            /// <summary>
            /// Used to convert a boolean value to a string (True = "Yes", False = "No").
            /// </summary>
            /// <param name="BooleanToConvert">The boolean value to convert to a string.</param>
            /// <returns>Returns "Yes" for True and "No" for False.</returns>
            public static string BooleanToYesNoString(bool BooleanToConvert)
            {
                // Convert the boolean to a yes / no string and return it
                if (BooleanToConvert == true)
                {
                    return "Yes";
                }
                else
                {
                    return "No";
                }
            }

            /// <summary>
            /// Used to convert a "Yes" / "No" string to a boolean value (ignores case).
            /// </summary>
            /// <param name="YesNoStringToConvert">The string to convert to a boolean.</param>
            /// <returns>Returns TRUE if the string is "Yes". Returns FALSE if the string is "No". Returns NULL if the string is an unrecognized format.</returns>
            public static Nullable<bool> YesNoStringToBoolean(string YesNoStringToConvert)
            {
                // Return nothing if string is empty
                if (string.IsNullOrEmpty(YesNoStringToConvert) == true) return null;

                // If there is a string, check for yes / no
                switch (YesNoStringToConvert.ToUpper())
                {
                    case "YES":
                        // Return TRUE for yes
                        return true;
                    case "NO":
                        // Return FALSE for no
                        return false;
                    default:
                        // Return null if not recognized
                        return null;
                }
            }

            /// <summary>
            /// Used to convert a "Yes" / "No" string to a boolean value (ignores case).
            /// </summary>
            /// <param name="YesNoStringToConvert">The string to convert to a boolean.</param>
            /// <param name="ValueToReturnForUnrecognizedString">Used to specify what you want returned if the source string is neither Yes or No.</param>
            /// <returns>Returns TRUE if the string is "Yes". Returns FALSE if the string is "No". Returns the specified value for unrecognized strings if the string to convert isn't "Yes" or "No".</returns>
            public static bool YesNoStringToBoolean(string YesNoStringToConvert, bool ValueToReturnForUnrecognizedString)
            {
                // Return default value if string is empty
                if (string.IsNullOrEmpty(YesNoStringToConvert) == true)
                {
                    return ValueToReturnForUnrecognizedString;
                }

                // If there is a string, check for yes / no
                switch (YesNoStringToConvert.ToUpper())
                {
                    case "YES":
                        // Return TRUE for yes
                        return true;
                    case "NO":
                        // Return FALSE for no
                        return false;
                    default:
                        // Return default value if not recognized
                        return ValueToReturnForUnrecognizedString;
                }
            }

            /// <summary>
            /// Used to convert an ASCII string to a Base64 string.
            /// </summary>
            /// <param name="StringToConvert">The source ASCII string to convert.</param>
            /// <returns>Returns the Base64 representation of the string if the conversion is successful. Returns an empty string if the source string is empty or the conversion fails.</returns>
            public static string ToBase64(string StringToConvert)
            {
                // Use underlying method and return result
                return ToBase64(StringToConvert, TextEncodingOptions.ASCII, false);
            }
            /// <summary>
            /// Used to convert a string to a Base64 string.
            /// </summary>
            /// <param name="StringToConvert">The source string to convert.</param>
            /// <param name="TextEncodingToUse">The encoding format of the source string to encode in Base64.</param>
            /// <param name="UseUrlSafeFormatting">Flag to indicate URL safe Base64 encoding should be used (+ -> -, / -> _, ='s removed).</param>
            /// <returns>Returns the Base64 representation of the string if the conversion is successful. Returns an empty string if the source string is empty or the conversion fails.</returns>
            public static string ToBase64(string StringToConvert, TextEncodingOptions TextEncodingToUse, bool UseUrlSafeFormatting)
            {
                try
                {
                    // Return empty string if source is empty
                    if (string.IsNullOrEmpty(StringToConvert) == true) return string.Empty;

                    // Create string to hold the converted base64
                    string Base64 = string.Empty;

                    // Key off of the encoding format selected
                    switch (TextEncodingToUse)
                    {
                        case TextEncodingOptions.ASCII:
                            // Use ascii format
                            Base64 = System.Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes(StringToConvert));
                            break;
                        case TextEncodingOptions.UTF8:
                            // Use UTF-8 format
                            Base64 = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(StringToConvert));
                            break;
                        case TextEncodingOptions.Unicode:
                            // Use unicode format
                            Base64 = System.Convert.ToBase64String(System.Text.Encoding.Unicode.GetBytes(StringToConvert));
                            break;
                        default:
                            // For unimplemented, just leave as empty string
                            Base64 = string.Empty;
                            break;
                    }

                    // Perform conversion to URL safe Base64 if requested
                    if (UseUrlSafeFormatting == true)
                    {
                        Base64 = Base64ToUrlSafeBase64(Base64);
                    }

                    // Return the string
                    return Base64;
                }
                catch (Exception ex)
                {
                    // Return an empty string if conversion fails
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

            /// <summary>
            /// Used to convert a Base64 string to an ASCII string.
            /// </summary>
            /// <param name="Base64StringToConvert">The source Base64 string to convert.</param>
            /// <returns>Returns the ASCII representation of the string if the conversion is successful. Returns an empty string if the source string is empty or the conversion fails.</returns>
            public static string FromBase64(string Base64StringToConvert)
            {
                // Use underlying method and return result
                return FromBase64(Base64StringToConvert, TextEncodingOptions.ASCII);
            }
            /// <summary>
            /// Used to convert a Base64 string to a human-readable string.
            /// </summary>
            /// <param name="Base64StringToConvert">The source Base64 string to convert.</param>
            /// <param name="TextEncodingToUse">The encoding to use for the returned human-readable string.</param>
            /// <returns>Returns the human-readable representation of the string if the conversion is successful. Returns an empty string if the source string is empty or the conversion fails.</returns>
            public static string FromBase64(string Base64StringToConvert, TextEncodingOptions TextEncodingToUse)
            {
                try
                {
                    // Return empty string if source is empty
                    if (string.IsNullOrEmpty(Base64StringToConvert) == true) return string.Empty;

                    // It's possible the input Base64 string might be URL safe, so we should make it standard Base64 chars first (won't hurt if it's already normal Base64)
                    Base64StringToConvert = UrlSafeBase64ToStandardBase64(Base64StringToConvert);

                    // Create var to hold the string to return
                    string HumanReadableString = string.Empty;

                    // Key off the text encoding they requested
                    switch (TextEncodingToUse)
                    {
                        case TextEncodingOptions.ASCII:
                            // Use ascii formatting
                            HumanReadableString = System.Text.Encoding.ASCII.GetString(System.Convert.FromBase64String(Base64StringToConvert));
                            break;
                        case TextEncodingOptions.UTF8:
                            // Use UTF-8 formatting
                            HumanReadableString = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(Base64StringToConvert));
                            break;
                        case TextEncodingOptions.Unicode:
                            // Use unicode formatting
                            HumanReadableString = System.Text.Encoding.Unicode.GetString(System.Convert.FromBase64String(Base64StringToConvert));
                            break;
                        default:
                            // Ensure string is empty if we have an unknown encoding specified
                            HumanReadableString = string.Empty;
                            break;
                    }

                    // Return the string after conversion
                    return HumanReadableString;
                }
                catch (Exception ex)
                {
                    // Return empty string if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

            /// <summary>
            /// Used to convert a standard Base64 string to the equivalent URL safe Base64 string (+ -> -, / -> _, ='s removed).
            /// </summary>
            /// <param name="Base64StringToConvert">The standard Base64 string to convert.</param>
            /// <returns>Returns a URL safe version of the Base64 string is successfully converted. Returns an empty string in all other conditions.</returns>
            public static string Base64ToUrlSafeBase64(string Base64StringToConvert)
            {
                try
                {
                    // Return an empty string if input is missing
                    if (string.IsNullOrEmpty(Base64StringToConvert) == true) return string.Empty;

                    // Save a copy of the string to manipulate
                    string UrlSafeBase64 = Base64StringToConvert;

                    // URL safe encoding removes equal sign padding and replaces + with - and / with _
                    UrlSafeBase64 = UrlSafeBase64.Replace("=", string.Empty);
                    UrlSafeBase64 = UrlSafeBase64.Replace("+", "-");
                    UrlSafeBase64 = UrlSafeBase64.Replace("/", "_");

                    // Return the string
                    return UrlSafeBase64;
                }
                catch (Exception ex)
                {
                    // Return an empty string if something fails
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

            /// <summary>
            /// Used to convert a URL safe Base64 string to standard Base64.
            /// </summary>
            /// <param name="UrlSafeBase64ToConvert">The URL safe Base64 string to convert back to standard Base64.</param>
            /// <returns>Returns a standard Base64 string is successfully converted. Returns an empty string in all other conditions.</returns>
            public static string UrlSafeBase64ToStandardBase64(string UrlSafeBase64ToConvert)
            {
                try
                {
                    // Create string to hold the converted standard Base64
                    string StandardBase64 = UrlSafeBase64ToConvert;

                    // Remove URL safe characters with the standard Base64 chars
                    StandardBase64 = StandardBase64.Replace("-", "+");
                    StandardBase64 = StandardBase64.Replace("_", "/");

                    // URL safe Base64 also removes all equal sign padding...ensure we're a multiple of 4 chars long (all standard Base64 is a multiple of 4 chars in length)
                    for (int i = 1; i <= 3; i++)   // At most you would need 3 equal signs bolted to the end
                    {
                        // Check if we're a multiple of 4 chars long
                        if (StandardBase64.Length % 4 != 0)
                        {
                            // If not, bolt on an equal sign
                            StandardBase64 += "=";
                        }
                        else
                        {
                            // If we're good, just bail out of loop
                            break;
                        }
                    }

                    // Return the string once we've fixed it
                    return StandardBase64;
                }
                catch (Exception ex)
                {
                    // Return an empty string if something fails
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

            /// <summary>
            /// Used to convert a number from Base10 / decimal to a fictitious Base36 (0-9, A-Z) extended hex format.
            /// </summary>
            /// <param name="NumberToConvert">The number to convert to Base36.</param>
            /// <returns>Returns the source number converted to Base36 if successful. Returns an empty string in all other conditions.</returns>
            public static string ToBase36(UInt64 NumberToConvert)
            {
                try
                {
                    // Get all base 36 values / chars
                    var AllBase36Chars = GetAllBase36Chars();

                    // Create var to hold the max number of chars that will be needed
                    int NumberOfCharsNeeded = 0;

                    // Create var to hold the cumulative max value possible as we add a new character
                    UInt64 MaxPossibleValueWithCurrentCharCount = 0;

                    // Loop through and figure out how many chars we're going to need for the full conversion
                    for (int i = 1; i <= UInt16.MaxValue; i++)
                    {
                        // Compute the max possible value for adding another character (subtract 1 for 0 base numbers)
                        UInt64 MaxValueForThisCharCount = System.Convert.ToUInt64(36 * (System.Math.Pow(36, (i - 1))) - 1);

                        // Add it to the cumulative count
                        MaxPossibleValueWithCurrentCharCount += MaxValueForThisCharCount;

                        // If this is big enough to hold our number, save char count needed and bail
                        if (MaxPossibleValueWithCurrentCharCount >= NumberToConvert)
                        {
                            NumberOfCharsNeeded = i;
                            break;
                        }
                    }

                    // Now that we know how many chars we need, create a list to hold all chars as we build the string
                    var AllCharsForFinalNumberString = new List<string>(NumberOfCharsNeeded);

                    // Create var to hold the running value of the number we're building
                    UInt64 RunningValueOfNumber = 0;

                    // Loop through and build string
                    for (int i = NumberOfCharsNeeded; i >= 1; i += -1)
                    {
                        // Loop through all chars find the first value that's the same size or smaller
                        for (int j = AllBase36Chars.Count - 1; j >= 0; j += -1)
                        {
                            // Save current value at this index
                            UInt64 CurrentValueOfThisCharAtThisPosition = System.Convert.ToUInt64(j * (System.Math.Pow(36, (i - 1))));

                            // If it's too big, we have to keep decrementing
                            if (CurrentValueOfThisCharAtThisPosition + RunningValueOfNumber > NumberToConvert)
                            {
                                continue;
                            }

                            // When we hit the first number that's smaller, use that char
                            AllCharsForFinalNumberString.Add(GetAllBase36Chars()[j]);

                            // Inc the current running value as we build the string and bail out of loop for next most sig fig
                            RunningValueOfNumber += CurrentValueOfThisCharAtThisPosition;
                            break;
                        }
                    }

                    // Once we get through, return empty string if we didn't tally the number correctly
                    if (RunningValueOfNumber != NumberToConvert) return string.Empty;

                    // If it's good, join and return it
                    return string.Join(string.Empty, AllCharsForFinalNumberString);
                }
                catch (Exception ex)
                {
                    // Return empty string if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }
            /// <summary>
            /// Used to convert a number in fictitious Base36 (0-9, A-Z) extended hex format back to Base10 decimal.
            /// </summary>
            /// <param name="StringToConvert">The Base36 string to convert back to decimal.</param>
            /// <returns>Returns an unsigned integer if the conversion is successful. Returns NOTHING in all other conditions.</returns>
            public static Nullable<UInt64> FromBase36(string StringToConvert)
            {
                try
                {
                    // Return nothing if there is no input
                    if (string.IsNullOrEmpty(StringToConvert) == true) return null;

                    // If we have a string, ensure it is upper case for comparisons
                    StringToConvert = StringToConvert.ToUpper();

                    // Get all known base36 chars
                    var AllBase36Chars = GetAllBase36Chars();

                    // Split up all chars in source string to make sure they are all legit
                    var AllCharsInSourceString = StringToConvert.ToCharArray();

                    // Return null if any strings in the source aren't valid base36 chars
                    foreach (var CHARACTER in AllCharsInSourceString)
                    {
                        if (AllBase36Chars.Contains(CHARACTER.ToString()) == false)
                        {
                            return null;
                        }
                    }

                    // If all chars are good, reverse the order (most significant part of number should have highest multiplier)
                    AllCharsInSourceString = AllCharsInSourceString.Reverse().ToArray();

                    // Create var to hold the running value of the number
                    UInt64 RunningValueOfNumber = 0;

                    // Start from most significant digit and compute number in decimal
                    for (int i = AllCharsInSourceString.Count() - 1; i >= 0; i += -1)
                    {
                        // Get the current char string
                        string CharAtCurrentPosition = AllCharsInSourceString[i].ToString();

                        // Get the multiplier value of that char
                        int MultiplierForThatChar = AllBase36Chars.IndexOf(CharAtCurrentPosition);

                        // Compute the value of this char when it's at the current index
                        UInt64 CharValueWhenInThisPosition = System.Convert.ToUInt64(MultiplierForThatChar * (System.Math.Pow(36, i)));

                        // Add that value to the running total
                        RunningValueOfNumber += CharValueWhenInThisPosition;
                    }

                    // Return the value once computed
                    return RunningValueOfNumber;
                }
                catch (Exception ex)
                {
                    // Return nothing if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }
            private static List<string> GetAllBase36Chars()
            {
                // Create list to hold all info
                var AllChars = new List<string>(36);

                // Add 0 to 9
                for (int i = 0; i <= 9; i++)
                {
                    AllChars.Add(i.ToString());
                }

                // Loop through and add all ascii chars from A to Z
                for (int i = 65; i <= 90; i++)
                {
                    // A is 65, Z is 90
                    AllChars.Add(System.Text.Encoding.ASCII.GetString(new[] { System.Convert.ToByte(i) }));
                }

                // Return the list
                return AllChars;
            }

            /// <summary>
            /// Used to convert a currency string to a numeric string (e.g. "$1,492.50" to "1492.50") - removes dollar signs ($) and commas (,) from the string.
            /// </summary>
            /// <param name="CurrencyString">The currency string to clean.</param>
            /// <returns>Returns the source string with the dollar sign ($) and comma (,) characters removed.</returns>
            public static string CurrencyStringToNumberString(string CurrencyString)
            {
                // Return empty string if nothing if passed in
                if (string.IsNullOrEmpty(CurrencyString) == true)
                {
                    return string.Empty;
                }

                // If there is a string, remove illegal chars and return it
                return CurrencyString.Replace("$", string.Empty).Replace(",", string.Empty).Trim();
            }

            /// <summary>
            /// Used to convert a font point size to actual pixels (i.e. 12 pt font = X pixels).
            /// </summary>
            /// <param name="FontPointSize">The font point size to convert.</param>
            /// <returns>Returns the equivalent pixels to use for the input font point size.</returns>
            public static Single FontPointSizeToPixels(Single FontPointSize)
            {
                return FontPointSize / System.Convert.ToSingle((72.0 / 96.0));
            }

            /// <summary>
            /// Used to safely run ToString() on an object even if the object is null (will return empty string if null).
            /// </summary>
            /// <param name="ObjectToConvert">The object to run ToString() on.</param>
            /// <returns>Returns the response of ToString() if successful. Returns an empty string if the object is null.</returns>
            public static string ToStringWithNullObjectSafety(object ObjectToConvert)
            {
                try
                {
                    // Return empty string if the object is null
                    if (ObjectToConvert == null)
                    {
                        return string.Empty;
                    }

                    // If there's something there, attempt ToString()
                    return ObjectToConvert.ToString();
                }
                catch (Exception ex)
                {
                    // Return empty string if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

        }

        public struct Transform
        {

            /// <summary>
            /// Used to add indenting to each line of a string that contains one or more lines (can be used to add bullets (> / - chars, etc.).
            /// </summary>
            /// <param name="MultiLineStringToIndentOnAllLines">The string to add indenting to.</param>
            /// <param name="IndentStringToUse">The string to use to indent each line with (tab, ''>'', etc.).</param>
            /// <param name="IndentEmptyLines">OPTIONAL:  Flag to indicate if lines that contain only white space should be omitted from indenting.</param>
            /// <returns>Return the source string with the requested indenting on each line if successful. Returns an empty string in all other conditions.</returns>
            public static string IndentAllLines(string MultiLineStringToIndentOnAllLines, string IndentStringToUse, bool IndentEmptyLines = false)
            {
                try
                {
                    // Return empty string if input string is empty
                    if (string.IsNullOrEmpty(MultiLineStringToIndentOnAllLines) == true) return string.Empty;

                    // If there is an input string, simply return it if there is no indent string specified (i.e. nothing to do to it)
                    if (string.IsNullOrEmpty(IndentStringToUse) == true) return MultiLineStringToIndentOnAllLines;

                    // If all inputs are good, split string by lines so all can be indented
                    var AllLines = MultiLineStringToIndentOnAllLines.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None).ToList();

                    // Loop through and indent lines
                    for (int i = 0; i <= AllLines.Count - 1; i++)
                    {
                        // Save the current line
                        string CurrentLine = AllLines[i];

                        // Skip line if it's empty AND user does not want empty lines indented
                        if (string.IsNullOrEmpty(CurrentLine.Trim()) == true && IndentEmptyLines == false) continue;

                        // If the line needs to be indented, indent and continue
                        AllLines[i] = IndentStringToUse + CurrentLine;
                    }

                    // Join and return info when done
                    return string.Join(Environment.NewLine, AllLines);
                }
                catch (Exception ex)
                {
                    // Return empty string if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

            /// <summary>
            /// Used to parse a source string into delimited groups for ease of reading (i.e. "1234567890" to "12 23 45 56 78 90", etc.).
            /// </summary>
            /// <param name="StringToParseIntoGroups">The source string to parse into groups.</param>
            /// <param name="GroupSize">The size of the groups (i.e. 3 characters, 4 characters, etc.).</param>
            /// <param name="DelimiterBetweenGroups">The delimiter string to use between groups (usually dashes [-] or spaces [ ].)</param>
            /// <returns>Returns the source string parsed into groups of the requested size with the requested delimiter if successful. Returns an empty string in all other conditions.</returns>
            public static string ParseIntoGroups(string StringToParseIntoGroups, uint GroupSize, string DelimiterBetweenGroups)
            {
                try
                {
                    // Return empty if not set
                    if (string.IsNullOrEmpty(StringToParseIntoGroups) == true) return string.Empty;

                    // Return empty string if group size is bad
                    if (GroupSize <= 0) return string.Empty;

                    // Begin by splitting into char array for grouping
                    var CharSplit = StringToParseIntoGroups.ToCharArray().ToList();

                    // Create list to hold blocks of strings
                    var StringBlocks = new List<string>();

                    // Create string to hold working block of characters
                    string WorkingBlock = string.Empty;

                    // Loop through and create blocks
                    foreach (var LETTER in CharSplit)
                    {
                        // Add the next letter
                        WorkingBlock += LETTER;

                        // Check if we've hit block size
                        if (WorkingBlock.Length >= GroupSize)
                        {
                            // If so, add to block list and clear var
                            StringBlocks.Add(WorkingBlock);
                            WorkingBlock = string.Empty;
                        }
                    }

                    // Once done, check if there's anything left (i.e. not a full block)
                    if (string.IsNullOrEmpty(WorkingBlock) == false)
                    {
                        StringBlocks.Add(WorkingBlock);
                    }

                    // Join and return string
                    return string.Join(DelimiterBetweenGroups, StringBlocks);
                }
                catch (Exception ex)
                {
                    // Return empty string if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

            /// <summary>
            /// Used to format a string such that the first letter of all words in the string will be uppercase (e.g. "some string" to "Some String") - sometimes called TitleCasing.
            /// </summary>
            /// <param name="OriginalString">The source string to format.</param>
            /// <returns>Returns the source string with the first letter of all words in uppercase if the method executes correctly. Returns an empty string in all other conditions.</returns>
            public static string UpperCaseFirstLetterOfAllWords(string OriginalString)
            {
                return Clean.UpperCaseFirstLetterOfAllWords(OriginalString);
            }

            /// <summary>
            /// Used to invert the casing of a user specified string (i.e. 'Abc123!' becomes 'aBC123!' - numbers and symbols are ignored).
            /// </summary>
            /// <param name="OriginalString">The source string to invert the casing of.</param>
            /// <returns>Returns a string that is the same as the source string except that all letters have opposite casing if the conversion works. Returns an empty string in all other conditions.</returns>
            public static string InvertCasing(string OriginalString)
            {
                try
                {
                    // Return empty if input string is empty
                    if (string.IsNullOrEmpty(OriginalString) == true) return string.Empty;

                    // If input string is good, get chars
                    var Chars = OriginalString.ToCharArray();

                    // Create array to hold strings for each char
                    var StringChars = new string[Chars.GetUpperBound(0) + 1];

                    // Loop through and invert casing on all chars
                    for (int i = 0; i <= Chars.GetUpperBound(0); i++)
                    {
                        // Get the char as a string
                        string StringChar = Chars[i].ToString();

                        // Check if this is a letter
                        if (StringChar.ToUpper() != StringChar.ToLower())
                        {
                            // If upper and lower case are different, it's a letter, figure out which way to flip
                            if (StringChar == StringChar.ToUpper())
                            {
                                // If it's upper case, make lower
                                StringChar = StringChar.ToLower();
                            }
                            else if (StringChar == StringChar.ToLower())
                            {
                                // If it's lower, make upper
                                StringChar = StringChar.ToUpper();
                            }
                        }

                        // At this point, it's either not a letter (i.e. just add as is), or it's a letter and the casing has been flipped - add to array
                        StringChars[i] = StringChar;
                    }

                    // Once done, join string back together and return
                    return string.Join(string.Empty, StringChars);
                }
                catch (Exception ex)
                {
                    // Return empty string if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

            /// <summary>
            /// Used to reverse the order of a user specified string (i.e. 'Abc' becomes 'cbA').
            /// </summary>
            /// <param name="OriginalString">The source string to reverse the order of.</param>
            /// <returns>Returns a string in the reverse order of the source string if successful. Returns an empty string in all other conditions.</returns>
            public static string ReverseOrder(string OriginalString)
            {
                try
                {
                    // Return empty string if source string is empty
                    if (string.IsNullOrEmpty(OriginalString) == true) return string.Empty;

                    // If original string is good, split to chars
                    var Chars = OriginalString.ToCharArray().ToList();

                    // Reverse the order
                    Chars.Reverse();

                    // Join back together and return
                    return string.Join("", Chars);
                }
                catch (Exception ex)
                {
                    // Return an empty string if conversion fails
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

            /// <summary>
            /// Used to pad a specified string with another string until the string is a specified length (adds the padded string to the LEFT of the original / source string).
            /// </summary>
            /// <param name="OriginalString">The starting string to pad.</param>
            /// <param name="StringToPadWith">The string to pad the starting string with.</param>
            /// <param name="MaxLengthOfFinalString">The maximum length of the padded string to return (string will be trimmed to match the requested length).</param>
            /// <returns>Returns the source string padded with the specified padding string until the length matches the requested number of characters if the padding is successful. Returns an empty string if an error occurs or inputs are bad.</returns>
            public static string PadLeft(string OriginalString, string StringToPadWith, int MaxLengthOfFinalString)
            {
                try
                {
                    // Return empty string if pad string is missing
                    if (string.IsNullOrEmpty(StringToPadWith) == true) return string.Empty;

                    // If padding string is good, return empty string if length is bad
                    if (MaxLengthOfFinalString <= 0) return string.Empty;

                    // If inputs are good, create string to return
                    string StringToReturn = OriginalString;

                    // Loop through and pad until long enough
                    while (StringToReturn.Length < MaxLengthOfFinalString)
                    {
                        // Keep padding until length is good
                        StringToReturn = StringToPadWith + StringToReturn;
                    }

                    // Once long enough, ensure that the string is no longer than max length (in case padding string was multi char)
                    if (StringToReturn.Length > MaxLengthOfFinalString)
                    {
                        StringToReturn = Extract.Substring.Left(StringToReturn, MaxLengthOfFinalString);
                    }

                    // Return the string
                    return StringToReturn;
                }
                catch (Exception ex)
                {
                    // Return empty string if issue occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

            /// <summary>
            /// Used to pad a specified string with another string until the string is a specified length (adds the padded string to the RIGHT of the original / source string).
            /// </summary>
            /// <param name="OriginalString">The starting string to pad.</param>
            /// <param name="StringToPadWith">The string to pad the starting string with.</param>
            /// <param name="MaxLengthOfFinalString">The maximum length of the padded string to return (string will be trimmed to match the requested length).</param>
            /// <returns>Returns the source string padded with the specified padding string until the length matches the requested number of characters if the padding is successful. Returns an empty string if an error occurs or inputs are bad.</returns>
            public static string PadRight(string OriginalString, string StringToPadWith, int MaxLengthOfFinalString)
            {
                try
                {
                    // Return empty string if pad string is missing
                    if (string.IsNullOrEmpty(StringToPadWith) == true) return string.Empty;

                    // If padding string is good, return empty string if length is bad
                    if (MaxLengthOfFinalString <= 0) return string.Empty;

                    // If inputs are good, create string to return
                    string StringToReturn = OriginalString;

                    // Loop through and pad until long enough
                    while (StringToReturn.Length < MaxLengthOfFinalString)
                    {
                        // Keep padding until length is good
                        StringToReturn = StringToReturn + StringToPadWith;
                    }

                    // Once long enough, ensure that the string is no longer than max length (in case padding string was multi char)
                    if (StringToReturn.Length > MaxLengthOfFinalString)
                    {
                        StringToReturn = Extract.Substring.Right(StringToReturn, MaxLengthOfFinalString);
                    }

                    // Return the string
                    return StringToReturn;
                }
                catch (Exception ex)
                {
                    // Return empty string if issue occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

            /// <summary>
            /// Used to pad a string with leading 0's so it is a desired length (e.g. "02", "0004", etc.).
            /// </summary>
            /// <param name="OriginalNumberString">The original string to pad.</param>
            /// <param name="NumberOfCharsToPadTo">The total desired length (in characters) to pad the string to.</param>
            /// <returns>Returns the number string padded to the specified length with the required number of leading 0's.</returns>
            public static string PadNumberLeadingDigits(string OriginalNumberString, uint NumberOfCharsToPadTo)
            {
                // Create the string to return
                string StringToReturn = OriginalNumberString;

                // Keep padding w/ 0's until length is big enough
                while (StringToReturn.Length < NumberOfCharsToPadTo)
                {
                    StringToReturn = "0" + StringToReturn;
                }

                // Check if string to return is too long (should never happen)
                if (StringToReturn.Length > NumberOfCharsToPadTo)
                {
                    // If the string is too long, crop it down to requested length
                    StringToReturn = StringToReturn.Substring(System.Convert.ToInt32(StringToReturn.Length - NumberOfCharsToPadTo));
                }

                // Return the string
                return StringToReturn;
            }

            /// <summary>
            /// Used to vertically stack the characters of a string by placing a line ending in between each character.
            /// </summary>
            /// <param name="OriginalString">The string to stack the characters of.</param>
            /// <param name="LineEndingStringToStackWith">OPTIONAL:  The line ending string to stack with.</param>
            /// <returns>Returns the string 'stacked' vertically if successful. Returns an empty string in all other conditions.</returns>
            public static string ToVerticallyStackedCharacters(string OriginalString, string LineEndingStringToStackWith = "\r\n")
            {
                try
                {
                    // Return empty string if source is missing
                    if (string.IsNullOrEmpty(OriginalString) == true) return string.Empty;

                    // If there is a string, break into chars
                    var AllCharacters = OriginalString.ToCharArray().ToList();

                    // Join and return
                    return string.Join(LineEndingStringToStackWith, AllCharacters);
                }
                catch (Exception ex)
                {
                    // Return empty string if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

        }

        public struct RegEx
        {

            /// <summary>
            /// Used to get all the first (if any) quoted text within an input string (i.e. There names were ''Bob'' and ''Bill'' -> this would return 'Bob').
            /// </summary>
            /// <param name="StringToParse">The string to parse / find quoted text within.</param>
            /// <param name="RemoveEnclosingQuotesBeforeReturning">Flag to indicate if the matching text should have the '' marks removed before returning.</param>
            /// <returns>Return the first quoted text found if successful. Returns an empty string if there is no quoted text or an error occurs.</returns>
            public static string GetQuotedString(string StringToParse, bool RemoveEnclosingQuotesBeforeReturning)
            {
                try
                {
                    // Use helper to get all quoted text
                    var AllQuotedText = GetQuotedStrings(StringToParse, RemoveEnclosingQuotesBeforeReturning);

                    // Return empty string if nothing is found
                    if (AllQuotedText == null || AllQuotedText.Count == 0) return string.Empty;

                    // If there is any quoted text, return the first entry
                    return AllQuotedText[0];
                }
                catch (Exception ex)
                {
                    // Return empty string if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }
            /// <summary>
            /// Used to get all quoted text within an input string (i.e. There names were ''Bob'' and ''Bill'' -> this would return 'Bob' and 'Bill').
            /// </summary>
            /// <param name="StringToParse">The string to parse / find quoted text within.</param>
            /// <param name="RemoveEnclosingQuotesBeforeReturning">Flag to indicate if the matching text should have the '' marks removed before returning.</param>
            /// <returns>Return a list of all matching / quoted text found if successful. Return NOTHING if an error occurs.</returns>
            public static List<string> GetQuotedStrings(string StringToParse, bool RemoveEnclosingQuotesBeforeReturning)
            {
                try
                {
                    // Create list to hold matching info to return
                    var MatchingInfoToReturn = new List<string>();

                    // Return empty list if input is missing
                    if (string.IsNullOrEmpty(StringToParse) == true) return MatchingInfoToReturn;

                    // If input is good, create regex for parsing
                    var RegExParser = new System.Text.RegularExpressions.Regex("([" + Generate.SpecialCharacter.QuotationMark() + @"'])(?:(?=(\\?))\2.)*?\1");

                    // Find all quoted text within source string
                    var AllMatchingInfo = RegExParser.Matches(StringToParse);

                    // Return empty list if there are no matches
                    if (AllMatchingInfo == null || AllMatchingInfo.Count == 0) return MatchingInfoToReturn;

                    // If we have at least one match, loop through and copy it over
                    for (int i = 0; i <= AllMatchingInfo.Count - 1; i++)
                    {
                        MatchingInfoToReturn.Add(AllMatchingInfo[i].Value);
                    }

                    // Check if removing enclosing quotes
                    if (RemoveEnclosingQuotesBeforeReturning == true)
                    {
                        // If so, loop through and remove quotes
                        for (int i = 0; i <= MatchingInfoToReturn.Count - 1; i++)
                        {
                            MatchingInfoToReturn[i] = MatchingInfoToReturn[i].Replace(Generate.SpecialCharacter.QuotationMark(), string.Empty);
                        }
                    }

                    // Return the matches
                    return MatchingInfoToReturn;
                }
                catch (Exception ex)
                {
                    // Return nothing if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }

        }

        public struct Obfuscate
        {

            // Constant to hold default character shift for encoding and decoding strings
            private const int DefaultObfuscationShift = 7;

            // Constant to hold the maximum shift allowed (so you don't roll the 256 possible chars from basic keyboard chars)
            private const int MaxAllowedShift = 75;       // Technically 129, round down for safety

            /// <summary>
            /// Used to obfuscate a given string.
            /// </summary>
            /// <param name="StringToEncode">The string to obfuscate.</param>
            /// <returns>Returns an obfuscated version of the passed in string.</returns>
            public static string Encode(string StringToEncode)
            {
                // Use the full function with default shift
                return Encode(StringToEncode, DefaultObfuscationShift);
            }
            /// <summary>
            /// Used to obfuscate a given string.
            /// </summary>
            /// <param name="StringToEncode">The string to obfuscate.</param>
            /// <param name="CharacterShift">The number of ASCII characters to shift each character of the string (must be the same as the Decode method to function correctly).</param>
            /// <returns>Returns an obfuscate version of the passed in string if the string can be obfuscated. Returns an empty string if passed in string is null or empty. Returns NULL if an error occurs obfuscateing the string.</returns>
            public static string Encode(string StringToEncode, int CharacterShift)
            {
                try
                {
                    // Return an empty string if there is no text
                    if (string.IsNullOrEmpty(StringToEncode) == true) return string.Empty;

                    // If there is text, create a var to hold the obfuscated string
                    string EncryptedText = string.Empty;

                    // Always roll in the positive direction
                    CharacterShift = System.Math.Abs(CharacterShift);

                    // Limit the allowed shift to minimize the possibility of rolling past the 256 chars (assumes standard keyboard entry)
                    if (CharacterShift > 10) CharacterShift = 10;

                    // Loop through, shift each character, and save to obfuscated string
                    foreach (char CH in StringToEncode)
                    {
                        EncryptedText += System.Convert.ToChar(System.Convert.ToInt32(CH) + CharacterShift);
                    }

                    // Return the obfuscated text
                    return EncryptedText;
                }
                catch (Exception ex)
                {
                    // Return nothing if string can't be char shifted by specified amount
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

            /// <summary>
            /// Used to decode a string that has been obfuscated.
            /// </summary>
            /// <param name="StringToDecode">The string to decode.</param>
            /// <returns>Returns a decrypted version of the passed in string.</returns>
            public static string Decode(string StringToDecode)
            {
                // Use the full function with default shift
                return Decode(StringToDecode, DefaultObfuscationShift);
            }
            /// <summary>
            /// Used to decode a string that has been obfuscated.
            /// </summary>
            /// <param name="StringToDecode">The string to decode.</param>
            /// <param name="CharacterShift">The number of ASCII characters to shift each character of the string (must be the same as the Encode method to function correctly).</param>
            /// <returns>Returns the decoded version of the passed in string if the string can be decoded. Returns an empty string if passed in string is null or empty. Returns NULL if an error occurs decoding the string.</returns>
            public static string Decode(string StringToDecode, int CharacterShift)
            {
                try
                {
                    // Return an empty string if there is no text
                    if (string.IsNullOrEmpty(StringToDecode) == true) return string.Empty;

                    // If there is encoded text, create var to hold decoded text
                    string DecodedText = string.Empty;

                    // Always roll in the positive direction
                    CharacterShift = System.Math.Abs(CharacterShift);

                    // Limit the allowed shift to minimize the possibility of rolling past the 256 chars (assumes standard keyboard entry)
                    if (CharacterShift > 10) CharacterShift = 10;

                    // Loop through, shift each character back, and save to decoded string
                    foreach (char CH in StringToDecode)
                    {
                        DecodedText += System.Convert.ToChar(System.Convert.ToInt32(CH) - CharacterShift);
                    }

                    // Return the decoded text
                    return DecodedText;
                }
                catch (Exception ex)
                {
                    // Return nothing if string can't be char shifted by specified amount
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }

            }

        }

        public struct Hash
        {

            /// <summary>
            /// Used to return the result of the hash operation.
            /// </summary>
            public class Result
            {

                // Vars to hold basic info from hash result
                public AlgorithmOptions HashAlgorithm { get; set; } = AlgorithmOptions.SHA1;
                public readonly List<byte> HashBytes = new List<byte>();

                // Read only to return the string representation of the bytes as base 64
                public string HashBytesAsBase64
                {
                    get
                    {
                        try
                        {
                            // Return empty string if there are no bytes
                            if (HashBytes.Count == 0) return string.Empty;

                            // If there are bytes, attempt base64 and return
                            return System.Convert.ToBase64String(HashBytes.ToArray());
                        }
                        catch (Exception ex)
                        {
                            // Return empty string if something fails
                            System.Diagnostics.Trace.WriteLine(ex.Message);
                            return string.Empty;
                        }
                    }
                }

                // Read only to return the string representation of the bytes in lower case
                public string HashBytesAsStringLower
                {
                    get
                    {
                        try
                        {
                            // Create string to hold the bytes
                            string HexStringToReturn = string.Empty;

                            // Loop through and build hex string
                            foreach (var BITE in HashBytes)
                            {
                                HexStringToReturn += BITE.ToString("X2");
                            }

                            // Return the string in lower case
                            return HexStringToReturn.ToLower();
                        }
                        catch (Exception ex)
                        {
                            // Return empty string if something fails
                            System.Diagnostics.Trace.WriteLine(ex.Message);
                            return string.Empty;
                        }
                    }
                }

                // Read only to return the string representation of the bytes in upper case
                public string HashBytesAsStringUpper
                {
                    get
                    {
                        // Return upper of lower case string
                        return HashBytesAsStringLower.ToUpper();
                    }
                }

                // Implement a meaningful ToString()
                public override string ToString()
                {
                    // Use lower case for return, that's the most common
                    return HashBytesAsStringLower;
                }

            }

            /// <summary>
            /// Used to specify how a string is encoded.
            /// </summary>
            public enum StringEncodingOptions
            {
                ASCII,
                Unicode
            }

            /// <summary>
            /// Used to specify different hashing options.
            /// </summary>
            public enum AlgorithmOptions
            {
                SHA1,
                SHA256,
                SHA384,
                SHA512,
                MD5
            }

            /// <summary>
            /// Used to compute a hash for a given string and return it.
            /// </summary>
            /// <param name="StringToHash">The string to hash.</param>
            /// <param name="StringEncoding">The format that the source string is encoded in (e.g. ASCII, Unicode, etc.).</param>
            /// <param name="HashAlgorithm">OPTIONAL:  The hashing algorithm to use (defaults to SHA1).</param>
            /// <returns>Returns a hashed string for the input string if the hash is successful. Returns an empty string in all other conditions.</returns>
            public static Result Compute(string StringToHash, StringEncodingOptions StringEncoding = StringEncodingOptions.ASCII, AlgorithmOptions HashAlgorithm = AlgorithmOptions.SHA1)
            {
                try
                {
                    // Return an empty object if input string is bad
                    if (string.IsNullOrEmpty(StringToHash) == true) return new Result();

                    // If there is a string, create array to hold bytes
                    byte[] StringBytes = null;

                    // Convert based on type
                    switch (StringEncoding)
                    {
                        case StringEncodingOptions.ASCII:
                            // Get ASCII bytes
                            StringBytes = System.Text.Encoding.ASCII.GetBytes(StringToHash);
                            break;
                        case StringEncodingOptions.Unicode:
                            // Get unicode bytes
                            StringBytes = System.Text.Encoding.Unicode.GetBytes(StringToHash);
                            break;
                        default:
                            // Throw error if not implemented
                            throw new Exception("Unimplemented string encoding method [ " + StringEncoding.ToString() + " ]!");
                    }

                    // Use helper for hashing bytes and return result
                    return Compute(StringBytes, HashAlgorithm: HashAlgorithm);
                }
                catch (Exception ex)
                {
                    // Return an empty object if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return new Result();
                }
            }

            /// <summary>
            /// Used to compute a hash for a given list of bytes and return it.
            /// </summary>
            /// <param name="BytesToHash">The bytes to hash.</param>
            /// <param name="HashAlgorithm">OPTIONAL:  The hashing algorithm to use (defaults to SHA1).</param>
            /// <returns>Returns a hashed string for the input string if the hash is successful. Returns an empty string in all other conditions.</returns>
            public static Result Compute(IEnumerable<byte> BytesToHash, AlgorithmOptions HashAlgorithm = AlgorithmOptions.SHA1)
            {
                try
                {
                    // Return an empty object if nothing is passed in
                    if (BytesToHash == null || BytesToHash.Count() == 0) return new Result();

                    // Create var to hold hashed bytes
                    byte[] HashedBytes = null;

                    // Hash based on requested algorithm
                    HashedBytes = System.Security.Cryptography.HashAlgorithm.Create(HashAlgorithm.ToString()).ComputeHash(BytesToHash.ToArray());

                    // Once hash is computed, create new result to return info
                    var ResultToReturn = new Result();

                    // Xfer over info the result
                    ResultToReturn.HashAlgorithm = HashAlgorithm;
                    ResultToReturn.HashBytes.AddRange(HashedBytes);

                    // Return the info
                    return ResultToReturn;
                }
                catch (Exception ex)
                {
                    // Return an empty object if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return new Result();
                }
            }

        }

        public struct HTML
        {

            /// <summary>
            /// Enum to specify HTML color hexadecimal values.
            /// </summary>
            public enum ColorOptions
            {
                Black = 0x0,
                GunMetal = 0x2C3539,
                Grey = 0x808080,
                Silver = 0xC0C0C0,
                Platinum = 0xE5E4E2,
                White = 0xFFFFFF,
                Red = 0xFF0000,
                Green = 0x8000,
                Blue = 0xFF,
                Orange = 0xFFA500,
                Cinnamon = 0xC58917,
                Brown = 0xA52A2A,
                Lime = 0xFF00,
                DarkBlue = 0xA0,
                LightBlue = 0xADD8E6,
                SteelBlue = 0x4863A0,
                Purple = 0x800080,
                Cyan = 0xFFFF,
                Magenta = 0xFF00FF,
                Yellow = 0xFFFF00
            }

            /// <summary>
            /// Used to apply HTML color coding tags to a specified keyword in an input string.
            /// </summary>
            /// <param name="SourceStringToFormat">The input string to add color coding tags to.</param>
            /// <param name="KeywordToColor">The keyword to find within the input string that needs to be color coded.</param>
            /// <param name="ColorToUse">The color to use.</param>
            public static string ColorCodeKeyword(string SourceStringToFormat, string KeywordToColor, ColorOptions ColorToUse)
            {
                try
                {
                    // Create dictionary to use for common method
                    var WordsAndColors = new Dictionary<string, ColorOptions>();
                    WordsAndColors.Add(KeywordToColor, ColorToUse);

                    // Use common method and return result
                    return ColorCodeKeywords(SourceStringToFormat, WordsAndColors);
                }
                catch (Exception ex)
                {
                    // Return empty string if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }
            /// <summary>
            /// Used to apply HTML color coding tags to specified keywords in an input string.
            /// </summary>
            /// <param name="SourceStringToFormat">The input string to add color coding tags to.</param>
            /// <param name="KeywordsAndColors">A dictionary holding the keywords to find in the input string and what color to set them to.</param>
            public static string ColorCodeKeywords(string SourceStringToFormat, Dictionary<string, ColorOptions> KeywordsAndColors)
            {
                try
                {
                    // Return empty string if input string is not set
                    if (string.IsNullOrEmpty(SourceStringToFormat) == true) return string.Empty;

                    // Return source string if no keywords are specified (i.e. nothing to find)
                    if (KeywordsAndColors == null || KeywordsAndColors.Count == 0) return SourceStringToFormat;

                    // If we have strings to find, loop through and set colors
                    foreach (var PAIR in KeywordsAndColors)
                    {
                        // Check if keyword is in input string
                        if (SourceStringToFormat.Contains(PAIR.Key) == true)
                        {
                            // If so, add color code tags around it
                            SourceStringToFormat = SourceStringToFormat.Replace(PAIR.Key, "<font color=" + Generate.SpecialCharacter.QuotationMark() + "#" + System.Convert.ToInt32(PAIR.Value).ToString("X6") + Generate.SpecialCharacter.QuotationMark() + ">" + PAIR.Key + "</font>");
                        }
                    }

                    // Once all keywords are processed, return the update string
                    return SourceStringToFormat;
                }
                catch (Exception ex)
                {
                    // Return empty string if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

            /// <summary>
            /// Used to apply HTML bold tags to a specified keyword in an input string.
            /// </summary>
            /// <param name="SourceStringToFormat">The input string to add color coding tags to.</param>
            /// <param name="KeywordToBold">The keyword to find within the input string that should be emboldened.</param>
            public static string BoldKeyword(string SourceStringToFormat, string KeywordToBold)
            {
                try
                {
                    // Use helper to format and return
                    return BoldKeywords(SourceStringToFormat, new[] { KeywordToBold });
                }
                catch (Exception ex)
                {
                    // Return empty string if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }
            /// <summary>
            /// Used to apply HTML bold tags to a specified list of keywords in an input string.
            /// </summary>
            /// <param name="SourceStringToFormat">The input string to add color coding tags to.</param>
            /// <param name="KeywordsToBold">The keywords to find within the input string that should be emboldened.</param>
            public static string BoldKeywords(string SourceStringToFormat, IEnumerable<string> KeywordsToBold)
            {
                try
                {
                    // Return empty string if input string is not set
                    if (string.IsNullOrEmpty(SourceStringToFormat) == true) return string.Empty;

                    // Return source string if no keywords are specified (i.e. nothing to find)
                    if (KeywordsToBold == null || KeywordsToBold.Count() == 0) return SourceStringToFormat;

                    // If we have strings to find, loop through and set bold
                    foreach (var KWD in KeywordsToBold)
                    {
                        // Check if keyword is in input string
                        if (SourceStringToFormat.Contains(KWD) == true)
                        {
                            // If so, add bold tag
                            SourceStringToFormat = SourceStringToFormat.Replace(KWD, "<b>" + KWD + "</b>");
                        }
                    }

                    // Once all keywords are processed, return the update string
                    return SourceStringToFormat;
                }
                catch (Exception ex)
                {
                    // Return empty string if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

            /// <summary>
            /// Used to convert an array of string data to an HTML table.
            /// </summary>
            /// <param name="TableDataWithColumnHeaders">The 2D array of data (where Row0 is the column headers) to convert to the HTML table.</param>
            /// <returns>Returns an HTML text table if successful. Returns an empty string in all other conditions.</returns>
            public static string TableFromArray(string[,] TableDataWithColumnHeaders)
            {
                try
                {
                    // Return empty string if there isn't enough info to make table
                    if (TableDataWithColumnHeaders == null || TableDataWithColumnHeaders.GetUpperBound(0) == 0)
                    {
                        return string.Empty;
                    }

                    // Loop through and remove any nulls to prevent possible null object exceptions
                    for (int ROW = 0; ROW <= TableDataWithColumnHeaders.GetUpperBound(0); ROW++)
                    {
                        for (int COL = 0; COL <= TableDataWithColumnHeaders.GetUpperBound(1); COL++)
                        {
                            // Ensure anything that is empty is an empty string and not null
                            if (string.IsNullOrEmpty(TableDataWithColumnHeaders[ROW, COL]) == true)
                            {
                                TableDataWithColumnHeaders[ROW, COL] = string.Empty;
                            }
                        }
                    }

                    // Create constants to hold start and end tags for table rows since they are used repeatedly
                    const string TagStartRow = "<tr>";
                    const string TagStopRow = "</tr>";
                    const string TagStartCellHeader = "<th align=\"Left\">";
                    const string TagStopCellHeader = "</th>";
                    const string TagStartCell = "<td>";
                    const string TagStopCell = "</td>";

                    // Create list to hold html lines that will eventually be joined and returned
                    var HtmlLines = new List<string>();

                    // Add tag to begin HTML table
                    HtmlLines.Add("<table border = \"1\" class=\"jeditorTable\" style=\"border-collapse:collapse\">");

                    // Start the table header block
                    HtmlLines.Add("<thead>");

                    // Add start of table header row
                    HtmlLines.Add(TagStartRow);

                    // Loop through and add header row info
                    for (int COL = 0; COL <= TableDataWithColumnHeaders.GetUpperBound(1); COL++)
                    {
                        HtmlLines.Add("\t" + TagStartCellHeader + TableDataWithColumnHeaders[0, COL] + TagStopCellHeader);
                    }

                    // End table header row
                    HtmlLines.Add(TagStopRow);

                    // End the table header block
                    HtmlLines.Add("</thead>");

                    // Begin the body
                    HtmlLines.Add("<tbody>");

                    // Loop through and add all rows (skip th first / header row)
                    for (int ROW = 1; ROW <= TableDataWithColumnHeaders.GetUpperBound(0); ROW++)
                    {
                        // Add start row
                        HtmlLines.Add(TagStartRow);

                        // Add each cell entry
                        for (int COL = 0; COL <= TableDataWithColumnHeaders.GetUpperBound(1); COL++)
                        {
                            HtmlLines.Add("\t" + TagStartCell + TableDataWithColumnHeaders[ROW, COL] + TagStopCell);
                        }

                        // Add stop row
                        HtmlLines.Add(TagStopRow);
                    }

                    // End the body
                    HtmlLines.Add("</tbody>");

                    // End the table
                    HtmlLines.Add("</table>");

                    // Join and return info
                    return string.Join(Environment.NewLine, HtmlLines);
                }
                catch (Exception ex)
                {
                    // Return empty string if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

        }

        public struct XML
        {

            /// <summary>
            /// Used to extend StringWriter to allow for different text encodings.
            /// </summary>
            private class StringWriterExtension : System.IO.StringWriter
            {

                // Var to hold the override encoding to use
                private System.Text.Encoding OverrideEncoding = null;

                // Override the property that returns encoding so we can use whatever encoding the user has requested vs. the default
                public override System.Text.Encoding Encoding
                {
                    get
                    {
                        // Use override encoding if specified otherwise use default
                        if (OverrideEncoding != null)
                        {
                            return OverrideEncoding;
                        }
                        else
                        {
                            return base.Encoding;
                        }
                    }
                }

                // Allow setting of encoding
                public StringWriterExtension(System.Text.Encoding OverrideTextEncodingToUse = null)
                {
                    // Save any override encoding specified
                    OverrideEncoding = OverrideTextEncodingToUse;
                }

            }

            /// <summary>
            /// Used to serialize an instance of a class / object into an XML string.
            /// </summary>
            /// <param name="InstanceToSerialize">The class / object instance to serialize.</param>
            /// <returns>Returns the serialized XML string if the serialization worked. Returns an empty string in all other conditions.</returns>
            public static string Serialize(object InstanceToSerialize)
            {
                // Use underlying method with no override encoding specified
                return Serialize(InstanceToSerialize, null);
            }
            /// <summary>
            /// Used to serialize an instance of a class / object into an XML string.
            /// </summary>
            /// <param name="InstanceToSerialize">The class / object instance to serialize.</param>
            /// <param name="TextEncodingToUse">The text encoding type to use for the XML (the operating system's default will be used if not specified).</param>
            /// <returns>Returns the serialized XML string if the serialization worked. Returns an empty string in all other conditions.</returns>
            public static string Serialize(object InstanceToSerialize, System.Text.Encoding TextEncodingToUse)
            {
                try
                {
                    // Return an empty string if object is missing
                    if (InstanceToSerialize == null) return string.Empty;

                    // If there is an object, create new instance of serializer
                    var Serializer = new System.Xml.Serialization.XmlSerializer(InstanceToSerialize.GetType());

                    // Create var to hold the serialized string
                    string SerializedString = string.Empty;

                    // Create string write to serialize the object into
                    using (StringWriterExtension StringMaker = new StringWriterExtension(OverrideTextEncodingToUse: TextEncodingToUse))
                    {
                        // Serialize the data into the string builder
                        Serializer.Serialize(StringMaker, InstanceToSerialize);

                        // Save the serialized string
                        SerializedString = StringMaker.ToString();
                    }

                    // Return the serialized xml string
                    return SerializedString;
                }
                catch (Exception ex)
                {
                    // Return an empty string if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

            /// <summary>
            /// Used to check if a given string can be deserialized into an object.
            /// </summary>
            /// <param name="StringToCheck">The string to check.</param>
            /// <param name="TypeToCheck">The type of the object that the string should be able to deserialize to.</param>
            /// <returns>Returns TRUE if the string can be deserialized into a generic object. Returns FALSE in all other conditions.</returns>
            public static bool CanDeserialize(string StringToCheck, Type TypeToCheck)
            {
                // Attempt to deserialize the object
                object DeserializedObject = Deserialize(StringToCheck, TypeToCheck);

                // Check if deserialization worked
                if (DeserializedObject != null)
                {
                    // If it worked, return true
                    return true;
                }
                else
                {
                    // If it failed, return false
                    return false;
                }
            }

            /// <summary>
            /// Used to deserialize a string into a specified type.
            /// </summary>
            /// <param name="StringToDeserialize">The string to deserialize.</param>
            /// <param name="TypeToDeserializeTo">The type of the object to deserialize the string to.</param>
            /// <returns>Returns an object that can be safely converted to the specified type if the deserialization is successful. Returns NOTHING in all other conditions.</returns>
            public static object Deserialize(string StringToDeserialize, Type TypeToDeserializeTo)
            {
                // Create dummy var to hold any returned error msg
                string DummyVar = string.Empty;

                // Use common method
                return Deserialize(StringToDeserialize, TypeToDeserializeTo, ref DummyVar);
            }
            /// <summary>
            /// Used to deserialize a string into a specified type.
            /// </summary>
            /// <param name="StringToDeserialize">The string to deserialize.</param>
            /// <param name="TypeToDeserializeTo">The type of the object to deserialize the string to.</param>
            /// <param name="refReturnedErrMsg">BYREF:  Returns any error messages that are captured if deserialization fails.</param>
            /// <returns>Returns an object that can be safely converted to the specified type if the deserialization is successful. Returns NOTHING in all other conditions.</returns>
            public static object Deserialize(string StringToDeserialize, Type TypeToDeserializeTo, ref string refReturnedErrMsg)
            {
                try
                {
                    // Init ref vars
                    refReturnedErrMsg = string.Empty;

                    // Return nothing if string is empty
                    if (string.IsNullOrEmpty(StringToDeserialize) == true)
                    {
                        refReturnedErrMsg = "Input string to deserialize cannot be empty!";
                        return null;
                    }

                    // Return nothing if type is null
                    if (TypeToDeserializeTo == null)
                    {
                        refReturnedErrMsg = "Type to deserialize to cannot be null!";
                        return null;
                    }

                    // If both are good, create object to hold the result of the deserialization
                    object DeserializedObject = null;

                    // If both are good, convert string into stream so it can be deserialized
                    using (System.IO.StringReader StringStream = new System.IO.StringReader(StringToDeserialize))
                    {
                        // Create deserializer to do the work
                        var Deserializer = new System.Xml.Serialization.XmlSerializer(TypeToDeserializeTo);

                        // Attempt deserialization
                        DeserializedObject = Deserializer.Deserialize(StringStream);
                    }

                    // Return the object when done
                    return DeserializedObject;
                }
                catch (Exception ex)
                {
                    // Return nothing if an error occurs
                    refReturnedErrMsg = "Caught Exception -> " + ex.Message;
                    return null;
                }
            }
            /// <summary>
            /// Used to directly deserialize a specified string into a specified type (no post conversion / casting required).
            /// </summary>
            /// <typeparam name="T">The type to deserialize the string into.</typeparam>
            /// <param name="StringToDeserialize">The string to deserialize.</param>
            /// <returns>Returns an instance of the specified type containing the deserialized contents of the source string if successful. Returns NOTHING in all other conditions.</returns>
            public static T Deserialize<T>(string StringToDeserialize)
            {
                // Create dummy var to hold any returned error msg
                string DummyVar = string.Empty;

                // Use common method
                return Deserialize<T>(StringToDeserialize, ref DummyVar);
            }
            /// <summary>
            /// Used to directly deserialize a specified string into a specified type (no post conversion / casting required).
            /// </summary>
            /// <typeparam name="T">The type to deserialize the string into.</typeparam>
            /// <param name="StringToDeserialize">The string to deserialize.</param>
            /// <param name="refReturnedErrMsg">BYREF:  Returns any error messages that are captured if deserialization fails.</param>
            /// <returns>Returns an instance of the specified type containing the deserialized contents of the source string if successful. Returns NOTHING in all other conditions.</returns>
            public static T Deserialize<T>(string StringToDeserialize, ref string refReturnedErrMsg)
            {
                try
                {
                    // Init ref vars
                    refReturnedErrMsg = string.Empty;

                    // Attempt to deserialize into an object
                    object DeserializedObject = Deserialize(StringToDeserialize, typeof(T), ref refReturnedErrMsg);

                    // Return nothing if nothing comes back
                    if (DeserializedObject == null) return default(T);

                    // If something comes back, attempt to convert and return it
                    return (T)DeserializedObject;
                }
                catch (Exception ex)
                {
                    // Return nothing if an error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return default(T);
                }
            }

            /// <summary>
            /// Used to create a simple XML node that contains a list of key / value paired elements.
            /// </summary>
            /// <param name="NodeName">The node name that the elements will be enclosed in (cannot be null / empty).</param>
            /// <param name="ElementNamesAndValues">The list of KVPs to add to the XML node.</param>
            /// <returns>Returns an XML string with the requested info if created successfully. Returns an empty string in all other conditions.</returns>
            public static string CreateElementNode(string NodeName, List<KeyValuePair<string, string>> ElementNamesAndValues)
            {
                try
                {
                    // Return empty string if node name is missing
                    if (string.IsNullOrEmpty(NodeName) == true) return string.Empty;

                    // Return empty string if KVPs are null
                    if (ElementNamesAndValues == null) return string.Empty;

                    // If there's enough info to build, create new XElement to make node
                    var XmlElement = new System.Xml.Linq.XElement(NodeName);

                    // Loop through and add element names and values
                    foreach (var PAIR in ElementNamesAndValues)
                    {
                        XmlElement.Add(new System.Xml.Linq.XElement(PAIR.Key, PAIR.Value));
                    }

                    // Return the string
                    return XmlElement.ToString();
                }
                catch (Exception ex)
                {
                    // Return empty string if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

            /// <summary>
            /// Used to retrieve an XML element (by its name) from a parent XML element.
            /// </summary>
            /// <param name="ParentElement">The parent XML element to retrieve the child element from.</param>
            /// <param name="ElementName">The string name of the XML child element to retrieve.</param>
            /// <returns>Returns the child XML element if a match is found in the parent element. Returns NOTHING in all other cases.</returns>
            public static System.Xml.Linq.XElement GetElement(System.Xml.Linq.XElement ParentElement, string ElementName)
            {
                try
                {
                    // Try to return the specified element
                    return ParentElement.Element(ElementName);
                }
                catch (Exception ex)
                {
                    // If element doesn't exist, return nothing
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }

            /// <summary>
            /// Used to format an unformatted XML string (adds carriage returns, indenting, etc.).
            /// </summary>
            /// <param name="UnformattedXML">The unformatted XML string to format.</param>
            /// <param name="NormalizeLineEndingsToCrLf">OPTIONAL:  Flag to indicate if all line endings should be normalized to CrLf before returning the string.</param>
            /// <param name="RemoveXmlEscapeCharaters">OPTIONAL:  Flag to indicate if any XML escapes should be removed before returning the string.</param>
            /// <returns>Returns a formatted version of the source XML string if formatting is successful. Returns the source / input string if formatting cannot be done.</returns>
            public static string FormatString(string UnformattedXML, bool NormalizeLineEndingsToCrLf = false, bool RemoveXmlEscapeCharaters = false)
            {
                try
                {
                    // Create var to hold xml to return
                    string XmlToReturn = UnformattedXML;

                    // Just return empty string if nothing is passed in
                    if (string.IsNullOrEmpty(XmlToReturn) == true) return string.Empty;

                    try
                    {
                        // Attempt to format xml
                        XmlToReturn = System.Xml.Linq.XDocument.Parse(UnformattedXML).ToString();
                    }
                    catch (Exception ex)
                    {
                        // If it fails, just keep using source string
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        XmlToReturn = UnformattedXML;
                    }

                    // Normalize line endings if requested
                    if (NormalizeLineEndingsToCrLf == true)
                    {
                        XmlToReturn = Clean.NormalizeLineEndings(XmlToReturn);
                    }

                    // Remove escapes if requested
                    if (RemoveXmlEscapeCharaters == true)
                    {
                        XmlToReturn = RemoveEscapes(XmlToReturn);
                    }

                    // Return the string
                    return XmlToReturn;
                }
                catch (Exception ex)
                {
                    // Return source string if formatting fails
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return UnformattedXML;
                }
            }

            /// <summary>
            /// Used to insert escape characters into a string BEFORE that string is placed into an XML document.
            /// </summary>
            /// <param name="StringToFormat">The XML string to replace illegal characters with escape characters in.</param>
            /// <returns>Returns the source XML string with all illegal characters replaced with escape characters.</returns>
            public static string InsertEscapes(string StringToFormat)
            {
                try
                {
                    // Put in escapes (remove ampersand FIRST or downstream escapes will have their ampersands escaped again and broken)
                    StringToFormat = StringToFormat.Replace("&", "&amp;");
                    StringToFormat = StringToFormat.Replace("<", "&lt;");
                    StringToFormat = StringToFormat.Replace(">", "&gt;");
                    StringToFormat = StringToFormat.Replace("\"", "&quot;");
                    StringToFormat = StringToFormat.Replace("'", "&apos;");

                    // Return the string
                    return StringToFormat;
                }
                catch (Exception ex)
                {
                    // Return an empty string if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }
            /// <summary>
            /// Used to remove escape characters from string / entry that has been extracted from an XML document.
            /// </summary>
            /// <param name="StringToFormat">The XML string to replace escape characters with normal characters in.</param>
            /// <returns>Returns the source XML string with all escape characters replaced with their original / intended characters.</returns>
            public static string RemoveEscapes(string StringToFormat)
            {
                try
                {
                    // Remove escapes (remove ampersand LAST so we don't break other escaping)
                    StringToFormat = StringToFormat.Replace("&lt;", "<");
                    StringToFormat = StringToFormat.Replace("&gt;", ">");
                    StringToFormat = StringToFormat.Replace("&quot;", "\"");
                    StringToFormat = StringToFormat.Replace("&apos;", "'");
                    StringToFormat = StringToFormat.Replace("&amp;", "&");

                    // Return the string
                    return StringToFormat;
                }
                catch (Exception ex)
                {
                    // Return an empty string if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

            /// <summary>
            /// Used to navigate down an XML hierarchy and get the value of the last name in the hierarchy list.
            /// </summary>
            /// <param name="StartingElement">The XML element to traverse down.</param>
            /// <param name="ElementNameHierarchy">The element names to traverse down. The method will return the value for the last name in the list.</param>
            /// <returns>Returns the element value if it is found. Returns NULL string (NOT string.empty) if an error occurs or the element name doesn't exist.</returns>
            public static string GetElementValue(System.Xml.Linq.XElement StartingElement, List<string> ElementNameHierarchy)
            {
                try
                {
                    // Return empty string if no element hierarchy is passed in
                    if (ElementNameHierarchy == null || ElementNameHierarchy.Count == 0) return string.Empty;

                    // Create var to hold value to return
                    string ElementValue = string.Empty;

                    // Save parent element
                    var Element = StartingElement;

                    // Go through each step of the sequence returning the last value
                    for (int i = 0; i <= ElementNameHierarchy.Count - 1; i++)
                    {
                        // Check if this is the last step in the sequence
                        if (i == ElementNameHierarchy.Count - 1)
                        {
                            // If it is, save the value to return
                            ElementValue = Element.Element(ElementNameHierarchy[i]).Value.ToString();
                        }
                        else
                        {
                            // If not, go down to next level
                            Element = GetElement(Element, ElementNameHierarchy[i]);
                        }
                    }

                    // Return the value
                    return ElementValue;
                }
                catch (Exception ex)
                {
                    // Return nothing (not string.empty) for error so user can tell that exception occurred
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }

            /// <summary>
            /// Used to get the first value in a specified XML string that falls between a provided tag name (formats source XML and normalizes line endings prior to parsing).
            /// </summary>
            /// <param name="XMLStringToParse">The full XML string to parse.</param>
            /// <param name="TagName">The name of the XML tag to find. For example, 'MyTag' will find the first value between the start and end MyTag node in the string.</param>
            /// <returns>Returns a string containing the value between the first XML tag found that matches the specified tag name. Returns an empty string if the tag is not found or an error occurs.</returns>
            public static string GetValueBetweenTag(string XMLStringToParse, string TagName)
            {
                // Use alternate methods with flags set on
                return GetValueBetweenTag(XMLStringToParse, TagName, true, true);
            }
            /// <summary>
            /// Used to get the first value in a specified XML string that falls between a provided tag name.
            /// </summary>
            /// <param name="XMLStringToParse">The full XML string to parse.</param>
            /// <param name="TagName">The name of the XML tag to find. For example, 'MyTag' will find the first value between the start and end MyTag node in the string.</param>
            /// <param name="FormatXMLBeforeParse">Flag to indicate if the source XML should be formatted before parsing.</param>
            /// <param name="NormalizeLineEndingsBeforeParse">Flag to indicate if all line endings should be standardized to CrLf before parsing.</param>
            /// <returns>Returns a string containing the value between the first XML tag found that matches the specified tag name. Returns an empty string if the tag is not found or an error occurs.</returns>
            public static string GetValueBetweenTag(string XMLStringToParse, string TagName, bool FormatXMLBeforeParse, bool NormalizeLineEndingsBeforeParse)
            {
                // Use low level function to get all values
                var AllValues = GetValuesBetweenTag(XMLStringToParse, TagName, FormatXMLBeforeParse, NormalizeLineEndingsBeforeParse);

                // Return empty string if nothing came back
                if (AllValues == null || AllValues.Count == 0) return string.Empty;

                // If value came back, return the first one
                return AllValues[0];
            }

            /// <summary>
            /// Used to get all values in a specified XML string that fall between a provided tag name (formats source XML and normalizes line endings prior to parsing).
            /// </summary>
            /// <param name="XMLStringToParse">The full XML string to parse.</param>
            /// <param name="TagName">The name of the XML tag to find. For example, 'MyTag' will find all values between all start and end MyTag nodes in the string.</param>
            /// <returns>Returns a list of all values that fall between the specified XML tag name. Returns an empty list if the tag is not found. Returns NOTHING if an error occurs during parsing.</returns>
            public static List<string> GetValuesBetweenTag(string XMLStringToParse, string TagName)
            {
                // Use helper and return result
                return GetValuesBetweenTag(XMLStringToParse, TagName, true, true);
            }
            /// <summary>
            /// Used to get all values in a specified XML string that fall between a provided tag name.
            /// </summary>
            /// <param name="XMLStringToParse">The full XML string to parse.</param>
            /// <param name="TagName">The name of the XML tag to find. For example, 'MyTag' will find all values between all start and end MyTag nodes in the string.</param>
            /// <param name="FormatXMLBeforeParse">Flag to indicate if the source XML should be formatted before parsing.</param>
            /// <param name="NormalizeLineEndingsBeforeParse">Flag to indicate if all line endings should be standardized to CrLf before parsing.</param>
            /// <returns>Returns a list of all values that fall between the specified XML tag name. Returns an empty list if the tag is not found. Returns NOTHING if an error occurs during parsing.</returns>
            public static List<string> GetValuesBetweenTag(string XMLStringToParse, string TagName, bool FormatXMLBeforeParse, bool NormalizeLineEndingsBeforeParse)
            {
                try
                {
                    // Create the start and end tags from tag name
                    string StartTag = "<" + TagName + ">";
                    string EndTag = "</" + TagName + ">";

                    // Create list to return 
                    var ListToReturn = new List<string>();

                    // Return empty list if nothing is passed in
                    if (string.IsNullOrEmpty(XMLStringToParse) == true) return ListToReturn;

                    // If there is a string, return empty list if either tag is missing
                    if (XMLStringToParse.Contains(StartTag) == false || XMLStringToParse.Contains(EndTag) == false) return ListToReturn;

                    // If everything looks good, check if formatting before parse
                    if (FormatXMLBeforeParse == true)
                    {
                        // Try to get the visually formatted xml string
                        string FormattedXML = FormatString(XMLStringToParse);

                        // If formatting worked, overwrite original string
                        if (string.IsNullOrEmpty(FormattedXML) == false)
                        {
                            XMLStringToParse = FormattedXML;
                        }
                    }

                    // Check if normalizing line endings before parse
                    if (NormalizeLineEndingsBeforeParse == true)
                    {
                        // If so, ensure line endings are normalized
                        XMLStringToParse = Clean.NormalizeLineEndings(XMLStringToParse);
                    }

                    // After all potential formatting, split it into a list to iterate through
                    var SplitByEndOfLine = XMLStringToParse.Split(new[] { Generate.SpecialCharacter.CarriageReturnLineFeed(), Generate.SpecialCharacter.CarriageReturn(), Generate.SpecialCharacter.LineFeed() }, StringSplitOptions.RemoveEmptyEntries).ToList();

                    // Create list of KVPs to hold the start and end lines for stuff within tags
                    var StartAndEndLineIndexes = new List<KeyValuePair<int, int>>();

                    // Create vars to hold the next start and end index
                    int StartIndex = -1;
                    int EndIndex = -1;

                    // Loop through and find start and end indexes
                    for (int i = 0; i <= SplitByEndOfLine.Count - 1; i++)
                    {
                        // Save the current line
                        string CurrentLine = SplitByEndOfLine[i];

                        // Ignore if it isn't a start or end index
                        if (CurrentLine.Contains(StartTag) == false && CurrentLine.Contains(EndTag) == false)
                        {
                            continue;
                        }

                        // If it has a tag, check for start and end
                        if (CurrentLine.Contains(StartTag) == true) StartIndex = i;
                        if (CurrentLine.Contains(EndTag) == true) EndIndex = i;

                        // Check if both a start and end index have been found
                        if (StartIndex != -1 && EndIndex != -1)
                        {
                            // If so, add new kvp and clear the index vars for next loop
                            StartAndEndLineIndexes.Add(new KeyValuePair<int, int>(StartIndex, EndIndex));
                            StartIndex = -1;
                            EndIndex = -1;
                        }
                        else if (StartIndex == -1 && EndIndex != -1)
                        {
                            // If an end index was somehow found before a start index, clear it
                            EndIndex = -1;
                        }
                        else
                        {
                            // If not, just keep looping through file
                        }
                    }

                    // If no start / end pairs were found, return empty list
                    if (StartAndEndLineIndexes.Count == 0) return ListToReturn;

                    // If pairs were found, loop through and add lines to add to list
                    foreach (var PAIR in StartAndEndLineIndexes)
                    {
                        // Get lines between indexes
                        var LinesBetweenIndex = SplitByEndOfLine.GetRange(PAIR.Key, (PAIR.Value - PAIR.Key) + 1);

                        // Create var to hold concat'd lines
                        string ConcatedLines = string.Join(Environment.NewLine, LinesBetweenIndex);

                        // Remove start and end tags from concat'd line in case someone put xml tags on same line (i.e. <tag>VALUE</tag>)
                        ConcatedLines = ConcatedLines.Replace(StartTag, string.Empty).Replace(EndTag, string.Empty).Trim();

                        // Add concat'd info to list to return
                        ListToReturn.Add(ConcatedLines);
                    }

                    // Return the list when parsing is complete
                    return ListToReturn;
                }
                catch (Exception ex)
                {
                    // Return nothing if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }

            /// <summary>
            /// Used to get the first value in a specified XML string that falls between a provided tag name (formats source XML and normalizes line endings prior to parsing).
            /// </summary>
            /// <param name="XMLStringToParse">The full XML string to parse.</param>
            /// <param name="TagName">The name of the XML tag to find. For example, 'MyTag' will find the first value between the start and end MyTag node in the string.</param>
            /// <param name="UseXmlDocumentParse">Flag to indicate if XDocument should be used for XML parsing (vs. basic string parsing).</param>
            /// <returns>Returns a string containing the value between the first XML tag found that matches the specified tag name. Returns an empty string if the tag is not found or an error occurs.</returns>
            public static string GetValueBetweenTag(string XMLStringToParse, string TagName, bool UseXmlDocumentParse)
            {
                // If not using xdoc, just use string parse method
                if (UseXmlDocumentParse == false) return GetValueBetweenTag(XMLStringToParse, TagName);

                // If using xdoc, attempt to get all items between tags
                var AllItems = GetValuesBetweenTag(XMLStringToParse, TagName, true);

                // Return empty string if nothing comes back
                if (AllItems == null || AllItems.Count == 0) return string.Empty;

                // If items come back, return the first one
                return AllItems[0];
            }
            /// <summary>
            /// Used to get all values in a specified XML string that fall between a provided tag name (formats source XML and normalizes line endings prior to parsing).
            /// </summary>
            /// <param name="XMLStringToParse">The full XML string to parse.</param>
            /// <param name="TagName">The name of the XML tag to find. For example, 'MyTag' will find all values between all start and end MyTag nodes in the string.</param>
            /// <param name="UseXmlDocumentParse">Flag to indicate if XDocument should be used for XML parsing (vs. basic string parsing).</param>
            /// <returns>Returns a list of all values that fall between the specified XML tag name. Returns an empty list if the tag is not found. Returns NOTHING if an error occurs during parsing.</returns>
            public static List<string> GetValuesBetweenTag(string XMLStringToParse, string TagName, bool UseXmlDocumentParse)
            {
                try
                {
                    // If not using xdoc, just use string parse method
                    if (UseXmlDocumentParse == false) return GetValuesBetweenTag(XMLStringToParse, TagName);

                    // Return nothing if inputs are bad
                    if (string.IsNullOrEmpty(XMLStringToParse) == true || string.IsNullOrEmpty(TagName) == true) return null;

                    // If inputs look good, return nothing if tag is missing
                    if (XMLStringToParse.ToUpper().Contains(TagName.ToUpper()) == false) return null;

                    // If everything looks good, parse into xml document
                    var XML = System.Xml.Linq.XDocument.Parse(XMLStringToParse);

                    // If it parses, create list to hold info
                    var ItemsBetweenTag = new List<string>();

                    // Loop through and find items
                    foreach (var NODE in XML.Descendants(TagName))
                    {
                        ItemsBetweenTag.Add(NODE.Value);
                    }

                    // Return the info
                    return ItemsBetweenTag;
                }
                catch (Exception ex)
                {
                    // Return nothing if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }

            /// <summary>
            /// Used to walk down a tag tree / hierarchy in an XML string.
            /// </summary>
            /// <param name="XMLStringToParse">The full XML string to parse / walk through.</param>
            /// <param name="TagHierarchy">A list / hierarchy of tags to walk down (ordered outer-most tag to inner-most tag).</param>
            /// <returns>Returns the string value within the inner-most tag of the hierarchy.</returns>
            public static string GetValueWithinTagHierarchy(string XMLStringToParse, IEnumerable<string> TagHierarchy)
            {
                try
                {
                    // Return empty string if hierarchy is bad
                    if (TagHierarchy == null || TagHierarchy.Count() == 0) return string.Empty;

                    // Create string to hold the next subset xml to parse as loop walks through the tree
                    string ValueWithinTag = XMLStringToParse;

                    // Loop through walk down the xml tree
                    foreach (var TAG in TagHierarchy)
                    {
                        // Get the next subset of xml
                        ValueWithinTag = GetValueBetweenTag(ValueWithinTag, TAG);
                    }

                    // Return the string once the final tag is read
                    return ValueWithinTag;
                }
                catch (Exception ex)
                {
                    // Return an empty string if issue occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

        }

    }

}
