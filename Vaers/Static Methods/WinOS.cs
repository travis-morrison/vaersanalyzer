﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace StaticMethods
{

    public partial struct WinOS
    {

        #region Windows Information


        /// <summary>
        /// Class to hold all PC information.
        /// </summary>
        public class ComputerInfo
        {
            // Vars to hold info
            public readonly string Name = string.Empty;
            public readonly string OperatingSystemName = string.Empty;
            public readonly string OperatingSystemVersion = string.Empty;
            public readonly string OperatingSystemPlatform = string.Empty;
            public readonly string SystemDirectory = string.Empty;
            public readonly double MemoryAvailableInBytes = -1;
            public readonly double MemoryTotalInBytes = -1;
            public readonly int ProcessorCount = -1;
            public readonly bool IsOperatingSystem64Bit = false;
            public readonly bool IsProcessRunningAs64Bit = false;
            public readonly string ProcessActiveDirectory = string.Empty;
            public readonly string ProcessCmdLine = string.Empty;
            public readonly string UsersDomain = string.Empty;
            public readonly string UsersName = string.Empty;
            public readonly List<string> IPAddresses = new List<string>();
            public readonly List<string> MACAddresses = new List<string>();

            /// <summary>
            /// Creates a new instance of the class and captures all information about the operating system and PC.
            /// </summary>
            public ComputerInfo()
            {
                // Get all computer info using VB.NET helper
                var VBoNETComputerInfo = new Microsoft.VisualBasic.Devices.ComputerInfo();

                // Save basic PC info
                Name = System.Environment.MachineName;
                OperatingSystemName = VBoNETComputerInfo.OSFullName;
                OperatingSystemVersion = Environment.OSVersion.VersionString;
                OperatingSystemPlatform = Environment.OSVersion.Platform.ToString();
                SystemDirectory = Environment.SystemDirectory;

                // Save PC hardware information
                MemoryAvailableInBytes = VBoNETComputerInfo.AvailablePhysicalMemory;
                MemoryTotalInBytes = VBoNETComputerInfo.TotalPhysicalMemory;
                ProcessorCount = Environment.ProcessorCount;
                IsOperatingSystem64Bit = Environment.Is64BitOperatingSystem;

                // Save environment / process information
                IsProcessRunningAs64Bit = Environment.Is64BitProcess;
                ProcessActiveDirectory = Environment.CurrentDirectory;
                ProcessCmdLine = Environment.CommandLine;

                // Current user's information
                UsersDomain = Environment.UserDomainName;
                UsersName = Environment.UserName;

                // Get all IP addresses
                var AllIPs = System.Net.Dns.GetHostAddresses(System.Net.Dns.GetHostName());

                // Loop through and add string IPs to list
                foreach (var IP in AllIPs)
                {
                    // Skip if not valid IPv4
                    if (IsValidIPv4Format(IP.ToString()) == false) continue;

                    // If good, add to list
                    IPAddresses.Add(IP.ToString());
                }

                // Get all MAC addresses
                var AllNicInterfaces = System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces();

                // Loop through and get MACs
                foreach (var NIC in AllNicInterfaces)
                {
                    // Skip any that aren't online / connected
                    if (NIC.OperationalStatus != System.Net.NetworkInformation.OperationalStatus.Up) continue;

                    // If we find a NIC that's up / available, attempt to get it in XX:XX:XX:XX format
                    string MacInColonFormat = ConvertMacToColonFormat(NIC.GetPhysicalAddress().ToString());
                    if (string.IsNullOrEmpty(MacInColonFormat) == true)
                    {
                        // Skip if conversion failed
                        continue;
                    }

                    // If it worked, add to list
                    MACAddresses.Add(MacInColonFormat);
                }
            }
            private static bool IsValidIPv4Format(string IPToCheck)
            {
                try
                {
                    // Return false if nothing is sent in
                    if (string.IsNullOrEmpty(IPToCheck) == true) return false;

                    // If something is sent in, split it
                    var SplitIP = IPToCheck.Split(new[] { "." }, StringSplitOptions.RemoveEmptyEntries);

                    // Return false if there aren't 4 octets
                    if (SplitIP == null || SplitIP.Length != 4) return false;

                    // If there are 4 octets, loop and check for integers (can't be decimal b/c "." split)
                    foreach (var OCTET in SplitIP)
                    {
                        // Return false if not numeric
                        if (IsNumeric(OCTET) == false)
                        {
                            return false;
                        }

                        // Get the number value
                        int OctetValue = Convert.ToInt32(OCTET);

                        // Return false if not in valid range
                        if (OctetValue < 0 || OctetValue > 255)
                        {
                            return false;
                        }
                    }

                    // If everything looks good, return true
                    return true;
                }
                catch (Exception ex)
                {
                    // Return false if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }
            private static string ConvertMacToColonFormat(string MACToConvert)
            {
                try
                {
                    // Return empty string if missing
                    if (string.IsNullOrEmpty(MACToConvert) == true) return string.Empty;

                    // If we have a MAC, remove any chars that might be delimiting now
                    MACToConvert = MACToConvert.Replace("-", string.Empty);
                    MACToConvert = MACToConvert.Replace(":", string.Empty);

                    // Now we should have a pure hex string, return empty string if we have an invalid / odd number of chars
                    if (string.IsNullOrEmpty(MACToConvert) == true || MACToConvert.Length % 2 != 0) return string.Empty;

                    // Create list to hold the blocks of two chars to join back together with colons
                    var BlocksOfTwoChars = new List<string>();

                    // Loop through and get blocks of two (subtract one for 0 based i-loop)
                    for (int i = 0; i <= MACToConvert.Length - 1; i += 2)
                    {
                        BlocksOfTwoChars.Add(MACToConvert.Substring(i, 2));
                    }

                    // Join info back together and return
                    return string.Join(":", BlocksOfTwoChars);
                }
                catch (Exception ex)
                {
                    // Return empty string if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

            /// <summary>
            /// Returns all information concatenated into one large string message.
            /// </summary>
            public override string ToString()
            {
                // Create list to hold the information
                var LinesToJoin = new List<string>();

                try
                {
                    // Add PC info
                    LinesToJoin.Add("         Computer Name:  " + Name);
                    LinesToJoin.Add("           Computer OS:  " + OperatingSystemName);
                    LinesToJoin.Add("            OS Version:  " + OperatingSystemVersion);
                    LinesToJoin.Add("     Total Memory (GB):  " + BytesToGBs(MemoryTotalInBytes, NumberOfDigitsToRoundTo: 2).ToString());
                    LinesToJoin.Add(" Available Memory (GB):  " + BytesToGBs(MemoryAvailableInBytes, NumberOfDigitsToRoundTo: 2).ToString());
                    LinesToJoin.Add("       Processor Count:  " + ProcessorCount.ToString());
                    LinesToJoin.Add("          Is 64-Bit OS:  " + ConvertBooleanToYesNoString(IsOperatingSystem64Bit));
                    LinesToJoin.Add("         Is App 64-Bit:  " + ConvertBooleanToYesNoString(IsProcessRunningAs64Bit));
                    LinesToJoin.Add("      System Directory:  " + SystemDirectory);
                    LinesToJoin.Add("      Active Directory:  " + ProcessActiveDirectory);
                    LinesToJoin.Add("          Command Line:  " + ProcessCmdLine);
                    LinesToJoin.Add("         User's Domain:  " + UsersDomain);
                    LinesToJoin.Add("           User's Name:  " + UsersName);

                    // Check if there are IPs
                    if (IPAddresses != null && IPAddresses.Count > 0)
                    {
                        // If there are, add that info as well
                        for (int i = 0; i <= IPAddresses.Count - 1; i++)
                        {
                            LinesToJoin.Add(("IP Address " + (i + 1).ToString() + ":  ").PadLeft(25, System.Convert.ToChar(" ")) + IPAddresses[i]);
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Bolt on any exceptions that occur
                    LinesToJoin.Add("EXCEPTION ~ " + ex.Message);
                }

                // Join and return the info
                return string.Join(Environment.NewLine, LinesToJoin);
            }
        }

        /// <summary>
        /// Used to return all critical computer information on the device.
        /// </summary>
        /// <returns>Returns an instance of ComputerInfo containing all PC information.</returns>
        public static ComputerInfo GetComputerInfo()
        {
            // Return new instance of the computer info class
            return new ComputerInfo();
        }

        /// <summary>
        /// Used to return the major version of the operating system.
        /// </summary>
        /// <returns>Returns the number representing the major version of the OS. Returns NOTHING if the version cannot be determined.</returns>
        public static Nullable<int> GetWindowsVersionMajor()
        {
            try
            {
                // Return windows version
                return System.Environment.OSVersion.Version.Major;
            }
            catch (Exception ex)
            {
                // Return unknown
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Used to determine if the current running directory of the application is running on the C:\ drive.
        /// </summary>
        /// <param name="ShowErrorPromptIfRootNotOnC">OPTIONAL:  Boolean to set whether the function should prompt IF the application is NOT running on the C:\ drive.</param>
        /// <returns>Returns TRUE if the application is running on the C:\. Returns FALSE in all other conditions.</returns>
        public static bool IsWindowsRootOnC(bool ShowErrorPromptIfRootNotOnC = true)
        {
            try
            {
                // Get the unformatted software startup directory
                string UnformattedSoftwareDir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

                // Check if C:\ is in path
                if (UnformattedSoftwareDir.ToUpper().Contains(@"C:\") == false)
                {
                    // If not, prompt if requested and return false
                    if (ShowErrorPromptIfRootNotOnC == true) System.Windows.Forms.MessageBox.Show(@"The software is running from an invalid location and must end. Please ensure your software is installed on the ''C:\'' drive.", "Installation Directory Not Valid", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    return false;
                }

                // Return true if C is in root
                return true;
            }
            catch (Exception ex)
            {
                // Return false if error occurs somehow
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return false;
            }
        }


        #endregion

        #region Active Directory


        public struct ActiveDirectory
        {

            /// <summary>
            /// Used to check the Active Directory Service to test a given user credential and determine if it's valid.
            /// </summary>
            /// <param name="Username">The username to check.</param>
            /// <param name="Password">The password to check.</param>
            /// <param name="Domain">OPTIONAL:  If used, the credential will be checked against the domain. If not, the credential will be checked as a local account.</param>
            /// <returns>Returns TRUE if the credential is valid. Returns FALSE in all other conditions.</returns>
            public static bool VerifyUserCredentials(string Username, string Password, string Domain = "")
            {
                try
                {
                    // Key off of whether there is a domain
                    if (string.IsNullOrEmpty(Domain) == false)
                    {
                        // If there's a domain, use it and check credential
                        using (var PC = new System.DirectoryServices.AccountManagement.PrincipalContext(System.DirectoryServices.AccountManagement.ContextType.Domain, Domain))
                        {
                            return PC.ValidateCredentials(Username, Password);
                        }
                    }
                    else
                    {
                        // If there's NOT a domain, check local machine
                        using (var PC = new System.DirectoryServices.AccountManagement.PrincipalContext(System.DirectoryServices.AccountManagement.ContextType.Machine))
                        {
                            return PC.ValidateCredentials(Username, Password);
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Return false if check fails
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }

        }


        #endregion

        #region Font Information


        public struct Fonts
        {

            /// <summary>
            /// Used to check if a font is installed on the computer.
            /// </summary>
            /// <param name="FontName">The string name of the font to check for.</param>
            /// <returns>Returns TRUE if the font is installed. Returns FALSE in all other conditions.</returns>
            public static bool IsFontInstalled(string FontName)
            {
                // Get all font names
                var AllFonts = AllNames();

                // Return false if fonts not found
                if (AllFonts == null || AllFonts.Count == 0) return false;

                // If there are fonts, loop through and check
                foreach (var FONT in AllFonts)
                {
                    // Return true if match is found
                    if (FONT.ToUpper() == FontName.ToUpper() == true)
                    {
                        return true;
                    }
                }

                // Return false if no match is found
                return false;
            }

            /// <summary>
            /// Used to get a list of all fonts (by name) installed on the machine.
            /// </summary>
            /// <returns>Returns a list of all font names if successful. Returns NOTHING in all other conditions.</returns>
            public static List<string> AllNames()
            {
                try
                {
                    // Create list to return
                    var FontNames = new List<string>();

                    // Get all the installed fonts
                    using (var AllFonts = new System.Drawing.Text.InstalledFontCollection())
                    {
                        // Loop through and get each font name
                        foreach (System.Drawing.FontFamily Font in AllFonts.Families)
                        {
                            FontNames.Add(Font.Name);
                        }
                    }

                    // Return the list
                    return FontNames;
                }
                catch (Exception ex)
                {
                    // Return nothing if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }

        }


        #endregion

        #region .NET Framework Information


        public struct DotNetFramework
        {

            /// <summary>
            /// Used to request the HIGHEST version of the .NET Framework installed on the machine. Note that this method will only return information for version 4.5 AND NEWER of the Framework.
            /// </summary>
            /// <returns>Returns an instance of version info with the highest post-4.5 version of the Framework detected on the machine. Returns NOTHING if an error occurs OR the machine does not have .NET 4.5 or newer.</returns>
            public static Version GetNewestPost4o5VersionInstalled()
            {
                try
                {
                    // Get the framework version release key
                    var FrameworkReleaseKeyValue = GetVersionReleaseKey();

                    // Return nothing if there is no key (i.e. nothing at 4.5 or newer is installed)
                    if (FrameworkReleaseKeyValue == null) return null;

                    // If there is a release key value, create constants to hold the release keys for known versions
                    const int Framework4o5o0 = 378389;
                    const int Framework4o5o1 = 378675;
                    const int Framework4o5o2 = 379893;
                    const int Framework4o6o0 = 393295;
                    const int Framework4o6o1 = 394254;
                    const int Framework4o6o2 = 394802;
                    const int Framework4o7o0 = 460798;
                    const int Framework4o7o1 = 461308;

                    // Return the version that corresponds to the release key (start w/ newest to prevent returning if there's a newer version installed)
                    if (FrameworkReleaseKeyValue.Value >= Framework4o7o1) return new Version(4, 7, 1);
                    if (FrameworkReleaseKeyValue.Value >= Framework4o7o0) return new Version(4, 7, 0);
                    if (FrameworkReleaseKeyValue.Value >= Framework4o6o2) return new Version(4, 6, 2);
                    if (FrameworkReleaseKeyValue.Value >= Framework4o6o1) return new Version(4, 6, 1);
                    if (FrameworkReleaseKeyValue.Value >= Framework4o6o0) return new Version(4, 6, 0);
                    if (FrameworkReleaseKeyValue.Value >= Framework4o5o2) return new Version(4, 5, 2);
                    if (FrameworkReleaseKeyValue.Value >= Framework4o5o1) return new Version(4, 5, 1);
                    if (FrameworkReleaseKeyValue.Value >= Framework4o5o0) return new Version(4, 5, 0);

                    // Return nothing if there's an unknown release key (should not be possible)
                    return null;
                }
                catch (Exception ex)
                {
                    // Return nothing if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }

            /// <summary>
            /// Used to read and return the release key value in the registry that is used to indicate which version of the .NET Framework is installed on the machine (for versions 4.5 and newer of the .NET Framework).
            /// </summary>
            /// <returns>Returns an integer indicating the release key value of the Framework if found / installed. Returns NOTHING if the release key isn't found (i.e. the machine does NOT have version 4.5 or newer of the Framework).</returns>
            public static Nullable<int> GetVersionReleaseKey()
            {
                try
                {
                    // Save the registry sub-key and the value name to retrieve
                    const string FrameworkVersionSubKey = @"SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full\";
                    const string ValueNameToRetrieve = "Release";

                    // Open sub-key to get value to determine framework version's release key
                    using (var ReleaseKeyInfo = Microsoft.Win32.RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, Microsoft.Win32.RegistryView.Registry32).OpenSubKey(FrameworkVersionSubKey))
                    {
                        // Return nothing if key info is missing (i.e. we can't tell what version of .NET is in place)
                        if (ReleaseKeyInfo == null) return null;

                        // If there's key info, attempt to get the value that has the framework release key version
                        object ReleaseKeyObjectValue = ReleaseKeyInfo.GetValue(ValueNameToRetrieve);

                        // Return nothing if object is null or not numeric
                        if (ReleaseKeyObjectValue == null || IsNumeric(ReleaseKeyObjectValue) == false) return null;

                        // If it is numeric, convert to int and return
                        return Convert.ToInt32(ReleaseKeyObjectValue);
                    }
                }
                catch (Exception ex)
                {
                    // Return nothing if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }

        }


        #endregion

        #region Process Control & Management


        public struct Process
        {

            public struct Current
            {

                /// <summary>
                /// Class to hold all process information.
                /// </summary>
                public class ProcessInfo
                {
                    // Vars to hold info
                    public readonly string AssemblyName = string.Empty;
                    public readonly string CompanyName = string.Empty;
                    public readonly string Copyright = string.Empty;
                    public readonly string Description = string.Empty;
                    public readonly string DirectoryPath = string.Empty;
                    public readonly List<string> LoadedAssemblies = new List<string>();
                    public readonly string ProductName = string.Empty;
                    public readonly string Title = string.Empty;
                    public readonly string Trademark = string.Empty;
                    public readonly int VersionMajor = 0;
                    public readonly int VersionMinor = 0;
                    public readonly long InUseMemory = 0;

                    /// <summary>
                    /// Creates a new instance of the class and captures all information about the current running process.
                    /// </summary>
                    public ProcessInfo()
                    {
                        // Capture process info using VB.NET helper
                        var VBoNETProcInfo = new Microsoft.VisualBasic.ApplicationServices.AssemblyInfo(System.Reflection.Assembly.GetExecutingAssembly());

                        // Capture info
                        AssemblyName = VBoNETProcInfo.AssemblyName;
                        CompanyName = VBoNETProcInfo.CompanyName;
                        Copyright = VBoNETProcInfo.Copyright;
                        Description = VBoNETProcInfo.Description;
                        DirectoryPath = VBoNETProcInfo.DirectoryPath;
                        ProductName = VBoNETProcInfo.ProductName;
                        Title = VBoNETProcInfo.Title;
                        Trademark = VBoNETProcInfo.Trademark;
                        VersionMajor = VBoNETProcInfo.Version.Major;
                        VersionMinor = VBoNETProcInfo.Version.Minor;
                        InUseMemory = VBoNETProcInfo.WorkingSet;

                        // Loop through all loaded assemblies and add to list
                        foreach (var ASSEMBLY in VBoNETProcInfo.LoadedAssemblies)
                        {
                            LoadedAssemblies.Add(ASSEMBLY.FullName);
                        }
                    }

                    /// <summary>
                    /// Returns all information concatenated into one large string message.
                    /// </summary>
                    public override string ToString()
                    {
                        // Create list to hold all information to join
                        var LinesToJoin = new List<string>();

                        try
                        {
                            // Add all info
                            LinesToJoin.Add("         Assembly Name:  " + AssemblyName);
                            LinesToJoin.Add("     Loaded Assemblies:  " + string.Join(" | ", LoadedAssemblies));
                            LinesToJoin.Add("          Product Name:  " + ProductName);
                            LinesToJoin.Add("             Directory:  " + DirectoryPath);
                            LinesToJoin.Add("         Major Version:  " + VersionMajor);
                            LinesToJoin.Add("         Minor Version:  " + VersionMinor);
                            LinesToJoin.Add("    In Use Memory (GB):  " + BytesToGBs(InUseMemory, NumberOfDigitsToRoundTo: 2));
                            LinesToJoin.Add("           Description:  " + Description);
                            LinesToJoin.Add("               Company:  " + CompanyName);
                        }
                        catch (Exception ex)
                        {
                            // Add exception if we can't get info
                            LinesToJoin.Add("EXCEPTION ~ " + ex.Message);
                        }

                        // Join and return the info
                        return string.Join(Environment.NewLine, LinesToJoin);
                    }
                }

                /// <summary>
                /// Used to return all information for the current application / process.
                /// </summary>
                /// <returns>Returns an instance of ProcessInfo containing all information for the running application / process.</returns>
                public static ProcessInfo GetInfo()
                {
                    // Return info for this process
                    return new ProcessInfo();
                }

                /// <summary>
                /// Used to check if another instance of this process / application is running.
                /// </summary>
                /// <returns>Returns TRUE if a matching instance is found. Returns FALSE in all other conditions.</returns>
                public static bool IsRunning()
                {
                    try
                    {
                        // Get the name for the current process
                        string ThisProcess = System.Diagnostics.Process.GetCurrentProcess().ProcessName;

                        // Get the number of processes that are running that have this name
                        int ProcsWithThisName = System.Diagnostics.Process.GetProcessesByName(ThisProcess).Length;

                        // Return false IF there's only one instance running (this instance)
                        if (ProcsWithThisName <= 1) return false;

                        // If there's more processes running, return true (multiple instances)
                        return true;
                    }
                    catch (Exception ex)
                    {
                        // Return false if it can't be checked
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return false;
                    }
                }

                /// <summary>
                /// Used to check if the current process is running with admin privileges.
                /// </summary>
                /// <returns>Returns TRUE if the process is running with admin privileges. Returns FALSE in all other conditions.</returns>
                public static bool IsRunningAsAdmin()
                {
                    try
                    {
                        // Get current identity
                        using (var Identity = System.Security.Principal.WindowsIdentity.GetCurrent())
                        {
                            // Build principal object from identity to check current role
                            var Principal = new System.Security.Principal.WindowsPrincipal(Identity);

                            // Check if we're running as an admin and return the result
                            return Principal.IsInRole(System.Security.Principal.WindowsBuiltInRole.Administrator);
                        }
                    }
                    catch (Exception ex)
                    {
                        // Assume we're not if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return false;
                    }
                }

                /// <summary>
                /// Used to check if the current process is running with the Visual Studio debugger.
                /// </summary>
                /// <returns>Return TRUE if running within Visual Studio / the debugger. Returns FALSE in all other conditions.</returns>
                public static bool IsRunningInVisualStudio()
                {
                    try
                    {
                        // Check if debugger is attached and return result
                        return System.Diagnostics.Debugger.IsAttached;
                    }
                    catch (Exception ex)
                    {
                        // Return false if it can't be checked
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return false;
                    }
                }

                /// <summary>
                /// Used to check if the current code running is on the main / UI thread.
                /// </summary>
                /// <returns>Returns TRUE if on main / UI thread. Returns FALSE in all other conditions.</returns>
                public static bool IsOnMainThread()
                {
                    try
                    {
                        // Check if current thread is main thread (requires reference to PresentationFramework AND WindowsBase AND System.Xaml)
                        if (System.Windows.Application.Current.Dispatcher.Thread == System.Threading.Thread.CurrentThread)
                        {
                            // If so, return true
                            return true;
                        }
                        else
                        {
                            // If not, this is not the main thread
                            return false;
                        }
                    }
                    catch (Exception ex)
                    {
                        // Return false if thread cannot be checked
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return false;
                    }
                }

                /// <summary>
                /// Used to check if the current process is running as a WPF application.
                /// </summary>
                /// <returns>Returns TRUE if the current process is running as a WPF application. Returns FALSE in all conditions.</returns>
                public static bool IsWpfApplication()
                {
                    try
                    {
                        // Return false if application object is null (can't be WPF if that's missing)
                        if (System.Windows.Application.Current == null) return false;

                        // Return false if the dispatcher is null (don't think this is possible, just being cautious)
                        if (System.Windows.Application.Current.Dispatcher == null) return false;

                        // Return true if both are there
                        return true;
                    }
                    catch (Exception ex)
                    {
                        // Return false if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return false;
                    }
                }

                /// <summary>
                /// Used to kill all other open instances of this process (i.e. to prevent duplicate instances).
                /// </summary>
                public static void KillDuplicateInstances()
                {
                    try
                    {
                        // Get the name for the current process (this is the name that will need to be killed if duplicates are open)
                        string ThisProcessName = System.Diagnostics.Process.GetCurrentProcess().ProcessName;

                        // Get the ID of this process so we can use it to prevent this instance from being killed (i.e. only other instances will be killed)
                        int ThisProcessID = System.Diagnostics.Process.GetCurrentProcess().Id;

                        // Get all running processes
                        var AllRunningProcesses = System.Diagnostics.Process.GetProcesses();

                        // Loop through each process and check for ones with same name but different ID (i.e. duplicate instances)
                        foreach (var CurrentProcess in AllRunningProcesses)
                        {
                            // If a match is found, kill it
                            if (CurrentProcess.ProcessName == ThisProcessName && CurrentProcess.Id != ThisProcessID)
                            {
                                try
                                {
                                    // If a duplicate is found, kill it
                                    CurrentProcess.Kill();
                                }
                                catch (Exception ex)
                                {
                                    // Log errors
                                    System.Diagnostics.Trace.WriteLine(ex.Message);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        // Log errors
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                    }
                }

                /// <summary>
                /// Used to determine how the current application is running (e.g. x86, x64, etc.).
                /// </summary>
                /// <returns>Returns the number of bits of the Target CPU (x86=32, x64=64). Returns NOTHING if the architecture cannot be determined.</returns>
                public static Nullable<int> GetTargetCPU()
                {
                    try
                    {
                        // Get the IntPtr size
                        int IntPtrSize = IntPtr.Size;

                        // Multiply it by 8 to get bit size for the target CPU
                        return (8 * IntPtrSize);
                    }
                    catch (Exception ex)
                    {
                        // Return null if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return null;
                    }
                }

                /// <summary>
                /// Used to get the root / install directory of the running application.
                /// </summary>
                /// <returns>Returns the file path of the directory where the application is currently running from. Returns nothing if the running directory cannot be determined.</returns>
                public static string GetRootDirectory()
                {
                    try
                    {
                        // Attempt WinForms method first
                        return System.Windows.Forms.Application.StartupPath;
                    }
                    catch (Exception)
                    {
                        // No action needed
                    }

                    try
                    {
                        // Try to use reflection to figure out where app is running from
                        return System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                    }
                    catch (Exception)
                    {
                        // No action needed
                    }

                    try
                    {
                        // If reflection doesn't work, try using startup info from environment (first cmd line arg is normally the path to the running process)
                        return System.IO.Path.GetDirectoryName(Environment.GetCommandLineArgs()[0]);
                    }
                    catch (Exception)
                    {
                        // No action needed
                    }

                    // If nothing works, return empty string
                    return string.Empty;
                }

                /// <summary>
                /// Used to get and return the stack trace for the current application / process.
                /// </summary>
                /// <returns>Returns a string containing the full stack trace of the current application if retrievable. Returns an empty string if the stack trace cannot be read.</returns>
                public static string GetStackTrace()
                {
                    try
                    {
                        // Attempt to get and return the stack trace
                        return Environment.StackTrace;
                    }
                    catch (Exception ex)
                    {
                        // Returns an empty string if stack trace can't be read
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return string.Empty;
                    }
                }

                /// <summary>
                /// Used to launch a thread that will regularly request async garbage collections at more frequent intervals then the application / process normally would.
                /// </summary>
                /// <param name="TimeBetweenCollectionsInMS">The amount of time in milliseconds between forced collection requests.</param>
                public static void ForceRegularGarbageCollectionAsync(int TimeBetweenCollectionsInMS)
                {
                    // Create and launch thread to perform regular garbage collections
                    var thrForceGarbageCollections = new System.Threading.Thread(() => ForceRegularGarbageCollection(TimeBetweenCollectionsInMS));
                    thrForceGarbageCollections.SetApartmentState(System.Threading.ApartmentState.STA);
                    thrForceGarbageCollections.Priority = System.Threading.ThreadPriority.Lowest;
                    thrForceGarbageCollections.IsBackground = true;
                    thrForceGarbageCollections.Start();
                }
                private static void ForceRegularGarbageCollection(int TimeBetweenCollectionsInMS)
                {
                    // Exit if time is invalid
                    if (TimeBetweenCollectionsInMS <= 0) TimeBetweenCollectionsInMS = 5000;

                    // Loop while app is running and request collections
                    while (true)
                    {
                        // Wait the specified amount between requests
                        System.Threading.Thread.Sleep(TimeBetweenCollectionsInMS);

                        // Request garbage collection with no initial delay
                        RequestGarbageCollection(0);
                    }
                }

                /// <summary>
                /// Used to request garbage collection on a background thread to prevent UI locking / visible performance artifacts.
                /// </summary>
                public static void RequestGarbageCollectionAsync()
                {
                    // Launch thread to perform garbage collection
                    var thrCollect = new System.Threading.Thread(() => RequestGarbageCollection(-1));
                    thrCollect.Priority = System.Threading.ThreadPriority.BelowNormal;
                    thrCollect.SetApartmentState(System.Threading.ApartmentState.STA);
                    thrCollect.IsBackground = true;
                    thrCollect.Start();
                }
                /// <summary>
                /// Used to request garbage collection on a background thread to prevent UI locking / visible performance artifacts.
                /// </summary>
                /// <param name="PreRequestDelayInMS">The number of milliseconds to wait before the garbage collection is requested (i.e. a means of performing a delayed request).</param>
                public static void RequestGarbageCollectionAsync(int PreRequestDelayInMS)
                {
                    // Launch thread to perform garbage collection
                    var thrCollect = new System.Threading.Thread(() => RequestGarbageCollection(PreRequestDelayInMS));
                    thrCollect.Priority = System.Threading.ThreadPriority.BelowNormal;
                    thrCollect.SetApartmentState(System.Threading.ApartmentState.STA);
                    thrCollect.IsBackground = true;
                    thrCollect.Start();
                }
                /// <summary>
                /// Private method to request the garbage collection on background thread to prevent UI locking.
                /// </summary>
                private static void RequestGarbageCollection(int PreRequestDelayInMS)
                {
                    // Perform delay if time is valid
                    if (PreRequestDelayInMS > 0) System.Threading.Thread.Sleep(PreRequestDelayInMS);

                    try
                    {
                        // Request the garbage collection
                        GC.Collect();
                    }
                    catch (Exception ex)
                    {
                        // Log errors
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                    }
                }
            }

            /// <summary>
            /// Used to check if another process / EXE is currently running.
            /// </summary>
            /// <param name="ProcessNameToCheckFor">The name of the process(as it appears in Windows Task Manager) to check for.</param>
            /// <returns>Returns TRUE if a process matching the specified name is found. Returns FALSE in all other conditions.</returns>
            public static bool IsRunning(string ProcessNameToCheckFor)
            {
                // Get the number of processes that are running that have the requested name (remove possible file extension)
                int ProcsWithThisName = System.Diagnostics.Process.GetProcessesByName(System.IO.Path.GetFileNameWithoutExtension(ProcessNameToCheckFor)).Length;

                // Return false IF there's no instances running
                if (ProcsWithThisName == 0) return false;

                // If there's one or more procs running, return true
                return true;
            }

            /// <summary>
            /// Used to request that a specified process's window be given focus.
            /// </summary>
            /// <param name="ProcessName">The name of the process to give focus to.</param>
            /// <returns>Returns TRUE if the focus on the process's window is requested successfully. Returns FALSE in all other conditions.</returns>
            public static bool GiveFocusToWindow(string ProcessName)
            {
                // Use underlying helper and return result
                return GiveFocusToWindow(ProcessName, true);
            }
            /// <summary>
            /// Used to request that a specified process's window be given focus.
            /// </summary>
            /// <param name="ProcessName">The name of the process to give focus to.</param>
            /// <param name="MatchExact">Flag to indicate if the name must match exactly or only contain the search string / process name.</param>
            /// <returns>Returns TRUE if the focus on the process's window is requested successfully. Returns FALSE in all other conditions.</returns>
            public static bool GiveFocusToWindow(string ProcessName, bool MatchExact)
            {
                try
                {
                    // Get all processes
                    var AllRunningProcesses = System.Diagnostics.Process.GetProcesses().ToList();

                    // Loop through to look for matches
                    foreach (var PROC in AllRunningProcesses)
                    {
                        // Create flag to hold if the current process should be brought to front
                        bool BringThisProcessToFront = false;

                        // Check if we're looking for exact match or contains
                        switch (MatchExact)
                        {
                            case true:
                                // If exact, set flag if they're identical
                                if (PROC.ProcessName == ProcessName) BringThisProcessToFront = true;
                                break;
                            case false:
                                // If not exact, use contains and ignore case
                                if (PROC.ProcessName.ToUpper().Contains(ProcessName.ToUpper()) == true) BringThisProcessToFront = true;
                                break;
                        }

                        // Bring forward if this process is a match
                        if (BringThisProcessToFront == true)
                        {
                            GiveFocusToWindow(PROC);
                        }
                    }

                    // Return true if all process
                    return true;
                }
                catch (Exception ex)
                {
                    // Return false if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }

            /// <summary>
            /// Used to request that a specified process's window be given focus.
            /// </summary>
            /// <param name="ProcessHandle">The handle to the process to give focus to.</param>
            /// <returns>Returns TRUE if the focus on the process's window is requested successfully. Returns FALSE in all other conditions.</returns>
            public static bool GiveFocusToWindow(System.Diagnostics.Process ProcessHandle)
            {
                try
                {
                    // Return false if there is no handle to process
                    if (ProcessHandle == null) return false;

                    try
                    {
                        // If we have a process, try simple method first
                        Microsoft.VisualBasic.Interaction.AppActivate(ProcessHandle.Id);
                    }
                    catch (Exception ex)
                    {
                        // Log errors
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                    }

                    // If we have a process, attempt to get the window handle as an automation element (requires FRAMEWORK reference to UIAutomationClient)
                    var ProcessWindow = System.Windows.Automation.AutomationElement.FromHandle(ProcessHandle.MainWindowHandle);

                    // Return false if we can't get a window handle
                    if (ProcessWindow == null) return false;

                    // If we do have a window, try and focus
                    ProcessWindow.SetFocus();

                    // Return true if it works
                    return true;
                }
                catch (Exception ex)
                {
                    // Return false if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }

            /// <summary>
            /// Launches another process using .NET's 'Process.Start()' method.
            /// </summary>
            /// <param name="FullFilePathToRun">The full string file path of the file to launch.</param>
            /// <param name="RunAsAdmin">Boolean indicating whether the file should be launched at the administrator authority level.</param>
            /// <param name="WaitForProcessToFinish">Boolean indicating whether the function should wait for the file to finish executing before returning to the calling routine.</param>
            /// <param name="CmdLineArguments">OPTIONAL:  A list of command line arguments to pass to the process.</param>
            /// <param name="WindowStyle">OPTIONAL:  The window focus / style to use when starting the process.</param>
            /// <returns>Returns TRUE if the process starts successfully. Returns FALSE in all other conditions.</returns>
            public static bool Launch(string FullFilePathToRun, bool RunAsAdmin, bool WaitForProcessToFinish, IEnumerable<string> CmdLineArguments = null, System.Diagnostics.ProcessWindowStyle WindowStyle = System.Diagnostics.ProcessWindowStyle.Normal)
            {
                // Return result of low level method
                return Launch(FullFilePathToRun, CmdLineArguments, RunAsAdmin, WindowStyle, WaitForProcessToFinish);
            }
            /// <summary>
            /// Launches another process using .NET's 'Process.Start()' method and returns the standard text output from the process.
            /// </summary>
            /// <param name="FullFilePathToRun">The full string file path of the file to launch.</param>
            /// <param name="RunAsAdmin">Boolean indicating whether the file should be launched at the administrator authority level.</param>
            /// <param name="CmdLineArguments">OPTIONAL:  A list of command line arguments to pass to the process.</param>
            /// <param name="WindowStyle">OPTIONAL:  The window focus / style to use when starting the process.</param>
            /// <param name="MethodToInvokeWhenProcessOutputLineIsReceived">OPTIONAL:  Method to invoke and pass standard output text lines to from process that was launched.</param>
            /// <param name="TimeoutInSeconds">OPTIONAL:  The amount of time in seconds before the request will abort and fail.</param>
            /// <returns>Returns a string containing the full standard text output from the process if the process is started successfully. Returns an empty string if the process cannot be started or it generates no output.</returns>
            public static string Launch(string FullFilePathToRun, bool RunAsAdmin, IEnumerable<string> CmdLineArguments = null, System.Diagnostics.ProcessWindowStyle WindowStyle = System.Diagnostics.ProcessWindowStyle.Normal, Action<string> MethodToInvokeWhenProcessOutputLineIsReceived = null, Nullable<double> TimeoutInSeconds = default(Double?))
            {
                // Return result of low level method
                return Launch(FullFilePathToRun, CmdLineArguments, RunAsAdmin, WindowStyle, MethodToInvokeWhenProcessOutputLineIsReceived, TimeoutInSeconds);
            }
            private static bool Launch(string FullFilePathToRun, IEnumerable<string> CmdLineArguments, bool RunAsAdmin, System.Diagnostics.ProcessWindowStyle WindowStyle, bool WaitForProcessToFinish)
            {
                // Get the cmd line argument string to use
                string ArgumentString = CreateCmdLineArgumentString(CmdLineArguments);

                // Get the process start info
                var StartInfo = CreateProcessStartInfo(FullFilePathToRun, ArgumentString, RunAsAdmin, WindowStyle, false);

                // Create var to hold if process launched correctly
                bool ProcessLaunchedCorrectly = false;

                // Create process outside of try so it can be disposed in finally block
                System.Diagnostics.Process TheProcess = null;

                try
                {
                    // Start the process
                    TheProcess = System.Diagnostics.Process.Start(StartInfo);

                    // Wait for process to finish if requested
                    if (WaitForProcessToFinish == true)
                    {
                        TheProcess.WaitForExit();
                    }

                    // Save true if everything worked
                    ProcessLaunchedCorrectly = true;
                }
                catch (Exception ex)
                {
                    // If error occurs, prompt if requested and save error flag
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    ProcessLaunchedCorrectly = false;
                }
                finally
                {
                    // Clean up
                    if (TheProcess != null)
                    {
                        TheProcess.Dispose();
                    }
                }

                // Return the result of the launch
                return ProcessLaunchedCorrectly;
            }
            private static string Launch(string FullFilePathToRun, IEnumerable<string> CmdLineArguments, bool RunAsAdmin, System.Diagnostics.ProcessWindowStyle WindowStyle, Action<string> MethodToInvokeWhenStandardOutputLineIsReceived, Nullable<double> TimeoutInSeconds)
            {
                // Get the cmd line argument string to use
                string ArgumentString = CreateCmdLineArgumentString(CmdLineArguments);

                // Get the process start info
                var StartInfo = CreateProcessStartInfo(FullFilePathToRun, ArgumentString, RunAsAdmin, WindowStyle, true);

                // Create var to hold string output
                string OutputFromProcess = string.Empty;

                // Create process outside of try so it can be disposed in finally block
                System.Diagnostics.Process TheProcess = null;

                try
                {
                    // Get the start time to watch for timeout
                    var StartTime = DateTime.Now;

                    // Start the process
                    TheProcess = System.Diagnostics.Process.Start(StartInfo);

                    // Create reader to read the streamed output from the process
                    using (var OutputReader = TheProcess.StandardOutput)
                    {
                        // Keep reading data stream from the file until the end of the stream is found
                        while (OutputReader.EndOfStream == false)
                        {
                            // Capture the next line
                            string NextLine = OutputReader.ReadLine();

                            // If a method to call was specified, invoke it
                            if (MethodToInvokeWhenStandardOutputLineIsReceived != null)
                            {
                                System.Windows.Application.Current.Dispatcher.Invoke(() => MethodToInvokeWhenStandardOutputLineIsReceived(NextLine));
                            }

                            // Add to the string to the line to return
                            OutputFromProcess += NextLine + Environment.NewLine;

                            // Check if a timeout amount was specified
                            if (TimeoutInSeconds != null && TimeoutInSeconds > 0)
                            {
                                // If there is a timeout, check if timeout occurred
                                if ((DateTime.Now - StartTime).TotalSeconds >= TimeoutInSeconds)
                                {
                                    // If so, update output string, kill the process, and exit loop
                                    OutputFromProcess += "TIMEOUT AFTER " + TimeoutInSeconds.ToString() + " SECONDS AT " + DateTime.Now.ToString();
                                    TheProcess.Kill();
                                    break;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    // If error occurs, save error msgs
                    OutputFromProcess += ex.Message;
                }
                finally
                {
                    // Clean up
                    if (TheProcess != null)
                    {
                        TheProcess.Dispose();
                    }
                }

                // Return the output
                return OutputFromProcess;
            }
            private static string CreateCmdLineArgumentString(IEnumerable<string> CmdLineArguments)
            {
                // Create var to hold argument string
                string ArgumentString = string.Empty;

                // Create argument string from inputs
                if (CmdLineArguments != null && CmdLineArguments.Count() > 0)
                {
                    foreach (var ARG in CmdLineArguments)
                    {
                        ArgumentString += ARG + " ";
                    }
                }

                // Trim the string to remove last space if there were args
                ArgumentString = ArgumentString.Trim();

                // Return the string
                return ArgumentString;
            }
            private static System.Diagnostics.ProcessStartInfo CreateProcessStartInfo(string FullFilePathToRun, string ArgumentString, bool RunAsAdmin, System.Diagnostics.ProcessWindowStyle WindowStyle, bool InterceptOuptut)
            {
                // Create var to hold if window will be hidden
                bool HideWindow = false;

                // Set flag if window is hidden
                if (WindowStyle == System.Diagnostics.ProcessWindowStyle.Hidden)
                {
                    HideWindow = true;
                }

                // Create process start info
                var ProcStartInfo = new System.Diagnostics.ProcessStartInfo();

                // Save process info
                ProcStartInfo.FileName = FullFilePathToRun;
                ProcStartInfo.Arguments = ArgumentString;
                ProcStartInfo.UseShellExecute = !InterceptOuptut;
                ProcStartInfo.RedirectStandardOutput = InterceptOuptut;
                ProcStartInfo.RedirectStandardInput = InterceptOuptut;
                ProcStartInfo.CreateNoWindow = HideWindow;
                ProcStartInfo.WindowStyle = WindowStyle;

                // If admin is requested, see if running vista or higher
                if (RunAsAdmin == true && System.Environment.OSVersion.Version.Major >= 6)
                {
                    // Add run as admin verb in vista higher (UAC added in vista)
                    ProcStartInfo.Verb = "runas";
                }

                // Return the start info
                return ProcStartInfo;
            }

            /// <summary>
            /// Used to launch a process via a batch file so a delay prior to launch can be added if needed.
            /// </summary>
            /// <param name="FullFilePath">The full file path of the EXE to launch.</param>
            /// <param name="TimeToWaitBeforeLaunchInSeconds">The number of seconds to wait before launching the process.</param>
            public static void LaunchViaBatchFile(string FullFilePath, uint TimeToWaitBeforeLaunchInSeconds)
            {
                // Exit if file doesn't exist
                if (System.IO.File.Exists(FullFilePath) == false) return;

                // Create constant for quotation char
                const string QuotationMark = "\"";

                // Create list to hold text for bat file
                var BatContents = new List<string>();

                // Create vars to hold bat file name / path
                string RelaunchFileName = "Relaunch.bat";
                string RelaunchFilePath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), RelaunchFileName);

                // Add initial wait / sleep time before launching file
                if (TimeToWaitBeforeLaunchInSeconds > 0) BatContents.Add("TIMEOUT " + TimeToWaitBeforeLaunchInSeconds.ToString());

                // Add cmd to start the file
                BatContents.Add("start " + QuotationMark + QuotationMark + " " + QuotationMark + FullFilePath + QuotationMark);

                // Add the cmd so the file deletes itself when done
                BatContents.Add("TIMEOUT 1");
                BatContents.Add("DEL " + QuotationMark + RelaunchFileName + QuotationMark);

                try
                {
                    // Try to write the file
                    System.IO.File.WriteAllLines(RelaunchFilePath, BatContents.ToArray());
                }
                catch (Exception ex)
                {
                    // Exit if file can't be written
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return;
                }

                // Create the process to launch
                var ProcToLaunch = new System.Diagnostics.ProcessStartInfo(FullFilePath);
                ProcToLaunch.WindowStyle = System.Diagnostics.ProcessWindowStyle.Minimized;

                // Start the process minimized so user doesn't see cmd window
                System.Diagnostics.Process.Start(ProcToLaunch);
            }

            /// <summary>
            /// Used to kill a process running in Windows Task Manager.
            /// </summary>
            /// <param name="ProcessNameToKill">The exact name of the process to kill as it appears in Windows Task Manager. Note that ALL processes that match this name will be killed...tread carefully!</param>
            /// <returns>Returns TRUE if the process is killed OR not running. Returns FALSE in all other conditions.</returns>
            public static bool Kill(string ProcessNameToKill)
            {
                try
                {
                    // Ensure process name doesn't have exe (you kill w/ name only)
                    ProcessNameToKill = System.IO.Path.GetFileNameWithoutExtension(ProcessNameToKill);

                    // Get all processes
                    var AllRunningProcesses = System.Diagnostics.Process.GetProcesses();

                    // Loop through each process and check if names match (using UPPER CASE for both)
                    foreach (var CurrentProcess in AllRunningProcesses)
                    {
                        // If a match is found, kill it
                        if (CurrentProcess.ProcessName.ToUpper() == ProcessNameToKill.ToUpper())
                        {
                            CurrentProcess.Kill();
                        }
                    }

                    // Return true if kill(s) works
                    return true;
                }
                catch (Exception ex)
                {
                    // Return false if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }

        }


        #endregion

        #region Service Control & Management


        public struct Service
        {

            /// <summary>
            /// Used to start a given service.
            /// </summary>
            /// <param name="ServiceName">The name of the service to start.</param>
            /// <returns>Returns TRUE if the process starts successfully. Returns FALSE in all other conditions.</returns>
            public static bool Start(string ServiceName)
            {
                try
                {
                    // Create new service instance
                    using (System.ServiceProcess.ServiceController Service = new System.ServiceProcess.ServiceController(ServiceName))
                    {
                        // If service is stopped, attempt to start it
                        if (Service.Status == System.ServiceProcess.ServiceControllerStatus.Stopped || Service.Status == System.ServiceProcess.ServiceControllerStatus.StopPending)
                        {
                            Service.Start();
                        }
                    }

                    // Return true if service is already started or is started successfully
                    return true;
                }
                catch (Exception ex)
                {
                    // Return false if error occurs or service can't be started
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }

            /// <summary>
            /// Used to stop a given service.
            /// </summary>
            /// <param name="ServiceName">The name of the service to stop.</param>
            /// <returns>Returns TRUE if the process stops successfully. Returns FALSE in all other conditions.</returns>
            public static bool Stop(string ServiceName)
            {
                try
                {
                    // Create new service instance
                    using (var Service = new System.ServiceProcess.ServiceController(ServiceName))
                    {
                        // If service is running, attempt to stop it
                        if (Service.Status != System.ServiceProcess.ServiceControllerStatus.Stopped && Service.Status != System.ServiceProcess.ServiceControllerStatus.StopPending)
                        {
                            Service.Stop();
                        }
                    }

                    // Return true if service is already stopped or is stopped successfully
                    return true;
                }
                catch (Exception ex)
                {
                    // Return false if error occurs or service can't be stopped
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }

            /// <summary>
            /// Used to disable a service to it will not start on next boot up.
            /// </summary>
            /// <param name="ServiceName">The service name to disable.</param>
            /// <returns>Returns TRUE if the service is disabled. Returns FALSE in all other conditions.</returns>
            public static bool Disable(string ServiceName)
            {
                try
                {
                    // Attempt to disable service
                    System.Diagnostics.Process.Start("sc.exe", "config " + ServiceName + " start= disabled");

                    // Return true if service is disabled
                    return true;
                }
                catch (Exception ex)
                {
                    // Return false if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }

        }


        #endregion

        #region Hardware Resources


        public struct Devices
        {

            /// <summary>
            /// Class used to return info about connected USB devices.
            /// </summary>
            public class DeviceManagerInfo
            {
                // Vars to hold info about the System.Managment.ManagementObject
                public string ClassName = string.Empty;
                public string NamespacePath = string.Empty;

                // Vars to hold xml and json versions of info
                public string AsXml = string.Empty;
                public string AsJson = string.Empty;

                // Var to hold info about all properties of the device
                public readonly Dictionary<string, string> PropertyNamesAndValues = new Dictionary<string, string>();

                // Override ToString() for ease of reading
                public override string ToString()
                {
                    // Create list to hold info to merge
                    var LinesToMerge = new List<string>();

                    // Add name info
                    LinesToMerge.Add("Class Name:  " + ClassName);
                    LinesToMerge.Add(" Namespace:  " + NamespacePath);
                    LinesToMerge.Add("Properties:");

                    // Check if we have properties
                    if (PropertyNamesAndValues.Count == 0)
                    {
                        // If not, just show N/A
                        LinesToMerge.Add(" > None Found!");
                    }
                    else
                    {
                        // If we do have info, add it
                        foreach (var PAIR in PropertyNamesAndValues)
                        {
                            LinesToMerge.Add(" > " + PAIR.Key + " = " + PAIR.Value);
                        }
                    }

                    // Join and return info
                    return string.Join(Environment.NewLine, LinesToMerge);
                }
            }

            /// <summary>
            /// Used to return all serial port names on the machine sorted correctly (i.e. so 'COM10' doesn't come before 'COM2' like it would with normal string sorting).
            /// </summary>
            /// <returns>Returns a list of all serial port names sorted correctly if the function executes correctly. Returns NOTHING in all other conditions.</returns>
            public static List<string> GetSortedSerialPortNames()
            {
                try
                {
                    // Get serial ports for all boards
                    var AvailableSerialPorts = System.IO.Ports.SerialPort.GetPortNames().ToList();

                    // Start by sorting the list normally
                    AvailableSerialPorts.Sort();

                    // Create new list to hold true sort
                    var PortsSortedCorrectly = new List<string>();

                    // Go through and add all of the ports from the main list by string length because they don't sort correctly normally b/c they are strings (i.e. COM1, COM10, COM2, etc.)
                    for (int i = 4; i <= 7; i++)
                    {
                        foreach (var PORT in AvailableSerialPorts)
                        {
                            if (PORT.Length == i)
                            {
                                PortsSortedCorrectly.Add(PORT);
                            }
                        }
                    }

                    // Return the list
                    return PortsSortedCorrectly;
                }
                catch (Exception ex)
                {
                    // Return nothing if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }

            /// <summary>
            /// Used to get all connected serial port numbers (i.e. the number of the port without the COM prefix).
            /// </summary>
            public static List<UInt16> GetSerialPortNumbers()
            {
                try
                {
                    // Get all the port names in order
                    var PortNames = GetSortedSerialPortNames();
                    if (PortNames == null)
                    {
                        // Return nothing if error occurs
                        return null;
                    }

                    // If we have info, create list to hold just the numbers
                    var JustTheNumbers = new List<UInt16>();

                    // Loop through and convert
                    foreach (var PORTNAME in PortNames)
                    {
                        JustTheNumbers.Add(Convert.ToUInt16(PORTNAME.ToUpper().Replace("COM", string.Empty)));
                    }

                    // Return the ports
                    return JustTheNumbers;
                }
                catch (Exception ex)
                {
                    // Return nothing if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }

            /// <summary>
            /// Used to lookup the friendly name for all serial ports.
            /// </summary>
            /// <returns>Returns a list of the friendly name or all connected serial ports if successful. Returns NOTHING in all other conditions.</returns>
            public static List<string> GetSerialPortFriendlyNames()
            {
                try
                {
                    // Get the full dictionary
                    var FullDictionaryMap = GetSerialPortsWithFriendlyNames();
                    if (FullDictionaryMap == null)
                    {
                        // Return nothing if that fails
                        return null;
                    }

                    // If we get it back, return the values / friendly names
                    return FullDictionaryMap.Values.ToList();
                }
                catch (Exception ex)
                {
                    // Return nothing if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }

            /// <summary>
            /// Used to lookup the friendly name for each serial port and return it in a dictionary mapping it back to the COM port number.
            /// </summary>
            /// <returns>Returns a dictionary containing the port number and friendly name of that port number if successful. Returns NOTHING in all other conditions.</returns>
            public static Dictionary<UInt16, string> GetSerialPortsWithFriendlyNames()
            {
                try
                {
                    // Get all sorted serial port names so we can verify the port is legit
                    var AllSortedSerialPortNames = GetSortedSerialPortNames();
                    if (AllSortedSerialPortNames == null)
                    {
                        // Return nothing if we can't
                        return null;
                    }

                    // Get all names in device manager
                    var AllDeviceNames = GetDeviceManagerHardwareNames(true);
                    if (AllDeviceNames == null)
                    {
                        // Return nothing if error occurs
                        return null;
                    }

                    // If we get something back, create dictionary to hold port numbers and friendly name
                    var PortsAndFriendlyNames = new Dictionary<UInt16, string>();

                    // Go through and add port names defaulted to COMx (in case there isn't a friendly name)
                    foreach (var COMPORT in AllSortedSerialPortNames)
                    {
                        // Save the actual port number (i.e. get rid of COM in the string)
                        var PortNumber = Convert.ToUInt16(COMPORT.ToUpper().Replace("COM", string.Empty));

                        // Add an entry for the port
                        PortsAndFriendlyNames.Add(PortNumber, COMPORT);
                    }

                    // Once we have all the com ports that we need to find, save just the raw port numbers to search for
                    var PortNumbersToFind = PortsAndFriendlyNames.Keys.ToList();

                    // Loop through and look for friendly names
                    foreach (var PORT in PortNumbersToFind)
                    {
                        foreach (var ENTRY in AllDeviceNames)
                        {
                            // Check if we have a match
                            if (ENTRY.Contains("(COM" + PORT.ToString() + ")") == true)
                            {
                                // If found, save the friendly name and go on to next
                                PortsAndFriendlyNames[PORT] = ENTRY;
                                break;
                            }
                        }
                    }

                    // Once we're done, return info
                    return PortsAndFriendlyNames;
                }
                catch (Exception ex)
                {
                    // Return nothing if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }

            /// <summary>
            /// Used to request all COM ports that are reserved (ports 1-256).
            /// </summary>
            /// <param name="RegistryLocationToRead">OPTIONAL:  Specifies he registry location where the com reservations are stored (defaulted to Win7+ location).</param>
            /// <param name="RegistryEntryToRead">OPTIONAL:  Specifies he registry entry that holds the bit masking for the port reservations (defaulted to Win7+ location).</param>
            /// <returns>Returns a dictionary showing which com ports are reserved.</returns>
            public static Dictionary<UInt16, bool> GetReservedSerialPorts(string RegistryLocationToRead = @"HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\COM Name Arbiter", string RegistryEntryToRead = "ComDB")
            {
                try
                {
                    // Return nothing if inputs are bad
                    if (string.IsNullOrEmpty(RegistryLocationToRead) == true || string.IsNullOrEmpty(RegistryEntryToRead) == true)
                    {
                        return null;
                    }

                    // If we have a location, attempt to read the registry
                    object ReturnedRegistryVal = WinOS.Registry.GetValue(RegistryLocationToRead, RegistryEntryToRead);
                    if (ReturnedRegistryVal == null)
                    {
                        // Return nothing if read fails
                        return null;
                    }

                    // Return nothing if data isn't a byte array
                    if (ReturnedRegistryVal is byte[] == false) return null;

                    // If we have a byte array, get it as a list so it's easier to work with
                    var ByteList = ((byte[])ReturnedRegistryVal).ToList();

                    // Create list to hold all binary values for each byte
                    var BinaryValues = new List<string>();

                    // Loop through and convert
                    foreach (var BITE in ByteList)
                    {
                        // Get the byte as binary
                        string ByteAsBinaryInRegOrder = StaticMethods.Binary.Convert.Bits.ToBinaryString(BITE);

                        // Convert to char array b/c com port byte flags are backward (i.e. the MSB is the highest com port)
                        var BinaryAsCharList = ByteAsBinaryInRegOrder.ToCharArray().ToList();

                        // Reverse the order so the first bit is the lowest com port number
                        BinaryAsCharList.Reverse();

                        // Join back together in the order that we want
                        string ByteAsBinaryInSmallestToLargestOrder = string.Join(string.Empty, BinaryAsCharList);

                        // Once sorted, add to our list
                        BinaryValues.Add(ByteAsBinaryInSmallestToLargestOrder);
                    }

                    // Once we've got all bits, merge into one big string
                    string OneBigBinaryString = string.Join(string.Empty, BinaryValues);

                    // Now we should have 256 com port bits (i.e. a flag for each port from 1 to 256 indicating if reserved)
                    if (OneBigBinaryString.Length < 256) return null;

                    // If we're good, create dictionary to return
                    var DictionaryToReturn = new Dictionary<UInt16, bool>();

                    // Loop through and build dictionary
                    for (int i = 0; i <= 255; i++)
                    {
                        // Capture the flag at this position
                        string BitAtThisPosition = OneBigBinaryString.Substring(i, 1);

                        // Create flag to hold if reserved
                        bool IsReserved = false;
                        if (BitAtThisPosition == "1")
                        {
                            // If it is reserved, flip flag
                            IsReserved = true;
                        }

                        // Save back to dictionary
                        DictionaryToReturn.Add(Convert.ToUInt16(i + 1), IsReserved);
                    }

                    // Return the dictionary
                    return DictionaryToReturn;
                }
                catch (Exception ex)
                {
                    // Return nothing if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }

            /// <summary>
            /// Used to set which com port locations are reserved (REQUIRES ADMIN RIGHTS!).
            /// </summary>
            /// <param name="PortToIsReservedMapping">Dictionary to specify each port number and if it is reserved.</param>
            /// <param name="RegistryLocationToUpdate">OPTIONAL:  Specifies he registry location where the com reservations are stored (defaulted to Win7+ location).</param>
            /// <param name="RegistryEntryToUpdate">OPTIONAL:  Specifies he registry entry that holds the bit masking for the port reservations (defaulted to Win7+ location).</param>
            /// <returns>Returns TRUE if the reservations are set successfully. Returns FALSE in all other conditions.</returns>
            public static bool SetReservedSerialPorts(Dictionary<UInt16, bool> PortToIsReservedMapping, string RegistryLocationToUpdate = @"HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\COM Name Arbiter", string RegistryEntryToUpdate = "ComDB")
            {
                try
                {
                    // Return false if registry inputs are bad
                    if (string.IsNullOrEmpty(RegistryLocationToUpdate) == true || string.IsNullOrEmpty(RegistryEntryToUpdate) == true)
                    {
                        return false;
                    }

                    // Return false if dictionary is missing
                    if (PortToIsReservedMapping == null) return false;

                    // Return false if we don't have all entries
                    if (PortToIsReservedMapping.Count != 256) return false;

                    // Loop through and make sure ports are all accounted for
                    for (UInt16 i = 1; i <= 256; i++)
                    {
                        if (PortToIsReservedMapping.ContainsKey(i) == false)
                        {
                            // Return false if any COM 1 through 256 is missing
                            return false;
                        }
                    }

                    // If we're good, create list to hold all bit mapping as strings (to make big binary string)
                    var StringBitMappingFromSmallestToLargestComPort = new List<string>();

                    // Loop through and set all bit mapping
                    for (UInt16 i = 1; i <= 256; i++)
                    {
                        // Get the value at each position
                        bool IsPortEnabled = PortToIsReservedMapping[i];

                        // Save string for bit mapping
                        if (IsPortEnabled == true)
                        {
                            StringBitMappingFromSmallestToLargestComPort.Add("1");
                        }
                        else
                        {
                            StringBitMappingFromSmallestToLargestComPort.Add("0");
                        }
                    }

                    // Once we've got everything, group into 8 bit strings for each byte
                    var ListOfEightBitBinaryStringsForRegistry = new List<string>();

                    // Loop through and make groups
                    while (StringBitMappingFromSmallestToLargestComPort.Count > 0)
                    {
                        // Get the next 8 bit strings
                        var NextEightBitStrings = StringBitMappingFromSmallestToLargestComPort.GetRange(0, 8);

                        // Remove them from the source as we count down
                        StringBitMappingFromSmallestToLargestComPort.RemoveRange(0, 8);

                        // Reverse the bit order because that's what the registry expects (i.e. largest to smallest now)
                        NextEightBitStrings.Reverse();

                        // Join and add group
                        ListOfEightBitBinaryStringsForRegistry.Add(string.Join(string.Empty, NextEightBitStrings));
                    }

                    // Once we have everything, create list of bytes to hold the final entry for the registry
                    var ListOfBytesForRegistry = new List<byte>();

                    // Go through and convert
                    foreach (var BINARYSTRING in ListOfEightBitBinaryStringsForRegistry)
                    {
                        // Convert each one to a byte and add
                        ListOfBytesForRegistry.Add(StaticMethods.Binary.Convert.Bits.ToByte(BINARYSTRING).Value);
                    }

                    // Do one final check since we're about to edit the registry to make sure we have the right count (32 bytes = 256 bits)
                    if (ListOfBytesForRegistry.Count != 32) return false;

                    // If we're good, set that value
                    return Registry.SetValue(RegistryLocationToUpdate, RegistryEntryToUpdate, ListOfBytesForRegistry.ToArray(), ValueType: Microsoft.Win32.RegistryValueKind.Binary);
                }
                catch (Exception ex)
                {
                    // Return false if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }

            /// <summary>
            /// Used to clear all serial port reservations for all com ports that are NOT currently connected (REQUIRES ADMIN RIGHTS!).
            /// </summary>
            /// <returns>Returns TRUE if the reservation table is updated successfully. Returns FALSE in all other conditions.</returns>
            public static bool RemoveDisconnectedSerialReservations()
            {
                try
                {
                    // First, get a list of all connected serial ports
                    var AllSerialPortsAndFriendlyNames = GetSerialPortsWithFriendlyNames();
                    if (AllSerialPortsAndFriendlyNames == null)
                    {
                        // Return false if we can't
                        return false;
                    }

                    // If we get connected devices back, save just the port numbers
                    var ConnectedPortNumbers = AllSerialPortsAndFriendlyNames.Keys.ToList();

                    // Create a dictionary to hold all port reservation settings
                    var AllNewReservationSettings = new Dictionary<UInt16, bool>();

                    // Fill with all possible serial port numbers
                    for (UInt16 i = 1; i <= 256; i++)
                    {
                        // Fill dictionary with everything defaulted to not reserved
                        AllNewReservationSettings.Add(i, false);
                    }

                    // Finally, update the dictionary reservations for all ports that are currently connected
                    foreach (var PORT in ConnectedPortNumbers)
                    {
                        AllNewReservationSettings[PORT] = true;
                    }

                    // Lastly, request the update and return the result
                    return SetReservedSerialPorts(AllNewReservationSettings);
                }
                catch (Exception ex)
                {
                    // Return false if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }

            /// <summary>
            /// Used to attempt to open Device Manager.
            /// </summary>
            /// <returns>Return TRUE if the Device Manager process is started successfully. Return FALSE in all other conditions.</returns>
            public static bool OpenDeviceManager()
            {
                try
                {
                    // Try to open device manager
                    System.Diagnostics.Process.Start("devmgmt.msc");

                    // Return true if it opens
                    return true;
                }
                catch (Exception ex)
                {
                    // Return false if it won't open
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }

            /// <summary>
            /// Used to return the names all PC hardware recognized by Device Manager.
            /// </summary>
            /// <param name="IgnoreDuplicates">Boolean to indicate whether duplicate entries should be ignored.</param>
            /// <returns>Returns a list of all connected hardware if the search works. Returns NULL if the search fails.</returns>
            public static List<string> GetDeviceManagerHardwareNames(bool IgnoreDuplicates)
            {
                // Create the list to return
                var ListToReturn = new List<string>();

                try
                {
                    // Create new management search object (requires reference to System.Management)
                    using (var MgmtSearchObj = new System.Management.ManagementObjectSearcher("SELECT * From Win32_PnPEntity"))
                    {
                        // Loop through each element in the search object and get its info
                        foreach (var MgmtObj in MgmtSearchObj.Get())
                        {
                            try
                            {
                                // Get the current device name
                                string DeviceName = System.Convert.ToString(MgmtObj.GetPropertyValue("Caption"));

                                // Always ignore empty objects
                                if (string.IsNullOrEmpty(DeviceName) == true) continue;

                                // Check if ignoring duplicates
                                if (IgnoreDuplicates == false)
                                {
                                    // If not ignoring duplicates, add it no matter what
                                    ListToReturn.Add(DeviceName);
                                }
                                else
                                {
                                    // If ignoring duplicates, only add it if it's unique
                                    if (ListToReturn.Contains(DeviceName) == false)
                                    {
                                        ListToReturn.Add(DeviceName);
                                    }
                                }
                            }
                            finally
                            {
                                // Dispose of the mgmt object when done
                                MgmtObj.Dispose();
                            }
                        }
                    }

                    // Return the list if everything worked
                    return ListToReturn;
                }
                catch (Exception ex)
                {
                    // If an error occurs, return nothing
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }

            /// <summary>
            /// Used to query and return info from Device Manager regarding the connected USB devices.
            /// </summary>
            /// <returns>Returns a list of all connected USB info if the search works. Returns NULL if the search fails.</returns>
            public static List<DeviceManagerInfo> GetDeviceManagerUsbInfo()
            {
                // Create the list to return
                var ListToReturn = new List<DeviceManagerInfo>();

                try
                {
                    // Create new management search object (requires reference to System.Management)
                    using (var MgmtSearchObj = new System.Management.ManagementObjectSearcher("SELECT * From Win32_USBHub"))
                    {
                        // Loop through each element in the search object and get its info
                        foreach (var MgmtObj in MgmtSearchObj.Get())
                        {
                            // Create new object to hold info
                            var UsbItem = new DeviceManagerInfo();

                            // Copy over the basic info
                            UsbItem.ClassName = MgmtObj.ClassPath.ClassName;
                            UsbItem.NamespacePath = MgmtObj.ClassPath.NamespacePath;

                            // Get the property names and values
                            var PropertyNamesAndValues = GetPropertyNamesAndValuesFromMgmtObject((System.Management.ManagementObject)MgmtObj);

                            // Save back if found
                            if (PropertyNamesAndValues != null)
                            {
                                foreach (var PAIR in PropertyNamesAndValues)
                                {
                                    UsbItem.PropertyNamesAndValues.Add(PAIR.Key, PAIR.Value);
                                }
                            }

                            // Add item to list
                            ListToReturn.Add(UsbItem);

                            // Dispose of the mgmt object when done
                            MgmtObj.Dispose();
                        }
                    }

                    // Return the list if everything worked
                    return ListToReturn;
                }
                catch (Exception ex)
                {
                    // If an error occurs, return nothing
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }

            /// <summary>
            /// Used to get the information in Device Manager for a specified device by it's name (Caption property).
            /// </summary>
            /// <param name="ItemToFetch">The name / caption of the device to fetch.</param>
            /// <returns>Return the information for the specified device if successful. Returns NOTHING in all other conditions.</returns>
            public static DeviceManagerInfo GetDeviceManagerItemInfo(string ItemToFetch)
            {
                try
                {
                    // Return nothing if we're missing inputs
                    if (string.IsNullOrEmpty(ItemToFetch) == true) return null;

                    try
                    {
                        // Create new management search object (requires reference to System.Management)
                        using (var MgmtSearchObj = new System.Management.ManagementObjectSearcher("SELECT * From Win32_PnPEntity"))
                        {
                            // Loop through each element in the search object and get its info
                            foreach (var MgmtObj in MgmtSearchObj.Get())
                            {
                                try
                                {
                                    // Get the current device name
                                    string DeviceName = System.Convert.ToString(MgmtObj.GetPropertyValue("Caption"));

                                    // Skip if this isn't the device we're looking for
                                    if (DeviceName.ToUpper() != ItemToFetch.ToUpper()) continue;

                                    // If it is the item we want, create var to hold info to return
                                    var ItemInfo = new DeviceManagerInfo();

                                    // Xfer basic info
                                    ItemInfo.ClassName = MgmtObj.ClassPath.ClassName;
                                    ItemInfo.NamespacePath = MgmtObj.ClassPath.NamespacePath;

                                    // Save json directly (only one option for this)
                                    ItemInfo.AsJson = MgmtObj.GetText(System.Management.TextFormat.Mof);

                                    // Get XML text (start with WmiDtd20 as it is the newer format that supports embedded objects)
                                    string XmlText = MgmtObj.GetText(System.Management.TextFormat.WmiDtd20);
                                    if (string.IsNullOrEmpty(XmlText) == true)
                                    {
                                        // If we couldn't get that one, try the more basic xml
                                        XmlText = MgmtObj.GetText(System.Management.TextFormat.CimDtd20);
                                    }

                                    try
                                    {
                                        // Attempt to apply pretty formatting to xml
                                        string PrettyXml = System.Xml.Linq.XDocument.Parse(XmlText).ToString();
                                        XmlText = PrettyXml;
                                    }
                                    catch (Exception ex)
                                    {
                                        // Log issues
                                        System.Diagnostics.Trace.WriteLine(ex.Message);
                                    }

                                    // Save xml
                                    ItemInfo.AsXml = XmlText;

                                    // Get the property names and values
                                    var PropertyNamesAndValues = GetPropertyNamesAndValuesFromMgmtObject((System.Management.ManagementObject)MgmtObj);

                                    // Save back if found
                                    if (PropertyNamesAndValues != null)
                                    {
                                        foreach (var PAIR in PropertyNamesAndValues)
                                        {
                                            ItemInfo.PropertyNamesAndValues.Add(PAIR.Key, PAIR.Value);
                                        }
                                    }

                                    // Return the item when populated
                                    return ItemInfo;
                                }
                                finally
                                {
                                    // Dispose of the mgmt object when done
                                    MgmtObj.Dispose();
                                }
                            }
                        }

                        // Return the NOTHING if we can't find the specified object
                        return null;
                    }
                    catch (Exception ex)
                    {
                        // If an error occurs, return nothing
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    // Return nothing if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }

            /// <summary>
            /// Helper method for extracting data from a System.Management.ManagementObject.
            /// </summary>
            private static Dictionary<string, string> GetPropertyNamesAndValuesFromMgmtObject(System.Management.ManagementObject MgmtObj)
            {
                // Return nothing if missing input
                if (MgmtObj == null) return null;

                // Return empty if there are no properties
                if (MgmtObj.Properties == null || MgmtObj.Properties.Count == 0)
                {
                    return new Dictionary<string, string>();
                }

                // If there's data, create dictionary to hold info to return
                var PropertyNamesAndValues = new Dictionary<string, string>();

                // If there's data, loop through and get all property names and values
                foreach (var PROP in MgmtObj.Properties)
                {
                    // Skip if null somehow
                    if (PROP == null) continue;

                    // If we have a property, save the name and value
                    string PropertyName = PROP.Name;
                    object PropertyValue = PROP.Value;

                    // Skip if our dictionary already has an entry for this item (should never happen)
                    if (PropertyNamesAndValues.ContainsKey(PropertyName) == true) continue;

                    // Check if this was an array
                    if (PROP.IsArray == true)
                    {
                        // If it is, create list to hold each item
                        var ArrayItems = new List<string>();

                        try
                        {
                            // Loop through and convert
                            foreach (var ITEM in (Array)PropertyValue)
                            {
                                if (ITEM != null)
                                {
                                    // Convert items that exist to string
                                    ArrayItems.Add(ITEM.ToString());
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            // Log errors
                            System.Diagnostics.Trace.WriteLine(ex.Message);
                        }

                        // Join info and save back / overwrite
                        PropertyValue = string.Join(" | ", ArrayItems);
                    }

                    // Ensure property value is never null
                    if (PropertyValue == null)
                    {
                        PropertyValue = string.Empty;
                    }

                    // Save back to dictionary
                    PropertyNamesAndValues.Add(PropertyName, PropertyValue.ToString());
                }

                // Return the dictionary when populated
                return PropertyNamesAndValues;
            }

            /// <summary>
            /// Holds methods for working with FTDI USB to Serial devices.
            /// </summary>
            public struct FTDI
            {

                /// <summary>
                /// Internal constant for holding the location where FTDI stores registry entries for previously connected devices.
                /// </summary>
                private const string DeviceHistoryRegistryLocation = @"HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Enum\FTDIBUS";

                /// <summary>
                /// Used to list all FTDI USB to Serial devices that have been connected to this machine.
                /// </summary>
                public static List<string> GetFullDeviceListFromRegistry()
                {
                    try
                    {
                        // Attempt to get key names
                        var AllDevices = Registry.GetAllSubKeyNames(DeviceHistoryRegistryLocation);

                        // If nothing is returned, use empty list
                        if (AllDevices == null)
                        {
                            AllDevices = new List<string>();
                        }

                        // Return the device list
                        return AllDevices;
                    }
                    catch (Exception ex)
                    {
                        // Return empty list if there aren't any
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return new List<string>();
                    }
                }
                /// <summary>
                /// Used to list all FTDI USB to Serial devices that have been connected to this machine as well as the port number (COMx) that has been assigned to them.
                /// </summary>
                public static Dictionary<string, UInt16> GetFullDeviceListWithPortNumberFromRegistry()
                {
                    try
                    {
                        // Begin by getting all device names (needed to query port info)
                        var AllDeviceNames = GetFullDeviceListFromRegistry();
                        if (AllDeviceNames == null)
                        {
                            // Return nothing if error occurs
                            return null;
                        }

                        // Create dictionary to return
                        var DictionaryToReturn = new Dictionary<string, UInt16>();

                        // Loop through and query all names for the corresponding port info
                        foreach (var DEVICE in AllDeviceNames)
                        {
                            // Create the full path to the device registry entry
                            string FullPathToKeyToRead = DeviceHistoryRegistryLocation + @"\" + DEVICE + @"\0000\Device Parameters";

                            // Read the port value
                            var ObjPortValue = Registry.GetValue(FullPathToKeyToRead, "PortName");

                            // Create var to hold the serial port number to use from the reading
                            UInt16 PortNumber = 0;

                            // Check if we got a value back
                            if (ObjPortValue != null)
                            {
                                // If so, save the string of it
                                string StringComPort = ObjPortValue.ToString().ToUpper();

                                // Extract the port if format looks legit
                                if (StringComPort.Contains("COM") == true)
                                {
                                    PortNumber = Convert.ToUInt16(StringComPort.Replace("COM", String.Empty));
                                }
                            }

                            // Now we either have the port or it's defaulted at 0, save info to dictionary
                            DictionaryToReturn.Add(DEVICE, PortNumber);
                        }

                        // Return info once built
                        return DictionaryToReturn;
                    }
                    catch (Exception ex)
                    {
                        // Return nothing if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return null;
                    }
                }

                /// <summary>
                /// Used to check if there are any FTDI devices currently connected to the PC.
                /// </summary>
                public static Nullable<bool> AreAnyDevicesCurrentlyConnected()
                {
                    // Get currently connected devices
                    var AllDevicesConnectedNow = GetDevicePortsCurrentlyConnected();
                    if (AllDevicesConnectedNow == null)
                    {
                        // Return error if we can't find them
                        return null;
                    }

                    // If we get something back, use count to determine if anything is connected
                    if (AllDevicesConnectedNow.Count > 0)
                    {
                        // Return true if anything is connected
                        return true;
                    }
                    else
                    {
                        // Return false if nothing is connected
                        return false;
                    }
                }
                /// <summary>
                /// Used to return all FTDI USB to Serial devices that are currently connected to the PC.
                /// </summary>
                public static List<UInt16> GetDevicePortsCurrentlyConnected()
                {
                    try
                    {
                        // Get the full device list with port numbers
                        var AllFtdiDevsWithPortNumbers = GetFullDeviceListWithPortNumberFromRegistry();
                        if (AllFtdiDevsWithPortNumbers == null)
                        {
                            // Return nothing if error occurs
                            return null;
                        }

                        // If we get it, get all ports that are currently connected
                        var AllPortsCurrentlyConnected = GetSerialPortNumbers();
                        if (AllPortsCurrentlyConnected == null)
                        {
                            // Return error if we can't get them
                            return null;
                        }

                        // Save all FTDI ports that are currently connected
                        return (from AllFtdiPorts in AllFtdiDevsWithPortNumbers.Values.ToList() where AllPortsCurrentlyConnected.Contains(AllFtdiPorts) orderby AllFtdiPorts select AllFtdiPorts).ToList();
                    }
                    catch (Exception ex)
                    {
                        // Return nothing if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return null;
                    }
                }
                /// <summary>
                /// Used to return all FTDI USB to Serial devices that are NOT currently connected to the PC.
                /// </summary>
                public static List<UInt16> GetDevicePortsNotCurrentlyConnected()
                {
                    try
                    {
                        // Get the full device list with port numbers
                        var AllFtdiDevsWithPortNumbers = GetFullDeviceListWithPortNumberFromRegistry();
                        if (AllFtdiDevsWithPortNumbers == null)
                        {
                            // Return nothing if error occurs
                            return null;
                        }

                        // If we get it, get all ports that are currently connected
                        var AllPortsCurrentlyConnected = GetSerialPortNumbers();
                        if (AllPortsCurrentlyConnected == null)
                        {
                            // Return error if we can't get them
                            return null;
                        }

                        // Save all FTDI ports that are NOT currently connected
                        return (from AllFtdiPorts in AllFtdiDevsWithPortNumbers.Values.ToList() where AllPortsCurrentlyConnected.Contains(AllFtdiPorts) == false select AllFtdiPorts).ToList();
                    }
                    catch (Exception ex)
                    {
                        // Return nothing if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return null;
                    }
                }

                /// <summary>
                /// Used to change the assigned port number of a specified FTDI USB to Serial device.
                /// </summary>
                /// <param name="CurrentPortNumber">The current port number of FTDI device.</param>
                /// <param name="NewPortNumber">The new port number of the FTDI device.</param>
                public static bool ChangePortNumber(UInt16 CurrentPortNumber, UInt16 NewPortNumber)
                {
                    try
                    {
                        // Get list of all FTDI ports
                        var AllFtdiPorts = GetFullDeviceListWithPortNumberFromRegistry();
                        if (AllFtdiPorts == null)
                        {
                            // Return false if we can't find devices
                            return false;
                        }

                        // Return false if the com port requested is NOT an FTDI device
                        if (AllFtdiPorts.Values.ToList().Contains(CurrentPortNumber) == false) return false;

                        // Return false if there's already another FTDI device assigned to the other COM port we're switching to
                        if (AllFtdiPorts.Values.ToList().Contains(NewPortNumber) == true) return false;

                        // Get a list of all other COM ports to see if anything else is on that port
                        var AllOtherConnectedComPorts = Devices.GetSerialPortNumbers();
                        if (AllOtherConnectedComPorts == null)
                        {
                            // Return false if we can't get that info
                            return false;
                        }

                        // Return false if there's another ID at that address
                        if (AllOtherConnectedComPorts.Contains(NewPortNumber) == true) return false;

                        // Get all current reservations to check if anything else has booked the new position
                        var AllReservations = Devices.GetReservedSerialPorts();
                        if (AllReservations == null)
                        {
                            // Report error if we can't get reservations
                            return false;
                        }

                        // Finally, check if the port we're going to is already reserved and, if so, return false
                        if (AllReservations[NewPortNumber] == true) return false;

                        // Save the FTDI device name for this COM port to build the path
                        string FtdiDeviceId = (from AllInfo in AllFtdiPorts where AllInfo.Value == CurrentPortNumber select AllInfo.Key).ToList()[0];

                        // Build the paths to the two reg keys (friendly name and port number) that need to be updated to make the change
                        string RegPathToFriendlyName = DeviceHistoryRegistryLocation + @"\" + FtdiDeviceId + @"\0000";
                        string RegPathToPortNumber = DeviceHistoryRegistryLocation + @"\" + FtdiDeviceId + @"\0000\Device Parameters";

                        // Create constant to hold the fields for friendly name and port name that need to be updated
                        const string FriendlyName = "FriendlyName";
                        const string PortNumber = "PortName";

                        // Read both locations to just be sure they exist
                        object ObjFriendlyName = Registry.GetValue(RegPathToFriendlyName, FriendlyName);
                        object ObjPortNumber = Registry.GetValue(RegPathToPortNumber, PortNumber);

                        // Fail if either can't be found
                        if (ObjFriendlyName == null || ObjPortNumber == null) return false;

                        // If we're good, try to update the friendly name, return false if it fails
                        if (Registry.SetValue(RegPathToFriendlyName, FriendlyName, "USB Serial Port (COM" + NewPortNumber.ToString() + ")", ValueType: Microsoft.Win32.RegistryValueKind.String) == false)
                        {
                            return false;
                        }

                        // If friendly name updates, try updating port number and return false on failure
                        if (Registry.SetValue(RegPathToPortNumber, PortNumber, "COM" + NewPortNumber.ToString(), ValueType: Microsoft.Win32.RegistryValueKind.String) == false)
                        {
                            return false;
                        }

                        // If both keys save, update the reservation list
                        AllReservations[CurrentPortNumber] = false;
                        AllReservations[NewPortNumber] = true;

                        // Save the list back to the registry and return the result
                        return Devices.SetReservedSerialPorts(AllReservations);
                    }
                    catch (Exception ex)
                    {
                        // Return false if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return false;
                    }
                }

            }

        }


        #endregion

        #region Windows Registry


        public struct Registry
        {

            /// <summary>
            /// Used to spin through the entire registry and log all keys and values that are readable / accessible at the current privilege level.
            /// </summary>
            /// <param name="FullFolderPathToLogFilesTo">The full folder path to log the output files for each root registry node to.</param>
            /// <param name="MethodToInvokeWithStatusMsgs">The callback method to report status update string messages to.</param>
            /// <param name="MethodToInvokeWithResult">The callback method to report the final results to (list of key/value pairs of strings).</param>
            public static void LogAllAccessibleEntriesAsync(string FullFolderPathToLogFilesTo, Action<string> MethodToInvokeWithStatusMsgs, Action<bool> MethodToInvokeWithResult)
            {
                // Create and launch thread to do work
                var thrScanRegistry = new System.Threading.Thread(() => LogAllAccessibleEntries(FullFolderPathToLogFilesTo, MethodToInvokeWithStatusMsgs, MethodToInvokeWithResult));
                thrScanRegistry.SetApartmentState(System.Threading.ApartmentState.STA);
                thrScanRegistry.IsBackground = true;
                thrScanRegistry.Start();
            }
            /// <summary>
            /// Used to spin through the entire registry and log all keys and values that are readable / accessible at the current privilege level.
            /// </summary>
            /// <param name="FullFolderPathToLogFilesTo">The full folder path to log the output files for each root registry node to.</param>
            /// <returns>Returns a list of all entry / key paths and their values if successful. Returns NOTHING in all other conditions.</returns>
            public static bool LogAllAccessibleEntries(string FullFolderPathToLogFilesTo)
            {
                // Use helper and return result
                return LogAllAccessibleEntries(FullFolderPathToLogFilesTo, null, null);
            }
            private static bool LogAllAccessibleEntries(string FullFolderPathToLogFilesTo, Action<string> MethodToInvokeWithStatusMsgs, Action<bool> MethodToInvokeWithResult)
            {
                try
                {
                    // Ensure the folder path exists
                    if (System.IO.Directory.Exists(FullFolderPathToLogFilesTo) == false)
                    {
                        System.IO.Directory.CreateDirectory(FullFolderPathToLogFilesTo);
                    }

                    // Create list dictionary to hold all root regkeys and string name folder paths for looping through
                    var RootKeys = new Dictionary<Microsoft.Win32.RegistryKey, string>();

                    // Add all root keys (default, x86, & x64)
                    RootKeys.Add(Microsoft.Win32.RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.ClassesRoot, Microsoft.Win32.RegistryView.Default), "ClassesRoot-Default");
                    RootKeys.Add(Microsoft.Win32.RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.ClassesRoot, Microsoft.Win32.RegistryView.Registry32), "ClassesRoot-32");
                    RootKeys.Add(Microsoft.Win32.RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.ClassesRoot, Microsoft.Win32.RegistryView.Registry64), "ClassesRoot-64");
                    RootKeys.Add(Microsoft.Win32.RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.CurrentConfig, Microsoft.Win32.RegistryView.Default), "CurrentConfig-Default");
                    RootKeys.Add(Microsoft.Win32.RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.CurrentConfig, Microsoft.Win32.RegistryView.Registry32), "CurrentConfig-32");
                    RootKeys.Add(Microsoft.Win32.RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.CurrentConfig, Microsoft.Win32.RegistryView.Registry64), "CurrentConfig-64");
                    RootKeys.Add(Microsoft.Win32.RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.CurrentUser, Microsoft.Win32.RegistryView.Default), "CurrentUser-Default");
                    RootKeys.Add(Microsoft.Win32.RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.CurrentUser, Microsoft.Win32.RegistryView.Registry32), "CurrentUser-32");
                    RootKeys.Add(Microsoft.Win32.RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.CurrentUser, Microsoft.Win32.RegistryView.Registry64), "CurrentUser-64");
                    RootKeys.Add(Microsoft.Win32.RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, Microsoft.Win32.RegistryView.Default), "LocalMachine-Default");
                    RootKeys.Add(Microsoft.Win32.RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, Microsoft.Win32.RegistryView.Registry32), "LocalMachine-32");
                    RootKeys.Add(Microsoft.Win32.RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, Microsoft.Win32.RegistryView.Registry64), "LocalMachine-64");
                    RootKeys.Add(Microsoft.Win32.RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.PerformanceData, Microsoft.Win32.RegistryView.Default), "PerformanceData-Default");
                    RootKeys.Add(Microsoft.Win32.RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.PerformanceData, Microsoft.Win32.RegistryView.Registry32), "PerformanceData-32");
                    RootKeys.Add(Microsoft.Win32.RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.PerformanceData, Microsoft.Win32.RegistryView.Registry64), "PerformanceData-64");
                    RootKeys.Add(Microsoft.Win32.RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.Users, Microsoft.Win32.RegistryView.Default), "Users-Default");
                    RootKeys.Add(Microsoft.Win32.RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.Users, Microsoft.Win32.RegistryView.Registry32), "Users-32");
                    RootKeys.Add(Microsoft.Win32.RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.Users, Microsoft.Win32.RegistryView.Registry64), "Users-64");

                    // DynData node doesn't seem to work / be supported, ignoring for now
                    // RootKeys.Add(Microsoft.Win32.RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.DynData, Microsoft.Win32.RegistryView.Default), "DynData-Default")
                    // RootKeys.Add(Microsoft.Win32.RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.DynData, Microsoft.Win32.RegistryView.Registry32), "DynData-32")
                    // RootKeys.Add(Microsoft.Win32.RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.DynData, Microsoft.Win32.RegistryView.Registry64), "DynData-64")

                    // Loop through all reg keys and get values
                    foreach (var PAIR in RootKeys)
                    {
                        // Once all entries are defined, create var to hold file name for the current export
                        string FileNameToExportTo = PAIR.Value + ".log";

                        // Save the full file path to the file
                        string FullFilePathToExportTo = FullFolderPathToLogFilesTo + @"\" + FileNameToExportTo;

                        // Save string for status msg
                        string StatusMsg = "Scanning registry node " + (RootKeys.ToList().IndexOf(PAIR) + 1).ToString() + " of " + RootKeys.Count.ToString() + "  -->  [ " + PAIR.Value + " ]...";

                        // Report status if there's a callback
                        if (MethodToInvokeWithStatusMsgs != null)
                        {
                            System.Windows.Application.Current.Dispatcher.Invoke(() => MethodToInvokeWithStatusMsgs(StatusMsg));
                        }

                        // Attempt to log results for the current reg key
                        GetKeysAndValues(PAIR.Key, PAIR.Value.Split(new[] { "-" }, StringSplitOptions.None)[0], FullFilePathToExportTo);
                    }

                    // Report status if there's a callback
                    if (MethodToInvokeWithStatusMsgs != null)
                    {
                        System.Windows.Application.Current.Dispatcher.Invoke(() => MethodToInvokeWithStatusMsgs("Registry scanning complete - computing hashes..."));
                    }

                    // Get all files in the directory
                    var FilesInDirectory = System.IO.Directory.GetFiles(FullFolderPathToLogFilesTo).ToList();

                    // Loop through and append hash to each file
                    foreach (var FILE in FilesInDirectory)
                    {
                        // Get the file hash
                        string FileHash = GetFileContentHash(FILE);

                        // Remove any illegal chars from file hash
                        if (string.IsNullOrEmpty(FileHash) == false)
                        {
                            FileHash = RemoveIllegalFilePathChars(FileHash);
                        }

                        // Get the components of the file to append hash
                        string FileFolder = System.IO.Path.GetDirectoryName(FILE);
                        string FileNoExt = System.IO.Path.GetFileNameWithoutExtension(FILE);
                        string FileExt = System.IO.Path.GetExtension(FILE);

                        // Rename the file to show hash
                        Microsoft.VisualBasic.FileIO.FileSystem.RenameFile(FILE, FileNoExt + " - [ " + FileHash + " ]" + FileExt);
                    }

                    // Return true if everything works
                    if (MethodToInvokeWithResult != null) System.Windows.Application.Current.Dispatcher.Invoke(() => MethodToInvokeWithResult(true));
                    return true;
                }
                catch (Exception ex)
                {
                    // Return false if error occurs
                    System.Diagnostics.Trace.Write(ex.Message);
                    if (MethodToInvokeWithResult != null) System.Windows.Application.Current.Dispatcher.Invoke(() => MethodToInvokeWithResult(false));
                    return false;
                }
            }
            private static void GetKeysAndValues(Microsoft.Win32.RegistryKey RegKey, string OutterFolderPathNameOfRegEntry, string FullFilePathToLogDataTo)
            {
                // Create flag to hold if one or more values was logged for this regkey
                bool AtLeastOneValueInKeyRecorded = false;

                // Check if the current key has values
                if (RegKey.ValueCount > 0)
                {
                    // If so, get all the sub-key names with values
                    var KeyNamesWithValues = RegKey.GetValueNames().ToList();

                    // Create and size array to hold all keys and values
                    var KeyValuePairs = new KeyValuePair<string, string>[RegKey.ValueCount - 1 + 1];

                    // Loop through each key and add the name and value
                    for (int i = 0; i <= KeyNamesWithValues.Count - 1; i++)
                    {
                        // Save the key name
                        string KEYNAME = KeyNamesWithValues[i];

                        // Create var to hold the value
                        string KeyValue = string.Empty;

                        // Get the object value of the key
                        object ObjValue = RegKey.GetValue(KEYNAME);

                        // If there is a value, save it
                        if (ObjValue != null) KeyValue = ObjValue.ToString();

                        try
                        {
                            // Check if value was some type of array that needs to be converted to a string
                            if (KeyValue.ToUpper().Contains("BYTE[]") == true)
                            {
                                // If it's a byte array, cast it
                                var ByteData = (byte[])ObjValue;

                                // Convert to byte strings
                                string[] ByteStrings = new string[ByteData.Length - 1 + 1];
                                for (int j = 0; j <= ByteData.Length - 1; j++)
                                {
                                    ByteStrings[j] = ByteData[j].ToString("X").PadLeft(2, Convert.ToChar("0"));
                                }

                                // If conversion works, create string to hold bytes
                                string StringBytes = string.Join(" ", ByteStrings);

                                // Overwrite the keyval
                                KeyValue = StringBytes;
                            }
                            else if (KeyValue.ToUpper().Contains("STRING[]") == true)
                            {
                                // If it's a string array, cast it
                                var StringArray = (string[])ObjValue;

                                // If conversion works, create string to hold merged items
                                string ConcatedStrings = string.Join(" ", StringArray);

                                // Overwrite the keyval
                                KeyValue = ConcatedStrings;
                            }
                        }
                        catch (Exception ex)
                        {
                            // Log errors
                            System.Diagnostics.Trace.WriteLine(ex.Message);
                        }

                        // Add data to the array
                        KeyValuePairs[i] = new KeyValuePair<string, string>(OutterFolderPathNameOfRegEntry + @"\" + KEYNAME, KeyValue);
                    }

                    // Set flag if at least one value is recorded
                    AtLeastOneValueInKeyRecorded = true;

                    // Log any value data obtained
                    LogDataToFile(FullFilePathToLogDataTo, KeyValuePairs);
                }

                // After checking / saving values, get all subkeys within the current key
                var SubKeyNames = RegKey.GetSubKeyNames().ToList();

                // Check if there are sub-keys
                if (SubKeyNames.Count == 0)
                {
                    // If there no sub-keys (i.e. the folder is empty), check if at least one value was added to the list
                    if (AtLeastOneValueInKeyRecorded == false)
                    {
                        // If no values were added, log a place holder so empty folders show up in output
                        LogDataToFile(FullFilePathToLogDataTo, new[] { new KeyValuePair<string, string>(OutterFolderPathNameOfRegEntry, "[No SubKeys / Values]") });
                    }
                }
                else
                {
                    // If there are, loop through any subkeys recursively and process them
                    foreach (var SUBKEY in SubKeyNames)
                    {
                        try
                        {
                            // Open the subkey
                            using (Microsoft.Win32.RegistryKey SubRegKey = RegKey.OpenSubKey(SUBKEY))
                            {
                                // Use recursion to log all values within it
                                GetKeysAndValues(SubRegKey, OutterFolderPathNameOfRegEntry + @"\" + SUBKEY, FullFilePathToLogDataTo);
                            }
                        }
                        catch (Exception ex)
                        {
                            // Log errors
                            System.Diagnostics.Trace.WriteLine(ex.Message);
                        }
                    }
                }

                // Dispose of the key when done to prevent memory leaks
                RegKey.Dispose();
            }
            private static bool LogDataToFile(string FullFilePath, IEnumerable<KeyValuePair<string, string>> DataToLog)
            {
                try
                {
                    // Once results are returned, attempt to log to file
                    using (var OutFile = new System.IO.StreamWriter(FullFilePath, true))
                    {
                        foreach (var RESULT in DataToLog)
                        {
                            // Iterate through and write each line (tab delimited)
                            OutFile.WriteLine(RESULT.Key + Convert.ToString(Convert.ToChar(9)) + RESULT.Value);
                        }
                    }

                    // Return true if write works
                    return true;
                }
                catch (Exception ex)
                {
                    // Return false if error 
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }
            private static string GetFileContentHash(string FilePath)
            {
                try
                {
                    // Get the file bytes
                    var FileContentBytes = System.IO.File.ReadAllBytes(FilePath).ToList();

                    // Return empty string if there are no file bytes yet
                    if (FileContentBytes == null || FileContentBytes.Count == 0)
                    {
                        return string.Empty;
                    }

                    // If there are bytes, convert them to ASCII
                    string ASCII = System.Text.Encoding.ASCII.GetString(FileContentBytes.ToArray());

                    // If there is a string, get the unicode bytes
                    var UnicodeBytes = System.Text.Encoding.Unicode.GetBytes(ASCII);

                    // Get the hash bytes
                    var HashedBytes = System.Security.Cryptography.HashAlgorithm.Create("SHA1").ComputeHash(UnicodeBytes);

                    // Convert to base64 and return
                    return Convert.ToBase64String(HashedBytes);
                }
                catch (Exception ex)
                {
                    // Return an empty string if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }
            private static string RemoveIllegalFilePathChars(string StringToClean)
            {
                try
                {
                    // Create the string to return
                    string StringToReturn = StringToClean.Trim();

                    // Return empty string if nothing is passed in
                    if (string.IsNullOrEmpty(StringToReturn) == true) return StringToReturn;

                    // Save all illegal chars to replace in array
                    string[] IllegalChars = new[] { @"\", "/", "|", ":", "?", "<", ">", "\"" };

                    // Remove chars not allowed in a file name
                    foreach (string ILLEGALCHAR in IllegalChars)
                    {
                        StringToReturn = StringToReturn.Replace(ILLEGALCHAR, string.Empty);
                    }

                    // Return the string
                    return StringToReturn;
                }
                catch (Exception ex)
                {
                    // Return an empty string if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

            /// <summary>
            /// Lists all sub-keys directly under a specified parent key.
            /// </summary>
            /// <param name="ParentKeyPath">The full path to the parent registry key to check.</param>
            /// <returns>Returns a list of all child sub-keys if successful. Returns NOTHING in all other conditions.</returns>
            public static List<string> GetAllSubKeyNames(string ParentKeyPath)
            {
                try
                {
                    // Return nothing if parent location is bad
                    if (string.IsNullOrEmpty(ParentKeyPath) == true) return null;

                    // Create var to hold which registry we're using
                    Microsoft.Win32.RegistryKey RootRegistryLocation = null;

                    // Key off the path to figure out the root registry location
                    if (ParentKeyPath.ToUpper().Contains("LOCAL_MACHINE") == true)
                    {
                        RootRegistryLocation = Microsoft.Win32.Registry.LocalMachine;
                    }
                    else if (ParentKeyPath.ToUpper().Contains("CURRENT_USER") == true)
                    {
                        RootRegistryLocation = Microsoft.Win32.Registry.CurrentUser;
                    }
                    else if (ParentKeyPath.ToUpper().Contains("CLASSES_ROOT") == true)
                    {
                        RootRegistryLocation = Microsoft.Win32.Registry.ClassesRoot;
                    }

                    // We need to peel off the root node now for the remaining path to check, split by slash
                    var AllLocationChunks = ParentKeyPath.Split(new[] { @"\" }, StringSplitOptions.None).ToList();

                    // Remove the highest level
                    AllLocationChunks.RemoveAt(0);

                    // Join back together without root
                    ParentKeyPath = string.Join(@"\", AllLocationChunks);

                    // Return nothing if we can't tell where we're looking
                    if (RootRegistryLocation == null) return null;

                    // If we're good, create list to return
                    var ListToReturn = new List<string>();

                    // Save all sub keys
                    using (var ParentKey = RootRegistryLocation.OpenSubKey(ParentKeyPath, false))
                    {
                        ListToReturn = ParentKey.GetSubKeyNames().ToList();
                    }

                    // Dispose of parent key
                    RootRegistryLocation.Dispose();

                    // Return the entries
                    return ListToReturn;
                }
                catch (Exception ex)
                {
                    // Return nothing if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }

            /// <summary>
            /// Used to write a specified value to the registry.
            /// </summary>
            /// <param name="FullRegistryPathToKey">The full path to the registry key to write to.</param>
            /// <param name="ValueName">The name of the value to write to the key.</param>
            /// <param name="Value">The value to write to the key.</param>
            /// <param name="ValueType">OPTIONAL:  The type of the value that is being written.</param>
            /// <returns>Returns TRUE if the value is written and successfully read back. Returns FALSE in all other conditions.</returns>
            public static bool SetValue(string FullRegistryPathToKey, string ValueName, object Value, Microsoft.Win32.RegistryValueKind ValueType = Microsoft.Win32.RegistryValueKind.None)
            {
                try
                {
                    // Attempt to write to registry
                    Microsoft.Win32.Registry.SetValue(FullRegistryPathToKey, ValueName, Value, ValueType);

                    // Read the value back
                    object ReturnedValue = GetValue(FullRegistryPathToKey, ValueName);

                    // Return false if read fails
                    if (ReturnedValue == null) return false;

                    // If anything comes back, return true if values match
                    if (Value.ToString() == ReturnedValue.ToString()) return true;

                    // Return false if something else comes back
                    return false;
                }
                catch (Exception ex)
                {
                    // Return false if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }

            /// <summary>
            /// Used to read a specified value from the registry.
            /// </summary>
            /// <param name="FullRegistryPathToKey">The full path to the registry key to read.</param>
            /// <param name="ValueName">The name of the value read back.</param>
            /// <returns>Returns the value as an object if read back successfully (will be an empty string if no value has been set). Returns NOTHING if an error occurs reading the entry.</returns>
            public static object GetValue(string FullRegistryPathToKey, string ValueName)
            {
                try
                {
                    // Attempt to read the value back
                    object ReadValue = Microsoft.Win32.Registry.GetValue(FullRegistryPathToKey, ValueName, string.Empty);

                    // Return nothing if read returns nothing (should never happen)
                    if (ReadValue == null) return null;

                    // If something came back, return it
                    return ReadValue;
                }
                catch (Exception ex)
                {
                    // Return nothing if read fails
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }

            /// <summary>
            /// Used to delete a registry key / sub-key and all children elements in it.
            /// </summary>
            /// <param name="FullRegistryPathToKeyToDelete">The full registry path to the key to delete.</param>
            /// <returns>Returns TRUE if the key is deleted successfully. Returns FALSE in all other conditions.</returns>
            public static bool DeleteKey(string FullRegistryPathToKeyToDelete)
            {
                // Create var to hold the key
                Microsoft.Win32.RegistryKey SubKey = null;

                try
                {
                    // Create string to hold the path without the root node
                    string PathToKeyToDeleteWithoutRootNode = GetRegistryKeyPathWithoutHivePrefix(FullRegistryPathToKeyToDelete);
                    if (string.IsNullOrEmpty(PathToKeyToDeleteWithoutRootNode) == true)
                    {
                        // Return false if path is bad
                        return false;
                    }

                    // Capture the parent of that key (we actually have to go up a level to delete the key)
                    string ParentKeyOfKeyToDelete = System.IO.Path.GetDirectoryName(PathToKeyToDeleteWithoutRootNode);

                    // Save the actual name of the key to delete
                    string NameOfKeyToDelete = System.IO.Path.GetFileNameWithoutExtension(PathToKeyToDeleteWithoutRootNode);

                    // Get handle to the key for the specified location & hive
                    using (var RootKey = GetRegistryRootKeyHiveFromStringPath(FullRegistryPathToKeyToDelete))
                    {
                        // Return false if we can't get it
                        if (RootKey == null) return false;

                        // If we have the root key, open the sub key
                        SubKey = RootKey.OpenSubKey(ParentKeyOfKeyToDelete, true);
                        if (SubKey == null)
                        {
                            // Return false if we can't get it
                            return false;
                        }

                        // If we have the key, delete its children
                        SubKey.DeleteSubKeyTree(NameOfKeyToDelete, true);
                    }

                    // Return true if it deletes
                    return true;
                }
                catch (Exception ex)
                {
                    // Return false if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
                finally
                {
                    // Clean up if key was set
                    if (SubKey != null)
                    {
                        SubKey.Dispose();
                    }
                }
            }

            /// <summary>
            /// Used to delete a value from a specified registry key.
            /// </summary>
            /// <param name="FullRegistryPathToKey">The full path to the key with the value you want to delete.</param>
            /// <param name="ValueName">The value you want to delete from the key.</param>
            /// <returns>Returns TRUE if the value is deleted successfully. Returns FALSE in all other conditions.</returns>
            public static bool DeleteValue(string FullRegistryPathToKey, string ValueName)
            {
                // Create var to hold the key
                Microsoft.Win32.RegistryKey SubKey = null;

                try
                {
                    // Create string to hold the path without the root node
                    string PathToKeyWithoutRootNode = GetRegistryKeyPathWithoutHivePrefix(FullRegistryPathToKey);
                    if (string.IsNullOrEmpty(PathToKeyWithoutRootNode) == true)
                    {
                        // Return false if path is bad
                        return false;
                    }

                    // Get handle to the key for the specified location & hive
                    using (var RootKey = GetRegistryRootKeyHiveFromStringPath(FullRegistryPathToKey))
                    {
                        // Return false if we can't get it
                        if (RootKey == null) return false;

                        // If we have the root key, open the sub key
                        SubKey = RootKey.OpenSubKey(PathToKeyWithoutRootNode, true);
                        if (SubKey == null)
                        {
                            // Return false if we can't get it
                            return false;
                        }

                        // If we have the key, delete the requested value
                        SubKey.DeleteValue(ValueName, true);
                    }

                    // Return true if it deletes
                    return true;
                }
                catch (Exception ex)
                {
                    // Return false if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
                finally
                {
                    // Clean up if key was set
                    if (SubKey != null)
                    {
                        SubKey.Dispose();
                    }
                }
            }

            /// <summary>
            /// Helper to get handle to the registry root key hive from a full string path to a registry key.
            /// </summary>
            private static Microsoft.Win32.RegistryKey GetRegistryRootKeyHiveFromStringPath(string FullRegistryPathToKey)
            {
                try
                {
                    // Return nothing if missing input
                    if (string.IsNullOrEmpty(FullRegistryPathToKey) == true)
                    {
                        return null;
                    }

                    // Determine based on full reg path
                    if (FullRegistryPathToKey.ToUpper().Contains("HKEY_CLASSES_ROOT") == true)
                    {
                        return Microsoft.Win32.Registry.ClassesRoot;
                    }
                    else if (FullRegistryPathToKey.ToUpper().Contains("HKEY_CURRENT_USER") == true)
                    {
                        return Microsoft.Win32.Registry.CurrentUser;
                    }
                    else if (FullRegistryPathToKey.ToUpper().Contains("HKEY_LOCAL_MACHINE") == true)
                    {
                        return Microsoft.Win32.Registry.LocalMachine;
                    }
                    else if (FullRegistryPathToKey.ToUpper().Contains("HKEY_USERS") == true)
                    {
                        return Microsoft.Win32.Registry.Users;
                    }
                    else if (FullRegistryPathToKey.ToUpper().Contains("HKEY_CURRENT_CONFIG") == true)
                    {
                        return Microsoft.Win32.Registry.CurrentConfig;
                    }

                    // Return nothing if we can't match anything
                    return null;
                }
                catch (Exception ex)
                {
                    // Return nothing if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }

            /// <summary>
            /// Helper to remove the root hive from a full registry path and just return the rest of the path.
            /// </summary>
            private static string GetRegistryKeyPathWithoutHivePrefix(string FullRegistryPathToKey)
            {
                try
                {
                    // Return empty string if input is bad
                    if (string.IsNullOrEmpty(FullRegistryPathToKey) == true || FullRegistryPathToKey.Contains(@"\") == false)
                    {
                        return string.Empty;
                    }

                    // If we're good, we need to peel off the root node now for the remaining path to check, split by slash
                    var AllLocationChunks = FullRegistryPathToKey.Split(new[] { @"\" }, StringSplitOptions.None).ToList();

                    // Remove the highest level (i.e. the root hive)
                    return string.Join(@"\", AllLocationChunks);
                }
                catch (Exception ex)
                {
                    // Return empty if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

            /// <summary>
            /// Structure to hold methods for setting and checking if file extensions are associated with a particular program.
            /// </summary>
            public struct FileAssociation
            {

                /// <summary>
                /// Import kernel method for creating short path uint.
                /// </summary>
                [System.Runtime.InteropServices.DllImport("Kernel32.dll")]
                private static extern uint GetShortPathName(string lpszLongPath, [System.Runtime.InteropServices.Out()] System.Text.StringBuilder lpszShortPath, uint cchBuffer);
                /// <summary>
                /// Return short path format of a file name.
                /// </summary>
                private static string FullFilePathToShortPathName(string FullFilePath)
                {
                    // Create new string building to create the short path name (path will never be bigger than a few hundred chars)
                    var StringBuilder = new System.Text.StringBuilder(1000);

                    // Get the short path name
                    GetShortPathName(FullFilePath, StringBuilder, Convert.ToUInt32(StringBuilder.Capacity));

                    // Return the string
                    return StringBuilder.ToString();
                }

                /// <summary>
                /// Used to associate a given file extension with a particular application.
                /// </summary>
                /// <param name="FileExtension">The file extension to add an association for (.txt or txt, etc.).</param>
                /// <param name="FullPathOfAppToAssociateExtensionWith">The full file path for the application that should be used to open files with the specified extension.</param>
                /// <param name="OverrideProgramName">OPTIONAL:  Used a specify a ProgramID that is different than application's file name (i.e. 'Word' instead of 'WinWord' from 'WinWord.exe', etc.).</param>
                /// <param name="AppDescription">OPTIONAL:  The description to store in the registry for the application that will be used to open the file extension.</param>
                /// <param name="AppIconPath">OPTIONAL:  The full file path of the icon to associate with the specified file extension.</param>
                /// <returns>Returns TRUE if the file extension is successfully associated with the specified application. Returns FALSE in all other conditions.</returns>
                public static bool Set(string FileExtension, string FullPathOfAppToAssociateExtensionWith, string OverrideProgramName = "", string AppDescription = "", string AppIconPath = "")
                {
                    try
                    {
                        // Return false if no extension is specified
                        if (string.IsNullOrEmpty(FileExtension) == true) return false;

                        // Return false if app file path doesn't exist
                        if (string.IsNullOrEmpty(FullPathOfAppToAssociateExtensionWith) == true || System.IO.File.Exists(FullPathOfAppToAssociateExtensionWith) == false)
                        {
                            return false;
                        }

                        // Clear description if invalid
                        if (string.IsNullOrEmpty(AppDescription) == true) AppDescription = string.Empty;

                        // Clear icon path if not valid
                        if (string.IsNullOrEmpty(AppIconPath) == true || System.IO.File.Exists(AppIconPath) == false) AppIconPath = string.Empty;

                        // If both are good, ensure that the extension starts with the "."
                        if (FileExtension.Substring(0, 1) != ".")
                        {
                            FileExtension = "." + FileExtension;
                        }

                        // Save the readable name for the file extension
                        string FileExtReadable = FileExtension.Substring(1).ToUpper();

                        // If a program name wasn't specified, get it from the path to the app
                        if (string.IsNullOrEmpty(OverrideProgramName) == true)
                        {
                            OverrideProgramName = System.IO.Path.GetFileNameWithoutExtension(FullPathOfAppToAssociateExtensionWith);
                        }

                        try
                        {
                            // Delete any existing default
                            Microsoft.Win32.Registry.CurrentUser.DeleteSubKeyTree(@"Software\Microsoft\Windows\CurrentVersion\Explorer\FileExts\" + FileExtension + @"\UserChoice", true);
                        }
                        catch (Exception ex)
                        {
                            // Just log errors
                            System.Diagnostics.Trace.WriteLine(ex.Message);
                        }

                        // If everything looks good, create the subkey for the program id
                        Microsoft.Win32.Registry.ClassesRoot.CreateSubKey(FileExtension).SetValue(string.Empty, OverrideProgramName);

                        // Add any option information for the association
                        using (var Key = Microsoft.Win32.Registry.ClassesRoot.CreateSubKey(OverrideProgramName))
                        {
                            // Add shell command to launch extension with program
                            if (string.IsNullOrEmpty(FullPathOfAppToAssociateExtensionWith) == false)
                            {
                                Key.CreateSubKey(@"Shell\Open\Command").SetValue(string.Empty, FullFilePathToShortPathName(FullPathOfAppToAssociateExtensionWith) + Convert.ToString(" \"%1\""));
                            }

                            // Add description if present
                            if (string.IsNullOrEmpty(AppDescription) == false)
                            {
                                Key.SetValue(string.Empty, AppDescription);
                            }

                            // Add icon if path is present
                            if (string.IsNullOrEmpty(AppIconPath) == false)
                            {
                                Key.CreateSubKey("DefaultIcon").SetValue(string.Empty, FullFilePathToShortPathName(AppIconPath));
                            }
                        }

                        // Return true if everything sets correctly
                        return true;
                    }
                    catch (Exception ex)
                    {
                        // Return false if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return false;
                    }
                }

                /// <summary>
                /// Checks if an extension is currently associated with an application.
                /// </summary>
                /// <param name="ExtensionToCheck">The file extension to check (.txt or txt, etc.)</param>
                /// <returns>Returns TRUE if the file extension is associated with an application. Returns FALSE if the file is not associated with an application or the association could not be checked.</returns>
                public static bool IsFileExtensionAssociated(string ExtensionToCheck)
                {
                    try
                    {
                        // Return false if file extension is missing
                        if (string.IsNullOrEmpty(ExtensionToCheck) == true) return false;

                        // If there is an extension, ensure that there's a dot
                        if (ExtensionToCheck.Substring(0, 1) != ".") ExtensionToCheck = "." + ExtensionToCheck;

                        // Check if file is associated already and return the result (will be null if NOT associated)
                        return (Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ExtensionToCheck, false) != null);
                    }
                    catch (Exception ex)
                    {
                        // Return false if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return false;
                    }
                }

            }

        }


        #endregion

        #region System Idle Information


        public struct IdleInformation
        {

            /// <summary>
            /// Declare function from user32.dll to get inactivity time.
            /// </summary>
            [System.Runtime.InteropServices.DllImport("user32.dll")]
            private static extern bool GetLastInputInfo(ref SystemIdleInfo refIdleInfo);

            /// <summary>
            /// Structure to hold returned system idle time information.
            /// </summary>
            [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential)]
            private struct SystemIdleInfo
            {
                [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.U4)]
                public int ByteSize;
                [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.U4)]
                public int TimeInMS;
            }

            /// <summary>
            /// Used to determine how long the system / computer has been idle for.
            /// </summary>
            /// <returns>Returns the amount of time (in seconds) the system has been idle. Returns 0 if idle time cannot be determined.</returns>
            public static double GetIdleTimeInSeconds()
            {
                try
                {
                    // New up structure to hold idle time information
                    var SystemIdleInfo = new SystemIdleInfo();

                    // Initialize variables
                    SystemIdleInfo.ByteSize = System.Runtime.InteropServices.Marshal.SizeOf(SystemIdleInfo);
                    SystemIdleInfo.TimeInMS = 0;

                    // Return 0 if idle time can't be read (assume not idle)
                    if (GetLastInputInfo(ref SystemIdleInfo) == false) return 0;

                    // If idle time is read, save how long the system has been on and active
                    int SysStartMS = Environment.TickCount;
                    int TimeSinceLastActivityMS = SystemIdleInfo.TimeInMS;

                    // Compute difference in seconds
                    double DiffInSeconds = Convert.ToDouble((SysStartMS - TimeSinceLastActivityMS) / (double)1000);

                    // Return the difference
                    return DiffInSeconds;
                }
                catch (Exception ex)
                {
                    // Return 0 if idle time can't be read (assume not idle)
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return 0;
                }
            }

            /// <summary>
            /// Internal use only class to watch for system idle timeout and invoke a method.
            /// </summary>
            private class IdleTimeWatcher
            {
                public void WatchAsync(double IdleTimeoutInMinutes, Action MethodToInvokeIfIdleTimeoutOccurs)
                {
                    // Just exit if inputs are bad
                    if (IdleTimeoutInMinutes <= 0 || MethodToInvokeIfIdleTimeoutOccurs == null)
                    {
                        return;
                    }

                    // If inputs are good, create thread to watch for timeout
                    var thrWatchForTimeout = new System.Threading.Thread(() => BkrndWatch(IdleTimeoutInMinutes, MethodToInvokeIfIdleTimeoutOccurs));
                    thrWatchForTimeout.SetApartmentState(System.Threading.ApartmentState.STA);
                    thrWatchForTimeout.Priority = System.Threading.ThreadPriority.Lowest;
                    thrWatchForTimeout.IsBackground = true;
                    thrWatchForTimeout.Start();
                }
                private void BkrndWatch(double IdleTimeoutInMinutes, Action MethodToInvokeIfIdleTimeoutOccurs)
                {
                    // Create var to hold how often to check for timeout
                    int TimeoutCheckFrequencyInMS = 5000;

                    // If timeout is less than 1 minute, check more regularly
                    if (IdleTimeoutInMinutes <= 1) TimeoutCheckFrequencyInMS = 1000;

                    // Keep looping until timeout occurs (ignore errors)
                    while (true)
                    {
                        try
                        {
                            // Get total idle time in seconds
                            double CurrentIdleTimeInSeconds = GetIdleTimeInSeconds();

                            // Convert to minutes
                            double CurrentIdleTimeInMinutes = System.Math.Round(CurrentIdleTimeInSeconds / 60, 2, MidpointRounding.AwayFromZero);

                            // Check if idle timeout has occurred
                            if (CurrentIdleTimeInMinutes >= IdleTimeoutInMinutes)
                            {
                                // If idle timeout has occurred, invoke passed in method and exit
                                System.Windows.Application.Current.Dispatcher.Invoke(() => MethodToInvokeIfIdleTimeoutOccurs());
                                return;
                            }
                        }
                        finally
                        {
                            // If idle timeout has NOT occurred (or error occurred), just keep waiting and checking again during next loop
                            System.Threading.Thread.Sleep(TimeoutCheckFrequencyInMS);
                        }
                    }
                }
            }

            /// <summary>
            /// Used to watch for a user specified amount of time if a system idle event occurs (i.e. no keyboard or mouse input for n minutes). The
            /// method will call a user specified action if a timeout occurs. Note that the watcher must be RESTARTED once a timeout has occurred to
            /// prevent continual invoking of the passed in action.
            /// </summary>
            /// <param name="TimeoutToWatchForInMinutes">The amount of time in minutes that is considered an idle timeout.</param>
            /// <param name="MethodToInovkeIfTimeoutOccurs">The method to invoke if the idle timeout occurs.</param>
            public static void WatchForIdleTimeout(double TimeoutToWatchForInMinutes, Action MethodToInovkeIfTimeoutOccurs)
            {
                // Create new instance of watch class and start watching for timeout
                new IdleTimeWatcher().WatchAsync(TimeoutToWatchForInMinutes, MethodToInovkeIfTimeoutOccurs);
            }

        }


        #endregion

        #region Clipboard Interface


        public struct Clipboard
        {

            /// <summary>
            /// Attempts to return any string data on the clipboard.
            /// </summary>
            /// <returns>Returns the clipboard text if retrievable. Returns an empty string if there is no data or an error occurs reading the data.</returns>
            public static string GetText()
            {
                try
                {
                    // Attempt to return text on clipboard
                    return System.Windows.Clipboard.GetText();
                }
                catch (Exception ex)
                {
                    // Return empty string if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

            /// <summary>
            /// Sets the text on the clipboard (i.e. so the user can paste something).
            /// </summary>
            /// <param name="TextToSet">The text to save on the clipboard.</param>
            /// <returns>Returns TRUE if the text is saved to the clipboard. Returns FALSE in all other conditions.</returns>
            public static bool SetText(string TextToSet)
            {
                try
                {
                    // Attempt to set the text
                    System.Windows.Clipboard.SetText(TextToSet);

                    // Return true if text sets
                    return true;
                }
                catch (Exception ex)
                {
                    // Return false if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }

        }


        #endregion

        #region Keyboard Interface


        public struct Keyboard
        {

            /// <summary>
            /// Used to check if the alt key is pressed on the keyboard.
            /// </summary>
            /// <returns>Returns TRUE if the key is pressed. Returns FALSE if the key is not pressed OR an error occurs.</returns>
            public static bool IsAltKeyPressed()
            {
                try
                {
                    // Check for either key
                    if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftAlt) == true || System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightAlt) == true)
                    {
                        // Return true if either is pressed
                        return true;
                    }
                    else
                    {
                        // Return false if neither is pressed
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    // Return false if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }

            /// <summary>
            /// Used to check if the ctrl key is pressed on the keyboard.
            /// </summary>
            /// <returns>Returns TRUE if the key is pressed. Returns FALSE if the key is not pressed OR an error occurs.</returns>
            public static bool IsCtrlKeyPressed()
            {
                try
                {
                    // Check for either key
                    if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftCtrl) == true || System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftCtrl) == true)
                    {
                        // Return true if either is pressed
                        return true;
                    }
                    else
                    {
                        // Return false if neither is pressed
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    // Return false if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }

            /// <summary>
            /// Used to check if the shift key is pressed on the keyboard.
            /// </summary>
            /// <returns>Returns TRUE if the key is pressed. Returns FALSE if the key is not pressed OR an error occurs.</returns>
            public static bool IsShiftKeyPressed()
            {
                try
                {
                    // Check for either key
                    if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftShift) == true || System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightShift) == true)
                    {
                        // Return true if either is pressed
                        return true;
                    }
                    else
                    {
                        // Return false if neither is pressed
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    // Return false if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }

            /// <summary>
            /// Used to check if the CAPS lock is turned on.
            /// </summary>
            /// <returns>Returns TRUE if caps lock is on. Returns FALSE if off OR an error occurs.</returns>
            public static bool IsCapsLockOn()
            {
                try
                {
                    // Try to return state of the key
                    return new Microsoft.VisualBasic.Devices.Keyboard().CapsLock;
                }
                catch (Exception ex)
                {
                    // Return false if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }

            /// <summary>
            /// Used to send keyboard information to the active window.
            /// </summary>
            /// <param name="StringToSend">The keystrokes to send to the active window.</param>
            /// <param name="WaitForKeysToGetProcessed">Flag to indicate if the program should wait for the keys to be processed by the active window before continuing.</param>
            /// <returns>Returns TRUE if the keystrokes are sent successfully. Returns FALSE in all other conditions.</returns>
            public static bool SendKeys(string StringToSend, bool WaitForKeysToGetProcessed)
            {
                try
                {
                    // Attempt to send the keys
                    new Microsoft.VisualBasic.Devices.Keyboard().SendKeys(StringToSend, WaitForKeysToGetProcessed);

                    // Return true if keys send
                    return true;
                }
                catch (Exception ex)
                {
                    // Return false if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }

            /// <summary>
            /// Used to show the built in (Windows 7 and newer) modern on-screen keyboard.
            /// </summary>
            /// <returns>Returns TRUE if the keyboard is started. Returns FALSE in all other conditions.</returns>
            public static bool ShowOnScreenKeyboard()
            {
                // Create var to hold x86 string in program files path if app is run in 32 bit mode on a 64 bit machine
                const string x86String = " (x86)";

                // Get the path to the new win7/win8 style on screen keyboard and the legacy on screen keyboard
                string OnScreenKeyboardModern64 = (Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles) + @"\Microsoft Shared\ink\TabTip.exe").Replace(x86String, string.Empty);
                string OnScreenKeyboardModern32 = Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFilesX86) + @"\Microsoft Shared\ink\TabTip.exe";

                // Add keyboards to enumerable for indexing (priority is modern keyboard)
                var AllKeyboards = new[] { OnScreenKeyboardModern64, OnScreenKeyboardModern32 }.ToList();

                // Loop through and launch the first keyboard that is found (again, priority to the modern keyboard)
                foreach (string KEYBOARD in AllKeyboards)
                {
                    // Check if keyboard is there
                    if (System.IO.File.Exists(KEYBOARD) == true)
                    {
                        // If keyboard won't start, skip it and try the next one
                        if (Process.Launch(KEYBOARD, false, false) == true)
                        {
                            // If the keyboard is started, return true and exit to prevent launching other keyboards
                            return true;
                        }
                        else
                        {
                            // If keyboard can't be started, continue on and try next one
                            continue;
                        }
                    }
                }

                // If no keyboards were found and started, return false
                return false;
            }

            /// <summary>
            /// Used to close any open on-screen keyboards.
            /// </summary>
            public static void CloseOnScreenKeyboard()
            {
                // Save all processes to kill
                var ProcessesToKill = new[] { "TabTip" }.ToList();

                // Loop through and close all processes / keyboards
                foreach (string KEYBOARD in ProcessesToKill)
                {
                    // If any keyboard is killed, exit to reduce risk of closing a non-keyboard app with same name string
                    if (Process.Kill(KEYBOARD) == true)
                    {
                        return;
                    }
                }
            }

        }


        #endregion

        #region Audio Interface


        public partial struct Audio
        {

            /// <summary>
            /// Used to play an audio sound through the computer's speakers.
            /// </summary>
            /// <param name="FullFilePathOfAudioFile">The full file path to the audio file to play.</param>
            /// <param name="WaitForPlayToComplete">Flag to indicate if the process should stop and wait for the sound to complete before continuing.</param>
            public static void Play(string FullFilePathOfAudioFile, bool WaitForPlayToComplete)
            {
                try
                {
                    // Attempt to play audio
                    switch (WaitForPlayToComplete)
                    {
                        case true:
                            new Microsoft.VisualBasic.Devices.Audio().Play(FullFilePathOfAudioFile, Microsoft.VisualBasic.AudioPlayMode.WaitToComplete);
                            break;
                        default:
                            new Microsoft.VisualBasic.Devices.Audio().Play(FullFilePathOfAudioFile, Microsoft.VisualBasic.AudioPlayMode.Background);
                            break;
                    }
                }
                catch (Exception ex)
                {
                    // Just log errors
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                }
            }
            /// <summary>
            /// Used to play an audio sound through the computer's speakers.
            /// </summary>
            /// <param name="AudioBytes">The byte array that contains the audio data to play.</param>
            /// <param name="WaitForPlayToComplete">Flag to indicate if the process should stop and wait for the sound to complete before continuing.</param>
            public static void Play(IEnumerable<byte> AudioBytes, bool WaitForPlayToComplete)
            {
                try
                {
                    // Attempt to play audio
                    switch (WaitForPlayToComplete)
                    {
                        case true:
                            new Microsoft.VisualBasic.Devices.Audio().Play(AudioBytes.ToArray(), Microsoft.VisualBasic.AudioPlayMode.WaitToComplete);
                            break;
                        default:
                            new Microsoft.VisualBasic.Devices.Audio().Play(AudioBytes.ToArray(), Microsoft.VisualBasic.AudioPlayMode.Background);
                            break;
                    }
                }
                catch (Exception ex)
                {
                    // Just log errors
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    throw;
                }
            }
            /// <summary>
            /// Used to play an audio sound on a perpetual loop (until stop is called) through the computer's speakers.
            /// </summary>
            /// <param name="FullFilePathOfAudioFile">The full file path to the audio file to play.</param>
            public static void PlayOnLoop(string FullFilePathOfAudioFile)
            {
                try
                {
                    // Attempt to play audio
                    new Microsoft.VisualBasic.Devices.Audio().Play(FullFilePathOfAudioFile, Microsoft.VisualBasic.AudioPlayMode.BackgroundLoop);
                }
                catch (Exception ex)
                {
                    // Just log errors
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                }
            }
            /// <summary>
            /// Used to play an audio sound on a perpetual loop (until stop is called) through the computer's speakers.
            /// </summary>
            /// <param name="AudioBytes">The byte array that contains the audio data to play.</param>
            public static void PlayOnLoop(IEnumerable<byte> AudioBytes)
            {
                try
                {
                    // Attempt to play audio
                    new Microsoft.VisualBasic.Devices.Audio().Play(AudioBytes.ToArray(), Microsoft.VisualBasic.AudioPlayMode.BackgroundLoop);
                }
                catch (Exception ex)
                {
                    // Just log errors
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                }
            }

            /// <summary>
            /// Stops any audio sounds that were requested using the Play methods.
            /// </summary>
            public static void Stop()
            {
                try
                {
                    // Request stop to any playing audio
                    new Microsoft.VisualBasic.Devices.Audio().Stop();
                }
                catch (Exception ex)
                {
                    // Log errors
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                }
            }

            /// <summary>
            /// Method to convert an audio stream in memory to a list of bytes (automatically disposes of the source stream when done).
            /// </summary>
            /// <param name="AudioStream">The audio stream to convert to bytes.</param>
            /// <returns>Returns a list of bytes if the conversion is successful. Returns NOTHING in all other conditions.</returns>
            public static List<byte> ConvertAudioStreamToBytes(System.IO.Stream AudioStream)
            {
                try
                {
                    // Create list to hold bytes
                    var AudioBytes = new List<byte>();

                    // Loop through and read all bytes to array
                    for (var i = AudioStream.Position; i <= AudioStream.Length - 1; i++)
                    {
                        try
                        {
                            // Convert bytes
                            AudioBytes.Add(Convert.ToByte(AudioStream.ReadByte()));
                        }
                        catch (Exception ex)
                        {
                            // Log errors
                            System.Diagnostics.Trace.WriteLine(ex.Message);
                        }
                    }

                    // Return the bytes if we're good
                    return AudioBytes;
                }
                catch (Exception ex)
                {
                    // Return nothing if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
                finally
                {
                    // Always clean up stream
                    if (AudioStream != null)
                    {
                        AudioStream.Dispose();
                    }
                }
            }

        }


        #endregion

        #region Media Interface


        public struct Media
        {

            // Internal var to hold handle to media player
            private static System.Windows.Media.MediaPlayer MediaPlayer = new System.Windows.Media.MediaPlayer();

            /// <summary>
            /// Starts playing a media file.
            /// </summary>
            public static void PlayAsync(string FullFilePathToMediaFile)
            {
                try
                {
                    // Always stop whatever is currently playing
                    StopAsync();

                    // Bail if there's no input
                    if (string.IsNullOrEmpty(FullFilePathToMediaFile) == true)
                    {
                        return;
                    }

                    // Bail if the file is missing
                    if (System.IO.File.Exists(FullFilePathToMediaFile) == false)
                    {
                        return;
                    }

                    // Refresh the handle to the media player in case a new thread tried doing something
                    MediaPlayer = new System.Windows.Media.MediaPlayer();

                    // If it's there, load it up and attempt to play
                    MediaPlayer.Open(new System.Uri(FullFilePathToMediaFile));
                    MediaPlayer.Play();
                }
                catch (Exception ex)
                {
                    // Just log errors
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                }
            }
            /// <summary>
            /// Stops the currently playing media file.
            /// </summary>
            public static void StopAsync()
            {
                try
                {
                    // Attempt stop
                    MediaPlayer.Stop();
                }
                catch (Exception ex)
                {
                    // Just log errors
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                }
            }

        }


        #endregion

        #region Display Interface


        public struct Screen
        {

            /// <summary>
            /// Declare function from user32.dll to get access to LockWorkStation method.
            /// </summary>
            [System.Runtime.InteropServices.DllImport("user32.dll")]
            private static extern bool LockWorkStation();

            /// <summary>
            /// Used to force the screen to lock on the PC (i.e. equivalent to Ctrl + Alt + Delete -> Lock This Computer).
            /// </summary>
            /// <returns>Returns TRUE if the request is successful. Returns FALSE in all other conditions.</returns>
            public static bool Lock()
            {
                try
                {
                    // Attempt to lock screen and return result
                    return LockWorkStation();
                }
                catch (Exception ex)
                {
                    // Return false if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }

            /// <summary>
            /// Used to determine the resolution of the primary monitor.
            /// </summary>
            /// <returns>Returns the size in pixels of the primary monitor if successful. Returns NOTHING in all other conditions.</returns>
            public static Nullable<System.Drawing.Size> GetDisplayResolution()
            {
                try
                {
                    // Request the resolution
                    var Resolution = System.Windows.SystemParameters.WorkArea;

                    // Compute the rounded width / height (should always be ints, round to be safe)
                    int RoundedWidth = Convert.ToInt32(System.Math.Round(Resolution.Width, 0, MidpointRounding.AwayFromZero));
                    int RoundedHeight = Convert.ToInt32(System.Math.Round(Resolution.Height, 0, MidpointRounding.AwayFromZero));

                    // Convert to size
                    return new System.Drawing.Size(RoundedWidth, RoundedHeight);
                }
                catch (Exception ex)
                {
                    // Return nothing if error occurs
                    System.Diagnostics.Trace.Write(ex.Message);
                    return null;
                }
            }

            /// <summary>
            /// Used to determine the DPI scaling for the X / horizontal axis of a given WPF window (1 = 96 DPI = No Scaling).
            /// </summary>
            /// <param name="WindowToCheck">The window to check the DPI scaling of.</param>
            /// <returns>Returns a number indicating the scaling of the passed in window if the scaling factor was determined. Return NOTHING if the scaling factor could not be determined.</returns>
            public static Nullable<double> GetDisplayScalingFactorX(System.Windows.Window WindowToCheck)
            {
                try
                {
                    // Create source for determining DPI scaling
                    var Source = System.Windows.PresentationSource.FromVisual(WindowToCheck);

                    // Return X scaling
                    return Source.CompositionTarget.TransformToDevice.M11;
                }
                catch (Exception ex)
                {
                    // Return null if an error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }

            /// <summary>
            /// Used to determine the DPI scaling for the Y / horizontal axis of a given WPF window (1 = 96 DPI = No Scaling).
            /// </summary>
            /// <param name="WindowToCheck">The window to check the DPI scaling of.</param>
            /// <returns>Returns a number indicating the scaling of the passed in window if the scaling factor was determined. Return NOTHING if the scaling factor could not be determined.</returns>
            public static Nullable<double> GetDisplayScalingFactorY(System.Windows.Window WindowToCheck)
            {
                try
                {
                    // Create source for determining DPI scaling
                    var Source = System.Windows.PresentationSource.FromVisual(WindowToCheck);

                    // Return Y scaling
                    return Source.CompositionTarget.TransformToDevice.M22;
                }
                catch (Exception ex)
                {
                    // Return null if an error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }

            /// <summary>
            /// Used to check if a given WPF window has DPI scaling turned on.
            /// </summary>
            /// <param name="WindowToCheck">The window to check the DPI scaling of.</param>
            /// <returns>Returns TRUE if a DPI other than 96 is found. Returns FALSE if the window is using 96 DPI. Returns NOTHING if the DPI scaling of the window could not be determined.</returns>
            public static Nullable<bool> IsDisplayScalingOn(System.Windows.Window WindowToCheck)
            {
                try
                {
                    // Get the X and Y scaling factors
                    var XScaling = GetDisplayScalingFactorX(WindowToCheck);
                    var YScaling = GetDisplayScalingFactorY(WindowToCheck);

                    // Return null if either couldn't be checked
                    if (XScaling == null || YScaling == null) return null;

                    // If data came back, check for scaled values
                    if (XScaling == 1 && YScaling == 1)
                    {
                        // If neither dimension has scaling, return false
                        return false;
                    }
                    else
                    {
                        // If either dimension has scaling, return true
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    // Return nothing if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }

        }


        #endregion

        #region Helper Methods


        private static double BytesToGBs(double NumberOfBytes, Nullable<int> NumberOfDigitsToRoundTo = null)
        {
            // Compute the GBs
            double GBs = NumberOfBytes / System.Math.Pow(2, 30);

            // Return the value if user requested no rounding
            if (NumberOfDigitsToRoundTo == null || NumberOfDigitsToRoundTo < 0) return GBs;

            // If user wanted rounding, round and return
            return System.Math.Round(GBs, System.Convert.ToInt32(NumberOfDigitsToRoundTo), MidpointRounding.AwayFromZero);
        }

        private static string ConvertBooleanToYesNoString(bool BooleanToConvert)
        {
            // Convert the boolean to a yes / no string and return it
            if (BooleanToConvert == true)
            {
                return "Yes";
            }
            else
            {
                return "No";
            }
        }

        private static bool IsNumeric(object ObjectToCheck)
        {
            try
            {
                // Return false if nothing is set
                if (ObjectToCheck == null) return false;

                // Convert object to string for checking
                string StringValue = ObjectToCheck.ToString();

                // Return false if string is bad / empty
                if (string.IsNullOrEmpty(StringValue) == true) return false;

                // Create dummy var to hold the result of the attempted parse
                double DummyVar = 0;

                // If string looks good, attempt to parse it
                return double.TryParse(StringValue, out DummyVar);
            }
            catch (Exception ex)
            {
                // Return false if error occurs
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return false;
            }
        }


        #endregion

    }

}
