﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StaticMethods
{

    public partial struct FileSystem
    {

        /// <summary>
        /// Class for specifying file type filters.
        /// </summary>
        public class FileTypeFilter
        {
            // Vars to hold the file type descriptions and extensions
            public string Description = string.Empty;
            public string Extension = string.Empty;

            /// <summary>
            /// Creates a new instance of the file type selection filter.
            /// </summary>
            /// <param name="FileTypeDescription">The string name description to show for the file selection filter (e.g. "Text Files").</param>
            /// <param name="FileExtension">The wild card file extension for the file type (e.g. "*.txt").</param>
            public FileTypeFilter(string FileTypeDescription, string FileExtension)
            {
                // Save passed in info
                Description = FileTypeDescription;
                Extension = FileExtension;
            }
        }

        /// <summary>
        /// Used to display the Windows Open File Dialog so the user can select a file.
        /// </summary>
        /// <param name="MsgToDisplay">The string / message to display in the dialog's title bar.</param>
        /// <param name="StartingDirectory">OPTIONAL:  The directory to start in (using nothing allows Windows to choose the default starting location).</param>
        /// <param name="SelectionFilter">OPTIONAL:  A class instance to specify the selectable file descriptions and corresponding file extensions (if specified, only these types of files will be selectable using the dialog).</param>
        /// <returns>Returns the string file path to the selected file if the user confirms the selection. Returns an empty string in all other conditions.</returns>
        public static string RequestFile(string MsgToDisplay, string StartingDirectory = "", FileTypeFilter SelectionFilter = null)
        {
            // Use helper w/ single file select flag and capture the result
            var SelectionResult = RequestFilesHelper(MsgToDisplay, false, StartingDirectory, SelectionFilter);

            // Return an empty string if the user canceled
            if (SelectionResult == null || SelectionResult.Count == 0) return string.Empty;

            // If a file was selected, return it (will always be first item since multi select was disabled)
            return SelectionResult[0];
        }
        /// <summary>
        /// Used to display the Windows Open File Dialog so the user can select ONE OR MORE files.
        /// </summary>
        /// <param name="MsgToDisplay">The string / message to display in the dialog's title bar.</param>
        /// <param name="StartingDirectory">OPTIONAL:  The directory to start in (using nothing allows Windows to choose the default starting location).</param>
        /// <param name="SelectionFilter">OPTIONAL:  A class instance to specify the selectable file descriptions and corresponding file extensions (if specified, only these types of files will be selectable using the dialog).</param>
        /// <returns>Returns a list of selected file paths if the user confirms the selections. Returns nothing in all other conditions.</returns>
        public static List<string> RequestFiles(string MsgToDisplay, string StartingDirectory = "", FileTypeFilter SelectionFilter = null)
        {
            // Use helper method and return result
            return RequestFilesHelper(MsgToDisplay, true, StartingDirectory, SelectionFilter);
        }
        private static List<string> RequestFilesHelper(string MsgToDisplay, bool AllowMultiFileSelect, string StartingDirectory, FileTypeFilter SelectionFilter)
        {
            // Create a new instance of the file picker
            using (var FilePicker = new System.Windows.Forms.OpenFileDialog())
            {
                // Set starting directory if it exists
                if (string.IsNullOrEmpty(StartingDirectory) == false && System.IO.Directory.Exists(StartingDirectory) == true)
                {
                    FilePicker.InitialDirectory = StartingDirectory;
                }
                    
                // Set the title
                FilePicker.Title = MsgToDisplay;

                // Set if user can select multiple files
                FilePicker.Multiselect = AllowMultiFileSelect;

                try
                {
                    // If user passed in a filter, apply it (must have passed in both arguments)
                    if (SelectionFilter != null && string.IsNullOrEmpty(SelectionFilter.Description) == false && string.IsNullOrEmpty(SelectionFilter.Extension) == false)
                    {
                        // Ensure extension has wildcard & dot located correctly (*.EXT)
                        SelectionFilter.Extension = SelectionFilter.Extension.Replace("*", string.Empty);
                        SelectionFilter.Extension = SelectionFilter.Extension.Replace(".", string.Empty);
                        SelectionFilter.Extension = "*." + SelectionFilter.Extension;

                        // Create the filter string (file type name | wild card file extension)
                        string FilterString = SelectionFilter.Description + "|" + SelectionFilter.Extension;

                        // Set the filter string
                        FilePicker.Filter = FilterString;

                        // Set the default file extension (from selection filter)
                        FilePicker.DefaultExt = SelectionFilter.Extension;
                    }
                }
                catch (Exception)
                {
                    // No action needed on failure
                }

                // Return nothing if user cancels
                if (FilePicker.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return null;
                }

                try
                {
                    // If user confirms, attempt to convert file names to list and return
                    return FilePicker.FileNames.ToList();
                }
                catch (Exception ex)
                {
                    // If list can't be converted, return nothing
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }
        }

        /// <summary>
        /// Used to show the Windows Folder Browser so the user can select a directory.
        /// </summary>
        /// <param name="MsgToDisplay">String to display on the top of the window.</param>
        /// <param name="StartingDirectory">OPTIONAL:  The high-level directory for the browser to start in.</param>
        /// <param name="ShowCreateNewFolderButton">OPTIONAL:  Boolean indicating whether the "Create New Folder" button should be visible.</param>
        /// <returns>Returns the string file path of the selected directory if the user accepts. Returns an empty string in all other conditions.</returns>
        public static string RequestFolder(string MsgToDisplay, string StartingDirectory = "", bool ShowCreateNewFolderButton = false)
        {
            // Create the folder picker
            using (var FolderPicker = new System.Windows.Forms.FolderBrowserDialog())
            {
                // Set the description
                FolderPicker.Description = MsgToDisplay;

                // Set starting directory if it exists
                if (string.IsNullOrEmpty(StartingDirectory) == false && System.IO.Directory.Exists(StartingDirectory))
                {
                    FolderPicker.SelectedPath = StartingDirectory;
                }

                // Set the visibility of the make new folder button
                FolderPicker.ShowNewFolderButton = ShowCreateNewFolderButton;

                // Show the folder selector
                if (FolderPicker.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    // Return selected folder if user confirms
                    return FolderPicker.SelectedPath;
                }
                else
                {
                    // Return empty string if user cancels
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Used to display the Windows Save File Dialog so the user can select a save path.
        /// </summary>
        /// <param name="MsgToDisplay">String to display at the top of the window.</param>
        /// <param name="DefaultFileExtension">The default file extension to append if one isn't specified (e.g. ".CSV").</param>
        /// <param name="ShowOverwriteWarningIfFileExists">Boolean indicating whether the dialog should show an overwrite warning if the file already exists.</param>
        /// <param name="StartingFileNameToUse">OPTIONAL:  The starting / default file name to use when the dialog is open (e.g. "MyNewFile", etc.).</param>
        /// <param name="StartingDirectory">OPTIONAL:  The high-level directory to start in.</param>
        /// <returns>Returns the string file path to save to if user confirms. Returns an empty string in all other conditions.</returns>
        public static string RequestSavePath(string MsgToDisplay, string DefaultFileExtension, bool ShowOverwriteWarningIfFileExists, string StartingFileNameToUse = "", string StartingDirectory = "")
        {
            // Create a new instance of the file picker
            using (var FilePicker = new System.Windows.Forms.SaveFileDialog())
            {
                // Set starting directory if it exists
                if (string.IsNullOrEmpty(StartingDirectory) == false && System.IO.Directory.Exists(StartingDirectory) == true)
                {
                    FilePicker.InitialDirectory = StartingDirectory;
                }

                // Set the title
                FilePicker.Title = MsgToDisplay;

                try
                {
                    // Set the starting file name to use if possible (i.e. might have illegal chars)
                    FilePicker.FileName = StartingFileNameToUse;
                }
                catch (Exception)
                {
                    // No action needed on failure
                }

                // Ensure extension isn't a wildcard (causes strange behavior)
                if (string.IsNullOrEmpty(DefaultFileExtension) == false)
                {
                    DefaultFileExtension.Replace("*", string.Empty);
                }

                // Ensure file extension starts with a dot if any extension is specified
                if (string.IsNullOrEmpty(DefaultFileExtension) == false && DefaultFileExtension.Length > 0)
                {
                    if (DefaultFileExtension.Substring(0, 1) != ".")
                    {
                        DefaultFileExtension = "." + DefaultFileExtension;
                    }
                }

                // Set the file extension
                FilePicker.DefaultExt = DefaultFileExtension;

                // Set warning flag
                FilePicker.OverwritePrompt = ShowOverwriteWarningIfFileExists;

                // Show the dialog & save result if user pushes OK
                if (FilePicker.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    // Return the selected file if user confirms
                    return FilePicker.FileName;
                }
                else
                {
                    // Return empty string if user cancels
                    return string.Empty;
                }
            }
        }
    
    }

}
