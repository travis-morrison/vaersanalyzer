﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Vaers
{

    public partial class MainWindow : Window
    {

        #region Declarations


        private DatabaseManager.BasicStats Stats = null;


        #endregion

        #region Load & Unload Methods


        public MainWindow()
        {
            // Draw window
            InitializeComponent();

            // Hide test button if NOT in visual studio
            if (StaticMethods.WinOS.Process.Current.IsRunningInVisualStudio() == false)
            {
                BTNTest.Visibility = Visibility.Collapsed;
            }

            // Wire up events
            this.Loaded += MainWindow_Loaded;
            this.Closed += MainWindow_Closed;
            BTNTest.Click += BTNTest_Click;
            BTNImportData.Click += BTNImportData_Click;
            BTNBuildReport.Click += BTNBuildReport_Click;
        }

        private void MainWindow_Closed(object sender, EventArgs e)
        {
            // Kill app when main window is closed
            Application.Current.Shutdown();
        }

        private async void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            // Set title bar info
            this.Title = ThisApp.Name + " v" + ThisApp.Version;

            // When window loads, remove control while querying data
            SetUserCtrl(false);

            // Show status
            ReportStatusMsg("Loading up current database stats...");

            // Wait for database info
            Stats = await DatabaseManager.RequestBasicStatsAsync();

            // Once we're back, check if error occurred
            if (Stats == null)
            {
                // If we failed, show error message
                ReportStatusMsg("ERROR LOADING UP DATABASE INFORMATION!");
            }
            else
            {
                // If we're good, show stats
                ReportStatusMsg("Database Stats:\r\n" + Stats.ToString());
                ReportStatusMsg("Found [ " + Stats.RecordCount + " ] records between [ " + Stats.MinVaxDate + " ] and [ " + Stats.MaxVaxDate + " ].");

                // Show available years for reporting in CB
                PopulateYearListBoxes();
            }

            // Restore user control
            SetUserCtrl(true);
        }


        #endregion

        #region Button Click Methods


        private async void BTNImportData_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Have user pick import folder
                string FolderPath = StaticMethods.FileSystem.RequestFolder("VAERS CSV Location...");
                if (string.IsNullOrEmpty(FolderPath) == true)
                {
                    // Bail if user cancels
                    return;
                }

                // If they confirm, remove control
                SetUserCtrl(false);

                // Create import helper
                var ImportHelper = new FileImportManager(ReportStatusMsg);

                // Attempt import (bail on failure)
                if (await ImportHelper.LoadFolderAsync(FolderPath) == false)
                {
                    ReportStatusMsg("Data import failed!");
                    return;
                }

                // If it works, show success msg
                ReportStatusMsg("All NEW data was imported successfully!");
            }
            finally
            {
                // Restore control when done
                SetUserCtrl(true);
            }
        }

        private async void BTNBuildReport_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Bail if either start or end year is missing
                if (CBStartYear.SelectedIndex == -1 || CBStopYear.SelectedIndex == -1)
                {
                    ReportStatusMsg("MISSING START / END YEAR!");
                    return;
                }

                // If we have selections, save start and end year
                uint StartYear = Convert.ToUInt32(CBStartYear.SelectedItem);
                uint StopYear = Convert.ToUInt32(CBStopYear.SelectedItem);

                // Report error if bad
                if (StartYear > StopYear)
                {
                    ReportStatusMsg("START YEAR CANNOT BE AFTER STOP YEAR!");
                    return;
                }

                // If we're good, remove control
                SetUserCtrl(false);

                // Report status
                ReportStatusMsg("Aggregating data for selected report years...");

                // Create helper to make report and attempt load
                var ReportBuilder = new DatabaseManager(ReportStatusMsg);
                if (await ReportBuilder.LoadAsync(StartYear, StopYear) == false)
                {
                    // Report error if we fail
                    ReportStatusMsg("COULD NOT GENERATE REPORT!");
                    return;
                }

                // Report status
                ReportStatusMsg("Building final report...");

                // Create the report
                var FinalReport = await ReportBuilder.ToVaccineEventReportAsync();
                if (FinalReport == null)
                {
                    // Report error and bail if we couldn't make report
                    ReportStatusMsg("ERROR OCCURRED WHILE ATTEMPTING TO BUILD FINAL REPORT!");
                    return;
                }

                // Show details
                ReportStatusMsg(FinalReport.ToString());

                // Create new report window to show
                new ReportView(ReportBuilder, FinalReport, ReportStatusMsg).Show();
            }
            finally
            {
                // Restore control when done
                SetUserCtrl(true);
            }
        }


        #endregion

        #region Helper Methods


        private void ReportStatusMsg(string Msg)
        {
            // Check if on main thread
            if (this.Dispatcher.CheckAccess() == false)
            {
                // If not, invoke main
                this.Dispatcher.Invoke(() => ReportStatusMsg(Msg));
            }
            else
            {
                // If on main, create full msg and add to view
                string FullMsg = StaticMethods.Strings.Generate.UniqueID(true, true) + "  " + Msg;
                LBLog.Items.Add(FullMsg);

                // If there are lots of items, remove old ones
                while (LBLog.Items.Count > 150)
                {
                    LBLog.Items.RemoveAt(0);
                }

                // Scroll it into view
                LBLog.ScrollIntoView(FullMsg);
            }
        }

        private void PopulateYearListBoxes()
        {
            try
            {
                // Get the start and stop dates
                DateTime StartDate = StaticMethods.Strings.Convert.SortableStringToDate(Stats.MinVaxDate).Value;
                DateTime StopDate = StaticMethods.Strings.Convert.SortableStringToDate(Stats.MaxVaxDate).Value;

                // Get the start and stop year
                int StartYear = StartDate.Year;
                int StopYear = StopDate.Year;

                // Create a list to hold all years in range
                var AllYears = new List<int>();

                // Go through and add years
                for (int i = 0; i <= (StopYear - StartYear); i++)
                {
                    AllYears.Add(StartYear + i);
                }

                // Populate CBs
                CBStartYear.ItemsSource = AllYears;
                CBStopYear.ItemsSource = AllYears;

                // Default to newest year
                CBStartYear.SelectedIndex = AllYears.Count - 1;
                CBStopYear.SelectedIndex = AllYears.Count - 1;

                // If there's at least 5 years, default to that
                if (AllYears.Count >= 5)
                {
                    CBStartYear.SelectedIndex = AllYears.Count - 5;
                }
            }
            catch (Exception ex)
            {
                // Ignore errors
                ThisApp.Methods.Logging.WriteMsgWithException("Error loading combo boxes!", ex);
            }
        }

        private void SetUserCtrl(bool UserHasCtrl)
        {
            // Check if on main thread
            if (this.Dispatcher.CheckAccess() == false)
            {
                // If not, invoke main
                this.Dispatcher.Invoke(() => SetUserCtrl(UserHasCtrl));
            }
            else
            {
                // If on main, update control
                GRDMain.IsEnabled = UserHasCtrl;
            }
        }


        #endregion

        #region Engineering Use Only


        private void BTNTest_Click(object sender, RoutedEventArgs e)
        {

        }


        #endregion

    }
}
