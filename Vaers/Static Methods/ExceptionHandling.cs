﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StaticMethods
{

    public partial struct ExceptionHandling
    {

        /// <summary>
        /// Enum to specify the formating options for creating a string error message from an exception object.
        /// </summary>
        public enum ErrorMsgFormatOptions
        {
            HeadersBetweenMessages,
            NewLineBetweenMessages,
            SpaceBetweenMessages
        }

        /// <summary>
        /// Used to convert a full exception object into a human-readable text string.
        /// </summary>
        /// <param name="ExceptionToUse">An instance of an exception to extract the info for the message from.</param>
        /// <param name="IncludeInnerException">Flag to indicate if the text from the inner exception should be included.</param>
        /// <param name="IncludeStackTrace">Flag to indicate if the stack trace should be included.</param>
        /// <param name="ErrorMsgFormat">Used to specify how to format the different chunks of the message (i.e. the divide between the exception message and the inner exception message, etc.).</param>
        /// <returns>Returns a human-readable text string with the requested error info if the exception object is processed successfully. Returns an empty string in all other conditions.</returns>
        public static string CreateErrorMsgString(Exception ExceptionToUse, bool IncludeInnerException, bool IncludeStackTrace, ErrorMsgFormatOptions ErrorMsgFormat)
        {
            try
            {
                // Return empty string if there is no input
                if (ExceptionToUse == null) return string.Empty;

                // Create list to hold all info to join and return
                var LinesToMerge = new List<string>();

                // If exception exists, check if adding headers
                if (ErrorMsgFormat == ErrorMsgFormatOptions.HeadersBetweenMessages)
                {
                    // If so, add header and main exception msg
                    LinesToMerge.Add("Error Message:");
                    LinesToMerge.Add(ExceptionToUse.Message);
                }
                else
                {
                    // If not, just add the exception msg
                    LinesToMerge.Add(ExceptionToUse.Message);
                }

                // Check if adding the inner exception IF there is an inner exception
                if (ExceptionToUse.InnerException != null && IncludeInnerException == true)
                {
                    // If there is an inner exception AND caller requested that info too, add it
                    switch (ErrorMsgFormat)
                    {
                        case ErrorMsgFormatOptions.HeadersBetweenMessages:
                            // Add spacer
                            LinesToMerge.Add(string.Empty);

                            // Add inner exception header and info
                            LinesToMerge.Add("Inner Exception:");
                            LinesToMerge.Add(ExceptionToUse.InnerException.Message);
                            LinesToMerge.Add(GetAllNestedExceptionMessages(ExceptionToUse));
                            break;
                        case ErrorMsgFormatOptions.NewLineBetweenMessages:
                            // Add spacer
                            LinesToMerge.Add(string.Empty);

                            // Add inner exception message
                            LinesToMerge.Add(ExceptionToUse.InnerException.Message);
                            LinesToMerge.Add("   " + GetAllNestedExceptionMessages(ExceptionToUse));
                            break;
                        case ErrorMsgFormatOptions.SpaceBetweenMessages:
                            // Add spaces between info
                            LinesToMerge[0] = LinesToMerge[0] + " " + ExceptionToUse.InnerException.Message + " " + GetAllNestedExceptionMessages(ExceptionToUse);
                            break;
                        default:
                            // Throw error if something is selected we haven't implemented yet
                            throw new Exception("Unimplemented error message formatting option selected in IncludeInnerException block!");
                    }
                }

                // Check if adding the stack trace
                if (IncludeStackTrace == true)
                {
                    // If so, add using selected format
                    switch (ErrorMsgFormat)
                    {
                        case ErrorMsgFormatOptions.HeadersBetweenMessages:
                            // Add spacer
                            LinesToMerge.Add(string.Empty);

                            // Add header and trace
                            LinesToMerge.Add("Stack Trace:");
                            LinesToMerge.Add(ExceptionToUse.StackTrace);
                            break;
                        case ErrorMsgFormatOptions.NewLineBetweenMessages:
                            // Add spacer
                            LinesToMerge.Add(string.Empty);

                            // Add trace
                            LinesToMerge.Add(ExceptionToUse.StackTrace);
                            break;
                        case ErrorMsgFormatOptions.SpaceBetweenMessages:
                            // Add spaces between info
                            LinesToMerge[0] = LinesToMerge[0] + " " + ExceptionToUse.StackTrace;
                            break;
                        default:
                            // Throw error if unimplemented item is selected
                            throw new Exception("Unimplemented error message formatting option selected in IncludeStackTrace block!");
                    }
                }

                // Join and return the error info
                return string.Join(Environment.NewLine, LinesToMerge);
            }
            catch (Exception ex)
            {
                // Return empty string if error occurs
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return string.Empty;
            }
        }

        /// <summary>
        /// Used to recursively to through a top level exception and all inner exceptions and merge all error messages into a single string.
        /// </summary>
        /// <param name="TopLevelException">The outer-most (top level) exception to start at before drilling down to the inner exceptions.</param>
        /// <param name="DelimiterBetweenMessages">OPTIONAL:  Var to indicate how to delimit the error messages before merging them into a single string.</param>
        /// <returns>Returns a merged string with all exception messages in the exception hierarchy.</returns>
        public static string GetAllNestedExceptionMessages(Exception TopLevelException, string DelimiterBetweenMessages = " --> ")
        {
            // Create list to hold all exception messages
            var AllExceptionMsgs = new List<string>();

            // If there's an exception, save it
            if (TopLevelException != null)
            {
                AllExceptionMsgs.Add(TopLevelException.Message);
            }

            // Use recursion to drill down if there's an inner exception
            if (TopLevelException != null && TopLevelException.InnerException != null)
            {
                AllExceptionMsgs.Add(GetAllNestedExceptionMessages(TopLevelException.InnerException));
            }

            // Create final string to return
            string StringToReturn = string.Join(DelimiterBetweenMessages, AllExceptionMsgs);

            // If there's no data, add general message
            if (string.IsNullOrEmpty(StringToReturn) == true)
            {
                StringToReturn = "No additional exception details!";
            }

            // Return the info
            return StringToReturn;
        }

    }

}
