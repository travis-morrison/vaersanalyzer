﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utilities
{

    public class SerializableKeyValuePair<T1, T2>
    {

        // Vars to hold info
        public T1 Key { get; set; } = default(T1);
        public T2 Value { get; set; } = default(T2);

        // Default constructor
        public SerializableKeyValuePair()
        {
            // Empty to allow xml serialization
        }

        // Allow setting in constructor
        public SerializableKeyValuePair(T1 InputKey, T2 InputValue)
        {
            // Save inputs
            Key = InputKey;
            Value = InputValue;
        }

        // Create method to convert to standard KVP
        public KeyValuePair<T1, T2> ToDotNetKVP()
        {
            return new KeyValuePair<T1, T2>(Key, Value);
        }

        // Override ToString() to create the same behavior as normal KVPs
        public override string ToString()
        {
            return ToDotNetKVP().ToString();
        }
    
    }

}
