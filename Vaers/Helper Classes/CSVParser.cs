﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Utilities
{

    public class CSVParser
    {

        #region Declarations


        /// <summary>
        /// Holds all loaded data from the CSV file.
        /// </summary>
        public string[,] CSVContent { get; set; } = null;

        /// <summary>
        /// Holds all loaded data from the CSV file properly escaped so it can be written to file.
        /// </summary>
        public string[,] CSVContentEscaped
        {
            get
            {
                try
                {
                    // Return nothing if no data is set
                    if (CSVContent == null) return null;

                    // If we have data, create array of same size to return
                    string[,] EscapedArray = new string[CSVContent.GetUpperBound(0) + 1, CSVContent.GetUpperBound(1) + 1];

                    // Loop through and convert content
                    for (int i = 0; i <= CSVContent.GetUpperBound(0); i++)
                    {
                        for (int j = 0; j <= CSVContent.GetUpperBound(1); j++)
                        {
                            // Save the current entry to check for what needs to be done to it
                            string CleanedEntry = CSVContent[i, j];
                            if (string.IsNullOrEmpty(CleanedEntry) == true)
                            {
                                // Ensure we don't have nulls
                                CleanedEntry = string.Empty;
                            }

                            // Any quotes need to be double quote escaped per CSV standard (" -> "")
                            if (CleanedEntry.Contains("\"") == true)
                            {
                                CleanedEntry = CleanedEntry.Replace("\"", "\"" + "\"");
                            }

                            // If item has a comma in it, it needs to be surrounded in quotes
                            if (CleanedEntry.Contains(",") == true)
                            {
                                CleanedEntry = "\"" + CleanedEntry + "\"";
                            }

                            // Add the item to the clean escaped array
                            EscapedArray[i, j] = CleanedEntry;
                        }
                    }

                    // Return the array when done
                    return EscapedArray;
                }
                catch (Exception ex)
                {
                    // Return nothing if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }
        }


        #endregion

        #region Exposed Methods


        /// <summary>
        /// Loads a specified CSV file.
        /// </summary>
        public bool Load(string FullFilePathToCsv)
        {
            try
            {
                // Clear all existing data first before beginning
                CSVContent = null;

                // Return false if file path is missing
                if (string.IsNullOrEmpty(FullFilePathToCsv) == true) return false;

                // Return false if file is missing
                if (string.IsNullOrEmpty(FullFilePathToCsv) == true) return false;

                // Read the file to a string
                string FileContentAsString = ReadTextFileToString(FullFilePathToCsv);
                if (FileContentAsString == null)
                {
                    // Return false if NOTHING is returned (i.e. we got an error)
                    return false;
                }

                // Check if we got an empty string back
                if (FileContentAsString == string.Empty)
                {
                    // If so, size array to single item
                    CSVContent = new string[1, 1];

                    // Save an empty string
                    CSVContent[0, 0] = string.Empty;

                    // Return true (there is no data in file)
                    return true;
                }

                // If we have data, split it up by line endings to get rows
                var AllFileLines = FileContentAsString.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None).ToList();

                // For some reason, there's an empty line returned at the end, remove it
                if (AllFileLines.Count > 1 && string.IsNullOrEmpty(AllFileLines[AllFileLines.Count - 1]) == true)
                {
                    AllFileLines.RemoveAt(AllFileLines.Count - 1);
                }

                // Create list of list (jagged array) to hold all lines broken out into columns
                var RowsOfColumns = new List<List<string>>();

                // Loop through each line and parse into columns
                foreach (var LINE in AllFileLines)
                {
                    RowsOfColumns.Add(SplitRowToCsvContent(LINE));
                }

                // Loop through and find the max number of columns needed (should all be the same but be safe)
                int MaxColumnCount = 0;
                foreach (var COLLIST in RowsOfColumns)
                {
                    if (COLLIST.Count > MaxColumnCount)
                    {
                        MaxColumnCount = COLLIST.Count;
                    }
                }

                // Return false if we can't find a column count (makes no sense, should not happen)
                if (MaxColumnCount <= 0) return false;

                // If we're good, size up the array
                CSVContent = new string[RowsOfColumns.Count, MaxColumnCount];

                // Loop through and xfer data to array
                for (int i = 0; i <= RowsOfColumns.Count - 1; i++)
                {
                    for (int j = 0; j <= RowsOfColumns[i].Count - 1; j++)
                    {
                        CSVContent[i, j] = RowsOfColumns[i][j];
                    }
                }

                // Finally, replace any nulls for safety for user
                for (int i = 0; i <= CSVContent.GetUpperBound(0); i++)
                {
                    for (int j = 0; j <= CSVContent.GetUpperBound(1); j++)
                    {
                        if (string.IsNullOrEmpty(CSVContent[i, j]) == true)
                        {
                            CSVContent[i, j] = string.Empty;
                        }
                    }
                }

                // Return true if all parsing worked
                return true;
            }
            catch (Exception ex)
            {
                // Return false if error occurs
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Saves the current loaded CSV data to a specified CSV file.
        /// </summary>
        public bool Save(string FullFilePathToCsv)
        {
            try
            {
                // Return false if file path isn't specified
                if (string.IsNullOrEmpty(FullFilePathToCsv) == true) return false;

                // Return false if file path is actually an existing folder (this will fail)
                if (System.IO.Directory.Exists(FullFilePathToCsv) == true) return false;

                // Copy data to save to var so we don't keep rebuilding it with property
                string[,] DataToSave = CSVContentEscaped;

                // Return false if we don't have any data
                if (DataToSave == null) return false;

                // If we have data, create list to hold all lines to write
                var AllLinesToWrite = new List<string>();

                // Pre-size the list so items can be added to it faster
                AllLinesToWrite.Capacity = DataToSave.GetUpperBound(0) + 1;

                // Loop through and join all columns into single lines
                for (int i = 0; i <= DataToSave.GetUpperBound(0); i++)
                {
                    // Create list to hold all column data for joining
                    var ColumnsForRow = new List<string>();

                    // Set the capacity for speed of editing
                    ColumnsForRow.Capacity = DataToSave.GetUpperBound(1) + 1;

                    // Add all columns
                    for (int j = 0; j <= DataToSave.GetUpperBound(1); j++)
                    {
                        ColumnsForRow.Add(DataToSave[i, j]);
                    }

                    // Join info for row into single CSV string
                    AllLinesToWrite.Add(string.Join(",", ColumnsForRow));
                }

                // Once we have all lines built, attempt to write
                System.IO.File.WriteAllLines(FullFilePathToCsv, AllLinesToWrite.ToArray());

                // Return true if file writes
                return true;
            }
            catch (Exception ex)
            {
                // Return false if error occurs
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Returns the current CSV data as a markdown table.
        /// </summary>
        public string ToMarkdownTable(bool DataContainsColumnHeaders)
        {
            try
            {
                // Create array to hold data to write for markdown table
                string[,] DataToWrite = CSVContent;

                // Check if data does NOT have column headers
                if (DataContainsColumnHeaders == false)
                {
                    // If not, resize the array to add a new row for column headers
                    DataToWrite = new string[DataToWrite.GetUpperBound(0) + 1 + 1, DataToWrite.GetUpperBound(1) + 1];

                    // Add generic column headers
                    for (int j = 0; j <= DataToWrite.GetUpperBound(1); j++)
                    {
                        DataToWrite[0, j] = "Column " + (j + 1).ToString();
                    }

                    // Loop through and copy existing data to new array shifted one row down
                    for (int i = 0; i <= CSVContent.GetUpperBound(0); i++)
                    {
                        for (int j = 0; j <= CSVContent.GetUpperBound(1); j++)
                        {
                            DataToWrite[i + 1, j] = CSVContent[i, j];
                        }
                    }
                }

                // Return empty string if there isn't enough info to make table
                if (DataToWrite == null || DataToWrite.GetUpperBound(0) == 0) return string.Empty;

                // If we have all the info we need, create list to hold the max character length in each column
                var MaxCharLengthInEachColumn = new List<int>();

                // Loop through and compute max length
                for (int COL = 0; COL <= DataToWrite.GetUpperBound(1); COL++)
                {
                    // Add place holder for the current column
                    MaxCharLengthInEachColumn.Add(0);

                    // Loop through all rows and get the longest char length string in there
                    for (int ROW = 0; ROW <= DataToWrite.GetUpperBound(0); ROW++)
                    {
                        // Save the string item so we can check its length
                        string Entry = DataToWrite[ROW, COL];

                        // Skip if item is null (i.e. stick with 0 length)
                        if (string.IsNullOrEmpty(Entry) == true)
                        {
                            // Ensure item isn't null before continuing to prevent any null reference errors
                            DataToWrite[ROW, COL] = string.Empty;
                            continue;
                        }

                        // If we get a non-zero entry, save the length
                        int LengthOfEntry = Entry.Length;

                        // Overwrite the info for the max length of this column IF it's bigger than the last max on file
                        if (LengthOfEntry > MaxCharLengthInEachColumn[COL])
                        {
                            MaxCharLengthInEachColumn[COL] = LengthOfEntry;
                        }
                    }
                }

                // Once we have all max lengths, go through and pad each entry in each column to its length
                for (int COL = 0; COL <= DataToWrite.GetUpperBound(1); COL++)
                {
                    for (int ROW = 0; ROW <= DataToWrite.GetUpperBound(0); ROW++)
                    {
                        // Pad each entry to the max length of the column
                        DataToWrite[ROW, COL] = DataToWrite[ROW, COL].PadRight(MaxCharLengthInEachColumn[COL], System.Convert.ToChar(" "));

                        // Add one additional space char before and after entry per the spec (i.e. there's a space between the |'s and the text in the cell)
                        DataToWrite[ROW, COL] = " " + DataToWrite[ROW, COL] + " ";
                    }
                }

                // Now that everything is padded correctly, create a list to hold strings joined together and separated by the "|" char
                var AllJoinedInfoLines = new List<string>();

                // Loop through and join info
                for (int ROW = 0; ROW <= DataToWrite.GetUpperBound(0); ROW++)
                {
                    // Create list to hold all entries for row
                    var EntriesForRow = new List<string>();

                    // Get info from each column
                    for (int COL = 0; COL <= DataToWrite.GetUpperBound(1); COL++)
                    {
                        // Add all info
                        EntriesForRow.Add(DataToWrite[ROW, COL]);
                    }

                    // Join up info and save back to lines (add the post to the beginning and end too)
                    AllJoinedInfoLines.Add("|" + string.Join("|", EntriesForRow) + "|");
                }

                // Finally, we need the divider row between the headers and the data (|---------|-------------------|--------|, etc.), create list to hold minus (-) signs
                var MinusSignBlocks = new List<string>();

                // Loop through each set of columns and get the full padded lengths to make the minus signs
                for (int COL = 0; COL <= DataToWrite.GetUpperBound(1); COL++)
                {
                    // Get the full padded length of the first item (they will have all the same length at this point)
                    int FullPaddedLength = DataToWrite[0, COL].Length;

                    // Create minus signs padded to that length
                    string PaddedMinusSigns = new string(System.Convert.ToChar("-"), FullPaddedLength);

                    // Add padded item to minus blocks
                    MinusSignBlocks.Add(PaddedMinusSigns);
                }

                // Finally, join up the minus blocks and put post on either end to make the divider row
                string DividerRow = "|" + string.Join("|", MinusSignBlocks) + "|";

                // Insert the divider row after the header row
                AllJoinedInfoLines.Insert(1, DividerRow);

                // Join and return info
                return string.Join(Environment.NewLine, AllJoinedInfoLines);
            }
            catch (Exception ex)
            {
                // Return empty string if error occurs
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return string.Empty;
            }
        }


        #endregion

        #region Helper Methods


        private List<string> SplitRowToCsvContent(string RowToSplit)
        {
            // Return single empty item if the row is empty
            if (string.IsNullOrEmpty(RowToSplit) == true) return new[] { string.Empty }.ToList();

            // If we have data, start by splitting by comma only
            var InitialSplitByCommaOnly = RowToSplit.Split(new[] { "," }, StringSplitOptions.None).ToList();

            // CSV spec will have items with comma char enclosed in single quote (") blocks, and user entered quotes will be escaped to double quote ("")
            const string UserEnteredEscapedQuote = "\"" + "\"";
            const string EscapedQuoteNextToStartOrEndBlock = "\"" + UserEnteredEscapedQuote;

            // Create replacement chars for triple quote (i.e. entry that started / stopped with user entered " and had a comma in it which added enclosing quotes)
            const string ReplacementForDoubleQuote = "-!{#2X#~|~#QUOTE#}!-";
            const string ReplacementForTripleQuote = "-!{#3X#~|~#QUOTE#}!-";

            // For through all entries and remove 3x and 2x quotes so we should be left with only single / enclosing block quotes
            for (int i = 0; i <= InitialSplitByCommaOnly.Count - 1; i++)
            {
                // Remove nulls first to prevent any null string parsing errors
                if (string.IsNullOrEmpty(InitialSplitByCommaOnly[i]) == true)
                {
                    InitialSplitByCommaOnly[i] = string.Empty;
                }

                // Do triple fix first as double quotes are a substring of triple
                InitialSplitByCommaOnly[i] = InitialSplitByCommaOnly[i].Replace(EscapedQuoteNextToStartOrEndBlock, ReplacementForTripleQuote);
                InitialSplitByCommaOnly[i] = InitialSplitByCommaOnly[i].Replace(UserEnteredEscapedQuote, ReplacementForDoubleQuote);
            }

            // Now, create a list to hold the true splits (i.e. some items might have to be joined back together if they had commas in them and were in enclosing quote (") blocks)
            var TrueSplitItems = new List<string>();

            // Loop through each item and check if it has a single quote / enclosing block
            for (int i = 0; i <= InitialSplitByCommaOnly.Count - 1; i++)
            {
                // Save the current item
                string CurrentItem = InitialSplitByCommaOnly[i];

                // If item doesn't have any quotes, it's legit on its own (triple quote indicates it is part of an enclosing block AND has user entered quotes)
                if (CurrentItem.Contains("\"") == false && CurrentItem.Contains(ReplacementForTripleQuote) == false)
                {
                    TrueSplitItems.Add(CurrentItem);
                    continue;
                }

                // If the current item does have a quote, we'll need to join some number of items back together, create list to hold info
                var ItemsToMergeBackTogetherAsSingleItem = new List<string>();

                // Always add / start with the current item
                ItemsToMergeBackTogetherAsSingleItem.Add(CurrentItem);

                // Loop through remaining stuff looking for closing " block
                for (int j = i + 1; j <= InitialSplitByCommaOnly.Count - 1; j++)
                {
                    // Save the next item for checking
                    string NextItem = InitialSplitByCommaOnly[j];

                    // Add the item to the list that needs to be merged back together
                    ItemsToMergeBackTogetherAsSingleItem.Add(NextItem);

                    // Stop checking if we find a closing block 
                    if (NextItem.Contains("\"") == true || NextItem.Contains(ReplacementForTripleQuote) == true)
                    {
                        // Have i-loop pick up where we left off here
                        i = j;
                        break;
                    }
                }

                // Join the items we found back together as a single item
                TrueSplitItems.Add(string.Join(",", ItemsToMergeBackTogetherAsSingleItem));
            }

            // Once everything is joined back together, we'll have to go through and format quotes we've fixed / remove to do joining
            for (int i = 0; i <= TrueSplitItems.Count - 1; i++)
            {
                // The single quote enclosing blocks can now be safely removed
                TrueSplitItems[i] = TrueSplitItems[i].Replace("\"", string.Empty);

                // The removed double and triple quote blocks which represented a user specified quote can be put back as a single quote
                TrueSplitItems[i] = TrueSplitItems[i].Replace(ReplacementForTripleQuote, "\"");
                TrueSplitItems[i] = TrueSplitItems[i].Replace(ReplacementForDoubleQuote, "\"");
            }

            // Now, the entire row is split back out, return it
            return TrueSplitItems;
        }

        private string ReadTextFileToString(string FileToRead)
        {
            try
            {
                // Return nothing if input is missing
                if (string.IsNullOrEmpty(FileToRead) == true) return null;

                // Return nothing if file is missing
                if (System.IO.File.Exists(FileToRead) == false) return null;

                // Create var to hold file content
                string FileContent = string.Empty;

                // Read in the file content and save to var
                using (var Reader = new System.IO.StreamReader(System.IO.File.OpenRead(FileToRead)))
                {
                    FileContent = Reader.ReadToEnd();
                }

                // Return the file content
                return FileContent;
            }
            catch (Exception ex)
            {
                // Return nothing if error occurs
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return null;
            }
        }


        #endregion

    }

}
