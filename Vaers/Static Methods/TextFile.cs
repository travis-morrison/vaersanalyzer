﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StaticMethods
{

    public partial struct TextFile
    {

        /// <summary>
        /// Helper class to read a file into a list of bytes.
        /// </summary>
        private class ReadToBytesHelper
        {
            // Var to hold the list of bytes
            private List<byte[]> ListOfBytes = new List<byte[]>();

            // Method to read through all lines and convert to bytes
            public List<byte[]> ReadFileIntoByteList(string FileToRead)
            {
                // Try to read file into byte list
                if (ProcessLineByLine(FileToRead, HandleLineRead) == true)
                {
                    // If file is read in, return the populate list
                    return ListOfBytes;
                }
                else
                {
                    // If an error occurs reading file, return nothing
                    return null;
                }
            }
            private void HandleLineRead(string LineText)
            {
                // Convert line to byte array and add to list
                var TextBytes = System.Text.Encoding.ASCII.GetBytes(LineText);
                ListOfBytes.Add(TextBytes);
            }
        }

        /// <summary>
        /// Used to read a text file into a list of byte arrays (one byte array of ASCII characters for each line).
        /// </summary>
        /// <param name="FileToRead">Full string path of the text file to read in.</param>
        /// <returns>Returns a list of byte arrays containing the contents of the file if the file is processed successfully. Returns NOTHING if an error occurs during processing.</returns>
        public static List<byte[]> ReadToByteList(string FileToRead)
        {
            try
            {
                // Create class helper to process file
                return new ReadToBytesHelper().ReadFileIntoByteList(FileToRead);
            }
            catch (Exception ex)
            {
                // Return nothing if an error occurs
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Used read a text file one line at a time (to prevent OutOfMemory exceptions for large files) and pass each line from the file to a user specified call back method.
        /// </summary>
        /// <param name="FullFilePathToProcess">Full string path of the text file to read.</param>
        /// <param name="MethodToReturnEachLineToOnRead">The method to report each line of the text file to as the file is read.</param>
        /// <param name="LineIndexToStartAt">OPTIONAL:  The zero based line index of the file to start at (i.e. a value of 3 would ignore lines 0 through 2).</param>
        /// <param name="MaximumLinesToProcess">OPTIONAL:  The maximum number of lines that the method should process before exiting (default is Integer.MaxValue).</param>
        /// <returns>Returns TRUE if all requested lines are process and reported to the call back method. Returns FALSE in all other conditions.</returns>
        public static bool ProcessLineByLine(string FullFilePathToProcess, Action<string> MethodToReturnEachLineToOnRead, int LineIndexToStartAt = 0, int MaximumLinesToProcess = int.MaxValue)
        {
            try
            {
                // Return false if no method to call was passed in
                if (MethodToReturnEachLineToOnRead == null) return false;

                // Return false if max lines to process is invalid
                if (MaximumLinesToProcess <= 0) return false;

                // Fix min line index if invalid
                if (LineIndexToStartAt < 0) LineIndexToStartAt = 0;

                // Create vars to hold the current line index and the number of lines processed so far
                int CurrentLineIndex = -1;
                int LinesProcessed = 0;

                // Create stream reader to read in lines one at a time (vs. reading in entire file)
                using (var READER = new System.IO.StreamReader(System.IO.File.OpenRead(FullFilePathToProcess)))
                {
                    // Read until end of file OR the maximum number of requested lines has been processed
                    while (READER.EndOfStream == false && LinesProcessed < MaximumLinesToProcess)
                    {
                        // Inc the line index pointer
                        CurrentLineIndex += 1;

                        // Read the next line
                        string NextLine = READER.ReadLine();

                        // Ensure nulls are always empty strings
                        if (string.IsNullOrEmpty(NextLine) == true) NextLine = string.Empty;

                        // Skip if this index occurs before the requested start index
                        if (CurrentLineIndex < LineIndexToStartAt) continue;

                        // Return line to callback
                        MethodToReturnEachLineToOnRead(NextLine);

                        // Inc the number of lines processed so far
                        LinesProcessed += 1;
                    }
                }

                // Return true if all lines process
                return true;
            }
            catch (Exception ex)
            {
                // Return false if error occurs
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Used to read a subset of a file based on a start and end line index (0-based).
        /// </summary>
        /// <param name="FileToRead">Full string path of the text file to read in.</param>
        /// <param name="StartLineIndex">The 0-based line index / number to start reading on (inclusive).</param>
        /// <param name="EndLineIndex">The 0-based line index / number to stop reading on (inclusive).</param>
        /// <param name="TrimLinesBeforeReturning">OPTIONAL:  Flag to indicate if the line should be trimmed before it is added to the list that is returned (i.e. should "  Some text  " be trimmed to "Some text", etc.).</param>
        /// <returns>Returns a list of strings (one element for each line of the text file within the specified range) if the file is read correctly. Returns NOTHING if the read fails.</returns>
        public static List<string> Read(string FileToRead, int StartLineIndex, int EndLineIndex, bool TrimLinesBeforeReturning = false)
        {
            try
            {
                // Return nothing if file is bad
                if (string.IsNullOrEmpty(FileToRead) == true || System.IO.File.Exists(FileToRead) == false) return null;

                // Return nothing if line indexes are bad
                if (StartLineIndex < 0 || EndLineIndex < 0) return null;

                // Create vars to hold the start and end indexes to use (in case user flip-flopped them)
                int StartLine = StartLineIndex;
                int StopLine = EndLineIndex;

                // Fix if flipped
                if (StartLine > StopLine)
                {
                    StartLine = EndLineIndex;
                    StopLine = StartLineIndex;
                }

                // Create var to hold current line of file
                int CurrentLine = 0;

                // Create list to hold lines to return
                var LinesToReturn = new List<string>();

                // Create reader to process file
                using (var READER = new System.IO.StreamReader(System.IO.File.OpenRead(FileToRead)))
                {
                    // Keep reading till end of file
                    while (READER.EndOfStream == false)
                    {
                        // Read the current line
                        string LineText = READER.ReadLine();

                        // Trim if requested
                        if (TrimLinesBeforeReturning == true)
                        {
                            LineText = LineText.Trim();
                        }

                        // Add the line to the list if it's in range
                        if (CurrentLine >= StartLine && CurrentLine <= StopLine)
                        {
                            LinesToReturn.Add(LineText);
                        }

                        // Inc the counter for next line
                        CurrentLine += 1;

                        // If we're beyond the required stop line, just return everything
                        if (CurrentLine > StopLine)
                        {
                            return LinesToReturn;
                        }
                    }
                }

                // Once we've processed the whole file / lines required, return the info
                return LinesToReturn;
            }
            catch (Exception ex)
            {
                // Return nothing if error occurs
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Used to read a text file into a 2D array of strings.
        /// </summary>
        /// <param name="FileToRead">Full string path of the text file to read in.</param>
        /// <param name="DelimiterStringToParseEachLineWith">String delimiter to use to parse each line into multiple columns (e.g. CSV file would use ",").</param>
        /// <param name="OmitLastLineIfEmpty">Boolean indicating whether the last line should be ignored if it contains no characters other than a carriage return.</param>
        /// <returns>Returns a 2D array of strings if the file is read correctly and split by the line delimiter successfully. Returns NOTHING if the read fails.</returns>
        public static string[,] Read(string FileToRead, string DelimiterStringToParseEachLineWith, bool OmitLastLineIfEmpty)
        {
            try
            {
                // First, try to read in all lines to a 1D array
                var AllLines = Read(FileToRead, OmitLastLineIfEmpty);

                // Return nothing if nothing came back
                if (AllLines == null) return null;

                // If data came back, create var to hold how many columns are needed
                int ColMaxIndex = int.MinValue;

                // Loop through all lines and check the max split length
                foreach (string LINE in AllLines)
                {
                    // Get the line split count
                    int SplitCount = LINE.Split(new[] { DelimiterStringToParseEachLineWith }, StringSplitOptions.None).Count();

                    // If this line split has more terms than the most on file so far, update the max col index
                    if ((SplitCount - 1) > ColMaxIndex)
                    {
                        ColMaxIndex = (SplitCount - 1);
                    }
                }

                // Once loop is done, if max col index wasn't found, return nothing
                if (ColMaxIndex < 0) return null;

                // If there is a max index, create new 2D array to hold everything
                string[,] ArrayToReturn = new string[AllLines.GetUpperBound(0) + 1, ColMaxIndex + 1];

                // Set all items to string.empty so there won't be any nothing's if some rows have more columns then others
                for (int i = 0; i <= ArrayToReturn.GetUpperBound(0); i++)
                {
                    for (int j = 0; j <= ArrayToReturn.GetUpperBound(1); j++)
                    {
                        ArrayToReturn[i, j] = string.Empty;
                    }
                }

                // Loop through and xfer data
                for (int i = 0; i <= AllLines.GetUpperBound(0); i++)
                {
                    // Split the line
                    string[] SplitLine = AllLines[i].Split(new[] { DelimiterStringToParseEachLineWith }, StringSplitOptions.None);

                    // Xfer over all items
                    for (int j = 0; j <= SplitLine.GetUpperBound(0); j++)
                    {
                        ArrayToReturn[i, j] = SplitLine[j];
                    }
                }

                // Once all items are xfered, return the array
                return ArrayToReturn;
            }
            catch (Exception ex)
            {
                // If an error occurs, return nothing
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return null;
            }
        }
        /// <summary>
        /// Used to read a text file into a 1D array of strings.
        /// </summary>
        /// <param name="FileToRead">Full string path of the text file to read in.</param>
        /// <param name="OmitLastLineIfEmpty">Boolean indicating whether the last line should be ignored if it contains no characters other than a carriage return.</param>
        /// <returns>Returns a 1D array of strings (one element for each line of the text file) if the file is read correctly. Returns NOTHING if the read fails.</returns>
        public static string[] Read(string FileToRead, bool OmitLastLineIfEmpty)
        {
            try
            {
                // Attempt to read all file text
                string AllFileText = Read(FileToRead);

                // Return nothing if file is missing / error occurs
                if (AllFileText == null) return null;

                // Parse the full file contents by CrLf (Windows), Cr (CrApple), Lf (Linux)
                var AllLines = AllFileText.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);

                // Check if there are multiple lines AND if the last line is empty
                if (OmitLastLineIfEmpty == true && AllLines.Count() > 1 && string.IsNullOrEmpty(AllLines[AllLines.Count() - 1]) == true)
                {
                    // If so, create a copy of the array with everything
                    var CopyOfFullArray = AllLines;

                    // Resize all lines that will be returned to hack off a line
                    AllLines = new string[AllLines.Count() - 1];

                    // Copy the data to the array being returned with everything except the last line
                    Array.Copy(CopyOfFullArray, AllLines, AllLines.GetUpperBound(0) + 1);
                }

                // Return the array
                return AllLines;
            }
            catch (Exception ex)
            {
                // If an error occurs, return nothing
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return null;
            }
        }
        /// <summary>
        /// Used to read a text file and return the content as a single string.
        /// </summary>
        /// <param name="FileToRead">Full string path of the text file to read in.</param>
        /// <returns>Returns the file content if read successfully. Returns NOTHING if an error occurs reading the file.</returns>
        public static string Read(string FileToRead)
        {
            try
            {
                // Return nothing if file is missing
                if (System.IO.File.Exists(FileToRead) == false) return null;

                // Create var to hold file content
                string FileContent = string.Empty;

                // Read in the file content and save to var
                using (var READER = new System.IO.StreamReader(System.IO.File.OpenRead(FileToRead)))
                {
                    FileContent = READER.ReadToEnd();
                }

                // Return the file content
                return FileContent;
            }
            catch (Exception ex)
            {
                // Return nothing if error occurs
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Used to read a text file into a 2D array of strings.
        /// </summary>
        /// <param name="FileToRead">Full string path of the text file to read in.</param>
        /// <param name="refTwoDimentionalArrayToWriteFileTo">BYREF:  2D array of strings to return with the text file contents (each line is a row in the array).</param>
        /// <param name="DelimiterStringToParseEachLineWith">String delimiter to use to parse each line into multiple columns (e.g. CSV file would use ",").</param>
        /// <param name="OmitLastLineIfEmpty">Boolean indicating whether the last line should be ignored if it contains no characters other than a carriage return.</param>
        /// <returns>Returns TRUE if the file contents are saved to the array. Returns FALSE in all other conditions.</returns>
        public static bool Read(string FileToRead, ref string[,] refTwoDimentionalArrayToWriteFileTo, string DelimiterStringToParseEachLineWith, bool OmitLastLineIfEmpty)
        {
            try
            {
                // Create a 1D array for holding file contents and figuring out how many columns to make
                string[] OneDimentionalArrayToSaveFileTo = new string[1];

                // Return false if file can't be read
                if (Read(FileToRead, ref OneDimentionalArrayToSaveFileTo, OmitLastLineIfEmpty) == false) return false;

                // If read worked, create array for holding the split line
                string[] CurrentSplitLineArray = null;

                // Create var to hold total columns needed
                int ColumnsNeeded = 0;

                // Go through each line and figure out how many columns are needed for final 2D array
                for (int i = 0; i <= OneDimentionalArrayToSaveFileTo.GetUpperBound(0); i++)
                {
                    // Check if the current row's column count is larger than the current max column count var
                    CurrentSplitLineArray = OneDimentionalArrayToSaveFileTo[i].Split(new[] { DelimiterStringToParseEachLineWith }, StringSplitOptions.None);
                    if (CurrentSplitLineArray.Length - 1 > ColumnsNeeded)
                    {
                        // If so, save the new upper bound
                        ColumnsNeeded = CurrentSplitLineArray.Length - 1;
                    }
                }

                // Resize ByRef array to new dimensions using the max column size
                refTwoDimentionalArrayToWriteFileTo = new string[OneDimentionalArrayToSaveFileTo.GetUpperBound(0) + 1, ColumnsNeeded + 1];

                // Set all items to string.empty so there won't be any nothing's if some rows have more columns then others
                for (int i = 0; i <= refTwoDimentionalArrayToWriteFileTo.GetUpperBound(0); i++)
                {
                    for (int j = 0; j <= refTwoDimentionalArrayToWriteFileTo.GetUpperBound(1); j++)
                    {
                        refTwoDimentionalArrayToWriteFileTo[i, j] = string.Empty;
                    }
                }

                // Parse data into the 2D array
                for (int i = 0; i <= OneDimentionalArrayToSaveFileTo.GetUpperBound(0); i++)
                {
                    // Split the current line by the passed in delimiter
                    CurrentSplitLineArray = OneDimentionalArrayToSaveFileTo[i].Split(new[] { DelimiterStringToParseEachLineWith }, StringSplitOptions.None);

                    // Loop through the current line and add each split term as a column in the 2D array
                    for (int j = 0; j <= CurrentSplitLineArray.GetUpperBound(0); j++)
                    {
                        refTwoDimentionalArrayToWriteFileTo[i, j] = CurrentSplitLineArray[j];
                    }
                }

                // Return true if everything works
                return true;
            }
            catch (Exception ex)
            {
                // Return false on failure
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return false;
            }
        }
        /// <summary>
        /// Used to read a text file into a 1D array of strings.
        /// </summary>
        /// <param name="FileToRead">Full string path of the text file to read in.</param>
        /// <param name="refOneDimentionalArrayToWriteFileTo">BYREF:  1D array of strings to return with the text file contents (each line is an item in the array).</param>
        /// <param name="OmitLastLineIfEmpty">Boolean indicating whether the last line should be ignored if it contains no characters other than a carriage return.</param>
        /// <returns>Returns TRUE if the file contents are saved to the array. Returns FALSE in all other conditions.</returns>
        public static bool Read(string FileToRead, ref string[] refOneDimentionalArrayToWriteFileTo, bool OmitLastLineIfEmpty)
        {
            try
            {
                // Clear the previous contents of the inputed array
                refOneDimentionalArrayToWriteFileTo = new string[1];

                // Read in file content
                string FileContent = Read(FileToRead);

                // Return false if file can't be read
                if (FileContent == null) return false;

                // Parse the full file contents by CrLf (Windows), Cr (CrApple), Lf (Linux)
                refOneDimentionalArrayToWriteFileTo = FileContent.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);

                // Check if there is more than one line in the file
                if (refOneDimentionalArrayToWriteFileTo.GetUpperBound(0) > 0)
                {
                    // If so, check if user requested that last line be removed if empty
                    if (OmitLastLineIfEmpty == true)
                    {
                        // If they did, check if the last line is empty
                        if (string.IsNullOrEmpty(refOneDimentionalArrayToWriteFileTo[refOneDimentionalArrayToWriteFileTo.GetUpperBound(0)]) == true)
                        {
                            // Create a copy of the original array for resizing
                            var CopyOfOriginalArray = refOneDimentionalArrayToWriteFileTo;

                            // Resize the array that will be getting returned
                            refOneDimentionalArrayToWriteFileTo = new string[refOneDimentionalArrayToWriteFileTo.GetUpperBound(0) - 1 + 1];

                            // If the last line is "" / nothing, remove it
                            Array.Copy(CopyOfOriginalArray, refOneDimentionalArrayToWriteFileTo, refOneDimentionalArrayToWriteFileTo.GetUpperBound(0) + 1);
                        }
                    }
                }

                // Return true if nothing fails
                return true;
            }
            catch (Exception ex)
            {
                // Return false on failure
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Used to write a 2D array of strings to a text file.
        /// </summary>
        /// <param name="FileToWrite">Full string path of the file to write.</param>
        /// <param name="TwoDimensionalArrayToWrite">2D string array containing the data to write (each row of the array will be a new line in the file and each column will be delimited by the "DelimiterStringToUse").</param>
        /// <param name="DelimiterStringToUse">String delimiter to use to place in between concat'd columns in the text file.</param>
        /// <param name="AppendToExistingFileIfItExists">Boolean used to indicate whether a array data should be added to an existing text file (if it exists) or if it should overwrite the existing file (if it exists).</param>
        /// <returns>Returns TRUE if the file is written. Returns FALSE in all other conditions.</returns>
        public static bool Write(string FileToWrite, string[,] TwoDimensionalArrayToWrite, string DelimiterStringToUse, bool AppendToExistingFileIfItExists)
        {
            try
            {
                // Convert the 2D array into a 1D array separated by the delimiter
                var MergedArray = new string[TwoDimensionalArrayToWrite.GetUpperBound(0) + 1];
                for (int i = 0; i <= TwoDimensionalArrayToWrite.GetUpperBound(0); i++)
                {
                    // Add each term of the 2D array to the 1D array with the specified delimiter as term spacer
                    for (int j = 0; j <= TwoDimensionalArrayToWrite.GetUpperBound(1); j++)
                    {
                        MergedArray[i] += TwoDimensionalArrayToWrite[i, j] + DelimiterStringToUse;
                    }

                    // Need to hack off the last delimiter as it is extra
                    MergedArray[i] = MergedArray[i].Substring(0, MergedArray[i].Length - DelimiterStringToUse.Length);
                }

                // Once the 1D array is made, call the 1D WriteToTextFile function and return whether it passes or fails
                return Write(FileToWrite, MergedArray, AppendToExistingFileIfItExists);
            }
            catch (Exception ex)
            {
                // Return false if error occurs
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return false;
            }
        }
        /// <summary>
        /// Used to write a 1D array of strings to a text file.
        /// </summary>
        /// <param name="FileToWrite">Full string path of the file to write.</param>
        /// <param name="OneDimensionalArrayToWrite">1D string array containing the data to write (each item of the array will be a new line in the file).</param>
        /// <param name="AppendToExistingFileIfItExists">Boolean used to indicate whether a array data should be added to an existing text file (if it exists) or if it should overwrite the existing file (if it exists).</param>
        /// <returns>Returns TRUE if the file is written. Returns FALSE in all other conditions.</returns>
        public static bool Write(string FileToWrite, string[] OneDimensionalArrayToWrite, bool AppendToExistingFileIfItExists)
        {
            try
            {
                // Create the writer
                using (var WRITER = new System.IO.StreamWriter(FileToWrite, AppendToExistingFileIfItExists))
                {
                    // Iterate through and write each line (DON'T join and use single string write method the join operation is much slower)
                    for (int i = 0; i <= OneDimensionalArrayToWrite.Length - 1; i++)
                    {
                        WRITER.WriteLine(OneDimensionalArrayToWrite[i]);
                    }
                }

                // Return true if nothing fails
                return true;
            }
            catch (Exception ex)
            {
                // Return false if error occurs
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return false;
            }
        }
        /// <summary>
        /// Used to write a single string to a text file.
        /// </summary>
        /// <param name="FileToWrite">Full string path of the file to write.</param>
        /// <param name="StringToWrite">The string to write to the file.</param>
        /// <param name="AppendToExistingFileIfItExists"></param>
        /// <returns>Returns TRUE if the file is written. Returns FALSE in all other conditions.</returns>
        public static bool Write(string FileToWrite, string StringToWrite, bool AppendToExistingFileIfItExists)
        {
            try
            {
                // Create the writer
                using (var WRITER = new System.IO.StreamWriter(FileToWrite, AppendToExistingFileIfItExists))
                {
                    // Write the text
                    WRITER.Write(StringToWrite);
                }

                // Return true if nothing fails
                return true;
            }
            catch (Exception ex)
            {
                // Return false if error occurs
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Used to write exception info from a try/catch block to a specified text file error log.
        /// </summary>
        /// <param name="ErrorLogFilePath">The full file path to an error log text file.</param>
        /// <param name="ErrorInfo">The exception info (typically caught in the try/catch block).</param>
        /// <returns>Returns TRUE if the error info is successfully written to the specified file path. Returns FALSE in all other conditions.</returns>
        public static bool WriteToErrorLog(string ErrorLogFilePath, Exception ErrorInfo)
        {
            try
            {
                // Return false if info is missing
                if (ErrorInfo == null) return false;

                // Save the failure msg
                string FailureMsg = ErrorInfo.Message;

                // Try and separate the stack trace into something more readable
                var SplitStackTrace = ErrorInfo.StackTrace.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);

                // Clean up each line
                if (SplitStackTrace != null && SplitStackTrace.Length > 0)
                {
                    for (int i = 0; i <= SplitStackTrace.GetUpperBound(0); i++)
                    {
                        SplitStackTrace[i] = "  > " + SplitStackTrace[i].Trim();
                    }
                }

                // Save the full stack trace w/ clean formatting (standard line endings and indents)
                string FullStackTrace = string.Join(Environment.NewLine, SplitStackTrace);

                // Save header
                string MsgToLog = "EXCEPTION:  " + FailureMsg.ToUpper() + Environment.NewLine + "THROWN AT:  " + DateTime.Now.ToString();

                // Add divider line
                MsgToLog += Environment.NewLine + new string(Convert.ToChar("="), 150) + Environment.NewLine;

                // Add stack trace
                MsgToLog += FullStackTrace;

                // Add some divides for spacers between exception messages
                MsgToLog += Environment.NewLine + Environment.NewLine + "  -----" + Environment.NewLine + Environment.NewLine;

                // Write the message and return the result
                return Write(ErrorLogFilePath, MsgToLog, true);
            }
            catch (Exception ex)
            {
                // Return false if error occurs
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Used to iterate through and file, count how many lines it contains, and return the result to a call back method.
        /// </summary>
        /// <param name="FullFilePathToProcess">The full file path of the text file to count the lines in.</param>
        /// <param name="MethodToReturnCountTo">The method to pass the line count to.</param>
        public static void GetLineCountAsync(string FullFilePathToProcess, Action<Nullable<int>> MethodToReturnCountTo)
        {
            // Launch thread to use common method with specified parameters
            System.Threading.Thread thrGetCount = new System.Threading.Thread(() => GetLineCount(FullFilePathToProcess, MethodToReturnCountTo));
            thrGetCount.SetApartmentState(System.Threading.ApartmentState.STA);
            thrGetCount.IsBackground = true;
            thrGetCount.Start();
        }
        /// <summary>
        /// Used to iterate through and file and count how many lines it contains.
        /// </summary>
        /// <param name="FullFilePathToProcess">The full file path of the text file to count the lines in.</param>
        /// <returns>Returns the number of lines in the file if the lines were all counted successfully. Returns NOTHING if an error occurred while attempting to determine the line count.</returns>
        public static Nullable<int> GetLineCount(string FullFilePathToProcess)
        {
            // Use common method with no call back
            return GetLineCount(FullFilePathToProcess, null);
        }
        /// <summary>
        /// Internal method to get the line count and return it.
        /// </summary>
        private static Nullable<int> GetLineCount(string FullFilePathToProcess, Action<Nullable<int>> MethodToReturnCountTo)
        {
            // Create a var to hold the total line count
            Nullable<int> LineCount = 0;

            try
            {
                // Create stream reader to read in lines one at a time (vs. reading in entire file)
                using (var READER = new System.IO.StreamReader(System.IO.File.OpenRead(FullFilePathToProcess)))
                {
                    // Read each line
                    while (READER.EndOfStream == false)
                    {
                        // Read the next line and inc the counter
                        READER.ReadLine();
                        LineCount += 1;
                    }
                }
            }
            catch (Exception ex)
            {
                // Clear count if error occurs
                System.Diagnostics.Trace.WriteLine(ex.Message);
                LineCount = null;
            }

            try
            {
                // Check if there is a call back method
                if (MethodToReturnCountTo != null)
                {
                    // If not invoking main, just call the method and pass the result
                    MethodToReturnCountTo(LineCount);
                }
            }
            catch (Exception)
            {
                // No action needed on failure
            }

            // Return the result (will be null if error occurred)
            return LineCount;
        }

        /// <summary>
        /// Used to find the 0-based line location(s) of a specified search word in a text file.
        /// </summary>
        /// <param name="FileToRead">Full string path of the text file to search through.</param>
        /// <param name="ItemToFind">The search string to find in the file.</param>
        /// <param name="MatchExact">Flag to indicate if the line should be checked using equals or contains (note that the line is always trimmed first regardless of selection).</param>
        /// <returns>Returns a list of KVPs showing the criterion found and the 0-based line number it was found on if successful. Returns NOTHING in all other conditions.</returns>
        public static List<KeyValuePair<string, int>> FindLineLocations(string FileToRead, string ItemToFind, bool MatchExact)
        {
            // Use helper to return result
            return FindLineLocations(FileToRead, new[] { ItemToFind }, MatchExact);
        }
        /// <summary>
        /// Used to find the 0-based line location(s) of a specified set of search words in a text file.
        /// </summary>
        /// <param name="FileToRead">Full string path of the text file to search through.</param>
        /// <param name="ItemsToFind">The list of items to find in the file.</param>
        /// <param name="MatchExact">Flag to indicate if the line should be checked using equals or contains (note that the line is always trimmed first regardless of selection).</param>
        /// <returns>Returns a list of KVPs showing the criterion found and the 0-based line number it was found on if successful. Returns NOTHING in all other conditions.</returns>
        public static List<KeyValuePair<string, int>> FindLineLocations(string FileToRead, IEnumerable<string> ItemsToFind, bool MatchExact)
        {
            try
            {
                // Return nothing if file is bad
                if (string.IsNullOrEmpty(FileToRead) == true || System.IO.File.Exists(FileToRead) == false) return null;

                // Return nothing if there are no items to find
                if (ItemsToFind == null || ItemsToFind.Count() == 0) return null;

                // Convert items to find to list for ease of processing
                var Criteria = ItemsToFind.ToList();

                // Create list to hold KVPs to return
                var MatchesToReturn = new List<KeyValuePair<string, int>>();

                // Create counter to keep track of line number
                int LineNumber = 0;

                // If everything looks good, create stream reader to process file
                using (var READER = new System.IO.StreamReader(System.IO.File.OpenRead(FileToRead)))
                {
                    // Keep reading till end of file
                    while (READER.EndOfStream == false)
                    {
                        // Save the current line
                        string CurrentLine = READER.ReadLine().Trim();

                        // Loop through all criteria and check for match
                        foreach (var CRITERION in Criteria)
                        {
                            // Check for match based on match type
                            switch (MatchExact)
                            {
                                case true:
                                    // Skip to next criterion if matching exact and line isn't perfect
                                    if (CurrentLine == CRITERION == false) continue;
                                    break;
                                case false:
                                    // Skip to next criterion if doing partial match and contains is false
                                    if (CurrentLine.Contains(CRITERION) == false) continue;
                                    break;
                            }

                            // If this line appears to be a match, add item to KVP list to mark match
                            MatchesToReturn.Add(new KeyValuePair<string, int>(CRITERION, LineNumber));
                        }

                        // Inc the line counter after each read
                        LineNumber += 1;
                    }
                }

                // After processing file, return the matches
                return MatchesToReturn;
            }
            catch (Exception ex)
            {
                // Return nothing if error occurs
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return null;
            }
        }

        public struct XML
        {

            /// <summary>
            /// Used to read in the contents of a "shallow" XML file containing variable names and values into a list of key value pairs.
            /// </summary>
            /// <param name="FilePath">The full file path (.xml) of the file to read.</param>
            /// <param name="refElementNamesAndValues">BYREF:  List of key value pairs that will be used to return the variable names and corresponding values in the file.</param>
            /// <param name="refReturnedErrMsg">BYREF:  String used to return any error messages that occur while attempting to read the file.</param>
            /// <returns>Returns TRUE if the file is read successfully. Returns FALSE in all other conditions.</returns>
            public static bool Read(string FilePath, ref List<KeyValuePair<string, string>> refElementNamesAndValues, ref string refReturnedErrMsg)
            {
                // Clear collections & error msg var first
                refElementNamesAndValues = new List<KeyValuePair<string, string>>();
                refReturnedErrMsg = string.Empty;

                try
                {
                    // Get all the node names from the file
                    var AllNodeNames = GetNodeNames(FilePath, ref refReturnedErrMsg);

                    // Return false if nothing came back or we got an error
                    if (AllNodeNames == null || refReturnedErrMsg != null) return false;

                    // Check if there were any nodes found
                    if (AllNodeNames.Count == 0)
                    {
                        refReturnedErrMsg = "No XML nodes were found in the selected file.";
                        return false;
                    }

                    // Create an xml doc in memory from the file
                    var XMLDoc = new System.Xml.XmlDocument();
                    XMLDoc.Load(FilePath);

                    // Go through each node name looking for the root node
                    foreach (string NodeName in AllNodeNames)
                    {
                        // Select the node
                        var RootNode = XMLDoc.SelectSingleNode(NodeName);

                        // Check that is was the root node
                        if (RootNode != null)
                        {
                            // If it's the root, check for children
                            if (RootNode.HasChildNodes == true)
                            {
                                // If it has children, iterate through them
                                for (int i = 0; i <= RootNode.ChildNodes.Count - 1; i++)
                                {
                                    // Get the child name and value
                                    string VarName = RootNode.ChildNodes[i].Name;
                                    string VarValue = RootNode.ChildNodes[i].InnerText;

                                    // Add new kvp to the list to return
                                    refElementNamesAndValues.Add(new KeyValuePair<string, string>(VarName, VarValue));
                                }
                            }
                        }
                    }

                    // Return true if read works
                    return true;
                }
                catch (Exception ex)
                {
                    // If error occurs, save msg and return false
                    refReturnedErrMsg = ex.Message;
                    return false;
                }
            }
            private static List<string> GetNodeNames(string XMLFilePath, ref string refReturnedErrMsg)
            {
                // Create a list to return
                var ListToReturn = new List<string>();

                try
                {
                    // Create new reader settings
                    var XMLSettings = new System.Xml.XmlReaderSettings();
                    XMLSettings.IgnoreWhitespace = true;
                    XMLSettings.IgnoreComments = true;

                    // Create new xml reader with specified settings
                    using (var XMLReader = System.Xml.XmlReader.Create(XMLFilePath, XMLSettings))
                    {
                        while (XMLReader.Read())
                        {
                            // If the current node is an element, add it to the list
                            if (XMLReader.NodeType == System.Xml.XmlNodeType.Element)
                            {
                                ListToReturn.Add(XMLReader.Name);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Return any error msgs
                    refReturnedErrMsg = ex.Message;
                    return null;
                }

                // Return the list
                return ListToReturn;
            }

            /// <summary>
            /// Writes a list of variable names and values to an XML file.
            /// </summary>
            /// <param name="FilePathToWrite">The full file path (.xml) for the file to write.</param>
            /// <param name="HeadingName">The outer most / top level node for the XML file (e.g. AppSettings, etc.).</param>
            /// <param name="ElementNamesAndValues">The list of key value pairs to write in the XML file.</param>
            /// <param name="refReturnedErrMsg">BYREF:  String used to return any error messages that occur while attempting to write the file.</param>
            /// <returns>Returns TRUE if the file writes successfully. Returns FALSE in all other conditions.</returns>
            public static bool WriteToFile(string FilePathToWrite, string HeadingName, List<KeyValuePair<string, string>> ElementNamesAndValues, ref string refReturnedErrMsg)
            {
                try
                {
                    // Clear the error msg var in case it was not empty
                    refReturnedErrMsg = string.Empty;

                    // Check for items
                    if (ElementNamesAndValues == null || ElementNamesAndValues.Count == 0)
                    {
                        // If no items, pass back error msg
                        refReturnedErrMsg = "No items / values to write have been specified.";
                        return false;
                    }

                    // Setup XML writer
                    var XMLSettings = new System.Xml.XmlWriterSettings();
                    XMLSettings.NewLineOnAttributes = true;
                    XMLSettings.Indent = true;

                    // Create new xml writer w/ specified settings
                    using (var XMLWriter = System.Xml.XmlWriter.Create(FilePathToWrite, XMLSettings))
                    {
                        // Write xml header
                        XMLWriter.WriteStartDocument();

                        // Write outer element header
                        XMLWriter.WriteStartElement(HeadingName.Replace(" ", string.Empty));

                        // Loop through and add items
                        foreach (var PAIR in ElementNamesAndValues)
                        {
                            XMLWriter.WriteElementString(PAIR.Key.Replace(" ", string.Empty), PAIR.Value);
                        }

                        // Close the outer element header
                        XMLWriter.WriteEndElement();

                        // Write the end of the document and close out the writer
                        XMLWriter.WriteEndDocument();
                        XMLWriter.Flush();
                    }

                    // Return true if write works
                    return true;
                }
                catch (Exception ex)
                {
                    // Save error msg and return false if something fails
                    refReturnedErrMsg = ex.Message;
                    return false;
                }
            }

        }

        private struct Helpers
        {

            /// <summary>
            /// Helper for checking if an object can be converted to a number (double).
            /// </summary>
            public static bool IsNumeric(object ObjectToCheck)
            {
                try
                {
                    // Return false if nothing is set
                    if (ObjectToCheck == null) return false;

                    // Convert object to string for checking
                    string StringValue = ObjectToCheck.ToString();

                    // Return false if string is bad / empty
                    if (string.IsNullOrEmpty(StringValue) == true) return false;

                    // Create dummy var to hold result of conversion
                    double DummyVar = 0;

                    // If string looks good, attempt to parse it
                    return double.TryParse(StringValue, out DummyVar);
                }
                catch (Exception ex)
                {
                    // Return false if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }

        }

    }

}
