﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StaticMethods
{

    public partial struct FileSystem
    {

        #region File / Folder Path Methods


        /// <summary>
        /// Used to check if a specified folder path exists with a specified timeout to prevent hanging / long delays if checking disconnected network paths.
        /// </summary>
        /// <param name="FullFolderPath">The full path to check for.</param>
        /// <param name="TimeoutInMS">Length of time before timeout occurs and the method defaults to returning false if the path check hangs.</param>
        /// <returns>Returns TRUE if the path is found. Returns FALSE if the path isn't found OR a timeout / exception occurs.</returns>
        public static bool DoesFolderExists(string FullFolderPath, UInt16 TimeoutInMS)
        {
            // Use helper and return result
            return DoesPathExist(FullFolderPath, true, TimeoutInMS);
        }
        /// <summary>
        /// Used to check if a specified file path exists with a specified timeout to prevent hanging / long delays if checking disconnected network paths.
        /// </summary>
        /// <param name="FullFilePath">The full path to check for.</param>
        /// <param name="TimeoutInMS">Length of time before timeout occurs and the method defaults to returning false if the path check hangs.</param>
        /// <returns>Returns TRUE if the path is found. Returns FALSE if the path isn't found OR a timeout / exception occurs.</returns>
        public static bool DoesFileExists(string FullFilePath, UInt16 TimeoutInMS)
        {
            // Use helper and return result
            return DoesPathExist(FullFilePath, false, TimeoutInMS);
        }

        /// <summary>
        /// Used to get the root / install directory of the running application.
        /// </summary>
        /// <returns>Returns the file path of the directory where the application is currently running from. Returns nothing if the running directory cannot be determined.</returns>
        public static string GetRootFolderOfExecutingAssembly()
        {
            try
            {
                // Try to use reflection to figure out where app is running from
                return System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            }
            catch (Exception)
            {
                // No action needed on failure
            }

            try
            {
                // If reflection doesn't work, try using startup info from environment (first cmd line arg is normally the path to the running process)
                return System.IO.Path.GetDirectoryName(Environment.GetCommandLineArgs()[0]);
            }
            catch (Exception)
            {
                // No action needed on failure
            }

            // If nothing works, return empty string
            return string.Empty;
        }

        /// <summary>
        /// Used to return the user's local app data folder (typically C:\'Username'\AppData\Local).
        /// </summary>
        /// <returns>Returns the full folder path to the user's local app data folder if found. Returns an empty string if the folder cannot be found.</returns>
        public static string GetLocalAppDataFolderPath()
        {
            try
            {
                // Attempt to return the folder path for local app data
                return Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            }
            catch (Exception ex)
            {
                // Return empty string if error occurs
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return string.Empty;
            }
        }

        /// <summary>
        /// Used to set the active / current directory to whatever directory the application is running out of.
        /// </summary>
        /// <returns>Returns TRUE if set successfully. Returns FALSE in all other conditions.</returns>
        public static bool SetCurrentDirectoryToRootOfExecutingAssembly()
        {
            try
            {
                // Get the directory we're running out of
                string AppsRunningDirectory = GetRootFolderOfExecutingAssembly();
                if (string.IsNullOrEmpty(AppsRunningDirectory) == true)
                {
                    // Return false if we can't get it
                    return false;
                }

                // Attempt to set the active directory to where ever we're running out of
                System.IO.Directory.SetCurrentDirectory(AppsRunningDirectory);

                // Return true if it works
                return true;
            }
            catch (Exception ex)
            {
                // Return false if error occurs
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Used to generate a new unique temp folder sub-directory within the user's / system's root temp directory.
        /// </summary>
        /// <param name="CreateFolder">Flag to indicate if the folder should also be created once the path has been generated.</param>
        /// <returns>Returns the new temp folder path if it is generated successfully. Returns an empty string if the path is not generated OR if it is generated but the folder could not be created upon request.</returns>
        public static string GetNewUniqueTempFolderPath(bool CreateFolder)
        {
            try
            {
                // Create var to hold unique temp folder path
                string TempFolderPath = string.Empty;

                // Don't try more than 5 times, something would be wrong if it's not working
                for (int i = 1; i <= 5; i++)
                {
                    // Create a temp file path (file gets created as well)
                    string TempFilePath = System.IO.Path.GetTempFileName();

                    // Delete the file right away, it's only used for the generation of the file path
                    System.IO.File.Delete(TempFilePath);

                    // Remove the file extension so it can be used as a folder
                    string PathWithoutExtension = TempFilePath.Replace(System.IO.Path.GetExtension(TempFilePath), string.Empty);

                    // Pause a little and try again until max attempts hit if folder already exists
                    if (System.IO.Directory.Exists(PathWithoutExtension) == true)
                    {
                        System.Threading.Thread.Sleep(50);
                        continue;
                    }

                    // If folder doesn't exist, save it and exit loop
                    TempFolderPath = PathWithoutExtension;
                    break;
                }

                // Return empty string if unique path couldn't be found
                if (string.IsNullOrEmpty(TempFolderPath) == true) return string.Empty;

                // Just return the new path if we don't need to create it
                if (CreateFolder == false) return TempFolderPath;

                // If user wants the dir created, attempt to create it
                System.IO.Directory.CreateDirectory(TempFolderPath);

                // Return path if folder is created successfully
                return TempFolderPath;
            }
            catch (Exception ex)
            {
                // Return empty string if error occurs
                System.Diagnostics.Trace.Write(ex.Message);
                return string.Empty;
            }
        }

        /// <summary>
        /// Used to check the last time a specified file was modified.
        /// </summary>
        /// <param name="FullFilePath">The full file path of the file to check.</param>
        /// <returns>Returns the date and time the file was last modified if successful. Returns NOTHING in all other conditions.</returns>
        public static Nullable<DateTime> GetFileLastModifiedDate(string FullFilePath)
        {
            try
            {
                // Return nothing if input is bad
                if (string.IsNullOrEmpty(FullFilePath) == true || System.IO.File.Exists(FullFilePath) == false)
                {
                    return null;
                }

                // If the file is there, check the last write time and return
                return new System.IO.FileInfo(FullFilePath).LastWriteTime;
            }
            catch (Exception ex)
            {
                // Return nothing if error occurs
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Used to extract a file name from the end of a full file path (e.g. 'C:\Test\MyDoc.docx' returns 'MyDoc.docx' or 'MyDoc').
        /// </summary>
        /// <param name="FullFilePath">The full file path to extract the file name from.</param>
        /// <param name="RemoveFileExtension">OPTIONAL:  Boolean to indicate if the file extension should be removed from the file name (e.g. 'MyDoc.docx' becomes 'MyDoc').</param>
        /// <returns>Returns the file name if found. Returns an empty string in all other conditions.</returns>
        public static string ExtractFileNameFromFullPath(string FullFilePath, bool RemoveFileExtension = false)
        {
            try
            {
                // Use .net method to return file name w/ or w/o extension
                switch (RemoveFileExtension)
                {
                    case true:
                        return System.IO.Path.GetFileNameWithoutExtension(FullFilePath);
                    default:
                        return System.IO.Path.GetFileName(FullFilePath);
                }
            }
            catch (Exception ex)
            {
                // Return empty string on error
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return string.Empty;
            }
        }

        /// <summary>
        /// Used to extract a folder name from the end of a full directory path (e.g. 'C:\InnerDir\OuterDir' returns 'OuterDir').
        /// </summary>
        /// <param name="FullDirectoryPath">The full directory path to extract the folder name from.</param>
        /// <returns>Returns the folder name if found. Returns an empty string in all other conditions.</returns>
        public static string ExtractFolderNameFromFullPath(string FullDirectoryPath)
        {
            // Uses same code as extracting file name
            return ExtractFileNameFromFullPath(FullDirectoryPath);
        }

        /// <summary>
        /// Used to extract the root folder of a specified file / folder path (e.g. 'C:\SomeDir\MyDoc.docx' returns 'C:\SomeDir').
        /// </summary>
        /// <param name="FullPath">The full path of the file or folder to extract the root folder name from.</param>
        /// <returns>Returns the root path if found. Returns an empty string in all other conditions.</returns>
        public static string ExtractRootFolderFromFullPath(string FullPath)
        {
            try
            {
                // Use .net method to return folder name
                return System.IO.Path.GetDirectoryName(FullPath);
            }
            catch (Exception ex)
            {
                // Return empty string on error
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return string.Empty;
            }
        }

        /// <summary>
        /// Used to remove illegal characters from a file name (e.g. characters that can't be used when naming files or folder..."\", "?", etc.).
        /// </summary>
        /// <param name="StringToClean">The string to clean.</param>
        /// <returns>Returns the original string without any file system illegal characters.</returns>
        public static string CleanFileName(string StringToClean)
        {
            try
            {
                // Create the string to return
                string StringToReturn = StringToClean.Trim();

                // Return empty string if nothing is passed in
                if (string.IsNullOrEmpty(StringToReturn) == true)
                {
                    // Ensure null string is never returned
                    StringToClean = string.Empty;
                    return StringToReturn;
                }

                // Save all illegal chars to replace in array
                var IllegalChars = new string[] { @"\", "/", "|", ":", "?", "<", ">", "\"" };

                // Remove chars not allowed in a file name
                foreach (string ILLEGALCHAR in IllegalChars)
                {
                    StringToReturn = StringToReturn.Replace(ILLEGALCHAR, string.Empty);
                }

                // Return the string
                return StringToReturn;
            }
            catch (Exception ex)
            {
                // Return an empty string if error occurs
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return string.Empty;
            }
        }

        /// <summary>
        /// Used to quote blocks / levels of a file or folder with if they have spaces that would prevent them from working in the cmd line.
        /// </summary>
        /// <param name="FullFileOrFolderPathWithSpaces">The full path to the file or folder.</param>
        /// <returns>Returns the path with quote-enclosing blocks / levels that contained spaces if successful. Returns an empty string in all other conditions.</returns>
        public static string GetCmdLineSafeFilePath(string FullFileOrFolderPathWithSpaces)
        {
            try
            {
                // Return empty if there's no path
                if (string.IsNullOrEmpty(FullFileOrFolderPathWithSpaces) == true) return string.Empty;

                // If we have a path, create string to hold the char to split by
                string CharToSplitBy = @"\";
                if (FullFileOrFolderPathWithSpaces.Contains("/") == true)
                {
                    // If they're using a path with alternate level divisors, flip (should never be both)
                    CharToSplitBy = "/";
                }

                // Split the input string
                var AllLevels = FullFileOrFolderPathWithSpaces.Split(new[] { CharToSplitBy }, StringSplitOptions.None).ToList();

                // Go through and clean up levels with spaces
                for (int i = 0; i <= AllLevels.Count - 1; i++)
                {
                    if (AllLevels[i].Contains(" ") == true)
                    {
                        // If it has a space, enclose in quotes
                        AllLevels[i] = "\"" + AllLevels[i] + "\"";
                    }
                }

                // Once enclosed, join back and return
                return string.Join(CharToSplitBy, AllLevels);
            }
            catch (Exception ex)
            {
                // Return empty if error occurs
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return string.Empty;
            }
        }


        #endregion

        #region File System Enumeration Methods


        /// <summary>
        /// Used to get a list of all file names (WITH file extensions) in a specified directory.
        /// </summary>
        /// <param name="DirectoryToCheck">The folder to search.</param>
        /// <returns>Returns a list of all file names (with extensions) in the specified directory.</returns>
        public static List<string> GetFileNamesInFolder(string DirectoryToCheck)
        {
            // Create list to return
            var ListToReturn = new List<string>();

            // Return empty list if folder doesn't exist
            if (System.IO.Directory.Exists(DirectoryToCheck) == false)
            {
                return ListToReturn;
            }

            // If directory is good, get all files in it
            var AllFullPaths = System.IO.Directory.GetFiles(DirectoryToCheck).ToList();

            // Loop through and add file names only
            foreach (string FILEPATH in AllFullPaths)
            {
                ListToReturn.Add(System.IO.Path.GetFileName(FILEPATH));
            }

            // Return the list
            return ListToReturn;
        }
        /// <summary>
        /// Used to get a list of all files (without file extensions) in a specified directory.
        /// </summary>
        /// <param name="DirectoryToCheck">The folder to search.</param>
        /// <returns>Returns a list of all file names (without extensions) in the specified directory.</returns>
        public static List<string> GetFileNamesInFolderWithoutExtension(string DirectoryToCheck)
        {
            // Get all the file names with extensions
            var AllFiles = GetFileNamesInFolder(DirectoryToCheck);

            // If nothing or empty collection came back, return it
            if (AllFiles == null || AllFiles.Count == 0)
            {
                return new List<string>();
            }

            // If there were files, loop through and remove extensions
            for (int i = 0; i <= AllFiles.Count - 1; i++)
            {
                AllFiles[i] = System.IO.Path.GetFileNameWithoutExtension(AllFiles[i]);
            }

            // Return the collection
            return AllFiles;
        }

        /// <summary>
        /// Used to get a list of all sub folder names in a specified directory.
        /// </summary>
        /// <param name="DirectoryToCheck">The folder to search.</param>
        /// <returns>Returns a list of all sub folder names in the specified directory.</returns>
        public static List<string> GetSubFolderNamesInFolder(string DirectoryToCheck)
        {
            // Create list to return
            var ListToReturn = new List<string>();

            // Return empty list if folder doesn't exist
            if (System.IO.Directory.Exists(DirectoryToCheck) == false) return ListToReturn;

            // If directory is good, get all files in it
            var AllFullPaths = System.IO.Directory.GetDirectories(DirectoryToCheck).ToList();

            // Loop through and add folder names only
            foreach (string FILEPATH in AllFullPaths)
            {
                ListToReturn.Add(System.IO.Path.GetFileName(FILEPATH));
            }

            // Return the list
            return ListToReturn;
        }

        /// <summary>
        /// Used to get a list of all files (full file paths) in all sub directories of a top level folder.
        /// </summary>
        /// <param name="TopLevelDirectory">The top level / starting directory to search.</param>
        /// <returns>Returns a list of full file paths to all files in the top level directory and all of its sub directories.</returns>
        public static List<string> GetAllFilePathsRecursively(string TopLevelDirectory)
        {
            // Create list to hold all files to return
            var AllFilesToReturn = new List<string>();

            try
            {
                // Try to get all files in the current directory
                var FilesInThisDir = System.IO.Directory.GetFiles(TopLevelDirectory).ToList();

                // Loop through and add all files to the list to return
                foreach (var FILE in FilesInThisDir)
                {
                    AllFilesToReturn.Add(FILE);
                }
            }
            catch (Exception)
            {
                // No action needed on failure
            }

            try
            {
                // Try to get all sub directories
                var AllSubDirs = System.IO.Directory.GetDirectories(TopLevelDirectory).ToList();

                // If folders come back, loop through and use recursion to get all files in it
                foreach (var SUBDIR in AllSubDirs)
                {
                    // Get all files in the sub dir
                    var AllFilesInSubDir = GetAllFilePathsRecursively(SUBDIR);

                    // Add all these files to the main list of files to return
                    foreach (var FILE in AllFilesInSubDir)
                    {
                        AllFilesToReturn.Add(FILE);
                    }
                }
            }
            catch (Exception)
            {
                // No action needed on failure
            }

            // Return the files when done
            return AllFilesToReturn;
        }

        /// <summary>
        /// Used to get a list of all sub directories (full folder paths) in a top level directory.
        /// </summary>
        /// <param name="TopLevelDirectory">The top level / starting directory to search.</param>
        /// <returns>Returns a list of all sub directory paths in the top level directory.</returns>
        public static List<string> GetAllSubFoldersRecursively(string TopLevelDirectory)
        {
            // Create list to hold all folders to return
            var AllFoldersToReturn = new List<string>();

            try
            {
                // Try to get all sub directories in this directory
                var AllSubDirs = System.IO.Directory.GetDirectories(TopLevelDirectory).ToList();

                // If folders come back, loop through and use recursion to get all folders in it
                foreach (var SUBDIR in AllSubDirs)
                {
                    // Get all folders in the sub dir
                    var AllFoldersInSubDir = GetAllSubFoldersRecursively(SUBDIR);

                    // Check if folder structure goes any deeper
                    if (AllFoldersInSubDir == null || AllFoldersInSubDir.Count == 0)
                    {
                        // If not, just add this folder
                        AllFoldersToReturn.Add(SUBDIR);
                    }
                    else
                    {
                        // If it does, loop through and keep going
                        foreach (var FOLDER in AllFoldersInSubDir)
                        {
                            AllFoldersToReturn.Add(FOLDER);
                        }
                    }
                }
            }
            catch (Exception)
            {
                // Ignore errors
            }

            // Return the folders when done
            return AllFoldersToReturn;
        }


        #endregion

        #region File System Access Methods


        /// <summary>
        /// Used to check if the user has write access to a specified directory.
        /// </summary>
        /// <param name="LocationToCheck">Full string path of the directory to check.</param>
        /// <returns>Returns TRUE if the user has write access to the specified directory. Returns FALSE in all other conditions.</returns>
        public static bool DoesUserHaveWriteAccess(string LocationToCheck)
        {
            try
            {
                // Check if the directory doesn't exist yet
                if (System.IO.Directory.Exists(LocationToCheck) == false)
                {
                    try
                    {
                        // If not, try and create it
                        System.IO.Directory.CreateDirectory(LocationToCheck);

                        // If it is created, try to delete it
                        System.IO.Directory.Delete(LocationToCheck);

                        // If it can be created and deleted, user has access
                        return true;
                    }
                    catch (Exception ex)
                    {
                        // If it can't be created / deleted, user does not have access
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return false;
                    }
                }

                // If it does exist, create a file path that will be written
                string BaseFilePath = LocationToCheck + @"\TestFile-";
                string FileToWrite = string.Empty;

                // Loop to ensure that a unique file name was created (e.g. some file that already exists won't get overwritten)
                for (int i = 0; i <= 10000; i++)
                {
                    // Check if the file exists
                    if (System.IO.File.Exists(BaseFilePath + i.ToString() + ".rwaccess") == false)
                    {
                        // If a unique file path is found, save it and exit loop
                        FileToWrite = BaseFilePath + i.ToString() + ".rwaccess";
                        break;
                    }
                }

                // Return false if a unique file to write couldn't be found
                if (string.IsNullOrEmpty(FileToWrite) == true) return false;

                // If a unique file was found, return false if dummy data can't be written
                if (WriteTextFile(FileToWrite, new[] { "TEST WRITE ACCESS" }, false) == false) return false;

                try
                {
                    // If file is written, try to delete it
                    System.IO.File.Delete(FileToWrite);

                    // If file deletes, user has write access
                    return true;
                }
                catch (Exception ex)
                {
                    // If file can't be deleted, user does not have write access
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }
            catch (Exception ex)
            {
                // Return false if something fails
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return false;
            }
        }


        #endregion

        #region File System Manipulation Methods


        /// <summary>
        /// Used to create a directory and return the result.
        /// </summary>
        /// <param name="FullFolderPath">The full folder path of the directory to create.</param>
        /// <returns>Returns TRUE if the directory already exists or is created successfully. Returns FALSE in all other conditions.</returns>
        public static bool CreateFolder(string FullFolderPath)
        {
            try
            {
                // Return false if path is bad
                if (string.IsNullOrEmpty(FullFolderPath) == true || string.IsNullOrEmpty(FullFolderPath.Trim()) == true) return false;

                // Return true if path exists
                if (System.IO.Directory.Exists(FullFolderPath) == true) return true;

                // If the folder doesn't exist, try to create it
                System.IO.Directory.CreateDirectory(FullFolderPath);

                // Return true if creation works
                return true;
            }
            catch (Exception ex)
            {
                // Return false if error occurs
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Used to delete a specified file.
        /// </summary>
        /// <param name="FullFilePathToDelete">The full path of the file to delete.</param>
        /// <returns>Returns TRUE if the file is deleted OR if it doesn't exist. Returns FALSE in all other conditions.</returns>
        public static bool DeleteFile(string FullFilePathToDelete)
        {
            try
            {
                // Return false if input is bad
                if (string.IsNullOrEmpty(FullFilePathToDelete) == true) return false;

                // Return true if the file doesn't exist (don't need to delete)
                if (System.IO.File.Exists(FullFilePathToDelete) == false) return true;

                try
                {
                    // Ensure file isn't read only
                    SetFileReadOnlyAttribute(FullFilePathToDelete, false);
                    SetFileAttribute(FullFilePathToDelete, System.IO.FileAttributes.Normal, true);
                }
                finally
                {
                    // Attempt to delete file whether attributes can be changed or not
                    System.IO.File.Delete(FullFilePathToDelete);
                }

                // Return true if file is deleted
                return true;
            }
            catch (Exception ex)
            {
                // Return false if delete fails
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return false;
            }
        }
        /// <summary>
        /// Used to delete all files in a specified folder that haven't been changed since a specified cutoff date.
        /// </summary>
        /// <param name="FullFolderPathToScan">The full folder path to scan all files in.</param>
        /// <param name="CutoffDate">Any file that hasn't been modified since this date will be deleted.</param>
        /// <param name="RecursivelyScanChildFolders">Flag to indicate if just the specified folder or all child folders should be scanned.</param>
        /// <returns>Returns TRUE if all files older than the cutoff were deleted. Returns FALSE in all other conditions.</returns>
        public static bool DeleteFilesOlderThan(string FullFolderPathToScan, DateTime CutoffDate, bool RecursivelyScanChildFolders)
        {
            try
            {
                // Return false if the folder doesn't exist, this would generally be a user error in specifying path
                if (string.IsNullOrEmpty(FullFolderPathToScan) == true || DoesFolderExists(FullFolderPathToScan, 500) == false)
                {
                    return false;
                }

                // If the folder is there, create list to hold all file paths
                List<string> AllFilePaths = null;

                // Check if scanning recursively
                switch (RecursivelyScanChildFolders)
                {
                    case true:
                        // If so, get all child folders
                        AllFilePaths = GetAllFilePathsRecursively(FullFolderPathToScan);
                        break;
                    case false:
                        // If not, just get files in main folder
                        AllFilePaths = System.IO.Directory.GetFiles(FullFolderPathToScan).ToList();
                        break;
                }

                // Return false if we can't get files
                if (AllFilePaths == null) return false;

                // If we get something back, return true if there's no files
                if (AllFilePaths.Count == 0) return true;

                // If there are files, create var to hold if everything processes correctly
                bool AllFilesProcessedSuccessfully = true;

                // Loop through and try to process all
                foreach (var FILE in AllFilePaths)
                {
                    // Get the date
                    var DateOfFile = GetFileLastModifiedDate(FILE);
                    if (DateOfFile == null)
                    {
                        // If we can't get the date, set failure flag and skip (safest not to delete unknown file date)
                        AllFilesProcessedSuccessfully = false;
                        continue;
                    }

                    // If we get a date, skip if it's not old enough
                    if (DateOfFile.Value > CutoffDate) continue;

                    // If it's old enough, attempt delete and set failure flag if it doesn't work
                    if (DeleteFile(FILE) == false)
                    {
                        AllFilesProcessedSuccessfully = false;
                    }
                }

                // Return if all files were processed successfully (i.e. everything old enough is deleted)
                return AllFilesProcessedSuccessfully;
            }
            catch (Exception ex)
            {
                // Return false if error occurs
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return false;
            }
        }
        /// <summary>
        /// Used to delete a specified directory and all of its contents.
        /// </summary>
        /// <param name="FullFolderPathToDelete">The full path of the directory to delete.</param>
        /// <returns>Returns TRUE if the directory and all of its contents are deleted. Returns FALSE in all other conditions.</returns>
        public static bool DeleteFolder(string FullFolderPathToDelete)
        {
            try
            {
                // Return false if path is bad
                if (string.IsNullOrEmpty(FullFolderPathToDelete) == true) return false;

                // Return true if the folder doesn't exist (don't need to delete)
                if (System.IO.Directory.Exists(FullFolderPathToDelete) == false) return true;

                // If the folder does exist, create var to hold if any errors occur while deleting any files
                bool OneOrMoreErrorsOccurred = false;

                // Get all files in the folder to delete
                var AllFilesInFolder = StaticMethods.FileSystem.GetAllFilePathsRecursively(FullFolderPathToDelete);

                // Loop through and attempt to delete all files
                if (AllFilesInFolder != null && AllFilesInFolder.Count > 0)
                {
                    foreach (var FILE in AllFilesInFolder)
                    {
                        // Attempt delete and set flag if any file fails
                        if (DeleteFile(FILE) == false)
                        {
                            OneOrMoreErrorsOccurred = true;
                        }
                    }
                }

                try
                {
                    // Do a sleep 0, for some reason Directory.Delete can throw an error (even w/ Recursive=True) if accessed too quickly
                    System.Threading.Thread.Sleep(0);

                    // After deleting all files, attempt to blow away the full parent folder and any sub folders
                    System.IO.Directory.Delete(FullFolderPathToDelete, true);
                }
                catch (Exception ex)
                {
                    // If the final delete request fails, set error flag
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    OneOrMoreErrorsOccurred = true;
                }

                // Return the result (i.e. no errors means it worked)
                return !OneOrMoreErrorsOccurred;
            }
            catch (Exception ex)
            {
                // Return false if delete fails
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Used to move a specified file.
        /// </summary>
        /// <param name="SourceFilePath">The full path of the file to move.</param>
        /// <param name="DestinationFilePath">The full path of the new location for the file.</param>
        /// <param name="OverwriteIfFileExists">Flag to indicate if an existing file with the same name can be overwritten.</param>
        /// <returns>Returns TRUE if the source file is moved to the destination path. Returns FALSE in all other conditions.</returns>
        public static bool MoveFile(string SourceFilePath, string DestinationFilePath, bool OverwriteIfFileExists)
        {
            try
            {
                // Return false if inputs are bad
                if (string.IsNullOrEmpty(SourceFilePath) == true || string.IsNullOrEmpty(DestinationFilePath) == true) return false;

                // Return false if source file doesn't exist
                if (System.IO.File.Exists(SourceFilePath) == false) return false;

                // Return false if destination file exists and user doesn't want to overwrite
                if (OverwriteIfFileExists == false && System.IO.File.Exists(DestinationFilePath) == true) return false;

                // Get the destination directory
                string DestinationFolder = System.IO.Path.GetDirectoryName(DestinationFilePath);

                // Create the directory if missing
                if (System.IO.Directory.Exists(DestinationFolder) == false)
                {
                    System.IO.Directory.CreateDirectory(DestinationFolder);
                }

                // If overwrite is allowed and file already exists, ensure file isn't read only
                if (OverwriteIfFileExists == true && System.IO.File.Exists(DestinationFilePath) == true)
                {
                    SetFileReadOnlyAttribute(DestinationFilePath, false);
                }

                // Check if the file already exists
                if (System.IO.File.Exists(DestinationFilePath) == false)
                {
                    // If not, just do the standard move
                    System.IO.File.Move(SourceFilePath, DestinationFilePath);
                }
                else
                {
                    // If it does, safest way is to do a copy to case there are weird file permissions - copy will be slower though :-(
                    System.IO.File.Copy(SourceFilePath, DestinationFilePath, true);

                    // Once copied, blow away the original to complete the move (don't really care if this works)
                    DeleteFile(SourceFilePath);
                }

                // Return true if the file moves
                return true;
            }
            catch (Exception ex)
            {
                // Return false if error occurs
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return false;
            }
        }
        /// <summary>
        /// Used to move a specified directory (will overwrite and merge if destination folder already exists).
        /// </summary>
        /// <param name="SourceFolderPath">The full path of the directory to move.</param>
        /// <param name="DestinationFolderPath">The full path of the new location for the directory.</param>
        /// <returns>Returns TRUE if the entire source directory is moved to the new destination path. Returns FALSE in all other conditions.</returns>
        public static bool MoveFolder(string SourceFolderPath, string DestinationFolderPath)
        {
            try
            {
                // Return false if inputs are bad
                if (string.IsNullOrEmpty(SourceFolderPath) == true || string.IsNullOrEmpty(DestinationFolderPath) == true) return false;

                // Return false if source folder doesn't exist
                if (System.IO.Directory.Exists(SourceFolderPath) == false) return false;

                // Check if the destination folder already exists
                if (System.IO.Directory.Exists(DestinationFolderPath) == false)
                {
                    // If not, just do the move
                    System.IO.Directory.Move(SourceFolderPath, DestinationFolderPath);
                }
                else
                {
                    // If it does, safest way is to do a copy to case there are weird file permissions - copy will be slower though :-(
                    if (CopyFolder(SourceFolderPath, DestinationFolderPath, true) == false)
                    {
                        // Return false if we can't copy everything to the new location
                        return false;
                    }

                    // Once copied, blow away the original to complete the move (don't really care if this works)
                    DeleteFolder(SourceFolderPath);
                }

                // Return true if the file moves
                return true;
            }
            catch (Exception ex)
            {
                // Return false if error occurs
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Used to copy a specified file.
        /// </summary>
        /// <param name="SourceFilePath">The full path of the file to copy.</param>
        /// <param name="DestinationFilePath">The full path for the new copy.</param>
        /// <param name="OverwriteIfFileExists">Flag to indicate if an existing file with the same name can be overwritten.</param>
        /// <returns>Returns TRUE if the source file is copied to the destination path. Returns FALSE in all other conditions.</returns>
        public static bool CopyFile(string SourceFilePath, string DestinationFilePath, bool OverwriteIfFileExists)
        {
            try
            {
                // Return false if inputs are bad
                if (string.IsNullOrEmpty(SourceFilePath) == true || string.IsNullOrEmpty(DestinationFilePath) == true) return false;

                // Return false if source file doesn't exist
                if (System.IO.File.Exists(SourceFilePath) == false) return false;

                // Get the destination directory
                string DestinationFolder = System.IO.Path.GetDirectoryName(DestinationFilePath);

                // Create the directory if missing
                if (System.IO.Directory.Exists(DestinationFolder) == false)
                {
                    System.IO.Directory.CreateDirectory(DestinationFolder);
                }

                // If overwrite is allowed and file already exists, ensure file isn't read only
                if (OverwriteIfFileExists == true && System.IO.File.Exists(DestinationFilePath) == true)
                {
                    SetFileReadOnlyAttribute(DestinationFilePath, false);
                }

                // Try to copy the file
                System.IO.File.Copy(SourceFilePath, DestinationFilePath, OverwriteIfFileExists);

                // Return true if the file copies
                return true;
            }
            catch (Exception ex)
            {
                // Return false if error occurs
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return false;
            }
        }
        /// <summary>
        /// Loop through a high-level directory and recursively copies all inner folders and files to a specified output directory.
        /// </summary>
        /// <param name="SourceFolderPath">The full path of the directory to copy.</param>
        /// <param name="DestinationFolderPath">The full path for the new copy.</param>
        /// <param name="MergeAndOverwriteIfFolderExists">Flag to indicate if existing files in the destination location should be overwritten if they have the same name.</param>
        /// <returns>Returns TRUE if the entire source directory is copied to the new destination path. Returns FALSE in all other conditions.</returns>
        public static bool CopyFolder(string SourceFolderPath, string DestinationFolderPath, bool MergeAndOverwriteIfFolderExists)
        {
            try
            {
                // Return false if info is missing
                if (string.IsNullOrEmpty(SourceFolderPath) == true) return false;
                if (string.IsNullOrEmpty(DestinationFolderPath) == true) return false;

                // Create destination directory if not there yet
                if (System.IO.Directory.Exists(DestinationFolderPath) == false)
                {
                    System.IO.Directory.CreateDirectory(DestinationFolderPath);
                }

                // Get folder contents of source directory
                var FolderContents = System.IO.Directory.GetFileSystemEntries(SourceFolderPath).ToList();

                // Loop through and copy files
                foreach (var ENTRY in FolderContents)
                {
                    // Get the entry name
                    string EntryName = System.IO.Path.GetFileName(ENTRY);

                    // Create the target path
                    string TargetEntryPath = System.IO.Path.Combine(DestinationFolderPath, EntryName);

                    // Check if the entry is a folder
                    if (System.IO.Directory.Exists(ENTRY) == true)
                    {
                        // If it's a folder, use recursion to copy its content
                        if (CopyFolder(ENTRY, TargetEntryPath, MergeAndOverwriteIfFolderExists) == false)
                        {
                            // Return false if recursive step fails
                            return false;
                        }
                    }
                    else
                    {
                        // If it's a file, only copy if it doesn't exist OR overwrite is OK
                        if (System.IO.File.Exists(TargetEntryPath) == false || MergeAndOverwriteIfFolderExists == true)
                        {
                            System.IO.File.Copy(ENTRY, TargetEntryPath, true);
                        }
                    }
                }

                // Return true if everything copies
                return true;
            }
            catch (Exception ex)
            {
                // Return false if error occurs
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Used to rename a specified file.
        /// </summary>
        /// <param name="FullFilePathToRename">The full file path of the file to rename.</param>
        /// <param name="NewName">The new name (including extension) of the file.</param>
        /// <returns>Returns TRUE if the file is renamed. Returns FALSE in all other conditions.</returns>
        public static bool RenameFile(string FullFilePathToRename, string NewName)
        {
            try
            {
                // Return false if inputs are bad
                if (string.IsNullOrEmpty(FullFilePathToRename) == true || string.IsNullOrEmpty(NewName) == true) return false;

                // Return false if source file is missing
                if (System.IO.File.Exists(FullFilePathToRename) == false) return false;

                // Ensure that the name isn't a path
                NewName = System.IO.Path.GetFileName(NewName);

                // Create the full file path to the new / renamed file
                string FullNewFilePath = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(FullFilePathToRename), NewName);

                // Get the original name
                string OriginalName = System.IO.Path.GetFileName(FullFilePathToRename);

                // Return true if the name is identical (casing and all) - nothing to rename
                if (OriginalName == NewName) return true;

                // Check if just the casing is changing (System.IO.File.Move will falsely return true if only the casing changes)
                if (OriginalName.ToUpper() != NewName.ToUpper())
                {
                    // If the name is truly changing, verify that the new name is unique
                    if (System.IO.File.Exists(FullNewFilePath) == true)
                    {
                        // Return false if the file already exists (don't want to overwrite whatever is there by accident)
                        return false;
                    }

                    // If that name / path is unique, move it to accomplish rename
                    return MoveFile(FullFilePathToRename, FullNewFilePath, false);
                }
                else
                {
                    // If we're just changing the casing, we have to do a double rename to trick System.IO.File.Move, create temp file to hold unique name
                    string TempFileUniqueName = string.Empty;

                    // Keep looping until we get a unique name
                    while (string.IsNullOrEmpty(TempFileUniqueName) == true)
                    {
                        // Create var to hold candidate unique file name
                        string CandidateFile = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(FullFilePathToRename), System.IO.Path.GetRandomFileName());

                        // Keep looping if this file already exists
                        if (System.IO.File.Exists(CandidateFile) == true) continue;

                        // If the name is new, save it to exit loop
                        TempFileUniqueName = CandidateFile;
                    }

                    // Do a double move to rename the file with new casing
                    System.IO.File.Move(FullFilePathToRename, TempFileUniqueName);
                    System.IO.File.Move(TempFileUniqueName, FullNewFilePath);

                    // Return true if rename works
                    return true;
                }
            }
            catch (Exception ex)
            {
                // Return false if an error occurs
                System.Diagnostics.Trace.Write(ex.Message);
                return false;
            }
        }
        /// <summary>
        /// Used to rename a specified directory.
        /// </summary>
        /// <param name="FullFolderPathToRename">The full file path of the folder to rename.</param>
        /// <param name="NewName">The new name (including extension) of the file.</param>
        /// <returns>Returns TRUE if the folder is renamed. Returns FALSE in all other conditions.</returns>
        public static bool RenameFolder(string FullFolderPathToRename, string NewName)
        {
            try
            {
                // Return false if inputs are bad
                if (string.IsNullOrEmpty(FullFolderPathToRename) == true || string.IsNullOrEmpty(NewName) == true) return false;

                // Return false if source folder is missing
                if (System.IO.Directory.Exists(FullFolderPathToRename) == false) return false;

                // Ensure that the name isn't a path
                NewName = System.IO.Path.GetFileName(NewName);

                // Create the full folder path to the new / renamed folder
                string FullNewFolderPath = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(FullFolderPathToRename), NewName);

                // Get the original name
                string OriginalName = System.IO.Path.GetFileName(FullFolderPathToRename);

                // Return true if the name is identical (casing and all) - nothing to rename
                if (OriginalName == NewName) return true;

                // Check if just the casing is changing (System.IO.Directory.Move will falsely return true if only the casing changes)
                if (OriginalName.ToUpper() != NewName.ToUpper())
                {
                    // If the name is truly changing, verify that the new name is unique
                    if (System.IO.Directory.Exists(FullNewFolderPath) == true)
                    {
                        // Return false if the folder already exists (don't want to overwrite whatever is there by accident)
                        return false;
                    }

                    // If that name / path is unique, move it to accomplish rename
                    return MoveFolder(FullFolderPathToRename, FullNewFolderPath);
                }
                else
                {
                    // If we're just changing the casing, we have to do a double rename to trick System.IO.Directory.Move, create temp file to hold unique name
                    string TempFolderUniqueName = string.Empty;

                    // Keep looping until we get a unique name
                    while (string.IsNullOrEmpty(TempFolderUniqueName) == true)
                    {
                        // Create var to hold candidate unique folder name
                        string CandidateFolder = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(FullFolderPathToRename), System.IO.Path.GetFileNameWithoutExtension(System.IO.Path.GetRandomFileName()));

                        // Keep looping if this file already exists
                        if (System.IO.Directory.Exists(CandidateFolder) == true) continue;

                        // If the name is new, save it to exit loop
                        TempFolderUniqueName = CandidateFolder;
                    }

                    // Do a double move to rename the folder with new casing
                    System.IO.Directory.Move(FullFolderPathToRename, TempFolderUniqueName);
                    System.IO.Directory.Move(TempFolderUniqueName, FullNewFolderPath);

                    // Return true if rename works
                    return true;
                }
            }
            catch (Exception ex)
            {
                // Return false if an error occurs
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Used to change specified file's read-only attribute.
        /// </summary>
        /// <param name="FullFilePath">The full path to the file.</param>
        /// <param name="IsReadOnly">Flag to indicate if the file should be marked normal / read-only.</param>
        /// <returns>Returns TRUE if the attribute is set successfully. Returns FALSE in all other conditions.</returns>
        public static bool SetFileReadOnlyAttribute(string FullFilePath, bool IsReadOnly)
        {
            // Use helper and return result
            return SetFileAttribute(FullFilePath, System.IO.FileAttributes.ReadOnly, IsReadOnly);
        }

        /// <summary>
        /// Used to change specified file's visibility attribute.
        /// </summary>
        /// <param name="FullFilePath">The full path to the file.</param>
        /// <param name="IsVisible">Flag to indicate if the file should be marked visible / hidden.</param>
        /// <returns>Returns TRUE if the attribute is set successfully. Returns FALSE in all other conditions.</returns>
        public static bool SetFileVisibility(string FullFilePath, bool IsVisible)
        {
            // Use helper and return result
            return SetFileAttribute(FullFilePath, System.IO.FileAttributes.Hidden, !IsVisible);
        }

        /// <summary>
        /// Used to read all the bytes in a specified file.
        /// </summary>
        /// <param name="FullFilePath">The full file path of the file to read.</param>
        /// <returns>Returns a list of bytes with the file content if successful. Returns NOTHING in all other conditions.</returns>
        public static List<byte> ReadFile(string FullFilePath)
        {
            try
            {
                // Return nothing if file path is missing
                if (string.IsNullOrEmpty(FullFilePath) == true) return null;

                // Return nothing if the file doesn't exist
                if (System.IO.File.Exists(FullFilePath) == false) return null;

                // Create list to hold the bytes to return
                var BytesToReturn = new List<byte>();

                // Create reader to process file in case it's read-only
                using (System.IO.StreamReader READER = new System.IO.StreamReader(System.IO.File.OpenRead(FullFilePath)))
                {
                    // Have to copy to memory stream to extract byte array
                    using (System.IO.MemoryStream MemStream = new System.IO.MemoryStream())
                    {
                        READER.BaseStream.CopyTo(MemStream);
                        BytesToReturn.AddRange(MemStream.ToArray());
                    }
                }

                // Return the byte array
                return BytesToReturn;
            }
            catch (Exception ex)
            {
                // Return nothing if error occurs
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Used to write a list of bytes as a file (overwrites any existing files with the specified path).
        /// </summary>
        /// <param name="FullFilePath">The full file path to write the bytes to.</param>
        /// <param name="FileBytes">The list of bytes to write.</param>
        /// <returns>Returns TRUE if the write is successful. Returns FALSE in all other conditions.</returns>
        public static bool WriteFile(string FullFilePath, IEnumerable<byte> FileBytes)
        {
            try
            {
                // Return false if file path is missing
                if (string.IsNullOrEmpty(FullFilePath) == true) return false;

                // Return false if the bytes are missing
                if (FileBytes == null) return false;

                // If path looks legit, get folder
                string FolderLocation = System.IO.Path.GetDirectoryName(FullFilePath);

                // Attempt to create folder if missing
                if (CreateFolder(FolderLocation) == false) return false;

                // Once we're ready, write the file
                System.IO.File.WriteAllBytes(FullFilePath, FileBytes.ToArray());

                // Return true if write works
                return true;
            }
            catch (Exception ex)
            {
                // Return false if error occurs
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Used to check if a given file is currently in use / locked by another program.
        /// </summary>
        /// <param name="FilePathToCheck">The full string file path of the file to check.</param>
        /// <returns>Returns TRUE if the file is currently locked. Returns FALSE if the file is available OR if the file does not exist.</returns>
        public static bool IsFileInUse(string FilePathToCheck)
        {
            try
            {
                // If the file doesn't exist, it is not in use
                if (System.IO.File.Exists(FilePathToCheck) == false) return false;

                // If there's a file, attempt to open it for read / write access to see if it's in use
                using (var FS = System.IO.File.Open(FilePathToCheck, System.IO.FileMode.Open, System.IO.FileAccess.ReadWrite))
                {
                    // Return false if file opened (i.e. it isn't locked / in use)
                    if (FS != null) return false;
                }

                // If FS was somehow null, the file is in use
                return true;
            }
            catch (Exception ex)
            {
                // If the file can't be opened / closed, it is in use
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return true;
            }
        }


        #endregion

        #region File System Custom Methods


        /// <summary>
        /// Used to create a daily backup in a specified backup location for any number of files and folders.
        /// </summary>
        /// <param name="RootBackupFolderPath">The root location where you want the backup created (the method will automatically create a sub-folder with the date of the backup).</param>
        /// <param name="FoldersToBackup">A list of all folders (full paths) to copy to the backup folder.</param>
        /// <param name="FilesToBackup">A list of all files (full paths) to copy to the backup folder.</param>
        /// <param name="OverwriteExistingBackup">Flag to indicate whether an existing daily backup (e.g. a backup with today's date) should be overwritten (e.g. if the backup method is called a second time on the same day).</param>
        /// <param name="MaxBackupsToKeep">OPTIONAL:  Nullable integer to specify the maximum number of backups to keep in the directory. If the maximum is exceeded, the OLDEST backups will be removed until the the total number of backups is less than or equal to the max.</param>
        /// <returns>Returns TRUE if the backup is created OR if the backup already exists and the overwrite flag is set to false. Returns FALSE in all other conditions. Throws an exception if both the FoldersToBackup and FilesToBackup variables are null.</returns>
        public static bool CreateDailyBackup(string RootBackupFolderPath, IEnumerable<string> FoldersToBackup, IEnumerable<string> FilesToBackup, bool OverwriteExistingBackup, Nullable<int> MaxBackupsToKeep = null)
        {
            // Throw error if both folders and files to backup is null
            if (FoldersToBackup == null && FilesToBackup == null)
            {
                throw new ApplicationException("You must specify at least one file OR at least one folder.");
            }

            // Ensure root backup location doesn't end in "\"
            if (string.IsNullOrEmpty(RootBackupFolderPath) == false && RootBackupFolderPath.Length > 2)
            {
                // Check last char
                if (RootBackupFolderPath.Substring(RootBackupFolderPath.Length - 1) == @"\")
                {
                    // If it ends with "\", remove that
                    RootBackupFolderPath = RootBackupFolderPath.Substring(0, RootBackupFolderPath.Length - 1);
                }
            }

            // Check if root backup folder exists
            if (System.IO.Directory.Exists(RootBackupFolderPath) == false)
            {
                try
                {
                    // If not, try to create it
                    System.IO.Directory.CreateDirectory(RootBackupFolderPath);
                }
                catch (Exception ex)
                {
                    // If directory can't be created, return false
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }

            // Get today's year, month, and day
            string TodayYear = DateTime.Now.Year.ToString();
            string TodayMonth = DateTime.Now.Month.ToString();
            string TodayDay = DateTime.Now.Day.ToString();

            // Ensure that month & day are 2 digit for correct folder sorting
            if (TodayMonth.Length == 1) TodayMonth = "0" + TodayMonth;
            if (TodayDay.Length == 1) TodayDay = "0" + TodayDay;

            // Create name of today's backup folder
            string BackupFolderNameForToday = TodayYear + "-" + TodayMonth + "-" + TodayDay;

            // Create full backup path
            string FullBackupPath = RootBackupFolderPath + @"\" + BackupFolderNameForToday;

            // If backup already exists and user doesn't want to overwrite backup, return true
            if (System.IO.Directory.Exists(FullBackupPath) == true && OverwriteExistingBackup == false)
            {
                return true;
            }

            try
            {
                // Try to create the full backup directory
                System.IO.Directory.CreateDirectory(FullBackupPath);
            }
            catch (Exception)
            {
                // If directory does NOT exist, return false (if it was already there, keep going)
                if (System.IO.Directory.Exists(FullBackupPath) == false)
                {
                    return false;
                }
            }

            // If directory is there, create flag to hold if any copies fail
            bool CopyFailed = false;

            // Try to copy passed in directories to it
            if (FoldersToBackup != null && FoldersToBackup.Count() > 0)
            {
                try
                {
                    // Attempt to copy over each folder
                    foreach (string FOLDER in FoldersToBackup)
                    {
                        // Get the root location of the current folder
                        string RootSource = System.IO.Path.GetDirectoryName(FOLDER);

                        // Attempt the copy, set flag if it fails
                        if (CopyFolder(FOLDER, FOLDER.Replace(RootSource, FullBackupPath), true) == false)
                        {
                            CopyFailed = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    // If any folders can't be copied, set flag
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    CopyFailed = true;
                }
            }

            // Try to copy passed in files
            if (FilesToBackup != null && FilesToBackup.Count() > 0)
            {
                try
                {
                    // Attempt to copy over each file
                    foreach (string FILE in FilesToBackup)
                    {
                        // Get the root location of the current file
                        string RootSource = System.IO.Path.GetDirectoryName(FILE);

                        // Attempt the copy
                        System.IO.File.Copy(FILE, FILE.Replace(RootSource, FullBackupPath), true);
                    }
                }
                catch (Exception ex)
                {
                    // If any folders can't be copied, set flag
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    CopyFailed = true;
                }
            }

            // Check if the copies failed
            if (CopyFailed == true)
            {
                try
                {
                    // If so, try and remove the directory since we can't tell what's in it and user may not be overwritten on next backup
                    System.IO.Directory.Delete(FullBackupPath, true);
                }
                catch (Exception)
                {
                    // Ignore errors
                }

                // Return false if copy failed
                return false;
            }

            // If no valid max was specified, return true as backup is complete
            if (MaxBackupsToKeep == null || Convert.ToInt32(MaxBackupsToKeep) <= 0)
            {
                return true;
            }

            // If there's a valid max number to keep, save the number to keep
            int DirCountToKeep = Convert.ToInt32(MaxBackupsToKeep);

            // Get all folders in root backup dir
            var AllFoldersInBackupDir = System.IO.Directory.GetDirectories(RootBackupFolderPath).ToList();

            // Create list to hold all backup folders (in case user put other stuff in there too)
            var AllRecognizedBackupFolders = new List<string>();

            // Loop through all folders and add ones that are backup folders
            foreach (string FOLDER in AllFoldersInBackupDir)
            {
                // Get folder name only
                string FolderName = FOLDER.Replace(System.IO.Path.GetDirectoryName(FOLDER) + @"\", string.Empty);

                // Check length, "-" char, and is date
                if (FolderName.Length == 10 && FolderName.Contains("-") == true && IsDate(FolderName) == true)
                {
                    // If all are true, this is likely a backup folder, add it to list
                    AllRecognizedBackupFolders.Add(FOLDER);
                }
            }

            // Once all backups are found, create list to hold dirs to delete
            var DirsToDelete = new List<string>();

            // Add oldest folders to delete list until there will only be the specified max dirs left
            while (AllRecognizedBackupFolders.Count > DirCountToKeep)
            {
                // Move the oldest items to the delete list
                DirsToDelete.Add(AllRecognizedBackupFolders[0]);
                AllRecognizedBackupFolders.RemoveAt(0);
            }

            // Delete all old backups
            foreach (string OLDFOLDER in DirsToDelete)
            {
                try
                {
                    // Try to delete the folder
                    System.IO.Directory.Delete(OLDFOLDER, true);
                }
                catch (Exception)
                {
                    // No action needed on failure
                }
            }

            // Return true if everything works
            return true;
        }


        #endregion

        #region Helper Methods


        // Helper to set a specified file attribute
        private static bool SetFileAttribute(string FullFilePath, System.IO.FileAttributes AttributeToSet, bool IsAttributeEnabled)
        {
            try
            {
                // Return false if file is missing
                if (string.IsNullOrEmpty(FullFilePath) == true || System.IO.File.Exists(FullFilePath) == false)
                {
                    return false;
                }

                // Get the current file attributes to see if change is needed
                var OrigAttributes = System.IO.File.GetAttributes(FullFilePath);

                // If we get them, capture current file attribute
                bool IsFileAttributeCurrentlyEnabled = false;
                if (Convert.ToInt32(OrigAttributes & AttributeToSet) == Convert.ToInt32(AttributeToSet))
                {
                    // If the attribute is not currently set, clear flag
                    IsFileAttributeCurrentlyEnabled = true;
                }

                // Bail if we're already in the desired state (good to go already)
                if (IsAttributeEnabled == IsFileAttributeCurrentlyEnabled)
                {
                    return true;
                }

                // Create var to hold new attributes to set
                var NewAttributes = new System.IO.FileAttributes();

                // If not, update to the new state
                switch (IsAttributeEnabled)
                {
                    case true:
                        // Add attribute if missing and requested
                        NewAttributes = OrigAttributes | AttributeToSet;
                        break;
                    case false:
                        // Remove attribute if there and remove requested
                        NewAttributes = OrigAttributes & ~AttributeToSet;
                        break;
                }

                // Update the file attributes
                System.IO.File.SetAttributes(FullFilePath, NewAttributes);

                // Return true if update works
                return true;
            }
            catch (Exception ex)
            {
                // Return false if error occurs
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return false;
            }
        }

        // Helper to check if a path exists (particularly a network path) with a timeout to prevent hanging
        private static bool DoesPathExist(string FullPath, bool IsFolder, UInt16 TimeoutInMS)
        {
            try
            {
                // Return false if path is empty
                if (string.IsNullOrEmpty(FullPath) == true) return false;

                // Create delegate method to check for path so, if it's a disconnected network location, it doesn't take forever to timeout
                Func<string, bool> DelegateMethod = System.IO.Directory.Exists;

                // If it's a file, flip the method
                if (IsFolder == false)
                {
                    DelegateMethod = System.IO.File.Exists;
                }

                // If timeout isn't specified, use default method (based on path type) and return the result
                if (TimeoutInMS == 0) return DelegateMethod(FullPath);

                // If there is a timeout, create flag to hold if path exists
                bool PathExists = false;

                // Create and launch thread
                var thrCheckPath = new System.Threading.Thread(() => PathExists = DelegateMethod(FullPath));
                thrCheckPath.SetApartmentState(System.Threading.ApartmentState.STA);
                thrCheckPath.IsBackground = true;
                thrCheckPath.Start();

                // Attempt to join back into main within specified timeout and capture result
                bool ThreadCompleted = thrCheckPath.Join(TimeoutInMS);
                if (ThreadCompleted == false)
                {
                    // If thread didn't complete, assume path didn't exist
                    thrCheckPath.Abort();
                    return false;
                }

                // If the thread did complete, return the result
                return PathExists;
            }
            catch (Exception ex)
            {
                // Return false if error occurs
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return false;
            }
        }

        // Helper routine to attempt to write file to check write access
        private static bool WriteTextFile(string FileToWrite, string[] OneDimensionalArrayToWrite, bool AppendToExistingFileIfItExists)
        {
            try
            {
                // Create the file
                using (var OutFile = new System.IO.StreamWriter(FileToWrite, AppendToExistingFileIfItExists))
                {
                    // Iterate through and write each line
                    for (int i = 0; i <= OneDimensionalArrayToWrite.Length - 1; i++)
                    {
                        OutFile.WriteLine(OneDimensionalArrayToWrite[i]);
                    }
                }

                // Return true if nothing fails
                return true;
            }
            catch (Exception ex)
            {
                // Return false if error occurs
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return false;
            }
        }

        // Helper to check if a string is a date
        private static bool IsDate(string StringToCheck)
        {
            // Create dummy var to hold result
            DateTime ConvertedDate = DateTime.MinValue;

            // Try to parse save result
            return DateTime.TryParse(StringToCheck, out ConvertedDate);
        }


        #endregion

    }

}
