﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StaticMethods
{

    public struct Reflection
    {

        public struct Classes
        {

            /// <summary>
            /// Used to fetch all constant names and values of a specified type within a class.
            /// </summary>
            /// <typeparam name="T">The constant type (integer, string, double, etc.) to fetch within the class.</typeparam>
            /// <param name="ClassType">The class type to extract the constants from (GetType(ClassName)-VB / typeof(ClassName)-C#).</param>
            /// <returns>Returns a dictionary containing all constant names and values of the specified type within the class if successful. Returns NOTHING in all other conditions.</returns>
            public static Dictionary<string, T> GetMemberConstantNamesAndValues<T>(System.Type ClassType)
            {
                // Use same method in structure, no difference in implementation
                return Structures.GetMemberConstantNamesAndValues<T>(ClassType);
            }

            /// <summary>
            /// Used to extract all constant names and values from a given class and returns the result as a dictionary.
            /// </summary>
            /// <param name="ClassType">The class type to extract the constants from (GetType(ClassName)-VB / typeof(ClassName)-C#).</param>
            /// <returns>Returns a dictionary containing all constant names and values within the class if parsed. Returns NOTHING in all other conditions.</returns>
            public static Dictionary<string, object> GetAllMemberConstantNamesAndValues(System.Type ClassType)
            {
                // Use same method as structure (no difference in implementation)
                return Structures.GetAllMemberConstantNamesAndValues(ClassType);
            }

        }

        public struct Structures
        {

            /// <summary>
            /// Used to fetch all constant names and values of a specified type within a structure.
            /// </summary>
            /// <typeparam name="T">The constant type (integer, string, double, etc.) to fetch within the structure.</typeparam>
            /// <param name="StructureType">The structure type to extract the constants from (GetType(StrucName)-VB / typeof(StrucName)-C#).</param>
            /// <returns>Returns a dictionary containing all constant names and values of the specified type within the structure if successful. Returns NOTHING in all other conditions.</returns>
            public static Dictionary<string, T> GetMemberConstantNamesAndValues<T>(System.Type StructureType)
            {
                try
                {
                    // Get all member names and values
                    var AllMembers = GetAllMemberConstantNamesAndValues(StructureType);

                    // Return nothing if error occurred
                    if (AllMembers == null) return null;

                    // If extract worked, create dictionary to return
                    var DictionaryToReturn = new Dictionary<string, T>();

                    // Loop through all constants and find ones that have specified type
                    foreach (var PAIR in AllMembers)
                    {
                        if (PAIR.Value is T)
                        {
                            // If type matches, add to dictionary
                            DictionaryToReturn.Add(PAIR.Key, (T)PAIR.Value);
                        }
                    }

                    // Return the dictionary when done
                    return DictionaryToReturn;
                }
                catch (Exception ex)
                {
                    // Return nothing if exception occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }

            /// <summary>
            /// Used to extract all constant names and values from a given structure and returns the result as a dictionary.
            /// </summary>
            /// <param name="StructureType">The structure type to extract the constants from (GetType(StrucName)-VB / typeof(StrucName)-C#).</param>
            /// <returns>Returns a dictionary containing all constant names and values within the structure if parsed. Returns NOTHING in all other conditions.</returns>
            public static Dictionary<string, object> GetAllMemberConstantNamesAndValues(System.Type StructureType)
            {
                try
                {
                    // Return nothing if type is missing
                    if (StructureType == null) return null;

                    // Create dictionary to hold all constant names and values
                    var ConstantNamesAndValues = new Dictionary<string, object>();

                    // Get all public static vars in the specified type
                    var AllFieldInfo = StructureType.GetFields(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.FlattenHierarchy);

                    try
                    {
                        // Loop through all field info and extract values
                        foreach (var FIELD in AllFieldInfo)
                        {
                            // Check if literal and not an init only variable (i.e. it's a constant)
                            if (FIELD.IsLiteral && !FIELD.IsInitOnly)
                            {
                                // If so, cast the object to get info
                                var FieldInfo = (System.Reflection.FieldInfo)FIELD;

                                // Save the name and value if key is unique (should always be)
                                if (ConstantNamesAndValues.ContainsKey(FieldInfo.Name) == false)
                                {
                                    ConstantNamesAndValues.Add(FieldInfo.Name, FieldInfo.GetValue(null));
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        // Just log errors
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                    }

                    // Return the info
                    return ConstantNamesAndValues;
                }
                catch (Exception ex)
                {
                    // Return nothing if there's a high level failure
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }

        }

        public struct Enumerations
        {

            /// <summary>
            /// Used to return the string names for a specified enumeration type (GetType(EnumName)-VB / typeof(EnumName)-C#).
            /// </summary>
            /// <param name="EnumType">The enumeration type to check (GetType(EnumName)-VB / typeof(EnumName)-C#).</param>
            /// <returns>Returns list of strings containing all enumeration item string names if found. Returns NOTHING if error occurs.</returns>
            public static List<string> GetEntryNames(System.Type EnumType)
            {
                try
                {
                    // Return nothing if type is missing
                    if (EnumType == null) return null;

                    // Create list to return
                    var NamesToReturn = new List<string>();

                    // Loop through and get all names
                    foreach (var ITEM in System.Enum.GetNames(EnumType))
                    {
                        // Add to list
                        NamesToReturn.Add(ITEM);
                    }

                    // Return the list
                    return NamesToReturn;
                }
                catch (Exception ex)
                {
                    // Return nothing if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }
            /// <summary>
            /// Used to return the integer values for a specified enumeration type (GetType(EnumName)-VB / typeof(EnumName)-C#).
            /// </summary>
            /// <param name="EnumType">The enumeration type to check (GetType(EnumName)-VB / typeof(EnumName)-C#).</param>
            /// <returns>Returns list of 64 bit integers representing the enumeration values if found. Returns NOTHING if error occurs.</returns>
            public static List<long> GetEntryValues(System.Type EnumType)
            {
                try
                {
                    // Return nothing if type is missing
                    if (EnumType == null) return null;

                    // Create list to return
                    var ValuesToReturn = new List<long>();

                    // Loop through and get all names
                    foreach (var ITEM in System.Enum.GetValues(EnumType))
                    {
                        // Add to list
                        ValuesToReturn.Add(System.Convert.ToInt64(ITEM));
                    }

                    // Return the list
                    return ValuesToReturn;
                }
                catch (Exception ex)
                {
                    // Return nothing if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }
            /// <summary>
            /// Used to return a paired list of all enumeration names and values for a specified enumeration type (GetType(EnumName)-VB / typeof(EnumName)-C#).
            /// </summary>
            /// <param name="EnumType">The enumeration type to check (GetType(EnumName)-VB / typeof(EnumName)-C#).</param>
            /// <returns>Returns a dictionary mapping all enumeration names and values if found. Returns NOTHING if error occurs.</returns>
            public static Dictionary<string, long> GetEntryNamesAndValues(System.Type EnumType)
            {
                try
                {
                    // Get names
                    var Names = GetEntryNames(EnumType);

                    // Return nothing if can't be found
                    if (Names == null) return null;

                    // Get values
                    var Values = GetEntryValues(EnumType);

                    // Return nothing if can't be found
                    if (Values == null) return null;

                    // Return empty dictionary if there are no items
                    if (Names.Count == 0) return new Dictionary<string, long>();

                    // Return nothing if there is a count mismatch
                    if (Names.Count != Values.Count) return null;

                    // If counts match, create dictionary
                    var DictionaryToReturn = new Dictionary<string, long>();

                    // Add items
                    for (int i = 0; i <= Names.Count - 1; i++)
                    {
                        DictionaryToReturn.Add(Names[i], Values[i]);
                    }

                    // Return the dictionary
                    return DictionaryToReturn;
                }
                catch (Exception ex)
                {
                    // Return nothing if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }

        }

        public struct EmbeddedResources
        {

            /// <summary>
            /// Used to extract all embedded resources from the running assembly and return the file name and bytes for each resource.
            /// </summary>
            public static Dictionary<string, List<byte>> ExtractAll()
            {
                // Use helper with no restriction on file type
                return ExtractHelper(string.Empty);
            }
            /// <summary>
            /// Used to extract all embedded DLL resources from the running assembly and return the file name and bytes for each DLL.
            /// </summary>
            public static Dictionary<string, List<byte>> ExtractAllDLLs()
            {
                // Use helper with DLL extension
                return ExtractHelper(".dll");
            }
            /// <summary>
            /// Used to extract all embedded resources of a given file type from the running assembly and return the file name and bytes for each resource.
            /// </summary>
            /// <param name="FileExtensionToFind">The file extension to restrict to when returning embedded resource info (i.e. '.dll', '.png', etc.).</param>
            public static Dictionary<string, List<byte>> ExtractByFileExtension(string FileExtensionToFind)
            {
                // Use helper with specified extension
                return ExtractHelper(FileExtensionToFind);
            }
            /// <summary>
            /// Used to extract a specific embedded resource by name and return its file content as a list of bytes.
            /// </summary>
            /// <param name="FileNameToFind">The file name of the embedded resource to fine.</param>
            public static List<byte> ExtractByFileName(string FileNameToFind)
            {
                try
                {
                    // Return nothing if input is bad
                    if (string.IsNullOrEmpty(FileNameToFind) == true) return null;

                    // If input is good, get all files
                    var AllFiles = ExtractAll();
                    if (AllFiles == null || AllFiles.Count == 0)
                    {
                        // Return nothing if nothing came back
                        return null;
                    }

                    // If we got files back, first check for exact match
                    if (AllFiles.ContainsKey(FileNameToFind) == true)
                    {
                        // If there's an exact match (preferable), return the bytes for it
                        return AllFiles[FileNameToFind];
                    }

                    // If an exact match isn't found, go through and try to find an exact match that's not case sensitive (next most preferable)
                    foreach (var PAIR in AllFiles)
                    {
                        if (PAIR.Key.ToUpper() == FileNameToFind.ToUpper())
                        {
                            // If a case-insensitive match is found (i.e. exact match except for casing), return it
                            return PAIR.Value;
                        }
                    }

                    // Lastly, if we can't find exact or case-insensitive match, look for a partial match
                    foreach (var PAIR in AllFiles)
                    {
                        if (PAIR.Key.ToUpper().Contains(FileNameToFind.ToUpper()) == true)
                        {
                            // If a partial match is found, return it
                            return PAIR.Value;
                        }
                    }

                    // If no match of any kind can be found, return nothing
                    return null;
                }
                catch (Exception ex)
                {
                    // Return nothing if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }

            /// <summary>
            /// Used to extract embedded resources from the running assembly and write them to disk.
            /// </summary>
            /// <param name="FileExtensionToRestrictTo">OPTIONAL:  Used to restrict to a specific file type (vs. extracting all).</param>
            /// <param name="FolderToExtractTo">OPTIONAL:  Used to specify a folder to extract to (will extract to the running assembly's directory if not specified).</param>
            public static bool ExtractToDisk(string FileExtensionToRestrictTo = "", string FolderToExtractTo = "")
            {
                // Use helper and return to disk
                return ExtractToDiskHelper(FileExtensionToRestrictTo, FolderToExtractTo);
            }

            /// <summary>
            /// Helper to extract embedded resources from the running assembly.
            /// </summary>
            private static Dictionary<string, List<byte>> ExtractHelper(string FileExtensionToRestrictTo)
            {
                try
                {
                    // Ensure file extension isn't null
                    if (string.IsNullOrEmpty(FileExtensionToRestrictTo) == true)
                    {
                        FileExtensionToRestrictTo = string.Empty;
                    }

                    // Ensure file extension doesn't have any dots or wild cards and make uppercase for ease of checking
                    FileExtensionToRestrictTo = FileExtensionToRestrictTo.Replace(".", string.Empty);
                    FileExtensionToRestrictTo = FileExtensionToRestrictTo.Replace("*", string.Empty);
                    FileExtensionToRestrictTo = FileExtensionToRestrictTo.ToUpper();

                    // Create dictionary to hold info to return
                    var AllEmbeddedResources = new Dictionary<string, List<byte>>();

                    // Get all embedded resources within this assembly
                    var AllResources = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceNames();

                    // Loop through all the embedded resources and get info
                    foreach (var RESOURCE in AllResources)
                    {
                        // Split by "." b/c the first item will be parent assembly name (i.e. DotNetLib.System.My.dll, etc.)
                        var SplitByDot = RESOURCE.Split(new[] { "." }, StringSplitOptions.None).ToList();

                        // Create var to hold the file name to use (default to full resource name)
                        string FileNameToUse = RESOURCE;
                        if (SplitByDot.Count > 1)
                        {
                            // If there were items, join all but first one back together to trim off parent assembly name
                            FileNameToUse = string.Join(".", SplitByDot.GetRange(1, SplitByDot.Count - 1));
                        }

                        // Once we have a file name, check if we're restricting to a particular file type
                        if (string.IsNullOrEmpty(FileExtensionToRestrictTo) == false)
                        {
                            // If we are, skip this file if it doesn't have an extension
                            if (System.IO.Path.HasExtension(FileNameToUse) == false) continue;

                            // If we are restricting AND the file in question has an extension, get it
                            string ExtensionForThisFile = System.IO.Path.GetExtension(FileNameToUse).ToUpper().Replace(".", string.Empty);

                            // Skip this file if the extension isn't a match
                            if (FileExtensionToRestrictTo != ExtensionForThisFile) continue;
                        }

                        // If this is a resource we need to return, create stream to read the bytes for the resource
                        using (var Stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(RESOURCE))
                        {
                            // Create byte buffer to hold bytes
                            var StreamBytes = new byte[System.Convert.ToInt32(Stream.Length - 1) + 1];

                            // Read bytes into the buffer
                            Stream.Read(StreamBytes, 0, StreamBytes.Length);

                            // Add the file name and bytes to the dictionary
                            AllEmbeddedResources.Add(FileNameToUse, StreamBytes.ToList());
                        }
                    }

                    // Return the dictionary once populated
                    return AllEmbeddedResources;
                }
                catch (Exception ex)
                {
                    // Return nothing if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }

            /// <summary>
            /// Helper to extract files from the running assembly and write them to disk.
            /// </summary>
            private static bool ExtractToDiskHelper(string FileExtensionToRestrictTo, string FolderToExtractTo)
            {
                try
                {
                    // Get all files of the requested type
                    Dictionary<string, List<byte>> AllFilesToExport = ExtractHelper(FileExtensionToRestrictTo);
                    if (AllFilesToExport == null)
                    {
                        // Return false if query fails
                        return false;
                    }

                    // If there are no files, return true (nothing left to do)
                    if (AllFilesToExport.Count == 0) return true;

                    // If we have files, save the folder path to write to
                    string FolderToWriteTo = FolderToExtractTo;
                    if (string.IsNullOrEmpty(FolderToWriteTo) == true)
                    {
                        // If there is no override path, use the running path 
                        FolderToWriteTo = GetRootFolderOfExecutingAssembly();
                    }

                    // Create flag to hold if any errors occur (will try to write all files before bailing
                    bool AllFilesWrittenSuccessfully = true;   // Default to true, will be cleared if any fail

                    // Loop through and attempt to write all files
                    foreach (var PAIR in AllFilesToExport)
                    {
                        try
                        {
                            // Try to write all files
                            System.IO.File.WriteAllBytes(System.IO.Path.Combine(FolderToWriteTo, PAIR.Key), PAIR.Value.ToArray());
                        }
                        catch (Exception ex)
                        {
                            // If any file fails, clear flag
                            System.Diagnostics.Trace.WriteLine(ex.Message);
                            AllFilesWrittenSuccessfully = false;
                        }
                    }

                    // Return the result when done
                    return AllFilesWrittenSuccessfully;
                }
                catch (Exception ex)
                {
                    // Return false if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }

            /// <summary>
            /// Internal helper to get the current 'running' directory of the assembly (i.e. to put the DLL in)
            /// </summary>
            private static string GetRootFolderOfExecutingAssembly()
            {
                try
                {
                    // Try to use reflection to figure out where app is running from
                    return System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                }
                catch (Exception)
                {
                    // No action needed on failure
                }

                try
                {
                    // If reflection doesn't work, try using startup info from environment (first cmd line arg is normally the path to the running process)
                    return System.IO.Path.GetDirectoryName(Environment.GetCommandLineArgs()[0]);
                }
                catch (Exception)
                {
                    // No action needed on failure
                }

                // If nothing works, return empty string
                return string.Empty;
            }

        }

    }

}
