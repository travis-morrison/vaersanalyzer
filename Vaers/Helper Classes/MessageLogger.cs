﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Utilities
{

    public class MessageLogger : IDisposable
    {

        #region Sub Classes


        public class ExternalFile
        {
            /// <summary>
            /// The root folder to save log files in.
            /// </summary>
            public string RootFolder = string.Empty;

            /// <summary>
            /// The base file name to use for the log (the file will be rolled daily with a YYYY-MM-DD date stamp applied to it).
            /// </summary>
            public string BaseFileName = string.Empty;

            /// <summary>
            /// Returns the full file path (with today's YYYY-MM-DD appended) for the log file.
            /// </summary>
            public string FullPath
            {
                get
                {
                    try
                    {
                        // Return an empty string if folder isn't set
                        if (string.IsNullOrEmpty(RootFolder) == true) return string.Empty;

                        // Return empty if file name isn't set
                        if (string.IsNullOrEmpty(BaseFileName) == true) return string.Empty;

                        // If the file name is set, get the name and extension
                        string FileName = System.IO.Path.GetFileNameWithoutExtension(BaseFileName);
                        string FileExt = System.IO.Path.GetExtension(BaseFileName);

                        // Get the YYYY-MM-DD timestamp for today
                        string YYYYMMDD = DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString().PadLeft(2, Convert.ToChar("0")) + "-" + DateTime.Now.Day.ToString().PadLeft(2, Convert.ToChar("0"));

                        // Merge the folder path and name
                        string PathToReturn = RootFolder + @"\" + FileName;

                        // Create constant to hold the file number / index that will be used to break the file into multiple files if it gets too big
                        const string FileIndexMarker = "{FILE-NUM}";

                        // Add the date stamp with dot prefix and index marker place holder (e.g. "C:\Test\SomeFile.YYYY-MM-DD.{FILE-NUM}")
                        PathToReturn += "." + YYYYMMDD + "." + FileIndexMarker;

                        // Bolt log extension on
                        PathToReturn += ".log";

                        // Create constant to hold how many digits to pad the file number / index marker digits to (e.g. 00000, 00001, etc.)
                        const int PadNumberStringLength = 5;

                        // Create string to hold what the name of the first file should be (i.e. ".00000")
                        string FirstFile = PathToReturn.Replace(FileIndexMarker, new string(Convert.ToChar("0"), PadNumberStringLength));

                        // Check if the first file has been created yet
                        if (System.IO.File.Exists(FirstFile) == false)
                        {
                            // If not, just return that path
                            return FirstFile;
                        }

                        // If it has been created, need to loop through and find the newest file to return
                        int NewestFileIndex = 0;
                        for (int i = 1; i <= int.MaxValue; i++)
                        {
                            // Create the full file path
                            string FullFilePath = PathToReturn.Replace(FileIndexMarker, i.ToString().PadLeft(PadNumberStringLength, Convert.ToChar("0")));

                            // Check if the file exists
                            if (System.IO.File.Exists(FullFilePath) == false)
                            {
                                // Once the first file is not found, the newest file will be the preceding one
                                NewestFileIndex = i - 1;
                                break;
                            }
                        }

                        // Build the file path
                        string PathToNewestFile = PathToReturn.Replace(FileIndexMarker, NewestFileIndex.ToString().PadLeft(PadNumberStringLength, Convert.ToChar("0")));

                        // If there is no limit to file size, just return the file
                        if (MaxFileSizeInMB == 0) return PathToNewestFile;

                        // If there is a limit, get the size of the file
                        long FileSizeInBytes = new System.IO.FileInfo(PathToNewestFile).Length;

                        // If it's under the limit, return it
                        if (FileSizeInBytes / Math.Pow(10, 6) < MaxFileSizeInMB) return PathToNewestFile;

                        // If it's at or over the limit, return path to the NEXT file
                        return PathToReturn.Replace(FileIndexMarker, (NewestFileIndex + 1).ToString().PadLeft(PadNumberStringLength, Convert.ToChar("0")));
                    }
                    catch (Exception ex)
                    {
                        // Return an empty string if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return string.Empty;
                    }
                }
            }

            /// <summary>
            /// How many messages should be batched before writing to file to minimize file access (e.g. 1 = log every time, 10 = log messages in batches of 10).
            /// </summary>
            public UInt16 MsgBatchSizeToWrite = DefaultMaxBatchSize;

            /// <summary>
            /// Used to specify the max file size. Once exceeded, an addition file with a ".n+1" naming convention will be created. Using "0" indicates there is no max file size.
            /// </summary>
            public UInt16 MaxFileSizeInMB = 0;

            // Private constants for holding default msg batch size and max logs
            private const UInt16 DefaultMaxBatchSize = 10;

            // Allow setting in constructor
            public ExternalFile(string InputRootFolder = "", string InputBaseFileName = "", UInt16 InputMsgBatchSizeToWrite = DefaultMaxBatchSize, UInt16 InputMaxFileSizeInMB = 0)
            {
                // Save inputs
                RootFolder = InputRootFolder;
                BaseFileName = InputBaseFileName;
                MsgBatchSizeToWrite = InputMsgBatchSizeToWrite;
                MaxFileSizeInMB = InputMaxFileSizeInMB;
            }
        }


        #endregion

        #region Declarations


        /// <summary>
        /// Used to enable / disable logging to System.Diagnostic.Debug.
        /// </summary>
        public bool IsDebugLoggingEnabled = true;

        /// <summary>
        /// Used to enable / disable logging to System.Diagnostic.Trace.
        /// </summary>
        public bool IsTraceLoggingEnabled = true;

        /// <summary>
        /// Used to enable / disable logging to an text file.
        /// </summary>
        public ExternalFile FileLoggingOptions = null;

        /// <summary>
        /// Used to check if file logging is currently enabled.
        /// </summary>
        public bool IsFileLoggingEnabled
        {
            get
            {
                // Check if we have file logging options set
                if (FileLoggingOptions != null)
                {
                    // If there are, return true
                    return true;
                }
                else
                {
                    // If not, return false
                    return false;
                }
            }
        }

        /// <summary>
        /// Used to throttle how much time (in seconds) has to pass before an unfull message queue will be flushed / written out (to prevent orphaned messages).
        /// </summary>
        public UInt16 IdleTimeInSecondsBeforeWritingIncompleteQueue = 5;

        // Internal var to hold message queue for writing to file (keep time msg was added so queue can be flushed occasionally if no new messages are coming in)
        private List<KeyValuePair<DateTime, string>> MsgQueue = new List<KeyValuePair<DateTime, string>>();
        private object MsgQueueLock = new object();

        // Var to hold if immediate request to write to disk has been made
        private bool WriteQueueToDiskNow = false;

        // Local var for controlling / killing thread
        private bool KeepThreadRunning = true;


        #endregion

        #region Load & Unload Methods


        /// <summary>
        /// Creates new instance of the logger utility.
        /// </summary>
        public MessageLogger()
        {
            // Default constructor requires no parameters
            NewCommon();
        }
        /// <summary>
        /// Creates new instance of the logger utility with debug / trace logging enabled or disabled.
        /// </summary>
        /// <param name="EnableDebugLogging">Used to turn System.Diagnostic.Debug logging on or off.</param>
        /// <param name="EnableTraceLogging">Used to turn System.Diagnostic.Trace logging on or off.</param>
        public MessageLogger(bool EnableDebugLogging, bool EnableTraceLogging)
        {
            // Allow user to set debug / trace logs in constructor
            IsDebugLoggingEnabled = EnableDebugLogging;
            IsTraceLoggingEnabled = EnableTraceLogging;

            // Call common method
            NewCommon();
        }
        /// <summary>
        /// Creates new instance of the logger utility with debug / trace logging enabled or disabled.
        /// </summary>
        /// <param name="EnableDebugLogging">Used to turn System.Diagnostic.Debug logging on or off.</param>
        /// <param name="EnableTraceLogging">Used to turn System.Diagnostic.Trace logging on or off.</param>
        /// <param name="ExternalFileLoggingOptions">Used to set the external file logging options (i.e. to turn file logging on or off).</param>
        public MessageLogger(bool EnableDebugLogging, bool EnableTraceLogging, ExternalFile ExternalFileLoggingOptions)
        {
            // Allow user to set debug / trace logs in constructor
            IsDebugLoggingEnabled = EnableDebugLogging;
            IsTraceLoggingEnabled = EnableTraceLogging;

            // Save file logging options
            FileLoggingOptions = ExternalFileLoggingOptions;

            // Call common method
            NewCommon();
        }
        private void NewCommon()
        {
            // Launch thread to watch msg queue and write external log if user specifies log file
            var thrWatchQueue = new System.Threading.Thread(() => BkrndWatchAndWriteMsgQueue());
            thrWatchQueue.SetApartmentState(System.Threading.ApartmentState.STA);
            thrWatchQueue.IsBackground = true;
            thrWatchQueue.Start();
        }

        /// <summary>
        /// Cleans up all resources used by the object.
        /// </summary>
        public void Dispose()
        {
            // Kill thread
            KeepThreadRunning = false;
        }


        #endregion

        #region Exposed Methods


        /// <summary>
        /// Writes the specified message to any log listeners that are enabled (i.e. Debug, Trace, External File, etc.).
        /// </summary>
        /// <param name="Msg">The message to append to the log.</param>
        public void WriteLine(string Msg)
        {
            try
            {
                // Log to debug if requested
                if (IsDebugLoggingEnabled == true) System.Diagnostics.Debug.WriteLine(Msg);

                // Log to trace if requested
                if (IsTraceLoggingEnabled == true) System.Diagnostics.Trace.WriteLine(Msg);

                // Exit if there is no log to file options
                if (FileLoggingOptions == null) return;

                // Exit if log file options are missing info
                if (string.IsNullOrEmpty(FileLoggingOptions.FullPath) == true) return;

                // If everything looks good, add msg to queue async to prevent locking from potentially blocking the calling thread
                var thrAddToQeueue = new System.Threading.Thread(() => BkrndAddMsgToQueue(Msg));
                thrAddToQeueue.SetApartmentState(System.Threading.ApartmentState.STA);
                thrAddToQeueue.IsBackground = true;
                thrAddToQeueue.Start();
            }
            catch (Exception)
            {
                // Ignore errors
            }
        }

        /// <summary>
        /// Used to immediately write anything left in the queue to disk.
        /// </summary>
        public void WriteQueueToDiskImmediately()
        {
            // Bail if we aren't currently writing log msgs to disk
            if (IsFileLoggingEnabled == false) return;

            // Bail if we already have the flag set
            if (WriteQueueToDiskNow == true) return;

            // If we don't have the flag set, set it
            WriteQueueToDiskNow = true;

            // Capture time to hold when flush request was made
            var FlushRequestedAt = DateTime.Now;

            // Wait a little for cross thread safety
            System.Threading.Thread.Sleep(25);

            // Wait until flag is cleared
            while (WriteQueueToDiskNow == true)
            {
                // Wait in small increments
                System.Threading.Thread.Sleep(10);

                // Bail if it's been really long (don't want to hang forever)
                if ((DateTime.Now - FlushRequestedAt).TotalSeconds > 1)
                {
                    WriteQueueToDiskNow = false;
                    return;
                }
            }
        }

        /// <summary>
        /// Used to delete old logs from the logging folder.
        /// </summary>
        /// <param name="MaxLogsToKeep">The maximum number of logs / history to keep.</param>
        public void DeleteOldLogs(UInt16 MaxLogsToKeep)
        {
            try
            {
                // Bail if we don't have logging options
                if (FileLoggingOptions == null) return;

                // Bail if we don't have a root folder
                if (string.IsNullOrEmpty(FileLoggingOptions.RootFolder) == true) return;
                if (System.IO.Directory.Exists(FileLoggingOptions.RootFolder) == false) return;

                // Get the file extension being used on the log files
                string FileExt = System.IO.Path.GetExtension(FileLoggingOptions.FullPath);
                if (string.IsNullOrEmpty(FileExt) == true)
                {
                    // Always ensure we're not null
                    FileExt = string.Empty;
                }

                // Remove any dots (can never remember if that comes back with GetExtension() or not
                FileExt = FileExt.Replace(".", string.Empty);

                // Finally, if we have anything left, add the wildcard w/ dot to it
                if (string.IsNullOrEmpty(FileExt) == false)
                {
                    FileExt = "*." + FileExt;
                }

                // Create var to hold all log file paths
                List<string> AllLogs = null;

                // Check if we have an extension filter
                if (string.IsNullOrEmpty(FileExt) == false)
                {
                    // If there's an extension filter, use it
                    AllLogs = System.IO.Directory.GetFiles(FileLoggingOptions.RootFolder, FileExt).ToList();
                }
                else
                {
                    // If not, get everything
                    AllLogs = System.IO.Directory.GetFiles(FileLoggingOptions.RootFolder).ToList();
                }

                // Bail if we're already under the cutoff
                if (AllLogs.Count <= MaxLogsToKeep) return;

                // If we're not, delete older logs
                while (AllLogs.Count > MaxLogsToKeep)
                {
                    // Delete the oldest log and remove from array for next loop
                    StaticMethods.FileSystem.DeleteFile(AllLogs[0]);
                    AllLogs.RemoveAt(0);
                }
            }
            catch (Exception ex)
            {
                // Just log errors
                System.Diagnostics.Trace.WriteLine(ex.Message);
            }
        }


        #endregion

        #region Helper Methods


        private void BkrndAddMsgToQueue(string Msg)
        {
            try
            {
                // Lock up queue to prevent multi-access
                lock (MsgQueueLock)
                {
                    MsgQueue.Add(new KeyValuePair<DateTime, string>(DateTime.Now, Msg));
                }
            }
            catch (Exception)
            {
                // Ignore errors
            }
        }

        private void BkrndWatchAndWriteMsgQueue()
        {
            // Keep running until dispose is called
            while (KeepThreadRunning == true)
            {
                try
                {
                    // Just keep looping if no file logging options have been saved yet
                    if (FileLoggingOptions == null) continue;

                    // Just keep looping if there are file options but path info isn't complete
                    if (string.IsNullOrEmpty(FileLoggingOptions.FullPath) == true) continue;

                    // If there is file logging info, ensure that the folder exists
                    if (System.IO.Directory.Exists(FileLoggingOptions.RootFolder) == false)
                    {
                        System.IO.Directory.CreateDirectory(FileLoggingOptions.RootFolder);
                    }

                    // Lock up the queue while checking / writing data
                    lock (MsgQueueLock)
                    {
                        // Just keep looping if there are no messages
                        if (MsgQueue == null || MsgQueue.Count == 0)
                        {
                            // Clear flag if immediate file write was requested as there is nothing in queue to write
                            WriteQueueToDiskNow = false;
                            continue;
                        }

                        // If there are messages, check the last time a msg was added and save the max batch size to hold before writing
                        DateTime LastMsgAdded = MsgQueue[MsgQueue.Count - 1].Key;
                        int MaxBatchSize = FileLoggingOptions.MsgBatchSizeToWrite;

                        // Always ensure that the idle time setting is valid first
                        if (IdleTimeInSecondsBeforeWritingIncompleteQueue == 0)
                        {
                            IdleTimeInSecondsBeforeWritingIncompleteQueue = 1;
                        }

                        // Log all msgs to disk if immediate write was requested OR max count has been hit OR it's been a while (to prevent old msgs from just sitting in queue forever)
                        if (WriteQueueToDiskNow == true || MsgQueue.Count >= MaxBatchSize || (DateTime.Now - LastMsgAdded).TotalSeconds >= IdleTimeInSecondsBeforeWritingIncompleteQueue)
                        {
                            // If there's enough lines or it's been a while, log the data
                            using (var Writer = new System.IO.StreamWriter(FileLoggingOptions.FullPath, true))
                            {
                                // Loop through and log each line
                                foreach (var LINE in MsgQueue)
                                {
                                    Writer.NewLine = Environment.NewLine;
                                    Writer.WriteLine(LINE.Value);
                                }
                            }

                            // Always clear write now flag so method that requested it can end
                            WriteQueueToDiskNow = false;

                            // Clear out the msg queue for new msgs
                            MsgQueue.Clear();
                        }
                    }
                }
                catch (Exception)
                {
                    // No action needed on failure
                }

                finally
                {
                    // Wait a little between checks
                    for (int i = 1; i <= 200; i++)
                    {
                        // Do a bunch of small checks so we can purge log if requested
                        System.Threading.Thread.Sleep(50);

                        // Immediately go back into while loop if write to disk now has been requested
                        if (WriteQueueToDiskNow == true)
                        {
                            break;
                        }
                    }
                }
            }
        }


        #endregion

    }

}
