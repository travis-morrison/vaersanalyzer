﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Vaers
{

    public partial class ReportView : Window
    {

        #region Declarations


        // Vars to hold passed in inputs
        private DatabaseManager InputRawData = null;
        private DatabaseManager.VaccineEventReport InputFullReport = null;
        private Action<string> InputCallbackReportStatus = null;

        // Vars to hold handle to charts
        private System.Windows.Forms.DataVisualization.Charting.Chart CHARTCumulative = new System.Windows.Forms.DataVisualization.Charting.Chart();
        private System.Windows.Forms.DataVisualization.Charting.Chart CHARTCumulativeNormalized = new System.Windows.Forms.DataVisualization.Charting.Chart();
        private System.Windows.Forms.DataVisualization.Charting.Chart CHARTYearly = new System.Windows.Forms.DataVisualization.Charting.Chart();
        private System.Windows.Forms.DataVisualization.Charting.Chart CHARTMonthly = new System.Windows.Forms.DataVisualization.Charting.Chart();
        private System.Windows.Forms.DataVisualization.Charting.Chart CHARTSymptomsByMfg = new System.Windows.Forms.DataVisualization.Charting.Chart();
        private System.Windows.Forms.DataVisualization.Charting.Chart CHARTSymptoms = new System.Windows.Forms.DataVisualization.Charting.Chart();


        #endregion

        #region Load & Unload Methods


        public ReportView(DatabaseManager RawData, DatabaseManager.VaccineEventReport FullReport, Action<string> MethodToReportStatusMsgsTo)
        {
            // Draw the window
            InitializeComponent();

            // Save inputs
            InputRawData = RawData;
            InputFullReport = FullReport;
            InputCallbackReportStatus = MethodToReportStatusMsgsTo;

            // Fail if inputs are missing
            if (InputRawData == null || InputFullReport == null)
            {
                throw new Exception("Inputs are required to view report!");
            }

            // Hide CSV button for now, not implemented fast enough yet
            BTNToCsv.Visibility = Visibility.Hidden;

            // Wire up events
            this.Closed += ReportView_Closed;
            BTNToCsv.Click += BTNToCsv_Click;
            LBVaccineList.SelectionChanged += LBVaccineList_SelectionChanged;
            LBLVaxDetails.MouseLeftButtonDown += LBLVaxDetails_MouseLeftButtonDown;
            LBLSearch.MouseLeftButtonDown += LBLSearch_MouseLeftButtonDown;

            // Bind charts to WF hosts
            WFHostChartCumulative.Child = CHARTCumulative;
            WFHostChartCumulativeAvgPerMonth.Child = CHARTCumulativeNormalized;
            WFHostChartYearly.Child = CHARTYearly;
            WFHostChartMonthly.Child = CHARTMonthly;
            WFHostChartSymptomsByMfg.Child = CHARTSymptomsByMfg;
            WFHostChartSymptoms.Child = CHARTSymptoms;

            // Populate charts
            PopulateReport();
        }

        private void ReportView_Closed(object sender, EventArgs e)
        {
            // Dispose of WF charts
            foreach (var WF in new[] { CHARTCumulative, CHARTYearly, CHARTMonthly, CHARTSymptomsByMfg, CHARTSymptoms })
            {
                if (WF != null) WF.Dispose();
            }

            // Dispose of WF hosts
            foreach (var WF in new[] { WFHostChartCumulative, WFHostChartYearly, WFHostChartMonthly, WFHostChartSymptomsByMfg, WFHostChartSymptoms })
            {
                if (WF != null) WF.Dispose();
            }
        }


        #endregion

        #region Button Press Methods


        private async void BTNToCsv_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Remove user control
                SetUserCtrl(false);

                // Have user select save path
                string SavePath = StaticMethods.FileSystem.RequestSavePath("Save As (CSV)...", ".csv", true);
                if (string.IsNullOrEmpty(SavePath) == true)
                {
                    // Bail if user cancels
                    return;
                }

                // If file is already there, delete it
                StaticMethods.FileSystem.DeleteFile(SavePath);

                // Get the total chunk count
                int ChunkCount = InputRawData.GetCsvChunkCount();

                // Report status
                ThisApp.Methods.Logging.WriteMsg("Need to extract [ " + ChunkCount + " ] CSV chunks...", ThisApp.Methods.Logging.MsgTypeOptions.Info);
                ReportStatusMsg("Processing CSV in [ " + ChunkCount + " ] chunks...");

                // Loop through and get all chunks
                for (int i = 0; i < ChunkCount; i++)
                {
                    // Report status
                    ThisApp.Methods.Logging.WriteMsg("Processing CSV chunk [ " + (i + 1).ToString() + " ] of [ " + ChunkCount + " ]...", ThisApp.Methods.Logging.MsgTypeOptions.Info);
                    ReportStatusMsg("Processing chunk [ " + (i + 1).ToString() + " ] of [ " + ChunkCount + " ]...");

                    // Get the next chunk
                    var NextChunk = await InputRawData.GetCsvChunkAsync(i);
                    if (NextChunk == null)
                    {
                        // Report error if we can't get the chunk
                        ThisApp.Methods.UserPrompts.ShowErrorMsg("Error Extracting CSV Chunk!", "An error occurred extracting data for CSV!");
                        return;
                    }

                    // If we got the chunk, get as merged list
                    ThisApp.Methods.Logging.WriteMsg("Converting 2D array to 1D CSV list...", ThisApp.Methods.Logging.MsgTypeOptions.Info);
                    var MergedList = Convert2dToSingleList(NextChunk);

                    // Report status
                    ThisApp.Methods.Logging.WriteMsg("Appending data to file...", ThisApp.Methods.Logging.MsgTypeOptions.Info);
                    ReportStatusMsg("Appending CSV chunk to file...");

                    // If we get it, write it to file using append
                    StaticMethods.TextFile.Write(SavePath, string.Join(Environment.NewLine, MergedList), true);
                }
            }
            finally
            {
                // Restore control when done
                SetUserCtrl(true);
            }
        }

        private void LBLVaxDetails_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            // Exit if no vax is selected
            if (LBVaccineList.SelectedIndex == -1) return;

            // If we have a vax, save the name
            string VaxName = LBVaccineList.SelectedItem.ToString();

            // Create new report view and handoff info
            new VaxDetailedView(InputRawData, InputFullReport, VaxName, InputCallbackReportStatus).Show();
        }

        private void LBLSearch_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            // Get the currently selected vax
            object SelectedVax = LBVaccineList.SelectedItem;
            if (SelectedVax == null)
            {
                // Bail if nothing is selected
                return;
            }

            // If we have a selection, get all manufacturers for this var
            var AllManufacturers = (from AllInfo in InputRawData.Records where AllInfo.VaxName == SelectedVax.ToString() select AllInfo.VaxMfg).Distinct().ToList();
            AllManufacturers.Sort();

            // Once we have them, request search options
            var SearchOptions = SymptomSearchView.RequestSearchOptions(AllManufacturers);
            if (SearchOptions == null)
            {
                // Bail if user cancels
                return;
            }

            // If we get search options get the list based on the search criteria first
            var Matches = (from AllInfo in InputRawData.Records where AllInfo.VaxName == SelectedVax.ToString() && StaticMethods.Enumerables.DoesListContainItem(AllInfo.Symptoms, SearchOptions.SearchWord, false, false) select AllInfo).ToList();

            // Restrict to manufacturer if requested
            if (string.IsNullOrEmpty(SearchOptions.RestrictToMfg) == false)
            {
                Matches = (from AllInfo in Matches where AllInfo.VaxMfg == SearchOptions.RestrictToMfg select AllInfo).ToList();
            }

            // Show results
            new SearchResultsView(Matches).Show();
        }


        #endregion

        #region Helper Methods


        private void PopulateReport()
        {
            // Fill in header info
            LBLDateStart.Text = InputFullReport.StartDate;
            LBLDateEnd.Text = InputFullReport.StopDate;
            LBLTotalVaccines.Text = InputFullReport.Vaccines.Count.ToString();
            LBLTotalEvents.Text = ThisApp.Methods.Common.AddCommasToNumber(InputFullReport.EventCount, 0);

            // Create constant to hold the max number of vaccines to show in summary
            const UInt16 MaxVaxNamesToShow = 15;

            // Set the cumulative chart title
            LBLChartHeaderCumulative.Text = "Top " + MaxVaxNamesToShow + " Vaccines by Event Count from " + InputFullReport.StartDate + " to " + InputFullReport.StopDate;

            // Create list to populate cumulative chart
            var CumulativeData = new Dictionary<string, double>();
            foreach (var PAIR in InputFullReport.VaxEventsCumulative.SortedVaxNameToEventCount)
            {
                CumulativeData.Add(PAIR.Key, PAIR.Value);
            }

            // Create list to hold the yearly breakdown
            var YearlyData = new Dictionary<string, double>();
            foreach (var YEARLYBLOCK in InputFullReport.VaxEventsPerYear)
            {
                // Save the year
                string Year = YEARLYBLOCK.TimePeriod;

                // Sum all the events for that year
                double Sum = (from AllInfo in YEARLYBLOCK.VaxNameToEventCount select Convert.ToInt32(AllInfo.Value)).Sum();

                // Add to dictionary
                YearlyData.Add(Year, Sum);
            }

            // Create list to hold the monthly breakdown
            var MonthlyData = new Dictionary<string, double>();
            foreach (var YEARMONTHBLOCK in InputFullReport.VaxEventsPerMonth)
            {
                // Save the year
                string YearMonth = YEARMONTHBLOCK.TimePeriod;

                // Sum all the events for that year-mm
                double Sum = (from AllInfo in YEARMONTHBLOCK.VaxNameToEventCount select Convert.ToInt32(AllInfo.Value)).Sum();

                // Add to dictionary
                MonthlyData.Add(YearMonth, Sum);
            }

            // Convert dictionaries to list for charts
            var CumulativeDataForChart = ConvertDictionaryToKvpsForChart(CumulativeData, MaxItemsToKeep: MaxVaxNamesToShow);
            var YearlyDataForChart = ConvertDictionaryToKvpsForChart(YearlyData);
            var MonthlyDataForChart = ConvertDictionaryToKvpsForChart(MonthlyData);

            // Get the cumulative but normalized by month
            //var CumulativeNormalizedByMonth = ConvertDictionaryToKvpsForChart(InputFullReport.VaxNameToAvgMonthlyEventCount, MaxItemsToKeep: MaxVaxNamesToShow);

            var CumulativeNormalizedByMonth = InputFullReport.VaxNameToAvgMonthlyEventCount;
            if (CumulativeNormalizedByMonth.Count > MaxVaxNamesToShow)
            {
                // Restrict to max count
                CumulativeNormalizedByMonth = CumulativeNormalizedByMonth.Take(MaxVaxNamesToShow).ToList();
            }

            // Populate summary charts
            StaticMethods.WinForms.Charting.PopulateBarChart(CHARTCumulative, CumulativeDataForChart, true, BarColor: System.Drawing.Color.Blue, ShowNumericGridLines: true);
            StaticMethods.WinForms.Charting.PopulateBarChart(CHARTCumulativeNormalized, CumulativeNormalizedByMonth, true, BarColor: System.Drawing.Color.Blue, ShowNumericGridLines: true);
            StaticMethods.WinForms.Charting.PopulateBarChart(CHARTYearly, YearlyDataForChart, true, BarColor: System.Drawing.Color.Blue, ShowNumericGridLines: true);
            StaticMethods.WinForms.Charting.PopulateBarChart(CHARTMonthly, MonthlyDataForChart, true, BarColor: System.Drawing.Color.Blue, ShowNumericGridLines: true);

            // Fill in available vaccine list for period and select first item if there
            LBVaccineList.ItemsSource = InputFullReport.Vaccines;
            if (LBVaccineList.Items.Count > 0) LBVaccineList.SelectedIndex = 0;


        }
        private List<KeyValuePair<string, double>> ConvertDictionaryToKvpsForChart(Dictionary<string, double> DictionaryToConvert, Nullable<UInt16> MaxItemsToKeep = null)
        {
            // Convert dictionary to list
            var AsList = DictionaryToConvert.ToList();

            // If they want to restrict count, remove items
            if (MaxItemsToKeep != null && MaxItemsToKeep > 0)
            {
                while (AsList.Count > MaxItemsToKeep)
                {
                    AsList.RemoveAt(AsList.Count - 1);
                }
            }

            // Return the list
            return AsList;
        }
        private List<KeyValuePair<string, double>> ConvertKvpListForChart(List<KeyValuePair<string, uint>> ListToConvert, Nullable<UInt16> MaxItemsToKeep = null)
        {
            // Filter down input list if requested
            if (MaxItemsToKeep != null && MaxItemsToKeep > 0)
            {
                while (ListToConvert.Count > MaxItemsToKeep)
                {
                    ListToConvert.RemoveAt(ListToConvert.Count - 1);
                }
            }

            // Create list to return
            var ListToReturn = new List<KeyValuePair<string, double>>();
            foreach (var PAIR in ListToConvert)
            {
                ListToReturn.Add(new KeyValuePair<string, double>(PAIR.Key, PAIR.Value));
            }

            // Return the list
            return ListToReturn;
        }

        private List<string> Convert2dToSingleList(string[,] SourceArray)
        {
            // Save the row count
            int RowCount = SourceArray.GetUpperBound(0) + 1;

            // Create list big enough to hold
            var ListToReturn = new List<string>(RowCount + 5);

            // Loop through and merge rows
            for (int i = 0; i < RowCount; i++)
            {
                ListToReturn.Add(GetFullRowFrom2dAsCsvString(SourceArray, i));
            }

            // Return the list when built
            return ListToReturn;
        }
        private string GetFullRowFrom2dAsCsvString(string[,] SourceArray, int RowIndex)
        {
            // Save the column count
            int ColCount = SourceArray.GetUpperBound(1) + 1;

            // Create list to hold items (oversize for fun)
            var FullRowItems = new List<string>(ColCount + 5);

            // Get items
            for (int i = 0; i < ColCount; i++)
            {
                FullRowItems.Add(SourceArray[RowIndex, i]);
            }

            // Join and return
            return string.Join(",", FullRowItems);
        }

        private void SetUserCtrl(bool UserHasCtrl)
        {
            // Check if on main thread
            if (this.Dispatcher.CheckAccess() == false)
            {
                // If not, invoke main
                this.Dispatcher.Invoke(() => SetUserCtrl(UserHasCtrl));
            }
            else
            {
                // If on main, update control
                GRDMain.IsEnabled = UserHasCtrl;
            }
        }

        private void ReportStatusMsg(string Msg)
        {
            try
            {
                // Bail if there's no callback
                if (InputCallbackReportStatus == null) return;

                // If we have a callback, hit it
                InputCallbackReportStatus(Msg);
            }
            catch (Exception ex)
            {
                // If error occurs, just log and bail
                ThisApp.Methods.Logging.WriteMsgWithException("Error occurred while hitting callback to report status in " + nameof(DatabaseManager) + "!", ex);
            }
        }


        #endregion

        #region On Event Methods


        private async void LBVaccineList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                // Remove user control
                SetUserCtrl(false);

                // Clear old info first
                StaticMethods.WinForms.Charting.ClearChart(CHARTSymptoms);
                DGSymptomRecords.ItemsSource = null;

                // Bail if nothing is selected
                if (LBVaccineList.SelectedIndex == -1) return;

                // If we have something selected, save the vax name
                string VaxName = LBVaccineList.SelectedItem.ToString();

                // Get all records for the vax and show in grid
                var RecordsForVax = await InputRawData.LookupRecordsForVaxAsync(VaxName);
                DGSymptomRecords.ItemsSource = RecordsForVax;

                // Get counts by manufacturer
                var CountsForMfg = await InputRawData.LookupManufacturerEventCountsAsync(VaccineToRestrictTo: VaxName);

                // Convert for helper and populate chart
                var MfgDataForChart = ConvertKvpListForChart(CountsForMfg, MaxItemsToKeep: 15);

                // Chart the data
                StaticMethods.WinForms.Charting.PopulateBarChart(CHARTSymptomsByMfg, MfgDataForChart, true, BarColor: System.Drawing.Color.Blue, ShowNumericGridLines: true);

                // Get the counts for the symptoms
                var CountsForSymptoms = await InputRawData.LookupSymptomsAndCountsAsync(VaccineToRestrictTo: VaxName);

                // Show full list as markdown
                TBFullSymptomList.Text = ThisApp.Methods.Common.KvpListToMarkdown(CountsForSymptoms, "Symptom", "Count");

                // Convert to doubles for helper
                var SymptomDataForChart = ConvertKvpListForChart(CountsForSymptoms, MaxItemsToKeep: 30);

                // Chart the data
                StaticMethods.WinForms.Charting.PopulateBarChart(CHARTSymptoms, SymptomDataForChart, true, BarColor: System.Drawing.Color.Blue, ShowNumericGridLines: true);
            }
            finally
            {
                // Restore control when done
                SetUserCtrl(true);
            }
        }


        #endregion

    }

}
