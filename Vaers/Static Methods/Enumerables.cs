﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StaticMethods
{

    public struct Enumerables
    {

        /// <summary>
        /// Used to compare two enumerable lists item by item to determine if they are the same.
        /// </summary>
        /// <param name="EnumerableList1">The first list of items.</param>
        /// <param name="EnumerableList2">The second list of items.</param>
        /// <param name="SortBeforeChecking">Flag to indicate if the lists should be sorted before they are compared.</param>
        /// <returns>Returns TRUE if the lists are the same length and each item at each corresponding index of the lists are the same. Returns FALSE in all other conditions.</returns>
        public static bool AreEqual<T>(IEnumerable<T> EnumerableList1, IEnumerable<T> EnumerableList2, bool SortBeforeChecking)
        {
            try
            {
                // Return true if both lists are null
                if (EnumerableList1 == null && EnumerableList2 == null) return true;

                // Return false if one list is null and the other isn't
                if (EnumerableList1 == null && EnumerableList2 != null) return false;
                if (EnumerableList1 != null && EnumerableList2 == null) return false;

                // Return true if there are no items in either list
                if (EnumerableList1.Count() == 0 && EnumerableList2.Count() == 0) return true;

                // Return false if lists have different counts
                if (EnumerableList1.Count() != EnumerableList2.Count()) return false;

                // If enumerable list can be checked, convert to lists so they can be sorted if needed
                var List1 = EnumerableList1.ToList();
                var List2 = EnumerableList2.ToList();

                // If lists can be checked and are the same count, sort if user requested
                if (SortBeforeChecking == true)
                {
                    List1.Sort();
                    List2.Sort();
                }

                // Loop through and check all items in the list
                for (int i = 0; i <= List1.Count - 1; i++)
                {
                    if (List1[i].Equals(List2[i]) == false)
                    {
                        return false;
                    }
                }

                // If all items are the same, return true
                return true;
            }
            catch (Exception ex)
            {
                // Return false if unhandled error occurs
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Used to check if a specified enumerable list of strings contains a specified string.
        /// </summary>
        /// <param name="EnumerableListToSearch">The enumerable list of strings to search through.</param>
        /// <param name="StringToSearchFor">The string to check for in the enumerable list.</param>
        /// <param name="MatchWholeString">Flag to indicate if the item in the enumerable list must exactly match the search string OR if a the string to search for can be a substring of the item in the list.</param>
        /// <param name="MatchCase">Flag to indicate if the search is case sensitive.</param>
        /// <returns>Returns TRUE if the string (or substring depending on the selected options) was found. Returns FALSE in all other conditions.</returns>
        public static bool DoesListContainItem(IEnumerable<string> EnumerableListToSearch, string StringToSearchFor, bool MatchWholeString, bool MatchCase)
        {
            try
            {
                // Return false if the list is empty
                if (EnumerableListToSearch == null || EnumerableListToSearch.Count() == 0)
                {
                    return false;
                }

                // Check if case insensitive
                if (MatchCase == false)
                {
                    // If not, make search string upper case
                    if (string.IsNullOrEmpty(StringToSearchFor) == false)
                    {
                        StringToSearchFor = StringToSearchFor.ToUpper();
                    }

                    // Make each item in the list upper case
                    var UpperCaseList = new List<string>();
                    foreach (var ITEM in EnumerableListToSearch)
                    {
                        UpperCaseList.Add(ITEM.ToUpper());
                    }

                    // Overwrite the original list with the upper case values
                    EnumerableListToSearch = UpperCaseList;
                }

                // If looking for the whole string, do a basic check and return the result
                if (MatchWholeString == true)
                {
                    return EnumerableListToSearch.Contains(StringToSearchFor);
                }

                // If looking for a substring, loop through and see if any item in the list contains the search string
                foreach (var ITEM in EnumerableListToSearch)
                {
                    // Return true if the item is found
                    if (ITEM.Contains(StringToSearchFor) == true)
                    {
                        return true;
                    }
                }

                // If the entire list is search and the item isn't found, return false
                return false;
            }
            catch (Exception ex)
            {
                // Return false if error occurs
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Used to check if a list contains a subset of items.
        /// </summary>
        /// <param name="FullListToCheck">The full list of elements to search through for the specified pattern.</param>
        /// <param name="PatternToFind">The pattern to attempt to find.</param>
        /// <returns>Return TRUE if the pattern is found. Returns FALSE if the pattern is not found. Returns NOTHING if an error occurs.</returns>
        public static Nullable<bool> DoesListContainPattern<T>(List<T> FullListToCheck, List<T> PatternToFind)
        {
            try
            {
                // Check if list contains the sequence and return the result
                return !(PatternToFind.Except(FullListToCheck).Any());
            }
            catch (Exception ex)
            {
                // Return false if error occurs
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Used to create and return a new enumerable list of upper case strings from a source enumerable list of strings.
        /// </summary>
        /// <param name="EnumerableListToConvert">The enumerable list of strings to convert to upper case.</param>
        /// <returns>Returns a new list with the upper case representation of the strings. Returns NULL if the input enumerable list is null.</returns>
        public static List<string> ToUpper(IEnumerable<string> EnumerableListToConvert)
        {
            // Return nothing if input list is null
            if (EnumerableListToConvert == null) return null;

            // If input list is empty, return empty list
            if (EnumerableListToConvert.Count() == 0) return EnumerableListToConvert.ToList();

            // If there are items, create list to return
            var ListToReturn = new List<string>();

            // Loop through and convert items
            foreach (var ITEM in EnumerableListToConvert)
            {
                ListToReturn.Add(ITEM.ToUpper());
            }

            // Return the list
            return ListToReturn;
        }

        /// <summary>
        /// Used to create and return a new enumerable list of lower case strings from a source enumerable list of strings.
        /// </summary>
        /// <param name="EnumerableListOfStringsToMerge">The enumerable list of strings to convert to lower case.</param>
        /// <returns>Returns a new list with the lower case representation of the strings. Returns NULL if the input enumerable list is null.</returns>
        public static List<string> ToLower(IEnumerable<string> EnumerableListOfStringsToMerge)
        {
            // Return nothing if input list is null
            if (EnumerableListOfStringsToMerge == null) return null;

            // If input list is empty, return empty list
            if (EnumerableListOfStringsToMerge.Count() == 0) return EnumerableListOfStringsToMerge.ToList();

            // If there are items, create list to return
            var ListToReturn = new List<string>();

            // Loop through and convert items
            foreach (var ITEM in EnumerableListOfStringsToMerge)
            {
                ListToReturn.Add(ITEM.ToLower());
            }

            // Return the list
            return ListToReturn;
        }

        /// <summary>
        /// Used to take an enumerable list of strings and merge it into a single string with an optional / configurable delimiter between each item in the list.
        /// </summary>
        /// <param name="EnumerableListOfStrings">The enumerable list of strings to merge into a single string.</param>
        /// <param name="InterItemDelimiterToUse">OPTIONAL:  The string to delimit each item with before merging (new line, comma, etc.).</param>
        /// <returns>Returns a single string containing all items in the list (delimited by the optional string) if the merger is successful. Returns an empty string if the source list is empty.</returns>
        public static string MergeStrings(IEnumerable<string> EnumerableListOfStrings, string InterItemDelimiterToUse = "\r\n")
        {
            // Use common method and return result
            return Lists.ToStringItemsAndMerge(EnumerableListOfStrings, InterItemDelimiterToUse: InterItemDelimiterToUse);
        }

        /// <summary>
        /// Used to sort a specified enumerable list into a random order.
        /// </summary>
        /// <param name="EnumerableListToRandomize">The list of items to randomize.</param>
        /// <param name="RandomGeneratorToUse">The random generator to use (if not specified, one will be created).</param>
        /// <returns>Returns the source list in randomized order if successful. Returns NOTHING in all other conditions.</returns>
        public static List<T> Randomize<T>(IEnumerable<T> EnumerableListToRandomize, Random RandomGeneratorToUse = null)
        {
            try
            {
                // Return nothing if input is bad
                if (EnumerableListToRandomize == null) return null;

                // Return empty list if there are no items
                if (EnumerableListToRandomize.Count() == 0) return EnumerableListToRandomize.ToList();

                // If good, create var to generate randoms (default to user input)
                var RandomGen = RandomGeneratorToUse;

                // If user didn't pass in a randomizer to use, create one
                if (RandomGen == null) RandomGen = new Random();

                // Randomize and return
                return EnumerableListToRandomize.OrderBy(a => RandomGen.Next()).ToList();
            }
            catch (Exception ex)
            {
                // Return nothing if randomize fails
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return null;
            }
        }

        public struct Arrays
        {

            /// <summary>
            /// Used to transpose an array of any type.
            /// </summary>
            /// <typeparam name="T">Identifies the type of the array being transposed.</typeparam>
            /// <param name="SourceArray">The source array to transpose.</param>
            /// <returns>Returns a transposed copy of the passed in array if the conversion works. Returns NULL if the passed in array is null or the conversion fails.</returns>
            public static T[,] TransposeArray<T>(T[,] SourceArray)
            {
                try
                {
                    // Return nothing if nothing is passed in
                    if (SourceArray == null) return null;

                    // If something is passed in, create array to return
                    T[,] ArrToReturn = new T[SourceArray.GetUpperBound(1) + 1, SourceArray.GetUpperBound(0) + 1];

                    // Loop through and xfer over info
                    for (int ROW = 0; ROW <= SourceArray.GetUpperBound(0); ROW++)
                    {
                        for (int COL = 0; COL <= SourceArray.GetUpperBound(1); COL++)
                        {
                            // Copy over info
                            ArrToReturn[COL, ROW] = SourceArray[ROW, COL];
                        }
                    }

                    // Return the converted array
                    return ArrToReturn;
                }
                catch (Exception ex)
                {
                    // Return nothing if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }

            /// <summary>
            /// Used to transpose a STRING array.
            /// </summary>
            /// <param name="SourceStringArray">The 2D array to transpose.</param>
            /// <param name="refTargetTransposedStringArray">BYREF:  Array to write data to and return.</param>
            /// <returns>Returns TRUE if the array transposes successfully. Returns FALSE in all other conditions.</returns>
            public static bool TransposeStringArray(string[,] SourceStringArray, ref string[,] refTargetTransposedStringArray)
            {
                try
                {
                    // Invert size of target array
                    refTargetTransposedStringArray = new string[SourceStringArray.GetUpperBound(1) + 1, SourceStringArray.GetUpperBound(0) + 1];

                    // Loop through each item
                    for (int ROW = 0; ROW <= SourceStringArray.GetUpperBound(0); ROW++)
                    {
                        for (int COL = 0; COL <= SourceStringArray.GetUpperBound(1); COL++)
                        {
                            // Transpose the items
                            refTargetTransposedStringArray[COL, ROW] = SourceStringArray[ROW, COL];
                        }
                    }

                    // Return true if everything works
                    return true;
                }
                catch (Exception ex)
                {
                    // Return false if something fails
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }

            /// <summary>
            /// Used to transpose an OBJECT array.
            /// </summary>
            /// <param name="SourceObjectArray">The 2D array to transpose.</param>
            /// <param name="refTargetTransposedObjectArray">BYREF:  Array to write data to and return.</param>
            /// <returns>Returns TRUE if the array transposes successfully. Returns FALSE in all other conditions.</returns>
            public static bool TransposeObjectArray(object[,] SourceObjectArray, ref object[,] refTargetTransposedObjectArray)
            {
                try
                {
                    // Invert size of target array
                    refTargetTransposedObjectArray = new object[SourceObjectArray.GetUpperBound(1) + 1, SourceObjectArray.GetUpperBound(0) + 1];

                    // Loop through each item
                    for (int ROW = 0; ROW <= SourceObjectArray.GetUpperBound(0); ROW++)
                    {
                        for (int COL = 0; COL <= SourceObjectArray.GetUpperBound(1); COL++)
                        {
                            // Transpose the items
                            refTargetTransposedObjectArray[COL, ROW] = SourceObjectArray[ROW, COL];
                        }
                    }

                    // Return true if everything works
                    return true;
                }
                catch (Exception ex)
                {
                    // Return false if something fails
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }

            /// <summary>
            /// Used to convert a 2D array of strings into a 2D array of objects.
            /// </summary>
            /// <param name="SourceArray">The source array to convert.</param>
            /// <param name="refTargetArray">BYREF:  The array to write the converted values to and return.</param>
            /// <returns>Returns TRUE if the array is converted successfully. Returns FALSE in all other conditions.</returns>
            public static bool ConvertToObjectArray(string[,] SourceArray, ref object[,] refTargetArray)
            {
                try
                {
                    // Make object array same size as string array
                    refTargetArray = new object[SourceArray.GetUpperBound(0) + 1, SourceArray.GetUpperBound(1) + 1];

                    // Loop through each item
                    for (int ROW = 0; ROW <= SourceArray.GetUpperBound(0); ROW++)
                    {
                        for (int COL = 0; COL <= SourceArray.GetUpperBound(1); COL++)
                        {
                            // Convert each string entry to object
                            refTargetArray[ROW, COL] = (object)SourceArray[ROW, COL];
                        }
                    }

                    // Return true if everything works
                    return true;
                }
                catch (Exception ex)
                {
                    // Return false if something fails
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }

            /// <summary>
            /// Used to convert a 2D array of objects into a 2D array of strings.
            /// </summary>
            /// <param name="SourceArray">The source array to convert.</param>
            /// <param name="refTargetArray">BYREF:  The array to write the converted values to and return.</param>
            /// <returns>Returns TRUE if the array is converted successfully. Returns FALSE in all other conditions.</returns>
            public static bool ConvertToStringArray(object[,] SourceArray, ref string[,] refTargetArray)
            {
                try
                {
                    // Make string array same size as string array
                    refTargetArray = new string[SourceArray.GetUpperBound(0) + 1, SourceArray.GetUpperBound(1) + 1];

                    // Loop through each item
                    for (int ROW = 0; ROW <= SourceArray.GetUpperBound(0); ROW++)
                    {
                        for (int COL = 0; COL <= SourceArray.GetUpperBound(1); COL++)
                        {
                            // Convert each object entry to a string
                            refTargetArray[ROW, COL] = System.Convert.ToString((SourceArray[ROW, COL]));
                        }
                    }

                    // Return true if everything works
                    return true;
                }
                catch (Exception ex)
                {
                    // Return false if something fails
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }

            /// <summary>
            /// Used to convert an array of any type to a collection of the same type.
            /// </summary>
            /// <typeparam name="T">Identifies the type of array being converted.</typeparam>
            /// <param name="ArrayToConvert">The array to convert to a collection.</param>
            /// <returns>Returns a collection of the data in the passed in array if the conversion works. Returns NULL if nothing is passed in or the conversion fails.</returns>
            public static System.Collections.ObjectModel.Collection<T> ConvertToCollection<T>(T[] ArrayToConvert)
            {
                try
                {
                    // Return nothing if nothing is passed in
                    if (ArrayToConvert == null) return null;

                    // If something is passed in, create the collection to return
                    var CollectionToReturn = new System.Collections.ObjectModel.Collection<T>();

                    // Loop through and populate collection
                    foreach (T ITEM in ArrayToConvert)
                    {
                        CollectionToReturn.Add(ITEM);
                    }

                    // Return the collection
                    return CollectionToReturn;
                }
                catch (Exception ex)
                {
                    // Return nothing if conversion fails
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }

            /// <summary>
            /// Used to convert a 1D array of strings to a collection of strings.
            /// </summary>
            /// <param name="SourceArray">The source array to convert to a collection.</param>
            /// <param name="refTargetCollection">BYREF:  The collection to write values to and return</param>
            /// <returns>Returns TRUE if the array is converted successfully. Returns FALSE in all other conditions.</returns>
            public static bool ConvertStringArrayToStringCollection(string[] SourceArray, ref System.Collections.ObjectModel.Collection<string> refTargetCollection)
            {
                try
                {
                    // Return false if nothing is passed in
                    if (SourceArray == null) return false;

                    // If something was passed in, convert it to a collection
                    refTargetCollection = new System.Collections.ObjectModel.Collection<string>();
                    foreach (string ITEM in SourceArray)
                    {
                        refTargetCollection.Add(ITEM);
                    }

                    // Return true if conversion works
                    return true;
                }
                catch (Exception ex)
                {
                    // Return false if something fails
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }

            /// <summary>
            /// Used to convert a 1D array of objects to a collection of objects.
            /// </summary>
            /// <param name="SourceArray">The source array to convert to a collection.</param>
            /// <param name="refTargetCollection">BYREF:  The collection to write values to and return</param>
            /// <returns>Returns TRUE if the array is converted successfully. Returns FALSE in all other conditions.</returns>
            public static bool ConvertObjArrayToObjCollection(object[] SourceArray, ref System.Collections.ObjectModel.Collection<object> refTargetCollection)
            {
                try
                {
                    // Return false if nothing is passed in
                    if (SourceArray == null) return false;

                    // If something was passed in, convert it to a collection
                    refTargetCollection = new System.Collections.ObjectModel.Collection<object>();
                    foreach (object ITEM in SourceArray)
                    {
                        refTargetCollection.Add(ITEM);
                    }

                    // Return true
                    return true;
                }
                catch (Exception ex)
                {
                    // Return false if something fails
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }

        }

        public struct Lists
        {

            /// <summary>
            /// Enum to specify how to handle duplicates when converted a list of KVPs to a dictionary.
            /// </summary>
            public enum KVPToDictionaryDuplicateHandlingOptions
            {
                ReturnNothing,
                UseFirstOccurrence,
                UseLastOccurence
            }

            /// <summary>
            /// Used to convert a list of KVPs to a dictionary.
            /// </summary>
            /// <param name="ListOfKVPs">The list of KVPs to convert to a dictionary.</param>
            /// <returns>Returns a dictionary if the conversion is successful. Returns NOTHING in all other conditions.</returns>
            public static Dictionary<T1, T2> ConvertKeyValuePairsToDictionary<T1, T2>(List<KeyValuePair<T1, T2>> ListOfKVPs)
            {
                // Use helper and return result
                return ConvertKeyValuePairsToDictionary<T1, T2>(ListOfKVPs, KVPToDictionaryDuplicateHandlingOptions.ReturnNothing);
            }
            /// <summary>
            /// Used to convert a list of KVPs to a dictionary.
            /// </summary>
            /// <param name="ListOfKVPs">The list of KVPs to convert to a dictionary.</param>
            /// <param name="DuplicateHandling">Used to indicate how to handle duplicates in the source list.</param>
            /// <returns>Returns a dictionary if the conversion is successful. Returns NOTHING in all other conditions.</returns>
            public static Dictionary<T1, T2> ConvertKeyValuePairsToDictionary<T1, T2>(List<KeyValuePair<T1, T2>> ListOfKVPs, KVPToDictionaryDuplicateHandlingOptions DuplicateHandling)
            {
                try
                {
                    // Return nothing if input is bad
                    if (ListOfKVPs == null) return null;

                    // If we have an input list, new up the dictionary to hold converted info
                    var DictionaryToReturn = new Dictionary<T1, T2>();

                    // Loop through and start adding items
                    foreach (var PAIR in ListOfKVPs)
                    {
                        // If key is new, just add pair and continue processing
                        if (DictionaryToReturn.ContainsKey(PAIR.Key) == false)
                        {
                            DictionaryToReturn.Add(PAIR.Key, PAIR.Value);
                            continue;
                        }

                        // If key is NOT new, check how they wanted to handle duplicates
                        switch (DuplicateHandling)
                        {
                            case KVPToDictionaryDuplicateHandlingOptions.UseFirstOccurrence:
                                // If we're using the first occurrence, just skip this entry and continue processing (i.e. use what's already in there / first occurrence)
                                continue;
                            case KVPToDictionaryDuplicateHandlingOptions.UseLastOccurence:
                                // If they wanted to use the last occurrence, update the value
                                DictionaryToReturn[PAIR.Key] = PAIR.Value;
                                break;
                            default:
                                // For anything else, treat as error / return nothing
                                return null;
                        }
                    }

                    // Return the dictionary when populated
                    return DictionaryToReturn;
                }
                catch (Exception ex)
                {
                    // Return nothing if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }

            /// <summary>
            /// Used to convert an input list of any type to a list of string.
            /// </summary>
            /// <typeparam name="T1">The type of the input list.</typeparam>
            /// <param name="EnumerableListOfItemsToConvert">The list to convert to a list of strings.</param>
            /// <returns>Returns a converted list of strings if successful. Returns NOTHING in all other conditions.</returns>
            public static List<string> ToListOfString<T1>(IEnumerable<T1> EnumerableListOfItemsToConvert)
            {
                try
                {
                    // Return nothing if input is missing
                    if (EnumerableListOfItemsToConvert == null) return null;

                    // Create new list to return with converted strings
                    var ConvertedList = new List<string>();
                    if (EnumerableListOfItemsToConvert.Count() > 0)
                    {
                        // Adjust capacity if there are items, this will speed things up for large input lists
                        ConvertedList.Capacity = EnumerableListOfItemsToConvert.Count();
                    }

                    // Loop through and convert items
                    foreach (var ITEM in EnumerableListOfItemsToConvert)
                    {
                        ConvertedList.Add(ITEM.ToString());
                    }

                    // Return the list after converting
                    return ConvertedList;
                }
                catch (Exception ex)
                {
                    // Return nothing if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }

            /// <summary>
            /// Used to take an enumerable list of items, call ToString() on each item, and merge the ToString() results of each item into a single string with an optional / configurable delimiter between each item in the list.
            /// </summary>
            /// <param name="EnumerableListOfItemsToMerge">The enumerable list of items to merge into a single string.</param>
            /// <param name="InterItemDelimiterToUse">OPTIONAL:  The string to delimit each item with before merging (new line, comma, etc.).</param>
            /// <returns>Returns a single string containing all items in the list (delimited by the optional string) if the merger is successful. Returns an empty string if the source list is null or empty.</returns>
            public static string ToStringItemsAndMerge<T1>(IEnumerable<T1> EnumerableListOfItemsToMerge, string InterItemDelimiterToUse = "\r\n")
            {
                try
                {
                    // Return empty string if nothing is passed in
                    if (EnumerableListOfItemsToMerge == null || EnumerableListOfItemsToMerge.Count() == 0)
                    {
                        return string.Empty;
                    }

                    try
                    {
                        // If something is passed in, attempt to merge info and return
                        return string.Join(InterItemDelimiterToUse, EnumerableListOfItemsToMerge);
                    }
                    catch (Exception)
                    {
                        // If the build in merge function doesn't work (can't call ToString() on a null object for example), convert enumerable to list for manual merging
                        var ListOfItems = EnumerableListOfItemsToMerge.ToList();

                        // Create list to hold all ToStrings
                        var AllItemsAsString = new List<string>();

                        // Loop through and ToString items
                        foreach (var ITEM in ListOfItems)
                        {
                            // Check for null objects
                            if (ITEM != null)
                            {
                                // If item isn't null, ToString it
                                AllItemsAsString.Add(ITEM.ToString());
                            }
                            else
                            {
                                // If object is null, just add empty string
                                AllItemsAsString.Add(string.Empty);
                            }
                        }

                        // Once all objects have been ToString'd, join them
                        string MergedString = string.Join(InterItemDelimiterToUse, AllItemsAsString);

                        // Return the string when done
                        return MergedString;
                    }
                }
                catch (Exception ex)
                {
                    // Return empty string if failure occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

            /// <summary>
            /// Used to take an enumerable list of key value pairs, call ToString() on each item, merge the key and value with a specified delimiter, and merge each of the merged / delimited pairs into a single string with an optional / configurable delimiter between each merged pair.
            /// </summary>
            /// <param name="EnumerableListOfPairs">The enumerable list of key value pairs to merge into a single string.</param>
            /// <param name="KeyValueDelimiterToUse">OPTIONAL:  The string to delimit each key and value with (equal sign, colon, post, etc.).</param>
            /// <param name="InterPairDelimiterToUse">OPTIONAL:  The string to delimit each merged pair with before merging (new line, comma, etc.).</param>
            /// <returns>Returns a single string containing all pairs in the list, each pair delimited by a custom string and inter-pair items delimited by a custom string if the merger is successful. Returns an empty string if the source list is null / empty or the merger fails.</returns>
            public static string ToStringKeyValuePairsAndMerge<T1, T2>(IEnumerable<KeyValuePair<T1, T2>> EnumerableListOfPairs, string KeyValueDelimiterToUse = "=", string InterPairDelimiterToUse = "\r\n")
            {
                try
                {
                    // Return empty string if nothing is passed in
                    if (EnumerableListOfPairs == null || EnumerableListOfPairs.Count() == 0)
                    {
                        return string.Empty;
                    }

                    // If something is passed in, convert to list so easier to use
                    var KVPList = EnumerableListOfPairs.ToList();

                    // Create list to hold the merged keys and pairs
                    var MergedKeysAndValues = new List<string>();

                    // Loop through and merge items
                    foreach (var KVP in KVPList)
                    {
                        // Create vars to hold key and value
                        string Key = string.Empty;
                        string Val = string.Empty;

                        // Save items if not null objects
                        if (KVP.Key != null) Key = KVP.Key.ToString();
                        if (KVP.Value != null) Val = KVP.Value.ToString();

                        // Merge items and add to list
                        MergedKeysAndValues.Add(Key + KeyValueDelimiterToUse + Val);
                    }

                    // Once merged attempt to join and return the result
                    return ToStringItemsAndMerge(MergedKeysAndValues, InterItemDelimiterToUse: InterPairDelimiterToUse);
                }
                catch (Exception ex)
                {
                    // Return empty string if failure occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

            /// <summary>
            /// Used to check a list of string and boolean key value pairs and returns the paired boolean state of a specified string value in the list.
            /// </summary>
            /// <param name="ListOfSelections">The key value pair list of all string-boolean selection.</param>
            /// <param name="ItemToCheck">The string to return the boolean paired value for.</param>
            /// <returns>Returns TRUE if the specified string is found AND its value is true (will only find the first item in the list if there are duplicates). Returns FALSE in all other conditions.</returns>
            public static bool IsItemSelected(List<KeyValuePair<string, bool>> ListOfSelections, string ItemToCheck)
            {
                // Return false if nothing is passed in
                if (ListOfSelections == null) return false;

                // If there's data, loop through all pairs and return the first match
                foreach (var PAIR in ListOfSelections)
                {
                    // If match is found, return boolean value of the pair
                    if (PAIR.Key == ItemToCheck)
                    {
                        return PAIR.Value;
                    }
                }

                // If no match is found, return false
                return false;
            }

            /// <summary>
            /// Used to iterate through a list of string-boolean key value pairs and return all string names that have a boolean value of TRUE.
            /// </summary>
            /// <param name="ListOfSelections">The key value pair list of all string-boolean selection.</param>
            /// <returns>Returns a list of all strings that have been marked TRUE in the key value pair list.</returns>
            public static List<string> GetAllSelectedItems(List<KeyValuePair<string, bool>> ListOfSelections)
            {
                // Use low level method and return the result
                return GetItemsBasedOnSelectionState(ListOfSelections, true);
            }
            /// <summary>
            /// Used to iterate through a list of string-boolean key value pairs and return all string names that have a boolean value of FALSE.
            /// </summary>
            /// <param name="ListOfSelections">The key value pair list of all string-boolean selection.</param>
            /// <returns>Returns a list of all strings that have been marked FALSE in the key value pair list.</returns>
            public static List<string> GetAllUnSelectedItems(List<KeyValuePair<string, bool>> ListOfSelections)
            {
                // Use low level method and return the result
                return GetItemsBasedOnSelectionState(ListOfSelections, false);
            }
            /// <summary>
            /// Used to iterate through a list of string-boolean key value pairs and return all string names that have a user specified boolean state (i.e. either TRUE or FALSE).
            /// </summary>
            /// <param name="ListOfSelections">The key value pair list of all string-boolean selection.</param>
            /// <param name="SelectionStateToGet">What selection state you want returned (i.e. True = return everything marked true, False = return everything marked false)</param>
            /// <returns>Returns a list of all strings that have been marked with the requested boolean value in the key value pair list.</returns>
            public static List<string> GetItemsBasedOnSelectionState(List<KeyValuePair<string, bool>> ListOfSelections, bool SelectionStateToGet)
            {
                // Create list to return
                var ListToReturn = new List<string>();

                // Return empty list if nothing is passed in
                if (ListOfSelections == null) return ListToReturn;

                // Loop through the input list and look for selections
                foreach (var PAIR in ListOfSelections)
                {
                    // Add items with the matching selection state
                    if (PAIR.Value == SelectionStateToGet)
                    {
                        ListToReturn.Add(PAIR.Key);
                    }
                }

                // Return the list when done
                return ListToReturn;
            }

        }

        public struct Collections
        {

            /// <summary>
            /// Used to convert a collection of OBJECTS to a collection of STRINGS.
            /// </summary>
            /// <param name="SourceCollection">The source collection to convert.</param>
            /// <param name="refTargetCollection">BYREF:  The target collection to write values to and return.</param>
            /// <returns>Returns TRUE if the conversion works. Returns FALSE in all other conditions.</returns>
            public static bool ConvertObjCollectionToStringCollection(System.Collections.ObjectModel.Collection<object> SourceCollection, ref System.Collections.ObjectModel.Collection<string> refTargetCollection)
            {
                try
                {
                    // Return false if nothing is passed in
                    if (SourceCollection == null || SourceCollection.Count == 0) return false;

                    // If there were items, convert the collection
                    refTargetCollection = new System.Collections.ObjectModel.Collection<string>();
                    foreach (object ITEM in SourceCollection)
                    {
                        refTargetCollection.Add(ITEM.ToString());
                    }

                    // Return true if conversion works
                    return true;
                }
                catch (Exception ex)
                {
                    // Return false if something fails
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }

            /// <summary>
            /// Used to convert a collection of STRINGS to a collection of OBJECTS.
            /// </summary>
            /// <param name="SourceCollection">The source collection to convert.</param>
            /// <param name="refTargetCollection">BYREF:  The target collection to write values to and return.</param>
            /// <returns>Returns TRUE if the conversion works. Returns FALSE in all other conditions.</returns>
            public static bool ConvertStringCollectionToObjCollection(System.Collections.ObjectModel.Collection<string> SourceCollection, ref System.Collections.ObjectModel.Collection<object> refTargetCollection)
            {
                try
                {
                    // Return false if nothing is passed in
                    if (SourceCollection == null || SourceCollection.Count == 0) return false;

                    // If there were items, convert the collection
                    refTargetCollection = new System.Collections.ObjectModel.Collection<object>();
                    foreach (object ITEM in SourceCollection)
                    {
                        refTargetCollection.Add((object)ITEM);
                    }

                    // Return true if conversion works
                    return true;
                }
                catch (Exception ex)
                {
                    // Return false if something fails
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }

            /// <summary>
            /// Used to convert a collection of OBJECTS to a collection of INTEGERS.
            /// </summary>
            /// <param name="SourceCollection">The source collection to convert.</param>
            /// <param name="refTargetCollection">BYREF:  The target collection to write values to and return.</param>
            /// <returns>Returns TRUE if the conversion works. Returns FALSE in all other conditions.</returns>
            public static bool ConvertObjCollectionToIntegerCollection(System.Collections.ObjectModel.Collection<object> SourceCollection, ref System.Collections.ObjectModel.Collection<int> refTargetCollection)
            {
                try
                {
                    // Return false if nothing is passed in
                    if (SourceCollection == null || SourceCollection.Count == 0) return false;

                    // If there were items, convert the collection
                    refTargetCollection = new System.Collections.ObjectModel.Collection<int>();
                    foreach (object ITEM in SourceCollection)
                    {
                        if (Helpers.IsNumeric(ITEM) == true)
                        {
                            refTargetCollection.Add(Convert.ToInt32(ITEM));
                        }
                    }

                    // Return true if conversion works
                    return true;
                }
                catch (Exception ex)
                {
                    // Return false if something fails
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }

            /// <summary>
            /// Used to convert a collection of INTEGERS to a collection of OBJECTS.
            /// </summary>
            /// <param name="SourceCollection">The source collection to convert.</param>
            /// <param name="refTargetCollection">BYREF:  The target collection to write values to and return.</param>
            /// <returns>Returns TRUE if the conversion works. Returns FALSE in all other conditions.</returns>
            public static bool ConvertIntegerCollectionToObjCollection(System.Collections.ObjectModel.Collection<int> SourceCollection, ref System.Collections.ObjectModel.Collection<object> refTargetCollection)
            {
                try
                {
                    // Return false if nothing is passed in
                    if (SourceCollection == null || SourceCollection.Count == 0) return false;

                    // If there were items, convert the collection
                    refTargetCollection = new System.Collections.ObjectModel.Collection<object>();
                    foreach (object ITEM in SourceCollection)
                    {
                        refTargetCollection.Add((object)ITEM);
                    }

                    // Return true if conversion works
                    return true;
                }
                catch (Exception ex)
                {
                    // Return false if something fails
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }

            /// <summary>
            /// Used to convert a collection of OBJECTS to a collection of DOUBLES.
            /// </summary>
            /// <param name="SourceCollection">The source collection to convert.</param>
            /// <param name="refTargetCollection">BYREF:  The target collection to write values to and return.</param>
            /// <returns>Returns TRUE if the conversion works. Returns FALSE in all other conditions.</returns>
            public static bool ConvertObjCollectionToIntegerCollection(System.Collections.ObjectModel.Collection<object> SourceCollection, ref System.Collections.ObjectModel.Collection<double> refTargetCollection)
            {
                try
                {
                    // Return false if nothing is passed in
                    if (SourceCollection == null || SourceCollection.Count == 0) return false;

                    // If there were items, convert the collection
                    refTargetCollection = new System.Collections.ObjectModel.Collection<double>();
                    foreach (object ITEM in SourceCollection)
                    {
                        if (Helpers.IsNumeric(ITEM) == true)
                        {
                            refTargetCollection.Add(System.Convert.ToDouble(ITEM));
                        }
                    }

                    // Return true if conversion works
                    return true;
                }
                catch (Exception ex)
                {
                    // Return false if something fails
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }

            /// <summary>
            /// Used to convert a collection of DOUBLES to a collection of OBJECTS.
            /// </summary>
            /// <param name="SourceCollection">The source collection to convert.</param>
            /// <param name="refTargetCollection">BYREF:  The target collection to write values to and return.</param>
            /// <returns>Returns TRUE if the conversion works. Returns FALSE in all other conditions.</returns>
            public static bool ConvertIntegerCollectionToObjCollection(System.Collections.ObjectModel.Collection<double> SourceCollection, ref System.Collections.ObjectModel.Collection<object> refTargetCollection)
            {
                try
                {
                    // Return false if nothing is passed in
                    if (SourceCollection == null || SourceCollection.Count == 0) return false;

                    // If there were items, convert the collection
                    refTargetCollection = new System.Collections.ObjectModel.Collection<object>();
                    foreach (object ITEM in SourceCollection)
                    {
                        refTargetCollection.Add((object)ITEM);
                    }

                    // Return true if conversion works
                    return true;
                }
                catch (Exception ex)
                {
                    // Return false if something fails
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }

            /// <summary>
            /// Converts two lists of the same item count to an observable collection of key / value pairs.
            /// </summary>
            /// <param name="KeyList">The list of keys for all pairs.</param>
            /// <param name="ValueList">The list of values for all pairs.</param>
            /// <returns>Returns a new observable collection of key / value pairs if all inputs are non-null and of equal dimensions / item counts. Returns nothing in all other conditions.</returns>
            public static System.Collections.ObjectModel.ObservableCollection<KeyValuePair<string, double>> CreateKeyValuePairListsToObservableCollection(List<string> KeyList, List<double> ValueList)
            {
                // Return nothing if data is bad
                if (KeyList == null || ValueList == null || KeyList.Count != ValueList.Count) return null;

                // If inputs are good, create collection to return
                var CollectionToReturn = new System.Collections.ObjectModel.ObservableCollection<KeyValuePair<string, double>>();

                // Return empty collection if there's no data
                if (KeyList.Count == 0) return CollectionToReturn;

                // If there's data, loop through and add it
                for (int i = 0; i <= KeyList.Count - 1; i++)
                {
                    CollectionToReturn.Add(new KeyValuePair<string, double>(KeyList[i], ValueList[i]));
                }

                // Return the collection
                return CollectionToReturn;
            }

            /// <summary>
            /// Converts two lists of the same item count to an observable collection of key / value pairs.
            /// </summary>
            /// <param name="KeyList">The list of keys for all pairs.</param>
            /// <param name="ValueList">The list of values for all pairs.</param>
            /// <returns>Returns a new observable collection of key / value pairs if all inputs are non-null and of equal dimensions / item counts. Returns nothing in all other conditions.</returns>
            public static System.Collections.ObjectModel.ObservableCollection<KeyValuePair<double, double>> CreateObservableKeyValuePairCollection(List<double> KeyList, List<double> ValueList)
            {
                // Return nothing if data is bad
                if (KeyList == null || ValueList == null || KeyList.Count != ValueList.Count) return null;

                // If inputs are good, create collection to return
                var CollectionToReturn = new System.Collections.ObjectModel.ObservableCollection<KeyValuePair<double, double>>();

                // Return empty collection if there's no data
                if (KeyList.Count == 0) return CollectionToReturn;

                // If there's data, loop through and add it
                for (int i = 0; i <= KeyList.Count - 1; i++)
                {
                    CollectionToReturn.Add(new KeyValuePair<double, double>(KeyList[i], ValueList[i]));
                }

                // Return the collection
                return CollectionToReturn;
            }

        }

        public struct DataAggregation
        {

            /// <summary>
            /// Helper class to hold converted histogram data.
            /// </summary>
            public class HistogramData
            {
                // Class to define info for each bar of the histogram
                public class BarInfo
                {
                    // Vars to hold info about a given bar of histogram
                    public string GroupingRange = string.Empty;
                    public double GroupingMidPoint = 0;
                    public double CountInRange = 0;
                }

                // Var to hold all histogram bar info
                public readonly List<BarInfo> AllBars = new List<BarInfo>();

                // Method to get all bar data as double/double pairs
                public List<KeyValuePair<double, double>> ToDoubleDoublePairs()
                {
                    // Create list to return
                    var ListToReturn = new List<KeyValuePair<double, double>>();

                    // Loop through and add data
                    foreach (var BAR in AllBars)
                    {
                        ListToReturn.Add(new KeyValuePair<double, double>(BAR.GroupingMidPoint, BAR.CountInRange));
                    }

                    // Return the list
                    return ListToReturn;
                }

                // Method to get all data as string/double pairs
                public List<KeyValuePair<string, double>> ToStringDoublePairs()
                {
                    // Create list to return
                    var ListToReturn = new List<KeyValuePair<string, double>>();

                    // Loop through and add data
                    foreach (var BAR in AllBars)
                    {
                        ListToReturn.Add(new KeyValuePair<string, double>(BAR.GroupingRange, BAR.CountInRange));
                    }

                    // Return the list
                    return ListToReturn;
                }

                // Override ToString() for ease of reading
                public override string ToString()
                {
                    // Get all individual params so we can find lengths to pad to
                    var AllMidPoints = (from AllInfo in AllBars select AllInfo.GroupingMidPoint).ToList();
                    var AllCounts = (from AllInfo in AllBars select AllInfo.CountInRange).ToList();

                    // Create var to hold the padding length needed (we'll use the same for all)
                    int MaxStringLength = -1;

                    // Loop through all lists
                    foreach (var LST in new List<double>[] { AllMidPoints, AllCounts })
                    {
                        foreach (var ITEM in LST)
                        {
                            // Check if longer / more padding needed
                            if (ITEM.ToString().Length > MaxStringLength)
                            {
                                MaxStringLength = ITEM.ToString().Length;
                            }
                        }
                    }

                    // Once we're done, make max padding just a little longer than the biggest string
                    int PaddingLength = MaxStringLength + 2;

                    // Create list to hold info to join and return
                    var InfoToJoin = new List<string>();

                    // Add all lines
                    foreach (var BAR in AllBars)
                    {
                        InfoToJoin.Add(BAR.GroupingMidPoint.ToString().PadRight(PaddingLength, Convert.ToChar(" ")) + "  |  " + BAR.CountInRange.ToString().PadRight(PaddingLength, Convert.ToChar(" ")) + "  |  " + BAR.GroupingRange);
                    }

                    // Join and return info
                    return string.Join(Environment.NewLine, InfoToJoin);
                }
            }

            /// <summary>
            /// Enum to specify the different smoothing method options.
            /// </summary>
            public enum SmoothingMethodOptions
            {
                Average,
                Median
            }

            /// <summary>
            /// Used to convert a list of datapoints to list of KVP data that can be used to populate a histogram.
            /// </summary>
            /// <param name="DataToConvert">The datapoints to group into histogram data.</param>
            /// <param name="OverrideGroupingInterval">OPTOINAL:  Used to specify how big each frequency range / group interval should be in the returned data (i.e. to dictate how many bars to return).</param>
            /// <param name="DecimalsPointsToRoundGroupingAxisTo">OPTIONAL:  Used to indicate how many decimal points to round the grouping axis numeric scale to (will not be rounded if not specified).</param>
            /// <returns>Returns an object with histogram-ready KVPs if conversion is successful. Returns NOTHING in all other conditions.</returns>
            public static HistogramData CreateHistogramData(IEnumerable<double> DataToConvert, Nullable<double> OverrideGroupingInterval = default(Double?), Nullable<UInt16> DecimalsPointsToRoundGroupingAxisTo = null)
            {
                try
                {
                    // Return nothing if input data is missing
                    if (DataToConvert == null) return null;

                    // If we have input data, convert to list for ease of working with
                    var DataAsList = DataToConvert.ToList();

                    // Return nothing if there's no data
                    if (DataAsList.Count == 0) return null;

                    // Save the min and the max points
                    double MaxVal = DataToConvert.Max();
                    double MinVal = DataToConvert.Min();

                    // Create var to hold the frequency interval to use
                    double FrequencyInterval = 0;

                    // Create var to hold how many bars to use
                    UInt16 NumberOfBarsToGroupTo = 15;

                    // Check if they specified an interval to use
                    if (OverrideGroupingInterval != null && OverrideGroupingInterval.Value != 0)
                    {
                        // If they specified a non-zero entry, use it
                        FrequencyInterval = System.Math.Abs(OverrideGroupingInterval.Value);
                    }
                    else
                    {
                        // If they didn't specify an interval to use, make one up based on the min and max
                        FrequencyInterval = System.Math.Abs((MaxVal - MinVal) / System.Convert.ToDouble(NumberOfBarsToGroupTo));
                    }

                    // Once we have the interval to use, create var to hold data to return
                    var HistDataToReturn = new HistogramData();

                    // Create vars to hold the cutoff ranges that we're looking for as we loop through and count occurrences
                    double CutoffMin = MinVal - FrequencyInterval;
                    double CutoffMax = MinVal;

                    // Loop through get counts at all groupings
                    while (CutoffMax <= (MaxVal + FrequencyInterval))
                    {
                        // Compute the midpoint of this range to use as the X value of the KVP
                        double Midpoint = CutoffMin + (CutoffMax - CutoffMin) / 2.0;

                        // Round midpoint if requested
                        if (DecimalsPointsToRoundGroupingAxisTo != null)
                        {
                            Midpoint = System.Math.Round(Midpoint, DecimalsPointsToRoundGroupingAxisTo.Value, MidpointRounding.AwayFromZero);
                        }

                        // Count occurrences in the range
                        var PointsInRange = (from AllInfo in DataAsList where AllInfo >= CutoffMin && AllInfo < CutoffMax select AllInfo).ToList().Count();

                        // Create new bar info
                        var NewBar = new HistogramData.BarInfo();

                        // Add the data
                        NewBar.GroupingRange = CutoffMin.ToString() + ":" + CutoffMax.ToString();
                        NewBar.GroupingMidPoint = Midpoint;
                        NewBar.CountInRange = PointsInRange;

                        // Add the bar to the object being returned
                        HistDataToReturn.AllBars.Add(NewBar);

                        // Inc the cutoffs for the next loop iteration
                        CutoffMin += FrequencyInterval;
                        CutoffMax += FrequencyInterval;
                    }

                    // It's possible one of the end points will be have no data because we search outside the range, remove those
                    if (HistDataToReturn.AllBars[HistDataToReturn.AllBars.Count - 1].CountInRange == 0) HistDataToReturn.AllBars.RemoveAt(HistDataToReturn.AllBars.Count - 1);
                    if (HistDataToReturn.AllBars[0].CountInRange == 0) HistDataToReturn.AllBars.RemoveAt(0);

                    // Return data when done
                    return HistDataToReturn;
                }
                catch (Exception ex)
                {
                    // Return nothing if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }

            /// <summary>
            /// Used to smooth adjacent points along a curve. This will reduce the overall point count and soften spikes / noise in the data.
            /// </summary>
            /// <param name="AllPoints">The set of key / value pairs to average.</param>
            /// <param name="GroupSizeForSmoothing">The number points in a group average (e.g. 2 would average in groups of 2 and return a list of data that is 1/2 the size, 3 would average in groups of 3 and return a list of data 1/3 of the size, etc.).</param>
            /// <param name="SmoothingMethod">Used to specify what type of math you want to do to smooth the adjacent points.</param>
            /// <param name="NumberOfDigitsToRoundToKey">OPTIONAL:  The number of decimal points to round the Keys to when the average is computed (use NOTHING for no rounding).</param>
            /// <param name="NumberOfDigitsToRoundToValue">OPTIONAL:  The number of decimal points to round the Values to when the average is computed (use NOTHING for no rounding).</param>
            /// <returns>Returns the data scaled down by the requested group size.</returns>
            public static System.Collections.ObjectModel.ObservableCollection<KeyValuePair<double, double>> SmoothAdjacentPoints(System.Collections.ObjectModel.ObservableCollection<KeyValuePair<double, double>> AllPoints, int GroupSizeForSmoothing, SmoothingMethodOptions SmoothingMethod, Nullable<int> NumberOfDigitsToRoundToKey = null, Nullable<int> NumberOfDigitsToRoundToValue = null)
            {
                // Use lower level function to get smoothed list
                var AveragedList = SmoothAdjacentPoints(AllPoints.ToList(), GroupSizeForSmoothing, SmoothingMethod, NumberOfDigitsToRoundToKey: NumberOfDigitsToRoundToKey, NumberOfDigitsToRoundToValue: NumberOfDigitsToRoundToValue);

                // Convert back to observable collection
                var InfoToReturn = new System.Collections.ObjectModel.ObservableCollection<KeyValuePair<double, double>>();
                foreach (KeyValuePair<double, double> PAIR in AveragedList)
                {
                    InfoToReturn.Add(PAIR);
                }

                // Return the collection
                return InfoToReturn;
            }
            /// <summary>
            /// Used to smooth adjacent points along a curve. This will reduce the overall point count and soften spikes / noise in the data.
            /// </summary>
            /// <param name="AllPoints">The set of key / value pairs to average.</param>
            /// <param name="GroupSizeForSmoothing">The number points in a group average (e.g. 2 would average in groups of 2 and return a list of data that is 1/2 the size, 3 would average in groups of 3 and return a list of data 1/3 of the size, etc.).</param>
            /// <param name="SmoothingMethod">Used to specify what type of math you want to do to smooth the adjacent points.</param>
            /// <param name="NumberOfDigitsToRoundToKey">OPTIONAL:  The number of decimal points to round the Keys to when the average is computed (use NOTHING for no rounding).</param>
            /// <param name="NumberOfDigitsToRoundToValue">OPTIONAL:  The number of decimal points to round the Values to when the average is computed (use NOTHING for no rounding).</param>
            /// <returns>Returns the data scaled down by the requested group size.</returns>
            public static System.Collections.ObjectModel.Collection<KeyValuePair<double, double>> SmoothAdjacentPoints(System.Collections.ObjectModel.Collection<KeyValuePair<double, double>> AllPoints, int GroupSizeForSmoothing, SmoothingMethodOptions SmoothingMethod, Nullable<int> NumberOfDigitsToRoundToKey = null, Nullable<int> NumberOfDigitsToRoundToValue = null)
            {
                // Use lower level function to get smoothed list
                var AveragedList = SmoothAdjacentPoints(AllPoints.ToList(), GroupSizeForSmoothing, SmoothingMethod, NumberOfDigitsToRoundToKey: NumberOfDigitsToRoundToKey, NumberOfDigitsToRoundToValue: NumberOfDigitsToRoundToValue);

                // Convert back to collection
                var InfoToReturn = new System.Collections.ObjectModel.Collection<KeyValuePair<double, double>>();
                foreach (KeyValuePair<double, double> PAIR in AveragedList)
                {
                    InfoToReturn.Add(PAIR);
                }

                // Return the collection
                return InfoToReturn;
            }
            /// <summary>
            /// Used to smooth adjacent points along a curve. This will reduce the overall point count and soften spikes / noise in the data.
            /// </summary>
            /// <param name="AllPoints">The set of key / value pairs to average.</param>
            /// <param name="GroupSizeForSmoothing">The number points in a group average (e.g. 2 would average in groups of 2 and return a list of data that is 1/2 the size, 3 would average in groups of 3 and return a list of data 1/3 of the size, etc.).</param>
            /// <param name="SmoothingMethod">Used to specify what type of math you want to do to smooth the adjacent points.</param>
            /// <param name="NumberOfDigitsToRoundToKey">OPTIONAL:  The number of decimal points to round the Keys to when the average is computed (use NOTHING for no rounding).</param>
            /// <param name="NumberOfDigitsToRoundToValue">OPTIONAL:  The number of decimal points to round the Values to when the average is computed (use NOTHING for no rounding).</param>
            /// <returns>Returns the data scaled down by the requested group size.</returns>
            public static List<KeyValuePair<double, double>> SmoothAdjacentPoints(List<KeyValuePair<double, double>> AllPoints, int GroupSizeForSmoothing, SmoothingMethodOptions SmoothingMethod, Nullable<int> NumberOfDigitsToRoundToKey = null, Nullable<int> NumberOfDigitsToRoundToValue = null)
            {
                // Create info to return
                var InfoToReturn = new List<KeyValuePair<double, double>>();

                // Return empty list if no data is passed in
                if (AllPoints == null || AllPoints.Count == 0) return InfoToReturn;

                // Return empty list if averaging group size is invalid
                if (GroupSizeForSmoothing <= 0) return InfoToReturn;

                // Get the number of points that will be in the new data set (use FLOOR so only groups big enough will be selected, i.e. last small group / remainder is tossed out)
                int NewPointCount = Convert.ToInt32(System.Math.Floor(AllPoints.Count / (double)GroupSizeForSmoothing));

                // Size lists right away to expedite adding elements to them
                InfoToReturn.Capacity = NewPointCount;

                // Loop through and create keys
                for (int i = 0; i <= NewPointCount - 1; i++)
                {
                    // Get the next grouping
                    var NextGroup = AllPoints.GetRange(i * GroupSizeForSmoothing, GroupSizeForSmoothing);

                    // Save the X and Y coordinates of the next group (i.e. the keys and the values)
                    var XValues = (from AllInfo in NextGroup select AllInfo.Key).ToList();
                    var YValues = (from AllInfo in NextGroup select AllInfo.Value).ToList();

                    // Create vars to hold the smoothed values
                    double ComputedX = 0;
                    double ComputedY = 0;

                    // Check smoothing method to determine what math to do
                    switch (SmoothingMethod)
                    {
                        case SmoothingMethodOptions.Average:
                            // If we're averaging, get the means
                            ComputedX = ComputeMean(XValues);
                            ComputedY = ComputeMean(YValues);
                            break;
                        case SmoothingMethodOptions.Median:
                            // If we're using center, get median
                            ComputedX = ComputeMedian(XValues);
                            ComputedY = ComputeMedian(YValues);
                            break;
                        default:
                            // Return empty data if unimplemented method is specified
                            return InfoToReturn;
                    }

                    // Round X if requested
                    if (NumberOfDigitsToRoundToKey != null && NumberOfDigitsToRoundToKey >= 0)
                    {
                        ComputedX = System.Math.Round(ComputedX, NumberOfDigitsToRoundToKey.Value, MidpointRounding.AwayFromZero);
                    }

                    // Round Y if requested
                    if (NumberOfDigitsToRoundToValue != null && NumberOfDigitsToRoundToValue >= 0)
                    {
                        ComputedY = System.Math.Round(ComputedY, NumberOfDigitsToRoundToValue.Value, MidpointRounding.AwayFromZero);
                    }

                    // Add items to the list that will be returned
                    InfoToReturn.Add(new KeyValuePair<double, double>(ComputedX, ComputedY));
                }

                // Return the info
                return InfoToReturn;
            }
            private static double ComputeMean(List<double> Numbers)
            {
                try
                {
                    // Compute and return mean
                    return Numbers.Average();
                }
                catch (Exception ex)
                {
                    // Return 0 if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return 0;
                }
            }
            private static double ComputeMedian(List<double> Numbers)
            {
                try
                {
                    // First, sort the numbers
                    Numbers.Sort();

                    // Check if we have even or odd count to determine how to do median
                    if (Numbers.Count % 2 != 0)
                    {
                        // If it's odd, just return the center item
                        return Numbers[Convert.ToInt32((Numbers.Count - 1) / (double)2)];
                    }
                    else
                    {
                        // If it's even, we need to get the center two items and compute the mean
                        var CenterTwoNumbers = Numbers.GetRange(Convert.ToInt32(Numbers.Count / (double)2 - 1), 2);
                        return ComputeMean(CenterTwoNumbers);
                    }
                }
                catch (Exception ex)
                {
                    // Return 0 if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return 0;
                }
            }

            /// <summary>
            /// Used to create a new observable collection from a source observable collection with points omitted at a specified interval.
            /// </summary>
            /// <param name="AllPoints">The source set of key / value pairs to start from.</param>
            /// <param name="OmitEveryNthPoint">The interval to use when omitting points (e.g. 2 will omit every other point, 3 will omit every 3 point, etc.).</param>
            /// <returns>Returns a copy of the data with points removed in the specified interval.</returns>
            public static System.Collections.ObjectModel.ObservableCollection<KeyValuePair<T1, T2>> OmitPoints<T1, T2>(System.Collections.ObjectModel.ObservableCollection<KeyValuePair<T1, T2>> AllPoints, int OmitEveryNthPoint)
            {
                // Use lower level function to get new list
                var ReturnedList = OmitPoints(AllPoints.ToList(), OmitEveryNthPoint);

                // Convert back to collection
                var InfoToReturn = new System.Collections.ObjectModel.ObservableCollection<KeyValuePair<T1, T2>>();
                foreach (KeyValuePair<T1, T2> PAIR in ReturnedList)
                {
                    InfoToReturn.Add(PAIR);
                }

                // Return the collection
                return InfoToReturn;
            }
            /// <summary>
            /// Used to create a new collection from a source collection with points omitted at a specified interval.
            /// </summary>
            /// <param name="AllPoints">The source set of key / value pairs to start from.</param>
            /// <param name="OmitEveryNthPoint">The interval to use when omitting points (e.g. 2 will omit every other point, 3 will omit every 3 point, etc.).</param>
            /// <returns>Returns a copy of the data with points removed in the specified interval.</returns>
            public static System.Collections.ObjectModel.Collection<KeyValuePair<T1, T2>> OmitPoints<T1, T2>(System.Collections.ObjectModel.Collection<KeyValuePair<T1, T2>> AllPoints, int OmitEveryNthPoint)
            {
                // Use lower level function to get new list
                var ReturnedList = OmitPoints(AllPoints.ToList(), OmitEveryNthPoint);

                // Convert back to collection
                var InfoToReturn = new System.Collections.ObjectModel.Collection<KeyValuePair<T1, T2>>();
                foreach (KeyValuePair<T1, T2> PAIR in ReturnedList)
                {
                    InfoToReturn.Add(PAIR);
                }

                // Return the collection
                return InfoToReturn;
            }
            /// <summary>
            /// Used to create a new list from a source list with points omitted at a specified interval.
            /// </summary>
            /// <param name="AllPoints">The source set of key / value pairs to start from.</param>
            /// <param name="OmitEveryNthPoint">The interval to use when omitting points (e.g. 2 will omit every other point, 3 will omit every 3 point, etc.).</param>
            /// <returns>Returns a copy of the data with points removed in the specified interval.</returns>
            public static List<KeyValuePair<T1, T2>> OmitPoints<T1, T2>(List<KeyValuePair<T1, T2>> AllPoints, int OmitEveryNthPoint)
            {
                // Create info to return
                var InfoToReturn = new List<KeyValuePair<T1, T2>>();

                // Return empty list if no data is passed in
                if (AllPoints == null || AllPoints.Count == 0) return InfoToReturn;

                // Return empty list if omission interval is bad
                if (OmitEveryNthPoint <= 1) return InfoToReturn;

                // Return the full list IF there's not enough data in it for computation
                if (AllPoints.Count <= OmitEveryNthPoint) return AllPoints;

                // If inputs are good, loop through in the specified interval and remove the points
                for (int i = 0; i <= AllPoints.Count - 1; i++)
                {
                    // Xfer point if it's not a multiple of the omission interval
                    if ((i + 1) % OmitEveryNthPoint != 0)
                    {
                        InfoToReturn.Add(AllPoints[i]);
                    }
                }

                // Return the info
                return InfoToReturn;
            }

            /// <summary>
            /// Used to create a new observable collection from a source observable collection where a point is omitted from a specified group size without throwing out a high or low peak if possible.
            /// </summary>
            /// <param name="AllPoints">The source set of key / value pairs to start from.</param>
            /// <param name="GroupSizeToRemovePointFrom">The group size to use while iterating through and looking for points to remove (e.g. for "4", the method will break the full set of points into subgroups of 4 and remove one point from each subgroup that is neither the lowest or highest value in the group).</param>
            /// <returns>Returns a copy of the data with a single point removed from the specified group size that was neither the lowest or highest point in the group.</returns>
            public static System.Collections.ObjectModel.ObservableCollection<KeyValuePair<T1, double>> OmitPointsPreservePeaks<T1>(System.Collections.ObjectModel.ObservableCollection<KeyValuePair<T1, double>> AllPoints, int GroupSizeToRemovePointFrom)
            {
                // Use lower level function to get new list
                var ReturnedList = OmitPointsPreservePeaks(AllPoints.ToList(), GroupSizeToRemovePointFrom);

                // Convert back to collection
                var InfoToReturn = new System.Collections.ObjectModel.ObservableCollection<KeyValuePair<T1, double>>();
                foreach (KeyValuePair<T1, double> PAIR in ReturnedList)
                {
                    InfoToReturn.Add(PAIR);
                }

                // Return the collection
                return InfoToReturn;
            }
            /// <summary>
            /// Used to create a new collection from a source collection where a point is omitted from a specified group size without throwing out a high or low peak if possible.
            /// </summary>
            /// <param name="AllPoints">The source set of key / value pairs to start from.</param>
            /// <param name="GroupSizeToRemovePointFrom">The group size to use while iterating through and looking for points to remove (e.g. for "4", the method will break the full set of points into subgroups of 4 and remove one point from each subgroup that is neither the lowest or highest value in the group).</param>
            /// <returns>Returns a copy of the data with a single point removed from the specified group size that was neither the lowest or highest point in the group.</returns>
            public static System.Collections.ObjectModel.Collection<KeyValuePair<T1, double>> OmitPointsPreservePeaks<T1>(System.Collections.ObjectModel.Collection<KeyValuePair<T1, double>> AllPoints, int GroupSizeToRemovePointFrom)
            {
                // Use lower level function to get new list
                var ReturnedList = OmitPointsPreservePeaks(AllPoints.ToList(), GroupSizeToRemovePointFrom);

                // Convert back to collection
                var InfoToReturn = new System.Collections.ObjectModel.Collection<KeyValuePair<T1, double>>();
                foreach (KeyValuePair<T1, double> PAIR in ReturnedList)
                {
                    InfoToReturn.Add(PAIR);
                }

                // Return the collection
                return InfoToReturn;
            }
            /// <summary>
            /// Used to create a new list from a source list where a point is omitted from a specified group size without throwing out a high or low peak if possible.
            /// </summary>
            /// <param name="AllPoints">The source set of key / value pairs to start from.</param>
            /// <param name="GroupSizeToRemovePointFrom">The group size to use while iterating through and looking for points to remove (e.g. for "4", the method will break the full set of points into subgroups of 4 and remove one point from each subgroup that is neither the lowest or highest value in the group).</param>
            /// <returns>Returns a copy of the data with a single point removed from the specified group size that was neither the lowest or highest point in the group.</returns>
            public static List<KeyValuePair<T1, double>> OmitPointsPreservePeaks<T1>(List<KeyValuePair<T1, double>> AllPoints, int GroupSizeToRemovePointFrom)
            {
                // Return empty list if no data is passed in
                if (AllPoints == null || AllPoints.Count == 0) return new List<KeyValuePair<T1, double>>();

                // Return empty list if omission interval is bad (for preserving peaks, need at least every 3rd point)
                if (GroupSizeToRemovePointFrom <= 2) return new List<KeyValuePair<T1, double>>();

                // Return the full list IF there's not enough data in it for computation
                if (AllPoints.Count <= GroupSizeToRemovePointFrom) return AllPoints;

                // Create list to hold KVPs that are not min maxes and can be removed
                var KVPsThatCanBeRemoved = new List<KeyValuePair<T1, double>>();
                var PointIndexesThatCanBeRemoved = new HashSet<int>();

                // Loop through in blocks of requested size
                for (int i = 0; i <= AllPoints.Count - 1; i += GroupSizeToRemovePointFrom)
                {
                    // Stop omitting if at the end and there aren't enough points left
                    if ((i + 1) + GroupSizeToRemovePointFrom >= AllPoints.Count) break;

                    // If there's enough points left, get the next clump
                    var NextClumpOfPoints = AllPoints.GetRange(i, GroupSizeToRemovePointFrom);

                    // Get the min and max
                    double MinVal = GetMinValue(NextClumpOfPoints);
                    double MaxVal = GetMaxValue(NextClumpOfPoints);

                    // Create flag to hold if a non-peak point was found
                    bool NonPeakPointFound = false;

                    // Loop through clump and try to remove the first value that isn't the min or the max
                    for (int j = 0; j <= NextClumpOfPoints.Count - 1; j++)
                    {
                        // Check if this is a min or max
                        if (NextClumpOfPoints[j].Value != MinVal && NextClumpOfPoints[j].Value != MaxVal)
                        {
                            // If this is not the min / max, add index to list of safe to remove points
                            PointIndexesThatCanBeRemoved.Add(i + j);

                            // Set flag indicating a non-peak value was marked for removal and exit loop
                            NonPeakPointFound = true;
                            break;
                        }
                    }

                    // Check if a non-peak value was found
                    if (NonPeakPointFound == false)
                    {
                        // If not, just flag the last point in the group (e.g. there are multiple peak / valley points in a row)
                        PointIndexesThatCanBeRemoved.Add(i + NextClumpOfPoints.Count - 1);
                    }
                }

                // Once all safe to remove pints are found, create list that will hold all points to return
                var PointsToReturn = new List<KeyValuePair<T1, double>>();

                // Loop through all points and xfer over one's that aren't being removed
                for (int i = 0; i <= AllPoints.Count - 1; i++)
                {
                    // Check if the current point was a peak / unremovable point
                    if (PointIndexesThatCanBeRemoved.Contains(i) == false)
                    {
                        // If this point can't be removed, xfer it over to the list to return
                        PointsToReturn.Add(AllPoints[i]);
                    }
                }

                // Return the info
                return PointsToReturn;
            }
            private static double GetMinValue<T1>(List<KeyValuePair<T1, double>> PointsToCheck)
            {
                // Create var to hold min
                double MinVal = double.MaxValue;

                // Loop through and find min
                foreach (var PAIR in PointsToCheck)
                {
                    if (PAIR.Value < MinVal)
                    {
                        MinVal = PAIR.Value;
                    }
                }

                // Return the value
                return MinVal;
            }
            private static double GetMaxValue<T1>(List<KeyValuePair<T1, double>> PointsToCheck)
            {
                // Create var to hold max
                double MaxVal = double.MinValue;

                // Loop through and find max
                foreach (var PAIR in PointsToCheck)
                {
                    if (PAIR.Value > MaxVal)
                    {
                        MaxVal = PAIR.Value;
                    }
                }

                // Return the value
                return MaxVal;
            }

        }

        private struct Helpers
        {

            /// <summary>
            /// Helper for checking if an object can be converted to a number (double).
            /// </summary>
            public static bool IsNumeric(object ObjectToCheck)
            {
                try
                {
                    // Return false if nothing is set
                    if (ObjectToCheck == null) return false;

                    // Convert object to string for checking
                    string StringValue = ObjectToCheck.ToString();

                    // Return false if string is bad / empty
                    if (string.IsNullOrEmpty(StringValue) == true) return false;

                    // Create dummy var to attempt conversion
                    double DummyVar = 0;

                    // If string looks good, attempt to parse it
                    return double.TryParse(StringValue, out DummyVar);
                }
                catch (Exception ex)
                {
                    // Return false if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }

        }

    }

}
