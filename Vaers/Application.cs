﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Vaers
{

    public class ThisApp
    {

        // Vars to hold info about the API
        public const string Name = "Vaers";
        public const string Version = "0.6";

        public static void Terminate(int CountDownInMS)
        {
            // Launch thread to shutdown
            System.Threading.Thread thrTerminate = new System.Threading.Thread(() => TerminateHelper(CountDownInMS));
            thrTerminate.SetApartmentState(System.Threading.ApartmentState.STA);
            thrTerminate.IsBackground = true;
            thrTerminate.Start();
        }
        public static void Terminate()
        {
            // Shut down with no delay
            TerminateHelper(0);
        }
        private static void TerminateHelper(int CountDownInMS)
        {
            // Ensure countdown is good
            if (CountDownInMS < 0) CountDownInMS = 100;

            // Wait if requested
            if (CountDownInMS > 0) System.Threading.Thread.Sleep(CountDownInMS);

            // Force shutdown
            Environment.Exit(0);
        }

        // Structure to hold folder paths
        public struct Folders
        {
            // Returns the directory where the app is running from
            public static string AppRoot
            {
                get
                {
                    // Return the folder we're running out of
                    return StaticMethods.WinOS.Process.Current.GetRootDirectory();
                }
            }

            // Returns path to local app data folder
            public static string ApiAppDataFolder
            {
                get
                {
                    // Create string to hold the app data folder path
                    string FolderPath = System.IO.Path.Combine(StaticMethods.FileSystem.GetLocalAppDataFolderPath(), StaticMethods.FileSystem.CleanFileName(ThisApp.Name));
                    StaticMethods.FileSystem.CreateFolder(FolderPath);

                    // Once the folder is there, return it
                    return FolderPath;
                }
            }

            // Returns the directory for the logging folder
            public static string LogRoot
            {
                get
                {
                    // Create the full folder path and ensure it exists
                    string FolderPath = System.IO.Path.Combine(ApiAppDataFolder, "Logs");
                    StaticMethods.FileSystem.CreateFolder(FolderPath);

                    // Return the path
                    return FolderPath;
                }
            }
        }

        // Structure to hold file paths
        public struct Files
        {
            // Read only to return file path of settings file
            public static string SettingsXml
            {
                get
                {
                    // Create full path and ensure it exists
                    string FullPath = System.IO.Path.Combine(Folders.ApiAppDataFolder, "Settings.xml");
                    StaticMethods.FileSystem.CreateFolder(FullPath);

                    // Return the path
                    return FullPath;
                }
            }
        }

        // Common methods
        public struct Methods
        {

            // Struc to hold general initialization routines
            public struct Initialization
            {
                public void Initialize()
                {
                    // Log debug
                    ThisApp.Methods.Logging.WriteMsg("Initializing application...", Logging.MsgTypeOptions.Info);

                    // Attempt to restore whatever version of SQLite DLL is needed based on how they're running (x86/x64)
                    ThisApp.Methods.Logging.WriteMsg("Ensuring correct SQLite DLL is present...", Logging.MsgTypeOptions.Info);
                    StaticMethods.Database.SQLite.WriteBinaryDependencies(StaticMethods.Database.SQLite.SQLiteDLL.AUTO);

                    // Report error if there's no DLL in place (there's nothing we can do)
                    if (StaticMethods.Database.SQLite.CheckBinaryDependencies() == false)
                    {
                        throw new Exception("SQLite DLL is missing and could not be restored!");
                    }

                    // Log debug
                    ThisApp.Methods.Logging.WriteMsg("App initialization is complete.", Logging.MsgTypeOptions.Info);
                }
            }

            // Struc to hold general maintenance routines
            public struct Maintenance
            {

            }

            // Struc to hold various shared methods
            public struct Common
            {
                public static string AddCommasToNumber(double Number, UInt16 NumberOfDecimalPoints)
                {
                    // Convert to currency using helper
                    string AsCurrency = StaticMethods.Strings.Convert.ToCurrencyString(Number, NumberOfDecimalPoints, true);

                    // Once converted, remove dollar sign and return
                    return AsCurrency.Replace("$", string.Empty);
                }

                public static string KvpListToMarkdown(List<KeyValuePair<string, uint>> Data, string KeyHeader, string ValueHeader)
                {
                    // Create array to hold data
                    string[,] ArrData = new string[Data.Count + 1, 2];

                    // Add headers
                    ArrData[0, 0] = KeyHeader;
                    ArrData[0, 1] = ValueHeader;

                    // Create pointer for adding data
                    int CurrentPosition = 1;

                    // Loop through and insert data
                    foreach (var PAIR in Data)
                    {
                        ArrData[CurrentPosition, 0] = PAIR.Key;
                        ArrData[CurrentPosition, 1] = AddCommasToNumber(PAIR.Value,0);
                        CurrentPosition += 1;
                    }

                    // Create the text and return
                    return StaticMethods.Strings.Generate.MarkdownTextTable(ArrData);
                }
            }

            // Struc to hold all logging methods
            public struct Logging
            {
                // Create shared logging manager to write msgs
                private static Utilities.MessageLogger MsgLogger = new Utilities.MessageLogger(false, true, new Utilities.MessageLogger.ExternalFile(InputRootFolder: Folders.LogRoot, InputBaseFileName: ThisApp.Name, InputMaxFileSizeInMB: 5));

                // Enum to specify log msg type
                public enum MsgTypeOptions
                {
                    Info,
                    Warning,
                    Error,
                    Exception,
                    Debug,
                    AsIs
                }

                // Helper to log a general msg
                public static void WriteMsg(string MsgToWrite, MsgTypeOptions MsgType)
                {
                    try
                    {
                        // Bail if msg is null (note null is not the same as empty)
                        if (MsgToWrite == null) return;

                        // For exception, make the msg upper case
                        if (MsgType == MsgTypeOptions.Exception) MsgToWrite.ToUpper();

                        // Capture the current time (pad so it always looks spaced correctly in text file mm/dd/yyyy hh:mm:ss xM)
                        string PaddedTimestamp = StaticMethods.Strings.Generate.UniqueID(true, true).PadRight(25);

                        // Convert the msg type to a padded string as well for the log
                        string PaddedMsgTypeString = MsgType.ToString().PadRight(MsgTypePaddingLength);

                        // Check if this is a non-timestamped msg
                        if (MsgType == MsgTypeOptions.AsIs)
                        {
                            // If so, clear the timestamp and type string (this type of msg is used to dump stack traces and stuff like that)
                            PaddedTimestamp = string.Empty;
                            PaddedMsgTypeString = string.Empty;
                        }

                        // Create list to hold the merged msg to write
                        var LinesToMerge = new List<string>();

                        // Add the items
                        LinesToMerge.Add(PaddedTimestamp);
                        LinesToMerge.Add(PaddedMsgTypeString);
                        LinesToMerge.Add(MsgToWrite);

                        // Join info (empties will essentially be ignored in String.Join)
                        string FullMsgToWrite = string.Join(string.Empty, LinesToMerge);

                        // Attempt to log the msg
                        MsgLogger.WriteLine(FullMsgToWrite);
                    }
                    catch (Exception ex)
                    {
                        // Just log errors and ignore
                        System.Diagnostics.Trace.WriteLine("Exception while attempting to write log message! - " + ex.Message.ToUpper());
                    }
                }

                // Helper to log exception msg
                public static void WriteMsgWithException(string MainMsg, Exception ExceptionDetails)
                {
                    // Create vars to hold exception info
                    string MergedExceptionMsgs = string.Empty;
                    string ExceptionStackTrace = string.Empty;

                    // Save info if there's an exception
                    if (ExceptionDetails != null)
                    {
                        MergedExceptionMsgs = StaticMethods.ExceptionHandling.GetAllNestedExceptionMessages(ExceptionDetails);
                        ExceptionStackTrace = ExceptionDetails.StackTrace;
                    }

                    // Always write the main msg
                    WriteMsg(MainMsg, MsgTypeOptions.Exception);

                    // Bail if there was no attached exception
                    if (ExceptionDetails == null) return;

                    // If exception was there, log extra info
                    WriteMsg(MergedExceptionMsgs, MsgTypeOptions.Exception);
                    WriteMsg(ExceptionStackTrace, MsgTypeOptions.AsIs);
                }

                // Used to request that all log msgs be immediately pushed to disk
                public static void WriteQueueToDiskImmediately()
                {
                    MsgLogger.WriteQueueToDiskImmediately();
                }

                // Method to cleanup old logs
                public static void DeleteOldLogs()
                {
                    DeleteOldLogs(10);
                }
                public static void DeleteOldLogs(UInt16 MaxLogsToKeep)
                {
                    // Make call to delete old logs
                    MsgLogger.DeleteOldLogs(MaxLogsToKeep);
                }

                // Helper to get padding length to use for msg type
                private static int MsgTypePaddingLength
                {
                    get
                    {
                        // Create var to hold how long to pad msg type to
                        int MsgTypePadLength = 12;

                        try
                        {
                            // Get all enums as strings to check for max length
                            var EnumsAsStrings = StaticMethods.Reflection.Enumerations.GetEntryNames(typeof(MsgTypeOptions));
                            if (EnumsAsStrings != null && EnumsAsStrings.Count > 0)
                            {
                                // If we get enums, sort by longest
                                EnumsAsStrings = (from AllInfo in EnumsAsStrings orderby AllInfo.Length select AllInfo).ToList();

                                // Add a couple extra padding spaces on and save
                                MsgTypePadLength = EnumsAsStrings.Last().Length + 2;
                            }
                        }
                        catch (Exception ex)
                        {
                            // Ignore errors
                            System.Diagnostics.Trace.Write(ex.Message);
                        }

                        // Return the padding length
                        return MsgTypePadLength;
                    }
                }
            }

            // Struc to show common user prompts
            public struct UserPrompts
            {
                public static void ShowErrorMsg(string Header, string Body, Exception Ex = null)
                {
                    // Create string to hold full body
                    string FullBody = Body;

                    // Check if we have an exception
                    if (Ex != null) 
                    {
                        // If so, get merged info
                        string MergedExceptionMsg = StaticMethods.ExceptionHandling.GetAllNestedExceptionMessages(Ex);

                        // Add onto body
                        FullBody += "\r\n\r\nException Details:\r\n" + MergedExceptionMsg;
                    }

                    // Show the message
                    System.Windows.Forms.MessageBox.Show(FullBody, Header, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                }
            }

        }

    }

}
