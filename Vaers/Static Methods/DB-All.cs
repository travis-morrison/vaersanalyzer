﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace StaticMethods
{

    public partial struct Database
    {

        #region Declarations


        // Enum to specify the data type of a new column
        public enum ColumnDataTypeOptions
        {
            Text,
            NumericInt,
            NumericDbl,
            ShortDate,
            LongDate
        }


        #endregion

        #region Common Methods


        /// <summary>
        /// Used to "clean" a SQL command / string by replacing any single apostrophes (') with double apostrophe ('') escapes - Alias for RemoveIllegalChars.
        /// </summary>
        /// <param name="StringToClean">The string to clean.</param>
        /// <param name="RemoveNewLines">OPTIONAL:  Flag to indicate if new line characters should be removed from the string.</param>
        /// <returns>Returns the "clean" version of the string.</returns>
        public static string CleanSqlString(string StringToClean, bool RemoveNewLines = true)
        {
            // Use common method with false flags for extra parameters
            return RemoveIllegalChars(StringToClean, RemoveNewLines, false, false);
        }
        /// <summary>
        /// Used to "clean" a SQL command / string to remove characters that might cause a SQL error (always doubles the apostrophe (') character to make SQL commands safe) - Alias for RemoveIllegalChars.
        /// </summary>
        /// <param name="StringToClean">The string to clean.</param>
        /// <param name="RemoveNewLines">Flag to indicate if new line characters should be removed from the string.</param>
        /// <param name="RemovePoundSigns">Boolean to indicate if pound (#) signs should be removed from the string.</param>
        /// <param name="RemoveCommas">Boolean to indicate if commas (,) should be removed from the string.</param>
        /// <returns>Returns the "clean" version of the string.</returns>
        public static string CleanSqlString(string StringToClean, bool RemoveNewLines, bool RemovePoundSigns, bool RemoveCommas)
        {
            // Use common method
            return RemoveIllegalChars(StringToClean, RemoveNewLines, RemovePoundSigns, RemoveCommas);
        }
        /// <summary>
        /// Used to "clean" a SQL command / string by replacing any single apostrophes (') with double apostrophe ('') escapes.
        /// </summary>
        /// <param name="StringToClean">The string to clean.</param>
        /// <param name="RemoveNewLines">OPTIONAL:  Flag to indicate if new line characters should be removed from the string.</param>
        /// <returns>Returns the "clean" version of the string.</returns>
        public static string RemoveIllegalChars(string StringToClean, bool RemoveNewLines = true)
        {
            // Use common method with false flags for extra parameters
            return RemoveIllegalChars(StringToClean, RemoveNewLines, false, false);
        }
        /// <summary>
        /// Used to "clean" a SQL command / string to remove characters that might cause a SQL error (always doubles the apostrophe (') character to make SQL commands safe).
        /// </summary>
        /// <param name="StringToClean">The string to clean.</param>
        /// <param name="RemoveNewLines">Flag to indicate if new line characters should be removed from the string.</param>
        /// <param name="RemovePoundSigns">Boolean to indicate if pound (#) signs should be removed from the string.</param>
        /// <param name="RemoveCommas">Boolean to indicate if commas (,) should be removed from the string.</param>
        /// <returns>Returns the "clean" version of the string.</returns>
        public static string RemoveIllegalChars(string StringToClean, bool RemoveNewLines, bool RemovePoundSigns, bool RemoveCommas)
        {
            // Return empty string if nothing was passed in
            if (string.IsNullOrEmpty(StringToClean) == true) return string.Empty;

            // Create var to hold string to return
            string StringToReturn = StringToClean;

            // Always remove non-standard chars first
            StringToReturn = RemoveNonStandardCharacters(StringToReturn, AllowNewLinesCharacters: !RemoveNewLines);

            // Always remove the ` (slanted apostrophe), some databases will treat this as a standard apostrophe and cause sql errors / injection risk
            StringToReturn = StringToReturn.Replace("`", string.Empty);

            // Always escape (') w/ ('') to prevent injection attempts 
            StringToReturn = StringToReturn.Replace("'", "''");

            // Remove # signs if requested
            if (RemovePoundSigns == true) StringToReturn = StringToReturn.Replace("#", string.Empty);

            // Remove , chars if requested
            if (RemoveCommas == true) StringToReturn = StringToReturn.Replace(",", string.Empty);

            // Return the remaining string
            return StringToReturn;
        }


        #endregion

        #region Helper Methods


        private static string RemoveNonStandardCharacters(string StringToClean, bool AllowNewLinesCharacters = false)
        {
            try
            {
                // Make sure the string to clean has chars
                if (string.IsNullOrEmpty(StringToClean) == true) return string.Empty;

                // Create a list to hold all valid ASCII chars
                var AllValidAsciiChars = new List<string>();
                for (int i = 32; i <= 126; i++)
                {
                    // Ascii alphanumeric and standard symbols are in range 32 to 126
                    AllValidAsciiChars.Add(System.Text.Encoding.ASCII.GetString(new byte[] { System.Convert.ToByte(i) }));
                }

                // Allow new line chars if user said to leave them in
                if (AllowNewLinesCharacters == true)
                {
                    AllValidAsciiChars.Add("\r");
                    AllValidAsciiChars.Add("\n");
                }

                // Get all characters of the source string
                var AllCharactersInSourceString = StringToClean.ToCharArray().ToList();

                // Get all characters from source string that are in the valid ascii char set and return
                var AllValidCharsToMergeAndReturn = (from AllInfo in AllCharactersInSourceString where AllValidAsciiChars.Contains(AllInfo.ToString()) select AllInfo).ToList();

                // Join the string back together without illegal chars and return
                return string.Join(string.Empty, AllValidCharsToMergeAndReturn);
            }
            catch (Exception ex)
            {
                // Return empty string to be safe if something fails (don't want to return a string that possibly has non-standard chars in it)
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return string.Empty;
            }
        }
        private static bool IsNumeric(object ObjectToCheck)
        {
            try
            {
                // Return false if nothing is set
                if (ObjectToCheck == null) return false;

                // Convert object to string for checking
                string StringValue = ObjectToCheck.ToString();

                // Return false if string is bad / empty
                if (string.IsNullOrEmpty(StringValue) == true) return false;

                // Create dummy var to hold the result of the attempted parse
                double DummyVar = 0;

                // If string looks good, attempt to parse it
                return double.TryParse(StringValue, out DummyVar);
            }
            catch (Exception ex)
            {
                // Return false if error occurs
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return false;
            }
        }
        private static bool IsDate(object ObjectToCheck)
        {
            try
            {
                // Return false if nothing is set
                if (ObjectToCheck == null) return false;

                // Convert object to string for checking
                string StringValue = ObjectToCheck.ToString();

                // Return false if string is bad / empty
                if (string.IsNullOrEmpty(StringValue) == true) return false;

                // Create dummy var to hold the result of the attempted parse
                var DummyVar = DateTime.MinValue;

                // If string looks good, attempt to parse it
                return DateTime.TryParse(StringValue, out DummyVar);
            }
            catch (Exception ex)
            {
                // Return false if error occurs
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return false;
            }
        }


        #endregion

    }

}
