﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StaticMethods
{

    public struct UserPrompts
    {

        /// <summary>
        /// Used to show a generic "Operation Canceled" message.
        /// </summary>
        public static void ShowOperationCancelledMsg()
        {
            System.Windows.Forms.MessageBox.Show("The operation has been canceled per your request.", "Operation Canceled", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
        }

        /// <summary>
        /// Used to show a generic "Unexpected Error" message.
        /// </summary>
        /// <param name="MsgBody">The general error message to show.</param>
        /// <param name="ExceptionMessage">OPTIONAL:  Any additional error details (e.g. the exception message caught in a "Try" block).</param>
        public static void ShowUnexpectedErrorMsg(string MsgBody, string ExceptionMessage = "")
        {
            // Check if additional details were passed in
            if (string.IsNullOrEmpty(ExceptionMessage) == false)
            {
                // If so, show message with extra detail
                System.Windows.Forms.MessageBox.Show(MsgBody + Environment.NewLine + Environment.NewLine + "Error Details:" + Environment.NewLine + ExceptionMessage, "Unexpected Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
            }
            else
            {
                // If not, show message without detail
                System.Windows.Forms.MessageBox.Show(MsgBody, "Unexpected Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
            }
        }
    
    }

}
