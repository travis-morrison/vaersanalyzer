﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;

namespace StaticMethods
{

    public partial struct Database
    {

        public struct SQLite
        {

            // Constant file names for checking binary dependencies
            private const string DLLFileName = "System.Data.SQLite.dll";

            /// <summary>
            /// Checks if all binary dependencies are present in the application's root folder.
            /// </summary>
            /// <returns>Returns TRUE if all binaries are present. Returns FALSE in all other conditions.</returns>
            public static bool CheckBinaryDependencies()
            {
                // Get the active directory
                string ActiveDirectory = GetRootFolderOfExecutingAssembly();

                // Return false if it can't be fetched
                if (string.IsNullOrEmpty(ActiveDirectory) == true) return false;

                // If it works, use underlying method and return result
                return CheckBinaryDependencies(ActiveDirectory);
            }
            /// <summary>
            /// Checks if all binary dependencies are present in a specified folder.
            /// </summary>
            /// <param name="FolderPathToCheck">The folder path to check for dependency binaries.</param>
            /// <returns>Returns TRUE if all binaries are present. Returns FALSE in all other conditions.</returns>
            public static bool CheckBinaryDependencies(string FolderPathToCheck)
            {
                try
                {
                    // Return false if folder isn't specified
                    if (string.IsNullOrEmpty(FolderPathToCheck) == true) return false;

                    // Ensure directory didn't end with "\"
                    if (FolderPathToCheck.Substring(FolderPathToCheck.Length - 1, 1) == @"\")
                    {
                        // If it did, strip off the last "\"
                        FolderPathToCheck = FolderPathToCheck.Substring(0, FolderPathToCheck.Length - 1);
                    }

                    // Check if user accidentally passed in the full FILE path
                    if (System.IO.Path.HasExtension(FolderPathToCheck) == true)
                    {
                        // If they did, extract the folder name only
                        FolderPathToCheck = System.IO.Path.GetDirectoryName(FolderPathToCheck);
                    }

                    // Create full file paths
                    var DLLFullFilePaths = new[] { System.IO.Path.Combine(FolderPathToCheck, DLLFileName) }.ToList();

                    // Loop through and return false if any files are missing
                    foreach (string FILE in DLLFullFilePaths)
                    {
                        if (System.IO.File.Exists(FILE) == false)
                        {
                            return false;
                        }
                    }

                    // If all files are there, return true
                    return true;
                }
                catch (Exception ex)
                {
                    // Return false if unknown error occurs (safest to assume missing)
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }

            /// <summary>
            /// Enum to specify which SQLite DLL to use.
            /// </summary>
            public enum SQLiteDLL
            {
                x86DLL = 0,
                x64DLL = 1,
                AUTO = 2
            }

            /// <summary>
            /// Used to write / restore any binary dependencies needed to work with this API to the application's root directory.
            /// </summary>
            /// <param name="DLLToWrite">Enum indicating which DLL to restore (32 bit, 64 bit, or AutoDetect).</param>
            /// <returns>Returns TRUE if the dependencies are written. Returns FALSE in all other conditions.</returns>
            public static bool WriteBinaryDependencies(SQLiteDLL DLLToWrite)
            {
                try
                {
                    // Get the active directory
                    string ActiveDirectory = GetRootFolderOfExecutingAssembly();

                    // Return false if it can't be fetched
                    if (string.IsNullOrEmpty(ActiveDirectory) == true) return false;

                    // If it works, use underlying method and return result
                    return WriteBinaryDependencies(ActiveDirectory, DLLToWrite);
                }
                catch (Exception ex)
                {
                    // Return false if directory can't be determined
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }
            /// <summary>
            /// Used to write / restore any binary dependencies needed to work with this API.
            /// </summary>
            /// <param name="FolderPathToWriteTo">The directory to write the binaries to.</param>
            /// <param name="DLLToWrite">Enum indicating which binaries to restore (32 bit, 64 bit, or AutoDetect).</param>
            /// <returns>Returns TRUE if the dependencies are written. Returns FALSE in all other conditions.</returns>
            public static bool WriteBinaryDependencies(string FolderPathToWriteTo, SQLiteDLL DLLToWrite)
            {
                try
                {
                    // Create var to hold result of file write
                    bool DLLWriteWorked = false;

                    // Ensure directory didn't end with "\"
                    if (FolderPathToWriteTo.Substring(FolderPathToWriteTo.Length - 1, 1) == @"\")
                    {
                        // If it did, strip off the last "\"
                        FolderPathToWriteTo = FolderPathToWriteTo.Substring(0, FolderPathToWriteTo.Length - 1);
                    }

                    // Check if user accidentally passed in the full FILE path
                    if (System.IO.Path.HasExtension(FolderPathToWriteTo) == true)
                    {
                        // If they did, extract the folder name only
                        FolderPathToWriteTo = System.IO.Path.GetDirectoryName(FolderPathToWriteTo);
                    }

                    // Make sure that the folder exists
                    if (System.IO.Directory.Exists(FolderPathToWriteTo) == false)
                    {
                        System.IO.Directory.CreateDirectory(FolderPathToWriteTo);
                    }

                    // Once folder exists, create var to hold the full DLL path to write
                    string DLLFullFilePath = System.IO.Path.Combine(FolderPathToWriteTo, DLLFileName);

                    // Create var for getting OS size
                    int OSBitSize = GetBitCountOfTargetCPU();

                    // Check which dll to restore based on user's request
                    if (DLLToWrite == SQLiteDLL.x86DLL || (DLLToWrite == SQLiteDLL.AUTO && OSBitSize == 32))
                    {
                        // If user requested 32 bit version OR they requested auto and this is a 32 bit OS, write 32 bit version
                        DLLWriteWorked = WriteSQLiteDLL32Bit(DLLFullFilePath);
                    }
                    else if (DLLToWrite == SQLiteDLL.x64DLL || (DLLToWrite == SQLiteDLL.AUTO && OSBitSize == 64))
                    {
                        // If user requested 64 bit version OR they requested auto and this is a 64 bit OS, write 64 bit version
                        DLLWriteWorked = WriteSQLiteDLL64Bit(DLLFullFilePath);
                    }
                    else
                    {
                        // If auto was requested but OS size could not be determined, key off of version of Windows
                        if (Environment.OSVersion.Version.Major <= 5)
                        {
                            // For XP and earlier, use the 32 bit version
                            DLLWriteWorked = WriteSQLiteDLL32Bit(DLLFullFilePath);
                        }
                        else
                        {
                            // For vista and up, determine what disk the OS is installed on by getting drive letter (first char in path)
                            string WindowsInstallDriveLetter = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles).ToUpper().Substring(0, 1);

                            // Check for program files x86 OR syswow64 to determine if this OS is 64 bit
                            if (System.IO.Directory.Exists(WindowsInstallDriveLetter + @":\Program Files (x86)") == true | System.IO.Directory.Exists(WindowsInstallDriveLetter + @":\Windows\SysWOW64") == true)
                            {
                                // If either folder exists, assume a 64 bit OS
                                DLLWriteWorked = WriteSQLiteDLL64Bit(DLLFullFilePath);
                            }
                            else
                            {
                                // If neither exist, assume a 32 bit OS
                                DLLWriteWorked = WriteSQLiteDLL32Bit(DLLFullFilePath);
                            }
                        }
                    }

                    // Return the result
                    return DLLWriteWorked;
                }
                catch (Exception ex)
                {
                    // Return false if unexpected error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }
            private static int GetBitCountOfTargetCPU()
            {
                try
                {
                    // Get the IntPtr size
                    int IntPtrSize = IntPtr.Size;

                    // Multiply it by 8 to get bit size for the target CPU
                    return (8 * IntPtrSize);
                }
                catch (Exception ex)
                {
                    // Return -1 if we can't tell
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return -1;
                }
            }
            private static bool WriteSQLiteDLL32Bit(string FullDLLPath)
            {
                try
                {
                    // Try in case file already exists and is locked
                    System.IO.File.WriteAllBytes(FullDLLPath, Vaers.Properties.Resources.SQLITE32BIT);

                    // Return true if no errors
                    return true;
                }
                catch (Exception ex)
                {
                    // Return false if errors during write (e.g. file is in use)
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }
            private static bool WriteSQLiteDLL64Bit(string FullDLLPath)
            {
                try
                {
                    // Try in case file already exists and is locked
                    System.IO.File.WriteAllBytes(FullDLLPath, Vaers.Properties.Resources.SQLITE64BIT);

                    // Return true if no errors
                    return true;
                }
                catch (Exception ex)
                {
                    // Return false if errors during write (e.g. file is in use)
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return false;
                }
            }

            /// <summary>
            /// Internal helper to get the current 'running' directory of the assembly (i.e. to put the DLL in)
            /// </summary>
            private static string GetRootFolderOfExecutingAssembly()
            {
                try
                {
                    // Try to use reflection to figure out where app is running from
                    return System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                }
                catch (Exception)
                {
                    // No action needed on failure
                }

                try
                {
                    // If reflection doesn't work, try using startup info from environment (first cmd line arg is normally the path to the running process)
                    return System.IO.Path.GetDirectoryName(Environment.GetCommandLineArgs()[0]);
                }
                catch (Exception)
                {
                    // No action needed on failure
                }

                // If nothing works, return empty string
                return string.Empty;
            }

            // Default / constants for sql cmds
            private struct Defaults
            {
                public const int OverrideCompactLockAfterXSecsPerMB = 1;
                public const int OverrideReadWriteLockAfterXSeconds = 30;
            }

            // Enum to specify the data type of the unique ID column for deleting corrupted rows
            public enum UniqueIDColumnDataType
            {
                Numeric = 0,
                Text = 1
            }

            // OVERLOADED METHODS FOR PERFORMING A SINGLE DATABSE UPDATE CMD
            /// <summary>
            /// Used to perform a single database update command.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLCmd">The SQL command to execute.</param>
            /// <returns>Returns TRUE if the command executes correctly. Returns FALSE in all other conditions.</returns>
            public static bool ExecuteSQLUpdateCmd(string FullDatabasePath, string DatabasePassword, string SQLCmd)
            {
                // Use common method and pass defaults for unspecified values
                return ExecuteSQLXactionCmds(FullDatabasePath, DatabasePassword, new[] { SQLCmd }, false, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB, false);
            }
            /// <summary>
            /// Used to perform a single database update command.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLCmd">The SQL command to execute.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <returns>Returns TRUE if the command executes correctly. Returns FALSE in all other conditions.</returns>
            public static bool ExecuteSQLUpdateCmd(string FullDatabasePath, string DatabasePassword, string SQLCmd, bool LockDBDuringExchange)
            {
                // Use common method and pass defaults for unspecified values
                return ExecuteSQLXactionCmds(FullDatabasePath, DatabasePassword, new[] { SQLCmd }, LockDBDuringExchange, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB, false);
            }
            /// <summary>
            /// Used to perform a single database update command.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLCmd">The SQL command to execute.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <param name="TryToConvertMSAccesCmdToSQLiteCmd">Boolean to indicate if the function should attempt to convert the SQL command from MSAccess format to SQLite format by replacing "#DATES#" with SQLite's "DATE(" / "DATETIME(" format.</param>
            /// <returns>Returns TRUE if the command executes correctly. Returns FALSE in all other conditions.</returns>
            public static bool ExecuteSQLUpdateCmd(string FullDatabasePath, string DatabasePassword, string SQLCmd, bool LockDBDuringExchange, bool TryToConvertMSAccesCmdToSQLiteCmd)
            {
                // Use common method and pass defaults for unspecified values
                return ExecuteSQLXactionCmds(FullDatabasePath, DatabasePassword, new[] { SQLCmd }, LockDBDuringExchange, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB, TryToConvertMSAccesCmdToSQLiteCmd);
            }
            /// <summary>
            /// Used to perform a single database update command.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLCmd">The SQL command to execute.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <param name="OverrideReadWriteLockIfLockedMoreThanXSeconds">The amount of time (in seconds) to wait before a database READ/WRITE lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <param name="OverrideCompactLockIfLockedMoreThanXSecsPerMB">The amount of time (in seconds per megabyte) to wait before a COMPACTING database lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <param name="TryToConvertMSAccesCmdToSQLiteCmd">Boolean to indicate if the function should attempt to convert the SQL command from MSAccess format to SQLite format by replacing "#DATES#" with SQLite's "DATE(" / "DATETIME(" format.</param>
            /// <returns>Returns TRUE if the command executes correctly. Returns FALSE in all other conditions.</returns>
            public static bool ExecuteSQLUpdateCmd(string FullDatabasePath, string DatabasePassword, string SQLCmd, bool LockDBDuringExchange, int OverrideReadWriteLockIfLockedMoreThanXSeconds, int OverrideCompactLockIfLockedMoreThanXSecsPerMB, bool TryToConvertMSAccesCmdToSQLiteCmd)
            {
                // Use common method for xactions by placing single sql statement in array
                return ExecuteSQLXactionCmds(FullDatabasePath, DatabasePassword, new[] { SQLCmd }, LockDBDuringExchange, OverrideReadWriteLockIfLockedMoreThanXSeconds, OverrideCompactLockIfLockedMoreThanXSecsPerMB, TryToConvertMSAccesCmdToSQLiteCmd);
            }

            // OVERLOADED METHODS FOR PERFORMING A SET SQL CMDS IN AN "ALL OR NOTHING" TRANSACTION
            /// <summary>
            /// Used to execute multiple SQL update commands as an "all or nothing" transaction.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLCmds">The string array of SQL update commands to execute.</param>
            /// <returns>Returns TRUE if all commands work. Returns FALSE in all other conditions.</returns>
            public static bool ExecuteSQLXactionCmds(string FullDatabasePath, string DatabasePassword, IEnumerable<string> SQLCmds)
            {
                // Use common method and pass defaults for unspecified values
                return ExecuteSQLXactionCmds(FullDatabasePath, DatabasePassword, SQLCmds, false, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB, false);
            }
            /// <summary>
            /// Used to execute multiple SQL update commands as an "all or nothing" transaction.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLCmds">The string array of SQL update commands to execute.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <returns>Returns TRUE if all commands work. Returns FALSE in all other conditions.</returns>
            public static bool ExecuteSQLXactionCmds(string FullDatabasePath, string DatabasePassword, IEnumerable<string> SQLCmds, bool LockDBDuringExchange)
            {
                // Use common method and pass defaults for unspecified values
                return ExecuteSQLXactionCmds(FullDatabasePath, DatabasePassword, SQLCmds, LockDBDuringExchange, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB, false);
            }
            /// <summary>
            /// Used to execute multiple SQL update commands as an "all or nothing" transaction.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLCmds">The string array of SQL update commands to execute.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <param name="TryToConvertMSAccesCmdToSQLiteCmd">Boolean to indicate if the function should attempt to convert the SQL command from MSAccess format to SQLite format by replacing "#DATES#" with SQLite's "DATE(" / "DATETIME(" format.</param>
            /// <returns>Returns TRUE if all commands work. Returns FALSE in all other conditions.</returns>
            public static bool ExecuteSQLXactionCmds(string FullDatabasePath, string DatabasePassword, IEnumerable<string> SQLCmds, bool LockDBDuringExchange, bool TryToConvertMSAccesCmdToSQLiteCmd)
            {
                // Use common method and pass defaults for unspecified values
                return ExecuteSQLXactionCmds(FullDatabasePath, DatabasePassword, SQLCmds, LockDBDuringExchange, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB, TryToConvertMSAccesCmdToSQLiteCmd);
            }
            /// <summary>
            /// Used to execute multiple SQL update commands as an "all or nothing" transaction.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLCmds">The string array of SQL update commands to execute.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <param name="OverrideReadWriteLockIfLockedMoreThanXSeconds">The amount of time (in seconds) to wait before a database READ/WRITE lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <param name="OverrideCompactLockIfLockedMoreThanXSecsPerMB">The amount of time (in seconds per megabyte) to wait before a COMPACTING database lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <param name="TryToConvertMSAccesCmdToSQLiteCmd">Boolean to indicate if the function should attempt to convert the SQL command from MSAccess format to SQLite format by replacing "#DATES#" with SQLite's "DATE(" / "DATETIME(" format.</param>
            /// <returns>Returns TRUE if all commands work. Returns FALSE in all other conditions.</returns>
            public static bool ExecuteSQLXactionCmds(string FullDatabasePath, string DatabasePassword, IEnumerable<string> SQLCmds, bool LockDBDuringExchange, int OverrideReadWriteLockIfLockedMoreThanXSeconds, int OverrideCompactLockIfLockedMoreThanXSecsPerMB, bool TryToConvertMSAccesCmdToSQLiteCmd)
            {
                // Create var to hold if xaction works
                bool XactionWorked = false;

                // Create vars for capturing info IF a command fails
                int IndexOfFailingCmd = 0;
                string CmdThatFailed = string.Empty;

                // Check for database lock before continuing and execute lock if requested
                if (MainLockingCheckAndSetRoutine(FullDatabasePath, LockType.ReadWrite, OverrideReadWriteLockIfLockedMoreThanXSeconds, OverrideCompactLockIfLockedMoreThanXSecsPerMB, LockDBDuringExchange) == false)
                {
                    // If the db locked from a previous op OR cannot be locked, the function prompts, just return false
                    return false;
                }

                // Create SQLite database connection & xaction
                string ConnStr = CreateConnectionString(FullDatabasePath, DatabasePassword);
                var DBConn = new System.Data.SQLite.SQLiteConnection(ConnStr);
                System.Data.SQLite.SQLiteTransaction DBTrans = null;

                // Convert input commands into list so it's easier to work with
                var SQLCmdList = SQLCmds.ToList();

                // Convert MSAccess SQL date styles to SQLite date styles if user requested conversion
                if (TryToConvertMSAccesCmdToSQLiteCmd == true)
                {
                    for (int i = 0; i <= SQLCmdList.Count - 1; i++)
                    {
                        SQLCmdList[i] = ConvertMSAccessCmdToSQLiteCmd(SQLCmdList[i]);
                    }
                }

                try
                {
                    // Open database connection
                    DBConn.Open();

                    // Build the command
                    using (var CmdToExecute = new System.Data.SQLite.SQLiteCommand())
                    {
                        // Init the transaction
                        DBTrans = DBConn.BeginTransaction();
                        CmdToExecute.Connection = DBConn;
                        CmdToExecute.Transaction = DBTrans;

                        // Try to execute each SQL statement in the cmds array, will err to catch on failure & skip the commit
                        for (int i = 0; i <= SQLCmdList.Count - 1; i++)
                        {
                            // Save the current cmd info to vars in case if fails to execute
                            IndexOfFailingCmd = i;
                            CmdThatFailed = SQLCmdList[i];

                            // Execute the cmd
                            CmdToExecute.CommandText = SQLCmdList[i];
                            CmdToExecute.ExecuteNonQuery();
                        }

                        // Commit the changes & close xaction
                        DBTrans.Commit();
                        DBTrans.Dispose();
                    }

                    // Close the connection (the commit only takes if the conn is closed)
                    DBConn.Close();

                    // Save that the xaction worked
                    XactionWorked = true;
                }
                catch (Exception ex)
                {
                    // Close the connection
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    DBConn.Close();
                }
                finally
                {
                    // Release db connection resources
                    DBConn.Dispose();
                }

                // If database was locked, unlock it
                if (LockDBDuringExchange == true)
                {
                    LockOrUnlockSQLiteDatabase(FullDatabasePath, LockType.ReadWrite, LockState.Unlock);
                }

                // Return the result of the xaction
                return XactionWorked;
            }

            // OVERLOADED METHODS FOR EXECUTING A DATABASE CMD THAT RETURNS A VALUE
            /// <summary>
            /// Used to execute and return the value of an individual scalar command (e.g. MIN, MAX, COUNT, etc.).
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLCmd">The SQL command to execute.</param>
            /// <returns>Returns the value of the scalar command as an OBJECT if the command works AND value is determined. Returns an empty string if command works but there is no result (e.g. select max on a table with no records). Returns NULL in all other conditions.</returns>
            public static object ExecuteSQLScalarCmd(string FullDatabasePath, string DatabasePassword, string SQLCmd)
            {
                // Use common method and pass defaults for unspecified values
                return ExecuteSQLScalarCmd(FullDatabasePath, DatabasePassword, SQLCmd, false, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB, false);
            }
            /// <summary>
            /// Used to execute and return the value of an individual scalar command (e.g. MIN, MAX, COUNT, etc.).
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLCmd">The SQL command to execute.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <returns>Returns the value of the scalar command as an OBJECT if the command works AND value is determined. Returns an empty string if command works but there is no result (e.g. select max on a table with no records). Returns NULL in all other conditions.</returns>
            public static object ExecuteSQLScalarCmd(string FullDatabasePath, string DatabasePassword, string SQLCmd, bool LockDBDuringExchange)
            {
                // Use common method and pass defaults for unspecified values
                return ExecuteSQLScalarCmd(FullDatabasePath, DatabasePassword, SQLCmd, LockDBDuringExchange, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB, false);
            }
            /// <summary>
            /// Used to execute and return the value of an individual scalar command (e.g. MIN, MAX, COUNT, etc.).
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLCmd">The SQL command to execute.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <param name="TryToConvertMSAccesCmdToSQLiteCmd">Boolean to indicate if the function should attempt to convert the SQL command from MSAccess format to SQLite format by replacing "#DATES#" with SQLite's "DATE(" / "DATETIME(" format.</param>
            /// <returns>Returns the value of the scalar command as an OBJECT if the command works AND value is determined. Returns an empty string if command works but there is no result (e.g. select max on a table with no records). Returns NULL in all other conditions.</returns>
            public static object ExecuteSQLScalarCmd(string FullDatabasePath, string DatabasePassword, string SQLCmd, bool LockDBDuringExchange, bool TryToConvertMSAccesCmdToSQLiteCmd)
            {
                // Use common method and pass defaults for unspecified values
                return ExecuteSQLScalarCmd(FullDatabasePath, DatabasePassword, SQLCmd, LockDBDuringExchange, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB, TryToConvertMSAccesCmdToSQLiteCmd);
            }
            /// <summary>
            /// Used to execute and return the value of an individual scalar command (e.g. MIN, MAX, COUNT, etc.).
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLCmd">The SQL command to execute.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <param name="OverrideReadWriteLockIfLockedMoreThanXSeconds">The amount of time (in seconds) to wait before a database READ/WRITE lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <param name="OverrideCompactLockIfLockedMoreThanXSecsPerMB">The amount of time (in seconds per megabyte) to wait before a COMPACTING database lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <param name="TryToConvertMSAccesCmdToSQLiteCmd">Boolean to indicate if the function should attempt to convert the SQL command from MSAccess format to SQLite format by replacing "#DATES#" with SQLite's "DATE(" / "DATETIME(" format.</param>
            /// <returns>Returns the value of the scalar command as an OBJECT if the command works. Returns NOTHING in all other conditions.</returns>
            public static object ExecuteSQLScalarCmd(string FullDatabasePath, string DatabasePassword, string SQLCmd, bool LockDBDuringExchange, int OverrideReadWriteLockIfLockedMoreThanXSeconds, int OverrideCompactLockIfLockedMoreThanXSecsPerMB, bool TryToConvertMSAccesCmdToSQLiteCmd)
            {
                // Create var to hold sql result
                object ScalarResult = null;

                // Check for database lock before continuing and execute lock if requested
                if (MainLockingCheckAndSetRoutine(FullDatabasePath, LockType.ReadWrite, OverrideReadWriteLockIfLockedMoreThanXSeconds, OverrideCompactLockIfLockedMoreThanXSecsPerMB, LockDBDuringExchange) == false)
                {
                    // If the db locked from a previous op OR cannot be locked, the function prompts, just return nothing
                    return ScalarResult;
                }

                // Create SQLite database connection
                string ConnStr = CreateConnectionString(FullDatabasePath, DatabasePassword);
                var DBConn = new System.Data.SQLite.SQLiteConnection(ConnStr);

                // If user requested conversion, try to convert Access SQL cmd to SQLite version 
                if (TryToConvertMSAccesCmdToSQLiteCmd == true) SQLCmd = ConvertMSAccessCmdToSQLiteCmd(SQLCmd);

                try
                {
                    // Open database connection
                    DBConn.Open();

                    // Build the command
                    using (var ScalarCommand = new System.Data.SQLite.SQLiteCommand())
                    {
                        // Setup the cmd
                        ScalarCommand.Connection = DBConn;
                        ScalarCommand.CommandType = System.Data.CommandType.Text;
                        ScalarCommand.CommandText = SQLCmd;

                        // Execute the command and capture the result
                        ScalarResult = ScalarCommand.ExecuteScalar();
                    }

                    // Check if value is still null
                    if (ScalarResult == null)
                    {
                        // If so, convert it to empty string to signify sql worked
                        ScalarResult = string.Empty;
                    }

                    // Close the database connection
                    DBConn.Close();
                }
                catch (Exception ex)
                {
                    // Close the connection
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    DBConn.Close();
                }
                finally
                {
                    // Release db connection resources
                    DBConn.Dispose();
                }

                // If database was locked, unlock it
                if (LockDBDuringExchange == true)
                {
                    LockOrUnlockSQLiteDatabase(FullDatabasePath, LockType.ReadWrite, LockState.Unlock);
                }

                // Return the result of the sql scalar cmd
                return ScalarResult;
            }

            // OVERLOADED METHODS FOR PERFORMING MULTIPLE SQL COUNT CMDS (DURING ONE OPEN FOR IMPROVED SPEED) AND RETURNING THE COUNTS
            /// <summary>
            /// Used to execute multiple SQL COUNT commands and return the counts as a list in one database open / close session (faster than doing individual cmds).
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLCountCmds">The list of SQL COUNT commands to execute.</param>
            /// <param name="refCountsToReturn">BYREF:  The list that will return the counts for each command. A -1 indicates that the respective count command at that index did not return a numeric value.</param>
            /// <returns>Returns TRUE if ALL commands execute. Returns FALSE in all other conditions.</returns>
            public static bool ExecuteMultipleSQLCountCmds(string FullDatabasePath, string DatabasePassword, List<string> SQLCountCmds, ref List<int> refCountsToReturn)
            {
                // Use common method and pass defaults for unspecified values
                return ExecuteMultipleSQLCountCmds(FullDatabasePath, DatabasePassword, SQLCountCmds, ref refCountsToReturn, false, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB, false);
            }
            /// <summary>
            /// Used to execute multiple SQL COUNT commands and return the counts as a list in one database open / close session (faster than doing individual cmds).
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLCountCmds">The list of SQL COUNT commands to execute.</param>
            /// <param name="refCountsToReturn">BYREF:  The list that will return the counts for each command. A -1 indicates that the respective count command at that index did not return a numeric value.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <returns>Returns TRUE if ALL commands execute. Returns FALSE in all other conditions.</returns>
            public static bool ExecuteMultipleSQLCountCmds(string FullDatabasePath, string DatabasePassword, List<string> SQLCountCmds, ref List<int> refCountsToReturn, bool LockDBDuringExchange)
            {
                // Use common method and pass defaults for unspecified values
                return ExecuteMultipleSQLCountCmds(FullDatabasePath, DatabasePassword, SQLCountCmds, ref refCountsToReturn, LockDBDuringExchange, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB, false);
            }
            /// <summary>
            /// Used to execute multiple SQL COUNT commands and return the counts as a list in one database open / close session (faster than doing individual cmds).
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLCountCmds">The list of SQL COUNT commands to execute.</param>
            /// <param name="refCountsToReturn">BYREF:  The list that will return the counts for each command. A -1 indicates that the respective count command at that index did not return a numeric value.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <param name="TryToConvertMSAccesCmdToSQLiteCmd">Boolean to indicate if the function should attempt to convert the SQL command from MSAccess format to SQLite format by replacing "#DATES#" with SQLite's "DATE(" / "DATETIME(" format.</param>
            /// <returns>Returns TRUE if ALL commands execute. Returns FALSE in all other conditions.</returns>
            public static bool ExecuteMultipleSQLCountCmds(string FullDatabasePath, string DatabasePassword, List<string> SQLCountCmds, ref List<int> refCountsToReturn, bool LockDBDuringExchange, bool TryToConvertMSAccesCmdToSQLiteCmd)
            {
                // Use common method and pass defaults for unspecified values
                return ExecuteMultipleSQLCountCmds(FullDatabasePath, DatabasePassword, SQLCountCmds, ref refCountsToReturn, LockDBDuringExchange, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB, TryToConvertMSAccesCmdToSQLiteCmd);
            }
            /// <summary>
            /// Used to execute multiple SQL COUNT commands and return the counts as a list in one database open / close session (faster than doing individual cmds).
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLCountCmds">The list of SQL COUNT commands to execute.</param>
            /// <param name="refCountsToReturn">BYREF:  The list that will return the counts for each command. A -1 indicates that the respective count command at that index did not return a numeric value.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <param name="OverrideReadWriteLockIfLockedMoreThanXSeconds">The amount of time (in seconds) to wait before a database READ/WRITE lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <param name="OverrideCompactLockIfLockedMoreThanXSecsPerMB">The amount of time (in seconds per megabyte) to wait before a COMPACTING database lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <param name="TryToConvertMSAccesCmdToSQLiteCmd">Boolean to indicate if the function should attempt to convert the SQL command from MSAccess format to SQLite format by replacing "#DATES#" with SQLite's "DATE(" / "DATETIME(" format.</param>
            /// <returns>Returns TRUE if ALL commands execute. Returns FALSE in all other conditions.</returns>
            public static bool ExecuteMultipleSQLCountCmds(string FullDatabasePath, string DatabasePassword, List<string> SQLCountCmds, ref List<int> refCountsToReturn, bool LockDBDuringExchange, int OverrideReadWriteLockIfLockedMoreThanXSeconds, int OverrideCompactLockIfLockedMoreThanXSecsPerMB, bool TryToConvertMSAccesCmdToSQLiteCmd)
            {
                // Clear reference list before beginning
                refCountsToReturn = new List<int>();

                // Save the cmd we're looking for
                const string RequiredSelectCmd = "SELECT COUNT";

                // Verify commands are "COUNT" scalar cmds
                foreach (string CMD in SQLCountCmds)
                {
                    // Return false if any cmd is missing the correct SELECT syntax
                    if (CMD.ToUpper().Contains(RequiredSelectCmd) == false)
                    {
                        return false;
                    }
                }

                // Create object list to use low level function
                List<object> objList = null;

                // Use shared routine to try and get the counts
                bool Result = ExecuteMultipleSQLScalarCmds(FullDatabasePath, DatabasePassword, SQLCountCmds, ref objList, LockDBDuringExchange, OverrideReadWriteLockIfLockedMoreThanXSeconds, OverrideCompactLockIfLockedMoreThanXSecsPerMB, TryToConvertMSAccesCmdToSQLiteCmd);

                // If sql failed, just return false
                if (Result == false) return false;

                // If sql worked, loop through and convert objects to ints
                foreach (object OBJ in objList)
                {
                    // Check if result came back
                    if (OBJ != null && Database.IsNumeric(OBJ) == true)
                    {
                        // If good, convert to int and add to reference list
                        refCountsToReturn.Add(Convert.ToInt32(OBJ));
                    }
                    else
                    {
                        // If cmd failed or value is invalid, use -1
                        refCountsToReturn.Add(-1);
                    }
                }

                // Return true once all values are converted
                return true;
            }

            // OVERLOADED METHODS FOR PERFORMING MULTIPLE SQL SUM CMDS (DURING ONE OPEN FOR IMPROVED SPEED) AND RETURNING THE SUMS
            /// <summary>
            /// Used to execute multiple SQL SUM commands and return the sums as a list in one database open / close session (faster than doing individual cmds).
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLSumCmds">The list of SQL SUM commands to execute.</param>
            /// <param name="refSumsToReturn">BYREF:  The list that will return the sums for each command.</param>
            /// <returns>Returns TRUE if ALL commands execute. Returns FALSE in all other conditions.</returns>
            public static bool ExecuteMultipleSQLSumCmds(string FullDatabasePath, string DatabasePassword, IEnumerable<string> SQLSumCmds, ref List<Nullable<double>> refSumsToReturn)
            {
                // Use common method and pass defaults for unspecified values
                return ExecuteMultipleSQLSumCmds(FullDatabasePath, DatabasePassword, SQLSumCmds, ref refSumsToReturn, false, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB, false);
            }
            /// <summary>
            /// Used to execute multiple SQL SUM commands and return the sums as a list in one database open / close session (faster than doing individual cmds).
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLSumCmds">The list of SQL SUM commands to execute.</param>
            /// <param name="refSumsToReturn">BYREF:  The list that will return the sums for each command.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <returns>Returns TRUE if ALL commands execute. Returns FALSE in all other conditions.</returns>
            public static bool ExecuteMultipleSQLSumCmds(string FullDatabasePath, string DatabasePassword, IEnumerable<string> SQLSumCmds, ref List<Nullable<double>> refSumsToReturn, bool LockDBDuringExchange)
            {
                // Use common method and pass defaults for unspecified values
                return ExecuteMultipleSQLSumCmds(FullDatabasePath, DatabasePassword, SQLSumCmds, ref refSumsToReturn, LockDBDuringExchange, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB, false);
            }
            /// <summary>
            /// Used to execute multiple SQL SUM commands and return the sums as a list in one database open / close session (faster than doing individual cmds).
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLSumCmds">The list of SQL SUM commands to execute.</param>
            /// <param name="refSumsToReturn">BYREF:  The list that will return the sums for each command.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <param name="TryToConvertMSAccesCmdToSQLiteCmd">Boolean to indicate if the function should attempt to convert the SQL command from MSAccess format to SQLite format by replacing "#DATES#" with SQLite's "DATE(" / "DATETIME(" format.</param>
            /// <returns>Returns TRUE if ALL commands execute. Returns FALSE in all other conditions.</returns>
            public static bool ExecuteMultipleSQLSumCmds(string FullDatabasePath, string DatabasePassword, IEnumerable<string> SQLSumCmds, ref List<Nullable<double>> refSumsToReturn, bool LockDBDuringExchange, bool TryToConvertMSAccesCmdToSQLiteCmd)
            {
                // Use common method and pass defaults for unspecified values
                return ExecuteMultipleSQLSumCmds(FullDatabasePath, DatabasePassword, SQLSumCmds, ref refSumsToReturn, LockDBDuringExchange, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB, TryToConvertMSAccesCmdToSQLiteCmd);
            }
            /// <summary>
            /// Used to execute multiple SQL SUM commands and return the sums as a list in one database open / close session (faster than doing individual cmds).
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLSumCmds">The list of SQL SUM commands to execute.</param>
            /// <param name="refSumsToReturn">BYREF:  The list that will return the sums for each command.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <param name="OverrideReadWriteLockIfLockedMoreThanXSeconds">The amount of time (in seconds) to wait before a database READ/WRITE lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <param name="OverrideCompactLockIfLockedMoreThanXSecsPerMB">The amount of time (in seconds per megabyte) to wait before a COMPACTING database lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <param name="TryToConvertMSAccesCmdToSQLiteCmd">Boolean to indicate if the function should attempt to convert the SQL command from MSAccess format to SQLite format by replacing "#DATES#" with SQLite's "DATE(" / "DATETIME(" format.</param>
            /// <returns>Returns TRUE if ALL commands execute. Returns FALSE in all other conditions.</returns>
            public static bool ExecuteMultipleSQLSumCmds(string FullDatabasePath, string DatabasePassword, IEnumerable<string> SQLSumCmds, ref List<Nullable<double>> refSumsToReturn, bool LockDBDuringExchange, int OverrideReadWriteLockIfLockedMoreThanXSeconds, int OverrideCompactLockIfLockedMoreThanXSecsPerMB, bool TryToConvertMSAccesCmdToSQLiteCmd)
            {
                // Init ref vars
                refSumsToReturn = new List<Nullable<double>>();

                // Save the cmd we're looking for
                const string RequiredSelectCmd = "SELECT SUM";

                // Verify commands are "SUM" scalar cmds
                foreach (string CMD in SQLSumCmds)
                {
                    // Return false if any cmd is missing the correct SELECT syntax
                    if (CMD.ToUpper().Contains(RequiredSelectCmd) == false)
                    {
                        return false;
                    }
                }

                // Create object list to use low level function
                List<object> objList = null;

                // Use shared routine to try and get the sums
                bool Result = ExecuteMultipleSQLScalarCmds(FullDatabasePath, DatabasePassword, SQLSumCmds, ref objList, LockDBDuringExchange, OverrideReadWriteLockIfLockedMoreThanXSeconds, OverrideCompactLockIfLockedMoreThanXSecsPerMB, TryToConvertMSAccesCmdToSQLiteCmd);

                // If sql failed, just return false
                if (Result == false) return false;

                // If sql worked, loop through and convert objects to nums
                foreach (object OBJ in objList)
                {
                    // Check if result came back
                    if (OBJ != null && Database.IsNumeric(OBJ) == true)
                    {
                        // If good, convert to num and add to reference list
                        refSumsToReturn.Add(Convert.ToDouble(OBJ));
                    }
                    else
                    {
                        // If cmd failed or value is invalid, use nothing
                        refSumsToReturn.Add(null);
                    }
                }

                // Return true once all values are converted
                return true;
            }

            // OVERLOADED METHODS FOR PERFORMING MULTIPLE SQL SCALAR CMDS (DURING ONE OPEN FOR IMPROVED SPEED) AND RETURNING THE VALUES
            /// <summary>
            /// Used to execute multiple SQL scalar commands and return the results as a list in one database open / close session (faster than doing individual cmds).
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLScalarCmds">The list of SQL scalar commands to execute.</param>
            /// <param name="refResultsToReturn">BYREF:  The list to return with all the returned values.</param>
            /// <returns>Returns TRUE if ALL commands execute. Returns FALSE in all other conditions.</returns>
            public static bool ExecuteMultipleSQLScalarCmds(string FullDatabasePath, string DatabasePassword, IEnumerable<string> SQLScalarCmds, ref List<object> refResultsToReturn)
            {
                // Use common method and pass defaults for unspecified values
                return ExecuteMultipleSQLScalarCmds(FullDatabasePath, DatabasePassword, SQLScalarCmds, ref refResultsToReturn, false, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB, false);
            }
            /// <summary>
            /// Used to execute multiple SQL scalar commands and return the results as a list in one database open / close session (faster than doing individual cmds).
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLScalarCmds">The list of SQL scalar commands to execute.</param>
            /// <param name="refResultsToReturn">BYREF:  The list to return with all the returned values.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <returns>Returns TRUE if ALL commands execute. Returns FALSE in all other conditions.</returns>
            public static bool ExecuteMultipleSQLScalarCmds(string FullDatabasePath, string DatabasePassword, IEnumerable<string> SQLScalarCmds, ref List<object> refResultsToReturn, bool LockDBDuringExchange)
            {
                // Use common method and pass defaults for unspecified values
                return ExecuteMultipleSQLScalarCmds(FullDatabasePath, DatabasePassword, SQLScalarCmds, ref refResultsToReturn, LockDBDuringExchange, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB, false);
            }
            /// <summary>
            /// Used to execute multiple SQL scalar commands and return the results as a list in one database open / close session (faster than doing individual cmds).
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLScalarCmds">The list of SQL scalar commands to execute.</param>
            /// <param name="refResultsToReturn">BYREF:  The list to return with all the returned values.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <param name="TryToConvertMSAccesCmdToSQLiteCmd">Boolean to indicate if the function should attempt to convert the SQL command from MSAccess format to SQLite format by replacing "#DATES#" with SQLite's "DATE(" / "DATETIME(" format.</param>
            /// <returns>Returns TRUE if ALL commands execute. Returns FALSE in all other conditions.</returns>
            public static bool ExecuteMultipleSQLScalarCmds(string FullDatabasePath, string DatabasePassword, IEnumerable<string> SQLScalarCmds, ref List<object> refResultsToReturn, bool LockDBDuringExchange, bool TryToConvertMSAccesCmdToSQLiteCmd)
            {
                // Use common method and pass defaults for unspecified values
                return ExecuteMultipleSQLScalarCmds(FullDatabasePath, DatabasePassword, SQLScalarCmds, ref refResultsToReturn, LockDBDuringExchange, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB, TryToConvertMSAccesCmdToSQLiteCmd);
            }
            /// <summary>
            /// Used to execute multiple SQL scalar commands and return the results as a list in one database open / close session (faster than doing individual cmds).
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLScalarCmds">The list of SQL scalar commands to execute.</param>
            /// <param name="refResultsToReturn">BYREF:  The list to return with all the returned values.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <param name="OverrideReadWriteLockIfLockedMoreThanXSeconds">The amount of time (in seconds) to wait before a database READ/WRITE lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <param name="OverrideCompactLockIfLockedMoreThanXSecsPerMB">The amount of time (in seconds per megabyte) to wait before a COMPACTING database lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <param name="TryToConvertMSAccesCmdToSQLiteCmd">Boolean to indicate if the function should attempt to convert the SQL command from MSAccess format to SQLite format by replacing "#DATES#" with SQLite's "DATE(" / "DATETIME(" format.</param>
            /// <returns>Returns TRUE if ALL commands execute. Returns FALSE in all other conditions.</returns>
            public static bool ExecuteMultipleSQLScalarCmds(string FullDatabasePath, string DatabasePassword, IEnumerable<string> SQLScalarCmds, ref List<object> refResultsToReturn, bool LockDBDuringExchange, int OverrideReadWriteLockIfLockedMoreThanXSeconds, int OverrideCompactLockIfLockedMoreThanXSecsPerMB, bool TryToConvertMSAccesCmdToSQLiteCmd)
            {
                // Init ref vars
                refResultsToReturn = new List<object>();

                // Create var to hold if all cmds execute
                bool AllCmdsWorked = false;

                // Check for database lock before continuing and execute lock if requested
                if (MainLockingCheckAndSetRoutine(FullDatabasePath, LockType.ReadWrite, OverrideReadWriteLockIfLockedMoreThanXSeconds, OverrideCompactLockIfLockedMoreThanXSecsPerMB, LockDBDuringExchange) == false)
                {
                    // If the db locked from a previous op OR cannot be locked, the function prompts, just return false
                    return false;
                }

                // Convert input commands into list so it's easier to work with
                var SQLScalarCmdList = SQLScalarCmds.ToList();

                // Check if converting MS Access style dates
                if (TryToConvertMSAccesCmdToSQLiteCmd == true)
                {
                    // If so, do conversion
                    for (int i = 0; i <= SQLScalarCmdList.Count - 1; i++)
                    {
                        SQLScalarCmdList[i] = ConvertMSAccessCmdToSQLiteCmd(SQLScalarCmdList[i]);
                    }
                }

                // Create SQLite database connection
                string ConnStr = CreateConnectionString(FullDatabasePath, DatabasePassword);
                var DBConn = new System.Data.SQLite.SQLiteConnection(ConnStr);

                try
                {
                    // Open database connection
                    DBConn.Open();

                    // Loop through and execute each cmd while the database is open
                    foreach (string CMD in SQLScalarCmdList)
                    {
                        // Create the cmd
                        using (var FetchCommand = new System.Data.SQLite.SQLiteCommand())
                        {
                            // Build the cmd
                            FetchCommand.Connection = DBConn;
                            FetchCommand.CommandType = System.Data.CommandType.Text;
                            FetchCommand.CommandText = CMD;

                            // Create an adapter
                            using (var DAdapt = new System.Data.SQLite.SQLiteDataAdapter())
                            {
                                // Create a dataset to hold info
                                using (var DSet = new System.Data.DataSet())
                                {
                                    string SearchTable = "SearchTable";
                                    DAdapt.SelectCommand = FetchCommand;
                                    DAdapt.Fill(DSet, SearchTable);

                                    // Return the results of the count cmd
                                    object DBReturnedValue = FetchCommand.ExecuteScalar();
                                    refResultsToReturn.Add(DBReturnedValue);
                                }
                            }
                        }
                    }

                    // Close the database connection
                    DBConn.Close();

                    // Save true if everything works
                    AllCmdsWorked = true;
                }
                catch (Exception ex)
                {
                    // Close the connection
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    DBConn.Close();
                }
                finally
                {
                    // Release db connection resources
                    DBConn.Dispose();
                }

                // If database was locked, unlock it
                if (LockDBDuringExchange == true)
                {
                    LockOrUnlockSQLiteDatabase(FullDatabasePath, LockType.ReadWrite, LockState.Unlock);
                }

                // Return the result of the sql cmd execution
                return AllCmdsWorked;
            }

            // OVERLOADED METHODS FOR ADDING NEW COLUMN NAMES TO DATABASE
            /// <summary>
            /// Used to add a new column to a specified database table.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="TableToAlter">The name of the table to add the column to.</param>
            /// <param name="NewColumnName">The name of the new column.</param>
            /// <param name="NewColumnDataType">The data type of the new column.</param>
            /// <returns>Returns TRUE if the update works. Returns FALSE in all other conditions.</returns>
            public static bool AddColumnToTable(string FullDatabasePath, string DatabasePassword, string TableToAlter, string NewColumnName, ColumnDataTypeOptions NewColumnDataType)
            {
                // Use common method and pass defaults for unspecified values
                return AddColumnToTable(FullDatabasePath, DatabasePassword, TableToAlter, NewColumnName, NewColumnDataType, false, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB);
            }
            /// <summary>
            /// Used to add a new column to a specified database table.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="TableToAlter">The name of the table to add the column to.</param>
            /// <param name="NewColumnName">The name of the new column.</param>
            /// <param name="NewColumnDataType">The data type of the new column.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <returns>Returns TRUE if the update works. Returns FALSE in all other conditions.</returns>
            public static bool AddColumnToTable(string FullDatabasePath, string DatabasePassword, string TableToAlter, string NewColumnName, ColumnDataTypeOptions NewColumnDataType, bool LockDBDuringExchange)
            {
                // Use common method and pass defaults for unspecified values
                return AddColumnToTable(FullDatabasePath, DatabasePassword, TableToAlter, NewColumnName, NewColumnDataType, LockDBDuringExchange, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB);
            }
            /// <summary>
            /// Used to add a new column to a specified database table.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="TableToAlter">The name of the table to add the column to.</param>
            /// <param name="NewColumnName">The name of the new column.</param>
            /// <param name="NewColumnDataType">The data type of the new column.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <param name="OverrideReadWriteLockIfLockedMoreThanXSeconds">The amount of time (in seconds) to wait before a database READ/WRITE lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <param name="OverrideCompactLockIfLockedMoreThanXSecsPerMB">The amount of time (in seconds per megabyte) to wait before a COMPACTING database lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <returns>Returns TRUE if the update works. Returns FALSE in all other conditions.</returns>
            public static bool AddColumnToTable(string FullDatabasePath, string DatabasePassword, string TableToAlter, string NewColumnName, ColumnDataTypeOptions NewColumnDataType, bool LockDBDuringExchange, int OverrideReadWriteLockIfLockedMoreThanXSeconds, int OverrideCompactLockIfLockedMoreThanXSecsPerMB)
            {
                // Get string name of column data type
                string ColumnDataTypeStringRepresentation = "TEXT";
                switch (NewColumnDataType)
                {
                    case ColumnDataTypeOptions.NumericInt:
                        ColumnDataTypeStringRepresentation = "INTEGER";
                        break;
                    case ColumnDataTypeOptions.NumericDbl:
                        ColumnDataTypeStringRepresentation = "REAL";
                        break;
                    case ColumnDataTypeOptions.ShortDate:
                        ColumnDataTypeStringRepresentation = "DATE";
                        break;
                    case ColumnDataTypeOptions.LongDate:
                        ColumnDataTypeStringRepresentation = "TIMESTAMP";
                        break;
                }

                // Create the cmd
                string CmdToExecute = "ALTER TABLE " + TableToAlter + " ADD COLUMN " + NewColumnName + " " + ColumnDataTypeStringRepresentation;

                // Try and execute the cmd
                return ExecuteSQLUpdateCmd(FullDatabasePath, DatabasePassword, CmdToExecute, LockDBDuringExchange, OverrideReadWriteLockIfLockedMoreThanXSeconds, OverrideCompactLockIfLockedMoreThanXSecsPerMB, false);
            }

            // OVERLOADED METHODS FOR RETURNING THE NUMBER OF RECORDS THAT MATCH A GIVEN SQL SELECT STATEMENT
            /// <summary>
            /// Used to return the number of records fetched by a SQL SELECT statement (somewhat redundant to SELECT COUNT).
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLSelectCmd">The SQL SELECT command to execute.</param>
            /// <returns>Returns the matching record count IF the command executes correctly. Returns -1 in all other conditions.</returns>
            public static int GetMatchingRecordCount(string FullDatabasePath, string DatabasePassword, string SQLSelectCmd)
            {
                // Use common method and pass defaults for unspecified values
                return GetMatchingRecordCount(FullDatabasePath, DatabasePassword, SQLSelectCmd, false, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB, false);
            }
            /// <summary>
            /// Used to return the number of records fetched by a SQL SELECT statement (somewhat redundant to SELECT COUNT).
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLSelectCmd">The SQL SELECT command to execute.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <returns>Returns the matching record count IF the command executes correctly. Returns -1 in all other conditions.</returns>
            public static int GetMatchingRecordCount(string FullDatabasePath, string DatabasePassword, string SQLSelectCmd, bool LockDBDuringExchange)
            {
                // Use common method and pass defaults for unspecified values
                return GetMatchingRecordCount(FullDatabasePath, DatabasePassword, SQLSelectCmd, LockDBDuringExchange, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB, false);
            }
            /// <summary>
            /// Used to return the number of records fetched by a SQL SELECT statement (somewhat redundant to SELECT COUNT).
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLSelectCmd">The SQL SELECT command to execute.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <param name="TryToConvertMSAccesCmdToSQLiteCmd">Boolean to indicate if the function should attempt to convert the SQL command from MSAccess format to SQLite format by replacing "#DATES#" with SQLite's "DATE(" / "DATETIME(" format.</param>
            /// <returns>Returns the matching record count IF the command executes correctly. Returns -1 in all other conditions.</returns>
            public static int GetMatchingRecordCount(string FullDatabasePath, string DatabasePassword, string SQLSelectCmd, bool LockDBDuringExchange, bool TryToConvertMSAccesCmdToSQLiteCmd)
            {
                // Use common method and pass defaults for unspecified values
                return GetMatchingRecordCount(FullDatabasePath, DatabasePassword, SQLSelectCmd, LockDBDuringExchange, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB, TryToConvertMSAccesCmdToSQLiteCmd);
            }
            /// <summary>
            /// Used to return the number of records fetched by a SQL SELECT statement (somewhat redundant to SELECT COUNT).
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLSelectCmd">The SQL SELECT command to execute.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <param name="OverrideReadWriteLockIfLockedMoreThanXSeconds">The amount of time (in seconds) to wait before a database READ/WRITE lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <param name="OverrideCompactLockIfLockedMoreThanXSecsPerMB">The amount of time (in seconds per megabyte) to wait before a COMPACTING database lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <param name="TryToConvertMSAccesCmdToSQLiteCmd">Boolean to indicate if the function should attempt to convert the SQL command from MSAccess format to SQLite format by replacing "#DATES#" with SQLite's "DATE(" / "DATETIME(" format.</param>
            /// <returns>Returns the matching record count IF the command executes correctly. Returns -1 in all other conditions.</returns>
            public static int GetMatchingRecordCount(string FullDatabasePath, string DatabasePassword, string SQLSelectCmd, bool LockDBDuringExchange, int OverrideReadWriteLockIfLockedMoreThanXSeconds, int OverrideCompactLockIfLockedMoreThanXSecsPerMB, bool TryToConvertMSAccesCmdToSQLiteCmd)
            {
                // Create var to hold result to return
                int MatchingRecordCount = -1;

                // Check for database lock before continuing and execute lock if requested
                if (MainLockingCheckAndSetRoutine(FullDatabasePath, LockType.ReadWrite, OverrideReadWriteLockIfLockedMoreThanXSeconds, OverrideCompactLockIfLockedMoreThanXSecsPerMB, LockDBDuringExchange) == false)
                {
                    // If the db locked from a previous op OR cannot be locked, the function prompts, just exit
                    return MatchingRecordCount;
                }

                // Create SQLite database connection
                string ConnStr = CreateConnectionString(FullDatabasePath, DatabasePassword);
                var DBConn = new System.Data.SQLite.SQLiteConnection(ConnStr);

                // If user requested conversion, try to convert Access SQL cmd to SQLite version 
                if (TryToConvertMSAccesCmdToSQLiteCmd == true) SQLSelectCmd = ConvertMSAccessCmdToSQLiteCmd(SQLSelectCmd);

                try
                {
                    // Open database connection
                    DBConn.Open();

                    // Create the cmd
                    using (var SelectCommand = new System.Data.SQLite.SQLiteCommand())
                    {
                        // Build the command
                        SelectCommand.Connection = DBConn;
                        SelectCommand.CommandType = System.Data.CommandType.Text;
                        SelectCommand.CommandText = SQLSelectCmd;

                        // Create adapter to fill get data
                        using (var DAdapt = new System.Data.SQLite.SQLiteDataAdapter())
                        {
                            // Create dataset to hold data
                            using (var DSet = new System.Data.DataSet())
                            {
                                // Fill dataset using adapter
                                string SearchTable = "SearchTable";
                                DAdapt.SelectCommand = SelectCommand;
                                DAdapt.Fill(DSet, SearchTable);

                                // Count the number of rows
                                MatchingRecordCount = DSet.Tables[SearchTable].Rows.Count;
                            }
                        }
                    }

                    // Close the db connection
                    DBConn.Close();
                }
                catch (Exception ex)
                {
                    // Close the connection
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    DBConn.Close();
                }
                finally
                {
                    // Release db connection resources
                    DBConn.Dispose();
                }

                // If database was locked, unlock it
                if (LockDBDuringExchange == true)
                {
                    LockOrUnlockSQLiteDatabase(FullDatabasePath, LockType.ReadWrite, LockState.Unlock);
                }

                // Return the matching record count
                return MatchingRecordCount;
            }

            // OVERLOADED METHODS FOR RETURNING A DATASET OF RESULTS FOR A GIVEN SQL SELECT CMD
            /// <summary>
            /// Used to return matching criteria from SELECT cmd to a passed in dataset.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLSelectCmd">The SQL SELECT command to execute.</param>
            /// <param name="refDataSetToReturn">BYREF:  The dataset to pass back with the search results.</param>
            /// <returns>Returns TRUE if the function executes successfully. Returns FALSE in all other conditions.</returns>
            public static bool GetSearchResultsAsDataSet(string FullDatabasePath, string DatabasePassword, string SQLSelectCmd, ref System.Data.DataSet refDataSetToReturn)
            {
                // Use common method and pass defaults for unspecified values
                return GetSearchResultsAsDataSet(FullDatabasePath, DatabasePassword, SQLSelectCmd, ref refDataSetToReturn, false, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB, false);
            }
            /// <summary>
            /// Used to return matching criteria from SELECT cmd to a passed in dataset.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLSelectCmd">The SQL SELECT command to execute.</param>
            /// <param name="refDataSetToReturn">BYREF:  The dataset to pass back with the search results.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <returns>Returns TRUE if data is </returns>
            public static bool GetSearchResultsAsDataSet(string FullDatabasePath, string DatabasePassword, string SQLSelectCmd, ref System.Data.DataSet refDataSetToReturn, bool LockDBDuringExchange)
            {
                // Use common method and pass defaults for unspecified values
                return GetSearchResultsAsDataSet(FullDatabasePath, DatabasePassword, SQLSelectCmd, ref refDataSetToReturn, LockDBDuringExchange, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB, false);
            }
            /// <summary>
            /// Used to return matching criteria from SELECT cmd to a passed in dataset.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLSelectCmd">The SQL SELECT command to execute.</param>
            /// <param name="refDataSetToReturn">BYREF:  The dataset to pass back with the search results.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <param name="TryToConvertMSAccesCmdToSQLiteCmd">Boolean to indicate if the function should attempt to convert the SQL command from MSAccess format to SQLite format by replacing "#DATES#" with SQLite's "DATE(" / "DATETIME(" format.</param>
            /// <returns>Returns TRUE if the function executes successfully. Returns FALSE in all other conditions.</returns>
            public static bool GetSearchResultsAsDataSet(string FullDatabasePath, string DatabasePassword, string SQLSelectCmd, ref System.Data.DataSet refDataSetToReturn, bool LockDBDuringExchange, bool TryToConvertMSAccesCmdToSQLiteCmd)
            {
                // Use common method and pass defaults for unspecified values
                return GetSearchResultsAsDataSet(FullDatabasePath, DatabasePassword, SQLSelectCmd, ref refDataSetToReturn, LockDBDuringExchange, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB, TryToConvertMSAccesCmdToSQLiteCmd);
            }
            /// <summary>
            /// Used to return matching criteria from SELECT cmd to a passed in dataset.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLSelectCmd">The SQL SELECT command to execute.</param>
            /// <param name="refDataSetToReturn">BYREF:  The dataset to pass back with the search results.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <param name="OverrideReadWriteLockIfLockedMoreThanXSeconds">The amount of time (in seconds) to wait before a database READ/WRITE lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <param name="OverrideCompactLockIfLockedMoreThanXSecsPerMB">The amount of time (in seconds per megabyte) to wait before a COMPACTING database lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <param name="TryToConvertMSAccesCmdToSQLiteCmd">Boolean to indicate if the function should attempt to convert the SQL command from MSAccess format to SQLite format by replacing "#DATES#" with SQLite's "DATE(" / "DATETIME(" format.</param>
            /// <returns>Returns TRUE if the function executes successfully. Returns FALSE in all other conditions.</returns>
            public static bool GetSearchResultsAsDataSet(string FullDatabasePath, string DatabasePassword, string SQLSelectCmd, ref System.Data.DataSet refDataSetToReturn, bool LockDBDuringExchange, int OverrideReadWriteLockIfLockedMoreThanXSeconds, int OverrideCompactLockIfLockedMoreThanXSecsPerMB, bool TryToConvertMSAccesCmdToSQLiteCmd)
            {
                // Init ref vars
                refDataSetToReturn = new System.Data.DataSet();

                // Create var to hold the result
                bool SearchWorked = false;

                // Check for database lock before continuing and execute lock if requested
                if (MainLockingCheckAndSetRoutine(FullDatabasePath, LockType.ReadWrite, OverrideReadWriteLockIfLockedMoreThanXSeconds, OverrideCompactLockIfLockedMoreThanXSecsPerMB, LockDBDuringExchange) == false)
                {
                    // If the db locked from a previous op OR cannot be locked, the function prompts, just exit
                    return false;
                }

                // Create SQLite database connection
                string ConnStr = CreateConnectionString(FullDatabasePath, DatabasePassword);
                var DBConn = new System.Data.SQLite.SQLiteConnection(ConnStr);

                // If user requested conversion, try to convert Access SQL cmd to SQLite version 
                if (TryToConvertMSAccesCmdToSQLiteCmd == true) SQLSelectCmd = ConvertMSAccessCmdToSQLiteCmd(SQLSelectCmd);

                try
                {
                    // Open database connection
                    DBConn.Open();

                    // Create the command
                    using (var SelectCommand = new System.Data.SQLite.SQLiteCommand())
                    {
                        // Build the command
                        SelectCommand.Connection = DBConn;
                        SelectCommand.CommandType = System.Data.CommandType.Text;
                        SelectCommand.CommandText = SQLSelectCmd;

                        // Populate dataset with search results
                        using (var DAdapt = new System.Data.SQLite.SQLiteDataAdapter())
                        {
                            // Populate the dataset
                            string SearchTable = "SearchTable";
                            DAdapt.SelectCommand = SelectCommand;
                            DAdapt.Fill(refDataSetToReturn, SearchTable);
                        }
                    }

                    // Close the database connection
                    DBConn.Close();

                    // Save true if everything works
                    SearchWorked = true;
                }
                catch (Exception ex)
                {
                    // Erase the dataset
                    refDataSetToReturn = null;

                    // Close the connection
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    DBConn.Close();
                }
                finally
                {
                    // Release db connection resources
                    DBConn.Dispose();
                }

                // If database was locked, unlock it
                if (LockDBDuringExchange == true)
                {
                    LockOrUnlockSQLiteDatabase(FullDatabasePath, LockType.ReadWrite, LockState.Unlock);
                }

                // Return the result
                return SearchWorked;
            }

            // OVERLOADED METHODS FOR RETURNING A 2D ARRAY OF RESULTS FOR A GIVEN SQL SELECT CMD
            /// <summary>
            /// Used to return matching criteria from SELECT cmd to a passed in 2D array.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLSelectCmd">The SQL SELECT command to execute.</param>
            /// <param name="refObjectArrayToSaveTo">BYREF:  The 2D object array to pass back with the search results.</param>
            /// <returns>Returns TRUE if the command executes / the search results are returned. Returns FALSE in all other conditions.</returns>
            public static bool GetSearchResultsAsArray(string FullDatabasePath, string DatabasePassword, string SQLSelectCmd, ref object[,] refObjectArrayToSaveTo)
            {
                // Use common method and pass defaults for unspecified values
                return GetSearchResultsAsArray(FullDatabasePath, DatabasePassword, SQLSelectCmd, ref refObjectArrayToSaveTo, false, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB, false);
            }
            /// <summary>
            /// Used to return matching criteria from SELECT cmd to a passed in 2D array.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLSelectCmd">The SQL SELECT command to execute.</param>
            /// <param name="refObjectArrayToSaveTo">BYREF:  The 2D object array to pass back with the search results.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <returns>Returns TRUE if the command executes / the search results are returned. Returns FALSE in all other conditions.</returns>
            public static bool GetSearchResultsAsArray(string FullDatabasePath, string DatabasePassword, string SQLSelectCmd, ref object[,] refObjectArrayToSaveTo, bool LockDBDuringExchange)
            {
                // Use common method and pass defaults for unspecified values
                return GetSearchResultsAsArray(FullDatabasePath, DatabasePassword, SQLSelectCmd, ref refObjectArrayToSaveTo, LockDBDuringExchange, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB, false);
            }
            /// <summary>
            /// Used to return matching criteria from SELECT cmd to a passed in 2D array.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLSelectCmd">The SQL SELECT command to execute.</param>
            /// <param name="refObjectArrayToSaveTo">BYREF:  The 2D object array to pass back with the search results.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <param name="TryToConvertMSAccesCmdToSQLiteCmd">Boolean to indicate if the function should attempt to convert the SQL command from MSAccess format to SQLite format by replacing "#DATES#" with SQLite's "DATE(" / "DATETIME(" format.</param>
            /// <returns>Returns TRUE if the command executes / the search results are returned. Returns FALSE in all other conditions.</returns>
            public static bool GetSearchResultsAsArray(string FullDatabasePath, string DatabasePassword, string SQLSelectCmd, ref object[,] refObjectArrayToSaveTo, bool LockDBDuringExchange, bool TryToConvertMSAccesCmdToSQLiteCmd)
            {
                // Use common method and pass defaults for unspecified values
                return GetSearchResultsAsArray(FullDatabasePath, DatabasePassword, SQLSelectCmd, ref refObjectArrayToSaveTo, LockDBDuringExchange, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB, TryToConvertMSAccesCmdToSQLiteCmd);
            }
            /// <summary>
            /// Used to return matching criteria from SELECT cmd to a passed in 2D array.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLSelectCmd">The SQL SELECT command to execute.</param>
            /// <param name="refObjectArrayToSaveTo">BYREF:  The 2D object array to pass back with the search results.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <param name="OverrideReadWriteLockIfLockedMoreThanXSeconds">The amount of time (in seconds) to wait before a database READ/WRITE lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <param name="OverrideCompactLockIfLockedMoreThanXSecsPerMB">The amount of time (in seconds per megabyte) to wait before a COMPACTING database lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <param name="TryToConvertMSAccesCmdToSQLiteCmd">Boolean to indicate if the function should attempt to convert the SQL command from MSAccess format to SQLite format by replacing "#DATES#" with SQLite's "DATE(" / "DATETIME(" format.</param>
            /// <returns>Returns TRUE if the command executes / the search results are returned. Returns FALSE in all other conditions.</returns>
            public static bool GetSearchResultsAsArray(string FullDatabasePath, string DatabasePassword, string SQLSelectCmd, ref object[,] refObjectArrayToSaveTo, bool LockDBDuringExchange, int OverrideReadWriteLockIfLockedMoreThanXSeconds, int OverrideCompactLockIfLockedMoreThanXSecsPerMB, bool TryToConvertMSAccesCmdToSQLiteCmd)
            {
                // Init ref vars
                refObjectArrayToSaveTo = new object[1, 1];

                // Create var to hold the result
                bool SearchWorked = false;

                // Check for database lock before continuing and execute lock if requested
                if (MainLockingCheckAndSetRoutine(FullDatabasePath, LockType.ReadWrite, OverrideReadWriteLockIfLockedMoreThanXSeconds, OverrideCompactLockIfLockedMoreThanXSecsPerMB, LockDBDuringExchange) == false)
                {
                    // If the db locked from a previous op OR cannot be locked, the function prompts, just exit
                    return false;
                }

                // Create SQLite database connection
                string ConnStr = CreateConnectionString(FullDatabasePath, DatabasePassword);
                var DBConn = new System.Data.SQLite.SQLiteConnection(ConnStr);

                // If user requested conversion, try to convert Access SQL cmd to SQLite version 
                if (TryToConvertMSAccesCmdToSQLiteCmd == true) SQLSelectCmd = ConvertMSAccessCmdToSQLiteCmd(SQLSelectCmd);

                try
                {
                    // Open database connection
                    DBConn.Open();

                    // Build the command
                    using (var SelectCommand = new System.Data.SQLite.SQLiteCommand())
                    {
                        // Configure the cmd
                        SelectCommand.Connection = DBConn;
                        SelectCommand.CommandType = System.Data.CommandType.Text;
                        SelectCommand.CommandText = SQLSelectCmd;

                        // Create adapter to get data
                        using (var DAdapt = new System.Data.SQLite.SQLiteDataAdapter())
                        {
                            // Create dataset to hold data
                            using (var DSet = new System.Data.DataSet())
                            {
                                // Fill dataset using adapter
                                string SearchTable = "SearchTable";
                                DAdapt.SelectCommand = SelectCommand;
                                DAdapt.Fill(DSet, SearchTable);

                                // Get the row & column counts of the dataset
                                int RowCount = DSet.Tables[SearchTable].Rows.Count;
                                int ColCount = DSet.Tables[SearchTable].Columns.Count;

                                // Check that there is a row
                                if (RowCount > 0)
                                {
                                    // Size the array for the number of returned items
                                    refObjectArrayToSaveTo = new object[RowCount - 1 + 1, ColCount - 1 + 1];

                                    // Loop through and add value to array
                                    for (int i = 0; i <= RowCount - 1; i++)
                                    {
                                        for (int j = 0; j <= ColCount - 1; j++)
                                        {
                                            refObjectArrayToSaveTo[i, j] = DSet.Tables[SearchTable].Rows[i][j];
                                        }
                                    }
                                }
                                else
                                {
                                    // Erase the array if there is no data
                                    refObjectArrayToSaveTo = null;
                                }
                            }
                        }
                    }

                    // Close the database connection
                    DBConn.Close();

                    // Save true if everything works
                    SearchWorked = true;
                }
                catch (Exception ex)
                {
                    // Erase the array
                    refObjectArrayToSaveTo = null;

                    // Close the connection
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    DBConn.Close();
                }
                finally
                {
                    // Release db connection resources
                    DBConn.Dispose();
                }

                // If database was locked, unlock it
                if (LockDBDuringExchange == true)
                {
                    LockOrUnlockSQLiteDatabase(FullDatabasePath, LockType.ReadWrite, LockState.Unlock);
                }

                // Return the result
                return SearchWorked;
            }

            // OVERLOADED METHODS FOR RETURING A LIST OF RESULTS FOR A GIVEN SQL SELECT CMD
            /// <summary>
            /// Used to return an object list of search results from a SQL SELECT command.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLSelectCmd">The SQL SELECT command to execute.</param>
            /// <param name="refListToSaveTo">BYREF:  The list of objects to return.</param>
            /// <param name="ReturnUniqueValuesOnly">Boolean to flag whether all matching items should be returned OR just unique items.</param>
            /// <returns>Returns TRUE if the function executes successfully. Returns FALSE in all other conditions.</returns>
            public static bool GetSearchResultsAsList(string FullDatabasePath, string DatabasePassword, string SQLSelectCmd, ref List<object> refListToSaveTo, bool ReturnUniqueValuesOnly)
            {
                // Use common method and pass defaults for unspecified values
                return GetSearchResultsAsList(FullDatabasePath, DatabasePassword, SQLSelectCmd, ref refListToSaveTo, ReturnUniqueValuesOnly, false, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB, false);
            }
            /// <summary>
            /// Used to return an object list of search results from a SQL SELECT command.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLSelectCmd">The SQL SELECT command to execute.</param>
            /// <param name="refListToSaveTo">BYREF:  The list of objects to return.</param>
            /// <param name="ReturnUniqueValuesOnly">Boolean to flag whether all matching items should be returned OR just unique items.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <returns>Returns TRUE if the function executes successfully. Returns FALSE in all other conditions.</returns>
            public static bool GetSearchResultsAsList(string FullDatabasePath, string DatabasePassword, string SQLSelectCmd, ref List<object> refListToSaveTo, bool ReturnUniqueValuesOnly, bool LockDBDuringExchange)
            {
                // Use common method and pass defaults for unspecified values
                return GetSearchResultsAsList(FullDatabasePath, DatabasePassword, SQLSelectCmd, ref refListToSaveTo, ReturnUniqueValuesOnly, LockDBDuringExchange, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB, false);
            }
            /// <summary>
            /// Used to return an object list of search results from a SQL SELECT command.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLSelectCmd">The SQL SELECT command to execute.</param>
            /// <param name="refListToSaveTo">BYREF:  The list of objects to return.</param>
            /// <param name="ReturnUniqueValuesOnly">Boolean to flag whether all matching items should be returned OR just unique items.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <param name="TryToConvertMSAccesCmdToSQLiteCmd">Boolean to indicate if the function should attempt to convert the SQL command from MSAccess format to SQLite format by replacing "#DATES#" with SQLite's "DATE(" / "DATETIME(" format.</param>
            /// <returns>Returns TRUE if the function executes successfully. Returns FALSE in all other conditions.</returns>
            public static bool GetSearchResultsAsList(string FullDatabasePath, string DatabasePassword, string SQLSelectCmd, ref List<object> refListToSaveTo, bool ReturnUniqueValuesOnly, bool LockDBDuringExchange, bool TryToConvertMSAccesCmdToSQLiteCmd)
            {
                // Use common method and pass defaults for unspecified values
                return GetSearchResultsAsList(FullDatabasePath, DatabasePassword, SQLSelectCmd, ref refListToSaveTo, ReturnUniqueValuesOnly, LockDBDuringExchange, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB, TryToConvertMSAccesCmdToSQLiteCmd);
            }
            /// <summary>
            /// Used to return an object list of search results from a SQL SELECT command.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLSelectCmd">The SQL SELECT command to execute.</param>
            /// <param name="refListToSaveTo">BYREF:  The list of objects to return.</param>
            /// <param name="ReturnUniqueValuesOnly">Boolean to flag whether all matching items should be returned OR just unique items.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <param name="OverrideReadWriteLockIfLockedMoreThanXSeconds">The amount of time (in seconds) to wait before a database READ/WRITE lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <param name="OverrideCompactLockIfLockedMoreThanXSecsPerMB">The amount of time (in seconds per megabyte) to wait before a COMPACTING database lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <param name="TryToConvertMSAccesCmdToSQLiteCmd">Boolean to indicate if the function should attempt to convert the SQL command from MSAccess format to SQLite format by replacing "#DATES#" with SQLite's "DATE(" / "DATETIME(" format.</param>
            /// <returns>Returns TRUE if the function executes successfully. Returns FALSE in all other conditions.</returns>
            public static bool GetSearchResultsAsList(string FullDatabasePath, string DatabasePassword, string SQLSelectCmd, ref List<object> refListToSaveTo, bool ReturnUniqueValuesOnly, bool LockDBDuringExchange, int OverrideReadWriteLockIfLockedMoreThanXSeconds, int OverrideCompactLockIfLockedMoreThanXSecsPerMB, bool TryToConvertMSAccesCmdToSQLiteCmd)
            {
                // Init ref vars
                refListToSaveTo = new List<object>();

                // Create a collection to use the collection query method
                System.Collections.ObjectModel.Collection<object> CollectionOfResults = null;

                // Try to get results as collection
                if (GetSearchResultsAsCollection(FullDatabasePath, DatabasePassword, SQLSelectCmd, ref CollectionOfResults, ReturnUniqueValuesOnly, LockDBDuringExchange, OverrideReadWriteLockIfLockedMoreThanXSeconds, OverrideCompactLockIfLockedMoreThanXSecsPerMB, TryToConvertMSAccesCmdToSQLiteCmd) == false || CollectionOfResults == null)
                {
                    // If the query fails, return false
                    return false;
                }

                // If the query works, convert the collection to the list
                refListToSaveTo = CollectionOfResults.ToList();

                // Return true if everything worked
                return true;
            }

            // OVERLOADED METHODS FOR RETURNING A COLLECTION OF RESULTS FOR A GIVEN SQL SELECT CMD
            /// <summary>
            /// Used to return an object collection of search results from a SQL SELECT command.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLSelectCmd">The SQL SELECT command to execute.</param>
            /// <param name="refCollectionToSaveTo">BYREF:  The collection of objects to return.</param>
            /// <param name="ReturnUniqueValuesOnly">Boolean to flag whether all matching items should be returned OR just unique items.</param>
            /// <returns>Returns TRUE if the function executes successfully. Returns FALSE in all other conditions.</returns>
            public static bool GetSearchResultsAsCollection(string FullDatabasePath, string DatabasePassword, string SQLSelectCmd, ref System.Collections.ObjectModel.Collection<object> refCollectionToSaveTo, bool ReturnUniqueValuesOnly)
            {
                // Use common method and pass defaults for unspecified values
                return GetSearchResultsAsCollection(FullDatabasePath, DatabasePassword, SQLSelectCmd, ref refCollectionToSaveTo, ReturnUniqueValuesOnly, false, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB, false);
            }
            /// <summary>
            /// Used to return an object collection of search results from a SQL SELECT command.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLSelectCmd">The SQL SELECT command to execute.</param>
            /// <param name="refCollectionToSaveTo">BYREF:  The collection of objects to return.</param>
            /// <param name="ReturnUniqueValuesOnly">Boolean to flag whether all matching items should be returned OR just unique items.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <returns>Returns TRUE if the function executes successfully. Returns FALSE in all other conditions.</returns>
            public static bool GetSearchResultsAsCollection(string FullDatabasePath, string DatabasePassword, string SQLSelectCmd, ref System.Collections.ObjectModel.Collection<object> refCollectionToSaveTo, bool ReturnUniqueValuesOnly, bool LockDBDuringExchange)
            {
                // Use common method and pass defaults for unspecified values
                return GetSearchResultsAsCollection(FullDatabasePath, DatabasePassword, SQLSelectCmd, ref refCollectionToSaveTo, ReturnUniqueValuesOnly, LockDBDuringExchange, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB, false);
            }
            /// <summary>
            /// Used to return an object collection of search results from a SQL SELECT command.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLSelectCmd">The SQL SELECT command to execute.</param>
            /// <param name="refCollectionToSaveTo">BYREF:  The collection of objects to return.</param>
            /// <param name="ReturnUniqueValuesOnly">Boolean to flag whether all matching items should be returned OR just unique items.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <param name="TryToConvertMSAccesCmdToSQLiteCmd">Boolean to indicate if the function should attempt to convert the SQL command from MSAccess format to SQLite format by replacing "#DATES#" with SQLite's "DATE(" / "DATETIME(" format.</param>
            /// <returns>Returns TRUE if the function executes successfully. Returns FALSE in all other conditions.</returns>
            public static bool GetSearchResultsAsCollection(string FullDatabasePath, string DatabasePassword, string SQLSelectCmd, ref System.Collections.ObjectModel.Collection<object> refCollectionToSaveTo, bool ReturnUniqueValuesOnly, bool LockDBDuringExchange, bool TryToConvertMSAccesCmdToSQLiteCmd)
            {
                // Use common method and pass defaults for unspecified values
                return GetSearchResultsAsCollection(FullDatabasePath, DatabasePassword, SQLSelectCmd, ref refCollectionToSaveTo, ReturnUniqueValuesOnly, LockDBDuringExchange, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB, TryToConvertMSAccesCmdToSQLiteCmd);
            }
            /// <summary>
            /// Used to return an object collection of search results from a SQL SELECT command.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLSelectCmd">The SQL SELECT command to execute.</param>
            /// <param name="refCollectionToSaveTo">BYREF:  The collection of objects to return.</param>
            /// <param name="ReturnUniqueValuesOnly">Boolean to flag whether all matching items should be returned OR just unique items.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <param name="OverrideReadWriteLockIfLockedMoreThanXSeconds">The amount of time (in seconds) to wait before a database READ/WRITE lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <param name="OverrideCompactLockIfLockedMoreThanXSecsPerMB">The amount of time (in seconds per megabyte) to wait before a COMPACTING database lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <param name="TryToConvertMSAccesCmdToSQLiteCmd">Boolean to indicate if the function should attempt to convert the SQL command from MSAccess format to SQLite format by replacing "#DATES#" with SQLite's "DATE(" / "DATETIME(" format.</param>
            /// <returns>Returns TRUE if the function executes successfully. Returns FALSE in all other conditions.</returns>
            public static bool GetSearchResultsAsCollection(string FullDatabasePath, string DatabasePassword, string SQLSelectCmd, ref System.Collections.ObjectModel.Collection<object> refCollectionToSaveTo, bool ReturnUniqueValuesOnly, bool LockDBDuringExchange, int OverrideReadWriteLockIfLockedMoreThanXSeconds, int OverrideCompactLockIfLockedMoreThanXSecsPerMB, bool TryToConvertMSAccesCmdToSQLiteCmd)
            {
                // Init ref vars
                refCollectionToSaveTo = new System.Collections.ObjectModel.Collection<object>();

                // Create var to hold results
                object[,] SearchResults = null;

                // Try to get the search results as an array
                if (GetSearchResultsAsArray(FullDatabasePath, DatabasePassword, SQLSelectCmd, ref SearchResults, LockDBDuringExchange: LockDBDuringExchange, OverrideReadWriteLockIfLockedMoreThanXSeconds: OverrideReadWriteLockIfLockedMoreThanXSeconds, OverrideCompactLockIfLockedMoreThanXSecsPerMB: OverrideCompactLockIfLockedMoreThanXSecsPerMB, TryToConvertMSAccesCmdToSQLiteCmd: TryToConvertMSAccesCmdToSQLiteCmd) == false)
                {
                    // If search results can't be returned, return false
                    return false;
                }

                // If search results are returned, add them to the collection
                if (SearchResults != null)
                {
                    foreach (object ITEM in SearchResults)
                    {
                        // Ensure some data was found
                        if (SearchResults.Length > 1 || SearchResults[0, 0] != null)
                        {
                            // Add to collection if returning all values OR if returning unique values and this is a new / unique value
                            if (ReturnUniqueValuesOnly == false || (ReturnUniqueValuesOnly == true && refCollectionToSaveTo.Contains(ITEM) == false))
                            {
                                refCollectionToSaveTo.Add(ITEM);
                            }
                        }
                    }
                }

                // Return true if everything worked
                return true;
            }

            // OVERLOADED METHODS FOR RETURNING THE DATABASE COLUMN NAMES FOR A SPECIFIED TABLE
            /// <summary>
            /// Used to return all database column / field names in a given table.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="TableNameToCheck">The database table to read extract names from.</param>
            /// <param name="refListToSaveTo">BYREF:  The list to pass back that contains the database column names.</param>
            /// <returns>Returns TRUE if the function executes correctly. Returns FALSE in all other conditions.</returns>
            public static bool GetTableColumnNamesAsList(string FullDatabasePath, string DatabasePassword, string TableNameToCheck, ref List<string> refListToSaveTo)
            {
                // Use common method and pass defaults for unspecified values
                return GetTableColumnNamesAsList(FullDatabasePath, DatabasePassword, TableNameToCheck, ref refListToSaveTo, false, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB);
            }
            /// <summary>
            /// Used to return all database column / field names in a given table.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="TableNameToCheck">The database table to read extract names from.</param>
            /// <param name="refListToSaveTo">BYREF:  The list to pass back that contains the database column names.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <returns>Returns TRUE if the function executes correctly. Returns FALSE in all other conditions.</returns>
            public static bool GetTableColumnNamesAsList(string FullDatabasePath, string DatabasePassword, string TableNameToCheck, ref List<string> refListToSaveTo, bool LockDBDuringExchange)
            {
                // Use common method and pass defaults for unspecified values
                return GetTableColumnNamesAsList(FullDatabasePath, DatabasePassword, TableNameToCheck, ref refListToSaveTo, LockDBDuringExchange, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB);
            }
            /// <summary>
            /// Used to return all database column / field names in a given table.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="TableNameToCheck">The database table to read extract names from.</param>
            /// <param name="refListToSaveTo">BYREF:  The list to pass back that contains the database column names.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <param name="OverrideReadWriteLockIfLockedMoreThanXSeconds">The amount of time (in seconds) to wait before a database READ/WRITE lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <param name="OverrideCompactLockIfLockedMoreThanXSecsPerMB">The amount of time (in seconds per megabyte) to wait before a COMPACTING database lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <returns>Returns TRUE if the function executes correctly. Returns FALSE in all other conditions.</returns>
            public static bool GetTableColumnNamesAsList(string FullDatabasePath, string DatabasePassword, string TableNameToCheck, ref List<string> refListToSaveTo, bool LockDBDuringExchange, int OverrideReadWriteLockIfLockedMoreThanXSeconds, int OverrideCompactLockIfLockedMoreThanXSecsPerMB)
            {
                // Init ref vars
                refListToSaveTo = new List<string>();

                // Create a collection to use the collection query method
                System.Collections.ObjectModel.Collection<string> CollectionOfResults = null;

                // Try to get results as collection
                if (GetTableColumnNamesAsCollection(FullDatabasePath, DatabasePassword, TableNameToCheck, ref CollectionOfResults, LockDBDuringExchange, OverrideReadWriteLockIfLockedMoreThanXSeconds, OverrideCompactLockIfLockedMoreThanXSecsPerMB) == false || CollectionOfResults == null)
                {
                    // If the query fails, return false
                    return false;
                }

                // If the query works, convert the collection to the list
                refListToSaveTo = CollectionOfResults.ToList();

                // Return true if everything worked
                return true;
            }

            // OVERLOADED METHODS FOR RETURNING THE DATABASE COLUMN NAMES FOR A SPECIFIED TABLE
            /// <summary>
            /// Used to return all database column / field names in a given table.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="TableNameToCheck">The database table to read extract names from.</param>
            /// <param name="refCollectionToSaveTo">BYREF:  The collection to pass back that contains the database column names.</param>
            /// <returns>Returns TRUE if the function executes correctly. Returns FALSE in all other conditions.</returns>
            public static bool GetTableColumnNamesAsCollection(string FullDatabasePath, string DatabasePassword, string TableNameToCheck, ref System.Collections.ObjectModel.Collection<string> refCollectionToSaveTo)
            {
                // Use common method and pass defaults for unspecified values
                return GetTableColumnNamesAsCollection(FullDatabasePath, DatabasePassword, TableNameToCheck, ref refCollectionToSaveTo, false, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB);
            }
            /// <summary>
            /// Used to return all database column / field names in a given table.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="TableNameToCheck">The database table to read extract names from.</param>
            /// <param name="refCollectionToSaveTo">BYREF:  The collection to pass back that contains the database column names.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <returns>Returns TRUE if the function executes correctly. Returns FALSE in all other conditions.</returns>
            public static bool GetTableColumnNamesAsCollection(string FullDatabasePath, string DatabasePassword, string TableNameToCheck, ref System.Collections.ObjectModel.Collection<string> refCollectionToSaveTo, bool LockDBDuringExchange)
            {
                // Use common method and pass defaults for unspecified values
                return GetTableColumnNamesAsCollection(FullDatabasePath, DatabasePassword, TableNameToCheck, ref refCollectionToSaveTo, LockDBDuringExchange, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB);
            }
            /// <summary>
            /// Used to return all database column / field names in a given table.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="TableNameToCheck">The database table to read extract names from.</param>
            /// <param name="refCollectionToSaveTo">BYREF:  The collection to pass back that contains the database column names.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <param name="OverrideReadWriteLockIfLockedMoreThanXSeconds">The amount of time (in seconds) to wait before a database READ/WRITE lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <param name="OverrideCompactLockIfLockedMoreThanXSecsPerMB">The amount of time (in seconds per megabyte) to wait before a COMPACTING database lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <returns>Returns TRUE if the function executes correctly. Returns FALSE in all other conditions.</returns>
            public static bool GetTableColumnNamesAsCollection(string FullDatabasePath, string DatabasePassword, string TableNameToCheck, ref System.Collections.ObjectModel.Collection<string> refCollectionToSaveTo, bool LockDBDuringExchange, int OverrideReadWriteLockIfLockedMoreThanXSeconds, int OverrideCompactLockIfLockedMoreThanXSecsPerMB)
            {
                // Init ref vars
                refCollectionToSaveTo = new System.Collections.ObjectModel.Collection<string>();

                // Return false if table is missing
                if (string.IsNullOrEmpty(TableNameToCheck) == true) return false;

                // Create flag to hold if query works
                bool QueryWorked = false;

                // Check for database lock before continuing and execute lock if requested
                if (MainLockingCheckAndSetRoutine(FullDatabasePath, LockType.ReadWrite, OverrideReadWriteLockIfLockedMoreThanXSeconds, OverrideCompactLockIfLockedMoreThanXSecsPerMB, LockDBDuringExchange) == false)
                {
                    // If the db locked from a previous op OR cannot be locked, the function prompts, just exit
                    return QueryWorked;
                }

                // Create the SQL cmd using the table name (return 1 row, just get column names to speed up search)
                string SQLCmd = "SELECT * FROM " + TableNameToCheck.Trim() + " LIMIT 1";

                // Create SQLite database connection
                string ConnStr = CreateConnectionString(FullDatabasePath, DatabasePassword);
                var DBConn = new System.Data.SQLite.SQLiteConnection(ConnStr);

                try
                {
                    // Open database connection
                    DBConn.Open();

                    // Create the command
                    using (var TableSelectCommand = new System.Data.SQLite.SQLiteCommand())
                    {
                        // Configure the cmd
                        TableSelectCommand.Connection = DBConn;
                        TableSelectCommand.CommandType = System.Data.CommandType.Text;
                        TableSelectCommand.CommandText = SQLCmd;

                        // Create adapter to get data
                        using (var DAdapt = new System.Data.SQLite.SQLiteDataAdapter())
                        {
                            // Create dataset to hold data
                            using (var DSet = new System.Data.DataSet())
                            {
                                // Use adapter to fill dataset
                                string SearchTable = "SearchTable";
                                DAdapt.SelectCommand = TableSelectCommand;
                                DAdapt.Fill(DSet, SearchTable);

                                // Get column count (verify there is at least one)
                                int ColCount = DSet.Tables[SearchTable].Columns.Count;
                                if (ColCount > 0)
                                {
                                    for (int i = 0; i <= ColCount - 1; i++)
                                    {
                                        refCollectionToSaveTo.Add(DSet.Tables[SearchTable].Columns[i].ColumnName);
                                    }
                                }
                            }
                        }
                    }

                    // Close the connection
                    DBConn.Close();

                    // Save true if everything worked
                    QueryWorked = true;
                }
                catch (Exception ex)
                {
                    // Close the connection
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    DBConn.Close();
                }
                finally
                {
                    // Release db connection resources
                    DBConn.Dispose();
                }

                // If database was locked, unlock it
                if (LockDBDuringExchange == true)
                {
                    LockOrUnlockSQLiteDatabase(FullDatabasePath, LockType.ReadWrite, LockState.Unlock);
                }

                // Return the result
                return QueryWorked;
            }

            // OVERLOADED METHODS FOR POPULATING A WINFORMS DATAGRIDVIEW WITH THE RESULTS OF A GIVEN SQL SELECT CMD
            /// <summary>
            /// Used to populate a WinForms DataGridView with the results of a SQL SELECT command.
            /// </summary>
            /// <param name="DataGridToPopulate">The WinForms DataGridView to populate.</param>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLSelectCmd">The SQL SELECT command to execute.</param>
            /// <returns>Returns TRUE if the grid is populated. Returns FALSE in all other conditions.</returns>
            public static bool PopulateDataGrid(System.Windows.Forms.DataGridView DataGridToPopulate, string FullDatabasePath, string DatabasePassword, string SQLSelectCmd)
            {
                // Use common method and pass defaults for unspecified values
                return PopulateDataGrid(DataGridToPopulate, FullDatabasePath, DatabasePassword, SQLSelectCmd, false, false, false, null, false, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB, false);
            }
            /// <summary>
            /// Used to populate a WinForms DataGridView with the results of a SQL SELECT command.
            /// </summary>
            /// <param name="DataGridToPopulate">The WinForms DataGridView to populate.</param>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLSelectCmd">The SQL SELECT command to execute.</param>
            /// <param name="AllowUserToEditCells">Boolean to set whether cells can be edited or if they are read only.</param>
            /// <param name="AllowUserToAddRows">Boolean to set whether the user can add new rows after the grid has been populated.</param>
            /// <param name="AutoSizeColumns">Boolean to flag whether columns should be auto-sized to fit content.</param>
            /// <param name="ColumnsToHide">String array of column names to hide after the grid is populated.</param>
            /// <returns>Returns TRUE if the grid is populated. Returns FALSE in all other conditions.</returns>
            public static bool PopulateDataGrid(System.Windows.Forms.DataGridView DataGridToPopulate, string FullDatabasePath, string DatabasePassword, string SQLSelectCmd, bool AllowUserToEditCells, bool AllowUserToAddRows, bool AutoSizeColumns, string[] ColumnsToHide)
            {
                // Use common method and pass defaults for unspecified values
                return PopulateDataGrid(DataGridToPopulate, FullDatabasePath, DatabasePassword, SQLSelectCmd, AllowUserToEditCells, AllowUserToAddRows, AutoSizeColumns, ColumnsToHide, false, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB, false);
            }
            /// <summary>
            /// Used to populate a WinForms DataGridView with the results of a SQL SELECT command.
            /// </summary>
            /// <param name="DataGridToPopulate">The WinForms DataGridView to populate.</param>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLSelectCmd">The SQL SELECT command to execute.</param>
            /// <param name="AllowUserToEditCells">Boolean to set whether cells can be edited or if they are read only.</param>
            /// <param name="AllowUserToAddRows">Boolean to set whether the user can add new rows after the grid has been populated.</param>
            /// <param name="AutoSizeColumns">Boolean to flag whether columns should be auto-sized to fit content.</param>
            /// <param name="ColumnsToHide">String array of column names to hide after the grid is populated.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <returns>Returns TRUE if the grid is populated. Returns FALSE in all other conditions.</returns>
            public static bool PopulateDataGrid(System.Windows.Forms.DataGridView DataGridToPopulate, string FullDatabasePath, string DatabasePassword, string SQLSelectCmd, bool AllowUserToEditCells, bool AllowUserToAddRows, bool AutoSizeColumns, string[] ColumnsToHide, bool LockDBDuringExchange)
            {
                // Use common method and pass defaults for unspecified values
                return PopulateDataGrid(DataGridToPopulate, FullDatabasePath, DatabasePassword, SQLSelectCmd, AllowUserToEditCells, AllowUserToAddRows, AutoSizeColumns, ColumnsToHide, LockDBDuringExchange, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB, false);
            }
            /// <summary>
            /// Used to populate a WinForms DataGridView with the results of a SQL SELECT command.
            /// </summary>
            /// <param name="DataGridToPopulate">The WinForms DataGridView to populate.</param>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLSelectCmd">The SQL SELECT command to execute.</param>
            /// <param name="AllowUserToEditCells">Boolean to set whether cells can be edited or if they are read only.</param>
            /// <param name="AllowUserToAddRows">Boolean to set whether the user can add new rows after the grid has been populated.</param>
            /// <param name="AutoSizeColumns">Boolean to flag whether columns should be auto-sized to fit content.</param>
            /// <param name="ColumnsToHide">String array of column names to hide after the grid is populated.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <param name="TryToConvertMSAccesCmdToSQLiteCmd">Boolean to indicate if the function should attempt to convert the SQL command from MSAccess format to SQLite format by replacing "#DATES#" with SQLite's "DATE(" / "DATETIME(" format.</param>
            /// <returns>Returns TRUE if the grid is populated. Returns FALSE in all other conditions.</returns>
            public static bool PopulateDataGrid(System.Windows.Forms.DataGridView DataGridToPopulate, string FullDatabasePath, string DatabasePassword, string SQLSelectCmd, bool AllowUserToEditCells, bool AllowUserToAddRows, bool AutoSizeColumns, string[] ColumnsToHide, bool LockDBDuringExchange, bool TryToConvertMSAccesCmdToSQLiteCmd)
            {
                // Use common method and pass defaults for unspecified values
                return PopulateDataGrid(DataGridToPopulate, FullDatabasePath, DatabasePassword, SQLSelectCmd, AllowUserToEditCells, AllowUserToAddRows, AutoSizeColumns, ColumnsToHide, LockDBDuringExchange, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB, TryToConvertMSAccesCmdToSQLiteCmd);
            }
            /// <summary>
            /// Used to populate a WinForms DataGridView with the results of a SQL SELECT command.
            /// </summary>
            /// <param name="DataGridToPopulate">The WinForms DataGridView to populate.</param>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="SQLSelectCmd">The SQL SELECT command to execute.</param>
            /// <param name="AllowUserToEditCells">Boolean to set whether cells can be edited or if they are read only.</param>
            /// <param name="AllowUserToAddRows">Boolean to set whether the user can add new rows after the grid has been populated.</param>
            /// <param name="AutoSizeColumns">Boolean to flag whether columns should be auto-sized to fit content.</param>
            /// <param name="ColumnsToHide">String array of column names to hide after the grid is populated.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <param name="OverrideReadWriteLockIfLockedMoreThanXSeconds">The amount of time (in seconds) to wait before a database READ/WRITE lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <param name="OverrideCompactLockIfLockedMoreThanXSecsPerMB">The amount of time (in seconds per megabyte) to wait before a COMPACTING database lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <param name="TryToConvertMSAccesCmdToSQLiteCmd">Boolean to indicate if the function should attempt to convert the SQL command from MSAccess format to SQLite format by replacing "#DATES#" with SQLite's "DATE(" / "DATETIME(" format.</param>
            /// <returns>Returns TRUE if the grid is populated. Returns FALSE in all other conditions.</returns>
            public static bool PopulateDataGrid(System.Windows.Forms.DataGridView DataGridToPopulate, string FullDatabasePath, string DatabasePassword, string SQLSelectCmd, bool AllowUserToEditCells, bool AllowUserToAddRows, bool AutoSizeColumns, string[] ColumnsToHide, bool LockDBDuringExchange, int OverrideReadWriteLockIfLockedMoreThanXSeconds, int OverrideCompactLockIfLockedMoreThanXSecsPerMB, bool TryToConvertMSAccesCmdToSQLiteCmd)
            {
                // Create var to hold if populate works
                bool PopulateWorked = false;

                // Check for database lock before continuing and execute lock if requested
                if (MainLockingCheckAndSetRoutine(FullDatabasePath, LockType.ReadWrite, OverrideReadWriteLockIfLockedMoreThanXSeconds, OverrideCompactLockIfLockedMoreThanXSecsPerMB, LockDBDuringExchange) == false)
                {
                    // If the db locked from a previous op OR cannot be locked, the function prompts, just exit
                    return PopulateWorked;
                }

                // Create SQLite database connection
                string ConnStr = CreateConnectionString(FullDatabasePath, DatabasePassword);
                var DBConn = new System.Data.SQLite.SQLiteConnection(ConnStr);

                // If user requested conversion, try to convert Access SQL cmd to SQLite version 
                if (TryToConvertMSAccesCmdToSQLiteCmd == true) SQLSelectCmd = ConvertMSAccessCmdToSQLiteCmd(SQLSelectCmd);

                try
                {
                    // Open database connection
                    DBConn.Open();

                    // Create the command
                    using (var SelectCommand = new System.Data.SQLite.SQLiteCommand())
                    {
                        // Configure the cmd
                        SelectCommand.Connection = DBConn;
                        SelectCommand.CommandType = System.Data.CommandType.Text;
                        SelectCommand.CommandText = SQLSelectCmd;

                        // Create adapter to get data
                        using (var DAdapt = new System.Data.SQLite.SQLiteDataAdapter())
                        {
                            // Create dataset to hold data
                            using (var DSet = new System.Data.DataSet())
                            {
                                // Fill dataset with adapter
                                string SearchTable = "SearchTable";
                                DAdapt.SelectCommand = SelectCommand;
                                DAdapt.Fill(DSet, SearchTable);

                                // Bind data to grid
                                DataGridToPopulate.DataSource = DSet.Tables[SearchTable];
                            }
                        }
                    }

                    // Close the connection
                    DBConn.Close();
                }
                catch (Exception ex)
                {
                    // Close the connection
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    DBConn.Close();

                    // Return false / exit since dataset wasn't created
                    return PopulateWorked;
                }
                finally
                {
                    // Release db connection resources
                    DBConn.Dispose();
                }

                try
                {
                    // Set whether user can edit cells or add rows
                    DataGridToPopulate.ReadOnly = !AllowUserToEditCells;
                    DataGridToPopulate.AllowUserToAddRows = AllowUserToAddRows;

                    // Check if user wanted columns auto sized
                    if (AutoSizeColumns == true)
                    {
                        // If so, auto size them
                        DataGridToPopulate.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
                    }
                    else
                    {
                        // If not, turn auto size off
                        DataGridToPopulate.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.None;
                    }

                    try
                    {
                        // Hide the columns if the user requested hidden columns
                        for (int i = 0; i <= ColumnsToHide.Length - 1; i++)
                            DataGridToPopulate.Columns[ColumnsToHide[i]].Visible = false;
                    }
                    catch (Exception)
                    {
                        // No action needed on failure
                    }

                    // Save true if grid populates
                    PopulateWorked = true;
                }
                catch (Exception ex)
                {
                    // Log errors
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                }

                // If database was locked, unlock it
                if (LockDBDuringExchange == true)
                {
                    LockOrUnlockSQLiteDatabase(FullDatabasePath, LockType.ReadWrite, LockState.Unlock);
                }

                // Return the result
                return PopulateWorked;
            }

            // OVERLOADED METHODS FOR RESETTING A TABLE'S PRIMARY KEY TO 1
            /// <summary>
            /// Used to primary auto-incremented key of a table to 1.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="TableToReset">The database table name to reset.</param>
            /// <returns>Returns TRUE if the table is cleared successfully. Returns FALSE in all other conditions.</returns>
            public static bool ResetTablePrimaryKeyCount(string FullDatabasePath, string DatabasePassword, string TableToReset)
            {
                // Use common method and pass defaults for unspecified values
                return ResetTablePrimaryKeyCount(FullDatabasePath, DatabasePassword, TableToReset, false, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB);
            }
            /// <summary>
            /// Used to primary auto-incremented key of a table to 1.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="TableToReset">The database table name to reset.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <returns>Returns TRUE if the table is cleared successfully. Returns FALSE in all other conditions.</returns>
            public static bool ResetTablePrimaryKeyCount(string FullDatabasePath, string DatabasePassword, string TableToReset, bool LockDBDuringExchange)
            {
                // Use common method and pass defaults for unspecified values
                return ResetTablePrimaryKeyCount(FullDatabasePath, DatabasePassword, TableToReset, LockDBDuringExchange, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB);
            }
            /// <summary>
            /// Used to primary auto-incremented key of a table to 1.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="TableToReset">The database table name to reset.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <param name="OverrideReadWriteLockIfLockedMoreThanXSeconds">The amount of time (in seconds) to wait before a database READ/WRITE lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <param name="OverrideCompactLockIfLockedMoreThanXSecsPerMB">The amount of time (in seconds per megabyte) to wait before a COMPACTING database lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <returns>Returns TRUE if the table is cleared successfully. Returns FALSE in all other conditions.</returns>
            public static bool ResetTablePrimaryKeyCount(string FullDatabasePath, string DatabasePassword, string TableToReset, bool LockDBDuringExchange, int OverrideReadWriteLockIfLockedMoreThanXSeconds, int OverrideCompactLockIfLockedMoreThanXSecsPerMB)
            {
                // Return result of execute sql reset cmd
                return ExecuteSQLUpdateCmd(FullDatabasePath, DatabasePassword, "DELETE FROM SQLITE_SEQUENCE WHERE NAME='" + TableToReset + "'", LockDBDuringExchange, OverrideReadWriteLockIfLockedMoreThanXSeconds, OverrideCompactLockIfLockedMoreThanXSecsPerMB, false);
            }

            // OVERLOADED METHODS FOR DELETING ALL RECORDS FROM A SPECIFIED TABLE AND RESETTING THE PRIMARY KEY TO 1
            /// <summary>
            /// Used to delete all records from a table AND reset the primary auto-incremented key to 1.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="TableToClear">The database table name to clear and reset.</param>
            /// <returns>Returns TRUE if the table is cleared successfully. Returns FALSE in all other conditions.</returns>
            public static bool ClearTableAndResetPrimaryKeyCount(string FullDatabasePath, string DatabasePassword, string TableToClear)
            {
                // Use common method and pass defaults for unspecified values
                return ClearTableAndResetPrimaryKeyCount(FullDatabasePath, DatabasePassword, TableToClear, false, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB);
            }
            /// <summary>
            /// Used to delete all records from a table AND reset the primary auto-incremented key to 1.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="TableToClear">The database table name to clear and reset.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <returns>Returns TRUE if the table is cleared successfully. Returns FALSE in all other conditions.</returns>
            public static bool ClearTableAndResetPrimaryKeyCount(string FullDatabasePath, string DatabasePassword, string TableToClear, bool LockDBDuringExchange)
            {
                // Use common method and pass defaults for unspecified values
                return ClearTableAndResetPrimaryKeyCount(FullDatabasePath, DatabasePassword, TableToClear, LockDBDuringExchange, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB);
            }
            /// <summary>
            /// Used to delete all records from a table AND reset the primary auto-incremented key to 1.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="TableToClear">The database table name to clear and reset.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <param name="OverrideReadWriteLockIfLockedMoreThanXSeconds">The amount of time (in seconds) to wait before a database READ/WRITE lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <param name="OverrideCompactLockIfLockedMoreThanXSecsPerMB">The amount of time (in seconds per megabyte) to wait before a COMPACTING database lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <returns>Returns TRUE if the table is cleared successfully. Returns FALSE in all other conditions.</returns>
            public static bool ClearTableAndResetPrimaryKeyCount(string FullDatabasePath, string DatabasePassword, string TableToClear, bool LockDBDuringExchange, int OverrideReadWriteLockIfLockedMoreThanXSeconds, int OverrideCompactLockIfLockedMoreThanXSecsPerMB)
            {
                // Create xaction cmds to reset table
                string[] XactionCmds = new string[2];
                XactionCmds[0] = "DELETE FROM " + TableToClear;
                XactionCmds[1] = "DELETE FROM SQLITE_SEQUENCE WHERE NAME='" + TableToClear + "'";

                // Execute xaction and return the result
                return ExecuteSQLXactionCmds(FullDatabasePath, DatabasePassword, XactionCmds, LockDBDuringExchange, OverrideReadWriteLockIfLockedMoreThanXSeconds, OverrideCompactLockIfLockedMoreThanXSecsPerMB, false);
            }

            // OVERLOADED METHODS FOR SETTING / CLEARING A DATABASE PASSWORD
            /// <summary>
            /// Used to create or change a SQLite database password.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="CurrentPassword">The current database password.</param>
            /// <param name="NewPassword">The new database password.</param>
            /// <returns>Returns TRUE if the new password is set successfully. Returns FALSE in all other conditions.</returns>
            public static bool SetDatabasePassword(string FullDatabasePath, string CurrentPassword, string NewPassword)
            {
                // Use common method and pass defaults for unspecified values
                return SetDatabasePassword(FullDatabasePath, CurrentPassword, NewPassword, false, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB);
            }
            /// <summary>
            /// Used to create or change a SQLite database password.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="CurrentPassword">The current database password.</param>
            /// <param name="NewPassword">The new database password.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <returns>Returns TRUE if the new password is set successfully. Returns FALSE in all other conditions.</returns>
            public static bool SetDatabasePassword(string FullDatabasePath, string CurrentPassword, string NewPassword, bool LockDBDuringExchange)
            {
                // Use common method and pass defaults for unspecified values
                return SetDatabasePassword(FullDatabasePath, CurrentPassword, NewPassword, LockDBDuringExchange, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB);
            }
            /// <summary>
            /// Used to create or change a SQLite database password.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="CurrentPassword">The current database password.</param>
            /// <param name="NewPassword">The new database password.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <param name="OverrideReadWriteLockIfLockedMoreThanXSeconds">The amount of time (in seconds) to wait before a database READ/WRITE lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <param name="OverrideCompactLockIfLockedMoreThanXSecsPerMB">The amount of time (in seconds per megabyte) to wait before a COMPACTING database lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <returns>Returns TRUE if the new password is set successfully. Returns FALSE in all other conditions.</returns>
            public static bool SetDatabasePassword(string FullDatabasePath, string CurrentPassword, string NewPassword, bool LockDBDuringExchange, int OverrideReadWriteLockIfLockedMoreThanXSeconds, int OverrideCompactLockIfLockedMoreThanXSecsPerMB)
            {
                // Create var to hold result of set
                bool PasswordSetWorked = false;

                // Check for database lock before continuing and execute lock if requested
                if (MainLockingCheckAndSetRoutine(FullDatabasePath, LockType.ReadWrite, OverrideReadWriteLockIfLockedMoreThanXSeconds, OverrideCompactLockIfLockedMoreThanXSecsPerMB, LockDBDuringExchange) == false)
                {
                    // If the db locked from a previous op OR cannot be locked, the function prompts, just exit
                    return false;
                }

                // Create SQLite database connection
                string ConnStr = CreateConnectionString(FullDatabasePath, CurrentPassword);
                var DBConn = new System.Data.SQLite.SQLiteConnection(ConnStr);

                try
                {
                    // Open connection and try to set new password
                    DBConn.Open();
                    DBConn.ChangePassword(NewPassword);

                    // Close the connection
                    DBConn.Close();

                    // Save true if everything works
                    PasswordSetWorked = true;
                }
                catch (Exception ex)
                {
                    // Close the connection
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    DBConn.Close();
                }
                finally
                {
                    // Release db connection resources
                    DBConn.Dispose();
                }

                // If database was locked, unlock it
                if (LockDBDuringExchange == true)
                {
                    LockOrUnlockSQLiteDatabase(FullDatabasePath, LockType.ReadWrite, LockState.Unlock);
                }

                // Return the result
                return PasswordSetWorked;
            }



            // OVERLOADED METHODS FOR CHECKING A SQLITE TABLE IS CORRUPT (E.G. STRING IN DATE FIELD)
            /// <summary>
            /// Used to check if a SQLite table is corrupt (e.g. string in date field) by attempting to fill a dataset (dataset will error out if fields are corrupt).
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="TableToCheck">The SQLite database table to check for corruption.</param>
            /// <returns>Returns TRUE if the table is corrupt and needs repair. Returns FALSE if the table appears to be intact.</returns>
            public static bool DoesTableNeedToBeRepaired(string FullDatabasePath, string DatabasePassword, string TableToCheck)
            {
                // Use common method and pass defaults for unspecified values
                return DoesTableNeedToBeRepaired(FullDatabasePath, DatabasePassword, TableToCheck, false, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB);
            }
            /// <summary>
            /// Used to check if a SQLite table is corrupt (e.g. string in date field) by attempting to fill a dataset (dataset will error out if fields are corrupt).
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="TableToCheck">The SQLite database table to check for corruption.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <returns>Returns TRUE if the table is corrupt and needs repair. Returns FALSE if the table appears to be intact.</returns>
            public static bool DoesTableNeedToBeRepaired(string FullDatabasePath, string DatabasePassword, string TableToCheck, bool LockDBDuringExchange)
            {
                // Use common method and pass defaults for unspecified values
                return DoesTableNeedToBeRepaired(FullDatabasePath, DatabasePassword, TableToCheck, LockDBDuringExchange, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB);
            }
            /// <summary>
            /// Used to check if a SQLite table is corrupt (e.g. string in date field) by attempting to fill a dataset (dataset will error out if fields are corrupt).
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="TableToCheck">The SQLite database table to check for corruption.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <param name="OverrideReadWriteLockIfLockedMoreThanXSeconds">The amount of time (in seconds) to wait before a database READ/WRITE lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <param name="OverrideCompactLockIfLockedMoreThanXSecsPerMB">The amount of time (in seconds per megabyte) to wait before a COMPACTING database lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <returns>Returns TRUE if the table is corrupt and needs repair. Returns FALSE if the table appears to be intact.</returns>
            public static bool DoesTableNeedToBeRepaired(string FullDatabasePath, string DatabasePassword, string TableToCheck, bool LockDBDuringExchange, int OverrideReadWriteLockIfLockedMoreThanXSeconds, int OverrideCompactLockIfLockedMoreThanXSecsPerMB)
            {
                // Create flag to hold if table requires repair
                bool TableNeedsRepair = true;

                // Check for database lock before continuing and execute lock if requested
                if (MainLockingCheckAndSetRoutine(FullDatabasePath, LockType.Compacting, OverrideReadWriteLockIfLockedMoreThanXSeconds, OverrideCompactLockIfLockedMoreThanXSecsPerMB, LockDBDuringExchange) == false)
                {
                    // If the db locked from a previous op OR cannot be locked, the function prompts, return false
                    return false;
                }

                // Create SQLite database connection & command for selecting everything in table
                string ConnStr = CreateConnectionString(FullDatabasePath, DatabasePassword);
                var DBConn = new System.Data.SQLite.SQLiteConnection(ConnStr);
                string SQLCmdSelectAll = "SELECT * FROM " + TableToCheck;

                try
                {
                    // Open database connection
                    DBConn.Open();

                    // Select all records in table
                    using (var SelectCommand = new System.Data.SQLite.SQLiteCommand())
                    {
                        // Build the command
                        SelectCommand.Connection = DBConn;
                        SelectCommand.CommandType = System.Data.CommandType.Text;
                        SelectCommand.CommandText = SQLCmdSelectAll;

                        // Attempt to populate a dataset with info
                        using (var DAdapt = new System.Data.SQLite.SQLiteDataAdapter())
                        {
                            using (var DSet = new System.Data.DataSet())
                            {
                                DAdapt.SelectCommand = SelectCommand;
                                DAdapt.Fill(DSet, TableToCheck);
                            }
                        }
                    }

                    // If all data can load, the table is good
                    TableNeedsRepair = false;
                }
                catch (Exception ex)
                {
                    // If the dataset can't be populated, table needs repair
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    TableNeedsRepair = true;
                }
                finally
                {
                    // Close connection and release all resources
                    DBConn.Close();
                    DBConn.Dispose();
                }

                // Check if they are networking
                if (LockDBDuringExchange == true)
                {
                    // If so, the db would have been locked above, unlock it now
                    LockOrUnlockSQLiteDatabase(FullDatabasePath, LockType.Compacting, LockState.Unlock);
                }

                // Return the result
                return TableNeedsRepair;
            }

            // OVERLOADED METHODS FOR REPAIRING A CORRUPT SQLITE TABLE (E.G. DELETE ALL UNREADABLE ROWS)
            /// <summary>
            /// Used to delete any rows from the database that can't be read into a dataset using a "known good" unique field in the table.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="TableToRepair">The SQLite database table to repair.</param>
            /// <param name="KnownGoodUniqueIDColumnName">The name of a column that (1) contains unique IDs for the table and (2) will definitely NOT have corrupt data.</param>
            /// <param name="UniqueIDColumnDataType">The data type of the "Known Good" column to ensure that SQL repair statements are in the correct format (e.g. apostrophes vs. no apostrophes).</param>
            /// <returns>Returns TRUE if the table is repaired successfully. Returns FALSE in all other conditions.</returns>
            public static bool RepairTable(string FullDatabasePath, string DatabasePassword, string TableToRepair, string KnownGoodUniqueIDColumnName, UniqueIDColumnDataType UniqueIDColumnDataType)
            {
                // Use common method and pass defaults for unspecified values
                return RepairTable(FullDatabasePath, DatabasePassword, TableToRepair, KnownGoodUniqueIDColumnName, UniqueIDColumnDataType, false, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB);
            }
            /// <summary>
            /// Used to delete any rows from the database that can't be read into a dataset using a "known good" unique field in the table.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="TableToRepair">The SQLite database table to repair.</param>
            /// <param name="KnownGoodUniqueIDColumnName">The name of a column that (1) contains unique IDs for the table and (2) will definitely NOT have corrupt data.</param>
            /// <param name="UniqueIDColumnDataType">The data type of the "Known Good" column to ensure that SQL repair statements are in the correct format (e.g. apostrophes vs. no apostrophes).</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <returns>Returns TRUE if the table is repaired successfully. Returns FALSE in all other conditions.</returns>
            public static bool RepairTable(string FullDatabasePath, string DatabasePassword, string TableToRepair, string KnownGoodUniqueIDColumnName, UniqueIDColumnDataType UniqueIDColumnDataType, bool LockDBDuringExchange)
            {
                // Use common method and pass defaults for unspecified values
                return RepairTable(FullDatabasePath, DatabasePassword, TableToRepair, KnownGoodUniqueIDColumnName, UniqueIDColumnDataType, LockDBDuringExchange, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB);
            }
            /// <summary>
            /// Used to delete any rows from the database that can't be read into a dataset using a "known good" unique field in the table.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="TableToRepair">The SQLite database table to repair.</param>
            /// <param name="KnownGoodUniqueIDColumnName">The name of a column that (1) contains unique IDs for the table and (2) will definitely NOT have corrupt data.</param>
            /// <param name="UniqueIDColumnDataType">The data type of the "Known Good" column to ensure that SQL repair statements are in the correct format (e.g. apostrophes vs. no apostrophes).</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <param name="OverrideReadWriteLockIfLockedMoreThanXSeconds">The amount of time (in seconds) to wait before a database READ/WRITE lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <param name="OverrideCompactLockIfLockedMoreThanXSecsPerMB">The amount of time (in seconds per megabyte) to wait before a COMPACTING database lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <returns>Returns TRUE if the table is repaired successfully. Returns FALSE in all other conditions.</returns>
            public static bool RepairTable(string FullDatabasePath, string DatabasePassword, string TableToRepair, string KnownGoodUniqueIDColumnName, UniqueIDColumnDataType UniqueIDColumnDataType, bool LockDBDuringExchange, int OverrideReadWriteLockIfLockedMoreThanXSeconds, int OverrideCompactLockIfLockedMoreThanXSecsPerMB)
            {
                // Create SQL cmd & array for getting all table PKs
                string CmdToExecute = "SELECT " + KnownGoodUniqueIDColumnName + " FROM " + TableToRepair;
                object[,] oPKArray = null;

                // Try to get PKs
                if (GetSearchResultsAsArray(FullDatabasePath, DatabasePassword, CmdToExecute, ref oPKArray, LockDBDuringExchange: LockDBDuringExchange, OverrideReadWriteLockIfLockedMoreThanXSeconds: OverrideReadWriteLockIfLockedMoreThanXSeconds, OverrideCompactLockIfLockedMoreThanXSecsPerMB: OverrideCompactLockIfLockedMoreThanXSecsPerMB, TryToConvertMSAccesCmdToSQLiteCmd: false) == false || oPKArray == null)
                {
                    // Return false if info can't be retrieved
                    return false;
                }

                // If PKs are retrieved, get all PKs that need to be deleted (e.g. rows that can't be read)
                var PKsToDelete = GetAllPKsToDelete(FullDatabasePath, DatabasePassword, TableToRepair, oPKArray, LockDBDuringExchange, OverrideReadWriteLockIfLockedMoreThanXSeconds, OverrideCompactLockIfLockedMoreThanXSecsPerMB);

                // If no PK rows need to be deleted, return true
                if (PKsToDelete == null || PKsToDelete.Count == 0) return true;

                // If bad PKs are found, delete them as a xaction 
                string[] XactionCmds = new string[PKsToDelete.Count - 1 + 1];
                for (int i = 0; i <= XactionCmds.GetUpperBound(0); i++)
                {
                    // Check the data type to ensure SQL syntax is good
                    if (UniqueIDColumnDataType == UniqueIDColumnDataType.Numeric)
                    {
                        // No (')'s in numeric field names
                        XactionCmds[i] = "DELETE FROM " + TableToRepair + " WHERE " + KnownGoodUniqueIDColumnName + " = " + PKsToDelete[i];
                    }
                    else
                    {
                        // Use (')'s in string field names
                        XactionCmds[i] = "DELETE FROM " + TableToRepair + " WHERE " + KnownGoodUniqueIDColumnName + " = '" + PKsToDelete[i] + "'";
                    }
                }

                // Execute the deletes as a xaction (ExecuteDBExactionCmds will perform lock if needed)
                return ExecuteSQLXactionCmds(FullDatabasePath, DatabasePassword, XactionCmds, LockDBDuringExchange, OverrideReadWriteLockIfLockedMoreThanXSeconds, OverrideCompactLockIfLockedMoreThanXSecsPerMB, false);
            }
            private static System.Collections.ObjectModel.Collection<string> GetAllPKsToDelete(string FullDatabasePath, string DatabasePassword, string TableNameToCheck, object[,] PKsToCheck)
            {
                // Use the higher level method
                return GetAllPKsToDelete(FullDatabasePath, DatabasePassword, TableNameToCheck, PKsToCheck, false, 30, 1);
            }
            private static System.Collections.ObjectModel.Collection<string> GetAllPKsToDelete(string FullDatabasePath, string DatabasePassword, string TableNameToCheck, object[,] PKsToCheck, bool LockDBDuringExchange, int OverrideReadWriteLockIfLockedMoreThanXSeconds, int OverrideCompactLockIfLockedMoreThanXSecsPerMB)
            {
                // Create the collection to return
                var CollectionToReturn = new System.Collections.ObjectModel.Collection<string>();

                // Check for database lock before continuing and execute lock if requested
                if (MainLockingCheckAndSetRoutine(FullDatabasePath, LockType.Compacting, OverrideReadWriteLockIfLockedMoreThanXSeconds, OverrideCompactLockIfLockedMoreThanXSecsPerMB, LockDBDuringExchange) == false)
                {
                    // If the db locked from a previous op OR cannot be locked, the function prompts, just exit
                    return null;
                }

                // Create select cmds to individually check for bad rows
                string[] SQLCmds = new string[PKsToCheck.GetUpperBound(0) + 1];
                for (int i = 0; i <= PKsToCheck.GetUpperBound(0); i++)
                {
                    SQLCmds[i] = "SELECT * FROM " + TableNameToCheck + " WHERE PK = " + PKsToCheck[i, 0].ToString();
                }

                // Create SQLite database connection
                string ConnStr = CreateConnectionString(FullDatabasePath, DatabasePassword);
                var DBConn = new System.Data.SQLite.SQLiteConnection(ConnStr);

                try
                {
                    // Open database connection
                    DBConn.Open();

                    // Create vars for command and dataset
                    using (var SelectCommand = new System.Data.SQLite.SQLiteCommand())
                    {
                        using (var DAdapt = new System.Data.SQLite.SQLiteDataAdapter())
                        {
                            using (var DSet = new System.Data.DataSet())
                            {
                                // Try to execute each SQL statement in the cmds array, will error out if bad date is found
                                for (int i = 0; i <= SQLCmds.GetUpperBound(0); i++)
                                {
                                    try
                                    {
                                        // Build the command
                                        SelectCommand.Connection = DBConn;
                                        SelectCommand.CommandType = System.Data.CommandType.Text;
                                        SelectCommand.CommandText = SQLCmds[i];

                                        // Try and fill the dataset
                                        DAdapt.SelectCommand = SelectCommand;
                                        DAdapt.Fill(DSet, TableNameToCheck);
                                    }
                                    catch (Exception ex)
                                    {
                                        // If the dataset can't be filled, the pk is bad, add it to the deletion list
                                        System.Diagnostics.Trace.WriteLine(ex.Message);
                                        CollectionToReturn.Add(PKsToCheck[i, 0].ToString());
                                    }
                                }
                            }
                        }
                    }
                }
                finally
                {
                    // Close connection and release all resources
                    DBConn.Close();
                    DBConn.Dispose();
                }

                // Check if they are networking
                if (LockDBDuringExchange == true)
                {
                    // If so, the db would have been locked above, unlock it now
                    LockOrUnlockSQLiteDatabase(FullDatabasePath, LockType.Compacting, LockState.Unlock);
                }

                // Return the collection of bad rows
                return CollectionToReturn;
            }

            // OVERLOADED METHODS FOR COMPACTING THE DATABASE
            /// <summary>
            /// Used to compact a SQLite database.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <returns>Returns TRUE if the database is compacted. Returns FALSE in all other conditions.</returns>
            public static bool CompactDatabase(string FullDatabasePath, string DatabasePassword)
            {
                // Use common method and pass defaults for unspecified values
                return CompactDatabase(FullDatabasePath, DatabasePassword, false, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB);
            }
            /// <summary>
            /// Used to compact a SQLite database.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <returns>Returns TRUE if the database is compacted. Returns FALSE in all other conditions.</returns>
            public static bool CompactDatabase(string FullDatabasePath, string DatabasePassword, bool LockDBDuringExchange)
            {
                // Use common method and pass defaults for unspecified values
                return CompactDatabase(FullDatabasePath, DatabasePassword, LockDBDuringExchange, Defaults.OverrideReadWriteLockAfterXSeconds, Defaults.OverrideCompactLockAfterXSecsPerMB);
            }
            /// <summary>
            /// Used to compact a SQLite database.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <param name="LockDBDuringExchange">Boolean to set whether the database should be locked during the execution of the command.</param>
            /// <param name="OverrideReadWriteLockIfLockedMoreThanXSeconds">The amount of time (in seconds) to wait before a database READ/WRITE lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <param name="OverrideCompactLockIfLockedMoreThanXSecsPerMB">The amount of time (in seconds per megabyte) to wait before a COMPACTING database lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <returns>Returns TRUE if the database is compacted. Returns FALSE in all other conditions.</returns>
            public static bool CompactDatabase(string FullDatabasePath, string DatabasePassword, bool LockDBDuringExchange, int OverrideReadWriteLockIfLockedMoreThanXSeconds, int OverrideCompactLockIfLockedMoreThanXSecsPerMB)
            {
                // Create flag to hold if compacting worked
                bool CompactingWorked = false;

                // Check for database lock before continuing and execute lock if requested
                if (MainLockingCheckAndSetRoutine(FullDatabasePath, LockType.Compacting, OverrideReadWriteLockIfLockedMoreThanXSeconds, OverrideCompactLockIfLockedMoreThanXSecsPerMB, LockDBDuringExchange) == false)
                {
                    // If the db locked from a previous op OR cannot be locked, the function prompts, just exit
                    return false;
                }

                // Create SQLite database connection
                string ConnStr = CreateConnectionString(FullDatabasePath, DatabasePassword);
                var DBConn = new System.Data.SQLite.SQLiteConnection(ConnStr);

                try
                {
                    // Open database connection
                    DBConn.Open();

                    // Attempt to compact the database
                    using (var ExecuteCommand = new System.Data.SQLite.SQLiteCommand())
                    {
                        ExecuteCommand.Connection = DBConn;
                        ExecuteCommand.CommandType = System.Data.CommandType.Text;
                        ExecuteCommand.CommandText = "VACUUM;";
                        ExecuteCommand.ExecuteNonQuery();
                    }

                    // Close the connection
                    DBConn.Close();

                    // Save true if everything works
                    CompactingWorked = true;
                }
                catch (Exception ex)
                {
                    // Close the connection
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    DBConn.Close();
                }
                finally
                {
                    // Release db connection resources
                    DBConn.Dispose();
                }

                // If database was locked, unlock it
                if (LockDBDuringExchange == true)
                {
                    LockOrUnlockSQLiteDatabase(FullDatabasePath, LockType.Compacting, LockState.Unlock);
                }

                // Return the result of the compacting
                return CompactingWorked;
            }

            // Used to set how long to wait and check if the database is still locked before returning IsDatabaseLocked = True
            private const int RecheckDatabaseLocking = 1500;

            // Generic msg if database is locked
            private const string DatabaseLockedMsg = "The database is currently locked / in use. Please try again later.";

            // Generic failure to lock database msg
            private const string FailureToLockDatabaseMsg = "The software cannot lock your database. You may not have the necessary read/write permissions. You cannot access the database until the issue is resolved.";

            // Enum for referencing the type of lock
            private enum LockType
            {
                ReadWrite = 0,
                Compacting = 1
            }

            // Enum for referencing the lock state
            private enum LockState
            {
                Lock = 0,
                Unlock = 1
            }

            // Structure for holding string names of lock file prefixes (so other routines can check the locking type)
            private struct LockTypePrefix
            {
                public const string ReadWrite = "READWRITE";
                public const string Compacting = "COMPACTING";
            }

            // Combines the below functions to check for an existing db lock and, if requested, execute a new lock
            private static bool MainLockingCheckAndSetRoutine(string FullDatabasePath, LockType LockType, int OverrideReadWriteLockIfLockedMoreThanXSeconds, int OverrideCompactLockIfLockedMoreThanXSecsPerMB, bool LockDBDuringExchange)
            {
                // Check if the database is locked before continuing
                if (IsDatabaseLocked(FullDatabasePath, OverrideReadWriteLockIfLockedMoreThanXSeconds: OverrideReadWriteLockIfLockedMoreThanXSeconds, OverrideCompactLockIfLockedMoreThanXSecsPerMB: OverrideCompactLockIfLockedMoreThanXSecsPerMB) == true)
                {
                    // If the db is locked, wait a little and check one more time as most db interactions should be very quick
                    System.Threading.Thread.Sleep(RecheckDatabaseLocking);

                    // Check to see if the lock has finished one more time
                    if (IsDatabaseLocked(FullDatabasePath, OverrideReadWriteLockIfLockedMoreThanXSeconds: OverrideReadWriteLockIfLockedMoreThanXSeconds, OverrideCompactLockIfLockedMoreThanXSecsPerMB: OverrideCompactLockIfLockedMoreThanXSecsPerMB) == true)
                    {
                        // If database is still locked, return false
                        return false;
                    }
                }

                // Check if the database needs to be locked
                if (LockDBDuringExchange == true)
                {
                    // If so, execute the lock
                    if (LockOrUnlockSQLiteDatabase(FullDatabasePath, LockType, LockState.Lock) == false)
                    {
                        // Return false if a lock was requested but could not be executed
                        return false;
                    }
                }

                // Return true if the db was locked successfully or doesn't need to be locked
                return true;
            }

            /// <summary>
            /// Used to check if a given SQLite database has been locked using the custom read / write / compact lock routine.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="OverrideReadWriteLockIfLockedMoreThanXSeconds">The amount of time (in seconds) to wait before a database READ/WRITE lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <param name="OverrideCompactLockIfLockedMoreThanXSecsPerMB">The amount of time (in seconds per megabyte) to wait before a COMPACTING database lock can be overridden (e.g. in case of previous application crash while database was locked).</param>
            /// <returns>Returns TRUE if the database is locked. Returns FALSE if the database is NOT locked.</returns>
            public static bool IsDatabaseLocked(string FullDatabasePath, int OverrideReadWriteLockIfLockedMoreThanXSeconds = 30, int OverrideCompactLockIfLockedMoreThanXSecsPerMB = 1)
            {
                // Create flag to hold if db is locked (default assumption is that it is locked)
                bool DatabaseIsLocked = true;

                // Needs vars for capturing each lock type
                bool ReadWriteLockEnabled = true;
                bool CompactingLockEnabled = true;

                // Get the location & generic name for the lock file
                string LockFilePath = GetLockFilePath(FullDatabasePath);
                string LockFileName = GetLockFileName(FullDatabasePath);

                // Create path string for each type of lock file
                string LockFile_ReadWrite = LockFilePath + LockTypePrefix.ReadWrite + LockFileName;
                string LockFile_Compact = LockFilePath + LockTypePrefix.Compacting + LockFileName;

                // Wrap entire check in try as precaution
                try
                {
                    // Check for read-write lock
                    if (System.IO.File.Exists(LockFile_ReadWrite) == false)
                    {
                        // If it doesn't, a lock is not in place
                        ReadWriteLockEnabled = false;
                    }
                    else
                    {
                        // If the file exists, see how old it is
                        var ReadWriteLockStartTime = System.IO.File.GetLastAccessTime(LockFile_ReadWrite);

                        // Check if it is old enough to be overridden
                        if (System.Math.Abs((DateTime.Now - ReadWriteLockStartTime).TotalSeconds) < OverrideReadWriteLockIfLockedMoreThanXSeconds)
                        {
                            // If the file is not old enough to override, a lock is in place
                            ReadWriteLockEnabled = true;
                        }
                        else
                        {
                            try
                            {
                                // If it is old enough to override, try to delete it
                                System.IO.File.Delete(LockFile_ReadWrite);
                            }
                            catch (Exception)
                            {
                                // No action needed on failure
                            }

                            // Whether the file was deleted or not, if it's old enough, it can be ignored
                            ReadWriteLockEnabled = false;
                        }
                    }

                    // Check for compacting lock
                    if (System.IO.File.Exists(LockFile_Compact) == false)
                    {
                        // If it doesn't, a lock is not in place
                        CompactingLockEnabled = false;
                    }
                    else
                    {
                        // If the file exists, see how old it is & get the size of the db
                        var CompactingLockStartTime = System.IO.File.GetCreationTime(LockFile_Compact);
                        var DBInfo = new System.IO.FileInfo(FullDatabasePath);
                        int DBSizeInMBs = Convert.ToInt32(DBInfo.Length / System.Math.Pow(1000, 2));

                        // Calculate amount of time to wait before lock override (1 second / MB of db size)
                        int TotalOverrideWaitTime = OverrideCompactLockIfLockedMoreThanXSecsPerMB * DBSizeInMBs;

                        // Make sure that there is at least 30 seconds of override wait time
                        if (TotalOverrideWaitTime < 30) TotalOverrideWaitTime = 30;

                        // Check if the lock is old enough to override
                        if (System.Math.Abs((DateTime.Now - CompactingLockStartTime).TotalSeconds) < TotalOverrideWaitTime)
                        {
                            // If the file is not old enough to override, a lock is in place
                            CompactingLockEnabled = true;
                        }
                        else
                        {
                            try
                            {
                                // If it is old enough to override, try to delete it
                                System.IO.File.Delete(LockFile_Compact);
                            }
                            catch (Exception)
                            {
                                // No action needed on failure
                            }

                            // Whether the file was deleted or not, if it's old enough, it can be ignored
                            CompactingLockEnabled = false;
                        }
                    }

                    // Check if either type of lock is enabled
                    if (ReadWriteLockEnabled == false & CompactingLockEnabled == false)
                    {
                        // If no locks are in place, return false
                        DatabaseIsLocked = false;
                    }
                    else
                    {
                        // If one or both locks are in place (should only be one at any given time), return that the db is locked
                        DatabaseIsLocked = true;
                    }
                }
                catch (Exception ex)
                {
                    // Just log errors
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                }

                // Return the result of the lock check
                return DatabaseIsLocked;
            }

            // Used to create a or remove "Lock" file indicating the that database is in use (so other code doesn't attempt to access it at the same time)
            private static bool LockOrUnlockSQLiteDatabase(string FullDatabasePath, LockType LockType, LockState LockState)
            {
                // Create flag to hold if lock / unlock worked
                bool LockUnlockWorked = false;

                try
                {
                    // Get the location & generic name for the lock file
                    string LockFilePath = GetLockFilePath(FullDatabasePath);
                    string LockFileName = GetLockFileName(FullDatabasePath);

                    // Add prefix to lock file name so other routines can see why the db is locked (compacting locks could last for minutes, read/write locks should 
                    // only be a few seconds). Other routines can override locks that appear to be taking too long (e.g. power loss before lock file is deleted).
                    string LockFilePrefix = LockTypePrefix.ReadWrite;
                    if (LockType == LockType.Compacting) LockFilePrefix = LockTypePrefix.Compacting;
                    LockFileName = LockFilePrefix + LockFileName;
                    LockFilePath = LockFilePath + LockFileName;

                    // Check if locking or unlocking
                    switch (LockState)
                    {
                        case LockState.Lock:
                            // If locking, write the lock file
                            System.IO.File.WriteAllText(LockFilePath, "LOCK");
                            break;
                        case LockState.Unlock:
                            // If unlocking, delete the lock file
                            System.IO.File.Delete(LockFilePath);
                            break;
                    }

                    // Save true if the above executed without failure
                    LockUnlockWorked = true;
                }
                catch (Exception ex)
                {
                    // Log errors
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                }

                // Return the result of the locking
                return LockUnlockWorked;
            }

            // Used to get the lock file / path for locking the SQLite database
            private static string GetLockFilePath(string FullDatabasePath)
            {
                try
                {
                    // Return the folder
                    return System.IO.Path.GetDirectoryName(FullDatabasePath);
                }
                catch (Exception)
                {
                    // Return empty if error occurs
                    return string.Empty;
                }
            }
            private static string GetLockFileName(string FullDatabasePath)
            {
                try
                {
                    // Get the database file name
                    string DatabaseFileName = System.IO.Path.GetFileNameWithoutExtension(FullDatabasePath);

                    // Bolt on lock extension and return
                    return DatabaseFileName + ".lock";
                }
                catch (Exception)
                {
                    // Return empty if error occurs
                    return string.Empty;
                }
            }

            /// <summary>
            /// Used to create the SQLite connection string from the database location and password.
            /// </summary>
            /// <param name="FullDatabasePath">The full file path to the database file.</param>
            /// <param name="DatabasePassword">The database password.</param>
            /// <returns>Returns a SQLite format connection string to the database.</returns>
            public static string CreateConnectionString(string FullDatabasePath, string DatabasePassword)
            {
                // Check if there is a password
                if (string.IsNullOrEmpty(DatabasePassword) == true)
                {
                    return "Data Source=" + FullDatabasePath + ";FailIfMissing=True";
                }
                else
                {
                    return "Data Source=" + FullDatabasePath + ";Password=" + DatabasePassword + ";FailIfMissing=True";
                }
            }

            /// <summary>
            /// Used to convert an MSAccess format SQL command to a SQLite format SQL command by replacing "#DATES#" format with SQLite's "DATE(" / "DATETIME(" format.
            /// </summary>
            /// <param name="MSAccessSQLCmd">The SQL command in Microsoft Access format (i.e. dates wrapped in #'s).</param>
            /// <returns>Returns the SQL cmd with dates converted to the SQLite date format.</returns>
            public static string ConvertMSAccessCmdToSQLiteCmd(string MSAccessSQLCmd)
            {
                // Create the string to return
                string StringToReturn = string.Empty;

                // Split Access dates by # parsing
                string[] SplitArray = MSAccessSQLCmd.Split(new[] { "#" }, StringSplitOptions.None);

                // Loop through each of the split terms
                for (int i = 0; i <= SplitArray.GetUpperBound(0); i++)
                {
                    // First, check if the value is a date
                    if (Database.IsDate(SplitArray[i]) == true)
                    {
                        // If the value can be converted to a date, check for "/" char to prevent a number that could be converted from being mistaken as a date
                        int SlashCounter = 0;
                        for (int j = 1; j <= SplitArray[i].Length; j++)
                        {
                            // Loop through each char & count the "/"s
                            if (Microsoft.VisualBasic.Strings.Mid(SplitArray[i], j, 1) == "/")
                            {
                                SlashCounter = SlashCounter + 1;
                            }
                        }

                        // Check that exactly two /'s were found
                        if (SlashCounter == 2)
                        {
                            // If two /'s were found, check that the term contains at least 8 chars (e.g. smallest case date = x/x/xxxx)
                            if (SplitArray[i].Length >= 8)
                            {
                                // At this point, value is date, check if there is a time attached to the date
                                bool IncludeTime = SplitArray[i].Contains(":");

                                // Finally, convert this value to SQLite date and overwrite it's position in the split array
                                SplitArray[i] = ConvertToSQLiteDateString(Convert.ToDateTime(SplitArray[i]), IncludeTime);
                            }
                        }
                    }
                }

                // Re-assemble the array into a string
                for (int i = 0; i <= SplitArray.GetUpperBound(0); i++)
                {
                    StringToReturn += SplitArray[i];
                }

                // Return the string
                return StringToReturn;
            }

            /// <summary>
            /// Used to convert a date value to a SQLite compatible date that can be used in a SQL command (SQLite Date Format = YYYY-MM-DD HH:MM:SS).
            /// </summary>
            /// <param name="DateToConvert">The date value to convert.</param>
            /// <param name="IncludeTime">Boolean to indicate if time should be included in the returned string.</param>
            /// <returns>Returns a SQLite compatible date string.</returns>
            public static string ConvertToSQLiteDateString(DateTime DateToConvert, bool IncludeTime)
            {
                // Create the string to return
                string StringToReturn = string.Empty;

                // Get year, month, and day
                StringToReturn = DateToConvert.Year.ToString() + "-";
                StringToReturn += PadNumberString(DateToConvert.Month.ToString(), 2) + "-";
                StringToReturn += PadNumberString(DateToConvert.Day.ToString(), 2);

                // Check if time was requested
                if (IncludeTime == true)
                {
                    // Add space between date & time
                    StringToReturn += " ";

                    // Append H:M:S
                    StringToReturn += PadNumberString(DateToConvert.Hour.ToString(), 2) + ":";
                    StringToReturn += PadNumberString(DateToConvert.Minute.ToString(), 2) + ":";
                    StringToReturn += PadNumberString(DateToConvert.Second.ToString(), 2);
                }

                // Ensure string is clean
                StringToReturn = StringToReturn.Trim();

                // Add in SQL datetime wrapper
                if (IncludeTime == true)
                {
                    // If using time, use DATETIME
                    StringToReturn = "DATETIME('" + StringToReturn + "')";
                }
                else
                {
                    // If not, just use DATE
                    StringToReturn = "DATE('" + StringToReturn + "')";
                }

                // Return the string
                return StringToReturn;
            }

            /// <summary>
            /// Used to convert a standard boolean value to a SQLite boolean value string and can be used in SQL commands.
            /// </summary>
            /// <param name="BooleanValue">The standard .NET boolean value to convert.</param>
            /// <returns>Returns "1" for True and "0" for False.</returns>
            public static string ConvertToSQLiteBooleanValue(bool BooleanValue)
            {
                // Check state
                if (BooleanValue == true)
                {
                    // SQLite uses 1 for true (not -1)
                    return "1";
                }
                else
                {
                    // SQLite uses 0 for false
                    return "0";
                }
            }

            /// <summary>
            /// Used to convert a string value to a .NET boolean value.
            /// </summary>
            /// <param name="SQLiteBooleanValueString">The string to convert to boolean.</param>
            /// <returns>Returns TRUE for "1", "-1", or "True". Returns FALSE in all other conditions.</returns>
            public static bool ConvertToDotNetBooleanValue(string SQLiteBooleanValueString)
            {
                // Check SQLite boolean value string
                if (SQLiteBooleanValueString == "1" || SQLiteBooleanValueString == "-1" || SQLiteBooleanValueString.ToUpper() == "TRUE")
                {
                    // Technically, only 1 is SQLite boolean true but handle .net boolean true (-1) as well
                    return true;
                }
                else
                {
                    // Zero is equivalent of boolean false
                    return false;
                }
            }

            /// <summary>
            /// Helper method for padding numbers with leading zeros.
            /// </summary>
            private static string PadNumberString(string OriginalNumberString, uint NumberOfCharsToPadTo)
            {
                // Create the string to return
                string StringToReturn = OriginalNumberString;

                // Keep padding w/ 0's until length is big enough
                while (StringToReturn.Length < NumberOfCharsToPadTo)
                {
                    StringToReturn = "0" + StringToReturn;
                }

                // This condition should never occur
                if (StringToReturn.Length > NumberOfCharsToPadTo)
                {
                    // If the string is too long, crop it down to requested length
                    StringToReturn = StringToReturn.Substring(Convert.ToInt32(StringToReturn.Length - NumberOfCharsToPadTo));
                }

                // Return the string
                return StringToReturn;
            }

        }

    }

}
