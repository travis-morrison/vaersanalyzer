﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Vaers
{

    public partial class VaxDetailedView : Window
    {

        #region Declarations


        // Vars to hold passed in inputs
        private DatabaseManager InputRawData = null;
        private DatabaseManager.VaccineEventReport InputFullReport = null;
        private string InputVaccineName = string.Empty;
        private Action<string> InputCallbackReportStatus = null;

        // Var to hold handles to charts
        private System.Windows.Forms.DataVisualization.Charting.Chart CHARTSymptomsByMfg = new System.Windows.Forms.DataVisualization.Charting.Chart();

        // Constant to hold wildcard for show all
        private const string ShowAll = "*ALL*";


        #endregion

        #region Load & Unload Methods


        public VaxDetailedView(DatabaseManager RawData, DatabaseManager.VaccineEventReport FullReport, string VaxName, Action<string> MethodToReportStatusMsgsTo)
        {
            // Draw the window
            InitializeComponent();

            // Save inputs
            InputRawData = RawData;
            InputFullReport = FullReport;
            InputVaccineName = VaxName;
            InputCallbackReportStatus = MethodToReportStatusMsgsTo;

            // Report error for missing required inputs
            if (InputRawData == null || InputFullReport == null || string.IsNullOrEmpty(InputVaccineName) == true)
            {
                throw new Exception("One or more missing required inputs!");
            }

            // Bind charts to containers
            WFHostChartSymptomsByMfg.Child = CHARTSymptomsByMfg;

            // Wire up events
            this.Closed += VaxDetailedView_Closed;
            LBManufacturer.SelectionChanged += LBManufacturer_SelectionChanged;

            // Populate the window
            PopulateView();
        }

        private void VaxDetailedView_Closed(object sender, EventArgs e)
        {
            // Dispose of WF charts
            foreach (var WF in new[] { CHARTSymptomsByMfg })
            {
                if (WF != null) WF.Dispose();
            }

            // Dispose of WF hosts
            foreach (var WF in new[] { WFHostChartSymptomsByMfg })
            {
                if (WF != null) WF.Dispose();
            }
        }


        #endregion

        #region Button Press Methods





        #endregion

        #region Helper Methods


        private async void PopulateView()
        {
            try
            {
                // Remove user control
                SetUserCtrl(false);

                // Get all manufacturers
                var AllManufacturers = await InputRawData.LookupManufacturersAsync(VaccineToRestrictTo: InputVaccineName);

                // Insert wildcard for all
                AllManufacturers.Insert(0, ShowAll);

                // Set all available manufacturers and default to show all
                LBManufacturer.ItemsSource = AllManufacturers;
                LBManufacturer.SelectedIndex = 0;
            }
            finally
            {
                // Restore control
                SetUserCtrl(true);
            }
        }

        private List<KeyValuePair<string, double>> ConvertKvpListForChart(List<KeyValuePair<string, uint>> ListToConvert, Nullable<UInt16> MaxItemsToKeep = null)
        {
            // Filter down input list if requested
            if (MaxItemsToKeep != null && MaxItemsToKeep > 0)
            {
                while (ListToConvert.Count > MaxItemsToKeep)
                {
                    ListToConvert.RemoveAt(ListToConvert.Count - 1);
                }
            }

            // Create list to return
            var ListToReturn = new List<KeyValuePair<string, double>>();
            foreach (var PAIR in ListToConvert)
            {
                ListToReturn.Add(new KeyValuePair<string, double>(PAIR.Key, PAIR.Value));
            }

            // Return the list
            return ListToReturn;
        }

        private void SetUserCtrl(bool UserHasCtrl)
        {
            // Check if on main thread
            if (this.Dispatcher.CheckAccess() == false)
            {
                // If not, invoke main
                this.Dispatcher.Invoke(() => SetUserCtrl(UserHasCtrl));
            }
            else
            {
                // If on main, update control
                GRDMain.IsEnabled = UserHasCtrl;
            }
        }


        #endregion

        #region On Event Methods


        private async void LBManufacturer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                // Remove user control
                SetUserCtrl(false);

                // Clear downstream info first
                StaticMethods.WinForms.Charting.ClearChart(CHARTSymptomsByMfg);
                LBLMatchingRecordsHeader.Text = "Matching Records:";
                DGSymptomRecords.ItemsSource = null;
                LBLDeathCounts.Text = string.Empty;
                TBBuzzwordList.Text = "Loading...";

                // Bail if there's nothing selected
                if (LBManufacturer.SelectedIndex == -1) return;

                // If we have an item, save it
                string Manufacturer = LBManufacturer.SelectedItem.ToString();

                // If the *all* wildcard is selected, clear the filter
                if (Manufacturer == ShowAll) Manufacturer = string.Empty;

                // Pull back symptoms for selection
                var SymptomsAndCounts = await InputRawData.LookupSymptomsAndCountsAsync(VaccineToRestrictTo: InputVaccineName, ManufacturerToRestrictTo: Manufacturer);

                // Populate chart
                StaticMethods.WinForms.Charting.PopulateBarChart(CHARTSymptomsByMfg, ConvertKvpListForChart(SymptomsAndCounts, MaxItemsToKeep: 25), true, BarColor: System.Drawing.Color.Blue, ShowNumericGridLines: true);

                // Get the records to show in the grid
                var RecordsForGrid = await InputRawData.LookupRecordsForVaxAsync(InputVaccineName);

                // Check if filtering to manufacturer
                if (string.IsNullOrEmpty(Manufacturer) == false)
                {
                    // If so, filter
                    RecordsForGrid = (from AllInfo in RecordsForGrid where AllInfo.VaxMfg == Manufacturer select AllInfo).ToList();
                }

                // Show data in grid
                LBLMatchingRecordsHeader.Text = "Matching Records [ " + ThisApp.Methods.Common.AddCommasToNumber(RecordsForGrid.Count, 0) + " ]:";
                DGSymptomRecords.ItemsSource = RecordsForGrid;

                // Create vars to hold counts we're looking for
                uint CountIsDead = 0;
                uint CountDeathSymptom = 0;
                uint CountUniqueDead = 0;

                // Loop through and count
                foreach (var GRIDROW in RecordsForGrid)
                {
                    // Capture if row is marked as dead
                    bool IsDead = false;
                    if (GRIDROW.IsDead == "Y")
                    {
                        IsDead = true;
                    }

                    // Capture if death was a symptom
                    bool HasDeathSymptom = StaticMethods.Enumerables.DoesListContainItem(GRIDROW.Symptoms, "Death", true, false);

                    // Inc IsDead and death symptom flags
                    if (IsDead == true) CountIsDead += 1;
                    if (HasDeathSymptom == true) CountDeathSymptom += 1;

                    // Count unique rows with EITHER identifier
                    if (IsDead == true || HasDeathSymptom == true) CountUniqueDead += 1;
                }

                // Update labels
                LBLDeathCounts.Text = "Death Counts:  [ " + ThisApp.Methods.Common.AddCommasToNumber(CountIsDead, 0) + " ] IsDead | [ " + ThisApp.Methods.Common.AddCommasToNumber(CountDeathSymptom, 0) + " ] Death Symptom | Unique Deaths [ " + CountUniqueDead + " ]";

                // Create var to hold the min count we'll care about
                UInt16 MinCountWeCareAbout = 5;

                // Get the buzzwords
                var BuzzwordCounts = await InputRawData.LookupBuzzwordsAsync(MinCountWeCareAbout, VaccineToRestrictTo: InputVaccineName, ManufacturerToRestrictTo: Manufacturer);
                while (BuzzwordCounts.Count > 100)
                {
                    // Keep list relatively small
                    BuzzwordCounts.RemoveAt(BuzzwordCounts.Count - 1);
                }

                // Convert to markdown and populate TB
                TBBuzzwordList.Text = ThisApp.Methods.Common.KvpListToMarkdown(BuzzwordCounts, "Buzzword", "Count");
            }
            finally
            {
                // Restore control
                SetUserCtrl(true);
            }
        }


        #endregion

    }

}
