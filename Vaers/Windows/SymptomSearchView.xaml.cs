﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Vaers
{

    public partial class SymptomSearchView : Window
    {

        #region Static Methods


        public static SearchOptions RequestSearchOptions(IEnumerable<string> AllKnownManufacturers)
        {
            // Create new instance of window, show as dialog, and wait for result
            return new SymptomSearchView(AllKnownManufacturers).GetSearchOptions();
        }


        #endregion

        #region Sub Classes


        public class SearchOptions
        {
            public string SearchWord = string.Empty;
            public string RestrictToMfg = string.Empty;
        }


        #endregion

        #region Declarations


        // Var to hold search criteria to return
        private SearchOptions SearchCriteriaToReturn = null;


        #endregion

        #region Load & Unload Methods


        public SymptomSearchView(IEnumerable<string> AllKnownManufacturers)
        {
            // Draw the window
            InitializeComponent();

            // Wire up event handlers
            this.Loaded += SymptomSearchView_Loaded;
            BTNSearch.Click += BTNSearch_Click;
            TBSearchWord.TextChanged += TBSearchWord_TextChanged;

            // Create list to hold all known manufacturers
            var Manufacturers = new List<string>();

            // Add empty (i.e. search all) as default
            Manufacturers.Add(string.Empty);

            // If they passed in manufacturers, add to list
            if (AllKnownManufacturers != null)
            {
                Manufacturers.AddRange(AllKnownManufacturers);
            }

            // Add items to list
            CBManufactrer.ItemsSource = AllKnownManufacturers;

            // Ensure OK is disabled
            TBSearchWord_TextChanged(null, null);
        }

        private void SymptomSearchView_Loaded(object sender, RoutedEventArgs e)
        {
            // Set focus to search box
            TBSearchWord.Focus();
        }


        #endregion

        #region Button Press Methods


        private void BTNSearch_Click(object sender, RoutedEventArgs e)
        {
            // Save the current entries
            string SearchCriteria = TBSearchWord.Text.Trim();
            if (string.IsNullOrEmpty(SearchCriteria) == true)
            {
                // Bail if nothing entered
                return;
            }

            // If we've got search criteria, new up class to return
            SearchCriteriaToReturn = new SearchOptions();

            // Save the criteria
            SearchCriteriaToReturn.SearchWord = SearchCriteria;

            // Add manufacturer if there
            if (CBManufactrer.SelectedIndex != -1)
            {
                SearchCriteriaToReturn.RestrictToMfg = CBManufactrer.SelectedItem.ToString();
            }

            // Save dialog true to return info
            this.DialogResult = true;
        }


        #endregion

        #region Internal Methods


        private SearchOptions GetSearchOptions()
        {
            // Show window as dialog and wait for result
            if (this.ShowDialog() == true)
            {
                // Return selection if good
                return SearchCriteriaToReturn;
            }
            else
            {
                // Return nothing if user cancels
                return null;
            }
        }


        #endregion

        #region On Event Methods


        private void TBSearchWord_TextChanged(object sender, TextChangedEventArgs e)
        {
            // Set enable state of OK button based on if there's text
            BTNSearch.IsEnabled = !string.IsNullOrEmpty(TBSearchWord.Text.Trim());
        }


        #endregion

    }
}
