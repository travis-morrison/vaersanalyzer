﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace StaticMethods
{

    public struct Dates
    {

        public struct Convert
        {

            /// <summary>
            /// Used to convert a date into a large number that will sort correctly in YYYYMMDD[HHMMSS][mmm] format.
            /// </summary>
            /// <param name="DateToConvert">The date to convert to a number.</param>
            /// <param name="IncludeTime">Flag to indicate if the HHMMSS time should be included.</param>
            /// <param name="IncludeMilliSeconds">OPTIONAL:  Flag to indicate if the 3 digit milliseconds should be included (will be ignored if time is not included).</param>
            /// <returns>Returns a large 64 bit unsigned integer representing the date.</returns>
            public static UInt64 ToSortableNumber(DateTime DateToConvert, bool IncludeTime, bool IncludeMilliSeconds = false)
            {
                // First, get the number as a sortable string
                var SortableString = ToSortableString(DateToConvert, IncludeTime, IncludeMilliseconds: IncludeMilliSeconds);

                // Once we have the time, remove the hyphens
                SortableString = SortableString.Replace("-", string.Empty);

                try
                {
                    // Convert the string to a number and return
                    return System.Convert.ToUInt64(SortableString);
                }
                catch (Exception ex)
                {
                    // Return 0 if we aren't numeric (this really shouldn't be possible)
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return 0;
                }
            }
            /// <summary>
            /// Used to convert dates into sortable strings in YYYY-MM-DD[-HH-MM-SS][-mmm] format.
            /// </summary>
            /// <param name="DateToConvert">The date to convert to a sortable string.</param>
            /// <param name="IncludeTime">Flag to indicate if the HHMMSS time should be included.</param>
            /// <param name="IncludeMilliseconds">OPTIONAL:  Flag to indicate if the 3 digit milliseconds should be included (will be ignored if time is not included).</param>
            /// <returns>Returns a sortable string representation of the date.</returns>
            public static string ToSortableString(DateTime DateToConvert, bool IncludeTime, bool IncludeMilliseconds = false)
            {
                // Create the YYYY-MM-DD string to return
                string StringToReturn = DateToConvert.Year.ToString() + "-" + Helpers.PadNumberString(DateToConvert.Month.ToString(), 2) + "-" + Helpers.PadNumberString(DateToConvert.Day.ToString(), 2);

                // Check if appending time
                if (IncludeTime == true)
                {
                    // If so, add time components as well
                    StringToReturn += "-" + Helpers.PadNumberString(DateToConvert.Hour.ToString(), 2) + "-" + Helpers.PadNumberString(DateToConvert.Minute.ToString(), 2) + "-" + Helpers.PadNumberString(DateToConvert.Second.ToString(), 2);

                    // Append padded milliseconds if requested
                    if (IncludeMilliseconds == true)
                    {
                        StringToReturn += "-" + Helpers.PadNumberString(DateToConvert.Millisecond.ToString(), 3);
                    }
                }

                // Return the string
                return StringToReturn;
            }

            /// <summary>
            /// Used to convert a number date back to a date.
            /// </summary>
            /// <param name="DateAsSortableNumberToConvert">The date as a number to convert back to a date.</param>
            /// <returns>Returns the date if the conversion works. Returns NOTHING if the number can't be converted to a date.</returns>
            public static Nullable<DateTime> SortableNumberToDate(UInt64 DateAsSortableNumberToConvert)
            {
                // Use the string parser to handle it since it should work either way
                return SortableStringToDate(DateAsSortableNumberToConvert.ToString());
            }
            /// <summary>
            /// Used to convert a sortable date string back to a date.
            /// </summary>
            /// <param name="StringToConvert">The date string to convert back to a date.</param>
            /// <returns>Returns the date if the conversion works. Returns NOTHING if the string cannot be converted back.</returns>
            public static Nullable<DateTime> SortableStringToDate(string StringToConvert)
            {
                try
                {
                    // Return nothing if input is bad
                    if (string.IsNullOrEmpty(StringToConvert) == true) return null;

                    // If input is possibly good, create vars to hold the Y, M, D, h, m, s, mS values from the string
                    int Year = 0;
                    int Month = 0;
                    int Day = 0;
                    int Hour = 0;
                    int Minute = 0;
                    int Second = 0;
                    int Millisecond = 0;

                    // Save the possible / known formats that we have a good input
                    const string YYYYMMDDHHMMSSmmm = "YYYYMMDDHHMMSSmmm";
                    const string YYYYMMDDHHMMSS = "YYYYMMDDHHMMSS";
                    const string YYYYMMDD = "YYYYMMDD";

                    // Remove any delimiting chars that may be in string
                    StringToConvert = StringToConvert.Replace("-", string.Empty).Replace(":", string.Empty).Replace(".", string.Empty);

                    // Save the current length of the string
                    int RemainingLengthOfString = StringToConvert.Length;

                    // Return nothing if it's not a match
                    if ( new int[] { YYYYMMDDHHMMSSmmm.Length, YYYYMMDDHHMMSS.Length, YYYYMMDD.Length }.ToList().Contains(RemainingLengthOfString) == false)
                    {
                        return null;
                    }

                    // If it's one of the known lengths, get YYYY MM DD (those should always be there)
                    Year = System.Convert.ToInt32(StringToConvert.Substring(0, 4));
                    Month = System.Convert.ToInt32(StringToConvert.Substring(4, 2));
                    Day = System.Convert.ToInt32(StringToConvert.Substring(6, 2));

                    // Add HH:MM:SS if there
                    if (StringToConvert.Length >= YYYYMMDDHHMMSS.Length)
                    {
                        Hour = System.Convert.ToInt32(StringToConvert.Substring(8, 2));
                        Minute = System.Convert.ToInt32(StringToConvert.Substring(10, 2));
                        Second = System.Convert.ToInt32(StringToConvert.Substring(12, 2));
                    }

                    // Add milliseconds if there
                    if (StringToConvert.Length >= YYYYMMDDHHMMSSmmm.Length)
                    {
                        Millisecond = System.Convert.ToInt32(StringToConvert.Substring(14, 3));
                    }

                    // Once we've got everything, return date
                    return new System.DateTime(Year, Month, Day, Hour, Minute, Second, Millisecond);
                }
                catch (Exception ex)
                {
                    // Use nothing if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }

            /// <summary>
            /// Used to convert a specified date object to Central Standard Time.
            /// </summary>
            /// <param name="DateToConvert">The date object to convert to Central Standard Time.</param>
            /// <param name="IsInputDateAlreadyInUtc">Flag to hold if the input date has already been converted to UTC.</param>
            /// <returns>Returns the date/time adjusted to Central Standard Time.</returns>
            public static Nullable<DateTime> ToCentralStandardTime(DateTime DateToConvert, bool IsInputDateAlreadyInUtc)
            {
                // Use underlying method to perform conversion
                return ToNewTimeZone(DateToConvert, IsInputDateAlreadyInUtc, "Central Standard Time");
            }
            /// <summary>
            /// Used to convert a specified date object to a time zone of your choosing.
            /// </summary>
            /// <param name="DateToConvert">The date to object to convert to a new time zone to convert it to (names can be retrieved from 'Lookup.AllTimeZoneNameStrings()' method).</param>
            /// <param name="IsInputDateAlreadyInUtc">Flag to hold if the input date has already been converted to UTC.</param>
            /// <param name="TimeZoneStringName">The string name of the time zone to convert it to</param>
            /// <returns>Returns a new date adjusted to the specified time zone if successful. Returns NOTHING in all other conditions.</returns>
            public static Nullable<DateTime> ToNewTimeZone(DateTime DateToConvert, bool IsInputDateAlreadyInUtc, string TimeZoneStringName)
            {
                try
                {
                    // Return nothing if input is bad
                    if (string.IsNullOrEmpty(TimeZoneStringName) == true) return null;

                    // Return nothing if name isn't valid
                    if (Lookup.AllTimeZoneNameStrings().Contains(TimeZoneStringName) == false) return null;

                    // Create var to hold UTC date
                    var UTCDate = DateToConvert;

                    // Convert to UTC if the input isn't already UTC
                    if (IsInputDateAlreadyInUtc == false)
                    {
                        UTCDate = UTCDate.ToUniversalTime();
                    }

                    // Attempt to convert and return the date
                    return System.TimeZoneInfo.ConvertTimeFromUtc(UTCDate, System.TimeZoneInfo.FindSystemTimeZoneById(TimeZoneStringName));
                }
                catch (Exception ex)
                {
                    // Return nothing if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }

            /// <summary>
            /// Converts a UNIX second count to a standard date time value in the current time zone.
            /// </summary>
            /// <param name="UnixSecondsCount">The number of seconds that have elapsed since 1/1/1970.</param>
            /// <returns>Returns the converted date (in the current time zone) for the specified number of seconds. Returns NOTHING if an error occurs during conversion.</returns>
            public static Nullable<DateTime> UnixTimeSecondsToDateTime(double UnixSecondsCount)
            {
                try
                {
                    // Create base date for UNIX time without time zone
                    DateTime BaseUnixDate = Helpers.GetBaseUnixDateTime();

                    // Add the specified seconds
                    DateTime AdjustedWithSeconds = BaseUnixDate.AddSeconds(UnixSecondsCount);

                    // Convert to local time and return
                    return AdjustedWithSeconds.ToLocalTime();
                }
                catch (Exception ex)
                {
                    // If there's an overflow, return nothing 
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }
            /// <summary>
            /// Converts a specified date time value (in the current time zone) to the number of seconds since 1/1/1970 in Unix time.
            /// </summary>
            /// <param name="DateTimeToConvert">The date and time in the current time zone to convert.</param>
            /// <returns>Returns the number of seconds since 1/1/1970 in Unix time.</returns>
            public static double ToUnixTimeSeconds(DateTime DateTimeToConvert)
            {
                // Convert the date to utc / mountain time
                DateTime DateAsUTC = DateTimeToConvert.ToUniversalTime();

                // Create base date for UNIX time without time zone
                DateTime BaseUnixDate = Helpers.GetBaseUnixDateTime();

                // Return the number of seconds between the date and 1970
                return System.Math.Round(System.Convert.ToDouble((DateAsUTC - BaseUnixDate).TotalSeconds), 0, MidpointRounding.AwayFromZero);
            }

            /// <summary>
            ///  Converts a OLE date object (generally from Excel) to date time value.
            ///  </summary>
            ///  <param name="ValueToConvert">The OLE object (generally a double) to convert.</param>
            ///  <returns>Returns the converted date value is successful. Returns NOTHING in all other conditions.</returns>
            public static Nullable<DateTime> FromOleAutomationDate(object ValueToConvert)
            {
                try
                {
                    // Return nothing if input is bad
                    if (ValueToConvert == null) return null;

                    // Return nothing if input isn't a number
                    if (Helpers.IsNumeric(ValueToConvert.ToString()) == false) return null;

                    // If it's possibly a date, attempt conversion and return result
                    return DateTime.FromOADate(System.Convert.ToDouble(ValueToConvert));
                }
                catch (Exception ex)
                {
                    // Return nothing if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }

            /// <summary>
            /// Used to convert a date object to the corresponding UTC time object / date.
            /// </summary>
            /// <param name="DateToConvert">The date object to convert to UTC time.</param>
            /// <returns>Returns the converted date/time in UTC.</returns>
            public static DateTime ToUtcTime(DateTime DateToConvert)
            {
                // Convert to UTC and return
                return DateToConvert.ToUniversalTime();
            }

        }

        public struct Transform
        {

            /// <summary>
            /// Takes a given date and sets the time component to the min value of the date (i.e. m/d/y h:m:s => m/d/y 12:00:00 AM).
            /// </summary>
            /// <param name="DateToUse">The date to rail.</param>
            public static DateTime RailToBeginningOfDay(DateTime DateToUse)
            {
                // Return the converted date
                return new DateTime(DateToUse.Year, DateToUse.Month, DateToUse.Day, 0, 0, 0, 0);
            }
            /// <summary>
            /// Takes a given date and sets the time component to the max value of the date (i.e. m/d/y h:m:s => m/d/y 11:59:59 PM).
            /// </summary>
            /// <param name="DateToUse">The date to rail.</param>
            public static DateTime RailToEndOfDay(DateTime DateToUse)
            {
                // Return the converted date
                return new DateTime(DateToUse.Year, DateToUse.Month, DateToUse.Day, 23, 59, 59, 999);
            }

            /// <summary>
            /// Takes a given date and sets the day and time components to the min values of the date (i.e. m/d/y h:m:s => m/1/y 12:00:00 AM).
            /// </summary>
            /// <param name="DateToUse">The date to rail.</param>
            public static DateTime RailToBeginningOfMonth(DateTime DateToUse)
            {
                // Return the converted date
                return new DateTime(DateToUse.Year, DateToUse.Month, 1, 0, 0, 0, 0);
            }
            /// <summary>
            /// Takes a given date and sets the day and time components to the max values of the date (i.e. m/d/y h:m:s => m/31/y 11:59:59 PM).
            /// </summary>
            /// <param name="DateToUse">The date to rail.</param>
            public static DateTime RailToEndOfMonth(DateTime DateToUse)
            {
                // Get first day of month
                DateTime FirstDayOfMonth = RailToBeginningOfMonth(DateToUse);

                // Inc by one month, dec by one second, and return
                return FirstDayOfMonth.AddMonths(1).AddMilliseconds(-1);
            }

            /// <summary>
            /// Takes a given date and sets the month, day, and time components to the min values of the date (i.e. m/d/y h:m:s => 1/1/y 12:00:00 AM).
            /// </summary>
            /// <param name="DateToUse">The date to rail.</param>
            public static DateTime RailToBeginningOfYear(DateTime DateToUse)
            {
                // Return the converted date
                return new DateTime(DateToUse.Year, 1, 1, 0, 0, 0, 0);
            }
            /// <summary>
            /// Takes a given date and sets the month, day, and time components to the max values of the date (i.e. m/d/y h:m:s => 12/31/y 11:59:59 PM).
            /// </summary>
            /// <param name="DateToUse">The date to rail.</param>
            public static DateTime RailToEndOfYear(DateTime DateToUse)
            {
                // Get first day of year
                DateTime FirstDayOfYear = RailToBeginningOfYear(DateToUse);

                // Inc by one year, dec by one second, and return
                return FirstDayOfYear.AddYears(1).AddMilliseconds(-1);
            }
        
        }

        public struct Lookup
        {

            /// <summary>
            /// Used to return the string name for a specified 1-based month number (i.e. months 1-12).
            /// </summary>
            /// <param name="MonthNumber">The 1-BASED month number (e.g. 1 = January, 2 = February, etc.).</param>
            /// <param name="AbbreviateName">OPTIONAL:  Flag to indicate if month name should be abbreviated (e.g. January = Jan, etc.).</param>
            /// <returns>Returns the string month name for a specified 1-12 month number. Returns an empty string if an invalid month is specified.</returns>
            public static string MonthName(int MonthNumber, bool AbbreviateName = false)
            {
                try
                {
                    // Return an empty string for an invalid number
                    if (MonthNumber <= 0 || MonthNumber >= 13) return string.Empty;

                    // Use built in function to return name (key off of abbreviation request)
                    if (AbbreviateName == true)
                    {
                        // Get abbreviated name
                        return System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(MonthNumber);
                    }
                    else
                    {
                        // Get full name
                        return System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(MonthNumber);
                    }
                }
                catch (Exception ex)
                {
                    // Return empty string for error
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return string.Empty;
                }
            }

            /// <summary>
            /// Used to determine how many weeks into the year a specified dates falls on.
            /// </summary>
            /// <param name="DateToCheck">The date to check.</param>
            /// <param name="FirstDayOfWeek">The day the week starts on (generally Sunday or Monday).</param>
            /// <param name="FirstWeekStartRule">Designates the criteria to use when selecting the first week of the year (i.e. week one equals the first partial or full week of the calendar year).</param>
            /// <returns>Returns the how many weeks into the year the specified date falls on.</returns>
            public static int WeekNumber(DateTime DateToCheck, DayOfWeek FirstDayOfWeek, System.Globalization.CalendarWeekRule FirstWeekStartRule)
            {
                // Use current culture to get week number info
                return System.Globalization.CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(DateToCheck, FirstWeekStartRule, FirstDayOfWeek);
            }

            /// <summary>
            /// Used to get the current time from Microsoft.com (i.e. in case different PCs have different times set on their respective clocks).
            /// </summary>
            /// <param name="TimeoutInMS">OPTIONAL:  The amount of time in milliseconds to wait before the web request times out.</param>
            /// <returns>Returns the online time from Microsoft.com if found / accessible. Returns NOTHING in all other conditions.</returns>
            public static Nullable<DateTime> DateTimeOnline(int TimeoutInMS = 1000)
            {
                try
                {
                    // Create the web request
                    var Request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create("http://www.microsoft.com");

                    // Adjust timeout if not valid and set it
                    if (TimeoutInMS <= 0) TimeoutInMS = 100;
                    Request.Timeout = TimeoutInMS;

                    // Get the response
                    var Response = Request.GetResponse();

                    // Get the date string from the header
                    string DateString = Response.Headers["date"];

                    // Parse and return the date
                    return DateTime.ParseExact(DateString, "ddd, dd MMM yyyy HH:mm:ss 'GMT'", System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat, System.Globalization.DateTimeStyles.AssumeUniversal);
                }
                catch (Exception ex)
                {
                    // Return nothing if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }

            /// <summary>
            /// Used to lookup all known time zone string names.
            /// </summary>
            /// <returns>Returns a list of all known time zones.</returns>
            public static List<string> AllTimeZoneNameStrings()
            {
                // Create list to return
                var AllTimeZoneNames = new List<string>();

                // Loop through and get all
                foreach (var TZ in System.TimeZoneInfo.GetSystemTimeZones())
                {
                    AllTimeZoneNames.Add(TZ.Id);
                }

                // Sort by name
                AllTimeZoneNames.Sort();

                // Return the names
                return AllTimeZoneNames;
            }
        
        }

        private struct Helpers
        {

            /// <summary>
            /// Helper to pad number strings with leading zeros.
            /// </summary>
            public static string PadNumberString(string OriginalNumberString, uint NumberOfCharsToPadTo)
            {
                // Create the string to return
                string StringToReturn = OriginalNumberString;

                // Keep padding w/ 0's until length is big enough
                while (StringToReturn.Length < NumberOfCharsToPadTo)
                {
                    StringToReturn = "0" + StringToReturn;
                }

                // If the input string was too long, crop it down (should never happen unless user / caller screws up)
                if (StringToReturn.Length > NumberOfCharsToPadTo)
                {
                    StringToReturn = StringToReturn.Substring(System.Convert.ToInt32(StringToReturn.Length - NumberOfCharsToPadTo));
                }

                // Return the string
                return StringToReturn;
            }

            /// <summary>
            /// Helper to check if a string is numeric.
            /// </summary>
            public static bool IsNumeric(string StringToCheck)
            {
                // Create dummy var to hold result
                double ConvertedNumber = 0;

                // Try to parse and save result
                return double.TryParse(StringToCheck, out ConvertedNumber);
            }

            /// <summary>
            /// Helper to return the start year (1/1/1970) for all Unix time counts.
            /// </summary>
            public static DateTime GetBaseUnixDateTime()
            {
                // Base UNIX year is 1/1/1970 utc (mountain time)
                return new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            }
        
        }

    }

}
