﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StaticMethods
{

    public struct Binary
    {

        public struct Convert
        {

            public struct Bits
            {

                /// <summary>
                /// Used to convert a byte into an 8 bit binary string.
                /// </summary>
                /// <param name="ByteToConvert">The byte to convert to a binary string.</param>
                /// <returns>Returns an 8 character string of 1's and 0's for the binary representation of the byte.</returns>
                public static string ToBinaryString(byte ByteToConvert)
                {
                    // Convert byte to binary string (pad left to ensure 8 bits) and return the string
                    return Helpers.PadBinaryNumberStringsToFixedLength(System.Convert.ToString(ByteToConvert, 2), 8);
                }
                /// <summary>
                /// Used to convert a number into a 32 bit binary string.
                /// </summary>
                /// <param name="NumberToConvert">The number to convert to binary.</param>
                /// <returns>Returns a 32 character string of 1's and 0's for the binary representation of the number.</returns>
                public static string ToBinaryString(int NumberToConvert)
                {
                    try
                    {
                        // Get the number as binary
                        string BinaryString = System.Convert.ToString(NumberToConvert, 2);

                        // Once number is found, ensure it is padded to 32 bit (i.e. should be 8 bits per byte but leading 0's are cut off by default)
                        BinaryString = Helpers.PadBinaryNumberStringsToFixedLength(BinaryString, 32);

                        // Once padded so it's in 8 bit bytes, return it
                        return BinaryString;
                    }
                    catch (Exception ex)
                    {
                        // Return empty string if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return string.Empty;
                    }
                }
                /// <summary>
                /// Used to convert a number into a 32 bit binary string.
                /// </summary>
                /// <param name="NumberToConvert">The number to convert to binary.</param>
                /// <returns>Returns a 32 character string of 1's and 0's for the binary representation of the number.</returns>
                public static string ToBinaryString(UInt32 NumberToConvert)
                {
                    // Get the 64 bit string
                    string BinaryStringForInt64 = ToBinaryString(System.Convert.ToUInt64(NumberToConvert));

                    // Return empty string if length is off
                    if (BinaryStringForInt64.Length != 64)
                    {
                        return string.Empty;
                    }

                    // If length is good, get the 32 bit portion and return
                    return BinaryStringForInt64.Substring(32);
                }
                /// <summary>
                /// Used to convert a number into a 64 bit binary string.
                /// </summary>
                /// <param name="NumberToConvert">The number to convert to binary.</param>
                /// <returns>Returns a 64 character string of 1's and 0's for the binary representation of the number.</returns>
                public static string ToBinaryString(UInt64 NumberToConvert)
                {
                    try
                    {
                        // Get the bytes since direct conversion doesn't work with large uint32 / uint64
                        var NumberBytes = Binary.Convert.Bytes.ToBytes(NumberToConvert, Bytes.IntegerOptions.Uint64, true);

                        // Return empty string if nothing comes back
                        if (NumberBytes == null || NumberBytes.Count == 0)
                        {
                            return string.Empty;
                        }

                        // If bytes come back, create string to return
                        string StringToReturn = string.Empty;

                        // Loop through and convert chunks to binary
                        foreach (var BITE in NumberBytes)
                        {
                            StringToReturn += Binary.Convert.Bits.ToBinaryString(BITE);
                        }

                        // Return an empty string if length is off
                        if (StringToReturn.Length != 64)
                        {
                            return string.Empty;
                        }

                        // If good, return the string
                        return StringToReturn;
                    }
                    catch (Exception ex)
                    {
                        // Return an empty string if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return string.Empty;
                    }
                }

                /// <summary>
                /// Used to convert an 8 character / 8 bit binary string (e.g. "01001110") to a byte.
                /// </summary>
                /// <param name="BinaryStringToConvert">The 8 digit binary string to convert.</param>
                /// <returns>Returns the converted value if successful. Returns NOTHING if the conversion fails.</returns>
                public static Nullable<byte> ToByte(string BinaryStringToConvert)
                {
                    try
                    {
                        // Use underlying method to get numeric value
                        var AsUInt = ToUInt64(BinaryStringToConvert);
                        if (AsUInt == null)
                        {
                            // Return nothing if conversion fails
                            return null;
                        }

                        // If we get something back, return nothing if it's too big to fit into this type
                        if (AsUInt.Value > byte.MaxValue) return null;

                        // If we're good, convert and return
                        return System.Convert.ToByte(AsUInt.Value);
                    }
                    catch (Exception ex)
                    {
                        // Return nothing if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return null;
                    }
                }
                /// <summary>
                /// Used to convert an 16 character / 16 bit binary string to an unsigned 16-bit integer.
                /// </summary>
                /// <param name="BinaryStringToConvert">The 16 digit binary string to convert.</param>
                /// <returns>Returns the converted value if successful. Returns NOTHING if the conversion fails.</returns>
                public static Nullable<UInt16> ToUInt16(string BinaryStringToConvert)
                {
                    try
                    {
                        // Use underlying method to get numeric value
                        var AsUInt = ToUInt64(BinaryStringToConvert);
                        if (AsUInt == null)
                        {
                            // Return nothing if conversion fails
                            return null;
                        }

                        // If we get something back, return nothing if it's too big to fit into this type
                        if (AsUInt.Value > UInt16.MaxValue) return null;

                        // If we're good, convert and return
                        return System.Convert.ToUInt16(AsUInt.Value);
                    }
                    catch (Exception ex)
                    {
                        // Return nothing if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return null;
                    }
                }
                /// <summary>
                /// Used to convert an 32 character / 32 bit binary string to an unsigned 32-bit integer.
                /// </summary>
                /// <param name="BinaryStringToConvert">The 32 digit binary string to convert.</param>
                /// <returns>Returns the converted value if successful. Returns NOTHING if the conversion fails.</returns>
                public static Nullable<UInt32> ToUInt32(string BinaryStringToConvert)
                {
                    try
                    {
                        // Use underlying method to get numeric value
                        var AsUInt = ToUInt64(BinaryStringToConvert);
                        if (AsUInt == null)
                        {
                            // Return nothing if conversion fails
                            return null;
                        }

                        // If we get something back, return nothing if it's too big to fit into this type
                        if (AsUInt.Value > UInt32.MaxValue) return null;

                        // If we're good, convert and return
                        return System.Convert.ToUInt32(AsUInt.Value);
                    }
                    catch (Exception ex)
                    {
                        // Return nothing if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return null;
                    }
                }
                /// <summary>
                /// Used to convert an 64 character / 64 bit binary string to an unsigned 64-bit integer.
                /// </summary>
                /// <param name="BinaryStringToConvert">The 64 digit binary string to convert.</param>
                /// <returns>Returns the converted value if successful. Returns NOTHING if the conversion fails.</returns>
                public static Nullable<UInt64> ToUInt64(string BinaryStringToConvert)
                {
                    // Create constant to hold the expected length of the binary string
                    const UInt16 ExpectedBitLength = 64;

                    // Return nothing if the string is empty or too long
                    if (string.IsNullOrEmpty(BinaryStringToConvert) == true || BinaryStringToConvert.Length > ExpectedBitLength)
                    {
                        return null;
                    }

                    try
                    {
                        // If string isn't empty or too long, ensure it is the correct length (doesn't really matter)
                        BinaryStringToConvert = Helpers.PadBinaryNumberStringsToFixedLength(BinaryStringToConvert, ExpectedBitLength);

                        // Attempt to convert the binary string back to a number
                        return System.Convert.ToUInt64(BinaryStringToConvert, 2);
                    }
                    catch (Exception ex)
                    {
                        // Return nothing if the string won't convert
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return null;
                    }
                }

                /// <summary>
                /// Used to convert an 16 character / 16 bit binary string to a signed 16-bit integer.
                /// </summary>
                /// <param name="BinaryStringToConvert">The 16 digit binary string to convert.</param>
                /// <returns>Returns the converted value if successful. Returns NOTHING if the conversion fails.</returns>
                public static Nullable<Int16> ToInt16(string BinaryStringToConvert)
                {
                    try
                    {
                        // Use underlying method to get numeric value
                        var AsUInt = ToInt64(BinaryStringToConvert);
                        if (AsUInt == null)
                        {
                            // Return nothing if conversion fails
                            return null;
                        }

                        // If we get something back, return nothing if it's too big to fit into this type
                        if (AsUInt.Value > Int16.MaxValue) return null;

                        // If we're good, convert and return
                        return System.Convert.ToInt16(AsUInt.Value);
                    }
                    catch (Exception ex)
                    {
                        // Return nothing if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return null;
                    }
                }
                /// <summary>
                /// Used to convert an 32 character / 32 bit binary string to a signed 32-bit integer.
                /// </summary>
                /// <param name="BinaryStringToConvert">The 32 digit binary string to convert.</param>
                /// <returns>Returns the converted value if successful. Returns NOTHING if the conversion fails.</returns>
                public static Nullable<Int32> ToInt32(string BinaryStringToConvert)
                {
                    try
                    {
                        // Use underlying method to get numeric value
                        var AsUInt = ToInt64(BinaryStringToConvert);
                        if (AsUInt == null)
                        {
                            // Return nothing if conversion fails
                            return null;
                        }

                        // If we get something back, return nothing if it's too big to fit into this type
                        if (AsUInt.Value > Int32.MaxValue) return null;

                        // If we're good, convert and return
                        return System.Convert.ToInt32(AsUInt.Value);
                    }
                    catch (Exception ex)
                    {
                        // Return nothing if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return null;
                    }
                }
                /// <summary>
                /// Used to convert an 64 character / 64 bit binary string to a signed 64-bit integer.
                /// </summary>
                /// <param name="BinaryStringToConvert">The 64 digit binary string to convert.</param>
                /// <returns>Returns the converted value if successful. Returns NOTHING if the conversion fails.</returns>
                public static Nullable<Int64> ToInt64(string BinaryStringToConvert)
                {
                    // Create constant to hold the expected length of the binary string
                    const UInt16 ExpectedBitLength = 64;

                    // Return nothing if the string is empty or too long
                    if (string.IsNullOrEmpty(BinaryStringToConvert) == true || BinaryStringToConvert.Length > ExpectedBitLength)
                    {
                        return null;
                    }

                    try
                    {
                        // If string isn't empty or too long, ensure it is the correct length (doesn't really matter)
                        BinaryStringToConvert = Helpers.PadBinaryNumberStringsToFixedLength(BinaryStringToConvert, ExpectedBitLength);

                        // Attempt to convert the binary string back to a number
                        return System.Convert.ToInt64(BinaryStringToConvert, 2);
                    }
                    catch (Exception ex)
                    {
                        // Return nothing if the string won't convert
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return null;
                    }
                }

            }

            public struct Bytes
            {

                /// <summary>
                /// Enum to specify the integer type to convert to.
                /// </summary>
                public enum IntegerOptions
                {
                    Int32,
                    Int64,
                    UInt32,
                    Uint64
                }

                /// <summary>
                /// Used to convert an integer into a list of bytes.
                /// </summary>
                /// <param name="NumberToConvert">The integer to convert to bytes.</param>
                /// <param name="TypeToConvertTo">Used to specify the type of integer to convert to.</param>
                /// <param name="OrderBytesMSBToLSB">Flag to indicate if the bytes should be big endian (MSB first) or little endian (LSB first).</param>
                /// <returns>Returns a list of bytes if the conversion works. Returns NOTHING in all other conditions.</returns>
                public static List<byte> ToBytes(double NumberToConvert, IntegerOptions TypeToConvertTo, bool OrderBytesMSBToLSB)
                {
                    try
                    {
                        // Create list to hold bytes to return
                        List<byte> BytesToReturn = null;

                        // Check the type that is requested for the conversion
                        switch (TypeToConvertTo)
                        {
                            case IntegerOptions.Int32:
                                {
                                    // Convert var to specified type and get bytes
                                    BytesToReturn = BitConverter.GetBytes(System.Convert.ToInt32(NumberToConvert)).ToList();
                                    break;
                                }

                            case IntegerOptions.UInt32:
                                {
                                    // Convert var to specified type and get bytes
                                    BytesToReturn = BitConverter.GetBytes(System.Convert.ToUInt32(NumberToConvert)).ToList();
                                    break;
                                }

                            case IntegerOptions.Int64:
                                {
                                    // Convert var to specified type and get bytes
                                    BytesToReturn = BitConverter.GetBytes(System.Convert.ToInt64(NumberToConvert)).ToList();
                                    break;
                                }

                            case IntegerOptions.Uint64:
                                {
                                    // Convert var to specified type and get bytes
                                    BytesToReturn = BitConverter.GetBytes(System.Convert.ToUInt64(NumberToConvert)).ToList();
                                    break;
                                }

                            default:
                                {
                                    // Throw error for unknown type
                                    throw new Exception("Unimplemented integer type requested in byte conversion!");
                                }
                        }

                        // Order based on user selection
                        if (OrderBytesMSBToLSB == true && BitConverter.IsLittleEndian)
                        {
                            BytesToReturn.Reverse();
                        }

                        // Return the bytes
                        return BytesToReturn;
                    }
                    catch (Exception ex)
                    {
                        // Return nothing if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return null;
                    }
                }

                /// <summary>
                /// Used to convert an input stream into a list of bytes.
                /// </summary>
                /// <param name="StreamToConvert">The input stream to convert to bytes.</param>
                /// <param name="DisposeOfInputStreamAfterConversion">OPTIONAL:  Flag to indicate if the input stream should be disposed of before the method returns.</param>
                /// <returns>Return the input stream as a list of bytes if successful. Returns NOTHING if the conversion fails.</returns>
                public static List<byte> ToBytes(System.IO.Stream StreamToConvert, bool DisposeOfInputStreamAfterConversion = true)
                {
                    // Use common method and return result
                    return Stream.ToBytes(StreamToConvert, DisposeOfInputStreamAfterConversion: DisposeOfInputStreamAfterConversion);
                }

                /// <summary>
                /// Used to convert to 8-bit bytes (nibbles) to a 16-bit integer.
                /// </summary>
                /// <param name="MostSignificantByte">The byte that represents the larger part of the number (i.e. '25'10).</param>
                /// <param name="LeastSignificantByte">The byte that represents the smaller part of the number (i.e. 25'10').</param>
                /// <returns>Returns a 16-bit integer that represents the combined bytes if the conversion works. Returns NOTHING if the conversion fails.</returns>
                public static Nullable<Int16> ToInt16(byte MostSignificantByte, byte LeastSignificantByte)
                {
                    try
                    {
                        // Create list to perform conversion
                        var ListToConvert = new List<byte>();

                        // Add big bytes first
                        ListToConvert.Add(MostSignificantByte);
                        ListToConvert.Add(LeastSignificantByte);

                        // Flip list if little endian (i.e. small bytes first)
                        if (BitConverter.IsLittleEndian == true)
                        {
                            ListToConvert.Reverse();
                        }

                        // Convert and return
                        return BitConverter.ToInt16(ListToConvert.ToArray(), 0);
                    }
                    catch (Exception ex)
                    {
                        // Return nothing if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return null;
                    }
                }

                /// <summary>
                /// Used to convert 4 8-bit bytes to a 32-bit integer.
                /// </summary>
                /// <param name="Byte1x___">The most significant byte.</param>
                /// <param name="Byte2_x__">The second largest byte.</param>
                /// <param name="Byte3__x_">The third largest byte.</param>
                /// <param name="Byte4___x">The least significant byte.</param>
                /// <returns>Returns a 32-bit integer if the conversion is successful. Returns NOTHING in all other conditions.</returns>
                public static Nullable<Int32> ToInt32(byte Byte1x___, byte Byte2_x__, byte Byte3__x_, byte Byte4___x)
                {
                    // Use common method and return result
                    return ToInt32(new byte[] { Byte1x___, Byte2_x__, Byte3__x_, Byte4___x }, true);
                }
                /// <summary>
                /// Used to convert a list of bytes 4 or less bytes to a 32-bit integer.
                /// </summary>
                /// <param name="BytesToConvert">The list of bytes to convert.</param>
                /// <param name="BytesAreOrderedMSBToLSB">Flag to indicate if the bytes are big endian (MSB first) or little endian (LSB first).</param>
                /// <returns>Returns a 32-bit integer if the conversion is successful. Returns NOTHING in all other conditions.</returns>
                public static Nullable<Int32> ToInt32(IEnumerable<byte> BytesToConvert, bool BytesAreOrderedMSBToLSB)
                {
                    try
                    {
                        // Use helper to get and return result
                        return System.Convert.ToInt32(ToIntHelper(BytesToConvert, BytesAreOrderedMSBToLSB, IntegerOptions.Int32));
                    }
                    catch (Exception ex)
                    {
                        // Return nothing if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return null;
                    }
                }

                /// <summary>
                /// Used to convert 4 8-bit bytes to a 32-bit unsigned integer.
                /// </summary>
                /// <param name="Byte1x___">The most significant byte.</param>
                /// <param name="Byte2_x__">The second largest byte.</param>
                /// <param name="Byte3__x_">The third largest byte.</param>
                /// <param name="Byte4___x">The least significant byte.</param>
                /// <returns>Returns a 32-bit unsigned integer if the conversion is successful. Returns NOTHING in all other conditions.</returns>
                public static Nullable<UInt32> ToInt32Unsigned(byte Byte1x___, byte Byte2_x__, byte Byte3__x_, byte Byte4___x)
                {
                    // Use common method and return result
                    return ToInt32Unsigned(new byte[] { Byte1x___, Byte2_x__, Byte3__x_, Byte4___x }, true);
                }
                /// <summary>
                /// Used to convert a list of bytes 4 or less bytes to a 32-bit unsigned integer.
                /// </summary>
                /// <param name="BytesToConvert">The list of bytes to convert.</param>
                /// <param name="BytesAreOrderedMSBToLSB">Flag to indicate if the bytes are big endian (MSB first) or little endian (LSB first).</param>
                /// <returns>Returns a 32-bit unsigned integer if the conversion is successful. Returns NOTHING in all other conditions.</returns>
                public static Nullable<UInt32> ToInt32Unsigned(IEnumerable<byte> BytesToConvert, bool BytesAreOrderedMSBToLSB)
                {
                    try
                    {
                        // Use helper to get and return result
                        return System.Convert.ToUInt32(ToIntHelper(BytesToConvert, BytesAreOrderedMSBToLSB, IntegerOptions.UInt32));
                    }
                    catch (Exception ex)
                    {
                        // Return nothing if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return null;
                    }
                }

                /// <summary>
                /// Used to convert 8 8-bit bytes to a 64-bit integer.
                /// </summary>
                /// <param name="Byte1x_______">The most significant byte.</param>
                /// <param name="Byte2_x______">The second largest byte.</param>
                /// <param name="Byte3__x_____">The third largest byte.</param>
                /// <param name="Byte4___x____">The fourth largest byte.</param>
                /// <param name="Byte5____x___">The fifth largest byte.</param>
                /// <param name="Byte6_____x__">The sixth largest byte.</param>
                /// <param name="Byte7______x_">The seventh largest byte.</param>
                /// <param name="Byte8_______x">The least significant byte.</param>
                /// <returns>Returns a 64-bit integer if the conversion is successful. Returns NOTHING in all other conditions.</returns>
                public static Nullable<Int64> ToInt64(byte Byte1x_______, byte Byte2_x______, byte Byte3__x_____, byte Byte4___x____, byte Byte5____x___, byte Byte6_____x__, byte Byte7______x_, byte Byte8_______x)
                {
                    try
                    {
                        // Create list to perform conversion
                        var ListToConvert = new List<byte>();

                        // Add big bytes first
                        ListToConvert.Add(Byte1x_______);
                        ListToConvert.Add(Byte2_x______);
                        ListToConvert.Add(Byte3__x_____);
                        ListToConvert.Add(Byte4___x____);
                        ListToConvert.Add(Byte5____x___);
                        ListToConvert.Add(Byte6_____x__);
                        ListToConvert.Add(Byte7______x_);
                        ListToConvert.Add(Byte8_______x);

                        // Flip list if little endian (i.e. small bytes first)
                        if (BitConverter.IsLittleEndian == true)
                        {
                            ListToConvert.Reverse();
                        }

                        // Convert and return
                        return BitConverter.ToInt64(ListToConvert.ToArray(), 0);
                    }
                    catch (Exception ex)
                    {
                        // Return nothing if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return null;
                    }
                }
                /// <summary>
                /// Used to convert a list of bytes 8 or less bytes to a 64-bit integer.
                /// </summary>
                /// <param name="BytesToConvert">The list of bytes to convert.</param>
                /// <param name="BytesAreOrderedMSBToLSB">Flag to indicate if the bytes are big endian (MSB first) or little endian (LSB first).</param>
                /// <returns>Returns a 64-bit integer if the conversion is successful. Returns NOTHING in all other conditions.</returns>
                public static Nullable<Int64> ToInt64(IEnumerable<byte> BytesToConvert, bool BytesAreOrderedMSBToLSB)
                {
                    try
                    {
                        // Use helper to get and return result
                        return System.Convert.ToInt64(ToIntHelper(BytesToConvert, BytesAreOrderedMSBToLSB, IntegerOptions.Int64));
                    }
                    catch (Exception ex)
                    {
                        // Return nothing if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return null;
                    }
                }

                /// <summary>
                /// Helper to convert list of bytes to an integer value.
                /// </summary>
                private static Nullable<Int64> ToIntHelper(IEnumerable<byte> BytesToConvert, bool BytesAreOrderedMSBToLSB, IntegerOptions IntTypeToConvertTo)
                {
                    try
                    {
                        // Create var to hold the number of bytes that need to be in the list to convert to the requested type
                        int BytesNeededInList = 4;

                        // Flip if int64 is requested (uint 32 also uses 4 bytes)
                        if (IntTypeToConvertTo == IntegerOptions.Int64)
                        {
                            BytesNeededInList = 8;
                        }

                        // Return nothing if count is too high
                        if (BytesToConvert.Count() > BytesNeededInList)
                        {
                            return null;
                        }

                        // If good, convert enumerable to list for ease of use
                        List<byte> ByteList = BytesToConvert.ToList();

                        // If count is within range, ensure list order matches the endian style of the PC / OS
                        if ((BytesAreOrderedMSBToLSB == true && BitConverter.IsLittleEndian == true) || (BytesAreOrderedMSBToLSB == false && BitConverter.IsLittleEndian == false))
                        {
                            // If list is in reverse order of what bit converter needs, flip it
                            ByteList.Reverse();
                        }

                        // Ensure bytes list is padded to correct length
                        while (ByteList.Count < BytesNeededInList)
                        {
                            // Now that list is sorted per the PC / OS, add 0's to beginning or end based on endianess of the machine
                            if (BitConverter.IsLittleEndian == true)
                            {
                                // For little endian, add bytes to end (leading 0's on MSBs)
                                ByteList.Add(0);
                            }
                            else
                            {
                                // For big endian, add bytes to beginning (leading 0's on MSBs)
                                ByteList.Insert(0, 0);
                            }
                        }

                        // Once done, check the int type that is being requested
                        switch (IntTypeToConvertTo)
                        {
                            case IntegerOptions.Int32:
                                // Attempt to convert to int32 and return
                                return BitConverter.ToInt32(ByteList.ToArray(), 0);
                            case IntegerOptions.UInt32:
                                // Attempt to convert to uint32 and return
                                return BitConverter.ToUInt32(ByteList.ToArray(), 0);
                            case IntegerOptions.Int64:
                                // Attempt to convert to int64 and return
                                return BitConverter.ToInt64(ByteList.ToArray(), 0);
                            default:
                                // Throw error for unknown type
                                throw new Exception("Unimplemented integer type requested for byte conversion!");
                        }
                    }
                    catch (Exception ex)
                    {
                        // Return nothing if conversion fails
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return null;
                    }
                }

            }

            public struct Hexadecimal
            {

                /// <summary>
                /// Used to convert a long integer to its string hexadecimal representation (e.g. "11" = "0x0B", etc.).
                /// </summary>
                /// <param name="NumberToConvert">The number to convert.</param>
                /// <returns>Returns a string representing the hexadecimal value of the number (e.g. "10" = "0x0A).</returns>
                public static string ToHexString(long NumberToConvert)
                {
                    try
                    {
                        // Get raw hex value of the number (e.g. 10 = A, etc.)
                        string HexValueString = NumberToConvert.ToString("X2");

                        // Pad with 0 if not an even length (i.e. 2 should be 02, etc.)
                        if (HexValueString.Length % 2 != 0)
                        {
                            HexValueString = "0" + HexValueString;
                        }

                        // Add the 0x prefix to indicate the value is hex and return the string
                        return "0x" + HexValueString;
                    }
                    catch (Exception ex)
                    {
                        // Return an empty string if an error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return string.Empty;
                    }
                }

                /// <summary>
                /// Used to convert a list of bytes to a corresponding list of their string hexadecimal representations.
                /// </summary>
                /// <param name="BytesToConvert">The list of bytes to convert.</param>
                /// <returns>Returns a list of strings that contains the hexadecimal string representations of the list of bytes. Returns NOTHING if inputs are invalid.</returns>
                public static List<string> ToHexStringList(IEnumerable<byte> BytesToConvert)
                {
                    // Return nothing if input is null
                    if (BytesToConvert == null)
                    {
                        return null;
                    }

                    // Create list to return
                    var ListToReturn = new List<string>();

                    // Return the empty list if there are no elements in the byte list
                    if (BytesToConvert.Count() == 0)
                    {
                        return ListToReturn;
                    }

                    // If there are bytes to convert, loop through and convert bytes
                    foreach (var BITE in BytesToConvert)
                    {
                        ListToReturn.Add(ToHexString(BITE));
                    }

                    // When conversion is complete, return the list
                    return ListToReturn;
                }

                /// <summary>
                /// Used to create a list of key value pairs (KVPs) mapping a list of bytes to their string hexadecimal representations.
                /// </summary>
                /// <param name="BytesToConvert">The list of bytes to create the list of KVPs from.</param>
                /// <returns>Returns a list of key value pairs where the keys are the input list of bytes to convert and the pairs are their corresponding hexadecimal string representations. Returns NOTHING if inputs are invalid.</returns>
                public static List<KeyValuePair<byte, string>> ToByteHexStringPairs(IEnumerable<byte> BytesToConvert)
                {
                    // Return nothing if input is null
                    if (BytesToConvert == null)
                    {
                        return null;
                    }

                    // Create the list of pairs to return
                    var PairsToReturn = new List<KeyValuePair<byte, string>>();

                    // Return empty list if there are no input values
                    if (BytesToConvert.Count() == 0)
                    {
                        return PairsToReturn;
                    }

                    // If inputs are good, loop through and make pairs
                    foreach (var BITE in BytesToConvert)
                    {
                        // Add new pair
                        PairsToReturn.Add(new KeyValuePair<byte, string>(BITE, ToHexString(BITE)));
                    }

                    // Return the list
                    return PairsToReturn;
                }

                /// <summary>
                /// Used to convert a hex string (0x1B, &amp;H3A0C, etc.) to a list of bytes. Note that the string MUST have an even number of characters (0xA=Bad, 0x0A=Good)!
                /// </summary>
                /// <param name="HexNumberInStringFormat">Any string representation of a hexadecimal number (0x1B, &amp;H3A0C, etc.).</param>
                /// <returns>Returns a list of bytes if the conversion is successful. Returns NOTHING if the conversion fails.</returns>
                public static List<byte> ToByteList(string HexNumberInStringFormat)
                {
                    try
                    {
                        // Remove any 0x / &h prefixes
                        HexNumberInStringFormat = HexNumberInStringFormat.ToUpper().Replace("0X", string.Empty).Replace("&H", string.Empty);

                        // Remove spaces
                        HexNumberInStringFormat = HexNumberInStringFormat.Replace(" ", string.Empty);

                        // Return nothing if there isn't an even number
                        if (HexNumberInStringFormat.Length % 2 != 0)
                        {
                            return null;
                        }

                        // If length is OK, create list to hold blocks of 2 chunks to convert
                        var StringPairs = new List<string>();

                        // Loop through and get pairs
                        for (int i = 0; i <= HexNumberInStringFormat.Length - 1; i += 2)
                        {
                            StringPairs.Add(HexNumberInStringFormat.Substring(i, 2));
                        }

                        // Once pairs are fetched, create list to hold bytes to return
                        var BytesToReturn = new List<byte>();

                        // Loop through and attempt conversions
                        foreach (var HEXSTRING in StringPairs)
                        {
                            // Try to get the byte
                            var PossibleByte = ToByte(HEXSTRING);

                            // Return nothing if any conversion fails
                            if (PossibleByte == null)
                            {
                                return null;
                            }

                            // If it works, add it to the list
                            BytesToReturn.Add(System.Convert.ToByte(PossibleByte));
                        }

                        // Return the list
                        return BytesToReturn;
                    }
                    catch (Exception ex)
                    {
                        // Return nothing if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return null;
                    }
                }

                /// <summary>
                /// Used to convert a list of hex strings (0x3A, 0x13, etc.) to a list of numeric bytes.
                /// </summary>
                /// <param name="ListOfHexStrings">The list of hex strings to convert.</param>
                /// <returns>Returns a list of bytes if the conversion is successful. Returns NOTHING in all other conditions.</returns>
                public static List<byte> ToByteList(List<string> ListOfHexStrings)
                {
                    try
                    {
                        // Return nothing if input is bad
                        if (ListOfHexStrings == null)
                        {
                            return null;
                        }

                        // Return empty list if nothing is passed in
                        if (ListOfHexStrings.Count == 0)
                        {
                            return new List<byte>();
                        }

                        // If input looks good, create list to hold info to return
                        var BytesToReturn = new List<byte>();

                        // Loop through and convert strings
                        foreach (var BITESTRING in ListOfHexStrings)
                        {
                            // Get the byte
                            var BITE = ToByte(BITESTRING);

                            // Return nothing if anything is bad
                            if (BITE == null)
                            {
                                return null;
                            }

                            // If good, convert and add to list
                            BytesToReturn.Add(System.Convert.ToByte(BITE));
                        }

                        // Return the list
                        return BytesToReturn;
                    }
                    catch (Exception ex)
                    {
                        // Return nothing if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return null;
                    }
                }

                /// <summary>
                /// Used to convert a hex string (0x1B, &amp;H3A0C, etc.) to a byte. Note that the BYTE PORTION of string MUST be no longer than two characters (0x3BCF=Bad, 0xA3=Good)!
                /// </summary>
                /// <param name="HexNumberInStringFormat">The string representation of the hex number to convert.</param>
                /// <returns>Returns the converted value if the conversion is successful. Returns NOTHING in all other conditions.</returns>
                public static Nullable<byte> ToByte(string HexNumberInStringFormat)
                {
                    try
                    {
                        // Try to use common method to get 64 bit version
                        var ValueAsInt64 = ToInt64(HexNumberInStringFormat);

                        // Return nothing if conversion fails
                        if (ValueAsInt64 == null)
                        {
                            return null;
                        }

                        // If the value converted, return nothing if out of range
                        if (ValueAsInt64 < byte.MinValue || ValueAsInt64 > byte.MaxValue)
                        {
                            return null;
                        }

                        // If value looks good, convert and return it
                        return System.Convert.ToByte(ValueAsInt64);
                    }
                    catch (Exception ex)
                    {
                        // Return nothing if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return null;
                    }
                }

                /// <summary>
                /// Used to convert a hex string (0x1B, &amp;H3A0C, etc.) to a 32-bit integer.
                /// </summary>
                /// <param name="HexNumberInStringFormat">The string representation of the hex number to convert.</param>
                /// <returns>Returns the converted value if the conversion is successful. Returns NOTHING in all other conditions.</returns>
                public static Nullable<Int32> ToInt32(string HexNumberInStringFormat)
                {
                    try
                    {
                        // Try to use common method to get 64 bit version
                        var ValueAsInt64 = ToInt64(HexNumberInStringFormat);

                        // Return nothing if conversion fails
                        if (ValueAsInt64 == null)
                        {
                            return null;
                        }

                        // If the value converted, return nothing if out of range
                        if (ValueAsInt64 < Int32.MinValue || ValueAsInt64 > Int32.MaxValue)
                        {
                            return null;
                        }

                        // If value looks good, convert and return it
                        return System.Convert.ToInt32(ValueAsInt64);
                    }
                    catch (Exception ex)
                    {
                        // Return nothing if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return null;
                    }
                }

                /// <summary>
                /// Used to convert a hex string (0x1B, &amp;H3A0C, etc.) to a 64-bit integer.
                /// </summary>
                /// <param name="HexNumberInStringFormat">The string representation of the hex number to convert.</param>
                /// <returns>Returns the converted value if the conversion is successful. Returns NOTHING in all other conditions.</returns>
                public static Nullable<Int64> ToInt64(string HexNumberInStringFormat)
                {
                    try
                    {
                        // Remove any 0x / &h prefixes
                        HexNumberInStringFormat = HexNumberInStringFormat.ToUpper().Replace("0X", string.Empty).Replace("&H", string.Empty);

                        // Append "0" if there's an odd number of chars
                        if (HexNumberInStringFormat.Length % 2 != 0)
                        {
                            HexNumberInStringFormat = "0" + HexNumberInStringFormat;
                        }

                        // Attempt to convert and return
                        return Int64.Parse(HexNumberInStringFormat, System.Globalization.NumberStyles.AllowHexSpecifier);
                    }
                    catch (Exception ex)
                    {
                        // Return nothing if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return null;
                    }
                }

                /// <summary>
                /// Used to convert a hex string (0x1B, &amp;H3A0C, etc.) to a 64-bit unsigned integer.
                /// </summary>
                /// <param name="HexNumberInStringFormat">The string representation of the hex number to convert.</param>
                /// <returns>Returns the converted value if the conversion is successful. Returns NOTHING in all other conditions.</returns>
                public static Nullable<UInt64> ToUInt64(string HexNumberInStringFormat)
                {
                    try
                    {
                        // Remove any 0x / &h prefixes
                        HexNumberInStringFormat = HexNumberInStringFormat.ToUpper().Replace("0X", string.Empty).Replace("&H", string.Empty);

                        // Append "0" if there's an odd number of chars
                        if (HexNumberInStringFormat.Length % 2 != 0)
                        {
                            HexNumberInStringFormat = "0" + HexNumberInStringFormat;
                        }

                        // Attempt to convert and return
                        return UInt64.Parse(HexNumberInStringFormat, System.Globalization.NumberStyles.AllowHexSpecifier);
                    }
                    catch (Exception ex)
                    {
                        // Return nothing if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return null;
                    }
                }

            }

            public struct BinaryCodedDecimal
            {

                /// <summary>
                /// Used to convert a positive 2 digit decimal number to a binary coded decimal (BCD) byte.
                /// </summary>
                /// <param name="NumberToConvert">The positive 2 digit decimal number to convert.</param>
                /// <returns>Returns a binary coded decimal byte representation of the decimal number if the conversion works. Returns nothing if the conversion fails.</returns>
                public static Nullable<byte> ToBCD(int NumberToConvert)
                {
                    try
                    {
                        // Return nothing if number is invalid
                        if (NumberToConvert < 0 || NumberToConvert > 99)
                        {
                            return null;
                        }

                        // Convert the number to a string
                        string StringNumber = NumberToConvert.ToString();

                        // Create list to hold all single digits
                        var ListOfDigits = new List<string>();

                        // Loop through and add each char to the list
                        for (int i = 0; i <= StringNumber.Length - 1; i++)
                        {
                            ListOfDigits.Add(StringNumber.Substring(i, 1));
                        }

                        // Create list to hold all individual / single digit number bytes
                        var ListOfBytes = new List<byte>();

                        // Loop through and add converted numbers
                        foreach (string NUMCHAR in ListOfDigits)
                        {
                            // Convert single digit char to byte and add to list
                            ListOfBytes.Add(System.Convert.ToByte(System.Convert.ToInt32(NUMCHAR)));
                        }

                        // Create list of strings to hold the full binary conversions of each byte
                        var ListOfBinaryStrings = new List<string>();

                        // Loop through and convert bytes to binary strings
                        foreach (byte BITE in ListOfBytes)
                        {
                            // Convert byte to binary string (pad w/ leading zeros if string is too short)
                            ListOfBinaryStrings.Add(Helpers.PadBinaryStringToSpecifiedLenghtMultiple(System.Convert.ToString(BITE, 2), 4));
                        }

                        // If there was only one digit passed in, insert "0000" for first half of byte
                        if (NumberToConvert <= 9)
                        {
                            ListOfBinaryStrings.Insert(0, "0000");
                        }

                        // Create var to hold master binary string to join the individual binary strings back together
                        string FullBinaryString = string.Empty;

                        // Concat the each set of 4 bits back together to create full byte string
                        foreach (string BINARY in ListOfBinaryStrings)
                        {
                            FullBinaryString += BINARY;
                        }

                        // Convert the 8 string bits to a byte and return it
                        return System.Convert.ToByte(FullBinaryString, 2);
                    }
                    catch (Exception ex)
                    {
                        // Return nothing if something fails
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return null;
                    }
                }

                /// <summary>
                /// Used to convert a binary coded decimal (BCD) byte to an integer.
                /// </summary>
                /// <param name="BCDByteToConvert">The binary coded decimal byte to convert to an integer.</param>
                /// <returns>Returns an integer representation of the binary coded decimal byte if the conversion works. Returns NOTHING if the conversion fails.</returns>
                public static Nullable<int> FromBCD(byte BCDByteToConvert)
                {
                    try
                    {
                        // Create string to hold the full binary representation of the byte
                        string FullBinaryString = Helpers.PadBinaryStringToSpecifiedLenghtMultiple(System.Convert.ToString(BCDByteToConvert, 2), 8);

                        // Split byte into sets of 4 bits and pad each back out to 8 bits with leading zeros
                        string BinaryDigit1 = Helpers.PadBinaryStringToSpecifiedLenghtMultiple(FullBinaryString.Substring(0, 4), 8);
                        string BinaryDigit2 = Helpers.PadBinaryStringToSpecifiedLenghtMultiple(FullBinaryString.Substring(4, 4), 8);

                        // Convert each padded binary string back to a byte
                        byte ByteDigit1 = System.Convert.ToByte(BinaryDigit1, 2);
                        byte ByteDigit2 = System.Convert.ToByte(BinaryDigit2, 2);

                        // Create the full number string by converting each digit to decimal and concating together
                        string FullDecimalNumberString = System.Convert.ToInt32(ByteDigit1).ToString() + System.Convert.ToInt32(ByteDigit2).ToString();

                        // Convert the string to an int and return the value
                        return System.Convert.ToInt32(FullDecimalNumberString);
                    }
                    catch (Exception ex)
                    {
                        // Return nothing if the conversion fails
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return null;
                    }
                }

            }

            public struct Stream
            {

                /// <summary>
                /// Used to convert an input stream into a list of bytes.
                /// </summary>
                /// <param name="StreamToConvert">The input stream to convert to bytes.</param>
                /// <param name="DisposeOfInputStreamAfterConversion">OPTIONAL:  Flag to indicate if the input stream should be disposed of before the method returns.</param>
                /// <returns>Return the input stream as a list of bytes if successful. Returns NOTHING if the conversion fails.</returns>
                public static List<byte> ToBytes(System.IO.Stream StreamToConvert, bool DisposeOfInputStreamAfterConversion = true)
                {
                    try
                    {
                        // Return nothing if input is bad
                        if (StreamToConvert == null)
                        {
                            return null;
                        }

                        // If good, create list to hold bytes
                        var BytesToReturn = new List<byte>();

                        // If good, attempt conversion
                        using (System.IO.MemoryStream MemStream = new System.IO.MemoryStream())
                        {
                            // Copy in source stream
                            StreamToConvert.CopyTo(MemStream);

                            // Extract bytes
                            BytesToReturn.AddRange(MemStream.ToArray());
                        }

                        // Dispose of source stream if requested
                        if (DisposeOfInputStreamAfterConversion == true)
                        {
                            StreamToConvert.Dispose();
                        }

                        // Return the bytes
                        return BytesToReturn;
                    }
                    catch (Exception ex)
                    {
                        // Return nothing if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return null;
                    }
                }

            }

            public struct VarIntBase128
            {

                /// <summary>
                /// Output class hold information for a parsed byte stream that was prefixed with a VarIntBase128 count.
                /// </summary>
                public class DecodedByteStream
                {
                    // Vars to hold info
                    public int ExpectedDataByteCountFromPrefixBytes = -1;
                    public List<byte> PrefixCountBytes = null;
                    public List<byte> DataBytes = null;

                    // Property to check if the data byte count matches the expected count from the length prefix
                    public bool IsCountCorrect
                    {
                        get
                        {
                            // Return false if not loaded yet
                            if (ExpectedDataByteCountFromPrefixBytes == -1 || DataBytes == null)
                            {
                                return false;
                            }

                            // If loaded, check if counts match and return result
                            if (ExpectedDataByteCountFromPrefixBytes == DataBytes.Count)
                            {
                                // Return true if count is good
                                return true;
                            }
                            else
                            {
                                // Return false if counts are different
                                return false;
                            }
                        }
                    }
                }

                /// <summary>
                /// Used to extract the VarIntBase128 prefix bytes from a full byte stream and returned the parsed prefix and data bytes.
                /// </summary>
                /// <param name="FullByteStreamWithEncodedLenghtPrefix">The full byte stream with VarIntBase128 length prefix bytes appended to the beginning.</param>
                /// <returns>Returns an instance of DecodedByteStream with all parsed information if the decoding is successful. Returns NOTHING in all other conditions.</returns>
                public static DecodedByteStream DecodeLengthPrefixedByteStream(IEnumerable<byte> FullByteStreamWithEncodedLenghtPrefix)
                {
                    try
                    {
                        // Return nothing input is bad
                        if (FullByteStreamWithEncodedLenghtPrefix == null || FullByteStreamWithEncodedLenghtPrefix.Count() == 0)
                        {
                            return null;
                        }

                        // If data look like it can be decoded, create list to hold all binary strings
                        var BinaryStringList = new List<string>();

                        // Loop through and convert all bytes to binary strings
                        foreach (var BITE in FullByteStreamWithEncodedLenghtPrefix)
                        {
                            BinaryStringList.Add(Bits.ToBinaryString(BITE));
                        }

                        // Create pointer to hold where the actual data starts
                        int DataStartsAt = -1;

                        // Loop through all binary strings and look for the first one that starts with a "0" (encoded count prefix bytes will all start with "1" until the last count byte)
                        for (int i = 0; i <= BinaryStringList.Count - 1; i++)
                        {
                            // Skip to next if first bit is "1"
                            if (BinaryStringList[i].Substring(0, 1) != "0")
                            {
                                continue;
                            }

                            // Once found, the data will start at the NEXT index, save that location
                            DataStartsAt = i + 1;

                            // Bail out of loop
                            break;
                        }

                        // Return nothing if data start could not be determined
                        if (DataStartsAt == -1) return null;

                        // Return nothing if the data start index is not valid (i.e. no data attached)
                        if (DataStartsAt > BinaryStringList.Count - 1) return null;

                        // Convert enumerable list to array for ease of working
                        var FullByteStreamWithEncodedLenghtPrefixAsArray = FullByteStreamWithEncodedLenghtPrefix.ToArray();

                        // If everything is parsable, add the count bytes to a list
                        var EncodedCountBytes = new List<byte>();
                        for (int i = 0; i <= DataStartsAt - 1; i++)
                        {
                            EncodedCountBytes.Add(FullByteStreamWithEncodedLenghtPrefixAsArray[i]);
                        }

                        // Add the data bytes to a second list
                        var DataBytes = new List<byte>();
                        for (int i = DataStartsAt; i <= BinaryStringList.Count - 1; i++)
                        {
                            DataBytes.Add(FullByteStreamWithEncodedLenghtPrefixAsArray[i]);
                        }

                        // Decode the expected count
                        var ExpectedLength = ToInteger(EncodedCountBytes);

                        // Create class to return
                        var DecodedInfoToReturn = new DecodedByteStream();

                        // Xfer byte lists
                        DecodedInfoToReturn.PrefixCountBytes = EncodedCountBytes;
                        DecodedInfoToReturn.DataBytes = DataBytes;

                        // Add the decoded expected count IF it decoded successfully
                        if (ExpectedLength != null)
                        {
                            DecodedInfoToReturn.ExpectedDataByteCountFromPrefixBytes = System.Convert.ToInt32(ExpectedLength);
                        }

                        // Return the info
                        return DecodedInfoToReturn;
                    }
                    catch (Exception ex)
                    {
                        // Return nothing if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return null;
                    }
                }

                /// <summary>
                /// Used to encode a source list of bytes with one or more VarIntBase128 length prefix bytes.
                /// </summary>
                /// <param name="UnencodedByteStream">The unencoded byte stream / source data to add the length prefix to.</param>
                /// <returns>Returns the source list of bytes with one or more VarInt length prefix bytes appended to the beginning of the byte stream if the encoding is successful. Returns NOTHING in all other conditions.</returns>
                public static List<byte> EncodeByteStreamWithLengthPrefix(IEnumerable<byte> UnencodedByteStream)
                {
                    try
                    {
                        // Return nothing if nothing is passed in
                        if (UnencodedByteStream == null || UnencodedByteStream.Count() == 0)
                        {
                            return null;
                        }

                        // If there is data, convert the source bytes into a list (easier to work with)
                        var SourceDataBytes = UnencodedByteStream.ToList();

                        // Save the count
                        int DataCount = SourceDataBytes.Count;

                        // Get the VarInt bytes to prefix with for this count
                        var VarIntBytes = ToEncodedBytes(DataCount);

                        // Create the full encoded stream
                        var FullEncodedStreamOfBytes = new List<byte>();

                        // Add the count bytes and the source data bytes
                        FullEncodedStreamOfBytes.AddRange(VarIntBytes);
                        FullEncodedStreamOfBytes.AddRange(UnencodedByteStream);

                        // Return the stream
                        return FullEncodedStreamOfBytes;
                    }
                    catch (Exception ex)
                    {
                        // Return nothing if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return null;
                    }
                }

                /// <summary>
                /// Used to convert a decimal number to a list of VarInt formatted bytes.
                /// </summary>
                /// <param name="NumberToConvert">The decimal number to convert.</param>
                /// <returns>Returns a list of bytes in VarInt format if the conversion is successful. Returns NOTHING in all other conditions.</returns>
                public static List<byte> ToEncodedBytes(int NumberToConvert)
                {
                    try
                    {
                        // Get the VarInt binary string using lower level method
                        string VarIntBinaryString = ToEncodedBinaryString(NumberToConvert);

                        // Return nothing if conversion failed
                        if (string.IsNullOrEmpty(VarIntBinaryString) == true)
                        {
                            return null;
                        }

                        // If encoding works, chunk string into binary byte strings
                        var BinaryByteStrings = Helpers.GetBinaryStringsInChunksOfN(VarIntBinaryString, 8);

                        // Create list to hold converted bytes to return
                        var VarIntBytes = new List<byte>();

                        // Loop through and convert bytes
                        foreach (var BITESTRING in BinaryByteStrings)
                        {
                            VarIntBytes.Add(System.Convert.ToByte(Bits.ToByte(BITESTRING)));
                        }

                        // Return the list of bytes
                        return VarIntBytes;
                    }
                    catch (Exception ex)
                    {
                        // Return nothing if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return null;
                    }
                }
                /// <summary>
                /// Used to convert a decimal number to a binary string representation of the VarInt byte format of the number.
                /// </summary>
                /// <param name="NumberToConvert">The decimal number to convert.</param>
                /// <returns>Returns a binary string in VarInt format if the conversion is successful. Returns an empty string in all other conditions.</returns>
                public static string ToEncodedBinaryString(int NumberToConvert)
                {
                    try
                    {
                        // Return empty string if number is negative
                        if (NumberToConvert < 0) return string.Empty;

                        // Convert the number to a standard binary string
                        string StandardBinaryString = Bits.ToBinaryString(NumberToConvert);

                        // Return empty string if conversion fails
                        if (string.IsNullOrEmpty(StandardBinaryString) == true) return string.Empty;

                        // Chunk string into groups of 7 bits b/c VarInt is 8 bits but 1st bit is just ID for last byte of count (1 means another count byte coming, 0 means last count byte)
                        var GroupsOf7Bits = Helpers.GetBinaryStringsInChunksOfN(StandardBinaryString, 7);

                        // If there are multiple groups, throw away any leading 0's (except the last one b/c number could be 0)
                        while (GroupsOf7Bits.Count > 1 && GroupsOf7Bits[0] == "0000000")
                        {
                            GroupsOf7Bits.RemoveAt(0);
                        }

                        // Reverse the order for little endian
                        GroupsOf7Bits.Reverse();

                        // Add 0 in last position / byte
                        GroupsOf7Bits[GroupsOf7Bits.Count - 1] = "0" + GroupsOf7Bits[GroupsOf7Bits.Count - 1];

                        // Check if there is more than one byte for the count
                        if (GroupsOf7Bits.Count > 1)
                        {
                            // If so, add 1's in all bytes but last
                            for (int i = 0; i <= GroupsOf7Bits.Count - 2; i++)
                            {
                                GroupsOf7Bits[i] = "1" + GroupsOf7Bits[i];
                            }
                        }

                        // Merge the chunks back together once the indicator bits are added (each chunk now back up to 8 bits)
                        string MergedChunksWithIndicatorBits = Helpers.MergeBinaryStringChunksTogether(GroupsOf7Bits);

                        // Return the string
                        return MergedChunksWithIndicatorBits;
                    }
                    catch (Exception ex)
                    {
                        // Return empty string if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return string.Empty;
                    }
                }

                /// <summary>
                /// Used to convert a list of bytes in VarInt Base128 format to a 32 bit integer value.
                /// </summary>
                /// <param name="EncodedVarIntBytesToConvert">The VarInt formatted list of bytes to convert.</param>
                /// <returns>Returns the 32 bit converted number of the conversion is successful. Returns NOTHING if the conversion fails.</returns>
                public static Nullable<int> ToInteger(IEnumerable<byte> EncodedVarIntBytesToConvert)
                {
                    try
                    {
                        // Return nothing if nothing is passed in
                        if (EncodedVarIntBytesToConvert == null || EncodedVarIntBytesToConvert.Count() == 0)
                        {
                            return null;
                        }

                        // If there are bytes, create string to hold the binary representation of the full number for the main function
                        string BinaryString = string.Empty;

                        // Loop through and create binary string
                        foreach (var BITE in EncodedVarIntBytesToConvert)
                        {
                            BinaryString += Bits.ToBinaryString(BITE);
                        }

                        // Once created, use common method
                        return ToInteger(BinaryString);
                    }
                    catch (Exception ex)
                    {
                        // Return nothing if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return null;
                    }
                }
                /// <summary>
                /// Used to convert a binary string in VarInt Base128 format to a 32 bit integer value.
                /// </summary>
                /// <param name="EncodedVarIntBinaryStringToConvert">The VarInt binary string to convert (7 bit binaries with leading 8th bit indicating if it is the final byte in the number).</param>
                /// <returns>Returns the 32 bit converted number of the conversion is successful. Returns NOTHING if the conversion fails.</returns>
                public static Nullable<int> ToInteger(string EncodedVarIntBinaryStringToConvert)
                {
                    try
                    {
                        // Ensure string is an 8 bit byte lengths
                        EncodedVarIntBinaryStringToConvert = Helpers.PadBinaryStringToSpecifiedLenghtMultiple(EncodedVarIntBinaryStringToConvert, 8);

                        // Create list to hold all 8 bit bytes in source string
                        var ListOfSource8BitBinaryStrings = new List<string>();

                        // Loop through and chunk into byte-length binary strings
                        for (int i = 0; i <= EncodedVarIntBinaryStringToConvert.Length - 1; i += 8)
                        {
                            ListOfSource8BitBinaryStrings.Add(EncodedVarIntBinaryStringToConvert.Substring(i, 8));
                        }

                        // Reverse the list when done b/c VarInt bytes come in backwards (small bytes first / little endian)
                        ListOfSource8BitBinaryStrings.Reverse();

                        // Loop through each entry in the list and remove the fist digit / bit (in VarInt, this is used to identify the final byte of the count (bytes starting w/ 1 are NOT final count byte)
                        for (int i = 0; i <= ListOfSource8BitBinaryStrings.Count - 1; i++)
                        {
                            ListOfSource8BitBinaryStrings[i] = ListOfSource8BitBinaryStrings[i].Substring(1);
                        }

                        // Merge string back into one big string (each chunk will only have 7 bits now b/c leading bit was removed)
                        string MergedBinaryStringOf7Bits = string.Empty;
                        foreach (var CHUNK in ListOfSource8BitBinaryStrings)
                        {
                            MergedBinaryStringOf7Bits += CHUNK;
                        }

                        // Pad back to full 8 bit byte length when done
                        MergedBinaryStringOf7Bits = Helpers.PadBinaryStringToSpecifiedLenghtMultiple(MergedBinaryStringOf7Bits, 8);

                        // Split into new bytes of 8 bit groupings
                        var NewBytesWithoutLeadingBitCountIdenifier = new List<string>();
                        for (int i = 0; i <= MergedBinaryStringOf7Bits.Length - 1; i += 8)
                        {
                            NewBytesWithoutLeadingBitCountIdenifier.Add(MergedBinaryStringOf7Bits.Substring(i, 8));
                        }

                        // Convert to bytes
                        var ConvertedBytes = new List<byte>();
                        foreach (var BINSTRING in NewBytesWithoutLeadingBitCountIdenifier)
                        {
                            ConvertedBytes.Add(System.Convert.ToByte(Bits.ToByte(BINSTRING)));
                        }

                        // Ensure array holds 4 bytes b/c converting to int32 (i.e. should be 4 8 bit bytes)
                        while (ConvertedBytes.Count < 4)
                        {
                            // Pad w/ leading 0's if less than 4 bytes
                            ConvertedBytes.Insert(0, 0);
                        }

                        // Return nothing if there's more than 4 bytes in there (i.e. an int 64 or some other long binary string was passed in)
                        if (ConvertedBytes.Count > 4) return null;

                        // Reverse byte order if system is little endian (Windows is)
                        if (System.BitConverter.IsLittleEndian == true)
                        {
                            ConvertedBytes.Reverse();
                        }

                        // Use bit converted to turn back into integer
                        return System.BitConverter.ToInt32(ConvertedBytes.ToArray(), 0);
                    }
                    catch (Exception ex)
                    {
                        // Return nothing if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return null;
                    }
                }

            }

            public struct Units
            {

                /// <summary>
                /// Used to unit convert bytes to kilobytes.
                /// </summary>
                /// <param name="NumberOfBytes">The size in bytes to convert.</param>
                /// <param name="NumberOfDigitsToRoundTo">OPTIONAL:  The number of digits of precision to return in the result (e.g. "2" would return "X.XX").</param>
                /// <returns>Returns the number of kilobytes for the specified byte count.</returns>
                public static double BytesToKBs(double NumberOfBytes, Nullable<int> NumberOfDigitsToRoundTo = null)
                {
                    // Compute the KBs
                    double KBs = NumberOfBytes / System.Math.Pow(2, 10);

                    // Return the value if user requested no rounding
                    if (NumberOfDigitsToRoundTo == null || NumberOfDigitsToRoundTo < 0)
                    {
                        return KBs;
                    }

                    // If user wanted rounding, round and return
                    return System.Math.Round(KBs, System.Convert.ToInt32(NumberOfDigitsToRoundTo), MidpointRounding.AwayFromZero);
                }
                /// <summary>
                /// Used to unit convert bytes to megabytes.
                /// </summary>
                /// <param name="NumberOfBytes">The size in bytes to convert.</param>
                /// <param name="NumberOfDigitsToRoundTo">OPTIONAL:  The number of digits of precision to return in the result (e.g. "2" would return "X.XX").</param>
                /// <returns>Returns the number of megabytes for the specified byte count.</returns>
                public static double BytesToMBs(double NumberOfBytes, Nullable<int> NumberOfDigitsToRoundTo = null)
                {
                    // Compute the MBs
                    double MBs = NumberOfBytes / System.Math.Pow(2, 20);

                    // Return the value if user requested no rounding
                    if (NumberOfDigitsToRoundTo == null || NumberOfDigitsToRoundTo < 0)
                    {
                        return MBs;
                    }

                    // If user wanted rounding, round and return
                    return System.Math.Round(MBs, System.Convert.ToInt32(NumberOfDigitsToRoundTo), MidpointRounding.AwayFromZero);
                }
                /// <summary>
                /// Used to unit convert bytes to gigabytes.
                /// </summary>
                /// <param name="NumberOfBytes">The size in bytes to convert.</param>
                /// <param name="NumberOfDigitsToRoundTo">OPTIONAL:  The number of digits of precision to return in the result (e.g. "2" would return "X.XX").</param>
                /// <returns>Returns the number of gigabytes for the specified byte count.</returns>
                public static double BytesToGBs(double NumberOfBytes, Nullable<int> NumberOfDigitsToRoundTo = null)
                {
                    // Compute the GBs
                    double GBs = NumberOfBytes / System.Math.Pow(2, 30);

                    // Return the value if user requested no rounding
                    if (NumberOfDigitsToRoundTo == null || NumberOfDigitsToRoundTo < 0)
                    {
                        return GBs;
                    }

                    // If user wanted rounding, round and return
                    return System.Math.Round(GBs, System.Convert.ToInt32(NumberOfDigitsToRoundTo), MidpointRounding.AwayFromZero);
                }

                /// <summary>
                /// Used to unit convert kilobytes into bytes.
                /// </summary>
                /// <param name="KiloBytes">The number of kilobytes to convert.</param>
                /// <returns>Returns the number of bytes for the specified kilobyte count.</returns>
                public static double KBsToBytes(double KiloBytes)
                {
                    // Compute the result and return it
                    return System.Math.Round(KiloBytes * System.Math.Pow(2, 10), 0, MidpointRounding.AwayFromZero);
                }
                /// <summary>
                /// Used to unit convert megabytes into bytes.
                /// </summary>
                /// <param name="MegaBytes">The number of kilobytes to convert.</param>
                /// <returns>Returns the number of bytes for the specified megabytes count.</returns>
                public static double MBsToBytes(double MegaBytes)
                {
                    // Compute the result and return it
                    return System.Math.Round(MegaBytes * System.Math.Pow(2, 20), 0, MidpointRounding.AwayFromZero);
                }
                /// <summary>
                /// Used to unit convert gigabytes into bytes.
                /// </summary>
                /// <param name="GigaBytes">The number of kilobytes to convert.</param>
                /// <returns>Returns the number of bytes for the specified gigabytes count.</returns>
                public static double GBsToBytes(double GigaBytes)
                {
                    // Compute the result and return it
                    return System.Math.Round(GigaBytes * System.Math.Pow(2, 30), 0, MidpointRounding.AwayFromZero);
                }

            }

            public struct Text
            {

                /// <summary>
                /// Used to convert an ASCII string into a list of bytes.
                /// </summary>
                /// <param name="StringToConvert">The string to convert to bytes.</param>
                /// <returns>Returns a list of bytes. Returns an empty list if the conversion fails.</returns>
                public static List<byte> ToBytesFromASCII(string StringToConvert)
                {
                    try
                    {
                        // Create byte array
                        var ByteArray = System.Text.Encoding.ASCII.GetBytes(StringToConvert);

                        // Return empty list if there's no bytes
                        if (ByteArray == null)
                        {
                            return new List<byte>();
                        }

                        // If there is data, convert it to list and return it
                        return ByteArray.ToList();
                    }
                    catch (Exception ex)
                    {
                        // Return empty list if conversion fails
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return new List<byte>();
                    }
                }

                /// <summary>
                /// Used to convert a Unicode string into a list of bytes.
                /// </summary>
                /// <param name="StringToConvert">The string to convert to bytes.</param>
                /// <returns>Returns a list of bytes. Returns an empty list if the conversion fails.</returns>
                public static List<byte> ToBytesFromUnicodeString(string StringToConvert)
                {
                    try
                    {
                        // Create byte array
                        var ByteArray = System.Text.Encoding.Unicode.GetBytes(StringToConvert);

                        // Return empty list if there's no bytes
                        if (ByteArray == null)
                        {
                            return new List<byte>();
                        }

                        // If there is data, convert it to list and return it
                        return ByteArray.ToList();
                    }
                    catch (Exception ex)
                    {
                        // Return empty list if conversion fails
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return new List<byte>();
                    }
                }

                /// <summary>
                /// Used to convert a byte to an ASCII string character.
                /// </summary>
                /// <param name="ByteToConvert">The byte to convert to string.</param>
                /// <returns>Returns a string representation of the byte if the conversion works. Returns an empty string if the conversion fails.</returns>
                public static string ToASCIIString(byte ByteToConvert)
                {
                    // Add byte to list and return result of common method
                    return ToASCIIString(new byte[] { ByteToConvert }.ToList());
                }

                /// <summary>
                /// Used to convert a list of bytes to an ASCII string.
                /// </summary>
                /// <param name="BytesToConvert">The list of bytes to convert to string.</param>
                /// <returns>Returns a string representation of the bytes if the conversion works. Returns an empty string if the conversion fails.</returns>
                public static string ToASCIIString(IEnumerable<byte> BytesToConvert)
                {
                    try
                    {
                        // Try to convert and return the result
                        return System.Text.Encoding.ASCII.GetString(BytesToConvert.ToArray());
                    }
                    catch (Exception ex)
                    {
                        // Return an empty string if conversion fails
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return string.Empty;
                    }
                }

                /// <summary>
                /// Used to convert a byte to a Unicode string character.
                /// </summary>
                /// <param name="ByteToConvert">The byte to convert to string.</param>
                /// <returns>Returns a string representation of the byte if the conversion works. Returns an empty string if the conversion fails.</returns>
                public static string ToUnicodeString(byte ByteToConvert)
                {
                    // Add byte to list and return result of common method
                    return ToUnicodeString(new byte[] { ByteToConvert }.ToList());
                }

                /// <summary>
                /// Used to convert a list of bytes to a Unicode string.
                /// </summary>
                /// <param name="BytesToConvert">The list of bytes to convert to string.</param>
                /// <returns>Returns a string representation of the bytes if the conversion works. Returns an empty string if the conversion fails.</returns>
                public static string ToUnicodeString(IEnumerable<byte> BytesToConvert)
                {
                    try
                    {
                        // Try to convert and return the result
                        return System.Text.Encoding.Unicode.GetString(BytesToConvert.ToArray());
                    }
                    catch (Exception ex)
                    {
                        // Return an empty string if conversion fails
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return string.Empty;
                    }
                }

            }

            private struct Helpers
            {

                public static List<string> GetBinaryStringsInChunksOfN(string BinaryString, int GroupSize)
                {
                    // Ensure string is in 8 bit chunks
                    BinaryString = PadBinaryStringToSpecifiedLenghtMultiple(BinaryString, GroupSize);

                    // Create list to return
                    var ListToReturn = new List<string>();

                    // Loop through and chunk it
                    for (int i = 0; i <= BinaryString.Length - 1; i += GroupSize)
                    {
                        ListToReturn.Add(BinaryString.Substring(i, GroupSize));
                    }

                    // Return the list
                    return ListToReturn;
                }

                public static string MergeBinaryStringChunksTogether(List<string> ListOfBinsaryStrings)
                {
                    // Create string to hold merged binary string
                    string MergedBinary = string.Empty;

                    // Loop through and perform merge
                    foreach (var BINSTRING in ListOfBinsaryStrings)
                    {
                        MergedBinary += BINSTRING;
                    }

                    // Return the string
                    return MergedBinary;
                }

                public static string PadBinaryStringToSpecifiedLenghtMultiple(string BinaryStringToPad, int MultipleToPadTo)
                {
                    // Use 0 if nothing is passed in
                    if (string.IsNullOrEmpty(BinaryStringToPad) == true)
                    {
                        BinaryStringToPad = "0";
                    }

                    // Keep padding w/ 0's until requested length is reached
                    while (BinaryStringToPad.Length % MultipleToPadTo != 0)
                    {
                        BinaryStringToPad = BinaryStringToPad.PadLeft(BinaryStringToPad.Length + 1, System.Convert.ToChar("0"));
                    }

                    // Return the string when done
                    return BinaryStringToPad;
                }

                public static string PadBinaryNumberStringsToFixedLength(string BinaryStringToPad, int LengthToPadTo)
                {
                    // Return empty string if nothing is passed in
                    if (string.IsNullOrEmpty(BinaryStringToPad) == true)
                    {
                        return string.Empty;
                    }

                    // Return empty string if length is bad
                    if (LengthToPadTo <= 0)
                    {
                        return string.Empty;
                    }

                    // If the string is too long, return empty string
                    if (BinaryStringToPad.Length > LengthToPadTo)
                    {
                        return string.Empty;
                    }

                    // If padding is possible, pad and return
                    return BinaryStringToPad.PadLeft(LengthToPadTo, System.Convert.ToChar("0"));
                }

            }

        }

        public struct Checksum
        {

            /// <summary>
            /// Used to compute the XOR checksum compliment of a given list of bytes.
            /// </summary>
            /// <param name="BytesToComputeFor">The list of bytes to compute the XOR compliment of.</param>
            /// <returns>Returns the XOR compliment byte of the specified list of bytes if the checksum is computed successfully. Returns NOTHING in all other conditions.</returns>
            public static Nullable<byte> ComputeXorCompliment(IEnumerable<byte> BytesToComputeFor)
            {
                try
                {
                    // Return nothing if inputs are bad
                    if (BytesToComputeFor == null) return null;

                    // Create var to hold working checksum
                    byte WorkingCheckSum = 0;

                    // Loop through compute checksum
                    foreach (var BITE in BytesToComputeFor)
                    {
                        WorkingCheckSum = System.Convert.ToByte(WorkingCheckSum ^ BITE);
                    }

                    // Return the checksum when done
                    return WorkingCheckSum;
                }
                catch (Exception ex)
                {
                    // Return nothing if error occurs
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
            }

            public struct CRC32
            {

                /// <summary>
                /// Helper class for computing CRCs.
                /// </summary>
                private class CRCHelper
                {

                    /// <summary>
                    /// Internal var to hold the hash table when computing checksums.
                    /// </summary>
                    private UInt32[] HashTable = new UInt32[256];

                    /// <summary>
                    /// Build table in the constructor.
                    /// </summary>
                    public CRCHelper()
                    {
                        // Create generator polynomial
                        UInt32 GeneratorPolynomial = 0xEDB88320U;

                        // Loop through and populate hash table
                        for (int i = 0; i <= HashTable.Count() - 1; i++)
                        {
                            // Create var to hold the current item for the hash table
                            UInt32 HashItem = System.Convert.ToUInt32(i);

                            // Loop through all 8 columns of the hash table
                            for (int j = 8; j >= 1; j += -1)
                            {
                                // Create hash item based on polynomial bit shifting mumbo-jumbo
                                if ((HashItem & 1) == 1)
                                {
                                    HashItem = System.Convert.ToUInt32((HashItem >> 1) ^ GeneratorPolynomial);
                                }
                                else
                                {
                                    HashItem >>= 1;
                                }
                            }

                            // Add the item to the table
                            HashTable[i] = HashItem;
                        }
                    }

                    /// <summary>
                    /// Method to compute checksum from a list of bytes.
                    /// </summary>
                    public UInt32 ComputeChecksum(byte[] Bytes)
                    {
                        // Create var to hold the CRC
                        UInt32 CRC = 0xFFFFFFFFU;

                        // Loop through and compute checksum
                        for (int i = 0; i <= Bytes.Length - 1; i++)
                        {
                            byte index = System.Convert.ToByte(((CRC) & 0xFF) ^ Bytes[i]);
                            CRC = System.Convert.ToUInt32((CRC >> 8) ^ HashTable[index]);
                        }

                        // Return the check sum
                        return ~CRC;
                    }

                    /// <summary>
                    /// Method to get the checksum bytes from a list of bytes.
                    /// </summary>
                    public byte[] GetChecksumBytes(byte[] Bytes)
                    {
                        // Use bit converted to get and return the bytes
                        return BitConverter.GetBytes(ComputeChecksum(Bytes));
                    }
                }

                /// <summary>
                /// Used to compute a CRC32 and return the result.
                /// </summary>
                /// <param name="BytesToComputeFor">The list of bytes to compute the CRC for.</param>
                /// <returns>Returns the CRC for the list of bytes.</returns>
                public static UInt32 Compute(IEnumerable<byte> BytesToComputeFor)
                {
                    try
                    {
                        // Compute CRC using helper and return the result
                        return new CRCHelper().ComputeChecksum(BytesToComputeFor.ToArray());
                    }
                    catch (Exception ex)
                    {
                        // Return 0 if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return 0;
                    }
                }

                /// <summary>
                /// Used to compute a CRC32 and return the result.
                /// </summary>
                /// <param name="StringToComputeFor">The string to compute the CRC of.</param>
                /// <returns>Returns the CRC for a given string.</returns>
                public static UInt32 Compute(string StringToComputeFor)
                {
                    try
                    {
                        // Get the bytes for the string
                        var StringBytes = System.Text.Encoding.ASCII.GetBytes(StringToComputeFor);

                        // Use common method and return the result
                        return Compute(StringBytes);
                    }
                    catch (Exception ex)
                    {
                        // Return 0 if error occurs
                        System.Diagnostics.Trace.WriteLine(ex.Message);
                        return 0;
                    }
                }

            }

        }

    }

}
