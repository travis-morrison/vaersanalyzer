﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Vaers
{

    public partial class App : Application
    {

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            // Add handler for unhandled exceptions on threads
            AppDomain.CurrentDomain.UnhandledException += Application_ThreadException;

            // Clean up old logs
            ThisApp.Methods.Logging.DeleteOldLogs();
        }

        private void Application_Exit(object sender, ExitEventArgs e)
        {
            // Dump log to file and delete old logs
            ThisApp.Methods.Logging.WriteQueueToDiskImmediately();
            ThisApp.Methods.Logging.DeleteOldLogs();
        }

        private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            // If exception can be ignored, ignore and bail
            if (IsExceptionIgnorable(e) == true)
            {
                e.Handled = true;
                return;
            }

            // If it's an actual unhandled exception that can't be ignored, process it
            UnhandledExceptionHandler(e.Exception);
        }
        private void Application_ThreadException(object sender, System.UnhandledExceptionEventArgs e)
        {
            // Create var to hold exception info
            var Exception = new System.Exception("Unknown exception thrown on thread!");
            if (e.ExceptionObject != null && e.ExceptionObject is Exception)
            {
                Exception = (Exception)e.ExceptionObject;
            }

            // Invoke main to get crash info to display
            Application.Current.Dispatcher.Invoke(() => UnhandledExceptionHandler(Exception));
        }
        private void UnhandledExceptionHandler(System.Exception Exception)
        {
            // Try to get the log written to disk so nothing is lot
            ThisApp.Methods.Logging.WriteMsg("!!! -- >> UH-OH, UNHANDLED APP-LEVEL EXCEPTION!:", ThisApp.Methods.Logging.MsgTypeOptions.AsIs);
            ThisApp.Methods.Logging.WriteMsgWithException("An untrapped application level exception has occurred!", Exception);
            ThisApp.Methods.Logging.WriteQueueToDiskImmediately();

            // If custom msg fails, create string for simple message
            string ExceptionMsg = Exception.Message;

            // Add inner exception if it exists
            if (Exception.InnerException != null)
            {
                ExceptionMsg += Environment.NewLine + Environment.NewLine + Exception.InnerException.Message;
            }

            // Add the stack trace
            ExceptionMsg += Environment.NewLine + Environment.NewLine + Exception.StackTrace;

            // Show simple message
            StaticMethods.UserPrompts.ShowUnexpectedErrorMsg("The software must shut down due to an unexpected error.", ExceptionMessage: ExceptionMsg);

            // Force shutdown
            ThisApp.Terminate();
        }
        private bool IsExceptionIgnorable(System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            try
            {
                // It's not ignorable if we can't tell what it is
                if (e == null || e.Exception == null || string.IsNullOrEmpty(e.Exception.Message) == true) return false;

                // There's some new occasional clipboard error in Win10 when copying from datagrid w/ Ctrl+C, ignore it
                if (e.Exception.Message.ToUpper().Contains("CLIPBRD_E_CANT_OPEN") == true) return true;

                // Assume not ignorable if no match is found
                return false;
            }
            catch (Exception ex)
            {
                // Assume it isn't by default if something goes wrong checking
                ThisApp.Methods.Logging.WriteMsgWithException("Exception handling ignorable exception.", ex);
                return false;
            }
        }

    }

}
